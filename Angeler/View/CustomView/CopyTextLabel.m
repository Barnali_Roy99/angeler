//
//  CopyTextLabel.m
//  Angeler
//
//  Created by AJAY on 12/12/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "CopyTextLabel.h"
#import "UIWindow+VisibleViewController.h"

@implementation CopyTextLabel

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setGesture];
    }
    return self;
}


-(void)setGesture
{
     [self setUserInteractionEnabled:YES];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(textCopiedLongTap:)];
    longPress.minimumPressDuration = 0.5;
    [self addGestureRecognizer:longPress];
    
//    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textCopiedClickTap:)];
//    tapGes.numberOfTapsRequired = 2;
//    [self addGestureRecognizer:tapGes];
}

#pragma mark - Gesture Action

- (void)textCopiedLongTap:(UILongPressGestureRecognizer*)longPressGest
{
    if(UIGestureRecognizerStateBegan == longPressGest.state) {
        [self textCopied];
    }
}

- (void)textCopiedClickTap:(UITapGestureRecognizer*)tapGest
{
    [self textCopied];
}

-(void)textCopied
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.text;
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Text_Copied", nil)
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    
    
    [alert addAction:okButton];
    
    [[[AppDel window] visibleViewController] presentViewController:alert animated:YES completion:^{
        NSLog(@"Text Copied to clipboard %@",pasteboard.string);
    }];
    
}

@end
