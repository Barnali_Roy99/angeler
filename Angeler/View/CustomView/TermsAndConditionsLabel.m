//
//  TermsAndConditionsLabel.m
//  Angeler
//
//  Created by AJAY on 27/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "TermsAndConditionsLabel.h"
#import "TermsConditionsViewController.h"

@implementation TermsAndConditionsLabel


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setLabelText];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    textContainer.size = self.bounds.size;
}



-(void)setLabelText
{
    NSString *TC_text = NSLocalizedString(@"TermsConditions_Text", nil) ;
    NSString *TC_underLineText = NSLocalizedString(@"TermsConditions_UnderlineText", nil) ;
     NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:TC_text];
    self.attributedText = [Helper getUnderlineText:TC_text :[TC_text rangeOfString:TC_underLineText] withColor:[UIColor mediumBlueColor]];
    
    
    // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
    layoutManager = [[NSLayoutManager alloc] init];
    textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    textStorage = [[NSTextStorage alloc] initWithAttributedString:attributedString];
    
    // Configure layoutManager and textStorage
    [layoutManager addTextContainer:textContainer];
    [textStorage addLayoutManager:layoutManager];
    
    // Configure textContainer
    textContainer.lineFragmentPadding = 0.0;
    textContainer.lineBreakMode = self.lineBreakMode;
    textContainer.maximumNumberOfLines = self.numberOfLines;
    [self setGesture];
    
}

-(void)setGesture
{
    [self setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)];
    [self addGestureRecognizer:tapges];
 
}

- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture
{
    
   
    
    NSString *TC_text = NSLocalizedString(@"TermsConditions_Text", nil) ;
    NSString *TC_underLineText = NSLocalizedString(@"TermsConditions_UnderlineText", nil) ;
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    CGSize labelSize = tapGesture.view.bounds.size;
    CGRect textBoundingBox = [layoutManager usedRectForTextContainer:textContainer];
    CGPoint textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                         locationOfTouchInLabel.y - textContainerOffset.y);
    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                       inTextContainer:textContainer
                              fractionOfDistanceBetweenInsertionPoints:nil];
    NSRange linkRange = [TC_text rangeOfString:TC_underLineText];
    if (NSLocationInRange(indexOfCharacter, linkRange)) {
        [self redirectToTermsConditionsScreen];
    }
}

-(void)redirectToTermsConditionsScreen
{
    UIViewController *currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController visibleViewController];
    TermsConditionsViewController *obj = [[TermsConditionsViewController alloc] initWithNibName:@"TermsConditionsViewController" bundle:nil];
    obj.isShowBackBtn = YES;
    [currentVc.navigationController pushViewController:obj animated:YES];
}


@end
