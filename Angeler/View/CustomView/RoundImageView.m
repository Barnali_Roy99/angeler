//
//  RoundImageView.m
//  Angeler
//
//  Created by AJAY on 19/07/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "RoundImageView.h"

@implementation RoundImageView

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.layer.cornerRadius = self.frame.size.width/2.0f;
    self.clipsToBounds = TRUE;
}


@end
