//
//  TermsAndConditionsLabel.h
//  Angeler
//
//  Created by AJAY on 27/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TermsAndConditionsLabel : UILabel
{
    NSTextContainer *textContainer;
    NSLayoutManager *layoutManager;
    NSTextStorage *textStorage;
}

@end

NS_ASSUME_NONNULL_END
