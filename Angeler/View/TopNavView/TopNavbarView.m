//
//  TopNavbarView.m
//  Angeler
//
//  Created by AJAY on 19/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "TopNavbarView.h"
#import "SideMenuViewController.h"
#import "AddTripStepOneController.h"
#import "AddShipmentsStepOneController.h"
#import "SingleLocationMapController.h"
#import "MyTripsListViewController.h"
#import "MyShipmentsListController.h"
#import "AddShipmentsStepTwoController.h"
#import "ChooseAngelerStepOneController.h"
#import "DriverTripStartVC.h"
#import "DriverRequestListVC.h"
#import "DriverRequestDetailsVC.h"
#import "CustomerTrackTrip.h"
#import "CharityListVCViewController.h"
#import "AddCharityVC.h"
#import "TaxiDashboardVC.h"
#import "AddShopItemDetailsVC.h"
#import "BuyerShoppingListVC.h"
#import "CharityDetailsVC.h"
#import "CharityListVCViewController.h"
#import "TrackShoppingVC.h"
#import "BuyerShoppingListVC.h"
#import "SenderRequestListVC.h"
#import "AddLocalDeliveryVC.h"
#import "LocalDeliveryRequestListVC.h"
#import "SenderRequestDetailsVC.h"
#import "AngelerAcceptanceDetailsVC.h"
#import "SenderRequestListVC.h"
#import "AngelerAcceptanveListVC.h"
#import "LocalDeliveryDashboradVC.h"

//#import "BankInfoViewController.h"
//#import "WalletViewController.h"

@implementation TopNavbarView

#pragma mark - Init Method
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    _customConstraints = [[NSMutableArray alloc] init];
    
    UIView *view = nil;
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"TopNavbarView" owner:self options:nil];
    for (id object in objects) {
        if ([object isKindOfClass:[UIView class]]) {
            view = object;
            break;
        }
    }
    
    if (view != nil) {
        _containerView = view;
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:view];
        [self setNeedsUpdateConstraints];
        
    }
    currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    currentVc.navigationController.navigationBarHidden = YES;
    
    _titleLogoImg.hidden = YES;
    _squareTitleLogoImg.hidden = YES;
    _notificationBtn.hidden = YES;
    _plusBtn.hidden = YES;
    _plusImageIcon.hidden = YES;
    _btnSearch.hidden = YES;
    _highlightView.hidden = YES;
    [_mainView dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
}

- (void)updateConstraints
{
    [self removeConstraints:self.customConstraints];
    [self.customConstraints removeAllObjects];
    
    if (self.containerView != nil) {
        UIView *view = self.containerView;
        NSDictionary *views = NSDictionaryOfVariableBindings(view);
        
        [self.customConstraints addObjectsFromArray:
         [NSLayoutConstraint constraintsWithVisualFormat:
          @"H:|[view]|" options:0 metrics:nil views:views]];
        [self.customConstraints addObjectsFromArray:
         [NSLayoutConstraint constraintsWithVisualFormat:
          @"V:|[view]|" options:0 metrics:nil views:views]];
        
        [self addConstraints:self.customConstraints];
    }
    
    [super updateConstraints];
}

#pragma mark - General Methods

-(void)showNavMenuBtn
{
    _backBtn.hidden=YES;
    _backArrowImgView.hidden=YES;
    _menuBtn.hidden=NO;
    
    //Add Swipe gesture
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [currentVc.view addGestureRecognizer:rightSwipe];
}

-(void)showHighlightView{
    [_highlightView setHidden:NO];
}

-(void)hideHighlightView{
    [_highlightView setHidden:YES];
}

-(void)showNavbackBtn
{
    _backBtn.hidden=NO;
    _backArrowImgView.hidden = NO;
    _menuBtn.hidden=YES;
}

-(void)showNotificationBtn
{
    _notificationBtn.hidden = NO;
    _plusBtn.hidden = YES;
    _plusImageIcon.hidden = YES;
}

-(void)showPlusBtn
{
    _notificationBtn.hidden = YES;
    _plusBtn.hidden = NO;
    _plusImageIcon.hidden = NO;
}
-(void)showTitleLogo
{
    _titleLogoImg.hidden = NO;
}

-(void)showSquareTitleLogo
{
    _squareTitleLogoImg.hidden = NO;
}

-(void)showSearchBtn
{
    _notificationBtn.hidden = YES;
    _plusImageIcon.hidden = YES;
    _plusBtn.hidden = YES;
    _btnSearch.hidden = NO;
    
}

#pragma mark - Right Swipe
- (void)swipeRight:(UISwipeGestureRecognizer*)swipeGest{
    [AppDel openSideViewFromViewController:currentVc];
}

#pragma mark - Title Set
-(void)setTitle:(NSString*)title
{
    _titleLabel.text = title;
}

#pragma mark - Button Action

- (IBAction)onBackBtnClicked:(id)sender {
    
    CATransition* transition = [CATransition animation];
         transition.duration = 0.3;
         transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
         transition.type = kCATransitionMoveIn; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
         transition.subtype = kCATransitionFromLeft; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    
    
    if ([currentVc isKindOfClass:[ChooseAngelerStepOneController class]] || [currentVc isKindOfClass:[AddShipmentsStepOneController class]])
    {
        MyShipmentsListController *myShipmentObj = [[MyShipmentsListController alloc] initWithNibName:@"MyShipmentsListController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:myShipmentObj];
    }
    else if ([currentVc isKindOfClass:[DriverRequestListVC class]])
    {
        [GlobalInfo showTaxiDashboardScreen];
    }
    else  if ([currentVc isKindOfClass:[DriverTripStartVC class]]  || [currentVc isKindOfClass:[CustomerTrackTrip class]]){
        [_delegate backButtonClicked];
    }
    
    else if ([currentVc isKindOfClass:[TaxiDashboardVC class]]){
        [GlobalInfo showHomeScreen];
    }
    else if ([currentVc isKindOfClass:[DriverRequestDetailsVC class]]){
        DriverRequestListVC *reqListObj = [[DriverRequestListVC alloc] initWithNibName:@"DriverRequestListVC" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:reqListObj];
    }
    else if([currentVc isKindOfClass:[CharityDetailsVC class]])
    {
        CharityListVCViewController *Obj = [[CharityListVCViewController alloc] initWithNibName:@"CharityListVCViewController" bundle:nil];
        [currentVc.navigationController pushViewController:Obj animated:YES];
    }
    else if([currentVc isKindOfClass:[TrackShoppingVC class]])
    {
        BuyerShoppingListVC *Obj = [[BuyerShoppingListVC alloc] initWithNibName:@"BuyerShoppingListVC" bundle:nil];
        [currentVc.navigationController pushViewController:Obj animated:YES];
    }
    else if([currentVc isKindOfClass:[LocalDeliveryRequestListVC class]] || [currentVc isKindOfClass:[SenderRequestListVC class]] || [currentVc isKindOfClass:[AngelerAcceptanveListVC class]])
    {
        LocalDeliveryDashboradVC *Obj = [[LocalDeliveryDashboradVC alloc] initWithNibName:@"LocalDeliveryDashboradVC" bundle:nil];
        [currentVc.navigationController.view.layer addAnimation:transition forKey:nil];
        [currentVc.navigationController pushViewController:Obj animated:NO];
        
    }
    else if([currentVc isKindOfClass:[AngelerAcceptanceDetailsVC class]])
    {
        AngelerAcceptanveListVC *Obj = [[AngelerAcceptanveListVC alloc] initWithNibName:@"AngelerAcceptanveListVC" bundle:nil];
        [currentVc.navigationController.view.layer addAnimation:transition forKey:nil];
        [currentVc.navigationController pushViewController:Obj animated:YES];
    }
    else if([currentVc isKindOfClass:[SenderRequestDetailsVC class]])
    {
        SenderRequestListVC *Obj = [[SenderRequestListVC alloc] initWithNibName:@"SenderRequestListVC" bundle:nil];
        [currentVc.navigationController.view.layer addAnimation:transition forKey:nil];
        [currentVc.navigationController pushViewController:Obj animated:YES];
    }
    else
    {
        [currentVc.navigationController popViewControllerAnimated:TRUE];
    }
    
}

- (IBAction)onMenuBtnClicked:(id)sender {
    [AppDel openSideViewFromViewController:currentVc];
}

- (IBAction)onPlusBtnClicked:(id)sender
{
    if ([currentVc isKindOfClass:[MyShipmentsListController class]])
    {
        if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Pending])
        {
            MyShipmentsListController *myShipmentObj = (MyShipmentsListController*)currentVc;
            [myShipmentObj showPendingAccountAlertPopUp];
        }
        
        else if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Rejected])
        {
            MyShipmentsListController *myShipmentObj = (MyShipmentsListController*)currentVc;
            [myShipmentObj showRejectedAccountAlertPopUp];
        }
        else if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_NotVerified])
        {
            MyShipmentsListController *myShipmentObj = (MyShipmentsListController*)currentVc;
            [myShipmentObj showNotVerifiedAccountAlertPopUp];
        }
        else
        {
            AddShipmentsStepOneController *delObj = [[AddShipmentsStepOneController alloc] initWithNibName:@"AddShipmentsStepOneController" bundle:nil];
            [currentVc.navigationController pushViewController:delObj animated:YES];
        }
    }
    else if ([currentVc isKindOfClass:[MyTripsListViewController class]])
    {
        if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Pending])
        {
            MyTripsListViewController *myTripObj = (MyTripsListViewController*)currentVc;
            [myTripObj showPendingAccountAlertPopUp];
        }
        else if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Rejected])
        {
            MyTripsListViewController *myTripObj = (MyTripsListViewController*)currentVc;
            [myTripObj showRejectedAccountAlertPopUp];
        }
        else if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_NotVerified])
        {
            MyTripsListViewController *myTripObj = (MyTripsListViewController*)currentVc;
            [myTripObj showNotVerifiedAccountAlertPopUp];
        }
        else
        {
            AddTripStepOneController *delObj = [[AddTripStepOneController alloc] initWithNibName:@"AddTripStepOneController" bundle:nil];
            [currentVc.navigationController pushViewController:delObj animated:YES];
        }
        
    }
    else if([currentVc isKindOfClass:[CharityListVCViewController class]])
    {
        AddCharityVC *Obj = [[AddCharityVC alloc] initWithNibName:@"AddCharityVC" bundle:nil];
        [currentVc.navigationController pushViewController:Obj animated:YES];
    }
    else if([currentVc isKindOfClass:[BuyerShoppingListVC class]])
    {
        AddShopItemDetailsVC *Obj = [[AddShopItemDetailsVC alloc] initWithNibName:@"AddShopItemDetailsVC" bundle:nil];
        [currentVc.navigationController pushViewController:Obj animated:YES];
    }
    else if([currentVc isKindOfClass:[SenderRequestListVC class]])
    {
        AddLocalDeliveryVC *Obj = [[AddLocalDeliveryVC alloc] initWithNibName:@"AddLocalDeliveryVC" bundle:nil];
        [currentVc.navigationController pushViewController:Obj animated:YES];
    }
    
    
}

- (IBAction)onSearchBtnClicked:(id)sender {
    [_delegate searchIconClicked];
}

- (IBAction)onNotificationBtnClicked:(id)sender
{
    
}
@end
