//
//  TopNavbarView.h
//  Angeler
//
//  Created by AJAY on 19/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol topNavActionDelegate <NSObject>
@optional
- (void)searchIconClicked;
-(void)backButtonClicked;

@end
@interface TopNavbarView : UIView
{
    UIViewController *currentVc;
}

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backArrowImgView;
@property (weak, nonatomic) IBOutlet UIButton *notificationBtn;
@property (weak, nonatomic) IBOutlet UIImageView *titleLogoImg;
@property (weak, nonatomic) IBOutlet UIButton *plusBtn;
@property (weak, nonatomic) IBOutlet UIButton *plusImageIcon;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *squareTitleLogoImg;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property(nonatomic,assign) id <topNavActionDelegate> delegate;
@property (weak, nonatomic) IBOutlet RoundView *highlightView;


- (IBAction)onBackBtnClicked:(id)sender;
- (IBAction)onMenuBtnClicked:(id)sender;
- (IBAction)onNotificationBtnClicked:(id)sender;
- (IBAction)onPlusBtnClicked:(id)sender;
- (IBAction)onSearchBtnClicked:(id)sender;


-(void)setTitle:(NSString*)title;
-(void)showNavMenuBtn;
-(void)showNavbackBtn;
-(void)showNotificationBtn;
-(void)showTitleLogo;
-(void)showPlusBtn;
-(void)showSquareTitleLogo;
-(void)showSearchBtn;
-(void)showHighlightView;
-(void)hideHighlightView;
  
@end

NS_ASSUME_NONNULL_END
