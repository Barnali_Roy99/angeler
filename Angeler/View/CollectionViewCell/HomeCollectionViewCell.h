//
//  HomeCollectionViewCell.h
//  Angeler
//
//  Created by AJAY on 24/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backgroundLogoView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titlelabel;

@end

NS_ASSUME_NONNULL_END
