//
//  AddressClass.m
//  Angeler
//
//  Created by AJAY on 10/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "AddressClass.h"

@implementation AddressClass
@synthesize Address,lat_Value,long_Value,addresId,addressType,operatingFromHrs,operatingToHrs,cityCode,cityId,cityName,countryCode,countryId,countryIsdCode,countryName;
-(id)init{
    if (self = [super init]) {
        Address = Empty;
        cityCode = Empty;
        cityId = Empty;
        cityName = Empty;
        countryCode = Empty;
        countryId = Empty;
        countryIsdCode = Empty;
        countryName = Empty;
        lat_Value = [NSNumber numberWithFloat:0.0];
        long_Value = [NSNumber numberWithFloat:0.0];
        addresId = Empty;
        addressType = Empty;
        operatingToHrs = Empty;
        operatingFromHrs = Empty;
    }
    return self;
}
@end
