//
//  AddressClass.h
//  Angeler
//
//  Created by AJAY on 10/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressClass : NSObject
@property (nonatomic, strong) NSString *Address;
@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSString *countryId;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *countryIsdCode;
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *cityCode;

@property (nonatomic, strong) NSNumber *lat_Value;
@property (nonatomic, strong) NSNumber *long_Value;
//marker type address
@property (nonatomic, strong) NSString *addressType;
@property (nonatomic, strong) NSString *addresId;
@property (nonatomic, strong) NSString *operatingFromHrs;
@property (nonatomic, strong) NSString *operatingToHrs;


@end
