//
//  GlobalInfo.h
//  Angeler
//
//  Created by AJAY on 14/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalInfo : NSObject


@property (nonatomic, strong) NSString *userName;//manadatory
@property (nonatomic, strong) NSString *userLastName;//manadatory
@property (nonatomic, strong) NSString *userFirstName;//manadatory
@property (nonatomic, strong) NSString *socialId;//manadatory
@property (nonatomic, strong) NSString *userEmail;//manadatory

@property (nonatomic, strong) NSString *deviceId;//manadatory
@property (nonatomic, strong) NSString *userUniqueAngelerID;//manadatory
@property (nonatomic, strong) NSString *userAngelerStatus;//manadatory
@property (nonatomic, strong) NSString *userSelectedCountryID;//manadatory

@property (nonatomic, strong) NSString *userAddress;
@property (nonatomic, strong) NSString *userCountry;
@property (nonatomic, strong) NSString *userVerificationStatusText;


@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSString *phoneNo;
@property (nonatomic, strong) NSString *receiveNotification;
@property (nonatomic, strong) NSString *userDOB;
@property (nonatomic, strong) NSString *pushNotificationToken;
@property (nonatomic, strong) NSString *simOperatorName;
@property (nonatomic, strong) NSString *os;//manadatory
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *ipAddress;
@property (nonatomic, strong) NSString *profilePic;
@property (nonatomic, strong) NSString *profilePicExt;
@property (nonatomic, strong) NSString *registrationType;//manadatory
@property (nonatomic, strong) NSString *osVersion;//manadatory
@property (nonatomic, strong) NSString *appVersion;//manadatory
@property (nonatomic, strong) NSString *appBuild;

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *sessionId;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSArray *countryList;


@property (nonatomic) BOOL isLoggedIn;
@property (nonatomic) BOOL isHideOnboarding;

@property (nonatomic, strong) NSString *taxiDriverVerificationStatus;
@property (nonatomic, strong) NSString *taxiDriverVerificationStatusText;

#pragma mark -
#pragma mark - Initialize
- (id)init;

#pragma mark -
#pragma mark - Shared Info [Singleton Method]
+ (GlobalInfo *)sharedInfo;


#pragma mark -
#pragma mark - General Methods
- (void)setValuesFromLoginData:(NSDictionary*)dataDict;
- (void)setValuesFromEditProfileData:(NSDictionary*)dataDict;

+ (BOOL)checkStatusSuccess:(id)responseObject;
+ (void)showAlertTitle:(NSString*)title Message:(NSString*)message actions:(NSArray*)actionArray;
+ (NSString*)travelMode:(NSInteger)travelModeValue;
+ (NSString*)TypeOfPackage:(NSInteger)packType;
+ (UIImage*)AngelerImageForPickupType:(NSString*)code;
+ (void)hideViewNamed:(UIView*)vie withTopName:(NSString*)topName;
+ (NSAttributedString*)formatFinalStatusText:(NSString*)string andIndex:(NSInteger)index;
+ (NSDictionary*)getParmWithSession:(NSDictionary*)data;
+ (NSInteger)checkNotificationType:(NSString*)type;
+(void)showHomeScreen;
+(void)showTaxiDashboardScreen;
+(void)showDriverRequestListScreen;
+(void)showHomeScreen;
+(void)showLoginScreen;

@end
