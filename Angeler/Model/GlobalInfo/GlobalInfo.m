//
//  GlobalInfo.m
//  Angeler
//
//  Created by AJAY on 14/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "GlobalInfo.h"
#import "UIWindow+VisibleViewController.h"
#import <UICKeyChainStore/UICKeyChainStore.h>
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "DriverRequestListVC.h"
#import "TaxiDashboardVC.h"
//https://github.com/kishikawakatsumi/UICKeyChainStore   // Third party keychain store link url

@implementation GlobalInfo
@synthesize userName = _userName;
@synthesize userLastName = _userLastName;
@synthesize userFirstName = _userFirstName;
@synthesize userAddress = _userAddress;
@synthesize userCountry = _userCountry;
@synthesize userVerificationStatusText = _userVerificationStatusText;
@synthesize userEmail = _userEmail;
@synthesize userGender = _userGender;
@synthesize phoneNo = _phoneNo;
@synthesize userDOB = _userDOB;
@synthesize receiveNotification = _receiveNotification;
@synthesize socialId = _socialId;
@synthesize registrationType = _registrationType;
@synthesize userId = _userId;
@synthesize sessionId = _sessionId;
@synthesize isLoggedIn = _isLoggedIn;
@synthesize countryList = _countryList;
@synthesize isHideOnboarding = _isHideOnboarding;
@synthesize userAngelerStatus = _userAngelerStatus;
@synthesize userUniqueAngelerID = _userUniqueAngelerID;
@synthesize userSelectedCountryID = _userSelectedCountryID;
@synthesize taxiDriverVerificationStatus = _taxiDriverVerificationStatus;
@synthesize taxiDriverVerificationStatusText = _taxiDriverVerificationStatusText;


#pragma mark -
#pragma mark - Initialize
- (id)init {
    if (self == [super init]) {
        [self setDefaultValues];
    }
    return self;
}
#pragma mark -
#pragma mark - Shared Info [Singleton Method]
+ (GlobalInfo *)sharedInfo{
    static GlobalInfo *infoObj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        infoObj = [[GlobalInfo alloc] init];
    });
    return infoObj;
}
#pragma mark -
#pragma mark - Setter are override to update value in UserDefaaults
- (void)setUserName:(NSString *)userName{
    _userName = userName;
    UDSetValue(kUsername, userName);
}


- (void)setUserFirstName:(NSString *)userFirstName{
    _userFirstName = userFirstName;
    UDSetValue(kUserFirstName, userFirstName);
}

- (void)setUserLastName:(NSString *)userLastName{
    _userLastName = userLastName;
    UDSetValue(kUserLastName, userLastName);
}

- (void)setUserAddress:(NSString *)userAddress{
    _userAddress = userAddress;
    UDSetValue(kUserAddress, userAddress);
}

- (void)setUserCountry:(NSString *)userCountry{
    _userCountry = userCountry;
    UDSetValue(kUserCountry, userCountry);
}



- (void)setUserEmail:(NSString *)userEmail{
    _userEmail = userEmail;
    UDSetValue(kUserEmail, userEmail);
}

- (void)setUserAngelerID:(NSString *)userUniqueAngelerID{
    _userUniqueAngelerID = userUniqueAngelerID;
    UDSetValue(kuserUniqueAngelerID, userUniqueAngelerID);
}


-(void)setUserAngelerStatus:(NSString *)userAngelerStatus
{
    _userAngelerStatus = userAngelerStatus;
    UDSetValue(kuserStatus, userAngelerStatus);
}

- (void)setUserSelectedCountryID:(NSString *)userSelectedCountryID{
    _userSelectedCountryID = userSelectedCountryID;
    UDSetValue(kuserSelectedCountryID, _userSelectedCountryID);
}



- (void)setUserGender:(NSString *)userGender{
    _userGender = userGender;
    UDSetValue(kUserGender, userGender);
}
- (void)setPhoneNo:(NSString *)phoneNo{
    _phoneNo = phoneNo;
     UDSetValue(kUserPhoneNo, phoneNo);
}
- (void)setUserDOB:(NSString *)userDOB{
    _userDOB = userDOB;
    UDSetValue(kuserDob, userDOB);
}
- (void)setReceiveNotification:(NSString *)receiveNotification{
    _receiveNotification = receiveNotification;
    UDSetValue(kReceiveNotification, receiveNotification);
}
- (void)setSocialId:(NSString *)socialId{
    _socialId = socialId;
    UDSetValue(kUserSocialId, socialId);
}
- (void)setRegistrationType:(NSString *)registrationType{
    _registrationType = registrationType;
    UDSetValue(kRegistrationType, registrationType);
}
- (void)setUserId:(NSString *)userId{
    _userId = userId;
    UDSetValue(kUserId, userId);
}
- (void)setSessionId:(NSString *)sessionId{
    _sessionId = sessionId;
    UDSetValue(kSessionId, sessionId);
}
- (void)setIsLoggedIn:(BOOL)isLoggedIn{
    _isLoggedIn = isLoggedIn;
    UDSetBool(kUserLoggedInStatus, isLoggedIn);
}


- (void)setIsHideOnboarding:(BOOL)isHideOnboarding{
    _isHideOnboarding = isHideOnboarding;
    UDSetBool(kUserIsHideOnboard, isHideOnboarding);
}
- (void)setProfilePic:(NSString *)profilePic{
    _profilePic = profilePic;
    UDSetValue(kProfilePicUrl, profilePic);
}

- (void)setCountryList:(NSArray *)countryList{
    _countryList = countryList;
}
-(void)setUserVerificationStatusText:(NSString *)userVerificationStatusText
{
    _userVerificationStatusText = userVerificationStatusText;
    UDSetValue(kUserVerificationStatusText, userVerificationStatusText);
}

-(void)setTaxiDriverVerificationStatusText:(NSString *)taxiDriverVerificationStatusText
{
    _taxiDriverVerificationStatusText = taxiDriverVerificationStatusText;
    UDSetValue(kDriverVerificationStatusText, taxiDriverVerificationStatusText);
}

-(void)setTaxiDriverVerificationStatus:(NSString *)taxiDriverVerificationStatus
{
    _taxiDriverVerificationStatus = taxiDriverVerificationStatus;
    UDSetValue(kDriverVerificationStatus, taxiDriverVerificationStatus);
}
- (void)setDefaultValues{
    self.userName = UDValue(kUsername)?UDValue(kUsername):Empty;
    self.userFirstName = UDValue(kUserFirstName)?UDValue(kUserFirstName):Empty;
    self.userLastName = UDValue(kUserLastName)?UDValue(kUserLastName):Empty;
    self.userCountry = UDValue(kUserCountry)?UDValue(kUserCountry):Empty;
    self.userAddress = UDValue(kUserAddress)?UDValue(kUserAddress):Empty;
    self.userVerificationStatusText = UDValue(kUserVerificationStatusText)?UDValue(kUserVerificationStatusText):Empty;
    self.userEmail = UDValue(kUserEmail)?UDValue(kUserEmail):Empty;
    self.userGender = UDValue(kUserGender)?UDValue(kUserGender):Empty;
    self.phoneNo = UDValue(kUserPhoneNo)?UDValue(kUserPhoneNo):Empty;
    self.userDOB = UDValue(kuserDob)?UDValue(kuserDob):Empty;
    self.receiveNotification = UDValue(kReceiveNotification)?UDValue(kReceiveNotification):Empty;
    self.userId = UDValue(kUserId)?UDValue(kUserId):Empty;
    self.sessionId = UDValue(kSessionId)?UDValue(kSessionId):Empty;
    
    self.socialId = UDValue(kUserSocialId)?UDValue(kUserSocialId):Empty;
    self.registrationType = UDValue(kRegistrationType)?UDValue(kRegistrationType):Empty;
    self.profilePic = UDValue(kProfilePicUrl)?UDValue(kProfilePicUrl):Empty;
    self.userUniqueAngelerID = UDValue(kuserUniqueAngelerID)?UDValue(kuserUniqueAngelerID):Empty;
     self.userAngelerStatus = UDValue(kuserStatus)?UDValue(kuserStatus):Empty;
    self.userSelectedCountryID = UDValue(kuserSelectedCountryID)?UDValue(kuserSelectedCountryID):Empty;
    
    self.osVersion = [[UIDevice currentDevice] systemVersion];
    self.os = @"IOS";
    self.appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    self.appBuild = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    self.deviceId = [self getDeviceIdFromKeyChain];
    self.taxiDriverVerificationStatusText = UDValue(kDriverVerificationStatusText)?UDValue(kDriverVerificationStatusText):Empty;
    self.taxiDriverVerificationStatus = UDValue(kDriverVerificationStatus)?UDValue(kDriverVerificationStatus):Empty;
    
    
    NSLog(@"device id ====   %@",self.deviceId);
    
    //Non mandatory fields
   // self.phoneNo = Empty;
    self.pushNotificationToken = UDValue(kDeviceToken)?UDValue(kDeviceToken):Empty;
    self.simOperatorName = Empty;
    self.country = Empty;
    self.city = Empty;
    self.ipAddress = Empty;
    self.profilePicExt = Empty;
    self.latitude = Empty;
    self.longitude = Empty;
    
    
    self.isLoggedIn = UDBool(kUserLoggedInStatus);
    self.isHideOnboarding = UDBool(kUserIsHideOnboard);
    [self getCountryList];
}

- (NSString*)getDeviceIdFromKeyChain{
    UICKeyChainStore *keychainStore = [UICKeyChainStore keyChainStore];
    if (keychainStore[@"angleQueue"]) {
        return keychainStore[@"angleQueue"];
    }else{
        NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        Identifier = [NSString stringWithFormat:@"%@AngelerCom",Identifier];
        keychainStore[@"angleQueue"] = Identifier;
        return keychainStore[@"angleQueue"];
    }
}
- (void)getCountryList{
    
   
    //NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kCountryList
                                                    Method:REQMETHOD_GET
                                                parameters:@{@"caller":@"default"}
                                                     media:nil
                                            viewController:nil
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       if (statusCode == 200) {
                                                           _countryList = [responseObject objectForKey:@"responseData"];
                                                       }
                                                      else if (statusCode == 600) {
                                                           UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                               [GlobalInfo showLoginScreen];
                                                           }];
                                                          NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [GlobalInfo showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       NSLog(@"failure response ===   %@",responseObject);
                                                       //Web request call failed//Web service failed to retrieve data
                                                   }];
}

- (void)setValuesFromLoginData:(NSDictionary *)dataDict{
    
    NSString *uniqueAngelerId = [dataDict objectForKey:@"angelerUniqueId"];
    if (uniqueAngelerId == nil || [uniqueAngelerId isKindOfClass:[NSNull class]]) {
       [self setUserAngelerID:@""];
    }
    else
    {
        [self setUserAngelerID:uniqueAngelerId];
    }
    
    //FOR OLD USER THIS FIELD WAS NOT EXIST
    NSString *lastName = [dataDict objectForKey:@"lastName"];
    if (lastName == nil || [lastName isKindOfClass:[NSNull class]]) {
        [self setUserLastName:@""];
    }
    else
    {
        [self setUserLastName:lastName];
    }
    
    [self setSessionId:[dataDict objectForKey:@"sessionId"]];
    [self setProfilePic:[dataDict objectForKey:@"profilePic"]];
    [self setUserEmail:[dataDict objectForKey:@"userEmail"]];
    [self setUserId:[dataDict objectForKey:@"userId"]];
    [self setUserName:[dataDict objectForKey:@"userName"]];
    [self setUserFirstName:[dataDict objectForKey:@"firstName"]];
    [self setUserAddress:[dataDict objectForKey:@"userAddress"]];
    [self setUserCountry:[dataDict objectForKey:@"userCountry"]];
    [self setUserVerificationStatusText:[dataDict objectForKey:@"verifiedStatusText"]];
    [self setUserAngelerStatus:[dataDict objectForKey:@"angelerStatus"]];
    [self setUserGender:[dataDict objectForKey:@"userGender"]];
    [self setPhoneNo:[dataDict objectForKey:@"userPhone"]];
    [self setUserDOB:[dataDict objectForKey:@"userDOB"]];
    [self setReceiveNotification:[dataDict objectForKey:@"receivePushNotifications"]];
    [self setUserSelectedCountryID:[dataDict objectForKey:@"userSelectedCountryId"]];
}


- (void)setValuesFromEditProfileData:(NSDictionary*)dataDict
{
    //FOR OLD USER THIS FIELD WAS NOT EXIST
    NSString *lastName = [dataDict objectForKey:@"lastName"];
    if (lastName == nil || [lastName isKindOfClass:[NSNull class]]) {
        [self setUserLastName:@""];
    }
    else
    {
        [self setUserLastName:lastName];
    }
    [self setUserAngelerStatus:[dataDict objectForKey:@"angelerStatus"]];
    [self setProfilePic:[dataDict objectForKey:@"profilePic"]];
    [self setUserEmail:[dataDict objectForKey:@"userEmail"]];
    [self setUserName:[dataDict objectForKey:@"userName"]];
    [self setUserFirstName:[dataDict objectForKey:@"firstName"]];
    [self setUserAddress:[dataDict objectForKey:@"userAddress"]];
    [self setUserCountry:[dataDict objectForKey:@"userCountry"]];
    [self setUserGender:[dataDict objectForKey:@"userGender"]];
    [self setPhoneNo:[dataDict objectForKey:@"userPhone"]];
    [self setUserDOB:[dataDict objectForKey:@"userDOB"]];
    [self setReceiveNotification:[dataDict objectForKey:@"receivePushNotifications"]];
    [self setUserSelectedCountryID:[dataDict objectForKey:@"userSelectedCountryId"]];
}
#pragma mark -
#pragma mark - Getter to fetch value from UserDefaaults
+ (NSString*)travelMode:(NSInteger)travelModeValue{
    NSString *trvlMode = @"All";
    switch (travelModeValue) {
        case 1:
            trvlMode = @"Flight";
            break;
        case 2:
            trvlMode = @"Ship";
            break;

        case 3:
            trvlMode = @"Car";
            break;

        case 4:
            trvlMode = @"Train";
            break;            
        default:
            break;
    }
    
    return trvlMode;
}

+ (NSString*)TypeOfPackage:(NSInteger)packType{
    NSString *packgType = Empty;
    
    switch (packType) {
        case 0:
            packgType = NSLocalizedString(@"Item_Category", nil) ;
            break;
        case 1:
            packgType = NSLocalizedString(@"Document", nil) ;
            break;
        case 2:
            packgType = NSLocalizedString(@"Parcel", nil);
            break;
        case 3:
            packgType = NSLocalizedString(@"Both", nil);
            break;
        default:
            break;
    }
    
    return packgType;
}

+ (UIImage*)AngelerImageForPickupType:(NSString*)code{
    UIImage *img;
    if ([code isEqualToString:@"PMP"] || [code isEqualToString:@"DMP"]) {
        img = [UIImage imageNamed:@"choose_angeler_meeting"];
    }
    if ([code isEqualToString:@"PP"] || [code isEqualToString:@"DP"]) {
        img = [UIImage imageNamed:@"choose_angeler_premium"];
    }
    if ([code isEqualToString:@"PPP"]) {
        img = [UIImage imageNamed:@"choose_angeler_pickup"];
    }
    if ([code isEqualToString:@"DDP"]) {
        img = [UIImage imageNamed:@"choose_angeler_delivery"];
    }
    
    return img;
}
+ (void)hideViewNamed:(UIView*)vie withTopName:(NSString*)topName{
    if (![topName isEqualToString:Empty]) {
        for (NSLayoutConstraint *cons in vie.superview.constraints) {
            if ([cons.identifier isEqualToString:topName]) {
                cons.constant = 0;
            }
        }
    }
    [vie addConstraint:[NSLayoutConstraint constraintWithItem:vie attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:0]];
    vie.hidden = YES;
}

+ (NSAttributedString*)formatFinalStatusText:(NSString*)string andIndex:(NSInteger)index{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Regular" size:13.0] range:NSMakeRange(0, index)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:13.0] range:NSMakeRange(index, str.length-index)];
    [str addAttribute:NSForegroundColorAttributeName value:[Helper colorWithHexString:@"#68C1D4"] range:NSMakeRange(index, str.length-index)];
    return str;
}
+ (BOOL)checkStatusSuccess:(id)responseObject{
    NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
    if (statusCode == 200) return YES;
    
    NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
    if (statusCode == 600) {
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self showLoginScreen];
        }];
        [self showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
    }else if(statusCode == 601 || statusCode == 245)
    {
        //No record found label show => 601
        //Credit not sufficient => 245
        return YES;
    }
    else
    {
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [self showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
    }
    return NO;
}

+(void)showLoginScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LoginViewController *logInVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVCID"];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:logInVC];
}
+(void)showHomeScreen{
   
    HomeViewController *homeVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeVC];
}

+(void)showTaxiDashboardScreen{
    TaxiDashboardVC *Obj = [[TaxiDashboardVC alloc] initWithNibName:@"TaxiDashboardVC" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:Obj];
}
+(void)showDriverRequestListScreen{
    DriverRequestListVC *driverListVC = [[DriverRequestListVC alloc] initWithNibName:@"DriverRequestListVC" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:driverListVC];
}

+ (void)showAlertTitle:(NSString*)title Message:(NSString*)message actions:(NSArray*)actionArray{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    for (UIAlertAction *action in actionArray) {
        [alert addAction:action];
    }
    
   UIViewController *currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    //[[AppDel window] visibleViewController]
    [currentVc presentViewController:alert animated:YES completion:^{
        
    }];

}

+ (NSDictionary*)getParmWithSession:(NSDictionary *)data{
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:data];
    
    [tempDict setValue:[[self sharedInfo] userId] forKey:@"userId"];
    [tempDict setValue:[[self sharedInfo] sessionId] forKey:@"sessionId"];
    [tempDict setValue:[[self sharedInfo] os] forKey:@"os"];
    [tempDict setValue:[[self sharedInfo] osVersion] forKey:@"osVersion"];
    [tempDict setValue:[[self sharedInfo] appVersion] forKey:@"appVersion"];
    [tempDict setValue:[Helper getLocalData:kUserDeviceCurrentLanguage] forKey:@"lang"];
   
    return tempDict;
}
+ (NSInteger)checkNotificationType:(NSString*)type{
    if ([type isEqualToString:kSenderAngelerReceiver] ||
        [type isEqualToString:kAngelerReceiverReceiver] ||
        [type isEqualToString:kPPAngelerReceiver] ||
        [type isEqualToString:kDPReceiverReceiver] ||
        [type isEqualToString:kAngelerDPReceiver]) {
        return NOTIF_TYPE_RECEIVER;
    }
    else if ([type isEqualToString:kSenderAngelerSender] ||
             [type isEqualToString:kAngelerReceiverSender] ||
             [type isEqualToString:kSenderPPSender] ||
             [type isEqualToString:kPPAngelerSender] ||
             [type isEqualToString:kAngelerDPSender] ||
             [type isEqualToString:kDPReceiverSender]){
        return NOTIF_TYPE_SENDER;
        
    }
    else if ([type isEqualToString:kBookingRequest] ||
             [type isEqualToString:kSenderPPAngeler] ||
             [type isEqualToString:kPPAngelerAngeler] ||
             [type isEqualToString:kAngelerDPAngeler] ||
             [type isEqualToString:kDPReceiverAngeler]){
        return NOTIF_TYPE_ANGELER;
        
    }else if ([type isEqualToString:kPPAngelerDP] ||
              [type isEqualToString:kBookingResponsePP] ||
              [type isEqualToString:kSenderAngelerDP]){
        return NOTIF_TYPE_DELIVRY_PACKAGE;
    }
    else{
        return NOTIF_TYPE_NONE;
    }
}
//- (NSString*)userName{
//    _userName = UDValue(kUsername)?UDValue(kUsername):Empty;
//    return _userName;
//}

@end
