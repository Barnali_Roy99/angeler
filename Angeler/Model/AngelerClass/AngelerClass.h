//
//  AngelerClass.h
//  Angeler
//
//  Created by AJAY on 06/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AngelerClass : NSObject
//@property (nonatomic, strong) AddressClass *from_Address;
@property (nonatomic) BOOL from_Meet_Point_Checked;
@property (nonatomic) BOOL from_Pick_Point_Checked;
@property (nonatomic) BOOL from_Premium_Checked;
@property (nonatomic, strong) NSMutableArray *From_Point_Address_Array;

//@property (nonatomic, strong) AddressClass *to_Address;
@property (nonatomic) BOOL to_Meet_Point_Checked;
@property (nonatomic) BOOL to_Pick_Point_Checked;
@property (nonatomic) BOOL to_Premium_Checked;
@property (nonatomic, strong) NSMutableArray *to_Point_Address_Array;


@end
