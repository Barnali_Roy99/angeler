//
//  PackageClass.m
//  Angeler
//
//  Created by AJAY on 31/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "PackageClass.h"

@implementation PackageClass
@synthesize from_Address;
//@synthesize from_Country_City;
@synthesize to_Address;
//@synthesize to_Country_City;

@synthesize arrItemImages;
@synthesize travel_Mode;

@synthesize pack_Id;
@synthesize pack_Type;
@synthesize pack_Height;
@synthesize pack_Width;
@synthesize pack_Length;
@synthesize pack_Weight;
@synthesize pack_Name;
@synthesize pack_Email;
@synthesize pack_Country_Code;
@synthesize pack_Contact_NO;
@synthesize pack_Delivery_Date_Time;
@synthesize pack_Remarks;
@synthesize pack_Request_count;
@synthesize pack_Status_Text;
@synthesize meetingPointPrice,premiumPrice;

- (id)init{
    if (self = [super init]) {
        
        from_Address = [[AddressClass alloc] init];
        to_Address = [[AddressClass alloc] init];
        arrItemImages = [[NSMutableArray alloc] init];
        travel_Mode = TRAVEL_MODE_ALL;
        
        pack_Id = Empty;
        pack_Type = zeroUnit;
        pack_Height = Empty;
        pack_Width = Empty;
        pack_Length = Empty;
        pack_Weight = Empty;
        pack_Name = Empty;
        pack_Email = Empty;
        pack_Country_Code = Empty;
        pack_Contact_NO = Empty;
        pack_Delivery_Date_Time = Empty;
        pack_Remarks = Empty;
        pack_Status_Text = Empty;
        pack_Request_count = Empty;
        meetingPointPrice = Empty;
        premiumPrice = Empty;
    }
    return self;
}


@end
