//
//  PackageClass.h
//  Angeler
//
//  Created by AJAY on 31/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackageClass : NSObject
@property (nonatomic, strong) AddressClass *from_Address;
@property (nonatomic, strong) AddressClass *to_Address;
@property (nonatomic, strong) NSString *pack_Type;
@property (nonatomic, strong) NSString *pack_Id;

@property (nonatomic, strong) NSString *pack_Height;
@property (nonatomic, strong) NSString *pack_Width;
@property (nonatomic, strong) NSString *pack_Length;
@property (nonatomic, strong) NSString *pack_Weight;
@property (nonatomic, strong) NSMutableArray *arrItemImages;

@property (nonatomic) MODE_OF_TRAVEL travel_Mode;
@property (nonatomic, strong) NSString *pack_Name;
@property (nonatomic, strong) NSString *pack_Email;
@property (nonatomic, strong) NSString *recipient_Angeler_ID;
@property (nonatomic, strong) NSString *pack_Country_Code;
@property (nonatomic, strong) NSString *pack_Contact_NO;
@property (nonatomic, strong) NSString *pack_Delivery_Date_Time;
@property (nonatomic, strong) NSString *pack_Remarks;
@property (nonatomic, strong) NSString *pack_Request_count;
@property (nonatomic, strong) NSString *pack_Status_Text;
@property (nonatomic, strong) NSString *meetingPointPrice;
@property (nonatomic, strong) NSString *premiumPrice;



@end
