//
//  ImageObjectClass.m
//  Angeler
//
//  Created by AJAY on 31/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ImageObjectClass.h"

@implementation ImageObjectClass
@synthesize imgData,mimeType,mediaType,keyName;
- (id)init{
    if (self = [super init]) {
        imgData = [[NSData alloc] init];
        mimeType = Empty;
        mediaType = Empty;
        keyName = Empty;
    }
    return self;
}
@end
