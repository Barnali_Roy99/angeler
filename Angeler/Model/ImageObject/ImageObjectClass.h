//
//  ImageObjectClass.h
//  Angeler
//
//  Created by AJAY on 31/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageObjectClass : NSObject
@property (nonatomic, strong) NSData *imgData;
@property (nonatomic, strong) NSString *mediaType;
@property (nonatomic, strong) NSString *keyName;
@property (nonatomic, strong) NSString *mimeType;


@end
