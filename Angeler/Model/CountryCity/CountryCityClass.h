//
//  CountryCityClass.h
//  Angeler
//
//  Created by AJAY on 16/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryCityClass : NSObject
@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSString *countryId;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *countryIsdCode;
@property (nonatomic, strong) NSString *cityId;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *cityCode;

@end
