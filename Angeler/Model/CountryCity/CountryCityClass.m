//
//  CountryCityClass.m
//  Angeler
//
//  Created by AJAY on 16/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "CountryCityClass.h"

@implementation CountryCityClass
@synthesize cityCode,cityId,cityName,countryCode,countryId,countryIsdCode,countryName;
- (id)init{
    if (self = [super init]) {
        cityCode = Empty;
        cityId = Empty;
        cityName = Empty;
        countryCode = Empty;
        countryId = Empty;
        countryIsdCode = Empty;
        countryName = Empty;
    }
    return self;
}
@end
