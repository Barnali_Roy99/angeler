//
//  PickUpDropOffAddressModel.h
//  Angeler
//
//  Created by AJAY on 31/12/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PickUpDropOffAddressModel : NSObject

@property (nonatomic, strong) NSNumber *pickUpLatitude;
@property (nonatomic, strong) NSNumber *pickUpLongitude;
@property (nonatomic, strong) NSNumber *dropOffLatitude;
@property (nonatomic, strong) NSNumber *dropOffLongitude;

@property (nonatomic, strong) NSString *pickUpAddress;
@property (nonatomic, strong) NSString *dropOffAddress;

@end

NS_ASSUME_NONNULL_END
