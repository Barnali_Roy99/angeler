//
//  PickUpDropOffAddressModel.m
//  Angeler
//
//  Created by AJAY on 31/12/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "PickUpDropOffAddressModel.h"

@implementation PickUpDropOffAddressModel
@synthesize pickUpAddress,dropOffAddress,pickUpLatitude,dropOffLatitude,pickUpLongitude,dropOffLongitude;

-(id)init
{
    if (self = [super init]) {
        pickUpAddress = Empty;
        dropOffAddress = Empty;
        pickUpLatitude = [NSNumber numberWithFloat:0.0];
        pickUpLongitude = [NSNumber numberWithFloat:0.0];
        dropOffLatitude = [NSNumber numberWithFloat:0.0];
        dropOffLongitude = [NSNumber numberWithFloat:0.0];
    }
    return self;
}
@end
