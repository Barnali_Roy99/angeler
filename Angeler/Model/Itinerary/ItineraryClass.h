//
//  ItineraryClass.h
//  Angeler
//
//  Created by AJAY on 10/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ItineraryClass : NSObject

@property (nonatomic, strong) NSString *Departing_Date_Time;
@property (nonatomic, strong) NSString *Arrival_Date_Time;
@property (nonatomic) MODE_OF_TRAVEL travel_Mode;
@property (nonatomic, strong) NSString *Travel_Ref_No;

@property (nonatomic, strong) NSString *Size_Height_Min_Max;
@property (nonatomic, strong) NSString *Size_Width_Min_Max;
@property (nonatomic, strong) NSString *Size_Length_Min_Max;
@property (nonatomic, strong) NSString *Weight;
@property (nonatomic, strong) NSString *Maximum_Unit;
@property (nonatomic, strong) NSString *PackageType;

@property (nonatomic) BOOL Pick_Meet_Point_Checked;
@property (nonatomic, strong) AddressClass *from_Address;
@property (nonatomic) BOOL Pick_Pickup_Point_Checked;
@property (nonatomic, strong) NSMutableArray *Pick_Pickup_Point_Address_Array;
@property (nonatomic) BOOL Pick_Premium_Service_Checked;

@property (nonatomic) BOOL Delivery_Meet_Point_Checked;
@property (nonatomic, strong) AddressClass *to_Address;
@property (nonatomic) BOOL Delivery_Pickup_Point_Checked;
@property (nonatomic, strong) NSMutableArray *Delivery_Pickup_Point_Address_Array;
@property (nonatomic) BOOL Delivery_Premium_Service_Checked;

@end
