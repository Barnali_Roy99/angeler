//
//  ItineraryClass.m
//  Angeler
//
//  Created by AJAY on 10/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ItineraryClass.h"

@implementation ItineraryClass
@synthesize Arrival_Date_Time,
            to_Address,
            Delivery_Meet_Point_Checked,
            Delivery_Pickup_Point_Address_Array,
            Delivery_Pickup_Point_Checked,
            Delivery_Premium_Service_Checked,
            Departing_Date_Time,
            Maximum_Unit,
            PackageType,
            from_Address,
            Pick_Meet_Point_Checked,
            Pick_Pickup_Point_Address_Array,
            Pick_Pickup_Point_Checked,
            Pick_Premium_Service_Checked,
            Size_Height_Min_Max,
            Size_Length_Min_Max,
            Size_Width_Min_Max,
            travel_Mode,
            Travel_Ref_No,Weight;



- (id)init{
    if (self = [super init]) {
        Arrival_Date_Time = Empty;
        to_Address = [[AddressClass alloc] init];
        Delivery_Meet_Point_Checked = NO;
        Delivery_Pickup_Point_Address_Array = [[NSMutableArray alloc] init];
        Delivery_Pickup_Point_Checked = NO;
        Delivery_Premium_Service_Checked = NO;
        //Depart_Country_City = [[CountryCityClass alloc] init];
        //Dest_Country_City = [[CountryCityClass alloc] init];
//        Departing_City = Empty;
//        Departing_Country = Empty;
        Departing_Date_Time = Empty;
//        Destination_City = Empty;
//        Destination_Country = Empty;
        Maximum_Unit = OneUnit;
        PackageType = zeroUnit;
        from_Address = [[AddressClass alloc] init];
        Pick_Meet_Point_Checked = NO;
        Pick_Pickup_Point_Address_Array = [[NSMutableArray alloc] init];
        Pick_Pickup_Point_Checked = NO;
        Pick_Premium_Service_Checked = NO;
        Size_Height_Min_Max = [NSString stringWithFormat:@"%@,%@",OneUnit,hundredUnit];
        Size_Length_Min_Max = [NSString stringWithFormat:@"%@,%@",OneUnit,hundredUnit];
        Size_Width_Min_Max = [NSString stringWithFormat:@"%@,%@",OneUnit,hundredUnit];
        travel_Mode = TRAVEL_MODE_ALL;
        Travel_Ref_No = Empty;
        Weight = OneUnit;
    }
    return self;
}
@end
