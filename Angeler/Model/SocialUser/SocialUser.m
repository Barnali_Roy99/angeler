//
//  SocialUser.m
//  SpotDeal
//
//  Created by AJAY on 07/10/16.
//  Copyright © 2016 Matrixnmedia. All rights reserved.
//

#import "SocialUser.h"

@implementation SocialUser
@synthesize userId,userName,userEmail,userGender,userProfilePicUrl,userMobile,userType;
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.userId = Empty;
        self.userName = Empty;
        self.userEmail = Empty;
        self.userGender = Empty;
        self.userMobile = Empty;
        self.userProfilePicUrl = Empty;
        self.userType = Empty;
    }
    return self;
}

@end
