//
//  SocialUser.h
//  SpotDeal
//
//  Created by AJAY on 07/10/16.
//  Copyright © 2016 Matrixnmedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocialUser : NSObject
@property(nonatomic,strong) NSString *userName;
@property(nonatomic,strong) NSString *userEmail;
@property(nonatomic,strong) NSString *userId;
@property(nonatomic,strong) NSString *userGender;
@property(nonatomic,strong) NSString *userProfilePicUrl;
@property(nonatomic,strong) NSString *userMobile;
@property(nonatomic,strong) NSString *userType;

@end
