//
//  AppDelegate.m
//  Angeler
//
//  Created by AJAY on 20/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "AppDelegate.h"



#import <UICKeyChainStore/UICKeyChainStore.h> 
#import "SideMenuViewController.h"
#import "HomeViewController.h"


#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TWMessageBarManager/TWMessageBarManager.h>
#import "ItineraryPackageInfoController.h"
#import "ItineraryPickUpController.h"
#import "ItineraryDropOffController.h"
#import "TrakingInfoController.h"
#import "PackageRequestInfoController.h"
#import "DeliveryDetailController.h"
#import "ManageDeliveryPointViewController.h"
#import "DeliveryPackageDetailController.h"

#import "UIWindow+VisibleViewController.h"
#import "LoginViewController.h"
#import "WalletViewController.h"
#import "ProfileController.h"
#import "MyTripsListViewController.h"
#import "ItineraryPackageInfoController.h"
#import "DriverRequestDetailsVC.h"
#import "CustomerTrackTrip.h"
#import "TaxiDashboardVC.h"
#import "DriverTripStartVC.h"
#import "CharityDetailsVC.h"
#import "MyShipmentsListController.h"
#import "TrackShoppingVC.h"
#import "BuyerShoppingListVC.h"
#import "LocalDeliveryRequestListVC.h"
#import "SenderRequestDetailsVC.h"
#import "SenderRequestListVC.h"
#import "ChatViewController.h"


@import GoogleMaps;
@import GooglePlaces;
@import UIKit;
@import Firebase;

@interface AppDelegate ()

@end


/*
 
 Upload DSYM File in case of File required in case of Firebase Crashlytics
 
 Ajays-Mac-Mini:Pods AJAY$ Fabric/upload-symbols  -a 48df1d519bcfc17ddf43ced882acee8ba12ef6b3 -p ios /Users/AJAY/Library/Developer/Xcode/DerivedData/Angeler-duydidrijkuwcofujthemkgffxoh/Build/Products/Debug-iphoneos/Angeler.app.dSYM
 Successfully submitted symbols for architecture arm64 with UUID 2eb9022e30ad385bab3c0039532e8750 in dSYM: /Users/AJAY/Library/Developer/Xcode/DerivedData/Angeler-duydidrijkuwcofujthemkgffxoh/Build/Products/Debug-iphoneos/Angeler.app.dSYM
 
 Generic command
 
 /path/to/pods/directory/Fabric/upload-symbols upload-symbols -a <api-key> -p
 <platform> /path/to/dSYMs
 
 */

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Fabric with:@[[Crashlytics class]]];
    [self setDeviceLanguage];
    
   
    
    //[GIDSignIn sharedInstance].delegate = self;
    //*******End Of Google******//
    
    //AIzaSyB9WlBGBm4vNXKdPrz-IvE_vDUUASOPKx4
    //---------------Google Map Service-------------------------
    
    [GMSServices provideAPIKey:@"AIzaSyD6Op183qFWyBJOENq9wQ5h7ocX4XfEiCA"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyD6Op183qFWyBJOENq9wQ5h7ocX4XfEiCA"];

    
    [FIRApp configure];
    
    
    
    self.shareModel = [LocationManager sharedManager];
    [self.shareModel startMonitoringLocation];
    
    
    
    //==============Set Push Notification=============================
    
    
    // Let the device know we want to receive push notifications
    //    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
//    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]){
//        // iOS 8 Notifications
//        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//
//        [application registerForRemoteNotifications];
//    }
    
    
    
    [self setPushNotification];
    
       //==============Set Push Notification End=============================
    
    
   // [[GlobalInfo sharedInfo] setUserName:@"ajay"];
    NSLog(@"devId====  %@  \n %@   loged in %@",[[GlobalInfo sharedInfo] deviceId],[[GlobalInfo sharedInfo] userName],[[GlobalInfo sharedInfo] isLoggedIn]?@"YES":@"NO");
   
    if ([[GlobalInfo sharedInfo] isLoggedIn]) {
        [self showHomeScreen];
        [self logUser];
//        if([Helper isAngelerStatusVerified])
//        {
//
//
//            [NSTimer scheduledTimerWithTimeInterval:15.0f target:self selector:@selector(methodToRepeatCurrentLocationTrack) userInfo:nil repeats:YES];

    //    }
    }
    else  if ([[GlobalInfo sharedInfo] isHideOnboarding])
    {
       [self showLoginScreen];
       
    }
    
    [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(methodToRepeatCurrentLocationTrack) userInfo:nil repeats:YES];

    

    //If application is inactive and receive remote notification and user click on alert button to launch application, you can have this event by adding the below code in “didFinishLaunchingWithOptions” method: - See more at: http://blog.ifuturz.com/php/apns-apple-push-notification-service-embed-with-application.html?goback=%2Egde_3450236_member_262145694#sthash.Tb20UVqE.dpuf
    
    NSDictionary *pushNotificationPayload = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(pushNotificationPayload) {
        [self application:application didReceiveRemoteNotification:pushNotificationPayload];
    }
    
   
    
   // [self setStatusBarBackgroundColor:[UIColor appThemeBlueColor]];
    //Background Code
// [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    return YES;
}

- (void)methodToRepeatCurrentLocationTrack
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue,^{
        if ([[GlobalInfo sharedInfo] isLoggedIn]) {
        if([Helper isAngelerStatusVerified] && [Helper isDriverStatusVerified])
           [self callSocketServiceUpdateLocationDriver];
        }
    });
    

}

-(void)setDeviceLanguage
{
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    if([language isEqualToString:@"en"]){
        
        [Helper setLocalData:@"english" Key:kUserDeviceCurrentLanguage];
    }else{
        [Helper setLocalData:@"french" Key:kUserDeviceCurrentLanguage];
    }
    
}


-(void)setPushNotification
{
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) ) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    } else {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if( !error ) {
                // required to get the app to do anything at all about push notifications
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
                NSLog( @"Push registration success." );
            } else {
                NSLog( @"Push registration FAILED" );
                NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
               
            }
        }];
    }
    
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString* deviceTokenString = [[[[deviceToken description]
                                     stringByReplacingOccurrencesOfString: @"<" withString: Empty]
                                    stringByReplacingOccurrencesOfString: @">" withString: Empty]
                                   stringByReplacingOccurrencesOfString: @" " withString: Empty] ;
    NSLog(@"Device_Token: %@\n",deviceTokenString);
    
    UDSetValue(kDeviceToken, deviceTokenString);
    [[GlobalInfo sharedInfo] setPushNotificationToken:deviceTokenString];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    [Helper showAlertWithTitle:@"Push failed to register" Message:error.localizedDescription];
    // failed to register push
    NSLog(@"Push failed to register with error: %@", error);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"userInfo didReceiveRemoteNotification: %@",userInfo);
    NSLog(@"applicationState: %ld",(long)[application applicationState]);
    
    [self CallPageForAutoRedirectionOnGetNotificaion:userInfo];
    
    
//    if ([application applicationState] == UIApplicationStateActive){
//        NSString *notificationType = [userInfo objectForKey:@"type"];
//
//        [[TWMessageBarManager sharedInstance] showMessageWithTitle:[[userInfo objectForKey:@"aps"]  objectForKey:@"alert"] description:Empty type:TWMessageBarMessageTypeSuccess
//        callback:^{
//            [self CallPageForNotificaion:userInfo];
//            NSLog(@"Message bar tapped!");
//        }];
//        [self postLocalNotificationForType:notificationType];
//
//    }
//    else {
//
//        NSLog(@"Push Notification Info dic %@",userInfo);
      //  [self CallPageForNotificaion:userInfo];
  //  }
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    NSLog(@"userInfo willPresentNotification %@", notification.request.content.userInfo);
    // custom code to handle push while app is in the foreground
//    [self CallPageForNotificaion:notification.request.content.userInfo];
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound);
    
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler
{
     NSLog(@"userInfo didReceiveNotificationResponse withCompletionHandler %@", response.notification.request.content.userInfo);
    [self CallPageForNotificaion:response.notification.request.content.userInfo];
}


- (void)CallPageForNotificaion:(NSDictionary*)info{
    if ([[GlobalInfo sharedInfo] isLoggedIn]) {
        NSString *notificationType = [info objectForKey:@"type"];
        
        if ([notificationType isEqualToString:kBookingResponse]) {
            if ([[info objectForKey:@"status"] isEqualToString:@"Accepted"]) {
                
                TrakingInfoController *next = [[TrakingInfoController alloc] initWithNibName:@"TrakingInfoController" bundle:nil];
                next.bookingId = [info objectForKey:@"bookingId"];
                [[self getNotificationNavController] pushViewController:next animated:YES];
            }else{
                
                PackageRequestInfoController *next = [[PackageRequestInfoController alloc] initWithNibName:@"PackageRequestInfoController" bundle:nil];
                next.packageId = [info objectForKey:@"bookingId"];
                UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:next];
                AppDel.window.rootViewController = navCon;
            }
        }
 
       else if ([GlobalInfo checkNotificationType:notificationType] == NOTIF_TYPE_ANGELER) {
            [self callServiceToCheckStatus:info];
        }
        
       else if ([GlobalInfo checkNotificationType:notificationType] == NOTIF_TYPE_SENDER) {
            TrakingInfoController *next = [[TrakingInfoController alloc] initWithNibName:@"TrakingInfoController" bundle:nil];
            next.bookingId = [info objectForKey:@"bookingId"];
            [[self getNotificationNavController] pushViewController:next animated:YES];
        }
        
      else if ([GlobalInfo checkNotificationType:notificationType] == NOTIF_TYPE_RECEIVER) {
            DeliveryDetailController *next = [[DeliveryDetailController alloc] initWithNibName:@"DeliveryDetailController" bundle:nil];
            next.packageId = [info objectForKey:@"packageId"];
            [[self getNotificationNavController] pushViewController:next animated:YES];
        }
        
      else if ([notificationType isEqualToString:kNAngelerSahreCreditToReceiver] || [notificationType isEqualToString:kNAngelerWithdrawCredit] || [notificationType isEqualToString:kNAngelerAddCredit] || [notificationType isEqualToString:kNAngelerWalletCreditCommisionAfterDelivery] || [notificationType isEqualToString:KNAngelerBankDetailsVerified] || [notificationType isEqualToString:KNAngelerANCRefunded] || [notificationType isEqualToString:KNAngelerShoppingRequestDutyFreeEarning]) {
            
            WalletViewController *next = [[WalletViewController alloc] initWithNibName:@"WalletViewController" bundle:nil];
            [[self getNotificationNavController] pushViewController:next animated:YES];
        }
      else if ([notificationType isEqualToString:KNAngelerShipmentExpired]){
          MyShipmentsListController *next = [[MyShipmentsListController alloc] initWithNibName:@"MyShipmentsListController" bundle:nil];
          [[self getNotificationNavController] pushViewController:next animated:YES];
      }
        else if ([notificationType isEqualToString:kNAngelerAccountVerified] || [notificationType isEqualToString:KNTaxiDriverVerified] || [notificationType isEqualToString:KNAngelerGetConnected]) {
            
            if([notificationType isEqualToString:kNAngelerAccountVerified]){
            [[GlobalInfo sharedInfo] setUserVerificationStatusText:[info objectForKey:@"verifiedStatusText"]];
            [[GlobalInfo sharedInfo] setUserAngelerStatus:[info objectForKey:@"angelerStatus"]];
            }
            else if([notificationType isEqualToString:KNTaxiDriverVerified]){
                [[GlobalInfo sharedInfo] setTaxiDriverVerificationStatusText:[info objectForKey:@"driverVerifiedStatusText"]];
                [[GlobalInfo sharedInfo] setTaxiDriverVerificationStatus:[info objectForKey:@"driverVerificationStatus"]];
            }
            ProfileController *next = [[ProfileController alloc] initWithNibName:@"ProfileController" bundle:nil];
            [[self getNotificationNavController] pushViewController:next animated:YES];
        }
       else if ([notificationType isEqualToString:KNTaxiBookingRequest]){
            DriverRequestDetailsVC *next = [[DriverRequestDetailsVC alloc] initWithNibName:@"DriverRequestDetailsVC" bundle:nil];
            next.bookingID = [info objectForKey:@"bookingId"];
            [[self getNotificationNavController] pushViewController:next animated:YES];
        }
       else if ([notificationType isEqualToString:KNAngelerFoodBooking]){
           WalletViewController *next = [[WalletViewController alloc] initWithNibName:@"WalletViewController" bundle:nil];
           [[self getNotificationNavController] pushViewController:next animated:YES];
       }
       else if ([notificationType isEqualToString:KNAngelerCharityStatus]){
           CharityDetailsVC *next = [[CharityDetailsVC alloc] initWithNibName:@"CharityDetailsVC" bundle:nil];
           next.charityID = [info objectForKey:@"charityId"];
           next.selectedCallerStr = @"user_specific_charity";
           [[self getNotificationNavController] pushViewController:next animated:YES];
       }
       else if([notificationType isEqualToString:KNAngelerShoppingRequestAccepted] || [notificationType isEqualToString:KNAngelerShoppingRequestDelivered]){
           TrackShoppingVC *Obj = [[TrackShoppingVC alloc] initWithNibName:@"TrackShoppingVC" bundle:nil];
           Obj.shoppingID = [info objectForKey:@"shoppingId"];
             [[self getNotificationNavController] pushViewController:Obj animated:YES];
       }
       else if([notificationType isEqualToString:KNAngelerShoppingRequestCanceled]){
           BuyerShoppingListVC *Obj = [[BuyerShoppingListVC alloc] initWithNibName:@"BuyerShoppingListVC" bundle:nil];
           [[self getNotificationNavController] pushViewController:Obj animated:YES];
       }
        else if([notificationType isEqualToString:KNAngelerShoppingMessage]){
            ChatViewController *Obj = [[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
            Obj.shoppingID = [info objectForKey:@"shoppingId"];
            Obj.currentStatus = [info objectForKey:@"status"];
            Obj.receiverName = [info objectForKey:@"senderName"];
            Obj.itemName = [info objectForKey:@"shoppingItemName"];

            [[self getNotificationNavController] pushViewController:Obj animated:YES];
        }
       else if([notificationType isEqualToString:KNAngelerLocalDeliveryReqCancelled]){
           WalletViewController *Obj = [[WalletViewController alloc] initWithNibName:@"WalletViewController" bundle:nil];
           [[self getNotificationNavController] pushViewController:Obj animated:YES];
       }
       else if([notificationType isEqualToString:KNAngelerAddLocalDelivery]){
           LocalDeliveryRequestListVC *Obj = [[LocalDeliveryRequestListVC alloc] initWithNibName:@"LocalDeliveryRequestListVC" bundle:nil];
           [[self getNotificationNavController] pushViewController:Obj animated:YES];
       }
       else if([notificationType isEqualToString:KNAngelerLocalDeliveryReqAccepted] || [notificationType isEqualToString:KNAngelerLocalDeliveryReqRecieved] || [notificationType isEqualToString:KNAngelerLocalDeliveryReqDelivered]){
           SenderRequestDetailsVC *Obj = [[SenderRequestDetailsVC alloc] initWithNibName:@"SenderRequestDetailsVC" bundle:nil];
           Obj.deliveryID = [info objectForKey:@"deliveryId"];
           [[self getNotificationNavController] pushViewController:Obj animated:YES];
       }
       else if([notificationType isEqualToString:KNAngelerLocalDeliveryEarning]){
           WalletViewController *Obj = [[WalletViewController alloc] initWithNibName:@"WalletViewController" bundle:nil];
           [[self getNotificationNavController] pushViewController:Obj animated:YES];
       }
       else{
           [self CallPageForAutoRedirectionOnGetNotificaion:info];
       }
    
    }
}

- (void)CallPageForAutoRedirectionOnGetNotificaion:(NSDictionary*)info{
    if ([[GlobalInfo sharedInfo] isLoggedIn]) {
        NSString *notificationType = [info objectForKey:@"type"];
        UIViewController *currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
        
        if ([notificationType isEqualToString:KNTaxiCustomerRideStatusUpdate]){
            
            if([currentVc isKindOfClass:[CustomerTrackTrip class]])
            {
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:TaxiPostNotificationCustomerTrackTripRiderStatus
                 object:nil userInfo:info];
            }
            else{
                CustomerTrackTrip *Obj = [[CustomerTrackTrip alloc] initWithNibName:@"CustomerTrackTrip" bundle:nil];
                Obj.bookingID = [info objectForKey:@"bookingId"];
                [[self getNotificationNavController] pushViewController:Obj animated:YES];
            }
        }
        else  if ([notificationType isEqualToString:KNTaxiDriverRideCancelAccepted] || [notificationType isEqualToString:KNTaxiDriverRideCancelReached]){
            
            if([currentVc isKindOfClass:[CustomerTrackTrip class]])
            {
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:TaxiPostNotificationCustomerTrackTripRiderStatus
                 object:nil userInfo:info];
            }
            else{
                TaxiDashboardVC *Obj = [[TaxiDashboardVC alloc] initWithNibName:@"TaxiDashboardVC" bundle:nil];
                Obj.isFromCanelByDriverNotification = YES;
                [[self getNotificationNavController] pushViewController:Obj animated:YES];
            }
        }
        else if ([notificationType isEqualToString:KNTaxiCustomerRideCancelAccepted] || [notificationType isEqualToString:KNTaxiCustomerRideCancelReached]){
            
            if([currentVc isKindOfClass:[DriverTripStartVC class]])
            {
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:TaxiPostNotificationRiderCancelTrip
                 object:nil userInfo:info];
            }
            else{
                TaxiDashboardVC *Obj = [[TaxiDashboardVC alloc] initWithNibName:@"TaxiDashboardVC" bundle:nil];
                Obj.isFromCanelByRiderNotification = YES;
                [[self getNotificationNavController] pushViewController:Obj animated:YES];
            }
        }
    }
    
}



- (void)postLocalNotificationForType:(NSString*)notificationType{
    if ([GlobalInfo checkNotificationType:notificationType] == NOTIF_TYPE_SENDER ) {
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"TrakingInfoController"
         object:nil];
    }
    if ([GlobalInfo checkNotificationType:notificationType] == NOTIF_TYPE_RECEIVER) {
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"DeliveryDetailController"
         object:nil];
    }
    if ([GlobalInfo checkNotificationType:notificationType] == NOTIF_TYPE_DELIVRY_PACKAGE) {
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"DeliveryPackageDetailController"
         object:nil];
    }
}

- (void) logUser {
    // TODO: Use the current user's information
    // You can call any combination of these three methods
    [CrashlyticsKit setUserIdentifier:[[GlobalInfo sharedInfo] userId]];
    [CrashlyticsKit setUserEmail:[[GlobalInfo sharedInfo] userEmail]];
    [CrashlyticsKit setUserName:[[GlobalInfo sharedInfo] userName]];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
   
   
    return YES;
}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {

     [self setDeviceLanguage];
    
   
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    if([[SocketIOManager swiftSharedInstance] isSocketConnected]){
        [[SocketIOManager swiftSharedInstance] removeUserLogoutWithSessionID:[[GlobalInfo sharedInfo] sessionId] userID:[[GlobalInfo sharedInfo] userId]];
        [[SocketIOManager swiftSharedInstance] closeConnection];
    }
    NSLog(@"Application Will Terminate");
}

#pragma mark -
#pragma mark - Side Navigation Panel
- (void)openSideViewFromViewController:(UIViewController*)CurrentViewController{
    
    if (CurrentViewController.childViewControllers.count > 0) {
        return;
    }
    
    SideMenuViewController *side = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController" bundle:nil];
   // side.delegate = CurrentViewController;
    [CurrentViewController addChildViewController:side];
    [CurrentViewController.view addSubview:side.view];
    side.view.frame = CGRectMake(-CurrentViewController.view.frame.size.width, 0, CurrentViewController.view.frame.size.width, CurrentViewController.view.frame.size.height);
    
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        side.view.frame = CGRectMake(0, 0, CurrentViewController.view.frame.size.width, CurrentViewController.view.frame.size.height);
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             
                         }
                     }];
}

- (void)showHomeScreen{
    
    HomeViewController *homeObj = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeObj];
}



-(void)showLoginScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LoginViewController *logInVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVCID"];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:logInVC];
}

- (void)callServiceToCheckStatus:(NSDictionary*)info{
    
   // os, osVersion, appVersion, userId, sessionId, type, typeId
    
  //  Request example :
//http://103.15.232.68/~angeler/web_services/user_settings/dashboard_notification?userId=18&sessionId=jb2u3c430nm90h7ok889btt57gtuo938&os=Android&osVersion=M&appVersion=1.0&typeId=2&type=Booking%20Response

    NSDictionary *param = @{@"type":[info objectForKey:@"type"],
                            @"typeId":[info objectForKey:@"bookingId"]
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDashboardNotification
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:nil
   success:^(id responseObject){
       NSLog(@"response ===   %@",responseObject);
       if ([GlobalInfo checkStatusSuccess:responseObject]){
           NSString *status = [[responseObject objectForKey:@"responseData"] objectForKey:@"status"];
           NSString *BookingID = [[responseObject objectForKey:@"responseData"] objectForKey:@"bookingId"];
           
           if ([status isEqualToString:@"Reserved"]) {
               [self OpenTripCarryingInfoWithBookingID:BookingID];
           }
           if ([status isEqualToString:@"10"] || [status isEqualToString:@"9"] ) {
               
               //|| [status isEqualToString:@"Expired"] || [status isEqualToString:@"Rejected"]
               
               [self openTripList];
           }
           if ([status isEqualToString:@"Accepted"]) {
                [self OpenTripCarryingInfoWithBookingID:BookingID]; //title = @"Pickup";
           }
//           if ([status isEqualToString:@"5"] || [status isEqualToString:@"6"]) {
//              [self OpenDropOffWithBookingID:BookingID PackageType:DROPOFF andServiceCaller:@"1"]; //title = @"Dropoff";
//           }
//           if ([status isEqualToString:@"7"] || [status isEqualToString:@"8"]) {
//               [self OpenDropOffWithBookingID:BookingID PackageType:DELIVERED andServiceCaller:[status isEqualToString:@"7"]?@"2":@"3"]; //2 for status 7 and 3 for status 8//title = @"Delivered";
//           }
       }
   }
   failure:^(id responseObject){
       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
   }];
}

#pragma mark - Itinerary Related block
- (void)OpenPackageInfoWithBookingID:(NSString*)bookingId andPackageType:(ITINERARY_REQUEST_TITLE)packageStatus{

    ItineraryPackageInfoController *next = [[ItineraryPackageInfoController alloc] initWithNibName:@"ItineraryPackageInfoController" bundle:nil];
    next.bookingId = bookingId;
    next.buttonTitle = packageStatus;
    

    [[self getNotificationNavController] pushViewController:next animated:YES];
    
}

-(void)openTripList
{
    MyTripsListViewController *next = [[MyTripsListViewController alloc] initWithNibName:@"MyTripsListViewController" bundle:nil];
    [[self getNotificationNavController] pushViewController:next animated:YES];
}

- (void)OpenTripCarryingInfoWithBookingID:(NSString*)bookingId{
    
    ItineraryPackageInfoController *next = [[ItineraryPackageInfoController alloc] initWithNibName:@"ItineraryPackageInfoController" bundle:nil];
    next.bookingId = bookingId;
    
    [[self getNotificationNavController] pushViewController:next animated:YES];
}


- (void)OpenPickUpWithBookingID:(NSString*)bookingId{

    ItineraryPickUpController *next = [[ItineraryPickUpController alloc] initWithNibName:@"ItineraryPickUpController" bundle:nil];
    next.bookingId = bookingId;
    
    [[self getNotificationNavController] pushViewController:next animated:YES];
}

- (void)OpenDropOffWithBookingID:(NSString*)bookingId PackageType:(ITINERARY_REQUEST_TITLE)packageStatus andServiceCaller:(NSString*)caller{
    
    ItineraryDropOffController *next = [[ItineraryDropOffController alloc] initWithNibName:@"ItineraryDropOffController" bundle:nil];
    next.bookingId = bookingId;
    next.buttonTitle = packageStatus;
    next.serviceCaller = caller;
    
    [[self getNotificationNavController] pushViewController:next animated:YES];
    
}

- (UINavigationController*)getNotificationNavController{
    HomeViewController *homeObj = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:homeObj];
    AppDel.window.rootViewController = navCon;
    return navCon;
}

#pragma mark - Angeler Taxi

-(void)callSocketServiceUpdateLocationDriver{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSNumber *latitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
                   NSNumber *longitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
            
            if(latitude.integerValue != 0 && longitude.integerValue != 0)
                       {
                   
                       NSDictionary *param = @{@"lastLocationLatitude":latitude,
                                               @"lastLocationLongitude":longitude
                                               };
                         
                           [[SocketIOManager swiftSharedInstance] updateDriverCurrentLocationWithLocationDetails:[GlobalInfo getParmWithSession:param]] ;
                       }
        });
        
       
    });
   
        
        
        
        
}

//Previously with webservce without socket call was initiated to send location of driver


//- (void)callServiceToUpdateLocation{
//
//    NSNumber *latitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
//    NSNumber *longitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
//
//    if(latitude.integerValue != 0 && longitude.integerValue != 0)
//    {
////        NSLog(@"Not to call service");
////    }else{
//    NSDictionary *param = @{@"lastLocationLatitude":latitude,
//                            @"lastLocationLongitude":longitude,@"HideHUD":@"YES"
//                            };
//
//
//    //=======Call WebService Engine========
//    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kTrackCurrentLocation
//                                                    Method:REQMETHOD_POST
//                                                parameters:[GlobalInfo getParmWithSession:param]
//                                                     media:nil
//                                            viewController:nil
//                                                   success:^(id responseObject){
//                                                       NSLog(@"response kTrackCurrentLocation ===   %@",responseObject);
//
//                                                   }
//                                                   failure:^(id responseObject){
//
//                                                   }];
//    }
//}

@end

/* #import <Firebase.h>
 #import <RNCPushNotificationIOS.h>
 #import "RNFirebaseNotifications.h"
 #import "RNFirebaseMessaging.h"
 #import "AppDelegate.h"
 
 #import <React/RCTBridge.h>
 #import <React/RCTBundleURLProvider.h>
 #import <React/RCTRootView.h>
 #define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
 @implementation AppDelegate
 
 - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
 {
 //  if ([FIRApp defaultApp] == nil) {
 //    [FIRApp configure];
 //  }
 [FIRApp configure];
 [RNFirebaseNotifications configure];
 [self setPushNotification];
 RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
 RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
 moduleName:@"Collegify"
 initialProperties:nil];
 
 rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
 
 self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
 UIViewController *rootViewController = [UIViewController new];
 rootViewController.view = rootView;
 self.window.rootViewController = rootViewController;
 [self.window makeKeyAndVisible];
 return YES;
 }
 -(void)setPushNotification
 {
 if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) ) {
 [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
 [[UIApplication sharedApplication] registerForRemoteNotifications];
 
 } else {
 UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
 center.delegate = self;
 [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
 if( !error ) {
 // required to get the app to do anything at all about push notifications
 dispatch_async(dispatch_get_main_queue(), ^{
 [[UIApplication sharedApplication] registerForRemoteNotifications];
 });
 NSLog( @"Push registration success." );
 } else {
 NSLog( @"Push registration FAILED" );
 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
 }
 }];
 }
 
 }
 - (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
 [[RNFirebaseNotifications instance] didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
 }
 
/*start */
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
//    [RNCPushNotificationIOS didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
//}
//
///* end */
//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
//    [[RNFirebaseMessaging instance] didRegisterUserNotificationSettings:notificationSettings];
//}
//
//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
//    [[RNFirebaseNotifications instance] didReceiveLocalNotification:notification];
//}
//
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
//{
//    [UNPushNotificationTrigger didReceiveRemoteNotification:notification.request.content.userInfo ];
//    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
//}
//
//
//
//// Called when a user taps on a notification in the foreground
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler
//{
//    NSMutableDictionary *userData = [NSMutableDictionary dictionaryWithDictionary:response.notification.request.content.userInfo];
//    [userData setObject:@(1) forKey:@"openedInForeground"];
//    completionHandler();
//}
//
//- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
//{
//#if DEBUG
//    return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
//#else
//    return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
//#endif
//}
//*/
