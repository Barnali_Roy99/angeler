//
//  SideMenuViewController.h
//  Angeler
//
//  Created by AJAY on 23/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuViewController : UIViewController<UITabBarDelegate,UITableViewDataSource>

@end
