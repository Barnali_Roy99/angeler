//
//  PrivacyPolicyViewController.m
//  Angeler
//
//  Created by AJAY on 28/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "PrivacyPolicyViewController.h"
#import <WebKit/WebKit.h>

@interface PrivacyPolicyViewController ()

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *webViewContainer;


@end

@implementation PrivacyPolicyViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self loadContent];
    // Do any additional setup after loading the view from its nib.
}


#pragma mark - General Functions
-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView setTitle:[NSLocalizedString(@"Privacy Policy", nil) capitalizedString]];
}
- (void)loadContent{
    NSURL *url;
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    if([language isEqualToString:@"en"]){
        url = [NSURL URLWithString:kMenuPrivacyPolicyUrlEngLang];
    }
    else
    {
        url = [NSURL URLWithString:kMenuPrivacyPolicyUrlFrenchLang];
    }

    WKWebView *webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, self.webViewContainer.frame.size.width, self.webViewContainer.frame.size.height)];
    NSLog(@"Webview Frame %@",NSStringFromCGRect([webView frame]));
   NSURLRequest *nsrequest=[NSURLRequest requestWithURL:url];
   [webView loadRequest:nsrequest];
   [self.webViewContainer addSubview:webView];
}


@end
