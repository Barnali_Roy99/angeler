//
//  SideMenuViewController.m
//  Angeler
//
//  Created by AJAY on 23/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuTableViewCell.h"
#import "HomeViewController.h"
#import "ManageDeliveryPointViewController.h"
#import "DeliveryListController.h"
#import "MyTripsListViewController.h"
#import "MyShipmentsListController.h"
#import "ProfileController.h"
#import "LoginViewController.h"
#import "ChangePasswordViewController.h"
#import "WalletViewController.h"
//#import "FAQViewController.h"
#import "PrivacyPolicyViewController.h"
#import "TermsConditionsViewController.h"
#import "BuyerShoppingListVC.h"
#import "SenderAcceptanceListVC.h"
#import "AllPostedProductListVC.h"
#import "LocalDeliveryRequestListVC.h"
#import "AngelerAcceptanveListVC.h"
#import "SenderRequestListVC.h"

#define kMenuName @"MenuName"
#define kMenuIcon @"MenuImage"


@interface SideMenuViewController ()
@property (weak, nonatomic) IBOutlet UIView *gestView;
@property (weak, nonatomic) IBOutlet UIView *headPartView;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileEmail;

@property (weak, nonatomic) IBOutlet UITableView *tblMenu;
@property (nonatomic,readwrite) NSArray *arrSidePaneMenuData;
@property (weak, nonatomic) IBOutlet UIView *imgBackgroudView;
@property (weak, nonatomic) IBOutlet UILabel *angelerIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblDateVersion;

@end


static NSString *CellIdentifier = @"sidePaneCell";
int selectedIndex = 0;

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    _lblProfileName.text = [[GlobalInfo sharedInfo] userName];
    _lblProfileEmail.text = [[GlobalInfo sharedInfo] userEmail];
    
    
    [_imgProfile sd_setImageWithURL:[NSURL URLWithString:[[GlobalInfo sharedInfo] profilePic]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    // Do any additional setup after loading the view from its nib.
    
    //Add swipe gesture
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:leftSwipe];
    
    [_gestView setBackgroundColor:[UIColor blackColor]];
    [_gestView setAlpha:0.0];
    [_headPartView setAlpha:0.0];
    
    
    [_imgBackgroudView makeRound];
    [_imgProfile makeRound];
    
    // to bring smooth effect to dim view
    [UIView animateKeyframesWithDuration:0.15 delay:0.2 options:UIViewKeyframeAnimationOptionBeginFromCurrentState animations:^{
        [_gestView setAlpha:0.4];
        [_headPartView setAlpha:1.0];
    } completion:^(BOOL finished) {
        
    }];
    
    {
        [_tblMenu registerNib:[UINib nibWithNibName:@"SideMenuTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"sidePaneCell"];
        
        _arrSidePaneMenuData = nil;
        
        
        //=========Set Menu at Side Navigation Panel==============================
        
        _arrSidePaneMenuData = [NSArray arrayWithObjects:
                                @{kMenuName:NSLocalizedString(kMenuHome, nil),
                                  kMenuIcon:@"sm_home"},
                                
                                @{kMenuName:NSLocalizedString(kMenuProfile, nil),
                                  kMenuIcon:@"sm_profile"},
                                
                                @{kMenuName:NSLocalizedString(kMenuChangePassword, nil),
                                  kMenuIcon:@"sm_profile"},
                                
                                @{kMenuName:NSLocalizedString(kMenuWallet, nil),
                                  kMenuIcon:@"sm_Wallet"},
                                
                                @{kMenuName:NSLocalizedString(kMenuMytrips, nil),
                                  kMenuIcon:@"sm_MyTrips"},
                                
                                @{kMenuName:NSLocalizedString(kMenuMyShipments, nil),
                                  kMenuIcon:@"sm_MyShipments"},
                                
                                @{kMenuName:NSLocalizedString(kMenuMyDelivery, nil),
                                  kMenuIcon:@"sm_MyDelivery"},
                                
                                
                                @{kMenuName:NSLocalizedString(@"Personal_Shopper", nil),
                                  kMenuIcon:@"sm_DutyFreePersonalShopper"},
                                 @{kMenuName:NSLocalizedString(@"Buyer's_Shopping_List", nil),
                                  kMenuIcon:@"sm_DutyFreeBuyersList"},
                                @{kMenuName:NSLocalizedString(@"My_Acceptance", nil),
                                  kMenuIcon:@"sm_DutyFreeMyAcceptance"},
                                
                                @{kMenuName:NSLocalizedString(@"Local_Delivery_List", nil),
                                                                 kMenuIcon:@""},
                                @{kMenuName:NSLocalizedString(@"Local_Delivery_Requests", nil),
                                                                 kMenuIcon:@""},
                                @{kMenuName:NSLocalizedString(@"Accepted_Local_Deliveries", nil),
                                                                 kMenuIcon:@""},
                                @{kMenuName:NSLocalizedString(kMenuPrivacyPolicy, nil),
                                  kMenuIcon:@"sm_PrivacyPolicy"},
                                @{kMenuName:NSLocalizedString(kMenuTermsConditions, nil),
                                  kMenuIcon:@"sm_Terms"},
                             
                                @{kMenuName:NSLocalizedString(kMenuLogout, nil),
                                  kMenuIcon:@"sm_logout"},
                                nil];
        
        
        
        [_tblMenu reloadData];
        
        
        _lblDateVersion.text = [NSString stringWithFormat:@"23-06-2020 Version : %@ - %@",[[GlobalInfo sharedInfo] appVersion],[[GlobalInfo sharedInfo] appBuild]];
    }
    
    NSString *angelerID = [[GlobalInfo sharedInfo] userUniqueAngelerID];
    if ([angelerID isEqualToString:@""])
    {
        _angelerIDLabel.text = @"";
    }
    else
    {
        _angelerIDLabel.text = [NSString stringWithFormat:@"%@ :  %@",NSLocalizedString(@"ID",nil),angelerID];
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrSidePaneMenuData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.MenuTitleLbl.text = [[_arrSidePaneMenuData objectAtIndex:indexPath.row] objectForKey:kMenuName];
    cell.MenuImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[_arrSidePaneMenuData objectAtIndex:indexPath.row]objectForKey:kMenuIcon]]];
    
    if (indexPath.row == 0 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 11 || indexPath.row == 12 || indexPath.row == 14 ) {
        cell.separatorView.hidden = YES;
    }else{
        cell.separatorView.hidden = NO;
    }
    
    
    
   // cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *menuName = [[_arrSidePaneMenuData objectAtIndex:indexPath.row] objectForKey:kMenuName];
    
    if (indexPath.row == 0){
        HomeViewController *homeObj = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeObj];
    }
    else if (indexPath.row == 1){
        ProfileController *profileObj = [[ProfileController alloc] initWithNibName:@"ProfileController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:profileObj];
    }
    else if (indexPath.row == 2){
        
        ChangePasswordViewController *changePassObj = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:changePassObj];
    }
    
    else if (indexPath.row == 3){
        WalletViewController *walletObj = [[WalletViewController alloc] initWithNibName:@"WalletViewController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:walletObj];
    }
    
    
    else if (indexPath.row == 4){
        MyTripsListViewController *managObj = [[MyTripsListViewController alloc] initWithNibName:@"MyTripsListViewController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
    }
    else if (indexPath.row == 5){
        MyShipmentsListController *managObj = [[MyShipmentsListController alloc] initWithNibName:@"MyShipmentsListController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
    }
    else if (indexPath.row == 6){
        DeliveryListController *homeObj = [[DeliveryListController alloc] initWithNibName:@"DeliveryListController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeObj];
    }
    
    else if (indexPath.row == 7){
        AllPostedProductListVC *Obj = [[AllPostedProductListVC alloc] initWithNibName:@"AllPostedProductListVC" bundle:nil];
         AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:Obj];
    }

    else if (indexPath.row == 8){
        BuyerShoppingListVC *buyerListObj = [[BuyerShoppingListVC alloc] initWithNibName:@"BuyerShoppingListVC" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:buyerListObj];
    }
    else if (indexPath.row == 9){
        SenderAcceptanceListVC *Obj = [[SenderAcceptanceListVC alloc] initWithNibName:@"SenderAcceptanceListVC" bundle:nil];
          AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:Obj];
    }
    else if (indexPath.row == 10){
        LocalDeliveryRequestListVC *Obj = [[LocalDeliveryRequestListVC alloc] initWithNibName:@"LocalDeliveryRequestListVC" bundle:nil];
           AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:Obj];
    }
    else if (indexPath.row == 11){
        SenderRequestListVC *Obj = [[SenderRequestListVC alloc] initWithNibName:@"SenderRequestListVC" bundle:nil];
            AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:Obj];
    }
    else if (indexPath.row == 12){
        AngelerAcceptanveListVC *Obj = [[AngelerAcceptanveListVC alloc] initWithNibName:@"AngelerAcceptanveListVC" bundle:nil];
         AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:Obj];
    }
    else if (indexPath.row == 13)//13
    {
        PrivacyPolicyViewController *obj = [[PrivacyPolicyViewController alloc] initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:obj];
    }
    else if (indexPath.row == 14)//14
{
        TermsConditionsViewController *obj = [[TermsConditionsViewController alloc] initWithNibName:@"TermsConditionsViewController" bundle:nil];
        AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:obj];
    }
    else if (indexPath.row == 15)//15
    {
        
        if([[SocketIOManager swiftSharedInstance] isSocketConnected]){
                          [[SocketIOManager swiftSharedInstance] removeUserLogoutWithSessionID:[[GlobalInfo sharedInfo] sessionId] userID:[[GlobalInfo sharedInfo] userId]];
                                             [[SocketIOManager swiftSharedInstance] closeConnection];
                                         }

        
        [self callLogoutService];
    }
}




#pragma mark - Left Swipe
- (void)swipeLeft:(UISwipeGestureRecognizer*)swipeGest{
    [self swipeLeft];
}
- (IBAction)Back:(id)sender{
    [self swipeLeft];
}
-(void)swipeLeft
{
    [_gestView setAlpha:0.0];
    [_headPartView setAlpha:0.0];
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         self.view.frame = CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [self.view removeFromSuperview];
                             [self removeFromParentViewController];
                         }
                     }];
    
}
#pragma mark
#pragma mark - 'UITapGestureRecognizer'
- (IBAction)handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    //Code to handle the gesture
    NSLog(@"Tap Works!");
    
    [self swipeLeft];
    
}
- (void)callLogoutService{
    NSDictionary *param = @{@"sessionId":[[GlobalInfo sharedInfo] sessionId],
                            @"deviceId":[[GlobalInfo sharedInfo] deviceId],
                             @"lang":[Helper getLocalData:kUserDeviceCurrentLanguage]
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kLogout
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){

                NSLog(@"response ===   %@",responseObject);
                if ([GlobalInfo checkStatusSuccess:responseObject]) {
                    
                  
                    [[GlobalInfo sharedInfo] setSessionId:Empty];
                    [[GlobalInfo sharedInfo] setUserId:Empty];
                    [[GlobalInfo sharedInfo] setIsLoggedIn:NO];
                                        [self showLoginScreen];
                }
           }
       failure:^(id responseObject){
           NSLog(@"failure response ===   %@",responseObject);
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showLoginScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LoginViewController *logInVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVCID"];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:logInVC];
}


@end
