//
//  SideMenuTableViewCell.h
//  Angeler
//
//  Created by AJAY on 23/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *MenuImg;
@property (weak, nonatomic) IBOutlet UILabel *MenuTitleLbl;
@property (weak, nonatomic) IBOutlet UIView *separatorView;


@end
