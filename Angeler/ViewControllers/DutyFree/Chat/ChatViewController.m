//
//  ChatViewController.m
//  Angeler
//
//  Created by AJAY on 09/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "ChatViewController.h"
#import "LPlaceholderTextView.h"
#import "UIView+APToast.h"
#import "ChatMessageModel.h"
#import "ChatMessageTableViewCell.h"

static NSString *CellIdentifier = @"ChatMessageTableViewCell";

@interface ChatViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblChatList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;

@property(nonatomic, weak)IBOutlet UIView *postHolderView;
@property(nonatomic, weak)IBOutlet LPlaceholderTextView *postTextView;
@property(nonatomic, weak)IBOutlet UIButton *postButton;
@property(nonatomic, weak)IBOutlet UIView *postButtonBgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postHolderViewHeightConstraint;

@property (nonatomic, strong) NSMutableArray *arrChatMessageList;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postHolderViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIButton *RemoveImageOutlet;
@property (weak, nonatomic) IBOutlet UIToolbar *keyboardDoneToolBar;
@end

@implementation ChatViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([_currentStatus isEqualToString:DutyFreeShopItemStatus_Delivered]){
        _postHolderViewHeightConstraint.constant = 0;
    }
    
    _arrChatMessageList = [[NSMutableArray alloc]init];
    [self initialSetUp];
    [self initialLoadList];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.view endEditing:true];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark- General Functions

-(void)initialLoadList{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToFetchAddChatInfoIsAddChatMessage:NO];
}


-(void)initialSetUp
{
    [self setLocalization];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    [_topNavbarView showNavbackBtn];
    
    [self.topNavbarView setTitle:[NSString stringWithFormat:@"%@ - %@",_receiverName,_itemName]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    [_tblChatList registerNib:[UINib nibWithNibName:@"ChatMessageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblChatList.estimatedRowHeight = 44;
    _tblChatList.rowHeight = UITableViewAutomaticDimension;
    
 
    
    self.postTextView.layer.cornerRadius = 4; 
    self.postButtonBgView.layer.cornerRadius = 4;
    
    
    
}


-(void)setLocalization{
   
    self.postTextView.placeholderText = NSLocalizedString(@"Type your message", nil);
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        
        _postHolderViewBottomConstraint.constant = keyboardSize.height;
   
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        _postHolderViewBottomConstraint.constant = 0.0;
    }];
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self.keyboardDoneToolBar removeFromSuperview];
    textView.inputAccessoryView = _keyboardDoneToolBar;
    return true;
}

#pragma mark - API Call

- (void)callApiToFetchAddChatInfoIsAddChatMessage:(BOOL)isAddChatMessage{
    
    NSString *callerName = @"list_message";
    NSString *txtMessage = @"";
    
    if(isAddChatMessage){
        callerName = @"add_message";
        txtMessage = _postTextView.text;
    }
    
    NSDictionary *param = @{@"shoppingId":_shoppingID,
                            @"caller" : callerName,
                            @"message" : txtMessage,
                            @"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserGetAddChatMessage
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSLog(@"response ===   %@",responseObject);
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           
                                                           if(isAddChatMessage){
                                                               _postTextView.text = @"";
                                                           }
                                                           
                                                           BOOL isfirstTimeLoad = NO;
                                                           int scrollChatIndex = (int)[_arrChatMessageList count];
                                                           
                                                          
                                                           if ((arr.count == 0) && (START_POSITION != 0)){
                                                               moreRecordsAvailable = NO;
                                                               NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                               
                                                               if (statusCode == 601)
                                                               {
                                                                   if (_arrChatMessageList.count > 0){
                                                                       [self.tblChatList ap_makeToast:NSLocalizedString(@"No more messages", nil)  duration:1.0f position:APToastPositionTop
                                                                                    completion:^{
                                                                                    }];
                                                                   }
                                                                   _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                               }
                                                               return ;
                                                           }
                                                         
                                                               
                                                               if ([self.arrChatMessageList count] < 1 || START_POSITION == 0)
                                                                   isfirstTimeLoad = YES;
                                                               
                                                               
                                                               if (_arrChatMessageList.count > 0) {
                                                                   if (START_POSITION == 0) {
                                                                       [_arrChatMessageList removeAllObjects];
                                                                   }else if(_arrChatMessageList.count > START_POSITION){
                                                                       [_arrChatMessageList removeObjectsInRange:NSMakeRange(START_POSITION, _arrChatMessageList.count - START_POSITION)];
                                                                   }
//                                                                   [_arrChatMessageList addObjectsFromArray:arr];
                                                               }else{
                                                                //   _arrChatMessageList = [NSMutableArray arrayWithArray:arr];
                                                               }
                                                               
                                                               for (NSDictionary *dict in arr)
                                                               {
                                                                   ChatMessageModel *chats = [[ChatMessageModel alloc] initWithChatDictionary:dict];
                                                                   [self.arrChatMessageList addObject:chats];
                                                               }
                                                               
                                                               
                                                               
                                                               
                                                               [self sortChatList];
                                                               
                                                               START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           
                                                            if([_currentStatus isEqualToString:DutyFreeShopItemStatus_Delivered]){
                                                               _lblNoRecord.hidden = _arrChatMessageList.count > 0;
                                                            }
                                                               
                                                               
                                                               [_tblChatList reloadData];
                                                               
                                                               
                                                               if ([self.arrChatMessageList count] > 0)
                                                               {
                                                                   if (isfirstTimeLoad)
                                                                   {
                                                                       [self.tblChatList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.arrChatMessageList count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                                                                   }
                                                                   else
                                                                   {
                                                                       [self.tblChatList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.arrChatMessageList count] - scrollChatIndex - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                                                                   }
                                                               }
                                                                   
                                                          // }
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrChatMessageList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    ChatMessageModel *chat = [_arrChatMessageList objectAtIndex:indexPath.row];
    cell.chatMyMsg.text = @"";
    cell.chatReceieverContentMsg.text = @"";
  
    if(chat.isMyChat)
    {
       
        cell.receieverView.hidden = YES;
        cell.myView.hidden = NO;

        [cell.myImage sd_setImageWithURL:[NSURL URLWithString:chat.userProfilepicImageURL] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        cell.chatMyUserName.text = @"";
        //[[GlobalInfo sharedInfo] userName];
        cell.chatMyRecordTime.text = chat.displayTime;
        cell.chatMyMsg.text = chat.message;
    }
    else
    {
        
        cell.receieverView.hidden = NO;
        cell.myView.hidden = YES;
        
        [cell.receieverImage sd_setImageWithURL:[NSURL URLWithString:chat.userProfilepicImageURL] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        cell.chatReceieverUserName.text = @"";
        cell.chatReceieverRecordTime.text = chat.displayTime;
        cell.chatReceieverContentMsg.text = chat.message;
    }
   
  
    return cell;
    
}

- (void)sortChatList
{
    NSMutableArray *fullListChat = [NSMutableArray arrayWithArray:self.arrChatMessageList];
    
    [fullListChat sortUsingComparator:^NSComparisonResult(ChatMessageModel * obj1, ChatMessageModel *obj2) {
        
        return [obj1.recordDate  compare:obj2.recordDate];
    }];
    
    [self.arrChatMessageList setArray:fullListChat];
}

#pragma mark - Action methods

- (IBAction)sendAction:(id)sender
{
    if ([self.postTextView.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Enter_Text_To_Post", nil)];

        return;
    }
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToFetchAddChatInfoIsAddChatMessage:YES];
}

- (IBAction)onKeyboardDoneBtnClicked:(id)sender {
    
    [self.view endEditing:true];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView{
    if (aScrollView.contentOffset.y == 0)
    {
        //if(moreRecordsAvailable)
        [self callApiToFetchAddChatInfoIsAddChatMessage:NO];
    }
}

@end
