//
//  ChatMessageTableViewCell.m
//  Angeler
//
//  Created by AJAY on 09/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "ChatMessageTableViewCell.h"

@implementation ChatMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.chatCellContentBGView.layer.borderColor = [[UIColor redColor] CGColor];
//    self.chatCellContentBGView.layer.borderWidth = 3.0;
    // Initialization code
    
     [self.chatCellContentReceieverBGView makeCornerRound:10.0];
//withBoarderColor:[UIColor lightishGrayColor] borderWidth:1.5];
    [self.chatCellContentMyBGView makeCornerRound:10.0];
//withBoarderColor:[UIColor appThemeBlueColor] borderWidth:1.5];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
