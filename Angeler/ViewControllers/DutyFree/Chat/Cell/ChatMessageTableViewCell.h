//
//  ChatMessageTableViewCell.h
//  Angeler
//
//  Created by AJAY on 09/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatMessageTableViewCell : UITableViewCell


//Receiever View

@property(nonatomic, weak)IBOutlet UIView *receieverView;
@property(nonatomic, weak)IBOutlet UIView *chatCellContentReceieverBGView;
@property(nonatomic, weak)IBOutlet UILabel *chatReceieverContentMsg;
@property(nonatomic, weak)IBOutlet UILabel *chatReceieverUserName;
@property(nonatomic, weak)IBOutlet UILabel *chatReceieverRecordTime;
@property(nonatomic, weak)IBOutlet UIImageView *receieverImage;
@property(nonatomic, weak)IBOutlet UIView *receieverImageContainerView;


//My/Sender View

@property(nonatomic, weak)IBOutlet UIView *myView;
@property(nonatomic, weak)IBOutlet UIView *chatCellContentMyBGView;
@property(nonatomic, weak)IBOutlet UILabel *chatMyMsg;
@property(nonatomic, weak)IBOutlet UILabel *chatMyUserName;
@property(nonatomic, weak)IBOutlet UILabel *chatMyRecordTime;
@property(nonatomic, weak)IBOutlet UIImageView *myImage;
@property(nonatomic, weak)IBOutlet UIView *myImageContainerView;


@end

NS_ASSUME_NONNULL_END
