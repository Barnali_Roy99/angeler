//
//  ChatViewController.h
//  Angeler
//
//  Created by AJAY on 09/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatViewController : UIViewController
@property(nonatomic,strong)NSString *shoppingID;
@property(nonatomic,strong)NSString *receiverName;
@property(nonatomic,strong)NSString *itemName;
@property(nonatomic,strong)NSString *currentStatus;
@end

NS_ASSUME_NONNULL_END
