//
//  ChatMessageModel.h
//  Angeler
//
//  Created by AJAY on 09/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatMessageModel : NSObject

@property(nonatomic, strong)NSString *messageID;
@property(nonatomic, strong)NSString *receiverID;
@property(nonatomic, strong)NSString *message;
@property(nonatomic, strong)NSString *messageImage;
@property(nonatomic, strong)NSDate *recordDate;
@property(nonatomic, strong)NSString *displayTime;
@property(nonatomic, strong)NSString *userProfilepicImageURL;

@property(nonatomic)BOOL isMyChat;
- (id)initWithChatDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
