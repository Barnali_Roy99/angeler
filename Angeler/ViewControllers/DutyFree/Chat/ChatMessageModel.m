//
//  ChatMessageModel.m
//  Angeler
//
//  Created by AJAY on 09/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "ChatMessageModel.h"

//*messageID;
//"msgId": "1",
//"userId": "159",
//"message": "Hello",
//"recordDateTime": "06/04/2020 05:57 AM"


@implementation ChatMessageModel

@synthesize messageID,message,messageImage,receiverID,recordDate,displayTime,isMyChat;

- (id)initWithChatDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    self.messageID = [NSString stringWithFormat:@"%@", [dict objectForKey:@"msgId"]];
    self.message = [NSString stringWithFormat:@"%@", [dict objectForKey:@"message"]];
   // self.messageImage = [NSString stringWithFormat:@"%@", [dict objectForKey:@"messageImage"]];
    if([[[GlobalInfo sharedInfo] userId] isEqualToString:[dict objectForKey:@"userId"]])
    {
        self.isMyChat = YES;
        self.receiverID = @""; //Here current user is sender
    }
    else{
         self.isMyChat = NO;
        self.receiverID = [dict objectForKey:@"userId"];
    }
  
   
    self.userProfilepicImageURL = [dict objectForKey:@"userProfilepic"];
    self.recordDate = [Helper stringToDateUTC:[dict objectForKey:@"recordDateTime"]];
    self.displayTime = [Helper getPostedTimeStampDifferance:self.recordDate];
    
    return self;
}


@end
