//
//  DutyFreeDashboradVC.m
//  Angeler
//
//  Created by AJAY on 05/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "DutyFreeDashboradVC.h"
#import "BuyerShoppingListVC.h"
#import "SenderAcceptanceListVC.h"
#import "AllPostedProductListVC.h"
#import "DeliverToBuyerVC.h"
#import "TrackShoppingVC.h"
#import "AddShopItemDetailsVC.h"

@interface DutyFreeDashboradVC ()

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblShareAngelerTitle;
@property (weak, nonatomic) IBOutlet UIView *viewShareAngeler;
@property (weak, nonatomic) IBOutlet UIView *viewPostShopping;
@property (weak, nonatomic) IBOutlet UIView *viewShopperList;
@property (weak, nonatomic) IBOutlet UIView *viewBuyersShoppingList;
@property (weak, nonatomic) IBOutlet UIView *viewMyAcceptanceList;
@property (weak, nonatomic) IBOutlet UILabel *lblPostShopping;
@property (weak, nonatomic) IBOutlet UILabel *lblShopperList;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyersShoppingList;
@property (weak, nonatomic) IBOutlet UILabel *lblMyAcceptanceList;


@end

@implementation DutyFreeDashboradVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self setLocalization];
    [self setGesture];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark- General Functions

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView showTitleLogo];
   
}

-(void)setLocalization{
    //Localization
    
    _lblTitle.text = NSLocalizedString(@"Be_Paid_To_Travel", nil);
    _lblDesc.text = NSLocalizedString(@"Sender_Easier", nil);
    _lblPostShopping.text = NSLocalizedString(@"Post_Your_Shopping",nil);
    _lblShopperList.text = NSLocalizedString(@"Personal_Shopper",nil);
    _lblBuyersShoppingList.text = NSLocalizedString(@"Buyer's_Shopping_List",nil);
    _lblMyAcceptanceList.text = NSLocalizedString(@"My_Acceptance",nil);
}

-(void)setGesture
{
    UITapGestureRecognizer *shareAngelerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareAngelerViewTapped:)];
    [_viewShareAngeler addGestureRecognizer:shareAngelerTapRecognizer];
    
    
    UITapGestureRecognizer *postShopTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(postShopDetailsViewTapped:)];
    [_viewPostShopping addGestureRecognizer:postShopTapRecognizer];
    
    
    UITapGestureRecognizer *shoppingListTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showShoppingListViewTapped:)];
    [_viewShopperList addGestureRecognizer:shoppingListTapRecognizer];
    
    UITapGestureRecognizer *buyersShoppingTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buyersShoppingViewTapped:)];
    [_viewBuyersShoppingList addGestureRecognizer:buyersShoppingTapRecognizer];
    
    
    UITapGestureRecognizer *viewMyAcceptanceTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMyAcceptanceListViewTapped:)];
    [_viewMyAcceptanceList addGestureRecognizer:viewMyAcceptanceTapRecognizer];
}

#pragma mark- Gesture Action

- (void)postShopDetailsViewTapped:(UITapGestureRecognizer*)sender
{
    AddShopItemDetailsVC *Obj = [[AddShopItemDetailsVC alloc] initWithNibName:@"AddShopItemDetailsVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}

- (void)showShoppingListViewTapped:(UITapGestureRecognizer*)sender
{

    AllPostedProductListVC *Obj = [[AllPostedProductListVC alloc] initWithNibName:@"AllPostedProductListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
    
}
- (void)buyersShoppingViewTapped:(UITapGestureRecognizer*)sender
{
    BuyerShoppingListVC *Obj = [[BuyerShoppingListVC alloc] initWithNibName:@"BuyerShoppingListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}

- (void)showMyAcceptanceListViewTapped:(UITapGestureRecognizer*)sender
{
   SenderAcceptanceListVC *Obj = [[SenderAcceptanceListVC alloc] initWithNibName:@"SenderAcceptanceListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}

- (void)shareAngelerViewTapped:(UITapGestureRecognizer*)sender
{
    NSString *shareData=[NSString stringWithFormat:NSLocalizedString(@"Share_Link_Title", nil),@""];
    
    NSArray *objectsToShare = @[shareData];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


@end
