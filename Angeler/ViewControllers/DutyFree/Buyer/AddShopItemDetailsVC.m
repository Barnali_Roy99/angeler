//
//  AddShopItemDetailsVC.m
//  Angeler
//
//  Created by AJAY on 06/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AddShopItemDetailsVC.h"

#define MaxItemImageCount 3

@interface AddShopItemDetailsVC ()<UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    int selectedAirportNameIndex;
}


@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

//To Airport

@property (weak, nonatomic) IBOutlet UIView *viewToAirportLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblToAirportTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtAirportName;
@property (nonatomic, strong) NSMutableArray *arrAirportName;

//Item Name

@property (weak, nonatomic) IBOutlet UIView *viewItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNameTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtItemName;


//Item Description

@property (weak, nonatomic) IBOutlet UIView *viewItemDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDescTitle;
@property (nonatomic, weak) IBOutlet UITextView *txtItemDesc;


//Item Price

@property (weak, nonatomic) IBOutlet UIView *viewItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPriceTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNote;


//Picture of item


@property (weak, nonatomic) IBOutlet UILabel *itemPictureTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *itemPicOneView;
@property (weak, nonatomic) IBOutlet UIView *itemPicSecondView;
@property (weak, nonatomic) IBOutlet UIView *itemPicThirdView;

@property (weak, nonatomic) IBOutlet UIImageView *itemPicOneImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicSecondImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicThirdImgView;

@property (weak, nonatomic) IBOutlet UIButton *itemPicOneCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicSecondCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicThirdCancelButton;
@property (weak, nonatomic) IBOutlet UILabel *lblANCTitle;

@property (nonatomic, strong) NSMutableArray *arrMediaObjects;

//Add shop Details Terms

@property (weak, nonatomic) IBOutlet UISwitch *termsConditionSwitch;
@property (nonatomic, weak) IBOutlet UILabel *termsConditionsTextLabel;

@property (weak, nonatomic) IBOutlet UIView *addView;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd;


@end

@implementation AddShopItemDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrMediaObjects = [[NSMutableArray alloc] init];
    [self initialSetUp];
    [self setAirportPicker];
    [self callApiToGetAirportList];
     selectedAirportNameIndex = -1;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}

#pragma mark - Gesture Action

- (void)addViewTapped:(UITapGestureRecognizer*)sender
{
    if (![self validateItem]) {
        return;
    }
    [self callApiToAAddShopItem];
}

- (void)captureImageTapped:(UITapGestureRecognizer*)sender
{
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
                imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
                [Helper showCameraGoToSettingsAlert];
            }
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CameraNotAvailable_Alert", nil)];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
            imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"GalleryNotAvailable_Alert", nil)];
        }
    }];
    
    if (_arrMediaObjects.count >= MaxItemImageCount) {
        [GlobalInfo showAlertTitle:Empty Message:@"You cannot upload any more image" actions:[NSArray arrayWithObject:cancel]];
    }else{
        [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Upload_Image_From", nil) actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];
    }
}



#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:NSLocalizedString(@"Post_Your_Shopping", nil)];
    [_topNavbarView dropShadowWithColor:[UIColor lightGrayColor]];
    
    //Picture Item set Up
    
    _itemPicOneCancelButton.hidden = YES;
    _itemPicSecondCancelButton.hidden = YES;
    _itemPicThirdCancelButton.hidden = YES;
    
    
    [self viewSetUp];
    [self setLocalization];
    
}

- (BOOL)validateItem{
    
    if (selectedAirportNameIndex == -1)
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Select_Airport_Alert", nil)];
        return NO;
        
    }
    if ([_txtItemName.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Item_Name_Blank_Alert", nil)];
        return NO;
        
    }
    
    if ([_txtItemDesc.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Item_Details_Blank_Alert", nil)];
        return NO;
        
    }
    if ([_txtItemPrice.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Item_Price_Blank_Alert", nil)];
        return NO;
        
    }
    
    if (_arrMediaObjects.count == 0) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Add_Shipment_Image_Blank_Alert", nil)];
        return NO;
    }
    
    if (![_termsConditionSwitch isOn])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"TermsConditions_Alert", nil)];
        return NO;
    }
    
    return YES;
}

-(void)setAirportPicker
{
    //Airport List Set Up
    [_txtAirportName setInputView:[Helper pickerWithTag:AIRPORT_NAME forControler:self]];
    [_txtAirportName setInputAccessoryView:[Helper toolbarWithTag:AIRPORT_NAME forControler:self]];
}

-(void)viewSetUp{
    
    [_mainContentView makeCornerRound:15.0];
    [_viewToAirportLocation makeCornerRound:15.0];
    [_viewItemName makeCornerRound:15.0];
    [_viewItemDesc makeCornerRound:15.0];
    [_viewItemPrice makeCornerRound:15.0];
    [_addView makeCornerRound:15.0];
    
    _termsConditionSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
    [_termsConditionSwitch setOn:NO];
    
}


-(void)setLocalization{
    _lblToAirportTitle.text = NSLocalizedString(@"To_Airport", nil);
    _lblItemNameTitle.text = NSLocalizedString(@"Item_Name", nil);
    _lblItemDescTitle.text = NSLocalizedString(@"Details", nil);
    _lblItemPriceTitle .text = NSLocalizedString(@"Price", nil);
    _itemPictureTitleLabel.text = NSLocalizedString(@"Picture_Of_Item", nil);
    _lblPriceNote.text = NSLocalizedString(@"Price_Fee_Msg", nil);
    _lblANCTitle.text = NSLocalizedString(@"Credits", nil);
  
}

-(void)setGesture
{
    UITapGestureRecognizer *addTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addViewTapped:)];
    [_addView addGestureRecognizer:addTapRecognizer];
    
    UITapGestureRecognizer *imageFirstTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicOneImgView addGestureRecognizer:imageFirstTapRecognizer];
    
    UITapGestureRecognizer *imageSecondTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicSecondImgView addGestureRecognizer:imageSecondTapRecognizer];
    
    UITapGestureRecognizer *imageThirdTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicThirdImgView addGestureRecognizer:imageThirdTapRecognizer];
}

#pragma mark - API Call

- (void)callApiToGetAirportList{
    
    NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserSelectAirportList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           _arrAirportName = [NSMutableArray arrayWithArray:arr];
                                                           
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

- (void)callApiToAAddShopItem{
    
    //Image Json
    
    NSMutableArray *dictImages = [[NSMutableArray alloc] init];
    
    for (ImageObjectClass *imgObj in self.arrMediaObjects) {
        NSDictionary *dict = @{@"action":@"added",
                               @"base64String":[imgObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                               @"imageExt":imgObj.mimeType,
                               @"imageId":Empty,
                               @"imageName":imgObj.keyName,
                               @"imageURL":Empty
                               };
        [dictImages addObject:dict];
        
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictImages
                                                       options:0//NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = Empty;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"json data === %@",jsonString);
    }
    
    NSDictionary *param = @{@"airportId":[[_arrAirportName objectAtIndex:selectedAirportNameIndex] objectForKey:@"id"],
                            @"itemName"    :_txtItemName.text,
                            @"details" :_txtItemDesc.text,
                            @"price"     :_txtItemPrice.text,
                            @"imagesJSON" : jsonString
                            };
    
    
    //countryId, name, description, keyword, address, url
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserAddBuyerShoppingPost
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
//                                                           [self.navigationController popViewControllerAnimated:true];
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           if (statusCode == 200)
                                                           {
                                                               NSString *message = NSLocalizedString(@"AddShop_Success_Msg", nil);
                                                               
                                                               [self showAlertWithMsg:message];
                                                           }
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

-(void)showAlertWithMsg:(NSString*)message{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:Empty
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                  [self.navigationController popViewControllerAnimated:true];
                                   
                               }];
    
    
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}




#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Picker Call

- (void)pickerDoneTapped:(UIButton*)sender{
    
    if (sender.tag == AIRPORT_NAME) {
        
        UIPickerView *piker = (UIPickerView*)_txtAirportName.inputView;
        
        if(([piker selectedRowInComponent:0] - 1) >= 0)
        {
            selectedAirportNameIndex = (int)[piker selectedRowInComponent:0] - 1;
            _txtAirportName.text = [[_arrAirportName objectAtIndex:selectedAirportNameIndex] objectForKey:@"name"];
            
        }
        [_txtAirportName resignFirstResponder];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == AIRPORT_NAME) {
        return _arrAirportName.count + 1;
    }
    return 0;
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == AIRPORT_NAME) {
        if (row == 0)
        {
            return NSLocalizedString(@"Choose_Airport", nil);
        }
        return [[_arrAirportName objectAtIndex:row-1] objectForKey:@"name"];
    }
    return Empty;
}

#pragma mark --- Package Image upload/cancel

- (IBAction)cancelImageTapped:(UIButton*)sender{
    
    [_arrMediaObjects removeObjectAtIndex:sender.tag-1];
    [self setImagesToItem];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = info[UIImagePickerControllerEditedImage];
    ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
    
  
    //=========get image type and compress========
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
    // NSString *strImageType = Empty;
    
    if (UTTypeConformsTo(imageUTI, kUTTypeJPEG))
    {
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
        CFRelease(imageUTI);
    }
    else if (UTTypeConformsTo(imageUTI, kUTTypePNG))
    {
        // Handle PNG
        imageObj.mimeType = @"png";
        imageObj.imgData = UIImagePNGRepresentation(image);
        CFRelease(imageUTI);
    }
    else
    {
        NSLog(@"Unhandled Image UTI: %@", imageUTI);
        
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
    }
    
    //_imageData = [_imageData base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //CFRelease(imageUTI);
    //===============================
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //        [mediaData setObject:_imageData forKey:@"MediaData"];
        //        [mediaData setObject:@"File" forKey:@"TagName"];
        //        [imageObj setObject:@"image" forKey:@"MediaType"];
        imageObj.mediaType = @"image";
        [_arrMediaObjects addObject:imageObj];
        
        [self setImagesToItem];
    }];
}
- (void)setImagesToItem
{
    _itemPicOneImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    _itemPicSecondImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    _itemPicThirdImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    
    _itemPicOneCancelButton.hidden = YES;
    _itemPicSecondCancelButton.hidden = YES;
    _itemPicThirdCancelButton.hidden = YES;
    
    
    int counter = 1;
    for (ImageObjectClass *img in _arrMediaObjects) {
        if (counter == 1) {
            _itemPicOneImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicOneCancelButton.hidden = NO;
        }
        if (counter == 2) {
            _itemPicSecondImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicSecondCancelButton.hidden = NO;
        }
        if (counter == 3) {
            _itemPicThirdImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicThirdCancelButton.hidden = NO;
        }
        
        counter++;
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}


@end
