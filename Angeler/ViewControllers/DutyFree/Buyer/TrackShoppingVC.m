//
//  TrackShoppingVC.m
//  Angeler
//
//  Created by AJAY on 16/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "TrackShoppingVC.h"
#import "MWPhotoBrowser.h"

@interface TrackShoppingVC ()<MWPhotoBrowserDelegate,UITextFieldDelegate>
//{
//    BOOL isBuyerImgTap;
//}
{
    BOOL isAngelerImgTap,isPictureImgTap;
    NSDictionary *data;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

//QR code

@property (weak, nonatomic) IBOutlet UIView *viewQrCode;
@property (weak, nonatomic) IBOutlet UIView *viewQrCodeHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblviewQrCodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblQrTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblQrCode;
@property (weak, nonatomic) IBOutlet UIImageView *imgQrCode;
@property (weak, nonatomic) IBOutlet UIView *QRCodeContainerView;


//Item Details

@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;


@property (weak, nonatomic) IBOutlet UIView *viewAngelerDetails;
@property (weak, nonatomic) IBOutlet UIView *viewAngelerDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnAngelerPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgAngelerImage;

@property (nonatomic, strong) NSMutableArray *arrAngelerPhoto;
@property (nonatomic, strong) NSMutableArray *arrPhotos;



@end

@implementation TrackShoppingVC

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToFetchAngelerInfo];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
    
}


#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

#pragma mark - Utility Method

- (UIImage*)qrImageForCode:(NSString*)qrString andImageView:(UIImageView*)qrImageView{
    //    NSString *qrString = @"My string to encode";
    NSData *stringData = [qrString dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = qrImageView.frame.size.width / qrImage.extent.size.width;
    float scaleY = qrImageView.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    return [UIImage imageWithCIImage:qrImage scale:[UIScreen mainScreen].scale
                         orientation:UIImageOrientationUp];
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:NSLocalizedString(@"Track_Shopping", nil)];
    
    
    //Item Details
    
    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
    //Buyer Details
    
    [self.viewAngelerDetails makeCornerRound:15.0];
    [self.viewAngelerDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //QR Code Details
    
    [self.viewQrCode makeCornerRound:15.0];
    [self.viewQrCode dropShadowWithColor:[UIColor lightishGrayColor]];
    
    _QRCodeContainerView.layer.borderColor = [[UIColor blackColor] CGColor];
    _QRCodeContainerView.layer.borderWidth = 1.0;
    
    
 
}
-(void)roundAllSpecificCorner
{
    
    [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewAngelerDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewQrCodeHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
    //QR Code View
  
    _lblviewQrCodeTitle.text = NSLocalizedString(@"Qr_Code",nil);
    
    //Item Details View
    
    _lblItemDetailsHeaderTitle.text = NSLocalizedString(@"Item_Details", nil);
    _lblItemDetailsTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Details",nil)];
    _lblItemNameTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Name",nil)];
    _lblItemPriceTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Price",nil)];
     _lblItemStatusTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Status",nil)];
    
    //Angeler Details
    
    _lblAngelerDetailsTitle.text = NSLocalizedString(@"Angeler_Details", nil);
    
}

//-(void)setGesture
//{
//    [_lblQrTitle setUserInteractionEnabled:YES];
//
//    UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)];
//    [_lblQrTitle addGestureRecognizer:tapges];
//
//}
//
//
//#pragma mark - Gesture Action
//
//- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture
//{
//    isBuyerImgTap = true;
//    [self setMWPhotoBrowserWithTag:0];
//
//}

#pragma mark - API Call

- (void)callApiToFetchAngelerInfo{
    
    NSDictionary *param = @{@"shoppingId":_shoppingID,
                            @"caller" : @"my_shopping_details"
                            };
        //=======Call WebService Engine========
        [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserShoppingDetails
                                                        Method:REQMETHOD_GET
                                                    parameters:[GlobalInfo getParmWithSession:param]
                                                         media:nil
                                                viewController:self
                                                       success:^(id responseObject){
                                                           NSLog(@"response ===   %@",responseObject);
                                                           if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                               data = [responseObject objectForKey:@"responseData"];
                                                               [self updateValueWithData];
                                                           }
                                                       }
                                                       failure:^(id responseObject){
                                                           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                       }];
    
}



-(void)updateValueWithData{
 
    NSString *title = Empty;

        title = [NSString stringWithFormat:NSLocalizedString(@"Scan_Code_Delivery_Info", nil),[[data objectForKey:@"angelerName"] uppercaseString]];

        _lblQrTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[[data objectForKey:@"angelerName"] uppercaseString]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
    _lblQrCode.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Delivery_Code", nil),[data objectForKey:@"validateCode"]];
        _imgQrCode.image = [self qrImageForCode:[data objectForKey:@"validateCode"] andImageView:_imgQrCode];
    
    //Buyer Details
    
    if ([data objectForKey:@"angelerProfilepic"] == nil || [[data objectForKey:@"angelerProfilepic"] isEqualToString:@""])
    {
        _imgAngelerImage.userInteractionEnabled = NO;
    }
    else
    {
        _imgAngelerImage.userInteractionEnabled = YES;
        self.arrAngelerPhoto = [NSMutableArray array];
        [_imgAngelerImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnAngelerImage:)]];
        
        MWPhoto *buyerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]]];
        buyerPhoto.caption =[NSString stringWithFormat:@"Angeler : %@",[data objectForKey:@"angelerName"]];
        [self.arrAngelerPhoto addObject:buyerPhoto];
        [_imgAngelerImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
    }
    
    _lblAngelerName.text = [data objectForKey:@"angelerName"];
    _lblAngelerEmail.text = [data objectForKey:@"angelerEmail"];
    _lblAngelerPhoneNumber.text = [data objectForKey:@"angelerPhoneNo"];
    
    
    //Item Details
    
    
    _lblItemName.text = [data objectForKey:@"itemName"];
    _lblItemDetails.text = [data objectForKey:@"itemDetails"];
    _lblItemPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"itemPrice"],NSLocalizedString(@"Credits", nil)];
     _lblItemStatus.text = [data objectForKey:@"status"];
    
    
    //Picture of item
    {
        
        NSArray *imges = [data objectForKey:@"itemImages"];
        if (imges.count == 0) {
            //  [GlobalInfo hideViewNamed:_pictureView withTopName:@"pictureTop"];
        }else{
            self.arrPhotos = [NSMutableArray array];
            for (int i = 1; i <= imges.count ; i++) {
                UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
                imgV.hidden = NO;
                imgV.userInteractionEnabled = YES;
                [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
                NSLog(@"image url =====  %@",[imges objectAtIndex:i-1] );
                [imgV sd_setImageWithURL:[NSURL URLWithString:[imges objectAtIndex:i-1] ] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
                MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[imges objectAtIndex:i-1]]];
                photo.caption = @"";
                [self.arrPhotos addObject:photo];
                
            }
        }
    }
    
    
    
}


#pragma mark - Gesture Action

- (void)tapOnAngelerImage:(UITapGestureRecognizer*)tapGest
{
    isAngelerImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
}


- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    
    isAngelerImgTap = false;
    isPictureImgTap = true;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
    
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isAngelerImgTap)
    {
        return self.arrAngelerPhoto.count;
    }
    
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (isAngelerImgTap)
    {
        if (index < self.arrAngelerPhoto.count) {
            return [self.arrAngelerPhoto objectAtIndex:index];
        }
    }
    
    else
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    return nil;

}

#pragma mark - Button Action
- (IBAction)callAngeler:(id)sender {
    
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[data objectForKey:@"angelerPhoneNo"]]];
    
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    
}


@end
