//
//  BuyerShoppingListVC.m
//  Angeler
//
//  Created by AJAY on 05/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "BuyerShoppingListVC.h"
#import "BuyerShoppingListTableViewCell.h"
#import "MWPhotoBrowser.h"
#import "TrackShoppingVC.h"
#import "ChatViewController.h"

static NSString *CellIdentifier = @"BuyerShoppingListTableViewCell";

@interface BuyerShoppingListVC ()<MWPhotoBrowserDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
    BOOL isItemImgTap,isItemSenderImgTap;
    
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblBuyerItemList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, strong) NSMutableArray *arrBuyerItemList;
@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (nonatomic, strong) NSMutableArray *arrItemSenderImage;

@end

@implementation BuyerShoppingListVC


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
   
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
     [self initialLoadList];
    [super viewWillAppear:true];
}

#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
     [self initialLoadList];
    [refreshController endRefreshing];
}


#pragma mark- General Functions

-(void)initialLoadList{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadBuyerShoppingList];
}

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView showPlusBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"My_Shopping_List", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    [_tblBuyerItemList registerNib:[UINib nibWithNibName:@"BuyerShoppingListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblBuyerItemList.estimatedRowHeight = 20;
    _tblBuyerItemList.rowHeight = UITableViewAutomaticDimension;
    
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblBuyerItemList addSubview:refreshController];
    
}

#pragma mark - Webservice Call Method

- (void)callServiceToLoadBuyerShoppingList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"caller" : @"my_shopping_list"
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserShoppingList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       if ([GlobalInfo checkStatusSuccess:responseObject])  {
                                                           
                                                           if (_arrBuyerItemList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrBuyerItemList removeAllObjects];
                                                               }else if(_arrBuyerItemList.count > START_POSITION){
                                                                   [_arrBuyerItemList removeObjectsInRange:NSMakeRange(START_POSITION, _arrBuyerItemList.count - START_POSITION)];
                                                               }
                                                               [_arrBuyerItemList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrBuyerItemList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           _lblNoRecord.hidden = _arrBuyerItemList.count > 0;
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601)
                                                           {
                                                               _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           }
                                                           [_tblBuyerItemList reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

- (void)callServiceToCancelShoppingRequest:(NSString*)shoppingID{
    
    NSDictionary *param = @{@"shoppingId":shoppingID,
                          
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserCancelShoppingRequest
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                        
                                                           NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                           
                                                           [self initialLoadList];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];

}
#pragma mark - Scrollview Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //    NSLog(@"currentOffset %f",currentOffset);
    NSLog(@"maximumOffset currentOffset %f %f",maximumOffset,currentOffset);
    
    //NSInteger result = maximumOffset - currentOffset;
    
    CGFloat offsetDifference = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (offsetDifference <= 100.0) {
        if(moreRecordsAvailable)
        {
            NSLog(@"API Called");
             [self callServiceToLoadBuyerShoppingList];
        }
    }
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrBuyerItemList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuyerShoppingListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *data = [_arrBuyerItemList objectAtIndex:indexPath.row];
    
    if( [[data objectForKey:@"status"] isEqualToString:DutyFreeShopItemStatus_Pending] || [[data objectForKey:@"status"] isEqualToString:DutyFreeShopItemStatus_CustomerCanceled] ){
        
        if( [[data objectForKey:@"status"] isEqualToString:DutyFreeShopItemStatus_CustomerCanceled]){
            cell.btnShowStatus.userInteractionEnabled = NO;
        }
        else{
            cell.btnShowStatus.userInteractionEnabled = YES;
        }
        
        cell.senderProfileImageview.hidden = YES;
        cell.chatIconImageview.hidden = YES;
        
    }
    else{
        cell.senderProfileImageview.hidden = NO;
        cell.chatIconImageview.hidden = NO;
        [cell.senderProfileImageview sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        cell.btnShowStatus.userInteractionEnabled = YES;
    }
    
    UITapGestureRecognizer *itemImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemImageViewTapped:)];
    itemImageViewTap.numberOfTapsRequired = 1;
    cell.ItemImageView.userInteractionEnabled = YES;
    [cell.ItemImageView addGestureRecognizer:itemImageViewTap];
    cell.ItemImageView.tag = indexPath.row + 1;
    
    
    UITapGestureRecognizer *itemSenderImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemSenderImageViewTapped:)];
    itemSenderImageViewTap.numberOfTapsRequired = 1;
    cell.senderProfileImageview.userInteractionEnabled = YES;
    [cell.senderProfileImageview addGestureRecognizer:itemSenderImageViewTap];
    cell.senderProfileImageview.tag = indexPath.row + 1;
    
    
    
    
    UITapGestureRecognizer *chatImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chatImageViewTapped:)];
    chatImageViewTap.numberOfTapsRequired = 1;
    [cell.chatIconImageview addGestureRecognizer:chatImageViewTap];
    cell.chatIconImageview.userInteractionEnabled = YES;
    cell.chatIconImageview.tag = indexPath.row + 1;
    
    cell.btnShowStatus.tag = indexPath.row + 1;
    [cell.btnShowStatus addTarget:self action:@selector(cellActionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [cell.ItemImageView sd_setImageWithURL:[NSURL URLWithString:[[data objectForKey:@"itemImages"] objectAtIndex:0]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    
    cell.lblItemName.text = [data objectForKey:@"itemName"];
    cell.lblItemPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"itemPrice"],NSLocalizedString(@"Credits", nil)];
    cell.lblItemDropLocation.text = [data objectForKey:@"airportName"];
    [cell.btnShowStatus setTitle:[data objectForKey:@"buttonText"] forState:UIControlStateNormal];
    
    return cell;
}

#pragma mark - Table view Action

- (void)itemImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *itemImgViewView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrBuyerItemList objectAtIndex:itemImgViewView.tag - 1];
    
    NSArray *itemImages = [data objectForKey:@"itemImages"];
    self.arrPhotos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < itemImages.count; i++) {
        [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[itemImages objectAtIndex:i] ]]];
    }
    
    isItemSenderImgTap = false;
    isItemImgTap = true;
   
    [self setMWPHOtoBrowser];
    
}

- (void)itemSenderImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *senderProfileImgViewView = (UIImageView *)sender.view;
    
    NSDictionary *data = [_arrBuyerItemList objectAtIndex:senderProfileImgViewView.tag - 1];
    
    MWPhoto *angelerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]]];
    angelerPhoto.caption =[NSString stringWithFormat:@"Angeler : %@",[data objectForKey:@"angelerName"]];
    self.arrItemSenderImage = [[NSMutableArray alloc] init];
        [self.arrItemSenderImage addObject:angelerPhoto];
    
    isItemSenderImgTap = true;
    isItemImgTap = false;
    
    [self setMWPHOtoBrowser];
    
}

- (void)chatImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *ChatImgView = (UIImageView *)sender.view;
    
    NSDictionary *data = [_arrBuyerItemList objectAtIndex:ChatImgView.tag - 1];
    ChatViewController *Obj = [[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
    Obj.shoppingID = [data objectForKey:@"shoppingId"];
    Obj.currentStatus = [data objectForKey:@"status"];
    Obj.receiverName = [data objectForKey:@"angelerNameFirstName"];
    Obj.itemName = [data objectForKey:@"itemName"];
    [self.navigationController pushViewController:Obj animated:YES];
    
}

- (void)cellActionButtonTapped:(UIButton*)sender{
    NSDictionary *data = [_arrBuyerItemList objectAtIndex:sender.tag - 1];
    
    if([[[data objectForKey:@"status"] lowercaseString] isEqualToString:@"pending"]){
        [self showShopRequestCancelAlertWithShoppingID:[data objectForKey:@"shoppingId"]];
    }
    else{
    TrackShoppingVC *Obj = [[TrackShoppingVC alloc] initWithNibName:@"TrackShoppingVC" bundle:nil];
    Obj.shoppingID = [data objectForKey:@"shoppingId"];
    [self.navigationController pushViewController:Obj animated:YES];
    }
}

-(void)showShopRequestCancelAlertWithShoppingID:(NSString*)ShoppingID
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:NSLocalizedString(@"ShopRequest_Item_Cancel_Alert", nil)
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   [self callServiceToCancelShoppingRequest:ShoppingID];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                      
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if(isItemImgTap)
    return self.arrPhotos.count;
    else if(isItemSenderImgTap)
        return self.arrItemSenderImage.count;
    else
        return 0;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    else if (index < self.arrItemSenderImage.count) {
        return [self.arrItemSenderImage objectAtIndex:index];
    }
    return nil;
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}



@end
