//
//  BuyerShoppingListTableViewCell.m
//  Angeler
//
//  Created by AJAY on 05/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "BuyerShoppingListTableViewCell.h"

@implementation BuyerShoppingListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.containerView makeCornerRound:15.0];
    [self.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnShowStatus makeCornerRound:15.0];
    self.ItemImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.ItemImageView.layer.borderWidth = 1.0;
    self.senderProfileImageview.layer.borderColor = [[UIColor blackColor] CGColor];
    self.senderProfileImageview.layer.borderWidth = 1.0;
    
    _lblItemDropLocationTitle.text = NSLocalizedString(@"Drop_Location", nil);
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
