//
//  BuyerShoppingListTableViewCell.h
//  Angeler
//
//  Created by AJAY on 05/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuyerShoppingListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *btnShowStatus;
@property (weak, nonatomic) IBOutlet RoundImageView *ItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDropLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDropLocationTitle;
@property (weak, nonatomic) IBOutlet UIImageView *chatIconImageview;
@property (weak, nonatomic) IBOutlet RoundImageView *senderProfileImageview;

@end

NS_ASSUME_NONNULL_END
