//
//  AllPostedProductListVC.m
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AllPostedProductListVC.h"
#import "AllPostedProductListTableViewCell.h"
#import "SharePopUpViewController.h"
#import "MWPhotoBrowser.h"
#import "SenderAcceptanceListVC.h"
#import "SearchListVC.h"

static NSString *CellIdentifier = @"AllPostedProductListTableViewCell";

@interface AllPostedProductListVC ()<MWPhotoBrowserDelegate,SharePopUpDismissDelegate,SearchListDelegate,topNavActionDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
    NSString *filterAirportID;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblAllPostedItemList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, strong) NSMutableArray *arrAllPostedItemList;
@property (nonatomic, strong) NSMutableArray *arrPhotos;

@end

@implementation AllPostedProductListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self initialLoadList];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
   
    [super viewWillAppear:true];
}

#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
     [self initialLoadList];
    [refreshController endRefreshing];
}


#pragma mark- General Functions

-(void)initialLoadList{
    START_POSITION = 0;
    filterAirportID = @"";
    moreRecordsAvailable = YES;
    [self callServiceToLoadAllPostedItemList];
}

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    _topNavbarView.delegate = self;
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView showSearchBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Personal_Shopper", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    [_tblAllPostedItemList registerNib:[UINib nibWithNibName:@"AllPostedProductListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblAllPostedItemList.estimatedRowHeight = 20;
    _tblAllPostedItemList.rowHeight = UITableViewAutomaticDimension;
    
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblAllPostedItemList addSubview:refreshController];
    
}

#pragma mark - Webservice Call Method

- (void)callServiceToLoadAllPostedItemList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"caller" : @"personal_shopper",
                            @"airportId" : filterAirportID
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserShoppingList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       if ([GlobalInfo checkStatusSuccess:responseObject])  {
                                                           
                                                           if (_arrAllPostedItemList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrAllPostedItemList removeAllObjects];
                                                               }else if(_arrAllPostedItemList.count > START_POSITION){
                                                                   [_arrAllPostedItemList removeObjectsInRange:NSMakeRange(START_POSITION, _arrAllPostedItemList.count - START_POSITION)];
                                                               }
                                                               [_arrAllPostedItemList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrAllPostedItemList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           _lblNoRecord.hidden = _arrAllPostedItemList.count > 0;
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601)
                                                           {
                                                               _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           }
                                                           [_tblAllPostedItemList reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



- (void)callApiToAcceptShopItemWithShoppingID :(NSString*)shoppingID andItemName:(NSString*)itemName{
    
    NSDictionary *param = @{@"shoppingId":shoppingID
                            };
    
    
    //countryId, name, description, keyword, address, url
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserAcceptShopping
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                         
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           if (statusCode == 200)
                                                           {
                                                               [self showAlertPopUpWithItemName:itemName];
                                                           }
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}
#pragma mark - Scrollview Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //    NSLog(@"currentOffset %f",currentOffset);
    NSLog(@"maximumOffset currentOffset %f %f",maximumOffset,currentOffset);
    
    //NSInteger result = maximumOffset - currentOffset;
    
    CGFloat offsetDifference = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (offsetDifference <= 100.0) {
        if(moreRecordsAvailable)
        {
            NSLog(@"API Called");
             [self callServiceToLoadAllPostedItemList];
        }
    }
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrAllPostedItemList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllPostedProductListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITapGestureRecognizer *itemImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemImageViewTapped:)];
    itemImageViewTap.numberOfTapsRequired = 1;
    cell.ItemImageView.userInteractionEnabled = YES;
    [cell.ItemImageView addGestureRecognizer:itemImageViewTap];
    cell.ItemImageView.tag = indexPath.row + 1;
    

    
    cell.btnAccept.tag = indexPath.row + 1;
    [cell.btnAccept addTarget:self action:@selector(acceptButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
     NSDictionary *data = [_arrAllPostedItemList objectAtIndex:indexPath.row];
    
    [cell.ItemImageView sd_setImageWithURL:[NSURL URLWithString:[[data objectForKey:@"itemImages"] objectAtIndex:0]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    cell.lblItemName.text = [data objectForKey:@"itemName"];
    cell.lblItemPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"itemPrice"],NSLocalizedString(@"Credits", nil)];
    cell.lblItemDropLocation.text = [data objectForKey:@"airportName"];
    NSString *totalText = [NSString stringWithFormat:@"%@ %@ ANC",NSLocalizedString(@"You_Earn", nil),[data objectForKey:@"angelerComm"]];
    
    cell.lblAngelerEarningPrice.attributedText = [Helper getColorAttributedText:totalText :[totalText rangeOfString:NSLocalizedString(@"You_Earn", nil)] withColor:[UIColor appThemeBlueColor]];
    
    cell.lblItemPostedDate.text = [data objectForKey:@"recordDateTime"];
    
    if([[[GlobalInfo sharedInfo]userId] isEqualToString:[data objectForKey:@"buyerId"]]){
        cell.viewAccept.hidden = YES;
        cell.btnAccept.hidden = YES;
        cell.acceptViewHeightConstraint.constant = 0;
    }
    else{
        cell.viewAccept.hidden = NO;
        cell.btnAccept.hidden = NO;
        cell.acceptViewHeightConstraint.constant = 34;
    }
    
    return cell;
}

#pragma mark - Table view Action

- (void)itemImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *itemImgViewView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrAllPostedItemList objectAtIndex:itemImgViewView.tag - 1];
    
        NSArray *itemImages = [data objectForKey:@"itemImages"];
        self.arrPhotos = [[NSMutableArray alloc] init];
    
      for (int i = 0; i < itemImages.count; i++) {
        [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[itemImages objectAtIndex:i] ]]];
      }
        // [self showZoomImageScreen: self.arrPhotos];
        [self setMWPHOtoBrowser];
    
}

- (void)acceptButtonTapped:(UIButton*)sender{
    
    NSDictionary *data = [_arrAllPostedItemList objectAtIndex:sender.tag - 1];
    [self callApiToAcceptShopItemWithShoppingID:[data objectForKey:@"shoppingId"] andItemName:[data objectForKey:@"itemName"]];
}

#pragma mark - Share Pop Up show

-(void)showAlertPopUpWithItemName:(NSString*)itemName{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Congratulation", nil);
    
    sharePopUpVC.headerDescriptionText = [NSString stringWithFormat: NSLocalizedString(@"Accept_ShoItem_Title_Alert", nil),itemName];
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

#pragma mark - SearchListDelegate

- (void)selectedItem:(NSString*)selectedItemID{
    filterAirportID = selectedItemID;
    
    if([filterAirportID isEqualToString:Empty])
        [_topNavbarView hideHighlightView];
    else
        [_topNavbarView showHighlightView];
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadAllPostedItemList];
    
}

#pragma mark - topNavActionDelegate

- (void)searchIconClicked{
    SearchListVC *searchObj = [[SearchListVC alloc] initWithNibName:@"SearchListVC" bundle:nil];
    searchObj.airportId = filterAirportID;
    searchObj.delegate = self;
    [self presentViewController:searchObj animated:YES completion:nil];
}

#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    SenderAcceptanceListVC *Obj = [[SenderAcceptanceListVC alloc] initWithNibName:@"SenderAcceptanceListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}


#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}


@end
