//
//  DeliverToBuyerVC.m
//  Angeler
//
//  Created by AJAY on 13/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "DeliverToBuyerVC.h"
#import "MWPhotoBrowser.h"
#import "MTBBarcodeScanner.h"

@interface DeliverToBuyerVC ()<MWPhotoBrowserDelegate,UITextFieldDelegate>
{
    BOOL foundCode,isBuyerImgTap,isPictureImgTap;
    NSDictionary *data;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;


@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;


@property (weak, nonatomic) IBOutlet UIView *viewBuyerDetails;
@property (weak, nonatomic) IBOutlet UIView *viewBuyerDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyerDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyerName;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyerPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyerEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnBuyerPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgBuyerImage;


@property (weak, nonatomic) IBOutlet UIView *viewScanQRCodeDetails;
@property (weak, nonatomic) IBOutlet UIView *viewQRCodeInfo;
@property (weak, nonatomic) IBOutlet UIView *viewScanQRCodeDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;
@property (weak, nonatomic) IBOutlet UITextField *txtQrCode;
@property (weak, nonatomic) IBOutlet UIButton *btnDelivery;
@property (weak, nonatomic) IBOutlet UIButton *btnScanQRCode;
@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (weak, nonatomic) IBOutlet UIView *scannerView;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterDeliveryCodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblScanQRCodeDetailsTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *QrCodeViewHeightConstraint;

@property (nonatomic, strong) NSMutableArray *arrBuyerPhoto;
@property (nonatomic, strong) NSMutableArray *arrPhotos;

@end

@implementation DeliverToBuyerVC


#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToFetchDeliverToBuyerInfo];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];

}


#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:NSLocalizedString(@"Deliver_To_Buyer", nil)];
    
  
    //Item Details
    
    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
    //Buyer Details
    
    [self.viewBuyerDetails makeCornerRound:15.0];
    [self.viewBuyerDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Scan QR Code
    
    [self.viewScanQRCodeDetails makeCornerRound:15.0];
    [self.viewQRCodeInfo makeCornerRound:15.0];
    [self.viewScanQRCodeDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnDelivery makeCornerRound:15.0];
    
    
    //Scanner View
    _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:self.scannerView];
   
    
    
}
-(void)roundAllSpecificCorner
{
   
    [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewBuyerDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewScanQRCodeDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
   //QR Code View
    _lblScanQRCodeDetailsTitle.text = NSLocalizedString(@"Shopping_Delivery_Code", nil);
     [_txtQrCode setPlaceholder:NSLocalizedString(@"Enter_Code", nil)];
    _orLabel.text = NSLocalizedString(@"OR", nil);
    [_btnScanQRCode setTitle:NSLocalizedString(@"Scan_QR_Code",nil) forState:UIControlStateNormal];
      [self.btnDelivery setTitle:NSLocalizedString(@"Validate_Drop_Off", nil) forState:UIControlStateNormal];
    
    //Item Details View
    _lblItemDetailsHeaderTitle.text = NSLocalizedString(@"Item_Details", nil);
    _lblItemDetailsTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Details",nil)];
     _lblItemNameTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Name",nil)];
    _lblItemPriceTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Price",nil)];
     _lblItemStatusTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Status",nil)];

    //Buyer Details
    
    _lblBuyerDetailsTitle.text = NSLocalizedString(@"Buyer_Details", nil);

}

#pragma mark - API Call

- (void)callApiToFetchDeliverToBuyerInfo{
    
    NSDictionary *param = @{@"shoppingId":_shoppingID,
                            @"caller" : @"my_acceptance_details"
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserShoppingDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           data = [responseObject objectForKey:@"responseData"];
                                                           [self updateValueWithData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}



-(void)updateValueWithData{
    
    
    if([[data objectForKey:@"status"] isEqualToString:DutyFreeShopItemStatus_Delivered]){
        _viewScanQRCodeDetails.hidden = YES;
        _QrCodeViewHeightConstraint.constant = 0;
    }
    
    //Scan QR code details
    
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"Enter_Delivery_Code", nil), [[data objectForKey:@"buyerName"] uppercaseString]];
   
    _lblEnterDeliveryCodeTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[data objectForKey:@"buyerName"]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
    
    //Buyer Details
    
        if ([data objectForKey:@"buyerProfilepic"] == nil || [[data objectForKey:@"buyerProfilepic"] isEqualToString:@""])
        {
            _imgBuyerImage.userInteractionEnabled = NO;
        }
        else
        {
    _imgBuyerImage.userInteractionEnabled = YES;
    self.arrBuyerPhoto = [NSMutableArray array];
    [_imgBuyerImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnBuyerImage:)]];
            
            MWPhoto *buyerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"buyerProfilepic"]]];
            buyerPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Buyer", nil),[data objectForKey:@"buyerName"]];
    [self.arrBuyerPhoto addObject:buyerPhoto];
     [_imgBuyerImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"buyerProfilepic"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
    }
    
    _lblBuyerName.text = [data objectForKey:@"buyerName"];
    _lblBuyerEmail.text = [data objectForKey:@"buyerEmail"];
    _lblBuyerPhoneNumber.text = [data objectForKey:@"buyerPhoneNo"];
    
    
    //Item Details
    _lblItemName.text = [data objectForKey:@"itemName"];
    _lblItemDetails.text = [data objectForKey:@"itemDetails"];
    _lblItemPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"itemPrice"],NSLocalizedString(@"Credits", nil)];
    _lblItemStatus.text = [data objectForKey:@"status"];
    
    //Picture of item
    {

        NSArray *imges = [data objectForKey:@"itemImages"];
        if (imges.count == 0) {
            //  [GlobalInfo hideViewNamed:_pictureView withTopName:@"pictureTop"];
        }else{
            self.arrPhotos = [NSMutableArray array];
            for (int i = 1; i <= imges.count ; i++) {
                UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
                imgV.hidden = NO;
                imgV.userInteractionEnabled = YES;
                [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
                NSLog(@"image url =====  %@",[imges objectAtIndex:i-1] );
                [imgV sd_setImageWithURL:[NSURL URLWithString:[imges objectAtIndex:i-1] ] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
                MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[imges objectAtIndex:i-1]]];
                photo.caption = @"";
                [self.arrPhotos addObject:photo];

            }
        }
    }
    
    
    
}

#pragma mark - Gesture Action

- (void)tapOnBuyerImage:(UITapGestureRecognizer*)tapGest
{
    isBuyerImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
}


- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    
    isBuyerImgTap = false;
    isPictureImgTap = true;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
    
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isBuyerImgTap)
    {
        return self.arrBuyerPhoto.count;
    }
   
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (isBuyerImgTap)
    {
        if (index < self.arrBuyerPhoto.count) {
            return [self.arrBuyerPhoto objectAtIndex:index];
        }
    }
  
    else
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    return nil;
}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    [Helper showAlertWithTitle:Empty Message:@"Scanning Unavailable"];
}


#pragma mark - Button Action

- (IBAction)scanQrTapped:(id)sender {
    _scannerView.hidden = NO;
    foundCode = NO;
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            NSError* error = nil;
            [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                if (!foundCode) {
                    _txtQrCode.text = code.stringValue;
                    //                    [self callServiceToFetchDataForBranch:code.stringValue];
                    foundCode = YES;
                }
                _scannerView.hidden = YES;
                [self.scanner stopScanning];
            } error:&error];
        } else {
            // The user denied access to the camera
            [self displayPermissionMissingAlert];
        }
    }];
    
}


- (IBAction)validateDeliveryTapped:(id)sender {
    if ([Helper checkEmptyField:_txtQrCode.text]) {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Validate_Dropoff_Alert", nil)];
        return;
    }

    NSDictionary *param = @{@"shoppingId":_shoppingID,
                            @"code":_txtQrCode.text
                            };

    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserUpdateShoppingDeliverStatus
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {

//                                                           UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                                                               [self.navigationController popViewControllerAnimated:true];
//                                                           }];
                                                           NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [Helper showAlertWithTitle:Empty Message:message];
//                                                           [GlobalInfo showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}


- (IBAction)callBuyer:(id)sender {
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[data objectForKey:@"buyerPhoneNo"]]];

    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
   
}


@end
