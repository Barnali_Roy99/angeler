//
//  AllPostedProductListTableViewCell.h
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AllPostedProductListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UILabel *lblAcceptTitle;
@property (weak, nonatomic) IBOutlet RoundImageView *ItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDropLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDropLocationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPostedDate;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPostedDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEarningPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPostedTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acceptViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *viewAccept;

@end

NS_ASSUME_NONNULL_END
