//
//  AllPostedProductListTableViewCell.m
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AllPostedProductListTableViewCell.h"

@implementation AllPostedProductListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.containerView makeCornerRound:15.0];
    [self.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnAccept makeCornerRound:15.0];
    self.ItemImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.ItemImageView.layer.borderWidth = 1.0;
   
    _lblItemDropLocationTitle.text = NSLocalizedString(@"Drop_Location", nil);
    _lblItemPostedDateTitle.text = NSLocalizedString(@"Posted_On", nil);
   _lblAcceptTitle.text = NSLocalizedString(@"Accept", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
