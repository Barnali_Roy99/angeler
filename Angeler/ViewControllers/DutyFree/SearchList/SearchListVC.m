//
//  SearchListVC.m
//  Angeler
//
//  Created by AJAY on 10/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "SearchListVC.h"

@interface SearchListVC ()
{
    BOOL isSearchClearBtnClicked;
}
@property (nonatomic, strong) IBOutlet UITableView *tblSearch;
@property (nonatomic, strong) NSArray *arrMainData;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray *arrSortedData;
@end
static NSString *CellIdentifier = @"Cell";


@implementation SearchListVC


- (void)viewDidLoad {
    [super viewDidLoad];
    isSearchClearBtnClicked = NO;
    _searchBar.delegate = self;
    self.searchBar.placeholder = NSLocalizedString(@"Choose_Airport", nil);
    [_tblSearch registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    [self callApiToGetAirportList];
    
}
- (void)callApiToGetAirportList{
    
    NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserSelectAirportList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           _arrMainData = [responseObject objectForKey:@"responseData"];
                                                           [self createFilterArrOnFirstLoad];
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


- (IBAction)backTapped:(id)sender {
    if(isSearchClearBtnClicked)
        [self.delegate selectedItem:@""];
    [self dismissViewControllerAnimated:YES completion:^{
       
    }];
}


-(void)createFilterArrOnFirstLoad
{
    if ([_airportId isEqualToString:Empty])
    {
        _arrSortedData = _arrMainData;
    }
    else
    {
        NSPredicate* predicateID = [NSPredicate predicateWithFormat:@"SELF.id ==[c] %@", _airportId];
        NSArray* filteredData = [_arrMainData filteredArrayUsingPredicate:predicateID];
        _arrSortedData = filteredData;
        
        if ([_arrSortedData count] > 0)
            
        {
            self.searchBar.text = [[_arrSortedData objectAtIndex:0] objectForKey:@"name"];
        }
    }
    
    [_tblSearch reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrSortedData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[[_arrSortedData objectAtIndex:indexPath.row] objectForKey:@"name"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate selectedItem:[[_arrSortedData objectAtIndex:indexPath.row] objectForKey:@"id"]];
        
        
    }];
}


#pragma mark
#pragma search delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"sercj--- - %@",searchText);
    if ([searchText isEqualToString:Empty]) {
        isSearchClearBtnClicked = YES;
        _arrSortedData = _arrMainData;
        [_tblSearch reloadData];
        return;
    }
    
    
    NSPredicate* predicateID = [NSPredicate predicateWithFormat:@"SELF.id ==[c] %@", _airportId];
    NSArray* filteredData = [_arrMainData filteredArrayUsingPredicate:predicateID];
    _arrSortedData = filteredData;
    [_tblSearch reloadData];
    
}
//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
//    NSLog(@"searchBarCancelButtonClicked");
//
//    self.searchBar.text = @"";
//
//    _arrSortedData = _arrMainData;
//    [_tblSearch reloadData];
//}


@end
