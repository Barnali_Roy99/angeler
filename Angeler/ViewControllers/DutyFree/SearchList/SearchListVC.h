//
//  SearchListVC.h
//  Angeler
//
//  Created by AJAY on 10/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SearchListDelegate <NSObject>

- (void)selectedItem:(NSString*)selectedItemID ;
@end

@interface SearchListVC : UIViewController<UISearchBarDelegate>
@property (nonatomic, strong) NSString *airportId;
@property(nonatomic,assign) id <SearchListDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
