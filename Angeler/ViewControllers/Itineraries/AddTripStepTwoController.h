//
//  AddTripStepTwoController.h
//  Angeler
//
//  Created by AJAY on 02/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ItineryStepTwoDelegate <NSObject>

- (void)passItineryObjFromTwo:(ItineraryClass*)itineryObj;

@end

@interface AddTripStepTwoController : UIViewController

@property (nonatomic, weak) id <ItineryStepTwoDelegate> delegate;
@property (nonatomic, strong) ItineraryClass *Itinerar2ndStepObj;

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIView *viewDocument;
@property (weak, nonatomic) IBOutlet UIView *viewPickUpLocation;
@property (weak, nonatomic) IBOutlet UIView *viewDeliveryLocation;

@property (weak, nonatomic) IBOutlet UILabel *documentTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickUpLocationTitleTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryLocationTitleLabel;


@property (weak, nonatomic) IBOutlet UITextField *txtDocument;
@property (weak, nonatomic) IBOutlet UIView *addTripView;

@property (weak, nonatomic) IBOutlet UILabel *lblPickUpLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryLocation;

@property (weak, nonatomic) IBOutlet UIImageView *pickUpMeetPointImageview;
@property (weak, nonatomic) IBOutlet UIImageView *deliveryMeetPointImageview;



@end
