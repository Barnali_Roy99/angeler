//
//  ItineraryPickUpController.m
//  Angeler
//
//  Created by AJAY on 27/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ItineraryPickUpController.h"
#import "MTBBarcodeScanner.h"

@interface ItineraryPickUpController (){
    BOOL foundCode;
}
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *viPackDetail;
@property (weak, nonatomic) IBOutlet UIView *viPickLocation;
@property (weak, nonatomic) IBOutlet UIView *viSenderDetail;
@property (weak, nonatomic) IBOutlet UIView *viDeliveryPtDetail;
@property (weak, nonatomic) IBOutlet UIView *viEnterDeliveryCode;
@property (weak, nonatomic) IBOutlet UIView *viEnterDeliveryFrame;

@property (weak, nonatomic) IBOutlet UIView *viQrCode;
@property (weak, nonatomic) IBOutlet UIView *viFinalStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblFinalStatus;


@property (weak, nonatomic) IBOutlet UILabel *lblPackType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackSize;
@property (weak, nonatomic) IBOutlet UILabel *lblPackWeight;

@property (weak, nonatomic) IBOutlet UILabel *lblPickUpLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgPickType;

@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderMail;
@property (weak, nonatomic) IBOutlet UIImageView *imgSenderImage;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhone;


@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressType;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblOperatingHours;

@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryCode;
@property (weak, nonatomic) IBOutlet UIImageView *imgQrCode;

@property (weak, nonatomic) IBOutlet UITextField *txtQrCode;

@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (weak, nonatomic) IBOutlet UIView *scannerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *senderCallerHeightConstraint;


@end

@implementation ItineraryPickUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Helper addShadowToLayer:_headerView.layer];
    [Helper addShadowToLayer:_viPackDetail.layer];
    [Helper addShadowToLayer:_viPickLocation.layer];
    [Helper addShadowToLayer:_viSenderDetail.layer];
    [Helper addShadowToLayer:_viDeliveryPtDetail.layer];
    [Helper addShadowToLayer:_viEnterDeliveryFrame.layer];
    [Helper addShadowToLayer:_viQrCode.layer];
    [Helper addShadowToLayer:_viFinalStatus.layer];
    
    [self callApiToFetchInfoWithBookingId];
    _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:self.scannerView];
}

- (void)callApiToFetchInfoWithBookingId{
    //userId, sessionId, os, osVersion, appVersion, bookingId
    
    
///itinerary/pickup_details?userId=17&sessionId=8dc3535b3a1f36d0343d4664d53d693fe6f16ab4&os=Android&osVersion=M&appVersion=1.0&bookingId=1
    
    NSDictionary *param = @{@"bookingId":_bookingId //UDValue(_bookingId)//
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryPickupDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
               if ([GlobalInfo checkStatusSuccess:responseObject]) {
                   [self updateValuesWithData:[responseObject objectForKey:@"responseData"]];
               }
           }
           failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
           }];
}
- (void)updateValuesWithData:(NSDictionary*)data{
    NSDictionary *deliveryPointData = [data objectForKey:@"deliveryPointDetails"];
    if ([deliveryPointData allKeys].count == 0) {
        _viDeliveryPtDetail.hidden = YES;
        [_viDeliveryPtDetail addConstraint:[NSLayoutConstraint constraintWithItem:_viDeliveryPtDetail attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1000 constant:0]];
    }else{
        
        _lblDeliveryAddress.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address: %@",[deliveryPointData objectForKey:@"address"]] andIndex:8];
        
        _lblAddressType.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[deliveryPointData objectForKey:@"addressType"]] andIndex:12];
        
        _lblName.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Name: %@",[deliveryPointData objectForKey:@"contactName"]] andIndex:5];
        
        _lblPhone.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Phone: %@%@",[deliveryPointData objectForKey:@"contactPhoneISD"],[deliveryPointData objectForKey:@"contactPhone"]] andIndex:6];
        
        _lblOperatingHours.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Operating hours: %@-%@",[deliveryPointData objectForKey:@"operatingFromHours"],[deliveryPointData objectForKey:@"operatingToHours"]] andIndex:16];
        
   
//        if (![[deliveryPointData objectForKey:@"addressType"] isEqualToString:@"Residential"]) {
//            _lblOperatingHours.text = [deliveryPointData objectForKey:@"operatingFromHours"];
//        }
        
    }
    
    _lblPackType.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];
    _lblPackSize.text = [NSString stringWithFormat:@"%@x%@x%@ CM",[data objectForKey:@"packageHeight"],[data objectForKey:@"packageWidth"],[data objectForKey:@"packageLength"]];
    _lblPackWeight.text = [NSString stringWithFormat:@"%@ GM",[data objectForKey:@"packageWeight"]];
    
    _lblPickUpLocation.text = [data objectForKey:@"angelerPickupAddress"];
    _imgPickType.image = [GlobalInfo AngelerImageForPickupType:[data objectForKey:@"angelerPickupType"]];
    
    _lblSenderName.text = [data objectForKey:@"senderName"];
    _lblSenderMail.text = [data objectForKey:@"senderEmail"];
    [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    [Helper makeItRound:_imgSenderImage.layer withRadius:25];
    if ([[data objectForKey:@"senderPhone"] isEqualToString:Empty]) {
        _senderCallerHeightConstraint.constant = 0;
    }else{
        _lblSenderPhone.text = [data objectForKey:@"senderPhone"];
    }
    if ([[data objectForKey:@"finalStatusText"] isEqualToString:Empty]) {
        if (![[data objectForKey:@"code"] isEqualToString:Empty]) {
            _viQrCode.hidden = NO;
            _lblDeliveryCode.text = [NSString stringWithFormat:@"Delivery code: %@",[data objectForKey:@"code"]];
            _imgQrCode.image = [self qrImageForCode:[data objectForKey:@"code"] andImageView:_imgQrCode];
        }
    }else{
        _viEnterDeliveryCode.hidden = YES;
        
        _viFinalStatus.hidden = NO;
        _lblFinalStatus.attributedText = [GlobalInfo formatFinalStatusText:[NSString stringWithFormat:@"Status: %@",[data objectForKey:@"finalStatusText"]] andIndex:7];
    }
}

- (UIImage*)qrImageForCode:(NSString*)qrString andImageView:(UIImageView*)qrImageView{
//    NSString *qrString = @"My string to encode";
    NSData *stringData = [qrString dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = qrImageView.frame.size.width / qrImage.extent.size.width;
    float scaleY = qrImageView.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    return [UIImage imageWithCIImage:qrImage scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
}


- (IBAction)pickUpTapped:(id)sender {
    if ([Helper checkEmptyField:_txtQrCode.text]) {
        [Helper showAlertWithTitle:Empty Message:strKey_itinery_pickup_qr_code_required];
        return;
    }
  
    
//    Request parameter:
//    userId, sessionId, os, osVersion, appVersion, bookingId, code
    
   //itinerary/update_pickup_status?userId=17&sessionId=8dc3535b3a1f36d0343d4664d53d693fe6f16ab4&os=Android&osVersion=M&appVersion=1.0&bookingId=2&code=fL6AFZxG
    
    NSDictionary *param = @{@"bookingId":_bookingId,
                            @"code":_txtQrCode.text
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUpdatePickUpStatus
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               
               UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                   [self backTapped:nil];
               }];
               NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
               [GlobalInfo showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
    
}
- (IBAction)scanQrTapped:(id)sender {
    _scannerView.hidden = NO;
    foundCode = NO;
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            NSError* error = nil;
            [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                if (!foundCode) {
                    _txtQrCode.text = code.stringValue;
//                    [self callServiceToFetchDataForBranch:code.stringValue];
                    foundCode = YES;
                }
                 _scannerView.hidden = YES;
                [self.scanner stopScanning];
            } error:&error];
        } else {
            // The user denied access to the camera
            [self displayPermissionMissingAlert];
        }
    }];

}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    [Helper showAlertWithTitle:Empty Message:@"Scanning Unavailable"];
}


- (IBAction)backTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)callSender:(id)sender {
    if (_lblSenderPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblSenderPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
