//
//  ItineraryPackageInfoController.h
//  Angeler
//
//  Created by AJAY on 26/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItineraryPackageInfoController : UIViewController
{
    NSString *senderContactNumber,*recipientContactNumber;
}
@property (nonatomic, strong) NSString *bookingId;
@property (nonatomic) ITINERARY_REQUEST_TITLE buttonTitle;
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *contentView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TripDetailsTopConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shipmentDetailsTopConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *senderDetailsTopConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemDetailsTopConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recipentDetailsTopConstraint;


//@property (nonatomic, strong) NSString *status;
@end
