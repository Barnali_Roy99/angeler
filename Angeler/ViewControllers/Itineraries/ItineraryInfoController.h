//
//  ItineraryInfoController.h
//  Angeler
//
//  Created by AJAY on 17/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItineraryInfoController : UIViewController
@property (nonatomic, strong) NSString *itenaryId;
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@end
