//
//  AddTripStepOneController.h
//  Angeler
//
//  Created by AJAY on 01/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddTripStepOneController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIView *viewFromCity;
@property (weak, nonatomic) IBOutlet UIView *viewFromDateTime;
@property (weak, nonatomic) IBOutlet UIView *viewToCity;
@property (weak, nonatomic) IBOutlet UIView *viewToDateTime;
@property (weak, nonatomic) IBOutlet UICollectionView *typeOfTransportCollectionView;
@property (weak, nonatomic) IBOutlet UITextField *referenceNumberTextfield;
@property (weak, nonatomic) IBOutlet UIView *viewReferenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *fromSelectCityTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *fromSelectCityValueTextfield;
@property (weak, nonatomic) IBOutlet UILabel *fromDateTimeTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *fromDateTimeValueTextfield;
@property (weak, nonatomic) IBOutlet UILabel *fromDateTimeLocationTitlelabel;
@property (weak, nonatomic) IBOutlet UILabel *toSelectCityTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *toSelectCityValueTextfield;
@property (weak, nonatomic) IBOutlet UILabel *toDateTimeTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *toDateTimeValueTextfield;
@property (weak, nonatomic) IBOutlet UILabel *toDateTimeLocationTitlelabel;
@property (weak, nonatomic) IBOutlet UILabel *modeOfTravelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *travelReferenceNumberTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *nextView;
@property (weak, nonatomic) IBOutlet UILabel *lblNext;
@property (weak, nonatomic) IBOutlet UIImageView *nextImageView;

@end
