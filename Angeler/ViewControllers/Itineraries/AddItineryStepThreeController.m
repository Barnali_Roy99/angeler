//
//  AddItineryStepThreeController.m
//  Angeler
//
//  Created by AJAY on 07/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "AddItineryStepThreeController.h"

//#import "MapViewController.h"
#import "SingleLocationMapController.h"
#import "MultipleLocationMapViewController.h"
#import "AddItineraryCompletionPopUpViewController.h"
//#import <GoogleMaps/GoogleMaps.h>


@interface AddItineryStepThreeController ()<SingleLocationAddressDelegate,MultipleLocationAddressDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *viOne;
@property (weak, nonatomic) IBOutlet UILabel *lblPickMeetPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblPickPickUpPoint;

@property (nonatomic, weak) IBOutlet UIButton *btnPickMtPtText;
@property (nonatomic, weak) IBOutlet UIButton *btnPickPkPtText;
@property (nonatomic, weak) IBOutlet UIImageView *imgPickMtPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgPickPkPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgPickPremSer;

@property (weak, nonatomic) IBOutlet UIView *viTwo;
@property (weak, nonatomic) IBOutlet UILabel *lblDelvryMeetPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblDelvryPickUpPoint;

@property (nonatomic, weak) IBOutlet UIButton *btnDelvryMtPtText;
@property (nonatomic, weak) IBOutlet UIButton *btnDelvryPkPtText;
@property (nonatomic, weak) IBOutlet UIImageView *imgDelvryMtPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgDelvryPkPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgDelvryPremSer;

@property (nonatomic, strong) UIImage *select;
@property (nonatomic, strong) UIImage *deselect;

@property (nonatomic,strong) AddressClass *pickMeetPointAddress;
@property (nonatomic,strong) NSMutableArray *arrPickPointAddress;
@property (nonatomic,strong) AddressClass *deliveryMeetPointAddress;
@property (nonatomic,strong) NSMutableArray *arrDeliveryPointAddress;


@end

@implementation AddItineryStepThreeController

- (void)viewDidLoad {
    [super viewDidLoad];
    _select = [UIImage imageNamed:@"check_box_select"];
    _deselect = [UIImage imageNamed:@"check_box_deselect"];
    
    _pickMeetPointAddress =  [[AddressClass alloc] init];
    _pickMeetPointAddress.lat_Value = self.Itinerar3rdStepObj.from_Address.lat_Value;
    _pickMeetPointAddress.long_Value = self.Itinerar3rdStepObj.from_Address.long_Value;
    
    
    _deliveryMeetPointAddress =  [[AddressClass alloc] init];
    _deliveryMeetPointAddress.lat_Value = self.Itinerar3rdStepObj.to_Address.lat_Value;
    _deliveryMeetPointAddress.long_Value = self.Itinerar3rdStepObj.to_Address.long_Value;
    
    _arrPickPointAddress = [[NSMutableArray alloc] init];
    _arrDeliveryPointAddress = [[NSMutableArray alloc] init];
    
    [Helper addShadowToLayer:_headerView.layer];
    
    UIScrollView *scrollVi = (UIScrollView*)_viOne.superview;
    [scrollVi setContentSize:CGSizeMake(MainScreenWidth, 800)];
    
    [Helper addShadowToLayer:_viOne.layer];
    ROUNDEDCORNER(_viOne.layer);

    [Helper addShadowToLayer:_viTwo.layer];
    ROUNDEDCORNER(_viTwo.layer);
    
  
    [Helper addBorderToLayer:_btnPickMtPtText.layer];
    ROUNDEDCORNER(_btnPickMtPtText.layer);
    
    
    [Helper addBorderToLayer:_btnPickPkPtText.layer];
    ROUNDEDCORNER(_btnPickPkPtText.layer);
    
    [Helper addBorderToLayer:_btnDelvryMtPtText.layer];
    ROUNDEDCORNER(_btnDelvryMtPtText.layer);
    
    [Helper addBorderToLayer:_btnDelvryPkPtText.layer];
    ROUNDEDCORNER(_btnDelvryPkPtText.layer);
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if ([_pickMeetPointAddress.Address isEqualToString:Empty]) {
        _lblPickMeetPoint.text = @"Pick address where to meet the sender";
    }else{
        _lblPickMeetPoint.text = _pickMeetPointAddress.Address;
    }
    
    if (_arrPickPointAddress.count == 0) {
        _lblPickPickUpPoint.text = @"Select address where you can pickup packages";
    }else{
        NSString *addRess = Empty;
        for (int i = 0; i < _arrPickPointAddress.count; i++) {
            addRess = [NSString stringWithFormat:@"%@%d. %@\n",addRess,i+1,[(AddressClass*)[_arrPickPointAddress objectAtIndex:i] Address]];
        }
        _lblPickPickUpPoint.text = [addRess substringToIndex:addRess.length-1];
    }
    
    if ([_deliveryMeetPointAddress.Address isEqualToString:Empty]) {
        _lblDelvryMeetPoint.text = @"Pick address where to meet the reciver";
    }else{
        _lblDelvryMeetPoint.text = _deliveryMeetPointAddress.Address;
    }
    
    if (_arrDeliveryPointAddress.count == 0) {
        _lblDelvryPickUpPoint.text = @"Select address where you can drop off packages";
    }else{
        NSString *addRess = Empty;
        for (int i = 0; i < _arrDeliveryPointAddress.count; i++) {
            addRess = [NSString stringWithFormat:@"%@%d. %@\n",addRess,i+1,[(AddressClass*)[_arrDeliveryPointAddress objectAtIndex:i] Address]];
        }
        _lblDelvryPickUpPoint.text = [addRess substringToIndex:addRess.length-1];
    }
}

- (IBAction)pickMeetingPtTextClicked:(UIButton*)sender {
    [self showSingleAddressMap:_pickMeetPointAddress andTag:PICK_MEET_POINT_ADDRESS];
}
- (IBAction)DeliveryMeetingPtTextClicked:(UIButton*)sender {
    [self showSingleAddressMap:_deliveryMeetPointAddress andTag:DELIVERY_MEET_POINT_ADDRESS];
}
- (void)showSingleAddressMap:(AddressClass*)address andTag:(TAG_VALUE)tag{
    SingleLocationMapController *mapObj = [[SingleLocationMapController alloc] initWithNibName:@"SingleLocationMapController" bundle:nil];
    mapObj.delegate = self;
    mapObj.prevlat = address.lat_Value;
    mapObj.prevLong = address.long_Value;
    mapObj.tag = tag;
    [self.navigationController pushViewController:mapObj animated:YES];
}
#pragma mark-SingleAddressDelegate
- (void)selectedAddress:(AddressClass*)address forTag:(TAG_VALUE)tag
{
    if (tag == PICK_MEET_POINT_ADDRESS) {
        _lblPickMeetPoint.text = address.Address;
        _pickMeetPointAddress.Address = address.Address;
        _pickMeetPointAddress.lat_Value = address.lat_Value;
        _pickMeetPointAddress.long_Value = address.long_Value;
    }
    if (tag == DELIVERY_MEET_POINT_ADDRESS) {
        _lblDelvryMeetPoint.text = address.Address;
        _deliveryMeetPointAddress.Address = address.Address;
        _deliveryMeetPointAddress.lat_Value = address.lat_Value;
        _deliveryMeetPointAddress.long_Value = address.long_Value;
    }
}

- (IBAction)pickPickedPtTextClicked:(UIButton*)sender {
    [self showMultipleAddressMap:self.Itinerar3rdStepObj.from_Address selectedStores:_arrPickPointAddress andTag:PICK_PICKUP_POINT_ADDRESS];
}
- (IBAction)DeliveryPickPtTextClicked:(UIButton*)sender {
    [self showMultipleAddressMap:self.Itinerar3rdStepObj.to_Address selectedStores:_arrDeliveryPointAddress andTag:DELIVERY_PICKUP_POINT_ADDRESS];
}
- (void)showMultipleAddressMap:(AddressClass*)address selectedStores:(NSMutableArray*)stores andTag:(TAG_VALUE)tag{
    MultipleLocationMapViewController *mapObj = [[MultipleLocationMapViewController alloc] initWithNibName:@"MultipleLocationMapViewController" bundle:nil];
    mapObj.delegate = self;
    mapObj.addressObj = address;
    mapObj.tag = tag;
    mapObj.arrSelStorAddress = stores;
    
    [self.navigationController pushViewController:mapObj animated:YES];
}
#pragma mark-MultipleAddressDelegate
- (void)selectedAddressArray:(NSMutableArray*)Arr_address forTag:(TAG_VALUE)tag{
    NSString *str = Empty;
    for (int i = 0; i < Arr_address.count; i++) {
        str = [NSString stringWithFormat:@"%@%d. %@\n",str,i+1,[(AddressClass*)[Arr_address objectAtIndex:i] Address]];
    }
    if (tag == PICK_PICKUP_POINT_ADDRESS){
        _lblPickPickUpPoint.text = str;
        _arrPickPointAddress = Arr_address;
    }
    if (tag == DELIVERY_PICKUP_POINT_ADDRESS){
        _lblDelvryPickUpPoint.text = str;
        _arrDeliveryPointAddress = Arr_address;
    }
}


- (IBAction)checkBoxClicked:(UIButton*)sender {
   
    switch (sender.tag) {
        case 1:
            _imgPickMtPt.image = [_imgPickMtPt.image isEqual:_select]?_deselect:_select;
            break;
        case 2:
            _imgPickPkPt.image = [_imgPickPkPt.image isEqual:_select]?_deselect:_select;
            break;
        case 3:
            _imgPickPremSer.image = [_imgPickPremSer.image isEqual:_select]?_deselect:_select;
            break;
        case 4:
            _imgDelvryMtPt.image = [_imgDelvryMtPt.image isEqual:_select]?_deselect:_select;
            break;
        case 5:
            _imgDelvryPkPt.image = [_imgDelvryPkPt.image isEqual:_select]?_deselect:_select;
            break;
        case 6:
            _imgDelvryPremSer.image = [_imgDelvryPremSer.image isEqual:_select]?_deselect:_select;
            break;
            
        default:
            break;
    }
}

- (IBAction)backTapped:(id)sender{
    //[self.delegate passItineryObjFromThree:self.ItinerarObj];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)infoTapped:(UIButton*)sender {
    NSString *strUrl = @"http://angeler-app.com/";
    
//    InfoViewController *info = [[InfoViewController alloc] initWithNibName:@"InfoViewController" bundle:nil];
//    info.strUrl = strUrl;
//    
//    [self addChildViewController:info];
//    info.view.frame = CGRectMake(0, 0, MainScreenWidth, MainScreenHeight);
//    
//    [self.view addSubview:info.view];
//    
//    if (sender.tag == 1) {
//        
//    }else{
//        
//    }
}
- (BOOL)validateStep3{
    if ([_imgPickMtPt.image isEqual:_deselect] &&
        [_imgPickPkPt.image isEqual:_deselect] &&
        [_imgPickPremSer.image isEqual:_deselect]) {
        [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertPickIdEmpty];
        return NO;
    }else{
        if (([_imgPickMtPt.image isEqual:_select]) && ([_pickMeetPointAddress.Address isEqualToString:Empty])) {
            [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertPickMeetingPointEmpty];
            return NO;
        }
        if ([_imgPickPkPt.image isEqual:_select] && (_arrPickPointAddress.count == 0)) {
            [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertPickPointsEmpty];
            return NO;
        }
    }
    
    if ([_imgDelvryMtPt.image isEqual:_deselect] &&
        [_imgDelvryPkPt.image isEqual:_deselect] &&
        [_imgDelvryPremSer.image isEqual:_deselect]) {
        [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertDropoffIdEmpty];
        return NO;
    }else{
        if (([_imgDelvryMtPt.image isEqual:_select]) && ([_deliveryMeetPointAddress.Address isEqualToString:Empty])) {
            [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertDropMeetingPointEmpty];
            return NO;
        }
        if ([_imgDelvryPkPt.image isEqual:_select] && (_arrDeliveryPointAddress.count == 0)) {
            [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertDropPointsEmpty];
            return NO;
        }
    }

    
    return YES;
}
- (IBAction)confirmTapped:(id)sender {
    if ([self validateStep3]) {
        [self callServiceToUploadItinery];
    }

    
    
    NSLog(@"Submit success;;;;;;;;;");
    
}
- (void)callServiceToUploadItinery{
    
    NSString *pickPickUpPoint = Empty;
    if ([_imgPickPkPt.image isEqual:_select]) {
        BOOL firstObj = YES;
        for (AddressClass *address in _arrPickPointAddress) {
            if (firstObj) {
                firstObj = NO;
                pickPickUpPoint = [NSString stringWithFormat:@"%@",address.addresId];
            }else{
                pickPickUpPoint = [NSString stringWithFormat:@"%@,%@",pickPickUpPoint,address.addresId];
            }
        }
    }
    NSString *pickupMPAddress = Empty;
    NSString *pickupMPLatitude = Empty;
    NSString *pickupMPLongitude = Empty;
    if ([_imgPickMtPt.image isEqual:_select]) {
        pickupMPAddress = _pickMeetPointAddress.Address;
        pickupMPLatitude = [_pickMeetPointAddress.lat_Value stringValue];
        pickupMPLongitude = [_pickMeetPointAddress.long_Value stringValue];
    }
    
    NSString *deliveryPickUpPoint = Empty;
    if ([_imgDelvryPkPt.image isEqual:_select]) {
        BOOL firstObj = YES;
        for (AddressClass *address in _arrDeliveryPointAddress) {
            if (firstObj) {
                firstObj = NO;
                deliveryPickUpPoint = [NSString stringWithFormat:@"%@",address.addresId];
            }else{
                deliveryPickUpPoint = [NSString stringWithFormat:@"%@,%@",deliveryPickUpPoint,address.addresId];
            }
        }
    }
    NSString *dropoffMPAddress = Empty;
    NSString *dropoffMPLatitude = Empty;
    NSString *dropoffMPLongitude = Empty;
    if ([_imgDelvryMtPt.image isEqual:_select]) {
        dropoffMPAddress = _deliveryMeetPointAddress.Address;
        dropoffMPLatitude = [_deliveryMeetPointAddress.lat_Value stringValue];
        dropoffMPLongitude = [_deliveryMeetPointAddress.long_Value stringValue];
    }


    
    NSDictionary *param = @{@"departureCountry":self.Itinerar3rdStepObj.from_Address.countryId,
                            @"pickupMPCountry":self.Itinerar3rdStepObj.from_Address.countryId,
                            @"departureCity":self.Itinerar3rdStepObj.from_Address.cityId,
                            @"pickupMPCity":self.Itinerar3rdStepObj.from_Address.cityId,
                            @"arrivalCountry":self.Itinerar3rdStepObj.to_Address.countryId,
                            @"dropoffMPCountry":self.Itinerar3rdStepObj.to_Address.countryId,
                            @"arrivalCity":self.Itinerar3rdStepObj.to_Address.cityId,
                            @"dropoffMPCity":self.Itinerar3rdStepObj.to_Address.cityId,
                            @"departureDate":[[self.Itinerar3rdStepObj.Departing_Date_Time componentsSeparatedByString:@" "] firstObject],
                            @"departureTime":[[self.Itinerar3rdStepObj.Departing_Date_Time componentsSeparatedByString:@" "] lastObject],
                            @"arrivalDate":[[self.Itinerar3rdStepObj.Arrival_Date_Time componentsSeparatedByString:@" "] firstObject],
                            @"arrivalTime":[[self.Itinerar3rdStepObj.Arrival_Date_Time componentsSeparatedByString:@" "] lastObject],
                            @"travelMode":[NSString stringWithFormat:@"%ld",(long)self.Itinerar3rdStepObj.travel_Mode],
                            @"travelRefNo":self.Itinerar3rdStepObj.Travel_Ref_No,
                            @"packageHeightMin":[[self.Itinerar3rdStepObj.Size_Height_Min_Max componentsSeparatedByString:@","] firstObject],
                            @"packageHeightMax":[[self.Itinerar3rdStepObj.Size_Height_Min_Max componentsSeparatedByString:@","] lastObject],
                            @"packageWidthMin":[[self.Itinerar3rdStepObj.Size_Width_Min_Max componentsSeparatedByString:@","] firstObject],
                            @"packageWidthMax":[[self.Itinerar3rdStepObj.Size_Width_Min_Max componentsSeparatedByString:@","] lastObject],
                            @"packageLengthMin":[[self.Itinerar3rdStepObj.Size_Length_Min_Max componentsSeparatedByString:@","] firstObject],
                            @"packageLengthMax":[[self.Itinerar3rdStepObj.Size_Length_Min_Max componentsSeparatedByString:@","] lastObject],
                            @"packageMaxWeight":self.Itinerar3rdStepObj.Weight,
                            @"packageMaxUnit":self.Itinerar3rdStepObj.Maximum_Unit,
                            @"packageType":self.Itinerar3rdStepObj.PackageType,
                            @"pickupPoint":pickPickUpPoint,
                            @"pickupPremium":[_imgPickPremSer.image isEqual:_select]?@"1":@"0",
                            @"pickupMeetingPoint":[_imgPickMtPt.image isEqual:_select]?@"1":@"0",
                            @"pickupMPAddress":pickupMPAddress,
                            @"pickupMPLatitude":pickupMPLatitude,
                            @"pickupMPLongitude":pickupMPLongitude,
                            @"dropoffPoint":deliveryPickUpPoint,
                            @"dropoffPremium":[_imgDelvryPremSer.image isEqual:_select]?@"1":@"0",
                            @"dropoffMeetingPoint":[_imgDelvryMtPt.image isEqual:_select]?@"1":@"0",
                            @"dropoffMPAddress":dropoffMPAddress,
                            @"dropoffMPLatitude":dropoffMPLatitude,
                            @"dropoffMPLongitude":dropoffMPLongitude
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kAddItinerary
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
               if ([GlobalInfo checkStatusSuccess:responseObject]) {
                   AddItineraryCompletionPopUpViewController *popUp = [[AddItineraryCompletionPopUpViewController alloc] initWithNibName:@"AddItineraryCompletionPopUpViewController" bundle:nil];
                   [self addChildViewController:popUp];
                   popUp.view.frame = self.view.frame;
                   [self.view addSubview:popUp.view];
               }
            }
           failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
           }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
