//
//  AddTripStepTwoController.m
//  Angeler
//
//  Created by AJAY on 02/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

/*According to new design addItinertStepThree + AddItineraryCompletionPopUpViewController => Not needed */

#import "AddTripStepTwoController.h"
#import "RangeSlider.h"
#import "AddItineryStepThreeController.h"
#import "SingleLocationMapController.h"
#import "AddressClass.h"
#import "MeetPopUpViewController.h"
#import "MyTripsListViewController.h"

@interface AddTripStepTwoController ()<UIPickerViewDelegate,UIPickerViewDataSource,SingleLocationAddressDelegate>

@property (nonatomic, assign) NSInteger wtCounter;
@property (nonatomic, assign) NSInteger unitCounter;
@property (nonatomic, assign) NSInteger packageType;
@property (nonatomic,strong) AddressClass *pickMeetPointAddress;
@property (nonatomic,strong) AddressClass *deliveryMeetPointAddress;


@property (nonatomic,strong) ItineraryClass *ItinerarObjStep2;
@property (weak, nonatomic) IBOutlet UILabel *lblAddTrip;
@end

@implementation AddTripStepTwoController
@synthesize topBarHeightConstraint,topNavbarView,mainContentView,viewDocument,viewPickUpLocation,viewDeliveryLocation,deliveryLocationTitleLabel,documentTitleLabel,pickUpLocationTitleTitleLabel,txtDocument,addTripView,pickUpMeetPointImageview,deliveryMeetPointImageview;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ItinerarObjStep2 = self.Itinerar2ndStepObj;
    [self initialSetUp];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    _wtCounter = [self.ItinerarObjStep2.Weight integerValue];
    _unitCounter = [self.ItinerarObjStep2.Maximum_Unit integerValue];
    _packageType = [self.ItinerarObjStep2.PackageType integerValue];
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:NSLocalizedString(@"Add_your_trip", nil) ];
    [mainContentView makeCornerRound:15.0];
    [viewPickUpLocation makeCornerRound:15.0];
    [viewDeliveryLocation makeCornerRound:15.0];
    [addTripView makeCornerRound:15.0];
    [viewDocument makeCornerRound:20.0 withBoarderColor:[UIColor lightGrayColor] borderWidth:1.0];
    
    [addTripView dropShadowWithColor:[UIColor lightishGrayColor]];
    //Document Picker Set
    
    [txtDocument setInputView:[Helper pickerWithTag:PACKAGE_TYPE forControler:self]];
    [txtDocument setInputAccessoryView:[Helper toolbarWithTag:PACKAGE_TYPE forControler:self]];
    
    
    //Pick Up delivery address set
    
    _pickMeetPointAddress =  [[AddressClass alloc] init];
    _pickMeetPointAddress.lat_Value = self.ItinerarObjStep2.from_Address.lat_Value;
    _pickMeetPointAddress.long_Value = self.ItinerarObjStep2.from_Address.long_Value;
    
    
    _deliveryMeetPointAddress =  [[AddressClass alloc] init];
    _deliveryMeetPointAddress.lat_Value = self.ItinerarObjStep2.to_Address.lat_Value;
    _deliveryMeetPointAddress.long_Value = self.ItinerarObjStep2.to_Address.long_Value;
    
    
    //Package Type By default set
    
    txtDocument.text = [GlobalInfo TypeOfPackage:_packageType];
    
}

-(void)setLocalization
{
    pickUpLocationTitleTitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"In_MeetMeAt_Sender", nil), self.ItinerarObjStep2.from_Address.cityName];
    deliveryLocationTitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"In_MeetMeAt_Recipient", nil), self.ItinerarObjStep2.to_Address.cityName];
    documentTitleLabel.text = NSLocalizedString(@"I_Choose_To_Carry", nil);
    _lblAddTrip.text = [NSLocalizedString(@"Add_Trip", nil) uppercaseString];
}

-(void)setGesture{
    
    UITapGestureRecognizer *pickUpViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickUpLocationViewTapped:)];
    UITapGestureRecognizer *deliveryViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deliveryLocationViewTapped:)];
    UITapGestureRecognizer *addTripViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addTripViewTapped:)];
    UITapGestureRecognizer *senderPopUpTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(senderPopUpTapGesture:)];
     UITapGestureRecognizer *receieverPopUpTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receieverPopUpTapGesture:)];
    
    [self.viewPickUpLocation addGestureRecognizer:pickUpViewTapRecognizer];
    [self.viewDeliveryLocation addGestureRecognizer:deliveryViewTapRecognizer];
    [self.addTripView addGestureRecognizer:addTripViewTapRecognizer];
    [self.pickUpMeetPointImageview addGestureRecognizer:senderPopUpTapRecognizer];
    [self.deliveryMeetPointImageview addGestureRecognizer:receieverPopUpTapRecognizer];
    
}

- (void)showSingleAddressMap:(AddressClass*)address andTag:(TAG_VALUE)tag{
    SingleLocationMapController *mapObj = [[SingleLocationMapController alloc] initWithNibName:@"SingleLocationMapController" bundle:nil];
    mapObj.delegate = self;
    mapObj.prevlat = address.lat_Value;
    mapObj.prevLong = address.long_Value;
    mapObj.tag = tag;
    [self.navigationController pushViewController:mapObj animated:YES];
}


- (BOOL)ValidationPassed{
    
    if (_packageType == 0) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Package_Category_Choose_Alert", nil)];
        return NO;
    }
    if ([_pickMeetPointAddress.Address isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:[NSString stringWithFormat:NSLocalizedString(@"Select_MeetingPoint_Address_Alert", nil), self.ItinerarObjStep2.from_Address.cityName]];
        return NO;
    }
    if ([_deliveryMeetPointAddress.Address isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:[NSString stringWithFormat:NSLocalizedString(@"Select_MeetingPoint_Address_Alert", nil), self.ItinerarObjStep2.to_Address.cityName]];
        return NO;
    }
    return YES;
    
}

-(void)showPopUpWithTitle:(NSString*)title
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MeetPopUpViewController *meetPopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"MeetPopUpVCID"];
    
    meetPopUpVC.headerTitleText = NSLocalizedString(@"AddTrip_MeetingPoint", nil);
    meetPopUpVC.bottomTitleText = title;
    meetPopUpVC.bottomDescriptionText = NSLocalizedString(@"AddTrip_Meetpoint_Sender_Message", nil);
    meetPopUpVC.singleActionLabelTitleText = NSLocalizedString(@"OK", nil);
    meetPopUpVC.imageIconName = @"MeetingPoint_Blue_icon";
    
    self.definesPresentationContext = YES; //self is presenting view controller
    meetPopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    meetPopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:meetPopUpVC animated:YES completion:nil];
    
}

#pragma mark - Gesture Action


- (void)pickUpLocationViewTapped:(UITapGestureRecognizer*)sender
{
    [self showSingleAddressMap:_pickMeetPointAddress andTag:PICK_MEET_POINT_ADDRESS];
}

- (void)deliveryLocationViewTapped:(UITapGestureRecognizer*)sender
{
      [self showSingleAddressMap:_deliveryMeetPointAddress andTag:DELIVERY_MEET_POINT_ADDRESS];
}

- (void)senderPopUpTapGesture:(UITapGestureRecognizer*)sender
{
    [self showPopUpWithTitle:NSLocalizedString(@"Meet_The_Sender_Receiver", nil)];
}

- (void)receieverPopUpTapGesture:(UITapGestureRecognizer*)sender
{
    [self showPopUpWithTitle:NSLocalizedString(@"Meet_The_Sender_Receiver", nil)];
}


- (void)addTripViewTapped:(UITapGestureRecognizer*)sender
{
    self.ItinerarObjStep2.PackageType = [NSString stringWithFormat:@"%ld",_packageType];
    self.ItinerarObjStep2.Weight = [NSString stringWithFormat:@"%ld",_wtCounter];
    self.ItinerarObjStep2.Maximum_Unit = [NSString stringWithFormat:@"%ld",_unitCounter];
    
    if ([self ValidationPassed])
    {
        [self callServiceToAddTrip];

    }
}



#pragma mark-SingleAddressDelegate
- (void)selectedAddress:(AddressClass*)address forTag:(TAG_VALUE)tag
{
    if (tag == PICK_MEET_POINT_ADDRESS) {
       
        _lblPickUpLocation.text = address.Address;
        _pickMeetPointAddress.Address = address.Address;
        _pickMeetPointAddress.lat_Value = address.lat_Value;
        _pickMeetPointAddress.long_Value = address.long_Value;
    }
    if (tag == DELIVERY_MEET_POINT_ADDRESS) {
        _lblDeliveryLocation.text = address.Address;
        _deliveryMeetPointAddress.Address = address.Address;
        _deliveryMeetPointAddress.lat_Value = address.lat_Value;
        _deliveryMeetPointAddress.long_Value = address.long_Value;
    }
}




#pragma mark - Picker Action

- (void)pickerDoneTapped:(UIButton*)sender{

    if (sender.tag == PACKAGE_TYPE) {
        UIPickerView *piker = (UIPickerView*)txtDocument.inputView;
        txtDocument.text = [GlobalInfo TypeOfPackage:[piker selectedRowInComponent:0]];
        _packageType = [piker selectedRowInComponent:0];
        
    }
    [self.view endEditing:YES];
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 4;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [GlobalInfo TypeOfPackage:row];
}

#pragma mark - Service Call
- (void)callServiceToAddTrip{
    
    NSDictionary *param = @{@"departureCountry":self.ItinerarObjStep2.from_Address.countryId,
                            @"pickupMPCountry":self.ItinerarObjStep2.from_Address.countryId,
                            @"departureCity":self.ItinerarObjStep2.from_Address.cityId,
                            @"pickupMPCity":self.ItinerarObjStep2.from_Address.cityId,
                            @"arrivalCountry":self.ItinerarObjStep2.to_Address.countryId,
                            @"dropoffMPCountry":self.ItinerarObjStep2.to_Address.countryId,
                            @"arrivalCity":self.ItinerarObjStep2.to_Address.cityId,
                            @"dropoffMPCity":self.ItinerarObjStep2.to_Address.cityId,
                            @"departureDate":[[self.ItinerarObjStep2.Departing_Date_Time componentsSeparatedByString:@" "] firstObject],
                            @"departureTime":[[self.ItinerarObjStep2.Departing_Date_Time componentsSeparatedByString:@" "] lastObject],
                            @"arrivalDate":[[self.ItinerarObjStep2.Arrival_Date_Time componentsSeparatedByString:@" "] firstObject],
                            @"arrivalTime":[[self.ItinerarObjStep2.Arrival_Date_Time componentsSeparatedByString:@" "] lastObject],
                            @"travelMode":[NSString stringWithFormat:@"%ld",(long)self.ItinerarObjStep2.travel_Mode],
                            @"travelRefNo":self.ItinerarObjStep2.Travel_Ref_No,
                            @"packageHeightMin":zeroUnit,
                            @"packageHeightMax":zeroUnit,
                            @"packageWidthMin":zeroUnit,
                            @"packageWidthMax":zeroUnit,
                            @"packageLengthMin":zeroUnit,
                            @"packageLengthMax":zeroUnit,
                            @"packageMaxWeight":zeroUnit,
                            @"packageMaxUnit":zeroUnit,
                            @"packageType":self.ItinerarObjStep2.PackageType,
                            @"pickupPoint":zeroUnit,
                            @"pickupPremium":OneUnit,
                            @"pickupMeetingPoint":OneUnit,
                            @"pickupMPAddress":_pickMeetPointAddress.Address,
                            @"pickupMPLatitude":[_pickMeetPointAddress.lat_Value stringValue],
                            @"pickupMPLongitude":[_pickMeetPointAddress.long_Value stringValue],
                            @"dropoffPoint":zeroUnit,
                            @"dropoffPremium":OneUnit,
                            @"dropoffMeetingPoint":OneUnit,
                            @"dropoffMPAddress":_deliveryMeetPointAddress.Address,
                            @"dropoffMPLatitude":[_deliveryMeetPointAddress.lat_Value stringValue],
                            @"dropoffMPLongitude":[_deliveryMeetPointAddress.long_Value stringValue]
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kAddItinerary
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                               
                                                               MyTripsListViewController *managObj = [[MyTripsListViewController alloc] initWithNibName:@"MyTripsListViewController" bundle:nil];
                                                               AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
                                                           } ];
                                                           [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Trip_added_successful_Alert", nil) actions:[NSArray arrayWithObject:cancel]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}

@end


// ========================== Extra + Reuse Old code ===========================

//[Helper addShadowToLayer:_headerView.layer];
//[self callServiceToFetchRate];

/*- (void)callServiceToFetchRate{
 //os, osVersion, appVersion, userId, sessionId, fromCountryId, toCountryId, fromCityId, toCityId
 NSDictionary *param = @{@"fromCountryId":self.ItinerarObjStep2.from_Address.countryId,
 @"fromCityId":self.ItinerarObjStep2.from_Address.cityId,
 @"toCountryId":self.ItinerarObjStep2.to_Address.countryId,
 @"toCityId":self.ItinerarObjStep2.to_Address.cityId};
 //=======Call WebService Engine========
 [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryRateTable
 Method:REQMETHOD_GET
 parameters:[GlobalInfo getParmWithSession:param]
 media:nil
 viewController:self
 success:^(id responseObject){
 NSLog(@"response ===   %@",responseObject);
 
 NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
 // if ([GlobalInfo checkStatusSuccess:responseObject]) {
 if (statusCode == 200){
 _dataBasePrice = [responseObject objectForKey:@"responseData"];
 self.lblTotalAmount.text = [self formatApproxEarning];
 [[self.lblTotalAmount superview] superview].hidden = NO;
 }else{
 [[self.lblTotalAmount superview] superview].hidden = YES;
 }
 }
 failure:^(id responseObject){
 //[Helper showAlertWithTitle:Empty Message:strKey_error_wentwrong];
 }];
 
 } */


/* self.lblMaxWeight.text = [NSString stringWithFormat:@"%ldkg",(long)_wtCounter];
 self.lblMaxNoPackage.text = [NSString stringWithFormat:@"%ld",(long)_unitCounter];
 self.txtDocument.text = [GlobalInfo TypeOfPackage:_packageType];
 
 self.txtHeight.text = [[self.ItinerarObjStep2.Size_Height_Min_Max componentsSeparatedByString:@","] lastObject];
 self.txtWidth.text = [[self.ItinerarObjStep2.Size_Width_Min_Max componentsSeparatedByString:@","] lastObject];
 self.txtLength.text = [[self.ItinerarObjStep2.Size_Length_Min_Max componentsSeparatedByString:@","] lastObject];
 
 
 self.lblMessage.attributedText = [self formatFinalMessage];
 self.lblTotalAmount.text = [self formatApproxEarning]; */

/* - (IBAction)backTapped:(id)sender{
 //[self.delegate passItineryObjFromTwo:self.ItinerarObj];
 [self.navigationController popViewControllerAnimated:YES];
 }
*/


//#pragma mark - ItineryStepThreeDelegate
//- (void)passItineryObjFromThree:(ItineraryClass *)itineryObj{
//   // self.ItinerarObj = itineryObj;
//}


/* #pragma mark - UitextField Delegate
 - (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
 if ([textField isEqual:_txtLength] || [textField isEqual:_txtWidth] || [textField isEqual:_txtHeight]) {
 NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
 NSInteger length = [currentString length];
 if (length > 3) {
 return NO;
 }
 }
 return YES;
 }
 
 - (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
 if ([textField isEqual:_txtLength] || [textField isEqual:_txtWidth] || [textField isEqual:_txtHeight]) {
 if (textField.text.length == 0) {
 return NO;
 }
 }
 return YES;
 }
*/

/*   self.ItinerarObjStep2.Size_Height_Min_Max = [NSString stringWithFormat:@"%@,%@",OneUnit,_txtHeight.text];
 self.ItinerarObjStep2.Size_Width_Min_Max = [NSString stringWithFormat:@"%@,%@",OneUnit,_txtWidth.text];
 self.ItinerarObjStep2.Size_Length_Min_Max = [NSString stringWithFormat:@"%@,%@",OneUnit,_txtLength.text];*/
/*- (BOOL)validatePackageSize{
 if ([_txtHeight.text integerValue] > 100) {
 [Helper showAlertWithTitle:Empty Message:@"Height should not be more than 100 cm"];
 return NO;
 }
 if ([_txtWidth.text integerValue] > 100) {
 [Helper showAlertWithTitle:Empty Message:@"Width should not be more than 100 cm"];
 return NO;
 }
 if ([_txtLength.text integerValue] > 100) {
 [Helper showAlertWithTitle:Empty Message:@"Length should not be more than 100 cm"];
 return NO;
 }
 return YES;
 }
 */


/*- (IBAction)nextTapped:(id)sender{
 if ([self validatePackageSize]) {
 
 self.ItinerarObjStep2.PackageType = [NSString stringWithFormat:@"%ld",_packageType];
 self.ItinerarObjStep2.Weight = [NSString stringWithFormat:@"%ld",_wtCounter];
 self.ItinerarObjStep2.Maximum_Unit = [NSString stringWithFormat:@"%ld",_unitCounter];
 AddItineryStepThreeController *nxtStep = [[AddItineryStepThreeController alloc] initWithNibName:@"AddItineryStepThreeController" bundle:nil];
 nxtStep.Itinerar3rdStepObj = self.ItinerarObjStep2;
 [self.navigationController pushViewController:nxtStep animated:YES];
 }
 }
 
 */
