//
//  ItineraryRequestListCell.h
//  Angeler
//
//  Created by AJAY on 21/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundImageView.h"

@interface ItineraryRequestListCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UIView *roundView;
@property (weak, nonatomic) IBOutlet UILabel *lblPickup;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOff;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UIButton *btnStatusInfo;
//@property (weak, nonatomic) IBOutlet UIButton *btnPackInfoCentered;
//@property (weak, nonatomic) IBOutlet UIButton *btnConfirmDeny;

@property (weak, nonatomic) IBOutlet UILabel *lblDocument;
//@property (weak, nonatomic) IBOutlet UIImageView *imgDocument;

//@property (weak, nonatomic) IBOutlet UIImageView *imgTravelMode;
//@property (weak, nonatomic) IBOutlet UILabel *lblTravelRefNo;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *meetPremiumImageIcon;
@property (weak, nonatomic) IBOutlet RoundImageView *customerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *deliveredImageView;

//@property (weak, nonatomic) IBOutlet UIImageView *imgFromType;
//@property (weak, nonatomic) IBOutlet UIImageView *imgToType;


@end
