//
//  MyTripsListViewController.m
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "MyTripsListViewController.h"
#import "AddTripStepOneController.h"
#import "ItineraryCell.h"
#import "ItineraryRequestListController.h"
#import "ItineraryInfoController.h"
#import "ItineraryPackageInfoController.h"
#import "SharePopUpViewController.h"
#import "VerifyAccountViewController.h"

static NSString *CellIdentifier = @"itineraryCell";



@interface MyTripsListViewController ()<SharePopUpDismissDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
}
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tblItinerary;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;

@property (nonatomic, strong) NSMutableArray *arrItineraryList;


@end

@implementation MyTripsListViewController
@synthesize topNavbarView,topBarHeightConstraint;

#pragma mark - ViewLifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [Helper addShadowToLayer:_headerView.layer];
    // Do any additional setup after loading the view from its nib.
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [_tblItinerary registerNib:[UINib nibWithNibName:@"ItineraryCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    // Do any additional setup after loading the view from its nib.
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblItinerary addSubview:refreshController];
    
    _tblItinerary.estimatedRowHeight = 20.0;
    _tblItinerary.rowHeight = UITableViewAutomaticDimension;
    
    [self initialSetUp];
}
- (void)handleRefresh:(id)refreshController
{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadItineraryList];
    [refreshController endRefreshing];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [self callServiceToLoadItineraryList];
//    [self showPendingAccountAlertPopUp];
}



-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavMenuBtn];
    [topNavbarView showPlusBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"My Trips", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Trip_Msg", nil);
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self.tblItinerary addGestureRecognizer:gr];
    
   
    
}

#pragma mark - Gesture Action

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    
    for (int row = 0; row < _arrItineraryList.count ; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        ItineraryCell *cell = (ItineraryCell *)[_tblItinerary cellForRowAtIndexPath:indexPath];
        cell.menu_Container.hidden = YES;
        
    }
    
}


#pragma mark - Webservice Call Method
//New Method for Pagination
- (void)callServiceToLoadItineraryList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           NSArray *arr = [responseObject objectForKey:@"responseData"];
           if ((arr.count == 0) && (START_POSITION != 0)){
               moreRecordsAvailable = NO;
               return ;
           }
           if ([GlobalInfo checkStatusSuccess:responseObject])  {
           
               if (_arrItineraryList.count > 0) {
                   if (START_POSITION == 0) {
                       [_arrItineraryList removeAllObjects];
                   }else if(_arrItineraryList.count > START_POSITION){
                       [_arrItineraryList removeObjectsInRange:NSMakeRange(START_POSITION, _arrItineraryList.count - START_POSITION)];
                   }
                   [_arrItineraryList addObjectsFromArray:arr];
               }else{
                   _arrItineraryList = [NSMutableArray arrayWithArray:arr];
               }
               START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
               _lblNoRecord.hidden = _arrItineraryList.count > 0;
               [_tblItinerary reloadData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
    
}
- (IBAction)addItinerary:(id)sender{
    AddTripStepOneController *delObj = [[AddTripStepOneController alloc] initWithNibName:@"AddTripStepOneController" bundle:nil];
    [self.navigationController pushViewController:delObj animated:YES];
    
}

#pragma mark - Share Pop Up show

-(void)showNotVerifiedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_NotVerified_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_NotVerified_Message", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showPendingAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];

    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_Pending_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Pending_Message", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showRejectedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_Verification_Rejected_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Verification_Rejected_Msg", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}


#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    
    VerifyAccountViewController *verifyAccountObj = [[VerifyAccountViewController alloc] initWithNibName:@"VerifyAccountViewController" bundle:nil];
    [self.navigationController pushViewController:verifyAccountObj animated:YES];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrItineraryList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItineraryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *data = [_arrItineraryList objectAtIndex:indexPath.row];
    
    
    cell.lblJourneyDetials.text = [NSString stringWithFormat:@"%@ %@ %@",[data objectForKey:@"departureCityName"],NSLocalizedString(@"To", nil),[data objectForKey:@"arrivalCityName"]];

    cell.lblDepart.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Departure", nil),[Helper getDateTimeToStringWithDate:[data objectForKey:@"departureDate"] Time:[data objectForKey:@"departureTime"]]];
    
    cell.lblRequest.text = [NSString stringWithFormat:@"%@ request >",[data objectForKey:@"request_count"]];
    
    cell.btnDelete.tag = indexPath.row + 1;
    [cell.btnDelete addTarget:self action:@selector(deleteItinerary:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnShowTripDetails.tag = indexPath.row + 1;
    [cell.btnShowTripDetails addTarget:self action:@selector(ShowTripDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSDictionary * reqDic = [data objectForKey:@"request_details"];
    if ([reqDic objectForKey:@"senderName"])
    {
       
        cell.requestPriceView.hidden = NO;
        cell.reqPriceViewHeightConstraint.constant = 40;
        cell.requestPriceViewBottomConstraint.constant = 20;
        cell.lblRequest.text = [NSString stringWithFormat:NSLocalizedString(@"New_Request_From﻿", nil), [[data objectForKey:@"request_details"] objectForKey:@"senderName"]];
        cell.priceLabel.text = [[data objectForKey:@"request_details"] objectForKey:@"requestCost"];
     
         [cell.senderProfileImageView sd_setImageWithURL:[NSURL URLWithString:[reqDic objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
        UITapGestureRecognizer *newReqViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newReqViewTapped:)];
        newReqViewTap.numberOfTapsRequired = 1;
        [cell.requestPriceView addGestureRecognizer:newReqViewTap];
        cell.requestPriceView.tag = indexPath.row + 1;
    }
    else
    {
        cell.btnDelete.hidden = NO;
        cell.requestPriceView.hidden = YES;
        cell.reqPriceViewHeightConstraint.constant = 0;
        cell.requestPriceViewBottomConstraint.constant = 0;
    }
    
    NSString *parcelCount = [data objectForKey:@"packagesAcceptedCount"];
    
    if ([parcelCount intValue] > 0)
    {
        cell.btnDelete.hidden = YES;
        cell.btnShowTripDetails.hidden = NO;
       
        if([[data objectForKey:@"allDelivered"] boolValue] == YES){
           cell.lblParcel.textColor = [UIColor colorWithHexString:@"68B41B"];
        }
        else{
            cell.lblParcel.textColor = [UIColor redColor];
        }
        
        [cell.lblParcel setUserInteractionEnabled:YES];
    }
    else
    {
        cell.btnDelete.hidden = NO;
        cell.btnShowTripDetails.hidden = NO;
        cell.lblParcel.textColor = [UIColor blackColor];
        [cell.lblParcel setUserInteractionEnabled:NO];
        
    }
    
   cell.lblParcel.text = [[NSString stringWithFormat:NSLocalizedString(@"Carrying_Item", nil),[data objectForKey:@"packagesAcceptedCount"]] uppercaseString];
   

    UITapGestureRecognizer *tripReqListTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ShowItineraryRequestList:)];
    tripReqListTap.numberOfTapsRequired = 1;
    [cell.lblParcel addGestureRecognizer:tripReqListTap];
    cell.lblParcel.tag = indexPath.row + 1;
    
//    cell.btnRequest.tag = indexPath.row + 1;
//    [cell.btnRequest addTarget:self action:@selector(ShowItineraryList:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.menu_Container.hidden = YES;
    
    
    cell.btnMenu.tag = indexPath.row + 1;
    [cell.btnMenu addTarget:self action:@selector(menuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
   //Load More Data
   
    if (indexPath.row == [self.arrItineraryList count] - 1 && moreRecordsAvailable)
    {
        [self callServiceToLoadItineraryList];
    }
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}



#pragma mark - Table view Action
-(void)menuClicked:(UIButton*)sender
{
    for (int row = 0; row < _arrItineraryList.count ; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        ItineraryCell *cell = (ItineraryCell *)[_tblItinerary cellForRowAtIndexPath:indexPath];
        if (row == sender.tag - 1)
        {
            cell.menu_Container.hidden = !cell.menu_Container.hidden;
        }
        else
        {
            cell.menu_Container.hidden = YES;
        }
    }
}

- (void)newReqViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *newReqView = (UIView *)sender.view;
    START_POSITION = newReqView.tag - 1;
    ItineraryPackageInfoController *next = [[ItineraryPackageInfoController alloc] initWithNibName:@"ItineraryPackageInfoController" bundle:nil];
    next.bookingId = [[[_arrItineraryList objectAtIndex:newReqView.tag - 1] objectForKey:@"request_details"] objectForKey:@"bookingId"];

    [self.navigationController pushViewController:next animated:YES];
}

/*- (void)editTrip:(UIButton*)sender {
  
    AddTripStepOneController *manageObj = [[AddTripStepOneController alloc] initWithNibName:@"AddTripStepOneController" bundle:nil];
    [self.navigationController pushViewController:manageObj animated:YES];
}*/

- (void)ShowItineraryRequestList:(UITapGestureRecognizer*)sender {
    
    UILabel *lblCarryingParcel = (UILabel *)sender.view;
    if ([[[_arrItineraryList objectAtIndex:lblCarryingParcel.tag-1] objectForKey:@"request_count"] integerValue] == 0) {
        return;
    }
    START_POSITION = lblCarryingParcel.tag-1;
    ItineraryRequestListController *reqObj = [[ItineraryRequestListController alloc] initWithNibName:@"ItineraryRequestListController" bundle:nil];
    reqObj.itenaryData = [_arrItineraryList objectAtIndex:lblCarryingParcel.tag-1];
    [self.navigationController pushViewController:reqObj animated:YES];
}
//- (IBAction)ShowItineraryRequestList:(id)sender {
//
//    ItineraryRequestListController *reqObj = [[ItineraryRequestListController alloc] initWithNibName:@"ItineraryRequestListController" bundle:nil];
//    [self.navigationController pushViewController:reqObj animated:YES];
//}

- (void)ShowTripDetails:(UIButton*)sender{
    START_POSITION = sender.tag-1;
    ItineraryInfoController *reqObj = [[ItineraryInfoController alloc] initWithNibName:@"ItineraryInfoController" bundle:nil];
    reqObj.itenaryId = [[_arrItineraryList objectAtIndex:sender.tag-1] objectForKey:@"itineraryId"];
    [self.navigationController pushViewController:reqObj animated:YES];
    [_tblItinerary reloadData];
}

- (void)deleteItinerary:(UIButton*)sender{
    
    
    [self showTripDeleteAlertWithIndex:sender.tag-1];
    
}

-(void)showTripDeleteAlertWithIndex:(NSInteger)selectedIndex
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:NSLocalizedString(@"Delete_Trip_Alert", nil)
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   [self deleteTripApiCallWithIndex:selectedIndex];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       /** What we write here???????? **/
                                       NSLog(@"you pressed No, thanks button");
                                       // call method whatever u need
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)deleteTripApiCallWithIndex:(NSInteger)selectedIndex
{
    /*
     userId, sessionId, os, osVersion, appVersion, itineraryId
     
     Request example :
     http://103.15.232.68/~angeler/web_services/itinerary/delete_itinerary?userId=17&sessionId=8dc3535b3a1f36d0343d4664d53d693fe6f16ab4&os=Android&osVersion=M&appVersion=1.0&itineraryId=6
     */
    
    NSDictionary *param = @{@"itineraryId":[[_arrItineraryList objectAtIndex:selectedIndex] objectForKey:@"itineraryId"]
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kitineraryDelete
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           [_arrItineraryList removeObjectAtIndex:selectedIndex];
                                                           [_tblItinerary reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//#pragma mark - Scrollview Delegate
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//
//    // UITableView only moves in one direction, y axis
//    CGFloat currentOffset = scrollView.contentOffset.y;
//    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//
//    //NSInteger result = maximumOffset - currentOffset;
//
//    // Change 10.0 to adjust the distance from bottom
//    if (maximumOffset - currentOffset <= 5.0) {
//        if(moreRecordsAvailable)
//        {
//            [self callServiceToLoadItineraryList];
//        }
//    }
//}
@end
