//
//  AddTripStepOneController.m
//  Angeler
//
//  Created by AJAY on 01/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "AddTripStepOneController.h"
#import "MyTripsListViewController.h"
#import "PickerInfoViewController.h"
#import "AddTripStepTwoController.h"
#import "TypeOfTransportCollectionViewCell.h"

static NSString *typeOfTransportIdentifier = @"TypeOfTransportCollectionViewCellID";

@interface AddTripStepOneController ()<PickerInfoDelegate,ItineryStepTwoDelegate>
{
    NSArray *typeTransportImgArr;
   // CGFloat collectionviewItemHeight;
    
}



@property (nonatomic,strong) ItineraryClass *ItinerarObjStep1;
@end

@implementation AddTripStepOneController
@synthesize topBarHeightConstraint,topNavbarView,mainContentView,viewFromCity,viewFromDateTime,viewToCity,viewToDateTime,typeOfTransportCollectionView,referenceNumberTextfield,viewReferenceNumber,fromDateTimeTitleLabel,fromSelectCityTitleLabel,fromDateTimeLocationTitlelabel,toDateTimeTitleLabel,toDateTimeValueTextfield,toSelectCityTitleLabel,toSelectCityValueTextfield,toDateTimeLocationTitlelabel,modeOfTravelTitleLabel,travelReferenceNumberTitleLabel,fromSelectCityValueTextfield,fromDateTimeValueTextfield,nextView,nextImageView;


#pragma mark - ViewLifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ItinerarObjStep1 = [[ItineraryClass alloc] init];
    // Do any additional setup after loading the view from its nib,
    [self initialSetUp];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:false];
  //  [self setCollectionViewFlowLayout];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:NSLocalizedString(@"Add_your_trip", nil)];
    [topNavbarView dropShadowWithColor:[UIColor lightGrayColor]];
    [mainContentView makeCornerRound:15.0];
    [viewFromCity makeCornerRound:15.0];
    [viewFromDateTime makeCornerRound:15.0];
    [viewToCity makeCornerRound:15.0];
    [viewToDateTime makeCornerRound:15.0];
    [viewReferenceNumber makeCornerRound:15.0];
    
    
    [self.typeOfTransportCollectionView registerNib:[UINib nibWithNibName:@"TypeOfTransportCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:typeOfTransportIdentifier];
    typeTransportImgArr = [[NSArray alloc] initWithObjects:@"AT_Aeroplane",nil];
    //,@"AT_Car",@"AT_Cruize",@"AT_Bus"@"AT_Ship"
    
    //PickerView Set Up
    
    [fromDateTimeValueTextfield setInputView:[Helper datePickerWithTag:DEPRT_DATE_TIME]];
    [fromDateTimeValueTextfield setInputAccessoryView:[Helper toolbarWithTag:DEPRT_DATE_TIME forControler:self]];
    
    [toDateTimeValueTextfield setInputView:[Helper datePickerWithTag:ARRIVAL_DATE_TIME]];
    [toDateTimeValueTextfield setInputAccessoryView:[Helper toolbarWithTag:ARRIVAL_DATE_TIME forControler:self]];
    
    fromDateTimeLocationTitlelabel.text = @"";
    toDateTimeLocationTitlelabel.text = @"";
    
    //Travel Mode Aeroplane Set Always
    
    self.ItinerarObjStep1.travel_Mode = TRAVEL_MODE_FLIGHT;
}

-(void)setGesture{
   
    UITapGestureRecognizer *fromCityViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fromSelectCityTapped:)];
    UITapGestureRecognizer *toCityViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSelectCityTapped:)];
    [self.viewFromCity addGestureRecognizer:fromCityViewTapRecognizer];
    [self.viewToCity addGestureRecognizer:toCityViewTapRecognizer];
    
    UITapGestureRecognizer *nextViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextViewTapped:)];
    [self.nextView addGestureRecognizer:nextViewTapRecognizer];
}



-(void)setLocalization
{
   
    UIColor *color = [UIColor blackColor];
    fromSelectCityTitleLabel.text = NSLocalizedString(@"From", nil);
    fromSelectCityValueTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Select_City", nil) attributes:@{NSForegroundColorAttributeName: color}];
    fromDateTimeTitleLabel.text = NSLocalizedString(@"On", nil);
    fromDateTimeValueTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Select_DateTime", nil) attributes:@{NSForegroundColorAttributeName: color}];
    
    
    
    toSelectCityTitleLabel.text = NSLocalizedString(@"I_Am_Going_To", nil);
    toSelectCityValueTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Select_City", nil) attributes:@{NSForegroundColorAttributeName: color}];
    toDateTimeTitleLabel.text = NSLocalizedString(@"On", nil);
    toDateTimeValueTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Select_DateTime", nil) attributes:@{NSForegroundColorAttributeName: color}];
    

    modeOfTravelTitleLabel.text = NSLocalizedString(@"ModuleTrip_ModeOfTravel", nil);
    travelReferenceNumberTitleLabel.text = NSLocalizedString(@"Flight_Number", nil);
     self.lblNext.text =  NSLocalizedString(@"Next", nil);
    
}

-(void)setCollectionViewFlowLayout
{
   

   // For more than one element in array comment out this code
    
//    CGFloat collectionViewWidth = typeOfTransportCollectionView.frame.size.width;
//    CGFloat collectionViewHeight = typeOfTransportCollectionView.frame.size.height;
//    collectionviewItemHeight = collectionViewHeight * 0.91;
//
//    NSLog(@"collectionviewItemHeight %f",collectionviewItemHeight);
//    typeOfTransportCollectionView.layer.borderColor = [[UIColor redColor] CGColor];
//    typeOfTransportCollectionView.layer.borderWidth = 3.0;
    
  //  CGFloat totalItemWidth = collectionviewItemHeight * typeTransportImgArr.count;
    
    // CGFloat minimumSpace = (collectionViewWidth - totalItemWidth)/(typeTransportImgArr.count - 1);
    
//    NSLog(@"minimumSpace %f",minimumSpace);
//    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    layout.minimumInteritemSpacing = minimumSpace;
//    layout.minimumLineSpacing = 0;
//    typeOfTransportCollectionView.collectionViewLayout = layout;
}

- (BOOL)ValidationPassed{
    if ([self.ItinerarObjStep1.from_Address.countryId isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_From_city_selection_alert", nil)];
        return NO;
    }
    
    if ([self.ItinerarObjStep1.Departing_Date_Time isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_From_DateTime_selection_alert", nil)];
        return NO;
    }
    
    if ([self.ItinerarObjStep1.to_Address.countryId isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_To_city_selection_alert", nil)];
        return NO;
    }
    
    if ([self.ItinerarObjStep1.Arrival_Date_Time isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_To_DateTime_selection_alert", nil)];
        return NO;
    }
    
//    if ([self.ItinerarObjStep1.from_Address.cityId isEqualToString:self.ItinerarObjStep1.to_Address.cityId]) {
//
//        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_Different_City_selection_alert", nil)];
//        return NO;
//    }
//
    if([self.ItinerarObjStep1.Travel_Ref_No isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_FlightNo_EmptyAlert", nil)];
        return NO;
    }
    
    return YES;
}

-(BOOL)isSameCountryChoose
{
    if ([self.ItinerarObjStep1.from_Address.countryId isEqualToString:self.ItinerarObjStep1.to_Address.countryId]) {
        
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Different_Country_selection_alert", nil)];
        return YES;
    }
    return NO;
}

-(void)clearDepartureObjectData
{
    self.ItinerarObjStep1.from_Address.countryName = Empty;
    self.ItinerarObjStep1.from_Address.countryId = Empty;
    self.ItinerarObjStep1.from_Address.countryCode = Empty;
    self.ItinerarObjStep1.from_Address.countryIsdCode = Empty;
    self.ItinerarObjStep1.from_Address.cityName = Empty;
    self.ItinerarObjStep1.from_Address.cityId = Empty;
    self.ItinerarObjStep1.from_Address.cityCode = Empty;
    self.ItinerarObjStep1.from_Address.lat_Value = [NSNumber numberWithInt:0];
    self.ItinerarObjStep1.from_Address.long_Value = [NSNumber numberWithInt:0];
}

-(void)clearDestinationObjectData
{
    self.ItinerarObjStep1.to_Address.countryName = Empty;
    self.ItinerarObjStep1.to_Address.countryId = Empty;
    self.ItinerarObjStep1.to_Address.countryCode = Empty;
    self.ItinerarObjStep1.to_Address.countryIsdCode = Empty;
    
    self.ItinerarObjStep1.to_Address.cityName = Empty;
    self.ItinerarObjStep1.to_Address.cityId = Empty;
    self.ItinerarObjStep1.to_Address.cityCode = Empty;
    
    self.ItinerarObjStep1.to_Address.lat_Value = [NSNumber numberWithInt:0];
    self.ItinerarObjStep1.to_Address.long_Value = [NSNumber numberWithInt:0];
}

#pragma mark - Date & Time Picker Action

- (void)pickerDoneTapped:(UIButton*)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    if (sender.tag == DEPRT_DATE_TIME) {
        UIDatePicker *dtPic = (UIDatePicker*)fromDateTimeValueTextfield.inputView;
        fromDateTimeValueTextfield.text = [dateFormatter stringFromDate:[dtPic date]];
        [fromDateTimeValueTextfield resignFirstResponder];
        [self.ItinerarObjStep1 setDeparting_Date_Time:fromDateTimeValueTextfield.text];
    }
    if (sender.tag == ARRIVAL_DATE_TIME) {
        UIDatePicker *dtPic = (UIDatePicker*)toDateTimeValueTextfield.inputView;
        toDateTimeValueTextfield.text = [dateFormatter stringFromDate:[dtPic date]];
        [toDateTimeValueTextfield resignFirstResponder];
        [self.ItinerarObjStep1 setArrival_Date_Time:toDateTimeValueTextfield.text];
    }
}

#pragma mark - Gesture Action


- (void)nextViewTapped:(UITapGestureRecognizer*)sender
{
    self.ItinerarObjStep1.Travel_Ref_No = referenceNumberTextfield.text;
    if ([self ValidationPassed]) {
        self.ItinerarObjStep1.Travel_Ref_No = referenceNumberTextfield.text;
        AddTripStepTwoController *nxtStep = [[AddTripStepTwoController alloc] initWithNibName:@"AddTripStepTwoController" bundle:nil];
        nxtStep.Itinerar2ndStepObj = self.ItinerarObjStep1;
        [self.navigationController pushViewController:nxtStep animated:YES];
    }
}

- (void)fromSelectCityTapped:(UITapGestureRecognizer*)sender
{
    TAG_VALUE type = DEPRT_COUNTRY;
    NSString *countryId = self.ItinerarObjStep1.from_Address.countryId;
    [self showCountryCityPickerWithType:type
                             CountryID:countryId];
}

- (void)toSelectCityTapped:(UITapGestureRecognizer*)sender
{
    
    TAG_VALUE type = DEST_COUNTRY;
    NSString *countryId = self.ItinerarObjStep1.to_Address.countryId;
    [self showCountryCityPickerWithType:type
                              CountryID:countryId];
   
}

-(void)showCountryCityPickerWithType:(TAG_VALUE)type CountryID:(NSString*)countryId
{
    PickerInfoViewController *pick = [[PickerInfoViewController alloc] initWithNibName:@"PickerInfoViewController" bundle:nil];
    pick.type = type;
    pick.countryId = countryId;
    pick.delegate = self;
    [self presentViewController:pick animated:YES completion:nil];
}



#pragma mark - Button Action

- (IBAction)nextTapped:(id)sender {
    self.ItinerarObjStep1.Travel_Ref_No = referenceNumberTextfield.text;
    if ([self ValidationPassed]) {
        self.ItinerarObjStep1.Travel_Ref_No = referenceNumberTextfield.text;
        AddTripStepTwoController *nxtStep = [[AddTripStepTwoController alloc] initWithNibName:@"AddTripStepTwoController" bundle:nil];
        nxtStep.Itinerar2ndStepObj = self.ItinerarObjStep1;
        [self.navigationController pushViewController:nxtStep animated:YES];
    }
}



#pragma mark- Picker Info Delegate
- (void)selectedItem:(NSDictionary *)selectedItem andTagReference:(TAG_VALUE)tagRef{
    
    NSLog(@"----  SelectedItem ===  %@     tagRef ===  %ld",selectedItem,(long)tagRef);
    
    
    if (tagRef == DEPRT_COUNTRY){
        self.ItinerarObjStep1.from_Address.countryName = [selectedItem objectForKey:@"countryName"];
        self.ItinerarObjStep1.from_Address.countryId = [selectedItem objectForKey:@"countryId"];
        self.ItinerarObjStep1.from_Address.countryCode = [selectedItem objectForKey:@"countryCode"];
        self.ItinerarObjStep1.from_Address.countryIsdCode = [selectedItem objectForKey:@"countryIsdCode"];
        self.ItinerarObjStep1.from_Address.cityName = [selectedItem objectForKey:@"cityName"];
        self.ItinerarObjStep1.from_Address.cityId = [selectedItem objectForKey:@"cityId"];
        self.ItinerarObjStep1.from_Address.cityCode = [selectedItem objectForKey:@"cityCode"];
        self.ItinerarObjStep1.from_Address.lat_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLatitude"] doubleValue]];
        self.ItinerarObjStep1.from_Address.long_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLongitude"] doubleValue]];
        
        if([self isSameCountryChoose])
        {
            fromSelectCityValueTextfield.text = @"";
            fromDateTimeLocationTitlelabel.text = @"";
            [self clearDepartureObjectData];
        }
        else
        {
            
            fromSelectCityValueTextfield.text = [NSString stringWithFormat:@"%@, %@",self.ItinerarObjStep1.from_Address.cityName,self.ItinerarObjStep1.from_Address.countryName];
            
            fromDateTimeLocationTitlelabel.text = [NSString stringWithFormat:NSLocalizedString(@"SelectDateTime_ShowLocationMessage", nil), self.ItinerarObjStep1.from_Address.cityName];
        }
        
    }
    
    if (tagRef == DEST_COUNTRY) {
        self.ItinerarObjStep1.to_Address.countryName = [selectedItem objectForKey:@"countryName"];
        self.ItinerarObjStep1.to_Address.countryId = [selectedItem objectForKey:@"countryId"];
        self.ItinerarObjStep1.to_Address.countryCode = [selectedItem objectForKey:@"countryCode"];
        self.ItinerarObjStep1.to_Address.countryIsdCode = [selectedItem objectForKey:@"countryIsdCode"];
        
        self.ItinerarObjStep1.to_Address.cityName = [selectedItem objectForKey:@"cityName"];
        self.ItinerarObjStep1.to_Address.cityId = [selectedItem objectForKey:@"cityId"];
        self.ItinerarObjStep1.to_Address.cityCode = [selectedItem objectForKey:@"cityCode"];
        
        self.ItinerarObjStep1.to_Address.lat_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLatitude"] doubleValue]];
        self.ItinerarObjStep1.to_Address.long_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLongitude"] doubleValue]];
        
        if([self isSameCountryChoose])
        {
            toSelectCityValueTextfield.text = @"";
            toDateTimeLocationTitlelabel.text = @"";
            [self clearDestinationObjectData];
        }
        else
        {
            
        toSelectCityValueTextfield.text = [NSString stringWithFormat:@"%@, %@",self.ItinerarObjStep1.to_Address.cityName,self.ItinerarObjStep1.to_Address.countryName];
        toDateTimeLocationTitlelabel.text = [NSString stringWithFormat:NSLocalizedString(@"SelectDateTime_ShowLocationMessage", nil), self.ItinerarObjStep1.to_Address.cityName];
        }
    }
}



#pragma mark _-- TextField Delegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:toDateTimeValueTextfield] && fromDateTimeValueTextfield.text.length == 0) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"First_Dept_DateTime_Select_Alert", nil)];
        return NO;
    }else if ([textField isEqual:toDateTimeValueTextfield]){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        
        NSDate *departureDate = [dateFormatter dateFromString:fromDateTimeValueTextfield.text];
        
        UIDatePicker *dtPic = (UIDatePicker*)toDateTimeValueTextfield.inputView;
        [dtPic setMinimumDate:departureDate];
    }else if ([textField isEqual:fromDateTimeValueTextfield]){
        toDateTimeValueTextfield.text = Empty;
    }
    
    return YES;
}


#pragma mark - CollectionView Delegate & Datasource

 - (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TypeOfTransportCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:typeOfTransportIdentifier forIndexPath:indexPath];
    cell.typeOfTransportImgView.image = [UIImage imageNamed:[typeTransportImgArr objectAtIndex:indexPath.row]];
    return cell;
}
 
 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //When enable flowlayout set up for more than one element in collection view use global collectionviewItemHeight
    CGFloat collectionViewHeight = typeOfTransportCollectionView.frame.size.height;
    CGFloat collectionviewItemHeight = collectionViewHeight * 0.91;
    return CGSizeMake(collectionviewItemHeight,collectionviewItemHeight);
}



- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return typeTransportImgArr.count;
}
 
#pragma mark - ItinerySteptwoDelegate
- (void)passItineryObjFromTwo:(ItineraryClass *)itineryObj{
    self.ItinerarObjStep1 = itineryObj;
}

@end

// ========================== Extra + Reuse Old code ===========================


/*- (IBAction)backTapped:(id)sender{
    MyTripsListViewController *managObj = [[MyTripsListViewController alloc] initWithNibName:@"MyTripsListViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
}
- (IBAction)modeOfTravelTapped:(UIButton *)sender {
    for (UIButton *cutB in [sender.superview subviews]) {
        if ([cutB isKindOfClass:[UIButton class]]) {
            cutB.selected = NO;
        }
    }
    [sender setSelected:YES];
    self.ItinerarObjStep1.travel_Mode = sender.tag;
    _heightConstraintOfContainerView.constant = 66;
    _viewTravelRefrNo.hidden = NO;
}
*/

/*  if (tagRef == DEPRT_CITY){
 self.ItinerarObjStep1.from_Address.cityName = [selectedItem objectForKey:@"countryName"];
 self.ItinerarObjStep1.from_Address.cityId = [selectedItem objectForKey:@"countryId"];
 self.ItinerarObjStep1.from_Address.cityCode = [selectedItem objectForKey:@"countryCode"];
 self.ItinerarObjStep1.from_Address.lat_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLatitude"] doubleValue]];
 self.ItinerarObjStep1.from_Address.long_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLongitude"] doubleValue]];
 
 [_btnDeprtCity setTitle:self.ItinerarObjStep1.from_Address.cityName forState:UIControlStateNormal];
 _lblDeprtCityMessage.text = [NSString stringWithFormat:@"* Please enter %@ date & time",self.ItinerarObjStep1.from_Address.cityName];
 }
 
 
 //  [_btnDeprtCountry setTitle:self.ItinerarObjStep1.from_Address.countryName forState:UIControlStateNormal];
 //    // reset city
 //        self.ItinerarObjStep1.from_Address.cityName = Empty;
 //        self.ItinerarObjStep1.from_Address.cityId = Empty;
 //        self.ItinerarObjStep1.from_Address.cityCode = Empty;
 //
 //        [_btnDeprtCity setTitle:@"City" forState:UIControlStateNormal];
 //
 //        _lblDeprtCityMessage.text = @"* Please enter departure city date & time";
 */


/* if (tagRef == DEST_CITY){
 self.ItinerarObjStep1.to_Address.cityName = [selectedItem objectForKey:@"countryName"];
 self.ItinerarObjStep1.to_Address.cityId = [selectedItem objectForKey:@"countryId"];
 self.ItinerarObjStep1.to_Address.cityCode = [selectedItem objectForKey:@"countryCode"];
 
 self.ItinerarObjStep1.to_Address.lat_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLatitude"] doubleValue]];
 self.ItinerarObjStep1.to_Address.long_Value = [NSNumber numberWithDouble:[[selectedItem objectForKey:@"cityLongitude"] doubleValue]];
 
 [_btnDestCity setTitle:self.ItinerarObjStep1.to_Address.cityName forState:UIControlStateNormal];
 _lblArrivalCityMessage.text = [NSString stringWithFormat:@"* Please enter %@ date & time",self.ItinerarObjStep1.to_Address.cityName];
 }
 
 //        [_btnDestCountry setTitle:self.ItinerarObjStep1.to_Address.countryName forState:UIControlStateNormal];
 //    // reset city
 //
 //        self.ItinerarObjStep1.to_Address.cityName = Empty;
 //        self.ItinerarObjStep1.to_Address.cityId = Empty;
 //        self.ItinerarObjStep1.to_Address.cityCode = Empty;
 //        [_btnDestCity setTitle:@"City" forState:UIControlStateNormal];
 //
 //        _lblArrivalCityMessage.text = @"* Please enter arrival city date & time";
 */


/*
 - (BOOL)ValidationPassed{
 if ([self.ItinerarObjStep1.from_Address.countryId isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryDepartureCountryEmpty];
 return NO;
 }
 if ([self.ItinerarObjStep1.from_Address.cityId isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryDepartureCityEmpty];
 return NO;
 }
 if ([self.ItinerarObjStep1.to_Address.countryId isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryArrivalCountryEmpty];
 return NO;
 }
 if ([self.ItinerarObjStep1.to_Address.cityId isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryArrivalCityEmpty];
 return NO;
 }
 if ([self.ItinerarObjStep1.from_Address.cityId isEqualToString:self.ItinerarObjStep1.to_Address.cityId]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryArrivalDepartureCityEquals];
 return NO;
 }
 if ([self.ItinerarObjStep1.Departing_Date_Time isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryDepartureDateTimeEmpty];
 return NO;
 }
 if ([self.ItinerarObjStep1.Arrival_Date_Time isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryArrivalDateTimeEmpty];
 return NO;
 }
 if (self.ItinerarObjStep1.travel_Mode == TRAVEL_MODE_ALL) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryTranportModeEmpty];
 return NO;
 }
 return YES;
 } */


/* - (void)textFieldDidBeginEditing:(UITextField *)textField{
 if ([textField isEqual:_txtTravelRefNo]) {
 [UIView animateWithDuration:0.25 animations:^{
 _containerView.transform = CGAffineTransformTranslate(_containerView.transform, 0, -150);
 }];
 }
 
 }
 - (void)textFieldDidEndEditing:(UITextField *)textField{
 if ([textField isEqual:_txtTravelRefNo]) {
 self.ItinerarObjStep1.Travel_Ref_No = textField.text;
 [UIView animateWithDuration:0.25 animations:^{
 _containerView.transform = CGAffineTransformIdentity;
 }];
 }
 }*/

/*@property (weak, nonatomic) IBOutlet UIView *headerView;
 @property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintOfContainerView;
 @property (weak, nonatomic) IBOutlet UIView *containerView;
 @property (weak, nonatomic) IBOutlet UIView *viewTravelRefrNo;
 
 
 @property (weak, nonatomic) IBOutlet UIButton *btnDeprtCountry;
 @property (weak, nonatomic) IBOutlet UIButton *btnDestCountry;
 @property (weak, nonatomic) IBOutlet UIButton *btnDeprtCity;
 @property (weak, nonatomic) IBOutlet UIButton *btnDestCity;
 
 
 @property (weak, nonatomic) IBOutlet UITextField *txtDeprtDateTime;
 @property (weak, nonatomic) IBOutlet UITextField *txtArrivDateTime;
 
 @property (weak, nonatomic) IBOutlet UITextField *txtTravelRefNo;
 
 @property (weak, nonatomic) IBOutlet UILabel *lblArrivalCityMessage;
 @property (weak, nonatomic) IBOutlet UILabel *lblDeprtCityMessage; */

/* - (IBAction)btnCountryCityTapped:(UIButton*)sender {
 
 TAG_VALUE type = DEPRT_COUNTRY;
 NSString *countryId = Empty;
 
 if ([sender isEqual:_btnDestCountry])
 type = DEST_COUNTRY;
 else if ([sender isEqual:_btnDeprtCity]){
 if ([self.ItinerarObjStep1.from_Address.countryId isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryCountryRequired];
 return;
 }
 type = DEPRT_CITY; countryId = self.ItinerarObjStep1.from_Address.countryId;
 }
 else if ([sender isEqual:_btnDestCity]){
 if ([self.ItinerarObjStep1.to_Address.countryId isEqualToString:Empty]) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryCountryRequired];
 return;
 }
 type = DEST_CITY; countryId = self.ItinerarObjStep1.to_Address.countryId;
 }
 
 PickerInfoViewController *pick = [[PickerInfoViewController alloc] initWithNibName:@"PickerInfoViewController" bundle:nil];
 pick.type = type;
 pick.countryId = countryId;
 pick.delegate = self;
 [self presentViewController:pick animated:YES completion:nil];
 }
*/

/* [Helper addShadowToLayer:_headerView.layer];
 [Helper addShadowToLayer:_containerView.layer];
 ROUNDEDCORNER(_containerView.layer);
 
 _heightConstraintOfContainerView.constant = 0;
 _viewTravelRefrNo.hidden = YES;
 
 
 
 _lblDeprtCityMessage.text = @"* Please enter departure city date & time";
 _lblArrivalCityMessage.text = @"* Please enter arrival city date & time";*/


