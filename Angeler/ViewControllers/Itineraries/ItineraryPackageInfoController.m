//
//  ItineraryPackageInfoController.m
//  Angeler
//
//  Created by AJAY on 26/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ItineraryPackageInfoController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "MWPhotoBrowser.h"
#import "SharePopUpDoubleBtnActionVC.h"
#import "HomeViewController.h"
#import "MyTripsListViewController.h"
#import "MTBBarcodeScanner.h"
#import "ImageSlidingVC.h"

//#define ConfirmRefuseHeightConstant 40
//#define RecipientDetailsHeightConstant 100
//#define ScanQRCodeHeightConstant 100

@interface ItineraryPackageInfoController ()<MWPhotoBrowserDelegate,SharePopUpDoubleBtnActionDismissDelegate,UITextFieldDelegate>
{
    BOOL foundCode,isPickUp,isSenderImgTap,isRecipientImgTap,isPictureImgTap;
    NSDictionary *data;
}

@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsTitle;

@property (weak, nonatomic) IBOutlet UIView *viewTripDetails;
@property (weak, nonatomic) IBOutlet UIView *viewShipmentDetails;
@property (weak, nonatomic) IBOutlet UIView *viewSenderDetails;
@property (weak, nonatomic) IBOutlet UIView *viewRecipientDetails;
@property (weak, nonatomic) IBOutlet UIView *viewScanQRCodeDetails;
@property (weak, nonatomic) IBOutlet UIView *confirmDenyView;
@property (weak, nonatomic) IBOutlet UIView *viewQRCodeInfo;

@property (weak, nonatomic) IBOutlet UIView *viewTripDetailsHeader;
@property (weak, nonatomic) IBOutlet UIView *viewShipmentDetailsHeader;
@property (weak, nonatomic) IBOutlet UIView *viewSenderDetailsHeader;
@property (weak, nonatomic) IBOutlet UIView *viewRecipientDetailsHeader;
@property (weak, nonatomic) IBOutlet UIView *viewScanQRCodeDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;


@property (weak, nonatomic) IBOutlet UILabel *lblTripDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblShipmentDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblScanQRCodeDetailsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblPickUpCityCountry;
//@property (weak, nonatomic) IBOutlet UILabel *lblPickUpCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpDateTime;

@property (weak, nonatomic) IBOutlet UILabel *lblDropOffCityCountry;
//@property (weak, nonatomic) IBOutlet UILabel *lblDropOffCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffDateTime;


@property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransportTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransportValue;

@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNoValue;


@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet UILabel *lblFromAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblToAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblFromTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblToTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRefuse;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;



@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (nonatomic, strong) NSMutableArray *arrSenderPhoto;
@property (nonatomic, strong) NSMutableArray *arrRecipientPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;

@property (weak, nonatomic) IBOutlet UITextField *txtQrCode;
@property (weak, nonatomic) IBOutlet UIButton *btnPickUpDelivery;
@property (weak, nonatomic) IBOutlet UIButton *btnScanQRCode;
@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (weak, nonatomic) IBOutlet UIView *scannerView;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterDeliveryOrPickUpCodeTitle;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmDenyViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recipientViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *QRCodeViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmDenyViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recipientViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *QRCodeViewTopConstraint;


@property (weak, nonatomic) IBOutlet UILabel *lblPackageTypeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWeightTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblPackageType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryTime;

@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblCostTitle;
@property (weak, nonatomic) IBOutlet UIImageView *meetingPremiumImageIcon;


@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSenderPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgSenderImage;


@property (weak, nonatomic) IBOutlet UILabel *lblRecipientName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnRecipientPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgRecipientImage;



@property (weak, nonatomic) IBOutlet UIView *pickUpAddressView;
@property (weak, nonatomic) IBOutlet UIView *dropOffAddressView;
@property (weak, nonatomic) IBOutlet UIView *senderSeparatorView;
@property (weak, nonatomic) IBOutlet CopyTextLabel *lblShipmentUniqueID;
@property (weak, nonatomic) IBOutlet UILabel *lblIDTitle;


@end

@implementation ItineraryPackageInfoController
@synthesize topNavbarView,topBarHeightConstraint;

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialSetUp];
    [self callApiToFetchInfoWithPackageId];
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];

    
}




#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}



#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:NSLocalizedString(@"Trip_Carrying_Info﻿", nil)];
    
    //Trip Details
    
    [self.viewTripDetails makeCornerRound:15.0];
    [self.viewTripDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Shipment Details
    [self.viewShipmentDetails makeCornerRound:15.0];
    [self.viewShipmentDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.viewShipmentDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    
    //Item Details
    
    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Sender Details
    
    [self.viewSenderDetails makeCornerRound:15.0];
    [self.viewSenderDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
    //Recipient Details
    
    [self.viewRecipientDetails makeCornerRound:15.0];
    [self.viewRecipientDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    
    //Accept/Deny
    
    [self.btnAccept makeCornerRound:15.0];
    [self.btnRefuse makeCornerRound:15.0 withBoarderColor:[UIColor appThemeYellowColor] borderWidth:2.5];
    [self.btnAccept dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnRefuse dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Scan QR Code
    
    [self.viewScanQRCodeDetails makeCornerRound:15.0];
    [self.viewQRCodeInfo makeCornerRound:15.0];
    [self.viewScanQRCodeDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnPickUpDelivery makeCornerRound:15.0];
    
    //Gesture Action
    
    self.pickUpAddressView.userInteractionEnabled = YES;
    [self.pickUpAddressView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickUpAddressTap:)]];
    
    self.dropOffAddressView.userInteractionEnabled = YES;
    [self.dropOffAddressView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dropOffAddressTap:)]];
    
    //Scanner View
    _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:self.scannerView];
    [_txtQrCode setPlaceholder:NSLocalizedString(@"Enter_Code", nil)];
    
   
}
-(void)roundAllSpecificCorner
{
    [self.viewTripDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewShipmentDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewSenderDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewRecipientDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewScanQRCodeDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
    //Trip Details
    _lblTripDetailsTitle.text = NSLocalizedString(@"My_Trip", nil);
    _lblModeOfTransportTitle.text = NSLocalizedString(@"Mode_Of_Transport", nil);
    _lblTransportRefNoTitle.text = NSLocalizedString(@"Flight_Number", nil);
    
    //Shipment Details
    _lblShipmentDetailsTitle.text = NSLocalizedString(@"Appointment_Place_Detail", nil);
    
    _orLabel.text = NSLocalizedString(@"OR", nil);
    
    _lblPackageTypeTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Category", nil)];
    _lblPackageWeightTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Weight", nil)];
    _lblStatusTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Status", nil)];
    _lblDeliveryTimeTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Deliver_Before", nil)];
     _lblItemDetailsTitle.text = NSLocalizedString(@"Item_Details",nil);
    _lblCostTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"I_Earn", nil)];
     _lblIDTitle.text = NSLocalizedString(@"AGTN", nil);
    
    _lblSenderDetailsTitle.text = NSLocalizedString(@"Sender_Details",nil);
    _lblRecipientDetailsTitle.text = NSLocalizedString(@"Recipient_Details",nil);
    [_btnRefuse setTitle:NSLocalizedString(@"Refuse",nil) forState:UIControlStateNormal];
    [_btnAccept setTitle:NSLocalizedString(@"Accept",nil) forState:UIControlStateNormal];
    [_btnScanQRCode setTitle:NSLocalizedString(@"Scan_QR_Code",nil) forState:UIControlStateNormal];
    
}


-(void)showAngelerConfirmBookingSuccessPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpDoubleBtnActionVC *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpDoubleBtnVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Congratulation", nil);
    sharePopUpVC.headerMiddleTitleText = [NSString stringWithFormat:NSLocalizedString(@"You_Accept_Delivery", nil),_lblSenderName.text];
    
    
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"You'll_Earn_Money_On_Your_Trip", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.leftBtnActionitleText = NSLocalizedString(@"My Trips", nil);
    sharePopUpVC.rightBtnActionitleText = NSLocalizedString(@"Home", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

#pragma mark - Webservice Call

- (void)callApiToFetchInfoWithPackageId{
    //userId, sessionId, os, osVersion, appVersion, packageId
    
    NSDictionary *param = @{@"bookingId":_bookingId //UDValue(_bookingId)//
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryPackBookInfo
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               data = [responseObject objectForKey:@"responseData"];
               [self updateValuesWithData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
    
}
- (void)updateValuesWithData{
    
    _lblShipmentUniqueID.text = [data objectForKey:@"shipmentUniqueId"];
    
    //Trip Details
    {
        
        //Pick Up
  
         _lblPickUpCityCountry.text = [NSString stringWithFormat:@"%@, %@",[[data objectForKey:@"itineraryDetails"] objectForKey:@"departureCityName"],[[data objectForKey:@"itineraryDetails"] objectForKey:@"departureCountryName"]];
        _lblPickUpDateTime.text = [Helper getDateTimeToStringWithDate:[[data objectForKey:@"itineraryDetails"] objectForKey:@"departureDate"] Time:[[data objectForKey:@"itineraryDetails"] objectForKey:@"departureTime"]];
        
        
        //Drop Off
      
        _lblDropOffCityCountry.text = [NSString stringWithFormat:@"%@, %@",[[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalCityName"],[[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalCountryName"]];
        _lblDropOffDateTime.text = [Helper getDateTimeToStringWithDate:[[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalDate"] Time:[[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalTime"]];
        
        NSLog(@"Trnsport Type %ld",[[[data objectForKey:@"itineraryDetails"] objectForKey:@"travelMode"] integerValue] );
        _lblModeOfTransportValue.text = [GlobalInfo travelMode:[[[data objectForKey:@"itineraryDetails"] objectForKey:@"travelMode"] integerValue]];
        _lblTransportRefNoValue .text = [[data objectForKey:@"itineraryDetails"] objectForKey:@"travelRefNo"];
        
    }
    
    //Shipment details
    {
       
        _lblPackageType.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];
        _lblPackageWeight.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"packageWeight"],NSLocalizedString(@"package_weight_unit", nil)];
        _lblStatus.text = [data objectForKey:@"activeButtonLabel"];
        _lblCost.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"serviceCost"],NSLocalizedString(@"Credits", nil)];
       
       
        NSString *fromLat,*fromLong,*fromAddress;
        NSString *toLat,*toLong,*toAddress;
        
        
        if ([[data objectForKey:@"serviceType"] isEqualToString:serviceType_Premium])
        {
            [_meetingPremiumImageIcon setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
            fromLat = [data objectForKey:@"fromAddressLatitude"];
            fromLong = [data objectForKey:@"fromAddressLongitude"];
            fromAddress = [data objectForKey:@"fromAddress"];
            toLat = [data objectForKey:@"toAddressLatitude"];
            toLong = [data objectForKey:@"toAddressLongitude"];
            toAddress = [data objectForKey:@"toAddress"];
        }
        else
        {
            [_meetingPremiumImageIcon setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
            fromLat = [data objectForKey:@"angelerPickupLatitude"];
            fromLong = [data objectForKey:@"angelerPickupLongitude"];
            fromAddress = [data objectForKey:@"angelerPickupAddress"];
            toLat = [data objectForKey:@"angelerDropoffLatitude"];
            toLong = [data objectForKey:@"angelerDropoffLongitude"];
            toAddress = [data objectForKey:@"angelerDropoffAddress"];
        }

        _lblFromAddress.text = fromAddress;
        _lblToAddress.text = toAddress;
        
    }
    
    //Picture of item
    {
        
        NSArray *imges = [data objectForKey:@"images"];
        if (imges.count == 0) {
            //  [GlobalInfo hideViewNamed:_pictureView withTopName:@"pictureTop"];
        }else{
            self.arrPhotos = [NSMutableArray array];
            for (int i = 1; i <= imges.count ; i++) {
                UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
                imgV.hidden = NO;
                imgV.userInteractionEnabled = YES;
                [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
                NSLog(@"image url =====  %@",[[imges objectAtIndex:i-1] objectForKey:@"image"]);
                [imgV sd_setImageWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
                MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]]];
                photo.caption = @"";
                [self.arrPhotos addObject:photo];
                
            }
        }
    }
    
    //Sender Details
    
    _lblSenderName.text = [data objectForKey:@"senderName"];
    _lblSenderEmail.text = [data objectForKey:@"senderEmail"];
    _lblSenderPhoneNumber.text = [data objectForKey:@"senderPhone"];
    
    [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    
    
    if ([data objectForKey:@"senderImage"] == nil || [[data objectForKey:@"senderImage"] isEqualToString:@""])
    {
       _imgSenderImage.userInteractionEnabled = NO;
    }
    else
    {
        _imgSenderImage.userInteractionEnabled = YES;
        [_imgSenderImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSenderImage:)]];
        self.arrSenderPhoto = [NSMutableArray array];
        MWPhoto *senderPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"senderImage"]]];
        senderPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Sender", nil),[data objectForKey:@"senderName"]];
        [self.arrSenderPhoto addObject:senderPhoto];
    }
    
    
    senderContactNumber = [data objectForKey:@"senderPhone"];
//    if (senderContactNumber.length > PHONE_MIN_DIGIT)
//    {
//        _btnSenderPhone.hidden = NO;
//    }
//    else
//    {
//        _btnSenderPhone.hidden = YES;
//    }
    
    //Recipient Details
    
    _lblRecipientName.text = [data objectForKey:@"recipientName"];
    _lblRecipientEmail.text = [data objectForKey:@"recipientEmail"];
    _lblRecipientPhoneNumber.text = [data objectForKey:@"recipientPhone"];
    
    
    [_imgRecipientImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"recipientImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    recipientContactNumber = [data objectForKey:@"recipientPhone"];
//    if (recipientContactNumber.length > PHONE_MIN_DIGIT)
//    {
//        _btnRecipientPhone.hidden = NO;
//    }
//    else
//    {
//        _btnRecipientPhone.hidden = YES;
//    }
//
    
    if ([data objectForKey:@"recipientImage"] == nil || [[data objectForKey:@"recipientImage"] isEqualToString:@""])
    {
        _imgRecipientImage.userInteractionEnabled = NO;
    }
    else
    {
        _imgRecipientImage.userInteractionEnabled = YES;
         self.arrRecipientPhoto = [NSMutableArray array];
        [_imgRecipientImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRecipientImage:)]];
        [self.arrRecipientPhoto addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"recipientImage"]]]];
    }
    //Delivery Details
    
    _lblDeliveryTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"deliveryDate"] Time:[data objectForKey:@"deliveryTime"]];
    
   
    
    
    //Different Condition Show hide
    
   
    
  
    /*1. Sender Details
     2. Appointment Place Details      //While status user not accepted or denied 1st                                  stage
     3. My Trip
     4. View Item Details
 */
    if ([[data objectForKey:@"statusType"] integerValue] == 2)//Reserved
    {
        self.viewRecipientDetails.hidden = YES;
        self.recipientViewHeightConstraint.constant = 0;
        self.recipientViewTopConstraint.constant = 0;
        
        self.btnSenderPhone.hidden = YES;
        self.lblSenderEmail.hidden = YES;
        self.lblSenderPhoneNumber.hidden = YES;
        
        self.viewScanQRCodeDetails.hidden = YES;
        self.QRCodeViewHeightConstraint.constant = 0;
        self.QRCodeViewTopConstraint.constant = 0;
        [self setAcceptDenyAngelerStatusStatusConstraint];
        
        
    }
    else if ([[data objectForKey:@"statusType"] integerValue] == 3 || [[data objectForKey:@"statusType"] integerValue] == 4) //Accepted / Collect From sender
    {
    
      /*  For status : Collect from sender : => Block Order Sequence (Already designed in this order in XIB)
        
        1. View Scan QR Code
        3. Sender Details
        4. Appointment Place Details
        5. My Trip
        2. View Item Details*/
        
        self.confirmDenyView.hidden = YES;
        self.confirmDenyViewTopConstraint.constant = 0;
        self.confirmDenyViewHeightConstraint.constant = 0;
        self.senderSeparatorView.hidden = YES;
        isPickUp = YES;
        [self.btnPickUpDelivery setTitle:NSLocalizedString(@"Validate_Pick_Up", nil) forState:UIControlStateNormal];
        _lblScanQRCodeDetailsTitle.text = NSLocalizedString(@"Item_Pickup", nil);
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"Enter_Pickup_Code", nil), [[data objectForKey:@"senderName"] uppercaseString]];
        _lblEnterDeliveryOrPickUpCodeTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[[data objectForKey:@"senderName"] uppercaseString]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
        self.viewRecipientDetails.hidden = YES;
        self.recipientViewHeightConstraint.constant = 0;
        self.recipientViewTopConstraint.constant = 0;
        [self setCollectionFromSenderStatusConstraint];
      
    }
    else if ([[data objectForKey:@"statusType"] integerValue] == 5 || [[data objectForKey:@"statusType"] integerValue] == 6) // Deliver to recipient
    {
    
     /*   For status : Deliver to recipient : > Block Order Sequence (Already designed in this order in XIB)
        
        1. View Scan QR Code
        2. Recipient Details
        3. Appointment Place Details
        4. Sender Details
        5. My Trip
        6. View Item Details*/
        
        self.confirmDenyView.hidden = YES;
        self.confirmDenyViewTopConstraint.constant = 0;
        self.confirmDenyViewHeightConstraint.constant = 0;
        self.senderSeparatorView.hidden = YES;
        isPickUp = NO;
        [self.btnPickUpDelivery setTitle:NSLocalizedString(@"Validate_Drop_Off", nil) forState:UIControlStateNormal];
        _lblScanQRCodeDetailsTitle.text = NSLocalizedString(@"Item_Delivery", nil);
        
       
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"Enter_Delivery_Code", nil), [[data objectForKey:@"recipientName"] uppercaseString]];
        _lblEnterDeliveryOrPickUpCodeTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[[data objectForKey:@"recipientName"] uppercaseString]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
        [self setDeliverToRecipientStatusConstraint];
    }
    else if ([[data objectForKey:@"statusType"] integerValue] == 8) //Delivered
    {
        
       /* For status : Delivered :
    
        1. View Item Details
        2. Sender Details
        3. Appointment Place Details
        4. My Trip
        5. Recipient Details*/
        
        self.confirmDenyView.hidden = YES;
        self.confirmDenyViewTopConstraint.constant = 0;
        self.confirmDenyViewHeightConstraint.constant = 0;
        self.senderSeparatorView.hidden = YES;
        self.viewScanQRCodeDetails.hidden = YES;
        self.QRCodeViewHeightConstraint.constant = 0;
        self.QRCodeViewTopConstraint.constant = 0;
        [self setDeliveredStatusConstraint];
        
    }
    
    //Gesture Action Set
    
    
    UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)];
    [_lblEnterDeliveryOrPickUpCodeTitle setUserInteractionEnabled:YES];
    [_lblEnterDeliveryOrPickUpCodeTitle addGestureRecognizer:tapges];
    
    //Map
    {
        CLLocationCoordinate2D from_coordinate = CLLocationCoordinate2DMake([[data objectForKey:@"angelerPickupLatitude"] doubleValue] ,[[data objectForKey:@"angelerPickupLongitude"] doubleValue]);
        
        CLLocationCoordinate2D to_coordinate = CLLocationCoordinate2DMake([[data objectForKey:@"angelerDropoffLatitude"] doubleValue] ,[[data objectForKey:@"angelerDropoffLongitude"] doubleValue]);
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:from_coordinate zoom:12];
        
        GMSMarker *from_marker = [[GMSMarker alloc] init];
        from_marker.position = from_coordinate;
        from_marker.title = [data objectForKey:@"angelerPickupAddress"];
        from_marker.map = _mapView;
        
        GMSMarker *to_marker = [[GMSMarker alloc] init];
        to_marker.position = to_coordinate;
        //    markerPack.icon = [UIImage imageNamed:@"LocationPin"];
        to_marker.title = [data objectForKey:@"angelerDropoffAddress"];
        to_marker.map = _mapView;
        
        _mapView.camera = camera;
        [_mapView setSelectedMarker:from_marker];
        
        NSArray *arrMarkerLocation = [NSArray arrayWithObjects:from_marker,to_marker, nil];
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        
        for (GMSMarker *marker in arrMarkerLocation)
            bounds = [bounds includingCoordinate:marker.position];
        
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50]];
    }
    
}

#pragma mark - gesture Action

- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture{
    NSLog(@"handleTapOnLabel Called");
    if ([[data objectForKey:@"statusType"] integerValue] == 3 || [[data objectForKey:@"statusType"] integerValue] == 4){
        if ([data objectForKey:@"senderImage"] == nil || [[data objectForKey:@"senderImage"] isEqualToString:@""]){
            
        }
        else{
            isSenderImgTap = true;
            isRecipientImgTap = false;
            isPictureImgTap = false;
            [self setMWPhotoBrowserWithTag:0];
            
        }
    }
    
    else if ([[data objectForKey:@"statusType"] integerValue] == 5 || [[data objectForKey:@"statusType"] integerValue] == 6){
        
        if ([data objectForKey:@"recipientImage"] == nil || [[data objectForKey:@"recipientImage"] isEqualToString:@""]){
            NSLog(@"Not Found Recipient Image");
        }
        else{
            isSenderImgTap = false;
            isRecipientImgTap = true;
            isPictureImgTap = false;
            
            [self setMWPhotoBrowserWithTag:0];
        }
    }
}

- (void)pickUpAddressTap:(UITapGestureRecognizer*)tapGest
{
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic",[data objectForKey:@"fromAddressLatitude"] ,[data objectForKey:@"fromAddressLongitude"]]]];
    } else {
        NSLog(@"Can't use comgooglemaps://");
    }

}

- (void)dropOffAddressTap:(UITapGestureRecognizer*)tapGest
{
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic",[data objectForKey:@"toAddressLatitude"] ,[data objectForKey:@"toAddressLongitude"]]]];
    } else {
        NSLog(@"Can't use comgooglemaps://");
    }
}


- (void)tapOnRecipientImage:(UITapGestureRecognizer*)tapGest
{
    isSenderImgTap = false;
    isRecipientImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
}

- (void)tapOnSenderImage:(UITapGestureRecognizer*)tapGest{
    
    isSenderImgTap = true;
    isRecipientImgTap = false;
    isPictureImgTap = false;
    [self setMWPhotoBrowserWithTag:0];
    
}

- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    
    isSenderImgTap = false;
    isRecipientImgTap = false;
    isPictureImgTap = true;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
    
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isSenderImgTap)
    {
         return self.arrSenderPhoto.count;
    }
    else if (isRecipientImgTap)
        {
            return self.arrRecipientPhoto.count;
        }
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (isSenderImgTap)
    {
        if (index < self.arrSenderPhoto.count) {
            return [self.arrSenderPhoto objectAtIndex:index];
        }
    }
    else if (isRecipientImgTap)
    {
        if (index < self.arrRecipientPhoto.count) {
            return [self.arrRecipientPhoto objectAtIndex:index];
        }
    }
    else
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    return nil;
}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    [Helper showAlertWithTitle:Empty Message:@"Scanning Unavailable"];
}

#pragma mark - Button Action

- (IBAction)scanQrTapped:(id)sender {
    _scannerView.hidden = NO;
    foundCode = NO;
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            NSError* error = nil;
            [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                if (!foundCode) {
                    _txtQrCode.text = code.stringValue;
                    //                    [self callServiceToFetchDataForBranch:code.stringValue];
                    foundCode = YES;
                }
                _scannerView.hidden = YES;
                [self.scanner stopScanning];
            } error:&error];
        } else {
            // The user denied access to the camera
            [self displayPermissionMissingAlert];
        }
    }];
    
}

- (IBAction)pickUpTapped:(id)sender {
    if ([Helper checkEmptyField:_txtQrCode.text]) {
        
        if(isPickUp)
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Validate_PickUp_Alert", nil)];
        else
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Validate_Dropoff_Alert", nil)];
        return;
    }
    
    NSDictionary *param = @{@"bookingId":_bookingId,
                            @"code":_txtQrCode.text
                            };
    
    NSString *apiMethodname = (isPickUp) ? kUpdatePickUpStatus : kUpdateDropOffStatus;
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:apiMethodname
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                               [self.navigationController popViewControllerAnimated:true];
                                                           }];
                                                           NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [GlobalInfo showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}

- (IBAction)confirmDenyTapped:(UIButton*)sender {

    NSString *bookingStatus = sender.tag == 1?@"Accepted":@"Rejected";




    NSDictionary *param = @{@"bookingId":_bookingId,
                            @"bookingStatus":bookingStatus
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryBookingStatus
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               NSDictionary *extraData = [responseObject objectForKey:@"extraData"];

               if ([[extraData objectForKey:@"status"] isEqualToString:@"Accepted"] )
               {
                   [self showAngelerConfirmBookingSuccessPopUp];
               }
               else
               {
                   [self redirectToMyTripListScreen];

                   NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                   UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

                   }];
                   [GlobalInfo showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
               }
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];



}
- (IBAction)callSender:(id)sender {
//    if (senderContactNumber.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",senderContactNumber]];

        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
   // }
}
- (IBAction)callRecipient:(id)sender {
   // if (recipientContactNumber.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",recipientContactNumber]];

        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    //}
}

#pragma mark - SharePopUpDoubleBtnActionDismissDelegate

- (void)dismissPopUpLeftBtnAction
{
    [self redirectToMyTripListScreen];
    
}
- (void)dismissPopUpRightBtnAction
{
    HomeViewController *homeObj = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeObj];
}

-(void)redirectToMyTripListScreen
{
    MyTripsListViewController *myTripsObj = [[MyTripsListViewController alloc] initWithNibName:@"MyTripsListViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:myTripsObj];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Constraint Set Dynamic reorder of block Based on status 

-(void)setAcceptDenyAngelerStatusStatusConstraint{
    [self.viewSenderDetails.topAnchor constraintEqualToAnchor:self.viewScanQRCodeDetails.bottomAnchor constant:15].active = YES;
    [self.viewShipmentDetails.topAnchor constraintEqualToAnchor:self.viewSenderDetails.bottomAnchor constant:15].active = YES;
    [self.viewItemDetails.topAnchor constraintEqualToAnchor:self.viewShipmentDetails.bottomAnchor constant:15].active = YES;
    [self.viewTripDetails.topAnchor constraintEqualToAnchor:self.viewItemDetails.bottomAnchor constant:15].active = YES;
    [self.contentView.bottomAnchor constraintEqualToAnchor:self.viewTripDetails.bottomAnchor constant:15].active = YES;

}

-(void)setCollectionFromSenderStatusConstraint{
    
    [self.viewSenderDetails.topAnchor constraintEqualToAnchor:self.viewScanQRCodeDetails.bottomAnchor constant:15].active = YES;
    [self.viewShipmentDetails.topAnchor constraintEqualToAnchor:self.viewSenderDetails.bottomAnchor constant:15].active = YES;
     [self.viewItemDetails.topAnchor constraintEqualToAnchor:self.viewShipmentDetails.bottomAnchor constant:15].active = YES;
      [self.viewTripDetails.topAnchor constraintEqualToAnchor:self.viewItemDetails.bottomAnchor constant:15].active = YES;
    [self.viewRecipientDetails.topAnchor constraintEqualToAnchor:self.viewTripDetails.bottomAnchor constant:15].active = YES;
    [self.contentView.bottomAnchor constraintEqualToAnchor:self.viewRecipientDetails.bottomAnchor constant:15].active = YES;

}

-(void)setDeliverToRecipientStatusConstraint{
    
    
    [self.viewRecipientDetails.topAnchor constraintEqualToAnchor:self.viewScanQRCodeDetails.bottomAnchor constant:15].active = YES;
    [self.viewShipmentDetails.topAnchor constraintEqualToAnchor:self.viewRecipientDetails.bottomAnchor constant:15].active = YES;
    [self.viewSenderDetails.topAnchor constraintEqualToAnchor:self.viewShipmentDetails.bottomAnchor constant:15].active = YES;
    [self.viewItemDetails.topAnchor constraintEqualToAnchor:self.viewSenderDetails.bottomAnchor constant:15].active = YES;
    [self.viewTripDetails.topAnchor constraintEqualToAnchor:self.viewItemDetails.bottomAnchor constant:15].active = YES;
   [self.contentView.bottomAnchor constraintEqualToAnchor:self.viewTripDetails.bottomAnchor constant:15].active = YES;
    
}

-(void)setDeliveredStatusConstraint{

    [self.viewItemDetails.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:15].active = YES;
    [self.viewSenderDetails.topAnchor constraintEqualToAnchor:self.viewItemDetails.bottomAnchor constant:15].active = YES;
      [self.viewShipmentDetails.topAnchor constraintEqualToAnchor:self.viewSenderDetails.bottomAnchor constant:15].active = YES;
     [self.viewTripDetails.topAnchor constraintEqualToAnchor:self.viewShipmentDetails.bottomAnchor constant:15].active = YES;
    [self.viewRecipientDetails.topAnchor constraintEqualToAnchor:self.viewTripDetails.bottomAnchor constant:15].active = YES;
     [self.contentView.bottomAnchor constraintEqualToAnchor:self.viewRecipientDetails.bottomAnchor constant:15].active = YES;
}





@end


//======================= Old + Reuse Code ================================



//    if ([_status isEqualToString:@"Reserved"]) {
//        [self hideViewNamed:_senderView withTopName:@"senderTop"];
//        [self hideViewNamed:_recipentView withTopName:@"recipentTop"];
//    }
//    if (_buttonTitle == PACKAGE_INFO){
//        _infoViewHeightConstraint.constant = 0;
//        _infoView.hidden = YES;
//        _confirmDenyView.hidden = YES;
//        [GlobalInfo hideViewNamed:_finalStatusView withTopName:@"finalStatusTop"];
//    }else{
//        [GlobalInfo hideViewNamed:_senderView withTopName:@"senderTop"];
//        [GlobalInfo hideViewNamed:_recipentView withTopName:@"recipentTop"];
//
//        if (_buttonTitle == CONFIRM_DENY) {
//            [GlobalInfo hideViewNamed:_finalStatusView withTopName:@"finalStatusTop"];
//        }else{ // packageStatus == others
//            _infoViewHeightConstraint.constant = 0;
//            _infoView.hidden = YES;
//            _confirmDenyView.hidden = YES;
//        }
//    }
//    // Do any additional setup after loading the view from its nib.
//    [Helper makeItRound:_roundView.layer withRadius:7.0];
//
//
//    [Helper addShadowToLayer:_headerView.layer];
//    //[Helper addShadowToLayer:_infoView.layer];
//    [Helper addShadowToLayer:_sepratorView1.layer];
//    [Helper addShadowToLayer:_pictureView.layer];
//    [Helper addShadowToLayer:_recipentView.layer];
//    [Helper addShadowToLayer:_senderView.layer];
//    [Helper addShadowToLayer:_deliveryView.layer];
//    [Helper addShadowToLayer:_finalStatusView.layer];




//@property (weak, nonatomic) IBOutlet UILabel *lblRemarks;
//@property (weak, nonatomic) IBOutlet UILabel *lblPackageSize;
//@property (weak, nonatomic) IBOutlet UIImageView *imgModeOfTransport;
//@property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransport;
//@property (weak, nonatomic) IBOutlet UIView *roundView;
//@property (weak, nonatomic) IBOutlet UIView *sepratorView1;
//@property (weak, nonatomic) IBOutlet UIView *headerView;
//@property (weak, nonatomic) IBOutlet UIView *pictureView;
//@property (weak, nonatomic) IBOutlet UIView *senderView;
//@property (weak, nonatomic) IBOutlet UIView *recipentView;
//@property (weak, nonatomic) IBOutlet UIView *deliveryView;
//@property (weak, nonatomic) IBOutlet UIView *finalStatusView;
//@property (weak, nonatomic) IBOutlet UILabel *lblFinalStatusText;
//@property (weak, nonatomic) IBOutlet UILabel *departureAddress;
//@property (weak, nonatomic) IBOutlet UILabel *arrivalAddress;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoViewHeightConstraint;
//@property (weak, nonatomic) IBOutlet UIView *infoView;
//@property (weak, nonatomic) IBOutlet UIImageView *imgTravelMode;
//@property (weak, nonatomic) IBOutlet UILabel *lblTravelRefNo;
//@property (weak, nonatomic) IBOutlet UILabel *lblDocType;






//        _departureAddress.text = [NSString stringWithFormat:@"%@, %@\n%@, %@",
//                                [[data objectForKey:@"itineraryDetails"] objectForKey:@"departureCityName"],
//                                [[data objectForKey:@"itineraryDetails"] objectForKey:@"departureCountryName"],
//                                [[data objectForKey:@"itineraryDetails"] objectForKey:@"departureDate"],
//                                [[[data objectForKey:@"itineraryDetails"] objectForKey:@"departureTime"] substringToIndex:5]];
//
//        _arrivalAddress.text = [NSString stringWithFormat:@"%@, %@\n%@, %@",
//                              [[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalCityName"],
//                              [[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalCountryName"],
//                              [[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalDate"],
//                              [[[data objectForKey:@"itineraryDetails"] objectForKey:@"arrivalTime"] substringToIndex:5]];
//        _imgTravelMode.image = [UIImage imageNamed:[[GlobalInfo travelMode:[[[data objectForKey:@"itineraryDetails"] objectForKey:@"travelMode"] integerValue]] lowercaseString]];
//        _lblTravelRefNo.text = [[data objectForKey:@"itineraryDetails"] objectForKey:@"travelRefNo"];
//        _lblDocType.text = [GlobalInfo TypeOfPackage:[[[data objectForKey:@"itineraryDetails"] objectForKey:@"packageType"] integerValue]];


//    NSMutableAttributedString *strFromAdd = [data objectForKey:@"fromAddress"];
//    [strFromAdd addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:14.0] range:NSMakeRange(0, 5)];

//    NSMutableAttributedString *strToAdd = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"To: %@",[data objectForKey:@"toAddress"]]];
//    [strToAdd addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:14.0] range:NSMakeRange(0, 3)];
//
//
//
//    _lblPackageSize.text = [NSString stringWithFormat:@"%@ X %@ X %@ CM",[data objectForKey:@"packageHeight"],[data objectForKey:@"packageWidth"],[data objectForKey:@"packageLength"]];
//
//    _lblModeOfTransport.text = [GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]];
//    _imgModeOfTransport.image = [UIImage imageNamed:[NSString stringWithFormat:@"package_info_%@",[[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]] lowercaseString]]];
//
//
//    _lblRecipientName.text = [data objectForKey:@"recipientName"];
//    _lblRecipientContactNo.text = [data objectForKey:@"recipientPhone"];
//    _lblRecipientEmail.text = [data objectForKey:@"recipientEmail"];
//
//    _lblRemarks.text = [data objectForKey:@"remarks"];
//
//    //[data objectForKey:@"finalStatusText"];
//    _lblFinalStatusText.attributedText = [GlobalInfo formatFinalStatusText:[NSString stringWithFormat:@"Status: %@",[data objectForKey:@"finalStatusText"]] andIndex:7];
//
//
//    _lblSenderEmail.text = [data objectForKey:@"senderEmail"];
//
//

