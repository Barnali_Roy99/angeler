//
//  ItineraryInfoController.m
//  Angeler
//
//  Created by AJAY on 17/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ItineraryInfoController.h"
#import "MeetPopUpViewController.h"

@interface ItineraryInfoController ()

//Trip Details
@property (weak, nonatomic) IBOutlet UIView *viewTripDetails;
@property (weak, nonatomic) IBOutlet UIView *viewTripDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDetailsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransportTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblModeOftransport;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTripIdTitle;
@property (weak, nonatomic) IBOutlet CopyTextLabel *lblTripID;

@property (weak, nonatomic) IBOutlet UILabel *lblFromCityCountry;
//@property (weak, nonatomic) IBOutlet UILabel *lblFromCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblFromDateTime;

@property (weak, nonatomic) IBOutlet UILabel *lblToCityCountry;
//@property (weak, nonatomic) IBOutlet UILabel *lblToCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblToDateTime;

//Package Details
@property (weak, nonatomic) IBOutlet UIView *viewPackageDetails;
@property (weak, nonatomic) IBOutlet UIView *viewPackageDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageDetailsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbltypeOfPackageTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblpackageAcceptedTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbltypeOfPackage;
@property (weak, nonatomic) IBOutlet UILabel *lblpackageAccepted;


//Meeting Point Details

@property (weak, nonatomic) IBOutlet UIView *viewMeetingPointDetails;
@property (weak, nonatomic) IBOutlet UIView *viewMeetingPointDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingPointDetailsTitle;

//@property (weak, nonatomic) IBOutlet UILabel *lblFromMeetPointAddrsssTitle;
//@property (weak, nonatomic) IBOutlet UILabel *lblToMeetPointAddrsssTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFromMeetPointAddrsss;
@property (weak, nonatomic) IBOutlet UILabel *lblToMeetPointAddrsss;

@property (weak, nonatomic) IBOutlet UIImageView *imgMeetingPoint;





@end

@implementation ItineraryInfoController
@synthesize topNavbarView,topBarHeightConstraint;

- (void)viewDidLoad {
    self.automaticallyAdjustsScrollViewInsets = NO;
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToShowInfo];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
}

- (IBAction)backTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"Trip_Details", nil) capitalizedString]];
    
    //Trip Details
    
    [self.viewTripDetails makeCornerRound:15.0];
    [self.viewTripDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Package Details
    
    [self.viewPackageDetails makeCornerRound:15.0];
    [self.viewPackageDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Meeting Point Details
    
    [self.viewMeetingPointDetails makeCornerRound:15.0];
    [self.viewMeetingPointDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
}

//Round specific corner Function => in some cases issue found view not loaded properly and got design issue .Thats why specific corner round we have to make after view already appear.

-(void)roundAllSpecificCorner
{
    [self.viewTripDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewPackageDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
   [self.viewMeetingPointDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
   
}

-(void)setGesture{
    UITapGestureRecognizer *meetPointTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(meetPointPopUpTapGesture:)];
    [self.imgMeetingPoint addGestureRecognizer:meetPointTap];
}

-(void)setLocalization
{
    //Trip Details
    _lblTripDetailsTitle.text = NSLocalizedString(@"Trip_Details", nil);
    _lblModeOfTransportTitle.text = NSLocalizedString(@"Flight", nil);
    _lblTransportRefNoTitle.text = NSLocalizedString(@"Flight_Number", nil);
    _lblTripIdTitle.text = NSLocalizedString(@"Trip_ID", nil);
    
    //Package Details
    _lblPackageDetailsTitle.text = NSLocalizedString(@"Carrying_Item_Details", nil);
    _lbltypeOfPackageTitle.text = NSLocalizedString(@"Item_Category", nil);
    _lblpackageAcceptedTitle.text = NSLocalizedString(@"Item_Accepted", nil);
    
    //Meeting point Details
    
    _lblMeetingPointDetailsTitle.text = NSLocalizedString(@"Meeting_Point_Details", nil);
    
}

#pragma mark - Gesture Action

- (void)meetPointPopUpTapGesture:(UITapGestureRecognizer*)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MeetPopUpViewController *meetPopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"MeetPopUpVCID"];
    
    meetPopUpVC.headerTitleText = NSLocalizedString(@"AddTrip_MeetingPoint", nil);
    meetPopUpVC.bottomTitleText = NSLocalizedString(@"Meet_The_Sender_Receiver", nil);
    meetPopUpVC.bottomDescriptionText = NSLocalizedString(@"AddTrip_Meetpoint_Sender_Message", nil);
    meetPopUpVC.singleActionLabelTitleText = NSLocalizedString(@"OK", nil);
    meetPopUpVC.imageIconName = @"MeetingPoint_Blue_icon";
    
    self.definesPresentationContext = YES; //self is presenting view controller
    meetPopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    meetPopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:meetPopUpVC animated:YES completion:nil];
}

#pragma mark - API Call

- (void)callApiToShowInfo{
    /*
     Request parameter:
     userId, sessionId, os, osVersion, appVersion, itineraryId
     
     Request example :
     http://103.15.232.68/~angeler/web_services/itinerary/itinerary_details?userId=17&sessionId=8dc3535b3a1f36d0343d4664d53d693fe6f16ab4&os=Android&osVersion=M&appVersion=1.0&itineraryId=9

     */
    
    
    NSDictionary *param = @{@"itineraryId":_itenaryId
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryInfo
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
   success:^(id responseObject){
       NSLog(@"response ===   %@",responseObject);
       if ([GlobalInfo checkStatusSuccess:responseObject])  {
           [self showInfoWithData:[responseObject objectForKey:@"responseData"]];
       }
   }
   failure:^(id responseObject){
       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
   }];
}
- (void)showInfoWithData:(NSDictionary*)data{
    
    //Trip Details Set Up
    
    // Pick Up
    _lblFromCityCountry.text = [NSString stringWithFormat:@"%@, %@",[data objectForKey:@"departureCityName"],[data  objectForKey:@"departureCountryName"]];
  
    
    _lblFromDateTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"departureDate"] Time:[data objectForKey:@"departureTime"]];
    
    
    //drop Off
    
      _lblToCityCountry.text = [NSString stringWithFormat:@"%@, %@",[data objectForKey:@"arrivalCityName"],[data  objectForKey:@"arrivalCountryName"]];


    _lblToDateTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"arrivalDate"] Time:[data objectForKey:@"arrivalTime"]];
    
    _lblModeOftransport.text = [GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]];
    _lblTransportRefNo.text = [data objectForKey:@"travelRefNo"];
    _lblTripID.text = [data objectForKey:@"tripUniqueId"];
    
    //Package Details
    
     _lbltypeOfPackage.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];
    _lblpackageAccepted.text = [[data objectForKey:@"packagesAcceptedCount"] stringValue];
    
    //Meeting Point Details
    
    NSDictionary *services = [data objectForKey:@"services"];
    
    _lblFromMeetPointAddrsss.text = [services objectForKey:@"pickupMPAddress"];
    _lblToMeetPointAddrsss.text =  [services objectForKey:@"dropoffMPAddress"];
    

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
//========================== Old + Reuse Code =======================================
/*
 String TAG_ITINERY_INFO_URL_services="services";
 String TAG_ITINERY_INFO_URL_pickupMPAddress="pickupMPAddress";
 String TAG_ITINERY_INFO_URL_pickupPointAddress="pickupPointAddress";
 String TAG_ITINERY_INFO_URL_pickupPremium="pickupPremium";
 String TAG_ITINERY_INFO_URL_dropoffMPAddress="dropoffMPAddress";
 String TAG_ITINERY_INFO_URL_dropoffPointAddress="dropoffPointAddress";
 String TAG_ITINERY_INFO_URL_dropoffPremium="dropoffPremium";
 */
/*
 /*@property (weak, nonatomic) IBOutlet UIView *headerView;
 //@property (weak, nonatomic) IBOutlet UIScrollView *scrollContent;
 @property (weak, nonatomic) IBOutlet UIImageView *imgTravelMode;

 //bar 2
 @property (weak, nonatomic) IBOutlet UILabel *lblHeight;
 @property (weak, nonatomic) IBOutlet UILabel *lblWidth;
 @property (weak, nonatomic) IBOutlet UILabel *lblLength;
 @property (weak, nonatomic) IBOutlet UILabel *lblWeight;
 @property (weak, nonatomic) IBOutlet UILabel *lblType;
 @property (weak, nonatomic) IBOutlet UILabel *lblMaximumUnit;
 @property (weak, nonatomic) IBOutlet UILabel *lblPackageAcceptence;
 
 //pickup
 @property (weak, nonatomic) IBOutlet UIView *viewPickupMeet;
 @property (weak, nonatomic) IBOutlet UILabel *lblPickupMeetPt;
 
 @property (weak, nonatomic) IBOutlet UIView *viewPickupPick;
 @property (weak, nonatomic) IBOutlet UILabel *lblPickupPickPt1;
 @property (weak, nonatomic) IBOutlet UILabel *lblPickupPickPt2;
 @property (weak, nonatomic) IBOutlet UILabel *lblPickupPickPt3;
 
 @property (weak, nonatomic) IBOutlet UIView *viewPickupPremium;
 
 //dropoff
 
 @property (weak, nonatomic) IBOutlet UIView *viewDropoffMeet;
 @property (weak, nonatomic) IBOutlet UILabel *lblDropOffMeetPt;
 
 
 @property (weak, nonatomic) IBOutlet UIView *viewDropoffDrop;
 @property (weak, nonatomic) IBOutlet UILabel *lblDropOffPickPt1;
 @property (weak, nonatomic) IBOutlet UILabel *lblDropOffPickPt2;
 @property (weak, nonatomic) IBOutlet UILabel *lblDropOffPickPt3;
 
 @property (weak, nonatomic) IBOutlet UIView *viewDropPremium;
 
 */

/* [Helper addShadowToLayer:_headerView.layer];
 // Do any additional setup after loading the view from its nib.
 [Helper makeItRound:[_lblPickupMeetPt.superview viewWithTag:100].layer withRadius:8];
 [Helper makeItRound:[_lblPickupPickPt1.superview viewWithTag:100].layer withRadius:8];
 [Helper makeItRound:[_lblPickupPickPt2.superview viewWithTag:100].layer withRadius:8];
 [Helper makeItRound:[_lblPickupPickPt3.superview viewWithTag:100].layer withRadius:8];
 [Helper makeItRound:[_lblDropOffMeetPt.superview viewWithTag:100].layer withRadius:8];
 [Helper makeItRound:[_lblDropOffPickPt1.superview viewWithTag:100].layer withRadius:8];
 [Helper makeItRound:[_lblDropOffPickPt2.superview viewWithTag:100].layer withRadius:8];
 [Helper makeItRound:[_lblDropOffPickPt3.superview viewWithTag:100].layer withRadius:8];
 */

/*//    _lblHeight.text = [data objectForKey:@"packageHeightRange"];
 //    _lblWidth.text = [data objectForKey:@"packageWidthRange"];
 //    _lblLength.text = [data objectForKey:@"packageLengthRange"];
 //    _lblWeight.text = [data objectForKey:@"packageMaxWeight"];
 //
 //    _lblMaximumUnit.text = [data objectForKey:@"packageMaxUnit"];
 //    _lblPackageAcceptence.text = [[data objectForKey:@"packagesAcceptedCount"] stringValue]; */
/*//    if (services.allKeys.count > 0) {
 //        if (![services.allKeys containsObject:@"pickupMPAddress"]) {
 //            [GlobalInfo hideViewNamed:_viewPickupMeet withTopName:@"pickupMeetTop"];
 //        }else{
 //            _lblPickupMeetPt.text = [services objectForKey:@"pickupMPAddress"];
 //        }
 //        if (![services.allKeys containsObject:@"pickupPointAddress"]) {
 //            [GlobalInfo hideViewNamed:_viewPickupPick withTopName:@"pickupPickTop"];
 //        }else{
 //            NSArray *address = [services objectForKey:@"pickupPointAddress"];
 //            if (address.count == 1) {
 //                _lblPickupPickPt1.text = [address firstObject];
 //                [GlobalInfo hideViewNamed:_lblPickupPickPt2.superview withTopName:@"pickupPickTop2"];
 //                [GlobalInfo hideViewNamed:_lblPickupPickPt3.superview withTopName:@"pickupPickTop3"];
 //            }else if (address.count == 2){
 //                _lblPickupPickPt1.text = [address firstObject];
 //                _lblPickupPickPt2.text = [address lastObject];
 //                [GlobalInfo hideViewNamed:_lblPickupPickPt3.superview withTopName:@"pickupPickTop3"];
 //            }else{
 //                _lblPickupPickPt1.text = [address firstObject];
 //                _lblPickupPickPt2.text = [address objectAtIndex:1];
 //                _lblPickupPickPt3.text = [address lastObject];
 //            }
 //        }
 //        if (![services.allKeys containsObject:@"pickupPremium"]) {
 //            [GlobalInfo hideViewNamed:_viewPickupPremium withTopName:@"pickupPremiumTop"];
 //        }
 //
 //        if (![services.allKeys containsObject:@"dropoffMPAddress"]) {
 //            [GlobalInfo hideViewNamed:_viewDropoffMeet withTopName:@"dropoffMeetTop"];
 //        }else{
 //            _lblDropOffMeetPt.text = [services objectForKey:@"dropoffMPAddress"];
 //        }
 //        if (![services.allKeys containsObject:@"dropoffPointAddress"]) {
 //            [GlobalInfo hideViewNamed:_viewDropoffDrop withTopName:@"dropoffDropTop"];
 //        }else{
 //            NSArray *address = [services objectForKey:@"dropoffPointAddress"];
 //            if (address.count == 1) {
 //                _lblDropOffPickPt1.text = [address firstObject];
 //                [GlobalInfo hideViewNamed:_lblDropOffPickPt2.superview withTopName:@"dropoffDropTop2"];
 //                [GlobalInfo hideViewNamed:_lblDropOffPickPt3.superview withTopName:@"dropoffDropTop3"];
 //            }else if (address.count == 2){
 //                _lblDropOffPickPt1.text = [address firstObject];
 //                _lblDropOffPickPt2.text = [address lastObject];
 //                [GlobalInfo hideViewNamed:_lblDropOffPickPt3.superview withTopName:@"dropoffDropTop3"];
 //            }else{
 //                _lblDropOffPickPt1.text = [address firstObject];
 //                _lblDropOffPickPt2.text = [address objectAtIndex:1];
 //                _lblDropOffPickPt3.text = [address lastObject];
 //            }
 //        }
 //        if (![services.allKeys containsObject:@"dropoffPremium"]) {
 //            [GlobalInfo hideViewNamed:_viewDropPremium withTopName:@"dropoffPremiumTop"];
 //        }
 //    } */



//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    //NSLog(@"fff 11 ==  %@",NSStringFromCGPoint(scrollView.contentOffset));
//    if (scrollView.contentOffset.y == 0) {
//        [scrollView setContentOffset:CGPointMake(0, -20) animated:YES];
//    }
//}
