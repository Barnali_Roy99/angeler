//
//  ItineraryCell.h
//  Angeler
//
//  Created by AJAY on 22/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItineraryCell : UITableViewCell
//@property (nonatomic, weak) IBOutlet UIView *sepratorView;
//
//@property (weak, nonatomic) IBOutlet UIImageView *imgTravelMode;
//
//
//@property (weak, nonatomic) IBOutlet UILabel *lblArrival;
//@property (weak, nonatomic) IBOutlet UILabel *lblPackage;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;



@property (weak, nonatomic) IBOutlet UIView *requestPriceView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reqPriceViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblParcel;

@property (weak, nonatomic) IBOutlet UILabel *lblRequest;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblJourneyDetials;
@property (weak, nonatomic) IBOutlet UILabel *lblDepart;
@property (weak, nonatomic) IBOutlet UIView *viewPrice;

@property (weak, nonatomic) IBOutlet UIView *menu_Container;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnShowTripDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

//@property (weak, nonatomic) IBOutlet UIButton *btnRequest;
@property (weak, nonatomic) IBOutlet UIView *senderProfileView;
@property (weak, nonatomic) IBOutlet UIImageView *senderProfileImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *requestPriceViewBottomConstraint;


@end
