//
//  ItineraryDropOffController.m
//  Angeler
//
//  Created by AJAY on 02/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ItineraryDropOffController.h"
#import "MTBBarcodeScanner.h"

@interface ItineraryDropOffController (){
    BOOL foundCode;
}
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *viPackDetail;
@property (weak, nonatomic) IBOutlet UIView *viPickLocation;
@property (weak, nonatomic) IBOutlet UIView *viDeliveryLocation;
@property (weak, nonatomic) IBOutlet UIView *viSenderDetail;
@property (weak, nonatomic) IBOutlet UIView *viRecipientDetail;
@property (weak, nonatomic) IBOutlet UIView *viPickupPtDetail;
@property (weak, nonatomic) IBOutlet UIView *viDeliveryPtDetail;
@property (weak, nonatomic) IBOutlet UIView *viEnterDeliveryCode;
@property (weak, nonatomic) IBOutlet UIView *viHasCode;
@property (weak, nonatomic) IBOutlet UIView *viFinalStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPackType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackSize;
@property (weak, nonatomic) IBOutlet UILabel *lblPackWeight;

@property (weak, nonatomic) IBOutlet UILabel *lblPickupLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgPickType;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropType;

@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderMail;
@property (weak, nonatomic) IBOutlet UIImageView *imgSenderImage;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhone;

@property (weak, nonatomic) IBOutlet UILabel *lblRecipientName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientMail;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryBy;

@property (weak, nonatomic) IBOutlet UILabel *lblPickPtAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPickPtAddressType;
@property (weak, nonatomic) IBOutlet UILabel *lblPickPtName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickPtPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblPickPtOperatingHours;

@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtAddressType;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtName;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtOperatingHours;

@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryCode;
@property (weak, nonatomic) IBOutlet UIImageView *imgQrCode;

@property (weak, nonatomic) IBOutlet UITextField *txtQrCode;

@property (nonatomic, strong) MTBBarcodeScanner *scannerMT;
@property (weak, nonatomic) IBOutlet UIView *scannerView;

@property (weak, nonatomic) IBOutlet UIButton *btnDropOff;
@property (weak, nonatomic) IBOutlet UILabel *lblFinalStatusText;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reciepientCallerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *senderCallerHeightConstraint;

//@property (nonatomic) BOOL foundCode;
@end

@implementation ItineraryDropOffController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_buttonTitle == DROPOFF) {
        _lblTitle.text = strKey_itinery_dropoff_info_page_title;
        _viFinalStatus.hidden = YES;
        [GlobalInfo hideViewNamed:_viPickLocation withTopName:@"pickLocationTop"];
        [GlobalInfo hideViewNamed:_viPickupPtDetail withTopName:@"pickPointTop"];
        [GlobalInfo hideViewNamed:_viFinalStatus withTopName:@"finalStatusTop"];
    }else{
        [GlobalInfo hideViewNamed:_viPickupPtDetail withTopName:@"pickPointTop"];
        [GlobalInfo hideViewNamed:_viHasCode withTopName:@"qrImageTop"];
        [GlobalInfo hideViewNamed:_viEnterDeliveryCode withTopName:@"deliveryCodeTop"];
        _btnDropOff.hidden = YES;
        _lblTitle.text = strKey_itinery_delivery_info_page_title;
    }
    // Do any additional setup after loading the view from its nib.
    [Helper addShadowToLayer:_headerView.layer];
    [Helper addShadowToLayer:_viPackDetail.layer];
    [Helper addShadowToLayer:_viPickLocation.layer];
    [Helper addShadowToLayer:_viDeliveryLocation.layer];
    [Helper addShadowToLayer:_viSenderDetail.layer];
    [Helper addShadowToLayer:_viRecipientDetail.layer];
    [Helper addShadowToLayer:_viPickupPtDetail.layer];
    [Helper addShadowToLayer:_viDeliveryPtDetail.layer];
    [Helper addShadowToLayer:_viEnterDeliveryCode.layer];
    [Helper addShadowToLayer:_viHasCode.layer];
    [Helper addShadowToLayer:_viFinalStatus.layer];
   
    [self callDropOffApiToFetchInfoWithBookingId];
    _scannerMT = [[MTBBarcodeScanner alloc] initWithPreviewView:self.scannerView];
}

- (IBAction)backTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)callDropOffApiToFetchInfoWithBookingId{
    //Request parameter:
//    userId, sessionId, os, osVersion, appVersion, bookingId
//    
//    Request example :
//http://103.15.232.68/~angeler/web_services/itinerary/dropoff_details?userId=17&sessionId=c5d72abe169663362dcea576c8469a53ec6045c4&os=Android&osVersion=M&appVersion=1.0&bookingId=1

    
    NSDictionary *param = @{@"bookingId":_bookingId,
                            @"caller":_serviceCaller
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryDropDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject])  {
               [self updateValuesWithData:[responseObject objectForKey:@"responseData"]];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}

- (void)updateValuesWithData:(NSDictionary*)data{
    NSDictionary *pickupPointData = [data objectForKey:@"pickupPointDetails"];
    if ([pickupPointData allKeys].count == 0) {
        [GlobalInfo hideViewNamed:_viPickupPtDetail withTopName:@"pickPointTop"];
    }else{
        
        _lblPickPtAddress.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address: %@",[pickupPointData objectForKey:@"address"]] andIndex:8];
        
        _lblPickPtAddressType.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[pickupPointData objectForKey:@"addressType"]] andIndex:12];
        
        _lblPickPtName.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Name: %@",[pickupPointData objectForKey:@"contactName"]] andIndex:5];
        
        _lblPickPtPhone.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Phone: %@%@",[pickupPointData objectForKey:@"contactPhoneISD"],[pickupPointData objectForKey:@"contactPhone"]] andIndex:6];
        
        _lblPickPtOperatingHours.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Operating hours: %@-%@",[pickupPointData objectForKey:@"operatingFromHours"],[pickupPointData objectForKey:@"operatingToHours"]] andIndex:16];
        
    }

    
    NSDictionary *deliveryPointData = [data objectForKey:@"deliveryPointDetails"];
    if ([deliveryPointData allKeys].count == 0) {
        [GlobalInfo hideViewNamed:_viDeliveryPtDetail withTopName:@"deliveryPointTop"];
    }else{
        
        _lblDeliveryPtAddress.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address: %@",[deliveryPointData objectForKey:@"address"]] andIndex:8];
        
        _lblDeliveryPtAddressType.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[deliveryPointData objectForKey:@"addressType"]] andIndex:12];
        
        _lblDeliveryPtName.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Name: %@",[deliveryPointData objectForKey:@"contactName"]] andIndex:5];
        
        _lblDeliveryPtPhone.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Phone: %@%@",[deliveryPointData objectForKey:@"contactPhoneISD"],[deliveryPointData objectForKey:@"contactPhone"]] andIndex:6];
        
        _lblDeliveryPtOperatingHours.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Operating hours: %@-%@",[deliveryPointData objectForKey:@"operatingFromHours"],[deliveryPointData objectForKey:@"operatingToHours"]] andIndex:16];
        
    }
    
    _lblPackType.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];
    _lblPackSize.text = [NSString stringWithFormat:@"%@x%@x%@ CM",[data objectForKey:@"packageHeight"],[data objectForKey:@"packageWidth"],[data objectForKey:@"packageLength"]];
    _lblPackWeight.text = [NSString stringWithFormat:@"%@ GM",[data objectForKey:@"packageWeight"]];
    
    _lblPickupLocation.text = [data objectForKey:@"angelerPickupAddress"];
    _imgPickType.image = [GlobalInfo AngelerImageForPickupType:[data objectForKey:@"angelerPickupType"]];
    
    _lblDeliveryLocation.text = [data objectForKey:@"angelerDropoffAddress"];
    _imgDropType.image = [GlobalInfo AngelerImageForPickupType:[data objectForKey:@"angelerDropoffType"]];
    
    _lblSenderName.text = [data objectForKey:@"senderName"];
    _lblSenderMail.text = [data objectForKey:@"senderEmail"];
    [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    [Helper makeItRound:_imgSenderImage.layer withRadius:25];
    
    if ([[data objectForKey:@"senderPhone"] isEqualToString:Empty]) {
        _senderCallerHeightConstraint.constant = 0;
    }else{
        _lblSenderPhone.text = [data objectForKey:@"senderPhone"];
    }
    
    _lblRecipientName.text = [data objectForKey:@"recipientName"];
    _lblRecipientMail.text = [data objectForKey:@"recipientEmail"];
    if ([[data objectForKey:@"recipientPhone"] isEqualToString:Empty]) {
        _reciepientCallerHeightConstraint.constant = 0;
    }else{
        _lblRecipientPhone.text = [NSString stringWithFormat:@"+%@%@",[data objectForKey:@"recipientPhoneISD"],[data objectForKey:@"recipientPhone"]];
    }
    _lblDeliveryBy.text = [NSString stringWithFormat:@"%@, %@",[data objectForKey:@"deliveryDate"],[data objectForKey:@"deliveryTime"]];
    
    if ([[data objectForKey:@"showCodeScanner"] isEqualToString:@"Yes"]) {
        _viHasCode.hidden = YES;
        _viEnterDeliveryCode.hidden = NO;
        _viFinalStatus.hidden = YES;
     
    }else{
        
        _viHasCode.hidden = NO;
        _viEnterDeliveryCode.hidden = YES;
        _viFinalStatus.hidden = NO;
    }
    if (![[data objectForKey:@"code"] isEqualToString:Empty]) {
        _viHasCode.hidden = NO;
        _imgQrCode.image = [self qrImageForCode:[data objectForKey:@"code"] andImageView:_imgQrCode];
        _lblDeliveryCode.text = [NSString stringWithFormat:@"Delivery code: %@",[data objectForKey:@"code"]];
    }else{
        _lblDeliveryCode.text = Empty;
    }
    _lblFinalStatusText.attributedText = [GlobalInfo formatFinalStatusText:[NSString stringWithFormat:@"Status: %@",[data objectForKey:@"finalStatusText"]] andIndex:7];
    
}

- (UIImage*)qrImageForCode:(NSString*)qrString andImageView:(UIImageView*)qrImageView{
    //    NSString *qrString = @"My string to encode";
    NSData *stringData = [qrString dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = qrImageView.frame.size.width / qrImage.extent.size.width;
    float scaleY = qrImageView.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    return [UIImage imageWithCIImage:qrImage scale:[UIScreen mainScreen].scale
                         orientation:UIImageOrientationUp];
}

- (IBAction)dropOffTapped:(id)sender {
    if ([Helper checkEmptyField:_txtQrCode.text]) {
        [Helper showAlertWithTitle:Empty Message:strKey_itinery_pickup_qr_code_required];
        return;
    }
    
    //    Request parameter:
    //    userId, sessionId, os, osVersion, appVersion, bookingId, code
    
    //http://103.15.232.68/~angeler/web_services/itinerary/update_dropoff_status?userId=17&sessionId=8dc3535b3a1f36d0343d4664d53d693fe6f16ab4&os=Android&osVersion=M&appVersion=1.0&bookingId=1&code=RL1dTzfm
    
    NSDictionary *param = @{@"bookingId":_bookingId,
                            @"code":_txtQrCode.text
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUpdateDropOffStatus
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
               UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                   [self backTapped:nil];
               }];
               [GlobalInfo showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
    
}
- (IBAction)scanDropOffQrBackTapped:(id)sender{
    _scannerView.hidden = YES;
    [self.scannerMT stopScanning];
}

- (IBAction)scanDropOffQrTapped:(id)sender {
    _scannerView.hidden = NO;
    foundCode = NO;
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            NSError* error = nil;
            [self.scannerMT startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                if (!foundCode) {
                    _txtQrCode.text = code.stringValue;
                    foundCode = YES;
                }
                _scannerView.hidden = YES;
                [self.scannerMT stopScanning];
            } error:&error];
        } else {
            // The user denied access to the camera
            [self displayPermissionMissingAlert];
        }
    }];
}

#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    [Helper showAlertWithTitle:Empty Message:@"Scanning Unavailable"];
}

- (IBAction)callSender:(id)sender {
    if (_lblSenderPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblSenderPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}
- (IBAction)callRecipient:(id)sender {
    if (_lblRecipientPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblRecipientPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
