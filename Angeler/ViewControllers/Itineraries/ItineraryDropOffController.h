//
//  ItineraryDropOffController.h
//  Angeler
//
//  Created by AJAY on 02/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItineraryDropOffController : UIViewController
@property (nonatomic, strong) NSString *bookingId;
@property (nonatomic) ITINERARY_REQUEST_TITLE buttonTitle;
@property (nonatomic, strong) NSString *serviceCaller;
@end
