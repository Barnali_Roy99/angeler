//
//  ItineraryCell.m
//  Angeler
//
//  Created by AJAY on 22/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ItineraryCell.h"

@implementation ItineraryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.containerView makeCornerRound:15.0];
    self.containerView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 0);
    self.containerView.layer.shadowOpacity = 2.0;
    self.containerView.layer.shadowRadius = 2.0;
    
    self.viewPrice.layer.borderColor = [[UIColor blackColor] CGColor];
    self.viewPrice.layer.borderWidth = 2.0;
    
    [self.senderProfileView makeRoundwithBoarderColor:[UIColor blackColor] borderWidth:1.0];
    
//    self.sepratorView.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.sepratorView.layer.shadowOffset = CGSizeMake(0, 0);
//    self.sepratorView.layer.shadowOpacity = 1.0;
//    self.sepratorView.layer.shadowRadius = 1.0;
    
//    [Helper addBorderToLayer:_btnDelete.layer];
//    [Helper addBorderToLayer:_btnShowTripDetails.layer];
//   
    
    [self.menu_Container dropShadowWithColor:[UIColor lightishGrayColor]];
    
    [self.btnShowTripDetails setTitle:[NSString stringWithFormat:@"    %@",
     [NSLocalizedString(@"Trip_Details", nil) capitalizedString]] forState: UIControlStateNormal];
    [self.btnDelete setTitle:[NSString stringWithFormat:@"    %@",
                              NSLocalizedString(@"Delete", nil)]  forState: UIControlStateNormal];
   
    

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
