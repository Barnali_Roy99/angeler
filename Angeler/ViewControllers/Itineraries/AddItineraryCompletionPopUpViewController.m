//
//  AddItineraryCompletionPopUpViewController.m
//  Angeler
//
//  Created by AJAY on 22/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "AddItineraryCompletionPopUpViewController.h"
#import "MyTripsListViewController.h"
#import "AddTripStepOneController.h"

@interface AddItineraryCompletionPopUpViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@end

@implementation AddItineraryCompletionPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    ROUNDEDCORNER(_viewContainer.layer);
    [Helper addShadowToLayer:_viewContainer.layer];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)shareItinerary:(id)sender {
}
- (IBAction)addAnotherItinerary:(id)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
    
    MyTripsListViewController *managObj = [[MyTripsListViewController alloc] initWithNibName:@"MyTripsListViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
