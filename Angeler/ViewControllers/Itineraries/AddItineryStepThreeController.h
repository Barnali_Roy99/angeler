//
//  AddItineryStepThreeController.h
//  Angeler
//
//  Created by AJAY on 07/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ItineryStepThreeDelegate <NSObject>

//- (void)passItineryObjFromThree:(ItineraryClass*)itineryObj;

@end
@interface AddItineryStepThreeController : UIViewController
//@property (nonatomic, weak) id <ItineryStepThreeDelegate> delegate;
@property (nonatomic,strong) ItineraryClass *Itinerar3rdStepObj;
@end
