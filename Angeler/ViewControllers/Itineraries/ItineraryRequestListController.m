//
//  ItineraryRequestListController.m
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ItineraryRequestListController.h"
#import "ItineraryRequestListCell.h"
#import "ItineraryPackageInfoController.h"
#import "ItineraryPickUpController.h"
#import "ItineraryDropOffController.h"
#import "MWPhotoBrowser.h"

static NSString *CellIdentifier = @"ItineraryRequestListCell";

@interface ItineraryRequestListController ()<MWPhotoBrowserDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
}


@property (weak, nonatomic) IBOutlet UITableView *tblItineraryReqList;
@property (nonatomic, strong) NSMutableArray *arrRequestList;

@property (weak, nonatomic) IBOutlet UIView *viewTripDetails;
@property (weak, nonatomic) IBOutlet UIView *viewTripDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDetailsTitle;

@property (weak, nonatomic) IBOutlet UIView *viewPackageDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageDetailsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblPickUpCityCountry;
//@property (weak, nonatomic) IBOutlet UILabel *lblPickUpCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpDateTime;

@property (weak, nonatomic) IBOutlet UILabel *lblDropOffCityCountry;
//@property (weak, nonatomic) IBOutlet UILabel *lblDropOffCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffDateTime;

@property (weak, nonatomic) IBOutlet UILabel *lblTravelRefNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageType;

@property (nonatomic, strong) NSMutableArray *arrPhotos;


@end

@implementation ItineraryRequestListController
@synthesize topNavbarView,topBarHeightConstraint;

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToLoadItineraryRequestList];
    
   // Do any additional setup after loading the view from its nib.
}


-(void)viewDidAppear:(BOOL)animated
{
  
    [self roundAllSpecificCorner];
}


#pragma mark - Referesh Action

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToLoadItineraryRequestList];
    [refreshController endRefreshing];
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:NSLocalizedString(@"Trip_Item_List", nil)];
    
    //Trip Details
    
    [self.viewTripDetails makeCornerRound:15.0];
    [self.viewTripDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    //Pcakage Details
    
  
    [_tblItineraryReqList registerNib:[UINib nibWithNibName:@"ItineraryRequestListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblItineraryReqList.estimatedRowHeight = 155;
    _tblItineraryReqList.rowHeight = UITableViewAutomaticDimension;
   
    [_tblItineraryReqList setNeedsLayout];
    [_tblItineraryReqList layoutIfNeeded];
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblItineraryReqList addSubview:refreshController];
    

}

-(void)roundAllSpecificCorner
{
    [self.viewTripDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewPackageDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    
    
}

-(void)setLocalization
{
    _lblTripDetailsTitle.text = NSLocalizedString(@"Trip_Details", nil);
    _lblPackageDetailsTitle.text = NSLocalizedString(@"Item_Details", nil);
}

- (void)SetValuesWithData:(NSDictionary*)data{
    //Pick Up

    
    _lblPickUpCityCountry.text = [NSString stringWithFormat:@"%@, %@",[data objectForKey:@"departureCityName"],[data  objectForKey:@"departureCountryName"]];
    _lblPickUpDateTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"departureDate"] Time:[data objectForKey:@"departureTime"]];



    //Drop Off

   
    _lblDropOffCityCountry.text = [NSString stringWithFormat:@"%@, %@",[data objectForKey:@"arrivalCityName"],[data  objectForKey:@"arrivalCountryName"]];
    _lblDropOffDateTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"arrivalDate"] Time:[data objectForKey:@"arrivalTime"]];

  
    _lblTravelRefNumber.text = [data objectForKey:@"travelRefNo"];
    _lblPackageType.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];

}

#pragma mark - API Call

- (void)callApiToLoadItineraryRequestList{

        NSDictionary *param = @{@"itineraryId":self.itenaryData?[self.itenaryData objectForKey:@"itineraryId"]:Empty,
                                @"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                                @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kItineraryRequestList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           NSArray *arr = [responseObject objectForKey:@"responseData"];
           if ((arr.count == 0) && (START_POSITION != 0)) {
               moreRecordsAvailable = NO;
               return ;
           }
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               
               if (_arrRequestList.count > 0) {
                   if (START_POSITION == 0) {
                       [_arrRequestList removeAllObjects];
                   }else if(_arrRequestList.count > START_POSITION){
                       [_arrRequestList removeObjectsInRange:NSMakeRange(START_POSITION, _arrRequestList.count - START_POSITION)];
                   }
                   [_arrRequestList addObjectsFromArray:arr];
               }else{
                   _arrRequestList = [NSMutableArray arrayWithArray:arr];
               }
               START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
              // _arrRequestList = [responseObject objectForKey:@"responseData"];
               [self SetValuesWithData:[[_arrRequestList firstObject] objectForKey:@"itineraryDetails"]];
//               _lblNoRecord.hidden = _arrRequestList.count > 0;
               [_tblItineraryReqList reloadData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];


}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arrRequestList.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *cellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil)
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//
//
//    cell.textLabel.text = [NSString stringWithFormat:@"Cell Number %li",indexPath.row];

    ItineraryRequestListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];


    [cell.containerView makeCornerRound:15.0];
    [cell.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [cell.btnStatusInfo makeCornerRound:15.0];


    NSDictionary *req_data = [[_arrRequestList objectAtIndex:indexPath.row] objectForKey:@"requestDetails"];
    cell.lblPickup.text = [req_data objectForKey:@"angelerPickupAddress"];
    cell.lblDropOff.text = [req_data objectForKey:@"angelerDropoffAddress"];
    cell.lblCost.text = [req_data objectForKey:@"totalCost"];
    cell.btnStatusInfo.tag = indexPath.row;
    [cell.btnStatusInfo setTitle:[req_data objectForKey:@"activeButtonLabel"] forState:UIControlStateNormal];
    if([[[req_data objectForKey:@"activeButtonLabel"] lowercaseString] isEqualToString:@"delivered"])
    {
        cell.deliveredImageView.hidden = NO;
    }
    else
    {
        cell.deliveredImageView.hidden = YES;
    }

    //PackageInfoTapped
    [cell.btnStatusInfo addTarget:self action:@selector(PackageInfoTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.lblDocument.text = [NSString stringWithFormat:@"%@ | %@ %@",[GlobalInfo TypeOfPackage:[[req_data objectForKey:@"packageType"] integerValue]],[req_data objectForKey:@"packageWeight"],NSLocalizedString(@"package_weight_unit", nil)];

    cell.customerImageView.tag = indexPath.row + 1;
    [cell.customerImageView sd_setImageWithURL:[NSURL URLWithString: [req_data objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
    cell.customerImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    cell.customerImageView.layer.borderWidth = 1.0;
    [cell.customerImageView setUserInteractionEnabled:YES];

    UITapGestureRecognizer *customerImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(customerImageViewTapped:)];
    customerImageViewTap.numberOfTapsRequired = 1;
    [cell.customerImageView addGestureRecognizer:customerImageViewTap];

    if ([[req_data objectForKey:@"serviceType"] isEqualToString:serviceType_Premium])
    {
        [cell.meetPremiumImageIcon setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
    }
    else
    {
        [cell.meetPremiumImageIcon setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
    }


    //Load Moore Data

    if (indexPath.row == [self.arrRequestList count] - 1 && moreRecordsAvailable)
    {
        [self callApiToLoadItineraryRequestList];
    }

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}


#pragma mark - Table view action

- (void)customerImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIImageView *customerImageView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrRequestList objectAtIndex:customerImageView.tag - 1];
    self.arrPhotos = [[NSMutableArray alloc] init];
    MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[[data objectForKey:@"requestDetails"] objectForKey:@"senderImage"]]];
    photo.caption = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Sender", nil),[[data objectForKey:@"requestDetails"] objectForKey:@"senderName"]];
    [self.arrPhotos addObject:photo];
    [self setMWPHOtoBrowser];
}

- (void)PackageInfoTapped:(UIButton*)sender{
    START_POSITION = sender.tag;
    ItineraryPackageInfoController *next = [[ItineraryPackageInfoController alloc] initWithNibName:@"ItineraryPackageInfoController" bundle:nil];
    next.bookingId = [[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"bookingId"];
    //    next.buttonTitle = PACKAGE_INFO;
    // next.status = [[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"status"];
    [self.navigationController pushViewController:next animated:YES];
}

- (void)PickupTapped:(UIButton*)sender{
    START_POSITION = sender.tag;
    ItineraryPickUpController *next = [[ItineraryPickUpController alloc] initWithNibName:@"ItineraryPickUpController" bundle:nil];
    next.bookingId = [[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"bookingId"];
    [self.navigationController pushViewController:next animated:YES];
}



#pragma mark - MWPhotoBrowser

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

// ======================== Reuse + Old Code =======================

//@property (weak, nonatomic) IBOutlet UIView *headerView;
//@property (weak, nonatomic) IBOutlet UIView *infoView;
/*@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoViewHeightConstraint;
 @property (weak, nonatomic) IBOutlet UILabel *lblPickAddress;
 @property (weak, nonatomic) IBOutlet UILabel *lblDropAddress;
 @property (weak, nonatomic) IBOutlet UIImageView *imgTravelMode;
 @property (weak, nonatomic) IBOutlet UILabel *lblTravelRefNo;
 @property (weak, nonatomic) IBOutlet UILabel *lblDocumentType;

 */

/*//    if (self.itenaryData) {
 //        [self SetValuesWithData:_itenaryData];
 //
 //    }else{
 //        _infoView.hidden = YES;
 //        _infoViewHeightConstraint.constant = 0.0;
 //    }
 //
 [Helper addShadowToLayer:_headerView.layer];
 [Helper addShadowToLayer:_infoView.layer]; */

/*//    if (!self.itenaryData) {
 //        cell.lblDocument.hidden = NO;
 //        cell.lblTravelRefNo.hidden = NO;
 //        cell.imgDocument.hidden = NO;
 //        cell.imgTravelMode.hidden = NO;
 //    }
 //    cell.imgFromType.image = [GlobalInfo AngelerImageForPickupType:[req_data objectForKey:@"pickupType"]];
 //     cell.imgToType.image = [GlobalInfo AngelerImageForPickupType:[req_data objectForKey:@"dropoffType"]];
 //    cell.imgTravelMode.image = [UIImage imageNamed:[NSString stringWithFormat:@"list_%@",[[GlobalInfo travelMode:[[itineryData objectForKey:@"travelMode"] integerValue]] lowercaseString]]];
 //    cell.btnConfirmDeny.tag = indexPath.row;
 //    [cell.btnConfirmDeny addTarget:self action:@selector(ConfirmDenyButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
 ////    cell.btnPackInfoCentered.tag = indexPath.row;
 //    cell.btnPackInfoCentered.hidden = YES;
 ////    [cell.btnPackInfoCentered addTarget:self action:@selector(PackageInfoTapped:) forControlEvents:UIControlEventTouchUpInside];
 //
 //    NSString *status = [req_data objectForKey:@"status"];
 //    NSString *title = Empty;
 //
 //    if ([status isEqualToString:@"Reserved"]) {
 //        title = @"Confirm/Deny";
 //    }
 //    if ([status isEqualToString:@"Accepted"]) {
 //        title = @"Pickup";
 //    }
 //    if ([status isEqualToString:@"Expired"] || [status isEqualToString:@"Rejected"]) {
 //        title = status;
 //    }
 //
 //    if ([status isEqualToString:@"5"] || [status isEqualToString:@"6"]) {
 //        title = @"Dropoff";
 //    }
 //    if ([status isEqualToString:@"7"] || [status isEqualToString:@"8"]) {
 //        title = @"Delivered";
 //    }
 //    if ([status isEqualToString:@"9"]) {
 //        title = @"Rejected";
 //    }
 //    if ([status isEqualToString:@"10"]) {
 //        title = @"Cancelled";
 //    } */

//- (void)ConfirmDenyButtonTapped:(UIButton*)sender{
//    NSString *title = [sender titleForState:UIControlStateNormal];
//    START_POSITION = sender.tag;
//
//
//    if ([title isEqualToString:@"Delivered"] ) {
//        NSString *status = [[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"status"];
//        [self OpenDropOffWithBookingID:[[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"bookingId"] PackageType:DELIVERED andServiceCaller:[status isEqualToString:@"7"]?@"2":@"3"]; //2 for status 7 and 3 for status 8
//    }
//    if ([title isEqualToString:@"Dropoff"]) {
//        [self OpenDropOffWithBookingID:[[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"bookingId"] PackageType:DROPOFF andServiceCaller:@"1"];
//    }
//
//    if ([title isEqualToString:@"Pickup"]) {
//        [self PickupTapped:sender];
//    }
//
//    if ([title isEqualToString:@"Confirm/Deny"]) {
//       [self OpenPackageInfoWithBookingID:[[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"bookingId"] andPackageType:CONFIRM_DENY];
//    }
//    if ([title isEqualToString:@"Expired"] || [title isEqualToString:@"Rejected"] || [title isEqualToString:@"Cancelled"]) {
//        [self OpenPackageInfoWithBookingID:[[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"bookingId"] andPackageType:OTHERS];
//    }
//}
//- (void)OpenPackageInfoWithBookingID:(NSString*)bookingId andPackageType:(ITINERARY_REQUEST_TITLE)packageStatus{
//    ItineraryPackageInfoController *next = [[ItineraryPackageInfoController alloc] initWithNibName:@"ItineraryPackageInfoController" bundle:nil];
//    next.bookingId = bookingId;
//    next.buttonTitle = packageStatus;
//    [self.navigationController pushViewController:next animated:YES];
//}


//- (void)OpenDropOffWithBookingID:(NSString*)bookingId PackageType:(ITINERARY_REQUEST_TITLE)packageStatus andServiceCaller:(NSString*)caller{
//    ItineraryDropOffController *next = [[ItineraryDropOffController alloc] initWithNibName:@"ItineraryDropOffController" bundle:nil];
//    next.bookingId = bookingId;
//    next.buttonTitle = packageStatus;
//    next.serviceCaller = caller;
//
//    [self.navigationController pushViewController:next animated:YES];
//
//}
//- (void)NoActionTapped:(UIButton*)sender{
////    ItineraryDropOffController *next = [[ItineraryDropOffController alloc] initWithNibName:@"ItineraryDropOffController" bundle:nil];
////    next.bookingId = [[[_arrRequestList objectAtIndex:sender.tag] objectForKey:@"requestDetails"] objectForKey:@"bookingId"];
////    [self.navigationController pushViewController:next animated:YES];
//
//    return;
//}
//#pragma mark - Scrollview Delegate
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//
//    // UITableView only moves in one direction, y axis
//    CGFloat currentOffset = scrollView.contentOffset.y;
//    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//
//    //NSInteger result = maximumOffset - currentOffset;
//
//    // Change 10.0 to adjust the distance from bottom
//    if (maximumOffset - currentOffset <= 5.0) {
//        if(moreRecordsAvailable)
//        {
//            [self callApiToLoadItineraryRequestList];
//        }
//    }
//}

