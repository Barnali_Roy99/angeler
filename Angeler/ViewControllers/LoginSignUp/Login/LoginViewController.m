//
//  LoginViewController.m
//  Angeler
//
//  Created by AJAY on 21/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "LoginViewController.h"
#import "IQKeyboardManager.h"
#import "UIView+UIview_General.h"
#import "NormalSignUpController.h"
#import "ForgotPasswordViewController.h"


@interface LoginViewController ()

@property (nonatomic, weak) IBOutlet UITextField *txtUserEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtUserPassword;
@end

@implementation LoginViewController
@synthesize lblTitle,viewEmail,viewPassword,scrollView,viewLogin,viewSignUp;


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}
#pragma mark - General methods

-(void)initialSetUp{
    [self setPlaceHolderText];
    [self setGesture];
    self.navigationController.navigationBarHidden = YES;
    lblTitle.text = NSLocalizedString(@"Be_Paid_To_Travel_And_Store_Send_Easier_Cheaper_Faster", nil);
   
    [viewPassword makeCornerRound:25.0 withBoarderColor:[UIColor whiteColor] borderWidth:2.0];
    [viewEmail makeCornerRound:25.0 withBoarderColor:[UIColor whiteColor] borderWidth:2.0];
    
    [viewLogin makeCornerRound:25.0];
    [viewSignUp makeCornerRound:25.0];
    
    [self setLocalization];
   
}

-(void)setLocalization
{
    [self.btnForgotPassword setTitle:[NSString stringWithFormat:@"%@ ?", NSLocalizedString(@"Forgot_Password", nil)] forState:UIControlStateNormal];
    self.lblLogin.text = NSLocalizedString(@"Login", nil);
    self.lblOr.text = NSLocalizedString(@"OR", nil);
    self.lblSignUp.text = NSLocalizedString(@"Sign_Up", nil);
}

-(void)setPlaceHolderText{
    UIColor *color = [UIColor whiteColor];
    self.txtUserEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password", nil) attributes:@{NSForegroundColorAttributeName: color}];
}

-(void)setGesture{
    UITapGestureRecognizer *loginTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginTapped:)];
    [self.viewLogin addGestureRecognizer:loginTapRecognizer];
    
    UITapGestureRecognizer *signUpTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signUpTapped:)];
    [self.viewSignUp addGestureRecognizer:signUpTapRecognizer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loginTapped:(UITapGestureRecognizer*)sender
{
    [self.view endEditing:YES];
    [self callServiceForLogin];
}

- (void)signUpTapped:(UITapGestureRecognizer*)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NormalSignUpController *signUpVC = [storyboard instantiateViewControllerWithIdentifier:@"NormalSignUpVCID"];
    [self.navigationController pushViewController:signUpVC animated:true];
}


- (BOOL)validate{
    if ([Helper checkEmptyField:_txtUserEmail.text]){
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"EmailBlank_Alert", nil)];
        return NO;
    }

    if (![Helper checkEmailFormat:_txtUserEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"ValidEmail_Alert", nil)];
        return NO;
    }
    if ([Helper checkEmptyField:_txtUserPassword.text]){
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"PasswordBlank_Alert", nil)];
        return NO;
    }
    
    return YES;
}
- (void)callServiceForLogin{
    
    if (![self validate]) {
        return;
    }
  /*  Request parameter:
    userName, userEmail, latitude, longitude, deviceId, userPassword, phoneNo, pushNotificationToken, simOperatorName, os, country, city, ipAddress, profilePic, profilePicExt, regnLoginType (GenL/GenR), osVersion, appVersion, deviceName, deviceAndroidID, deviceWifiMacID
    
    Note :
    When regnLoginType =’GenR’ :  userName is mandatory
    
    Request example :
//http://103.15.232.68/~angeler/web_services/user_settings/general_signup_login ?
   userName=Rajesh&userEmail=rajes@rpigroup.com&phoneNo=&profilePic=&profilePicExt=&latitude=22.5691318&longitude=88.4354286&city=Kolkata&country=India&ipAddress=&deviceId=1&pushNotificationToken=11&simOperatorName=&regnLoginType=GenR&os=Android&osVersion=6.1&appVersion=1.1&userPassword=3213213
   */
    
    NSDictionary *param = @{@"userName":Empty,
                            @"userEmail":_txtUserEmail.text,
                            @"latitude":[[GlobalInfo sharedInfo] latitude],
                            @"longitude":[[GlobalInfo sharedInfo] longitude],
                            @"deviceId":[[GlobalInfo sharedInfo] deviceId],
                            @"userPassword":_txtUserPassword.text,
                            @"phoneNo":Empty,
                            @"pushNotificationToken":[[GlobalInfo sharedInfo] pushNotificationToken],
                            @"simOperatorName":[[GlobalInfo sharedInfo] simOperatorName],
                            @"os":[[GlobalInfo sharedInfo] os],
                            @"country":Empty,
                            @"city":Empty,
                            @"ipAddress":[[GlobalInfo sharedInfo] ipAddress],
                            @"profilePic":Empty,
                            @"profilePicExt":Empty,
                            @"regnLoginType":@"GenL",
                            @"osVersion":[[GlobalInfo sharedInfo] osVersion],
                            @"appVersion":[[GlobalInfo sharedInfo] appVersion],
                            @"deviceName":Empty,
                            @"deviceAndroidID":Empty,
                            @"deviceWifiMacID":Empty,
                            @"lang":[Helper getLocalData:kUserDeviceCurrentLanguage]
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGeneralRegLogin
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
    success:^(id responseObject)
     {
         
         NSLog(@"response ===   %@",responseObject);
         if ([GlobalInfo checkStatusSuccess:responseObject]) {
             NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
             [[GlobalInfo sharedInfo] setValuesFromLoginData:dataDict];
             [[GlobalInfo sharedInfo] setIsLoggedIn:YES];
             
            
             [AppDel showHomeScreen];
             //NSLog(@"devId====  %@  \n %@   loged in %@",[[GlobalInfo sharedInfo] deviceId],[[GlobalInfo sharedInfo] userName],[[GlobalInfo sharedInfo] isLoggedIn]?@"YES":@"NO");
         }
     }
   failure:^(id responseObject){
       NSLog(@"failure response ===   %@",responseObject);
       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       //Web request call failed//Web service failed to retrieve data
   }];
}
- (IBAction)forgotPassword:(id)sender {
    

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ForgotPasswordViewController *forgotPasswordVC = [storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVCID"];
    [self.navigationController pushViewController:forgotPasswordVC animated:true];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

@end
