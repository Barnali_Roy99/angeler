//
//  NormalSignUpController.m
//  Angeler
//
//  Created by AJAY on 22/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "NormalSignUpController.h"
#import "HomeViewController.h"

@interface NormalSignUpController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    int selectedCountryIndex;
}


@property (nonatomic, strong) ImageObjectClass *imageObj;
@property (nonatomic, strong) NSMutableArray *arrCountry;
@end

@implementation NormalSignUpController
@synthesize txtUserEmail,txtUserLastName,txtUserFirstName,txtUserPassword,containerView,viewLastName,viewFirstName,viewEmail,viewCreatePassword,imgProfile,termsConditionSwitch,termsConditionsTextLabel,registerBtn,topNavbarView,topBarHeightConstraint,viewCountry,txtUserSelectCountry;


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedCountryIndex = -1;
    [self setCountryPicker];
    [self callApiToGetCountryList];
    [self initialSetUp];
    //Localization
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2;
    imgProfile.clipsToBounds = YES;
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}

#pragma mark - General methods

-(void)initialSetUp{
    [self setPlaceHolderText];
  
    self.navigationController.navigationBarHidden = YES;
    
    //Localization
    
    [topNavbarView setTitle:NSLocalizedString(@"Sign_Up", nil)];
    
    [topNavbarView showNavbackBtn];
    [containerView makeCornerRound:15.0];
    [viewLastName makeCornerRound:15.0];
    [viewFirstName makeCornerRound:15.0];
    [viewEmail makeCornerRound:15.0];
    [viewCreatePassword makeCornerRound:15.0];
    [viewCountry makeCornerRound:15.0];
    [registerBtn makeCornerRound:25.0];
    
    [registerBtn setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    registerBtn.titleLabel.textAlignment = UITextAlignmentCenter;
//    NSString *TC_text = NSLocalizedString(@"TermsConditions_Text", nil) ;
//    NSString *TC_underLineText = NSLocalizedString(@"TermsConditions_UnderlineText", nil) ;
//    termsConditionsTextLabel.attributedText = [Helper getUnderlineText:TC_text :[TC_text rangeOfString:TC_underLineText] withColor:[UIColor mediumBlueColor]];
//    
    termsConditionSwitch.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [termsConditionSwitch setOn:NO];
    
    _imageObj = [[ImageObjectClass alloc] init];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    
}

-(void)setPlaceHolderText{
    
    //Localization
    
    UIColor *color = [UIColor lightGrayColor];
    self.txtUserLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Last_Name", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserFirstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"First_Name", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Create_A_Password", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserSelectCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Choose_Country", nil) attributes:@{NSForegroundColorAttributeName: color}];
}


- (IBAction)RegisterTapped:(id)sender{
    [self callApiToRegister];
}

-(void)setCountryPicker
{
    //Country List Set Up
    [txtUserSelectCountry setInputView:[Helper pickerWithTag:COUNTRY_NAME forControler:self]];
    [txtUserSelectCountry setInputAccessoryView:[Helper toolbarWithTag:COUNTRY_NAME forControler:self]];
}
- (BOOL)validate{
    
  
    
    if ([_imageObj.mimeType isEqualToString:@""])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Attach_ProfilePic_Alert", nil)];
        return NO;
    }
    
    
    if ([Helper checkEmptyField:txtUserLastName.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"LastNameBlank_Alert", nil)];
        return NO;
    }

    if ([Helper checkEmptyField:txtUserFirstName.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"FirstNameBlank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:txtUserEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"EmailBlank_Alert", nil)];
        return NO;
    }
    
    if (![Helper checkEmailFormat:txtUserEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"ValidEmail_Alert", nil)];
        return NO;
    }
    if ([Helper checkEmptyField:txtUserPassword.text] ) {
       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"PasswordBlank_Alert", nil)];
        return NO;
    }
    if (txtUserPassword.text.length < 8 ) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"PasswordLenght_Alert", nil)];
        return NO;
    }
    
    if (![termsConditionSwitch isOn])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"TermsConditions_Alert", nil)];
        return NO;
    }
    
    if (selectedCountryIndex == -1)
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Select_Country_Alert", nil)];
        return NO;
        
    }
    
    
    
    
    return YES;

}

#pragma mark - Picker Call

- (void)pickerDoneTapped:(UIButton*)sender{
    
    if (sender.tag == COUNTRY_NAME) {
        
        UIPickerView *piker = (UIPickerView*)txtUserSelectCountry.inputView;
        
        if(([piker selectedRowInComponent:0] - 1) >= 0)
        {
            selectedCountryIndex = (int)[piker selectedRowInComponent:0] - 1;
            txtUserSelectCountry.text = [[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_name"];
            
        }
        [txtUserSelectCountry resignFirstResponder];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        return _arrCountry.count + 1;
    }
    return 0;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        if (row == 0)
        {
            return NSLocalizedString(@"Choose", nil);
        }
        return [[_arrCountry objectAtIndex:row-1] objectForKey:@"country_name"];
    }
    return Empty;
}


#pragma mark -API Call

- (void)callApiToGetCountryList{
    
    NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserSelectCountryList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           _arrCountry = [NSMutableArray arrayWithArray:arr];
                                                           
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



- (void)callApiToRegister{
    if (![self validate]) {
        return;
    }
    NSDictionary *param = @{@"firstName":txtUserFirstName.text,
                            @"lastName":txtUserLastName.text,
                            @"userName":Empty,
                            @"userEmail":txtUserEmail.text,
                            @"latitude":[[GlobalInfo sharedInfo] latitude],
                            @"longitude":[[GlobalInfo sharedInfo] longitude],
                            @"deviceId":[[GlobalInfo sharedInfo] deviceId],
                            @"userPassword":txtUserPassword.text,
                            @"phoneNo":Empty,
                            @"pushNotificationToken":[[GlobalInfo sharedInfo] pushNotificationToken],
                            @"simOperatorName":[[GlobalInfo sharedInfo] simOperatorName],
                            @"os":[[GlobalInfo sharedInfo] os],
                            @"country":Empty,
                            @"city":Empty,
                            @"ipAddress":[[GlobalInfo sharedInfo] ipAddress],
                            @"profilePic":[_imageObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                            @"profilePicExt":_imageObj.mimeType,
                            @"regnLoginType":@"GenR",
                            @"osVersion":[[GlobalInfo sharedInfo] osVersion],
                            @"appVersion":[[GlobalInfo sharedInfo] appVersion],
                            @"deviceName":Empty,
                            @"deviceAndroidID":Empty,
                            @"deviceWifiMacID":Empty,
                            @"lang":[Helper getLocalData:kUserDeviceCurrentLanguage],
                            @"userSelectedCountryId":[[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_id"]
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGeneralRegLogin
                                                    Method:REQMETHOD_POST
                                                parameters:param
                                                     media:nil
                                            viewController:self
   success:^(id responseObject)
     {
         
         NSLog(@"response ===   %@",responseObject);
         if ([GlobalInfo checkStatusSuccess:responseObject]) {
             
             NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
             [[GlobalInfo sharedInfo] setValuesFromLoginData:dataDict];
             [[GlobalInfo sharedInfo] setIsLoggedIn:YES];
             [self showHomeScreen];
             //NSLog(@"devId====  %@  \n %@   loged in %@",[[GlobalInfo sharedInfo] deviceId],[[GlobalInfo sharedInfo] userName],[[GlobalInfo sharedInfo] isLoggedIn]?@"YES":@"NO");
         }
     }
     
   failure:^(id responseObject){
       NSLog(@"failure response ===   %@",responseObject);
       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       //Web request call failed//Web service failed to retrieve data
   }];

}


-(void)showHomeScreen
{
    HomeViewController *homeObj = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    homeObj.isSignUp = true;
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeObj];
}
- (IBAction)profileImageTapped:(id)sender{
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
                imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
                [Helper showCameraGoToSettingsAlert];
            }
            
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CameraNotAvailable_Alert", nil)];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
            imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"GalleryNotAvailable_Alert", nil)];
        }
    }];
    
    [GlobalInfo showAlertTitle:Empty Message: NSLocalizedString(@"Upload_Image_From", nil)actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = info[UIImagePickerControllerEditedImage];
    //ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
    
    
    
    imgProfile.image = image;
    
    //=========get image type and compress========
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI =   (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
    // NSString *strImageType = Empty;
    
    if (UTTypeConformsTo(imageUTI, kUTTypeJPEG))
    {
        // Handle JPG
        _imageObj.mimeType = @"jpeg";
        _imageObj.imgData = [Helper compressImage:image];
        CFRelease(imageUTI);
    }
    else if (UTTypeConformsTo(imageUTI, kUTTypePNG))
    {
        // Handle PNG
        _imageObj.mimeType = @"png";
        _imageObj.imgData = UIImagePNGRepresentation(image);
        CFRelease(imageUTI);
    }
    else
    {
        NSLog(@"Unhandled Image UTI: %@", imageUTI);
        
        // Handle JPG
        _imageObj.mimeType = @"jpeg";
        _imageObj.imgData = [Helper compressImage:image];
    }
    
    //_imageData = [_imageData base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //CFRelease(imageUTI);
    //===============================
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        _imageObj.mediaType = @"image";
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
