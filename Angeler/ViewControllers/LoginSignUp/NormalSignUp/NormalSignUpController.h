//
//  NormalSignUpController.h
//  Angeler
//
//  Created by AJAY on 22/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreServices/CoreServices.h>


@interface NormalSignUpController : UIViewController

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *viewLastName;
@property (weak, nonatomic) IBOutlet UIView *viewFirstName;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIView *viewCreatePassword;
@property (weak, nonatomic) IBOutlet UIView *viewCountry;

@property (nonatomic, weak) IBOutlet UITextField *txtUserLastName;
@property (nonatomic, weak) IBOutlet UITextField *txtUserFirstName;
@property (nonatomic, weak) IBOutlet UITextField *txtUserEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtUserPassword;
@property (nonatomic, weak) IBOutlet UITextField *txtUserSelectCountry;

@property (nonatomic, weak) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UISwitch *termsConditionSwitch;
@property (nonatomic, weak) IBOutlet UILabel *termsConditionsTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

- (IBAction)RegisterTapped:(id)sender;

@end
