//
//  ForgotPasswordViewController.m
//  Angeler
//
//  Created by AJAY on 19/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "ForgotPasswordViewController.h"


@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController
@synthesize topNavbarView,topBarHeightConstraint,containerView,viewEmail,txtUserEmail,confirmView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}

#pragma mark - General methods

-(void)initialSetUp{
    [self setPlaceHolderText];
    //Localization
    [topNavbarView setTitle:NSLocalizedString(@"Forgot_Password", nil)];
    [topNavbarView showNavbackBtn];
    [containerView makeCornerRound:18.0];
    [viewEmail makeCornerRound:18.0];
    [confirmView makeCornerRound:25.0];
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    UITapGestureRecognizer *loginTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(confirmTapped:)];
    [self.confirmView addGestureRecognizer:loginTapRecognizer];
    
    self.lblConfirm.text = [NSLocalizedString(@"Confirm", nil) uppercaseString];
    
}


-(void)setPlaceHolderText{
    
    //Localization
    
    UIColor *color = [UIColor lightGrayColor];
    self.txtUserEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Enter_Your_Email_Address", nil) attributes:@{NSForegroundColorAttributeName: color}];
}

- (BOOL)validate{
    
    
    if ([Helper checkEmptyField:txtUserEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"EmailBlank_Alert", nil)];
        return NO;
    }
    
    if (![Helper checkEmailFormat:txtUserEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"ValidEmail_Alert", nil)];
        return NO;
    }
    
    return YES;
}

-(void)showPopUp{
   //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Thanks", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Forgot_Password_Message_PopUp", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"Login", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}


#pragma mark - Button Action

- (void)confirmTapped:(UITapGestureRecognizer*)sender
{
    [self.view endEditing:YES];
    [self callForgotPasswordServiceForEmail];
    
}

#pragma mark - Webservice Call

- (void)callForgotPasswordServiceForEmail{
    
    if (![self validate]) {
        return;
    }
    NSDictionary *param = @{@"userEmail":txtUserEmail.text};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kForgotPassword
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
//                                                           [Helper showAlertWithTitle:Empty Message:[[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"]];
                                                           
                                                            [self showPopUp];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       NSLog(@"failure response ===   %@",responseObject);
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                       //Web request call failed//Web service failed to retrieve data
                                                   }];
    
}

#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    [self.navigationController popViewControllerAnimated:false];
}

@end
