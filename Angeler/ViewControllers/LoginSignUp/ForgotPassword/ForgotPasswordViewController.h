//
//  ForgotPasswordViewController.h
//  Angeler
//
//  Created by AJAY on 19/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharePopUpViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForgotPasswordViewController : UIViewController<SharePopUpDismissDelegate>

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;

@property (nonatomic, weak) IBOutlet UITextField *txtUserEmail;
@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirm;


@end

NS_ASSUME_NONNULL_END
