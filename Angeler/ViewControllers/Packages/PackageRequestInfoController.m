//
//  PackageRequestInfoController.m
//  Angeler
//
//  Created by AJAY on 19/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "PackageRequestInfoController.h"
#import "PackageRequestInfoCell.h"
#import "MWPhotoBrowser.h"
#import "TrakingInfoController.h"


static NSString *CellIdentifier = @"packageRequestInfoCell";

@interface PackageRequestInfoController ()<MWPhotoBrowserDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;


@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDtailsTitle;

@property (weak, nonatomic) IBOutlet UIView *viewRequestDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestDtailsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDocType;
@property (weak, nonatomic) IBOutlet UILabel *lblDtTime;

@property (weak, nonatomic) IBOutlet UILabel *lblShipmentIdTitle;
@property (weak, nonatomic) IBOutlet CopyTextLabel *lblShipmentID;

@property (weak, nonatomic) IBOutlet UILabel *lblNoData;
@property (weak, nonatomic) IBOutlet UITableView *tblPackageRequest;

@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (nonatomic, strong) NSMutableArray *arrPackReqInfo;
@property (nonatomic, strong) NSDictionary *packDetails;









@end

@implementation PackageRequestInfoController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToLoadRequest];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
}


#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToLoadRequest];
    [refreshController endRefreshing];
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:NSLocalizedString(@"Shipment_History", nil) ];
    
    //Item Details (Now this block is hidden)

    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
  
    
    //Table view Set Up
   
    [_tblPackageRequest registerNib:[UINib nibWithNibName:@"PackageRequestInfoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblPackageRequest.estimatedRowHeight = 209;
    _tblPackageRequest.rowHeight = UITableViewAutomaticDimension;
    [_tblPackageRequest setNeedsLayout];
    [_tblPackageRequest layoutIfNeeded];
    
    
     //Referesh control Set Up
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblPackageRequest addSubview:refreshController];
    
}

-(void)roundAllSpecificCorner
{
     [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    //Request Details
    [self.viewRequestDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
    _lblItemDtailsTitle.text = NSLocalizedString(@"Item_Details", nil);
    _lblRequestDtailsTitle.text = NSLocalizedString(@"Booking_Details", nil);
    _lblShipmentIdTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Shipment_ID", nil)];
}

- (void)callApiToLoadRequest{

    NSDictionary *param = @{@"packageId":_packageId,
                            @"start":[NSString stringWithFormat:@"%ld",(long)START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPackageRequestList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           NSArray *arr = [[responseObject objectForKey:@"responseData"] objectForKey:@"requestList"];
           if ((arr.count == 0) && (START_POSITION != 0)) {
               moreRecordsAvailable = NO;
               return ;
           }

           if ([GlobalInfo checkStatusSuccess:responseObject]){
               _packDetails = [[responseObject objectForKey:@"responseData"] objectForKey:@"packageDetails"];
               [self showPackageDetails];
               if (_arrPackReqInfo.count > 0) {
                   if (START_POSITION == 0) {
                       [_arrPackReqInfo removeAllObjects];
                   }else if(_arrPackReqInfo.count > START_POSITION+1){
                       [_arrPackReqInfo removeObjectsInRange:NSMakeRange(START_POSITION+1, _arrPackReqInfo.count - (START_POSITION+1))];
                   }
                    [_arrPackReqInfo addObjectsFromArray:arr];
               }else{
                   _arrPackReqInfo = [NSMutableArray arrayWithArray:arr];
               }
               START_POSITION = START_POSITION + [arr count];
               _lblNoData.hidden = _arrPackReqInfo.count > 0;
               [_tblPackageRequest reloadData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}

- (void)showPackageDetails{
    _lblFrom.text = [_packDetails objectForKey:@"fromAddress"];
    _lblTo.text = [_packDetails objectForKey:@"toAddress"];
    _lblDocType.text =  [GlobalInfo TypeOfPackage:[[_packDetails objectForKey:@"packageType"] integerValue]];;
    _lblDtTime.text = [Helper getDateTimeToStringWithDate:[_packDetails objectForKey:@"deliveryDate"] Time:[_packDetails objectForKey:@"deliveryTime"]];
    _lblShipmentID.text = [_packDetails objectForKey:@"shipmentUniqueId"];
}

- (IBAction)backTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrPackReqInfo.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PackageRequestInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *data = [_arrPackReqInfo objectAtIndex:indexPath.row];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.angelerProfileImgView sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]]];
    cell.lblAngelerName.text = [data objectForKey:@"angelerName"];
    
    
    cell.angelerProfileImgView.tag = indexPath.row + 1;
    [cell.angelerProfileImgView sd_setImageWithURL:[NSURL URLWithString: [data objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
    [cell.angelerProfileImgView setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *angelerImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(angelerImageViewTapped:)];
    angelerImageViewTap.numberOfTapsRequired = 1;
    [cell.angelerProfileImgView addGestureRecognizer:angelerImageViewTap];
    
    cell.lblPicAddress.text = [data objectForKey:@"angelerPickupAddress"];
    cell.lblDropAddress.text = [data objectForKey:@"angelerDropoffAddress"];
    
    cell.lblPrice.text = [data objectForKey:@"totalCost"];
    
    cell.lblDate.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"bookingDate"] Time:[data objectForKey:@"bookingTime"]];
    NSString *bookingStatus = [data objectForKey:@"status"];
    cell.lblStatus.text = [data objectForKey:@"statusText"];
    
    cell.lblTrackShipment.hidden = YES;
    if ([bookingStatus isEqualToString:Trip_Booking_Status_Accepted])
    {
        cell.lblStatus.textColor = [UIColor colorWithHexString:@"68B41B"];
        cell.lblTrackShipment.hidden = NO;
        UITapGestureRecognizer *trackShipmentTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(trackShipmentTapped:)];
        trackShipmentTap.numberOfTapsRequired = 1;
        [cell.lblTrackShipment addGestureRecognizer:trackShipmentTap];
        cell.lblTrackShipment.tag = indexPath.row + 1;
    }
    else if ([bookingStatus isEqualToString:Trip_Booking_Status_Reserved])
    {
        cell.lblStatus.textColor = [UIColor colorWithHexString:@"1D96D5"];
    }
    else if ([bookingStatus isEqualToString:Trip_Booking_Status_Rejected] || [bookingStatus isEqualToString:Trip_Booking_Status_Expired])
    {
         cell.lblStatus.textColor = [UIColor colorWithHexString:@"DD4B39"];
    }
    

    
    if ([[data objectForKey:@"serviceType"] isEqualToString:serviceType_Premium])
    {
        [cell.MeetingPointPremiumImageView setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
    }
    else
    {
        [cell.MeetingPointPremiumImageView setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
    }
    

    //Load More Data
    
    if (indexPath.row == [self.arrPackReqInfo count] - 1 && moreRecordsAvailable)
    {
        [self callApiToLoadRequest];
    }
    return cell;
}

#pragma mark - Tableview Action

- (void)angelerImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIImageView *angelerImageView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrPackReqInfo objectAtIndex:angelerImageView.tag - 1];
    self.arrPhotos = [[NSMutableArray alloc] init];
    MWPhoto *angelerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]]];
    angelerPhoto.caption =[NSString stringWithFormat:@"Angeler : %@",[data objectForKey:@"angelerName"]];
    
    [self.arrPhotos addObject:angelerPhoto];
    [self setMWPHOtoBrowser];
}

- (void)trackShipmentTapped:(UITapGestureRecognizer*)sender{
    
    UIView *trackLabel = (UILabel *)sender.view;
    NSDictionary *data = [_arrPackReqInfo objectAtIndex:trackLabel.tag - 1];
    
    TrakingInfoController *traking = [[TrakingInfoController alloc] initWithNibName:@"TrakingInfoController" bundle:nil];
    traking.bookingId = [data objectForKey:@"bookingId"];
    [self.navigationController pushViewController:traking animated:YES];
}



-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


//===================== Old + Reuse Code =============================

//[Helper addShadowToLayer:_headerView.layer];
//[Helper addShadowToLayer:_sepratorView1.layer];


#pragma mark - Scrollview Delegate
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//
//    // UITableView only moves in one direction, y axis
//    CGFloat currentOffset = scrollView.contentOffset.y;
//    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//
//    //NSInteger result = maximumOffset - currentOffset;
//
//    // Change 10.0 to adjust the distance from bottom
//    if (maximumOffset - currentOffset <= 5.0) {
//        if(moreRecordsAvailable)
//        {
//            [self callApiToLoadRequest];
//        }
//    }
//}

/*@property (weak, nonatomic) IBOutlet UIView *sepratorView1;
 @property (weak, nonatomic) IBOutlet UIView *headerView;
 @property (weak, nonatomic) IBOutlet UIImageView *imgModeOfTransport; */
/* //    cell.imgPick.image = [GlobalInfo AngelerImageForPickupType:[data objectForKey:@"pickupType"]];
 //
 //    cell.imgDrop.image = [GlobalInfo AngelerImageForPickupType:[data objectForKey:@"dropoffType"]];
 //    cell.imgModeOfTransport.image = [UIImage imageNamed:[NSString stringWithFormat:@"list_%@",[[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]] lowercaseString]]];;
 //    cell.lblModeOfTransportValue.text = [data objectForKey:@"travelRefNo"];
 //    cell.lblCost.text = [NSString stringWithFormat:@"SGD%@",[data objectForKey:@"totalCost"]];
 //    cell.lblBookingDtTime.text = [data objectForKey:@"recordDateTime"];
 //
 //    cell.btnCancel.hidden = ![[data objectForKey:@"status"] isEqualToString:@"Reserved"];
 //
 //    [cell.btnCancel addTarget:self action:@selector(cancelTapped) forControlEvents:UIControlEventTouchUpInside];
 //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 //
*/

/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
 
 NSDictionary *data = [_arrPackReqInfo objectAtIndex:0];
 
 //set width depending on device orientation
 self.cellPrototype.frame = CGRectMake(self.cellPrototype.frame.origin.x, self.cellPrototype.frame.origin.y, tableView.frame.size.width, self.cellPrototype.frame.size.height);
 
 CGFloat quotationLabelHeight = [Helper sizeOfLabel:self.cellPrototype.lblPicAddress withText:[NSString stringWithFormat:@"%@",[data objectForKey:@"pickupAddress"]]].height;
 
 CGFloat attributionLabelHeight = [Helper sizeOfLabel:self.cellPrototype.lblDropAddress withText:[data objectForKey:@"dropoffAddress"]].height;
 
 CGFloat padding = self.cellPrototype.lblPicAddress.frame.origin.y;
 
 CGFloat combinedHeight = padding + quotationLabelHeight + padding/2 + attributionLabelHeight + padding + 100;
 
 CGFloat minHeight = padding + padding;
 
 return MAX(combinedHeight, minHeight);
 
 }*/


//- (void)cancelTapped{
//    NSLog(@"cancelTapped");
//}


