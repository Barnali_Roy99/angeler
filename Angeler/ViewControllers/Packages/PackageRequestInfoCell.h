//
//  PackageRequestInfoCell.h
//  Angeler
//
//  Created by AJAY on 20/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundImageView.h"

@interface PackageRequestInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet RoundImageView *angelerProfileImgView;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblPicAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UILabel *lblStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (nonatomic, weak) IBOutlet UIView *sepratorView;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UIImageView *MeetingPointPremiumImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblTrackShipment;

//@property (weak, nonatomic) IBOutlet UIImageView *imgDrop;
//@property (weak, nonatomic) IBOutlet UIImageView *imgModeOfTransport;
//@property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransportValue;
//@property (weak, nonatomic) IBOutlet UILabel *lblCost;
//@property (weak, nonatomic) IBOutlet UILabel *lblBookingDtTime;
//@property (weak, nonatomic) IBOutlet UIButton *btnCancel;


@end
