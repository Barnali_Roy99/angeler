//
//  PackageInfoViewController.m
//  Angeler
//
//  Created by AJAY on 19/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "PackageInfoViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "MWPhotoBrowser.h"


@interface PackageInfoViewController ()<MWPhotoBrowserDelegate>
@property (nonatomic, strong) NSMutableArray *arrPhotos;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *viewDetails;

@property (weak, nonatomic) IBOutlet UILabel *lblFromAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblToAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;



//@property (weak, nonatomic) IBOutlet UILabel *lblFromAddressTitle;
//@property (weak, nonatomic) IBOutlet UILabel *lblToAddressTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblStatusTitle;

@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageTypeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWeightTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryTime;

@property (weak, nonatomic) IBOutlet UIView *viewRecipientDetails;
@property (weak, nonatomic) IBOutlet UIView *viewRecipientDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientContactNo;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientEmail;





@end

@implementation PackageInfoViewController
@synthesize topBarHeightConstraint,topNavbarView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToFetchInfoWithPackageId];
    // Do any additional setup after loading the view from its nib.
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
}



#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"Shipment_Details﻿", nil) capitalizedString]];
    
    //details
    
    [self.viewDetails makeCornerRound:15.0];
    [self.viewDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Item Details
    
    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Recipient Details
    
    [self.viewRecipientDetails makeCornerRound:15.0];
    [self.viewRecipientDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
}

-(void)roundAllSpecificCorner
{
    [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewRecipientDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    
    
}

-(void)setLocalization
{
//    _lblFromAddressTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"From", nil)];
//    _lblToAddressTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"To", nil)];
    _lblPackageTypeTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Item_Category", nil)];
    _lblPackageWeightTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Weight", nil)];
    _lblStatusTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Status", nil)];
    
    _lblItemDetailsTitle.text = NSLocalizedString(@"Item_Details",nil);
    
    _lblRecipientDetailsTitle.text = NSLocalizedString(@"Recipient_Details",nil);
  
    _lblDeliveryTimeTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Delivery_Time", nil)];
   
    
}
#pragma mark - API Call

- (void)callApiToFetchInfoWithPackageId{
    //userId, sessionId, os, osVersion, appVersion, packageId
    
    NSDictionary *param = @{@"packageId":_packageId
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPackageInfo
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               [self updateValuesWithData:[responseObject objectForKey:@"responseData"]];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];

}
- (void)updateValuesWithData:(NSDictionary*)data{
    

    _lblFromAddress.text = [data objectForKey:@"fromAddress"];
    _lblToAddress.text = [data objectForKey:@"toAddress"];
    
    _lblPackageType.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];
    _lblPackageWeight.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"packageWeight"],NSLocalizedString(@"package_weight_unit", nil)];
     _lblStatus.text = [data objectForKey:@"statusText"];
    

//Delivery Details
    _lblRecipientName.text = [data objectForKey:@"recipientName"];
    _lblRecipientContactNo.text = [data objectForKey:@"recipientPhone"];
    _lblRecipientEmail.text = [data objectForKey:@"recipientEmail"];
    _lblDeliveryTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"deliveryDate"] Time:[data objectForKey:@"deliveryTime"]];

    
//   Picture of item
    
    NSArray *imges = [data objectForKey:@"images"];
    if (imges.count == 0) {

    }else{
        self.arrPhotos = [NSMutableArray array];
        for (int i = 1; i <= imges.count ; i++) {
            UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
            imgV.hidden = NO;
            imgV.userInteractionEnabled = YES;
            [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
            NSLog(@"image url =====  %@",[[imges objectAtIndex:i-1] objectForKey:@"image"]);
            [imgV sd_setImageWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
             [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]]]];
        }
    }
   //Map
    {
        CLLocationCoordinate2D from_coordinate = CLLocationCoordinate2DMake([[data objectForKey:@"fromAddressLatitude"] doubleValue] ,[[data objectForKey:@"fromAddressLongitude"] doubleValue]);
        
        CLLocationCoordinate2D to_coordinate = CLLocationCoordinate2DMake([[data objectForKey:@"toAddressLatitude"] doubleValue] ,[[data objectForKey:@"toAddressLongitude"] doubleValue]);
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:from_coordinate zoom:12];
        
        GMSMarker *from_marker = [[GMSMarker alloc] init];
        from_marker.position = from_coordinate;
        from_marker.title = [data objectForKey:@"fromAddress"];
        from_marker.map = _mapView;
        
        GMSMarker *to_marker = [[GMSMarker alloc] init];
        to_marker.position = to_coordinate;
        //    markerPack.icon = [UIImage imageNamed:@"LocationPin"];
        to_marker.title = [data objectForKey:@"toAddress"];
        to_marker.map = _mapView;
        
        _mapView.camera = camera;
        [_mapView setSelectedMarker:from_marker];
        
        NSArray *arrMarkerLocation = [NSArray arrayWithObjects:from_marker,to_marker, nil];
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        
        for (GMSMarker *marker in arrMarkerLocation)
            bounds = [bounds includingCoordinate:marker.position];
        
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50]];
    }
}

#pragma mark - Gesture Action

- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:[tapGest view].tag-1];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

#pragma mark - Button Action

- (IBAction)callRecipient:(id)sender {
    if (_lblRecipientContactNo.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblRecipientContactNo.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end


//================================= Old + Reuse Code ================================


//    NSMutableAttributedString *strFromAdd = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"From: %@",[data objectForKey:@"fromAddress"]]];
//    [strFromAdd addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:14.0] range:NSMakeRange(0, 5)];
//    NSMutableAttributedString *strToAdd = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"To: %@",[data objectForKey:@"toAddress"]]];
//    [strToAdd addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:14.0] range:NSMakeRange(0, 3)];


//    _lblModeOfTransport.text = [GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]];
//
//    _imgModeOfTransport.image = [UIImage imageNamed:[NSString stringWithFormat:@"package_info_%@",[[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]] lowercaseString]]];
//      _lblPackageSize.text = [NSString stringWithFormat:@"%@ X %@ X %@ CM",[data objectForKey:@"packageHeight"],[data objectForKey:@"packageWidth"],[data objectForKey:@"packageLength"]];
//

/*@property (weak, nonatomic) IBOutlet UIView *roundView;
 @property (weak, nonatomic) IBOutlet UIView *sepratorView1;
 @property (weak, nonatomic) IBOutlet UIView *headerView;
 @property (weak, nonatomic) IBOutlet UIView *pictureView;
 @property (weak, nonatomic) IBOutlet UIView *recipentView;
 @property (weak, nonatomic) IBOutlet UIView *deliveryView;
 
 
 @property (weak, nonatomic) IBOutlet UILabel *lblPackageSize;
 
 @property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransport;
 
 @property (weak, nonatomic) IBOutlet UIImageView *imgModeOfTransport;
 
 [Helper makeItRound:_roundView.layer withRadius:7.0];
 
 [Helper addShadowToLayer:_headerView.layer];
 [Helper addShadowToLayer:_sepratorView1.layer];
 [Helper addShadowToLayer:_pictureView.layer];
 [Helper addShadowToLayer:_recipentView.layer];
 [Helper addShadowToLayer:_deliveryView.layer];
 
 
*/
