//
//  PackageInfoViewController.h
//  Angeler
//
//  Created by AJAY on 19/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageInfoViewController : UIViewController
@property (nonatomic, strong) NSString *packageId;
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@end
