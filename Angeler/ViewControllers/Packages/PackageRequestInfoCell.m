//
//  PackageRequestInfoCell.m
//  Angeler
//
//  Created by AJAY on 20/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "PackageRequestInfoCell.h"


@implementation PackageRequestInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.containerView makeCornerRound:15.0];
    self.angelerProfileImgView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.angelerProfileImgView.layer.borderWidth = 1.0;
    
    
    self.lblPriceTitle.text = NSLocalizedString(@"Price", nil);
    self.lblDateTitle.text = NSLocalizedString(@"Reaching_On", nil);
    self.lblStatusTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Status", nil)];

    NSString *TC_text = [NSString stringWithFormat:@"(%@)", NSLocalizedString(@"Track_Shipment", nil)];
   
  //  NSString *TC_underLineText = NSLocalizedString(@"Track_Shipment", nil) ;
    self.lblTrackShipment.attributedText = [Helper getUnderlineText:TC_text :[TC_text rangeOfString:TC_text]  withColor:[UIColor blackColor]];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
