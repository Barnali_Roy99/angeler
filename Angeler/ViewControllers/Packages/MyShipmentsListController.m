//
//  MyShipmentsListController.m
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "MyShipmentsListController.h"
#import "AddShipmentsStepOneController.h"
#import "PackageCell.h"
#import "ChooseAngelerStepOneController.h"
#import "PackageInfoViewController.h"
#import "PackageRequestInfoController.h"
#import "TrakingInfoController.h"
#import "RequestLocalAngelerController.h"
#import "ImageSlidingVC.h"
#import "MWPhotoBrowser.h"
#import "SharePopUpViewController.h"
#import "VerifyAccountViewController.h"

#define logoImageHolderViewHeight 30


static NSString *CellIdentifier = @"packageCell";

@interface MyShipmentsListController ()<MWPhotoBrowserDelegate,SharePopUpDismissDelegate>{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
    
}


@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tblPackage;
@property (nonatomic, strong) NSMutableArray *arrPackage;
@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;


@end

@implementation MyShipmentsListController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self callApiToLoadPackagesList];
}

#pragma mark - General Functions

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView showPlusBtn];
    [_topNavbarView setTitle:NSLocalizedString(@"My Shipments", nil)];
    
    [_tblPackage registerNib:[UINib nibWithNibName:@"PackageCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    
    
    _tblPackage.estimatedRowHeight = 20;
    _tblPackage.rowHeight = UITableViewAutomaticDimension;
    
    [_tblPackage setNeedsLayout];
    [_tblPackage layoutIfNeeded];
    
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblPackage addSubview:refreshController];
    
    //Delete/Info hide
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self.tblPackage addGestureRecognizer:gr];
    
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
}

#pragma mark - Gesture Action

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    
    for (int row = 0; row < _arrPackage.count ; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        PackageCell *cell = (PackageCell *)[self.tblPackage cellForRowAtIndexPath:indexPath];
        cell.menu_Container.hidden = YES;
        
    }
    
}

#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToLoadPackagesList];
    [refreshController endRefreshing];
}

#pragma mark - Webservice Call

- (void)callApiToLoadPackagesList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",(long)START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPackageList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
              
               NSArray *arr = [responseObject objectForKey:@"responseData"];
               if ((arr.count == 0) && (START_POSITION != 0)){
                   moreRecordsAvailable = NO;
                   return ;
               }
               if ([GlobalInfo checkStatusSuccess:responseObject]) {
               
               //No Record found label show
                   
                   if (_arrPackage.count > 0) {
                       if (START_POSITION == 0) {
                           [_arrPackage removeAllObjects];
                       }else if(_arrPackage.count > START_POSITION){
                           [_arrPackage removeObjectsInRange:NSMakeRange(START_POSITION, _arrPackage.count - START_POSITION)];
                       }
                       [_arrPackage addObjectsFromArray:arr];
                   }else{
                       _arrPackage = [NSMutableArray arrayWithArray:arr];
                   }
                   START_POSITION = START_POSITION + [arr count];
                 _lblNoRecord.hidden = _arrPackage.count > 0;
                   [_tblPackage reloadData];
               }
           }
           failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
           }];
}

#pragma mark - Share Pop Up show

-(void)showNotVerifiedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_NotVerified_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_NotVerified_Message", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showPendingAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_Pending_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Pending_Message", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showRejectedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_Verification_Rejected_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Verification_Rejected_Msg", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}


#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    
    VerifyAccountViewController *verifyAccountObj = [[VerifyAccountViewController alloc] initWithNibName:@"VerifyAccountViewController" bundle:nil];
    [self.navigationController pushViewController:verifyAccountObj animated:YES];
}


#pragma mark - Table view data source Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrPackage.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PackageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *data = [_arrPackage objectAtIndex:indexPath.row];
    
    [cell.containerView makeCornerRound:15.0];
    [cell.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [cell.packageStatusActionBtn makeCornerRound:15.0];
    
   
    cell.lblDeliveryDateTitle.text = NSLocalizedString(@"Deliver_Before", nil);
    
    cell.fromAddressLabel.text = [data objectForKey:@"fromAddress"];
    cell.toAddressLabel.text = [data objectForKey:@"toAddress"];

    NSArray *packageImages = [data objectForKey:@"images"];
    NSDictionary *firstImageDic = [packageImages objectAtIndex:0];
    
    UITapGestureRecognizer *documentImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(documentImageViewTapped:)];
        documentImageViewTap.numberOfTapsRequired = 1;
    [cell.shipmentImgBackgroundView addGestureRecognizer:documentImageViewTap];
    
    cell.shipmentImgBackgroundView.tag = indexPath.row + 1;
    [cell.shipmentImageView sd_setImageWithURL:[NSURL URLWithString: [firstImageDic objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
    cell.shipmentImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    cell.shipmentImageView.layer.borderWidth = 1.0;
    
    
    cell.lblDocumentWeight.text = [NSString stringWithFormat:@"%@ | %@ %@",[GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]],[data objectForKey:@"packageWeight"],NSLocalizedString(@"package_weight_unit", nil)];
    
    
   
    cell.lblDeliveryDate.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"deliveryDate"] Time:[data objectForKey:@"deliveryTime"]];
    

    [cell.angelerProfileImageview sd_setImageWithURL:[NSURL URLWithString: [data objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
    UITapGestureRecognizer *angelerImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(angelerImageViewTapped:)];
    angelerImageViewTap.numberOfTapsRequired = 1;
    [cell.angelerProfileImageview addGestureRecognizer:angelerImageViewTap];
    cell.angelerProfileImageview.tag = indexPath.row + 1;
    
    cell.angelerProfileImageview.layer.borderColor = [[UIColor blackColor] CGColor];
    cell.angelerProfileImageview.layer.borderWidth = 1.0;
    
    
    
    NSInteger statusType = [[data objectForKey:@"statusType"] integerValue];
    [cell.packageStatusActionBtn setTitle:[data objectForKey:@"statusText"] forState:UIControlStateNormal];
    if (statusType == 1 || statusType == 2 || statusType == 9 || statusType == 10 || statusType == 11) {
        
       

        cell.logoImagesHolderView.hidden = YES;
        cell.logoImageHolderViewHeightConstraint.constant = 0;
        
    }else{

        cell.logoImagesHolderView.hidden = NO;
        cell.logoImageHolderViewHeightConstraint.constant = logoImageHolderViewHeight;
    }
    
    if ([[data objectForKey:@"serviceType"] isEqualToString:serviceType_Premium])
    {
        [cell.meetingPointPremiumImageView setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
    }
    else
    {
        [cell.meetingPointPremiumImageView setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
    }
    
    if(statusType == 11){
        
        cell.packageStatusActionBtn.userInteractionEnabled = NO;
    }
    else{
        
     cell.packageStatusActionBtn.userInteractionEnabled = YES;
    cell.packageStatusActionBtn.tag = indexPath.row + 1;
    [cell.packageStatusActionBtn addTarget:self action:@selector(cellActionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    }
    cell.btnMenu.tag = indexPath.row + 1;
    [cell.btnMenu addTarget:self action:@selector(menuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    

    cell.btnRequestList.tag = indexPath.row+1;
    [cell.btnRequestList addTarget:self action:@selector(openRequestList:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnDelete.tag = indexPath.row+1;
    [cell.btnDelete addTarget:self action:@selector(deletePackage:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[data objectForKey:@"requestCount"] integerValue] > 0) {
        cell.btnDelete.hidden = YES;
        cell.btnRequestList.hidden = NO;
       
    }else{
        cell.btnDelete.hidden = NO;
        cell.btnRequestList.hidden = YES;
       
    }
    
    cell.menu_Container.hidden = YES;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //Load More Data
    
    if (indexPath.row == [self.arrPackage count] - 1 && moreRecordsAvailable)
    {
        [self callApiToLoadPackagesList];
    }
    
  
    return cell;
}

//- (void)showInfo:(UIButton*)sender{
//    START_POSITION = sender.tag-1;
//    PackageInfoViewController *next = [[PackageInfoViewController alloc] initWithNibName:@"PackageInfoViewController" bundle:nil];
//    
//    next.packageId = [self getPackageObjectFromData:[_arrPackage objectAtIndex:sender.tag-1]].pack_Id;
//    [self.navigationController pushViewController:next animated:YES];
//    
//    [_tblPackage reloadData];
//
//}
- (void)deletePackage:(UIButton*)sender{
    
    [self showShipmentDeleteAlertWithIndex:sender.tag-1];
    
}

-(void)showShipmentDeleteAlertWithIndex:(NSInteger)selectedIndex
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:NSLocalizedString(@"Delete_Shipment_Alert", nil)
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   [self deleteShipmentAPICallWithIndex:selectedIndex];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       /** What we write here???????? **/
                                       NSLog(@"you pressed No, thanks button");
                                       // call method whatever u need
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)deleteShipmentAPICallWithIndex:(NSInteger)selectedIndex
{
    //userId, sessionId, os, osVersion, appVersion, packageId
    NSDictionary *param = @{@"packageId":[[_arrPackage objectAtIndex:selectedIndex] objectForKey:@"packageId"]
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPackageDelete
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                     
                                                           [Helper showAlertWithTitle:Empty Message:[[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"]];
                                            [_arrPackage removeObjectAtIndex: selectedIndex];
                                                           [_tblPackage reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];

}



- (void)openRequestList:(UIButton*)sender{
    PackageRequestInfoController *next = [[PackageRequestInfoController alloc] initWithNibName:@"PackageRequestInfoController" bundle:nil];
    next.packageId = [self getPackageObjectFromData:[_arrPackage objectAtIndex:sender.tag-1]].pack_Id;
    [self.navigationController pushViewController:next animated:YES];
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

#pragma mark - Table view Action
-(void)menuClicked:(UIButton*)sender
{
    for (int row = 0; row < _arrPackage.count ; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        PackageCell *cell = (PackageCell *)[_tblPackage cellForRowAtIndexPath:indexPath];
        if (row == sender.tag - 1)
        {
            cell.menu_Container.hidden = !cell.menu_Container.hidden;
        }
        else
        {
            cell.menu_Container.hidden = YES;
        }
    }
}


- (void)cellActionButtonTapped:(UIButton*)sender{
    
    START_POSITION = sender.tag-1;
    NSDictionary *data = [_arrPackage objectAtIndex:sender.tag-1];
    
    NSInteger statusType = [[data objectForKey:@"statusType"] integerValue];
    if (statusType == 1 || statusType == 2 || statusType == 9 || statusType == 10) {
        [self openChooseAngeler:[self getPackageObjectFromData:data]];
    }
    else {
        [self openTrakingInfoWithBookingId:[data objectForKey:@"bookingId"]];
    }
    
}

- (void)documentImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *documentView = (UIView *)sender.view;
    NSDictionary *data = [_arrPackage objectAtIndex:documentView.tag - 1];
    
    NSArray *packageImages = [data objectForKey:@"images"];
    self.arrPhotos = [[NSMutableArray alloc] init];
    
    //In list show only one image
    //    NSDictionary *imageDic = [packageImages objectAtIndex:0];
    //  [urlImageArr addObject:[imageDic  objectForKey:@"image"]];
    
    [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[packageImages objectAtIndex:0] objectForKey:@"image"]]]];
    
    // [self showZoomImageScreen: self.arrPhotos];
    [self setMWPHOtoBrowser];
    
    
    
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}



- (void)angelerImageViewTapped:(UITapGestureRecognizer*)sender{
    
     UIImageView *angelerImageView = (UIImageView *)sender.view;
     NSDictionary *data = [_arrPackage objectAtIndex:angelerImageView.tag - 1];
     self.arrPhotos = [[NSMutableArray alloc] init];
    MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]]];
    photo.caption = [NSString stringWithFormat:@"Angeler : %@",[data objectForKey:@"angelerName"]];
    [self.arrPhotos addObject:photo];
       [self setMWPHOtoBrowser];
}

-(void)showZoomImageScreen:(NSMutableArray*)imgURLArr
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ImageSlidingVC *imgSlideVC = [storyboard instantiateViewControllerWithIdentifier:@"ImageSlidingVC"];
    imgSlideVC.imgArr = imgURLArr;
    [self.navigationController pushViewController:imgSlideVC animated:YES];
}

- (void)openChooseAngeler:(PackageClass*)sender{
    ChooseAngelerStepOneController *angler = [[ChooseAngelerStepOneController alloc] initWithNibName:@"ChooseAngelerStepOneController" bundle:nil];
    angler.packObj = sender;// [self getPackageObjectFromData:[_arrPackage objectAtIndex:sender.tag-1]];
    [self.navigationController pushViewController:angler animated:YES];
}
- (void)openTrakingInfoWithBookingId:(NSString*)sender{
    TrakingInfoController *traking = [[TrakingInfoController alloc] initWithNibName:@"TrakingInfoController" bundle:nil];
    traking.bookingId = sender;// [self getPackageObjectFromData:[_arrPackage objectAtIndex:sender.tag-1]];
    [self.navigationController pushViewController:traking animated:YES];
}


- (PackageClass*)getPackageObjectFromData:(NSDictionary*)dict{
    PackageClass *packObj = [[PackageClass alloc] init];
    
    packObj.from_Address.countryId = [dict objectForKey:@"fromCountryId"];
    packObj.from_Address.countryCode = [dict objectForKey:@"fromCountryCode"];
    packObj.from_Address.countryName = [dict objectForKey:@"fromCountryName"];
    packObj.from_Address.cityId = [dict objectForKey:@"fromCityId"];
    packObj.from_Address.cityCode = [dict objectForKey:@"fromCityCode"];
    packObj.from_Address.cityName = [dict objectForKey:@"fromCityName"];
    packObj.from_Address.Address = [dict objectForKey:@"fromAddress"];
    packObj.from_Address.lat_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"fromAddressLatitude"] doubleValue]];
    packObj.from_Address.long_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"fromAddressLongitude"] doubleValue]];
    
    packObj.to_Address.countryId = [dict objectForKey:@"toCountryId"];
    packObj.to_Address.countryCode = [dict objectForKey:@"toCountryCode"];
    packObj.to_Address.countryName = [dict objectForKey:@"toCountryName"];
    packObj.to_Address.cityId = [dict objectForKey:@"toCityId"];
    packObj.to_Address.cityCode = [dict objectForKey:@"toCityCode"];
    packObj.to_Address.cityName = [dict objectForKey:@"toCityName"];
    packObj.to_Address.Address = [dict objectForKey:@"toAddress"];
    packObj.to_Address.lat_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"toAddressLatitude"] doubleValue]];
    packObj.to_Address.long_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"toAddressLongitude"] doubleValue]];
   
    packObj.pack_Id = [dict objectForKey:@"packageId"];
    packObj.pack_Type = [dict objectForKey:@"packageType"];
    packObj.travel_Mode = [[dict objectForKey:@"travelMode"] integerValue];
    packObj.pack_Remarks = [dict objectForKey:@"remarks"];
    packObj.pack_Delivery_Date_Time = [NSString stringWithFormat:@"%@,%@",[dict objectForKey:@"deliveryDate"],[dict objectForKey:@"deliveryTime"]];
    packObj.pack_Request_count = [dict objectForKey:@"requestCount"];
    packObj.pack_Status_Text = [dict objectForKey:@"statusText"];
    
    
    /*
    bookingId = 0;
    statusType = 1;
    
   */
    
    return packObj;
}

//#pragma mark - Scrollview Delegate
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//
//    // UITableView only moves in one direction, y axis
//    CGFloat currentOffset = scrollView.contentOffset.y;
//    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//
//    //NSInteger result = maximumOffset - currentOffset;
//
//    // Change 10.0 to adjust the distance from bottom
//    if (maximumOffset - currentOffset <= 5.0) {
//        if(moreRecordsAvailable)
//        {
//            [self callApiToLoadPackagesList];
//        }
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


//========================== Old + Reuse Code ==============================


/*- (IBAction)addPackages:(id)sender{
 AddShipmentsStepOneController *delObj = [[AddShipmentsStepOneController alloc] initWithNibName:@"AddShipmentsStepOneController" bundle:nil];
 [self.navigationController pushViewController:delObj animated:YES];
 } */

/*  cell.lblPackDateTime.text = [NSString stringWithFormat:@"%@,%@",[data objectForKey:@"deliveryDate"],[data objectForKey:@"deliveryTime"]];
 cell.lblFromAddress.text = [data objectForKey:@"fromAddress"];
 cell.lblToAddress.text = [data objectForKey:@"toAddress"];*/

/*//    [NSString stringWithFormat:@"Preferred travel mode: %@",[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]]];
 cell.lblTravelMode.attributedText =
 [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Preferred travel mode: %@",[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]]] andIndex:22];
 cell.lblDocType.attributedText =
 [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Type of package: %@",[GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]]] andIndex:16];
 // [NSString stringWithFormat:@"Type of package: %@",[GlobalInfo packageType:[[data objectForKey:@"packageType"] integerValue]]];
 cell.lblStatus.text = [data objectForKey:@"statusText"];
 
 
 cell.btnStatus.tag = indexPath.row+1;
 [cell.btnStatus addTarget:self action:@selector(cellLeftButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
 
 [cell.btnBookingRequest setTitle:[NSString stringWithFormat:@"Booking request: %@",[data objectForKey:@"requestCount"]] forState:UIControlStateNormal];
 cell.btnBookingRequest.tag = indexPath.row+1;
 [cell.btnBookingRequest addTarget:self action:@selector(openBookingRequest:) forControlEvents:UIControlEventTouchUpInside];
 */
