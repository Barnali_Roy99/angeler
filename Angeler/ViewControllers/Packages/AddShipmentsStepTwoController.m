//
//  AddShipmentsStepTwoController.m
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "AddShipmentsStepTwoController.h"
#import "IQKeyboardManager.h"
#import "ChooseAngelerStepOneController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>



@interface AddShipmentsStepTwoController ()<UIPickerViewDelegate,UIPickerViewDataSource,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewTravelMode;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *travelModeView;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
//@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtDateTime;
@property (weak, nonatomic) IBOutlet UITextView *txtVRemarks;

@property (weak, nonatomic) IBOutlet UILabel *lblCityDateTimeMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carLeadingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblShareMessageTitle;

//@property (strong, nonatomic) NSArray *arrCountryCode;
@end

@implementation AddShipmentsStepTwoController
@synthesize topNavbarView,topBarHeightConstraint,mainContentView,titlelabel,txtRecipientEmail,txtRecipientAngelerID,viewRecipientEmail,viewRecipientAngelerID,orLabel,infoTitlelabel,nextView;


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
}


#pragma mark - General Methods

-(void)initialSetUp
{
    
    [self setPlaceHolderText];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:NSLocalizedString(@"Recipient_Info", nil)];
    
    
    [mainContentView makeCornerRound:15.0];
    [viewRecipientAngelerID makeCornerRound:15.0];
    [viewRecipientEmail makeCornerRound:15.0];
    
    //TitleLabel text Set
    titlelabel.text = NSLocalizedString(@"Recipient_Alert_Text", nil);
    
    //Or label Text Set
    orLabel.text = NSLocalizedString(@"OR", nil);
    
    //Recipient Info Set
    infoTitlelabel.text = NSLocalizedString(@"Recipient_Info_text", nil);
    
    //Share Title
    
    self.lblShareMessageTitle.text = NSLocalizedString(@"Recipient_Share_Title", nil);
    
    //Next View
    
     self.lblNext.text =  NSLocalizedString(@"Next", nil);
    
}

-(void)setGesture
{
    //Next View
    
    UITapGestureRecognizer *nextViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextViewTapped:)];
    [self.nextView addGestureRecognizer:nextViewTapRecognizer];
}

-(void)setPlaceHolderText{
    
    
    
    UIColor *color = [UIColor lightGrayColor];
    self.txtRecipientAngelerID.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Angeler_ID", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtRecipientEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Receiver_Account_Email﻿", nil) attributes:@{NSForegroundColorAttributeName: color}];
}

- (BOOL)validate{

    if ([Helper checkEmptyField:txtRecipientEmail.text] && [Helper checkEmptyField:txtRecipientAngelerID.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Recipient_Info_Blank_Alert", nil)];
        return NO;
    }
    
    if (![Helper checkEmptyField:txtRecipientEmail.text] && ![Helper checkEmptyField:txtRecipientAngelerID.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Receiver_Info_Id_Or_Email_Alert", nil)];
        return NO;
    }
    
    if (![Helper checkEmptyField:txtRecipientEmail.text]) {
        if (![Helper checkEmailFormat:txtRecipientEmail.text]) {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"ValidEmail_Alert", nil)];
            return NO;
        }
    }
    
    return YES;
    
}

#pragma mark - Button Action

- (IBAction)onShareBtnClicked:(id)sender {
    
    NSString *shareData=[NSString stringWithFormat:NSLocalizedString(@"Share_Link_Title", nil),@""];
    
    NSArray *objectsToShare = @[shareData];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


#pragma mark - Gesture Action


- (void)nextViewTapped:(UITapGestureRecognizer*)sender
{
    if ([self validate])
    {
        self.packageObject.pack_Email = txtRecipientEmail.text ;
        self.packageObject.recipient_Angeler_ID = txtRecipientAngelerID.text;
        [self callApiToAddPackage];
    }
}

#pragma mark - UITextfielsDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Webservice Call

- (void)callApiToAddPackage{
    
    NSMutableArray *dictImages = [[NSMutableArray alloc] init];
    //    imagesJSON, , , ,
    for (ImageObjectClass *imgObj in self.packageObject.arrItemImages) {
        NSDictionary *dict = @{@"action":@"added",
                               @"base64String":[imgObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                               @"imageExt":imgObj.mimeType,
                               @"imageId":Empty,
                               @"imageName":imgObj.keyName,
                               @"imageURL":Empty
                               };
        [dictImages addObject:dict];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictImages
                                                       options:0//NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = Empty;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"json data === %@",jsonString);
    }
    
    //    NSString *dataString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    
    NSDictionary *param = @{@"fromCountry":self.packageObject.from_Address.countryId,
                            @"fromCity":self.packageObject.from_Address.cityId,
                            @"toCountry":self.packageObject.to_Address.countryId,
                            @"toCity":self.packageObject.to_Address.cityId,
                            @"deliveryDate":[[self.packageObject.pack_Delivery_Date_Time componentsSeparatedByString:@" "] firstObject],
                            @"deliveryTime":[[self.packageObject.pack_Delivery_Date_Time componentsSeparatedByString:@" "] lastObject],
                            @"travelMode":Empty,
                            @"fromAddress":self.packageObject.from_Address.Address,
                            @"toAddress":self.packageObject.to_Address.Address,
                            @"packageType":self.packageObject.pack_Type,
                            @"packageHeight":Empty,
                            @"packageWidth":Empty,
                            @"packageLength":Empty,
                            @"packageWeight":self.packageObject.pack_Weight,
                            @"recipientName":Empty,
                            @"recipientEmail":self.packageObject.pack_Email,
                            @"recipientId":self.packageObject.recipient_Angeler_ID,
                            @"recipientPhone":Empty,
                            @"recipientPhoneISD":Empty,
                            @"remarks":Empty,
                            @"fromAddressLatitude":[self.packageObject.from_Address.lat_Value stringValue],
                            @"fromAddressLongitude":[self.packageObject.from_Address.long_Value stringValue],
                            @"toAddressLatitude":[self.packageObject.to_Address.lat_Value stringValue],
                            @"toAddressLongitude":[self.packageObject.to_Address.long_Value stringValue],
                            @"imagesJSON":jsonString
                            };
    
    NSLog(@"Param Add shipments %@", param);
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kAddPackage
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                               ChooseAngelerStepOneController *angler = [[ChooseAngelerStepOneController alloc] initWithNibName:@"ChooseAngelerStepOneController" bundle:nil];
                                                               angler.packObj = [self getPackageObjectFromData:[[responseObject objectForKey:@"extraData"] objectForKey:@"packageProperties"]];
                                                               [self.navigationController pushViewController:angler animated:YES];
                                                           }];
                                                           [GlobalInfo showAlertTitle:Empty Message:[[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"] actions:[NSArray arrayWithObject:cancel]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


- (PackageClass*)getPackageObjectFromData:(NSDictionary*)dict{
    PackageClass *packObj = [[PackageClass alloc] init];
    
    packObj.from_Address.countryId = [dict objectForKey:@"fromCountryId"];
    packObj.from_Address.countryCode = [dict objectForKey:@"fromCountryCode"];
    packObj.from_Address.countryName = [dict objectForKey:@"fromCountryName"];
    packObj.from_Address.cityId = [dict objectForKey:@"fromCityId"];
    packObj.from_Address.cityCode = [dict objectForKey:@"fromCityCode"];
    packObj.from_Address.cityName = [dict objectForKey:@"fromCityName"];
    packObj.from_Address.Address = [dict objectForKey:@"fromAddress"];
    packObj.from_Address.lat_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"fromAddressLatitude"] doubleValue]];
    packObj.from_Address.long_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"fromAddressLongitude"] doubleValue]];
    
    packObj.to_Address.countryId = [dict objectForKey:@"toCountryId"];
    packObj.to_Address.countryCode = [dict objectForKey:@"toCountryCode"];
    packObj.to_Address.countryName = [dict objectForKey:@"toCountryName"];
    packObj.to_Address.cityId = [dict objectForKey:@"toCityId"];
    packObj.to_Address.cityCode = [dict objectForKey:@"toCityCode"];
    packObj.to_Address.cityName = [dict objectForKey:@"toCityName"];
    packObj.to_Address.Address = [dict objectForKey:@"toAddress"];
    packObj.to_Address.lat_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"toAddressLatitude"] doubleValue]];
    packObj.to_Address.long_Value = [NSNumber numberWithDouble:[[dict objectForKey:@"toAddressLongitude"] doubleValue]];
    
    packObj.pack_Id = [dict objectForKey:@"packageId"];
    packObj.pack_Type = [dict objectForKey:@"packageType"];
    packObj.travel_Mode = [[dict objectForKey:@"travelMode"] integerValue];
    packObj.pack_Remarks = [dict objectForKey:@"remarks"];
    packObj.pack_Delivery_Date_Time = [NSString stringWithFormat:@"%@,%@",[dict objectForKey:@"deliveryDate"],[dict objectForKey:@"deliveryTime"]];
    packObj.pack_Request_count = [dict objectForKey:@"requestCount"];
    packObj.pack_Status_Text = [dict objectForKey:@"statusText"];
    
    
    
    return packObj;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


//=================== Old + Reuse Code =========================

/* [Helper addShadowToLayer:_headerView.layer];
 [Helper addShadowToLayer:_containerView.layer];
 ROUNDEDCORNER(_containerView.layer);
 [Helper addShadowToLayer:_travelModeView.layer];
 ROUNDEDCORNER(_travelModeView.layer);
 
 // Do any additional setup after loading the view from its nib.
 [_txtDateTime setInputView:[Helper datePickerWithTag:PACKAGE_DATE_TIME]];
 [_txtDateTime setInputAccessoryView:[Helper toolbarWithTag:PACKAGE_DATE_TIME forControler:self]];
 
 [_txtCountryCode setInputView:[Helper pickerWithTag:COUNTRY_CODE forControler:self]];
 [_txtCountryCode setInputAccessoryView:[Helper toolbarWithTag:COUNTRY_CODE forControler:self]];
 
 UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
 [self.view addGestureRecognizer:tap];
 
 _lblCityDateTimeMessage.text = [NSString stringWithFormat:@"* Please enter %@ date & time",self.packageObject.to_Address.cityName];
 if ([self.packageObject.to_Address.cityId isEqualToString:self.packageObject.from_Address.cityId]) {
 for (UIButton *cutB in [_viewTravelMode subviews]) {
 if ([cutB isKindOfClass:[UIButton class]] && (cutB.tag != TRAVEL_MODE_CAR)) {
 cutB.hidden = YES;
 }
 }
 
 _carLeadingConstraint.constant = 17.0;
 }else{
 _carLeadingConstraint.constant = 123.0;
 } */

/*- (void)pickerDoneTapped:(UIButton*)sender{
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
 [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
 
 if (sender.tag == PACKAGE_DATE_TIME) {
 UIDatePicker *dtPic = (UIDatePicker*)_txtDateTime.inputView;
 _txtDateTime.text = [dateFormatter stringFromDate:[dtPic date]];
 [self.packageObject setPack_Delivery_Date_Time:_txtDateTime.text];
 [_txtDateTime resignFirstResponder];
 }
 if (sender.tag == COUNTRY_CODE) {
 UIPickerView *piker = (UIPickerView*)_txtCountryCode.inputView;
 _txtCountryCode.text = [[_arrCountry objectAtIndex:[piker selectedRowInComponent:0]] objectForKey:@"countryIsdCode"];
 [_txtCountryCode resignFirstResponder];
 }
 }
 
 - (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
 return 1;
 }
 // returns the # of rows in each component..
 - (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
 if (pickerView.tag == COUNTRY_CODE) {
 return _arrCountry.count;
 }
 return 0;
 }
 - (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
 if (pickerView.tag == COUNTRY_CODE) {
 return [[_arrCountry objectAtIndex:row] objectForKey:@"countryIsdCode"];
 }
 return Empty;
 } */

/*  if (self.packageObject.travel_Mode != TRAVEL_MODE_ALL) {
 for (UIButton *cutB in [_viewTravelMode subviews]) {
 if ([cutB isKindOfClass:[UIButton class]]){
 if (cutB.tag == self.packageObject.travel_Mode) {
 cutB.selected = YES;
 }else{
 cutB.selected = NO;

 }
 }
 }
 }
 
 
 [IQKeyboardManager sharedManager].previousNextDisplayMode = IQPreviousNextDisplayModeAlwaysShow;
 
 _txtName.text = self.packageObject.pack_Name;
 _txtEmail.text = self.packageObject.pack_Email;
 _txtCountryCode.text = [self.packageObject.pack_Country_Code isEqualToString:Empty]?self.packageObject.to_Address.countryIsdCode:self.packageObject.pack_Country_Code;
 _txtNumber.text = self.packageObject.pack_Contact_NO;
 _txtDateTime.text = self.packageObject.pack_Delivery_Date_Time;
 _txtVRemarks.text = self.packageObject.pack_Remarks;
 */


/*- (IBAction)backTapped:(id)sender{
 [self.delegate passPackageObject:self.packageObject];
 [self.navigationController popViewControllerAnimated:YES];
 }
 - (IBAction)modeOfTravelTapped:(UIButton *)sender {
 for (UIButton *cutB in [sender.superview subviews]) {
 if ([cutB isKindOfClass:[UIButton class]]) {
 cutB.selected = NO;
 }
 }
 [sender setSelected:YES];
 self.packageObject.travel_Mode = sender.tag;
 } */

/*- (IBAction)Save:(id)sender{
 if ([Helper checkEmptyField:_txtName.text]) {
 [Helper showAlertWithTitle:Empty Message:strKey_packageReceipientNameRequired];
 return;
 }
 self.packageObject.pack_Name = _txtName.text;
 if ([Helper checkEmptyField:_txtEmail.text]) {
 [Helper showAlertWithTitle:Empty Message:strKey_packageReceipientEmailRequired];
 return;
 }
 self.packageObject.pack_Email = _txtEmail.text;
 if ([Helper checkEmptyField:_txtNumber.text]) {
 [Helper showAlertWithTitle:Empty Message:strKey_packageReceipientPhoneRequired];
 return;
 }
 self.packageObject.pack_Contact_NO = _txtNumber.text;
 if ([Helper checkEmptyField:_txtDateTime.text]) {
 [Helper showAlertWithTitle:Empty Message:strKey_packageReceipientDeliveryDateTimeRequired];
 return;
 }
 self.packageObject.pack_Delivery_Date_Time = _txtDateTime.text;
 self.packageObject.pack_Country_Code = _txtCountryCode.text;
 self.packageObject.pack_Remarks = _txtVRemarks.text;
 [self callApiToAddPackage];
 
 } */


/*- (void)sendMailWithData:(NSDictionary*)param{
 // From within your active view controller
 if([MFMailComposeViewController canSendMail]) {
 MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
 mailCont.mailComposeDelegate = self;
 
 [mailCont setSubject:@"yo!"];
 [mailCont setToRecipients:[NSArray arrayWithObject:@"joel@stackoverflow.com"]];
 [mailCont setMessageBody:[NSString stringWithFormat:@" %@", param] isHTML:NO];
 
 [self presentViewController:mailCont animated:YES completion:^{
 
 }];
 }
 }
 // Then implement the delegate method
 - (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
 [self dismissViewControllerAnimated:YES completion:^{
 
 }];
 } */

/*- (void)textViewDidChange:(UITextView *)textView{
 [[textView superview] viewWithTag:10].hidden = textView.text.length>0?YES:NO;
 } */
