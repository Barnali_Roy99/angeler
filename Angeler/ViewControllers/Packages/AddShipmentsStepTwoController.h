//
//  AddShipmentsStepTwoController.h
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddPackageStepTwoDelegate <NSObject>

- (void)passPackageObject:(PackageClass*)package;

@end

@interface AddShipmentsStepTwoController : UIViewController
@property (nonatomic, assign) id <AddPackageStepTwoDelegate> delegate;
@property (nonatomic, strong) PackageClass *packageObject;

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UILabel *titlelabel;

@property (weak, nonatomic) IBOutlet UIView *viewRecipientEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtRecipientEmail;

@property (weak, nonatomic) IBOutlet UIView *viewRecipientAngelerID;
@property (weak, nonatomic) IBOutlet UITextField *txtRecipientAngelerID;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;

@property (weak, nonatomic) IBOutlet UILabel *infoTitlelabel;
@property (weak, nonatomic) IBOutlet UIView *nextView;
@property (weak, nonatomic) IBOutlet UILabel *lblNext;


@end

/*Old Code => @property (nonatomic, strong) NSArray *arrCountry; */
