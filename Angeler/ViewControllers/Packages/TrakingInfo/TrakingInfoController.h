//
//  TrakingInfoController.h
//  Angeler
//
//  Created by AJAY on 16/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrakingInfoController : UIViewController
@property (nonatomic, strong) NSString *bookingId;
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@end
