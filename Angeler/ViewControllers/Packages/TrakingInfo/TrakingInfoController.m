//
//  TrakingInfoController.m
//  Angeler
//
//  Created by AJAY on 16/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "TrakingInfoController.h"
#import "MWPhotoBrowser.h"
#import "UILabel+General.h"

@interface TrakingInfoController ()<MWPhotoBrowserDelegate>
{
    BOOL isAngelerImgTap;
    NSDictionary *data;
}

//Tracking Status

@property (weak, nonatomic) IBOutlet UIView *viewTrackingStatus;
@property (weak, nonatomic) IBOutlet UIView *viewTrackingStatusHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblTrackingStatusTitle;
@property (weak, nonatomic) IBOutlet UIView *viewTrakingStep;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trackHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *trackViewFirst;
@property (weak, nonatomic) IBOutlet UIView *trackViewlast;


//Angeler Info

@property (weak, nonatomic) IBOutlet UIView *viewAngeler;
@property (weak, nonatomic) IBOutlet UIView *viewAngelerHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerInfoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgAngelerImage;
@property (weak, nonatomic) IBOutlet UIButton *btnAngelerPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNo;


//Package Details

@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (nonatomic, strong) NSMutableArray *arrAngelerPhoto;
@property (weak, nonatomic) IBOutlet UIView *viewPackageDetails;
@property (weak, nonatomic) IBOutlet UIView *viewPackageDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageDetailsTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;

@property (weak, nonatomic) IBOutlet UILabel *lblPackageTypeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageTypeWeight;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalCostTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCost;
@property (weak, nonatomic) IBOutlet UIImageView *meetingPremiumImageIcon;

//Delivery Details

@property (weak, nonatomic) IBOutlet UIView *viewDeliveryDetails;
@property (weak, nonatomic) IBOutlet UIView *viewDeliveryDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupPtAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblFromAddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblToAddressTitle;
@property (weak, nonatomic) IBOutlet UIView *pickUpAddressView;
@property (weak, nonatomic) IBOutlet UIView *dropOffAddressView;

//Recipient Details

@property (weak, nonatomic) IBOutlet UIView *viewRecipientDetails;
@property (weak, nonatomic) IBOutlet UIView *viewRecipientDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientDeliverBy;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientDeliverByTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRecipientPhone;


//QR code

@property (weak, nonatomic) IBOutlet UIView *viewQrCode;
@property (weak, nonatomic) IBOutlet UIView *viewQrCodeHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblviewQrCodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblQrTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblQrCode;
@property (weak, nonatomic) IBOutlet UIImageView *imgQrCode;
@property (weak, nonatomic) IBOutlet UIView *QRCodeContainerView;
@property (weak, nonatomic) IBOutlet CopyTextLabel *lblShipmentUniqueID;
@property (weak, nonatomic) IBOutlet UILabel *lblIDTitle;



@end

@implementation TrakingInfoController
@synthesize topBarHeightConstraint,topNavbarView;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initialSetUp];
    [self setGesture];
    [self callApiToLoadTrakingInfo];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:true];
    [self roundAllSpecificCorner];
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"Track_Shipment", nil) capitalizedString]];
    
    //Tracking Status
    
    [self.viewTrackingStatus makeCornerRound:15.0];
    [self.viewTrackingStatus dropShadowWithColor:[UIColor lightishGrayColor]];
   
    //Angeler Info
    
    [self.viewAngeler makeCornerRound:15.0];
    [self.viewAngeler dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Package Details
    
    [self.viewPackageDetails makeCornerRound:15.0];
    [self.viewPackageDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Delivery Details
    
    [self.viewDeliveryDetails makeCornerRound:15.0];
    [self.viewDeliveryDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Recipient Details
    
    [self.viewRecipientDetails makeCornerRound:15.0];
    [self.viewRecipientDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //QR Code Details
    
    [self.viewQrCode makeCornerRound:15.0];
    [self.viewQrCode dropShadowWithColor:[UIColor lightishGrayColor]];
   
    _QRCodeContainerView.layer.borderColor = [[UIColor blackColor] CGColor];
    _QRCodeContainerView.layer.borderWidth = 1.0;
    
    
}

-(void)roundAllSpecificCorner
{
    [self.viewTrackingStatusHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewAngelerHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewPackageDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewDeliveryDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewRecipientDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewQrCodeHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
    //Tracking Status
    _lblTrackingStatusTitle.text = NSLocalizedString(@"Tracking_Status", nil);

    //Angeler Info
    _lblAngelerInfoTitle.text = NSLocalizedString(@"Angeler_Details", nil);
    _lblTransportRefNoTitle.text = [NSString stringWithFormat:@"%@ : ",NSLocalizedString(@"Flight_Number", nil)];
    
    
    //Package Info
    _lblPackageDetailsTitle.text = NSLocalizedString(@"Item_Details", nil);
    
    _lblPackageTypeTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Item_Category", nil)];
    _lblTotalCostTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Price", nil)];
    
    //Delivery Info
     _lblDeliveryDetailsTitle.text = NSLocalizedString(@"Pickup_DropOff_Details",nil);
    _lblFromAddressTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"From", nil)];
    _lblToAddressTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"To", nil)];
    
    //Recipient Details
    
    _lblRecipientDetailsTitle.text = NSLocalizedString(@"Recipient_Details",nil);
    _lblRecipientDeliverByTitle.text = NSLocalizedString(@"Deliver_Before",nil);
    
    //QR code
    
    _lblviewQrCodeTitle.text = NSLocalizedString(@"Qr_Code",nil);
     _lblIDTitle.text = NSLocalizedString(@"AGTN", nil);
    
    
    
}

-(void)setGesture
{
    [_lblQrTitle setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)];
    [_lblQrTitle addGestureRecognizer:tapges];
    
}

#pragma mark - Gesture Action

- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture
{

    isAngelerImgTap = true;
    [self setMWPhotoBrowserWithTag:0];
    
}



#pragma mark - API Call

- (void)callApiToLoadTrakingInfo{
    /*
     Request type: query string
     Request parameter:
     userId, sessionId, os, osVersion, appVersion, bookingId
     
     Request example :
     http://angeler.serveraddress.com/web_services/package/tracking_details?userId=3&sessionId=7673576e476adb1430626e4d1c7a734af96fc104&os=Android&osVersion=M&appVersion=1.0&bookingId=06546

     */
    NSDictionary *param = @{@"userEmail":[[GlobalInfo sharedInfo] userEmail],
                            @"bookingId":_bookingId
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPackageTrakingInfo
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               data = [responseObject objectForKey:@"responseData"];
               [self showInfoWithData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}

- (void)showInfoWithData{
    //Track info
    { //track
        NSArray *arrTrackInfo = [data objectForKey:@"trackingDetails"];
        
        if (arrTrackInfo.count == 0) {[GlobalInfo hideViewNamed:_trackViewFirst.superview withTopName:Empty]; return;}
        
        BOOL FistIsNotLast = NO;
        
        if (arrTrackInfo.count == 1) {
            _trackHeightConstraint.constant = 0;
            _trackViewlast.hidden = YES;

        }else if(arrTrackInfo.count > 2){
            FistIsNotLast = YES;
            
            for (int i = 1; i < arrTrackInfo.count-1; i++) {
                CGRect fromImg = [_trackViewFirst viewWithTag:10].frame;
                CGRect fromLbl = [_trackViewFirst viewWithTag:20].frame;
                
                UIImageView *newImgV = [[UIImageView alloc] init];
                UILabel *lblNew = [[UILabel alloc] init];
                lblNew.font = [UIFont fontWithName:@"Roboto-Regular" size:15.0];
                //lblNew.backgroundColor = [UIColor yellowColor];
                
                newImgV.image = [[[arrTrackInfo objectAtIndex:i] objectForKey:@"status"] isEqualToString:@"Pending"]?[UIImage imageNamed:@"package_tracking_gray"]:[UIImage imageNamed:@"package_tracking_Blue"];
                lblNew.text = [[arrTrackInfo objectAtIndex:i] objectForKey:@"text"];
                
                
                fromImg.origin.y = fromImg.origin.y + 15 + (40*i);
                fromLbl.origin.y = fromLbl.origin.y + 15 + (40*i);
                
                fromImg.origin.x = fromImg.origin.x + 20;
                fromLbl.origin.x = fromLbl.origin.x + 20;
                
                newImgV.frame = fromImg;
                lblNew.frame = fromLbl;
                
                [_viewTrakingStep addSubview:newImgV];
                [_viewTrakingStep addSubview:lblNew];
                _trackHeightConstraint.constant = _trackHeightConstraint.constant + 25;
            }
            
        }else{
            FistIsNotLast = YES;
        }
        {// for first step
            UIImageView *imgV = [_trackViewFirst viewWithTag:10];
            UILabel *lblStep = [_trackViewFirst viewWithTag:20];
            imgV.image = [[[arrTrackInfo firstObject] objectForKey:@"status"] isEqualToString:@"Pending"]?[UIImage imageNamed:@"package_tracking_gray"]:[UIImage imageNamed:@"package_tracking_Blue"];
            lblStep.text = [[arrTrackInfo firstObject] objectForKey:@"text"];
        }
        if (FistIsNotLast) { // for last step
            UIImageView *imgV = [_trackViewlast viewWithTag:10];
            UILabel *lblStep = [_trackViewlast viewWithTag:20];
            imgV.image = [[[arrTrackInfo lastObject] objectForKey:@"status"] isEqualToString:@"Pending"]?[UIImage imageNamed:@"package_tracking_gray"]:[UIImage imageNamed:@"package_tracking_Blue"];
            lblStep.text = [[arrTrackInfo lastObject] objectForKey:@"text"];
        }
    }
    
    //Angeler Info
    NSDictionary *angelerData = [data objectForKey:@"angelerDetails"];
    if ([angelerData allKeys].count == 0) {
        [GlobalInfo hideViewNamed:_viewAngeler withTopName:@"angelerTop"];
    }else{
        [Helper makeItRound:_imgAngelerImage.layer withRadius:25.0];
        
        _imgAngelerImage.userInteractionEnabled = YES;
        [_imgAngelerImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnAngelerImage:)]];
        self.arrAngelerPhoto = [NSMutableArray array];
        MWPhoto *angelerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[angelerData objectForKey:@"angelerImage"]]];
        angelerPhoto.caption =[NSString stringWithFormat:@"Angeler : %@",[angelerData objectForKey:@"angelerName"]];
        [self.arrAngelerPhoto addObject:angelerPhoto];
        
        [_imgAngelerImage sd_setImageWithURL:[NSURL URLWithString:[angelerData objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        _lblAngelerName.text = [angelerData objectForKey:@"angelerName"];
        _lblAngelerEmail.text = [angelerData objectForKey:@"angelerEmail"];
        _lblAngelerPhone.hidden = [[angelerData objectForKey:@"angelerPhoneNo"] isEqualToString:Empty];
        _lblAngelerPhone.text = [angelerData objectForKey:@"angelerPhoneNo"];
//        if (_lblAngelerPhone.text.length > PHONE_MIN_DIGIT)
//        {
//            _btnAngelerPhone.hidden = NO;
//        }
//        else
//        {
//            _btnAngelerPhone.hidden = YES;
//        }
    }
    
     NSDictionary *pickUpDeliveryData = [data objectForKey:@"bookingDetails"];
    
    //Flight Number Set
    _lblTransportRefNo.text = [pickUpDeliveryData objectForKey:@"travelRefNo"];
    
    //Package Details
    NSDictionary *pckgDetailsDic = [data objectForKey:@"packageDetails"];
    
    _lblShipmentUniqueID.text = [pckgDetailsDic objectForKey:@"shipmentUniqueId"];
    
    _lblPackageTypeWeight.text = [NSString stringWithFormat:@"%@ | %@",[GlobalInfo TypeOfPackage:[[pckgDetailsDic objectForKey:@"packageType"] integerValue]],[NSString stringWithFormat:@"%@ %@",[pckgDetailsDic objectForKey:@"packageWeight"],NSLocalizedString(@"package_weight_unit", nil)]];
    _lblTotalCost.text = [NSString stringWithFormat:@"%@ %@",[pickUpDeliveryData objectForKey:@"serviceCost"],NSLocalizedString(@"Credits", nil)];
    
    if ([[pickUpDeliveryData objectForKey:@"serviceType"] isEqualToString:serviceType_Premium])
    {
        [_meetingPremiumImageIcon setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
    }
    else
    {
         [_meetingPremiumImageIcon setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
    }
    
    //   Item Details
    
    NSArray *imges = [pckgDetailsDic objectForKey:@"images"];
    if (imges.count == 0) {
        
    }else{
        self.arrPhotos = [NSMutableArray array];
        for (int i = 1; i <= imges.count ; i++) {
            UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
            imgV.hidden = NO;
            imgV.userInteractionEnabled = YES;
            [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
            NSLog(@"image url =====  %@",[[imges objectAtIndex:i-1] objectForKey:@"image"]);
            [imgV sd_setImageWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
            [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]]]];
        }
    }
    
    //Delivery Details
    
   
     _lblPickupPtAddress.text = [pickUpDeliveryData objectForKey:@"angelerPickupAddress"];
    _lblDeliveryPtAddress.text = [pickUpDeliveryData objectForKey:@"angelerDropoffAddress"];
    

    //Gesture Action
    
    self.pickUpAddressView.userInteractionEnabled = YES;
    [self.pickUpAddressView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickUpAddressTap:)]];
    
    self.dropOffAddressView.userInteractionEnabled = YES;
    [self.dropOffAddressView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dropOffAddressTap:)]];
    
    // Recipient Info //packageDetails
    NSDictionary *recipientInfoData = [data objectForKey:@"packageDetails"];

        _lblRecipientName.text = [recipientInfoData objectForKey:@"recipientName"];
        _lblRecipientEmail.text = [recipientInfoData objectForKey:@"recipientEmail"];
        _lblRecipientPhone.hidden = [[recipientInfoData objectForKey:@"recipientPhone"] isEqualToString:Empty];
        _lblRecipientPhone.text = [recipientInfoData objectForKey:@"recipientPhone"];
        _lblRecipientDeliverBy.text = [Helper getDateTimeToStringWithDate:[recipientInfoData objectForKey:@"deliveryDate"] Time:[recipientInfoData objectForKey:@"deliveryTime"]];
    
//    if (_lblRecipientPhone.text.length > PHONE_MIN_DIGIT)
//    {
//        _btnRecipientPhone.hidden = NO;
//    }
//    else
//    {
//        _btnRecipientPhone.hidden = YES;
//    }
 
    //Code Details
    NSDictionary *qrCodeData = [data objectForKey:@"codeDetails"];
//    if ([qrCodeData allKeys].count == 0) {
//        [GlobalInfo hideViewNamed:_viewQrCode withTopName:@"qrTop"];
//    }else{
        NSString *title = Empty;
        if ([[qrCodeData objectForKey:@"type"] isEqualToString:@"PP"] || [[qrCodeData objectForKey:@"type"] isEqualToString:@"PMP"]) {
            title = [NSString stringWithFormat:NSLocalizedString(@"Scan_Code_Track_Shipment_PickUp", nil),[[angelerData objectForKey:@"angelerName"] uppercaseString]];
            
            _lblQrTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[[angelerData objectForKey:@"angelerName"] uppercaseString]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
            
        }
        else if ([[qrCodeData objectForKey:@"type"] isEqualToString:@"PPP"]) {
            title = NSLocalizedString(@"Show_QR_Code_PickUp_Info", nil);
            _lblQrTitle.text = title;
            
        }

        _lblQrCode.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Pickup_Code", nil),[qrCodeData objectForKey:@"code"]];
        
        _imgQrCode.image = [self qrImageForCode:[qrCodeData objectForKey:@"code"] andImageView:_imgQrCode];
        
   
}



- (UIImage*)qrImageForCode:(NSString*)qrString andImageView:(UIImageView*)qrImageView{
    //    NSString *qrString = @"My string to encode";
    NSData *stringData = [qrString dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = qrImageView.frame.size.width / qrImage.extent.size.width;
    float scaleY = qrImageView.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    return [UIImage imageWithCIImage:qrImage scale:[UIScreen mainScreen].scale
                         orientation:UIImageOrientationUp];
}

#pragma mark - Button Action


- (IBAction)callRecipient:(id)sender {
   // if (_lblRecipientPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblRecipientPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
  //  }
}
- (IBAction)callAngeler:(id)sender {
  //  if (_lblAngelerPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblAngelerPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
   // }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Gesture Action

- (void)pickUpAddressTap:(UITapGestureRecognizer*)tapGest
{
    
    NSDictionary *bookingDetailsData = [data objectForKey:@"bookingDetails"];
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic",[bookingDetailsData objectForKey:@"angelerPickupLatitude"] ,[bookingDetailsData objectForKey:@"angelerPickupLongitude"]]]];
    } else {
        NSLog(@"Can't use comgooglemaps://");
    }
    
}

- (void)dropOffAddressTap:(UITapGestureRecognizer*)tapGest
{
    NSDictionary *bookingDetailsData = [data objectForKey:@"bookingDetails"];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic",[bookingDetailsData objectForKey:@"angelerDropoffLatitude"] ,[bookingDetailsData objectForKey:@"angelerDropoffLongitude"]]]];
    } else {
        NSLog(@"Can't use comgooglemaps://");
    }
}

- (void)tapOnAngelerImage:(UITapGestureRecognizer*)tapGest{
    
    isAngelerImgTap = true;
    [self setMWPhotoBrowserWithTag:0];
    
}

- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    isAngelerImgTap = false;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}


#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isAngelerImgTap)
        return self.arrAngelerPhoto.count;
    else
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    
    if (isAngelerImgTap)
    {
        if (index < self.arrAngelerPhoto.count) {
            return [self.arrAngelerPhoto objectAtIndex:index];
        }
    }
    else
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
//==================== old + reuse Code ===========================

//}



//Picup point data
//   NSDictionary *pickupData = [data objectForKey:@"pickupPointDetails"];
//    if ([pickupData allKeys].count == 0) {
//        [GlobalInfo hideViewNamed:_viewPickupPt withTopName:@"pickupPointTop"];
//    }else{
//        _lblPickupPtAddress.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address: %@",[pickupData objectForKey:@"address"]] andIndex:8];
//
//        _lblPickupPtAddressType.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[pickupData objectForKey:@"addressType"]] andIndex:12];
//
//        _lblPickupPtName.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Name: %@",[pickupData objectForKey:@"contactName"]] andIndex:5];
//
//        _lblPickupPtPhone.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Phone: %@%@",[pickupData objectForKey:@"contactPhoneISD"],[pickupData objectForKey:@"contactPhone"]] andIndex:6];
//
//        _lblPickupPtOperatingHours.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Operating hours: %@-%@",[pickupData objectForKey:@"operatingFromHours"],[pickupData objectForKey:@"operatingToHours"]] andIndex:16];
//    }
//    //Delivery Point data
//    NSDictionary *deliveryPointData = [data objectForKey:@"deliveryPointDetails"];
//    if ([deliveryPointData allKeys].count == 0) {
//        [GlobalInfo hideViewNamed:_viewDeliveryPt withTopName:@"deliveryPointTop"];
//    }else{
//
//        _lblDeliveryPtAddress.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address: %@",[deliveryPointData objectForKey:@"address"]] andIndex:8];
//
//        _lblDeliveryPtAddressType.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[deliveryPointData objectForKey:@"addressType"]] andIndex:12];
//
//        _lblDeliveryPtName.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Name: %@",[deliveryPointData objectForKey:@"contactName"]] andIndex:5];
//
//        _lblDeliveryPtPhone.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Phone: %@%@",[deliveryPointData objectForKey:@"contactPhoneISD"],[deliveryPointData objectForKey:@"contactPhone"]] andIndex:6];
//
//        _lblDeliveryPtOperatingHours.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Operating hours: %@-%@",[deliveryPointData objectForKey:@"operatingFromHours"],[deliveryPointData objectForKey:@"operatingToHours"]] andIndex:16];
//
//    }


 
 //    // PickUp Delivery Location //pickupDeliveryTop
 //    NSDictionary *pickUpDeliveryData = [data objectForKey:@"bookingDetails"];
 //    if ([pickUpDeliveryData allKeys].count == 0) {
 //        [GlobalInfo hideViewNamed:_viewPickUpNDeliveryLocation withTopName:@"pickupDeliveryTop"];
 //    }else{
 //        [_imgPickupLocation setImage:[GlobalInfo AngelerImageForPickupType:[pickUpDeliveryData objectForKey:@"pickupType"]]];
 //        [_imgDeliveryLocation setImage:[GlobalInfo AngelerImageForPickupType:[pickUpDeliveryData objectForKey:@"dropoffType"]]];
 //        _lblPickUpLocation.text = [pickUpDeliveryData objectForKey:@"angelerPickupAddress"];
 //        _lblDeliveryLocation.text = [pickUpDeliveryData objectForKey:@"angelerDropoffAddress"];
 //    }
 //
 
 //    if ([recipientInfoData allKeys].count == 0) {
 //        [GlobalInfo hideViewNamed:_viewRecipient withTopName:@"recipientTop"];
 //    }else{

// Do any additional setup after loading the view from its nib.
//    [Helper addShadowToLayer:_headerView.layer];
//    [Helper addShadowToLayer:_viewAngeler.layer];
//    [Helper addShadowToLayer:_viewRecipient.layer];
//    [Helper addShadowToLayer:_viewDeliveryPt.layer];
//    [Helper addShadowToLayer:_viewPickupPt.layer];
//    [Helper addShadowToLayer:_viewQrCode.layer];
//    [Helper addShadowToLayer:_viewTrakingStep.layer];
//    [Helper addShadowToLayer:_viewPickUpNDeliveryLocation.layer];
//    ROUNDEDCORNER(_viewAngeler.layer);
//    ROUNDEDCORNER(_viewRecipient.layer);
//    ROUNDEDCORNER(_viewDeliveryPt.layer);
//    ROUNDEDCORNER(_viewPickupPt.layer);
//    ROUNDEDCORNER(_viewQrCode.layer);
//    ROUNDEDCORNER(_viewTrakingStep.layer);
//    ROUNDEDCORNER(_viewPickUpNDeliveryLocation.layer);
//


//@property (weak, nonatomic) IBOutlet UIView *viewPickUpNDeliveryLocation;
//@property (weak, nonatomic) IBOutlet UILabel *lblPickUpLocation;
//@property (weak, nonatomic) IBOutlet UIImageView *imgPickupLocation;
//@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryLocation;
//@property (weak, nonatomic) IBOutlet UIImageView *imgDeliveryLocation;
//
//@property (weak, nonatomic) IBOutlet UIView *viewRecipient;
//
//
//@property (weak, nonatomic) IBOutlet UIView *viewPickupPt;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblPickupPtAddressType;
//@property (weak, nonatomic) IBOutlet UILabel *lblPickupPtName;
//@property (weak, nonatomic) IBOutlet UILabel *lblPickupPtPhone;
//@property (weak, nonatomic) IBOutlet UILabel *lblPickupPtOperatingHours;
//
//@property (weak, nonatomic) IBOutlet UIView *viewDeliveryPt;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtAddressType;
//@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtName;
//@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtPhone;
//@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryPtOperatingHours;
//
//@property (weak, nonatomic) IBOutlet UIView *headerView;
/*  [[NSNotificationCenter defaultCenter] addObserver:self
 selector:@selector(receiveTestNotification:)
 name:@"TrakingInfoController"
 object:nil];
 - (void)receiveTestNotification:(id)sender{
 [self viewDidLoad];
 }
 - (void) dealloc
 {
 [[NSNotificationCenter defaultCenter] removeObserver:self];
 }
*/

/*- (IBAction)backtapped:(id)sender{
 [self.navigationController popViewControllerAnimated:YES];
 } */
