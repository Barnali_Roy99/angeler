//
//  PackageCell.h
//  Angeler
//
//  Created by AJAY on 04/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundImageView.h"

@interface PackageCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UIView *roundView;
//@property (weak, nonatomic) IBOutlet UILabel *lblPackDateTime;
//@property (weak, nonatomic) IBOutlet UILabel *lblFromAddress;
//@property (weak, nonatomic) IBOutlet UILabel *lblToAddress;
//@property (weak, nonatomic) IBOutlet UILabel *lblTravelMode;
//@property (weak, nonatomic) IBOutlet UILabel *lblDocType;
//@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
//@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
//
//@property (weak, nonatomic) IBOutlet UIButton *btnBookingRequest;

@property (weak, nonatomic) IBOutlet UIView *menu_Container;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
//@property (weak, nonatomic) IBOutlet UIButton *btnShipmentDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnRequestList;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *fromAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *toAddressLabel;
@property (weak, nonatomic) IBOutlet UIView *shipmentImgBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *shipmentImageView;
@property (weak, nonatomic) IBOutlet UIButton *packageStatusActionBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblDocumentWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryDate;
@property (weak, nonatomic) IBOutlet UIView *logoImagesHolderView;
@property (weak, nonatomic) IBOutlet RoundImageView *angelerProfileImageview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageHolderViewHeightConstraint;
@property (weak, nonatomic) IBOutlet RoundImageView *meetingPointPremiumImageView;


@end
