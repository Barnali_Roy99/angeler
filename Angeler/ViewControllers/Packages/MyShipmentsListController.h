//
//  MyShipmentsListController.h
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MyShipmentsListController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
-(void)showPendingAccountAlertPopUp;
-(void)showNotVerifiedAccountAlertPopUp;
-(void)showRejectedAccountAlertPopUp;

@end
