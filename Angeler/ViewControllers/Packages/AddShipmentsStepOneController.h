//
//  AddShipmentsStepOneController.h
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddShipmentsStepOneController : UIViewController

@property (nonatomic, strong) PackageClass *packageObject;
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

@property (weak, nonatomic) IBOutlet UILabel *sendFromTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliverToTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblFromAddressLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblToAddressLocation;
@property (weak, nonatomic) IBOutlet UIView *viewFromAddress;
@property (weak, nonatomic) IBOutlet UIView *viewToAddress;


@property (weak, nonatomic) IBOutlet UIView *viewFromCity;
@property (weak, nonatomic) IBOutlet UIView *viewToCity;
@property (weak, nonatomic) IBOutlet UILabel *fromSelectCityTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *fromSelectCityValueTextfield;
@property (weak, nonatomic) IBOutlet UILabel *toSelectCityTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *toSelectCityValueTextfield;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fromSelectCityViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toSelectCityViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fromSelectCityViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toSelectCityViewBottomConstraint;

@property (weak, nonatomic) IBOutlet UIView *viewDeliveryDateTime;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDateTimeTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *deliveryDateTimeValueTextfield;

@property (weak, nonatomic) IBOutlet UIView *viewDocument;
@property (weak, nonatomic) IBOutlet UIView *txtFieldViewDocument;
@property (weak, nonatomic) IBOutlet UILabel *documentTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *txtDocument;


@property (weak, nonatomic) IBOutlet UIView *viewWeight;
@property (weak, nonatomic) IBOutlet UIView *txtFieldViewWeight;
@property (weak, nonatomic) IBOutlet UILabel *weightTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;

@property (weak, nonatomic) IBOutlet UILabel *itemPictureTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *itemPicOneView;
@property (weak, nonatomic) IBOutlet UIView *itemPicSecondView;
@property (weak, nonatomic) IBOutlet UIView *itemPicThirdView;

@property (weak, nonatomic) IBOutlet UIImageView *itemPicOneImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicSecondImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicThirdImgView;

@property (weak, nonatomic) IBOutlet UIButton *itemPicOneCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicSecondCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicThirdCancelButton;

@property (weak, nonatomic) IBOutlet UISwitch *termsConditionSwitch;
@property (nonatomic, weak) IBOutlet UILabel *termsConditionsTextLabel;

@property (weak, nonatomic) IBOutlet UIView *nextView;
@property (weak, nonatomic) IBOutlet UIImageView *nextImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblNext;



@end
