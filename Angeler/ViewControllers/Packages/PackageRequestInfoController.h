//
//  PackageRequestInfoController.h
//  Angeler
//
//  Created by AJAY on 19/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageRequestInfoController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSString *packageId;
@end
