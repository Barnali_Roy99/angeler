//
//  AddShipmentsStepOneController.m
//  Angeler
//
//  Created by AJAY on 28/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//



//========= Package = Shipment (New Logic)======

#import "AddShipmentsStepOneController.h"
#import "AddShipmentsStepTwoController.h"
#import "MyShipmentsListController.h"
#import "SingleLocationMapController.h"
#import "IQKeyboardManager.h"
#define MaxItemImageCount 3

@interface AddShipmentsStepOneController ()<SingleLocationAddressDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AddPackageStepTwoDelegate>

@property (nonatomic, strong) NSArray *arrPickerData;
@property (nonatomic, strong) NSArray *arrCountryList;

@property (nonatomic, strong) NSArray *arrFromCityList;
@property (nonatomic, strong) NSArray *arrToCityList;

@property (nonatomic, strong) NSMutableArray *arrMediaObjects;




@property (nonatomic, assign) NSInteger wtCounter;

@end

@implementation AddShipmentsStepOneController
@synthesize topBarHeightConstraint,topNavbarView,mainContentView,sendFromTitleLabel,deliverToTitleLabel,lblFromAddressLocation,lblToAddressLocation,viewFromAddress,viewToAddress,viewToCity,viewFromCity,toSelectCityValueTextfield,fromSelectCityTitleLabel,fromSelectCityValueTextfield,toSelectCityTitleLabel,toSelectCityViewHeightConstraint,fromSelectCityViewHeightConstraint,fromSelectCityViewBottomConstraint,toSelectCityViewBottomConstraint,deliveryDateTimeTitleLabel,deliveryDateTimeValueTextfield,viewDeliveryDateTime,documentTitleLabel,txtDocument,viewDocument,txtFieldViewDocument,weightTitleLabel,txtWeight,viewWeight,txtFieldViewWeight,itemPicOneCancelButton,itemPicThirdCancelButton,itemPicSecondCancelButton,itemPicOneImgView,itemPicSecondImgView,itemPicThirdImgView,itemPicOneView,itemPicSecondView,itemPicThirdView,itemPictureTitleLabel,termsConditionSwitch,termsConditionsTextLabel,nextView;


#pragma mark - View life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.packageObject = [[PackageClass alloc] init];
    _arrMediaObjects = [[NSMutableArray alloc] init];
    txtDocument.text = [GlobalInfo TypeOfPackage:[self.packageObject.pack_Type integerValue]];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [self initialSetUp];
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    [self setInitialConstraint];
    [self setPlaceHolderText];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"Send", nil) capitalizedString]];
   
    
    [mainContentView makeCornerRound:15.0];
    [viewFromAddress makeCornerRound:15.0];
    [viewToAddress makeCornerRound:15.0];
    [viewFromCity makeCornerRound:15.0];
    [viewToCity makeCornerRound:15.0];
    [viewDeliveryDateTime makeCornerRound:15.0];
    
    
    [viewDocument makeCornerRound:15.0 withBoarderColor:[UIColor lightGrayColor] borderWidth:1.0];
    
    
   
    [viewWeight makeCornerRound:15.0 withBoarderColor:[UIColor lightGrayColor] borderWidth:1.0];
    
    
    //City Picker Set
    
    [fromSelectCityValueTextfield setInputView:[Helper pickerWithTag:DEPRT_CITY forControler:self]];
    [fromSelectCityValueTextfield setInputAccessoryView:[Helper toolbarWithTag:DEPRT_CITY forControler:self]];
    [toSelectCityValueTextfield setInputView:[Helper pickerWithTag:DEST_CITY forControler:self]];
    [toSelectCityValueTextfield setInputAccessoryView:[Helper toolbarWithTag:DEST_CITY forControler:self]];
    
    //Delivery Date Time Picker Set
    
    [deliveryDateTimeValueTextfield setInputView:[Helper datePickerWithTag:PACKAGE_DATE_TIME]];
    [deliveryDateTimeValueTextfield setInputAccessoryView:[Helper toolbarWithTag:PACKAGE_DATE_TIME forControler:self]];
    
    //Document Picker Set
    
    [txtDocument setInputView:[Helper pickerWithTag:PACKAGE_TYPE forControler:self]];
    [txtDocument setInputAccessoryView:[Helper toolbarWithTag:PACKAGE_TYPE forControler:self]];
    
    
    //Weight Counter Set
    _wtCounter = 1;
    self.txtWeight.text = [NSString stringWithFormat:@"%ld %@",(long)_wtCounter,NSLocalizedString(@"package_weight_unit", nil)];
    
    //Picture Item set Up
    
    itemPicOneCancelButton.hidden = YES;
    itemPicSecondCancelButton.hidden = YES;
    itemPicThirdCancelButton.hidden = YES;
    
    
    //Terms & Conditions
//    NSString *TC_text = NSLocalizedString(@"TermsConditions_Text", nil) ;
//    NSString *TC_underLineText = NSLocalizedString(@"TermsConditions_UnderlineText", nil) ;
//    termsConditionsTextLabel.attributedText = [Helper getUnderlineText:TC_text :[TC_text rangeOfString:TC_underLineText] withColor:[UIColor mediumBlueColor]];
    
    termsConditionSwitch.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [termsConditionSwitch setOn:NO];
    
    
    //Next View
    
    UITapGestureRecognizer *nextViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextViewTapped:)];
    [self.nextView addGestureRecognizer:nextViewTapRecognizer];
    
}

-(void)roundAllSpecificCorner
{
    [txtFieldViewDocument roundSpicficCorner:UIRectCornerBottomRight | UIRectCornerTopRight withRadius:14.0];
     [txtFieldViewWeight roundSpicficCorner:UIRectCornerBottomRight | UIRectCornerTopRight withRadius:14.0];
}

-(void)setPlaceHolderText{
    
    //Localization
    
    UIColor *color = [UIColor blackColor];
    self.lblFromAddressLocation.text = NSLocalizedString(@"Address", nil);
     self.lblToAddressLocation.text = NSLocalizedString(@"Address", nil);
    self.deliveryDateTimeValueTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Please_Select_DateTime", nil) attributes:@{NSForegroundColorAttributeName: color}];
}


-(void)setInitialConstraint
{
    fromSelectCityViewHeightConstraint.constant = 0;
    viewFromCity.superview.hidden = YES;
    fromSelectCityViewBottomConstraint.constant = 0;
    
    toSelectCityViewHeightConstraint.constant = 0;
    viewToCity.superview.hidden = YES;
    toSelectCityViewBottomConstraint.constant = 0;
}

-(void)setLocalization
{
    sendFromTitleLabel.text = NSLocalizedString(@"Send_From", nil);
    deliverToTitleLabel.text = NSLocalizedString(@"Deliver_To", nil);
    deliveryDateTimeTitleLabel.text = NSLocalizedString(@"Deliver_Before", nil);
    documentTitleLabel.text = NSLocalizedString(@"I_Want_To_Send", nil);
    weightTitleLabel.text = NSLocalizedString(@"Weight", nil);
    itemPictureTitleLabel.text = NSLocalizedString(@"Picture_Of_Item", nil);
    self.lblNext.text =  NSLocalizedString(@"Next", nil);
    fromSelectCityTitleLabel.text = NSLocalizedString(@"Main_City", nil);
    toSelectCityTitleLabel.text = NSLocalizedString(@"Main_City", nil);
    
}

-(void)setGesture
{
    UITapGestureRecognizer *sendFrompViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendFromLocationViewTapped:)];
    [viewFromAddress addGestureRecognizer:sendFrompViewTapRecognizer];
    
    
    UITapGestureRecognizer *deliverToTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deliverToLocationViewTapped:)];
     [viewToAddress addGestureRecognizer:deliverToTapRecognizer];
    
    
    UITapGestureRecognizer *imageFirstTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicOneImgView addGestureRecognizer:imageFirstTapRecognizer];
    
    UITapGestureRecognizer *imageSecondTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicSecondImgView addGestureRecognizer:imageSecondTapRecognizer];
    
    UITapGestureRecognizer *imageThirdTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicThirdImgView addGestureRecognizer:imageThirdTapRecognizer];
}



- (void)showSingleAddressMapWithTag:(TAG_VALUE)tag
{
    SingleLocationMapController *mapObj = [[SingleLocationMapController alloc] initWithNibName:@"SingleLocationMapController" bundle:nil];
    mapObj.delegate = self;
    
    mapObj.tag = tag;
    if (tag == PACKAGE_FROM) {
        
        mapObj.prevlat = self.packageObject.from_Address.lat_Value;
        mapObj.prevLong = self.packageObject.from_Address.long_Value;
    }else{
        
        mapObj.prevlat = self.packageObject.to_Address.lat_Value;
        mapObj.prevLong = self.packageObject.to_Address.long_Value;
    }
    
    [self.navigationController pushViewController:mapObj animated:YES];
}

-(BOOL)isSameCountryChoose
{
    if ([self.packageObject.from_Address.countryId isEqualToString:self.packageObject.to_Address.countryId]) {
        
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Different_Country_selection_alert", nil)];
        return YES;
    }
    return NO;
}


- (BOOL)validatePackage{
    if ([Helper checkEmptyField:self.packageObject.from_Address.Address]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Add_Shipment_From_Address_Blank_Alert", nil)];
        return NO;
    }
    
    if (!([viewFromCity.superview isHidden]) && ([fromSelectCityValueTextfield.text isEqualToString:Empty]))
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_From_city_selection_alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:self.packageObject.to_Address.Address]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Add_Shipment_To_Address_Blank_Alert", nil)];
        return NO;
    }
    
    if (!([viewToCity.superview isHidden]) && ([toSelectCityValueTextfield.text isEqualToString:Empty]))
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_To_city_selection_alert", nil)];
        return NO;
    }
    
//    if ([self.packageObject.from_Address.cityId isEqualToString:self.packageObject.to_Address.cityId]) {
//
//        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"AddTrip_Different_City_selection_alert", nil)];
//        return NO;
//    }
    
    if ([Helper checkEmptyField:self.packageObject.pack_Delivery_Date_Time]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Add_Shipment_Delivery_DateTime_Blank_Alert", nil)];
        return NO;
    }
    
   if([self.packageObject.pack_Type isEqualToString:zeroUnit])
   {
       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Package_Category_Choose_Alert", nil)];
       return NO;
   }
    
    
    if (_arrMediaObjects.count == 0) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Add_Shipment_Image_Blank_Alert", nil)];
        return NO;
    }
    
    if (![termsConditionSwitch isOn])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"TermsConditions_Alert", nil)];
        return NO;
    }
    
    return YES;
}



#pragma mark - Gesture Action


- (void)sendFromLocationViewTapped:(UITapGestureRecognizer*)sender
{
    [self showSingleAddressMapWithTag:PACKAGE_FROM];
}

- (void)deliverToLocationViewTapped:(UITapGestureRecognizer*)sender
{
     [self showSingleAddressMapWithTag:PACKAGE_TO];
}

- (void)captureImageTapped:(UITapGestureRecognizer*)sender
{
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
                imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
               [Helper showCameraGoToSettingsAlert];
            }
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CameraNotAvailable_Alert", nil)];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
            imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"GalleryNotAvailable_Alert", nil)];
        }
    }];
    
    if (_arrMediaObjects.count >= MaxItemImageCount) {
        [GlobalInfo showAlertTitle:Empty Message:@"You cannot upload any more image" actions:[NSArray arrayWithObject:cancel]];
    }else{
        [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Upload_Image_From", nil) actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];
    }
}


#pragma mark - Button Action

- (void)nextViewTapped:(UITapGestureRecognizer*)sender
{
    if ([self validatePackage]) {
        self.packageObject.arrItemImages = self.arrMediaObjects;
        self.packageObject.pack_Height = Empty;
        self.packageObject.pack_Width = Empty;
        self.packageObject.pack_Length = Empty;
        self.packageObject.pack_Weight = [NSString stringWithFormat:@"%ld",_wtCounter];
        
        AddShipmentsStepTwoController *nxtStep = [[AddShipmentsStepTwoController alloc] initWithNibName:@"AddShipmentsStepTwoController" bundle:nil];
        nxtStep.packageObject = self.packageObject;
        [self.navigationController pushViewController:nxtStep animated:YES];
        
    }
}

-(IBAction)MaxMinWeightTapped:(UIButton*)sender{
    
    if (sender.tag == 1 && _wtCounter > 1) {
        _wtCounter = _wtCounter - 1;
    }else if(sender.tag == 2 && _wtCounter < 10){
        _wtCounter = _wtCounter + 1;
    }
    self.txtWeight.text = [NSString stringWithFormat:@"%ld %@",(long)_wtCounter,NSLocalizedString(@"package_weight_unit", nil)];
}




#pragma mark-SingleAddressDelegate

- (void)selectedAddress:(AddressClass*)address forTag:(TAG_VALUE)tag
{
    NSDictionary *param = @{@"countryName":[address countryName],
                            @"cityName":[address cityName]};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kCountryCityDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSDictionary *responData = [responseObject objectForKey:@"responseData"];
                                                           if ([[responData objectForKey:@"countryFound"] isEqualToString:@"Yes"]) {
                                                               address.countryId = [responData objectForKey:@"countryId"];
                                                               address.countryName = [responData objectForKey:@"countryName"];
                                                               address.countryCode = [responData objectForKey:@"countryCode"];
                                                               address.countryIsdCode = [responData objectForKey:@"countryISDCode"];
                                                               BOOL cityEditable = YES;
                                                               BOOL isSameCountryChoose = NO;
                                                               
                                                            //Same country will not permit
                                                               if(tag == PACKAGE_FROM){
                                                                   self.packageObject.from_Address.countryId = address.countryId;
                                                                   if([self isSameCountryChoose])
                                                                   {
                                                                       lblFromAddressLocation.text = Empty;
                                                                       fromSelectCityValueTextfield.text = Empty;
                                                                       fromSelectCityViewHeightConstraint.constant = 0;
                                                                       viewFromCity.superview.hidden = YES;
                                                                       fromSelectCityViewBottomConstraint.constant = 0;
                                                                       self.packageObject.from_Address.Address = Empty;
                                                                       isSameCountryChoose = YES;
                                                                       self.packageObject.from_Address.countryId = @"";
                                                                   }
                                                               }
                                                               else if(tag == PACKAGE_TO){
                                                               
                                                                   self.packageObject.to_Address.countryId = address.countryId;
                                                                   if([self isSameCountryChoose])
                                                                   {
                                                                       lblToAddressLocation.text = Empty;
                                                                       toSelectCityValueTextfield.text = Empty;
                                                                       toSelectCityViewHeightConstraint.constant = 0;
                                                                       viewToCity.superview.hidden = YES;
                                                                       toSelectCityViewBottomConstraint.constant = 0;
                                                                       self.packageObject.to_Address.Address = Empty;
                                                                       isSameCountryChoose = YES;
                                                                       self.packageObject.to_Address.countryId = @"";
                                                                   }
                                                               }
                                                               
                                                               if(!isSameCountryChoose){
                                                                   
                                                                   if ([[responData objectForKey:@"cityFound"] isEqualToString:@"Yes"]) {
                                                                       address.cityId = [responData objectForKey:@"citId"];
                                                                       address.cityName = [responData objectForKey:@"cityName"];
                                                                       address.cityCode = [responData objectForKey:@"cityCode"];
                                                                       cityEditable = NO;
                                                                       
                                                                       if(tag == PACKAGE_FROM){
                                                                           
                                                                           fromSelectCityViewHeightConstraint.constant = 0;
                                                                           viewFromCity.superview.hidden = YES;
                                                                           fromSelectCityViewBottomConstraint.constant = 0;
                                                                           
                                                                       }else if(tag == PACKAGE_TO){
                                                                           
                                                                           toSelectCityViewHeightConstraint.constant = 0;
                                                                           viewToCity.superview.hidden = YES;
                                                                           toSelectCityViewBottomConstraint.constant = 0;
                                                                           
                                                                           
                                                                       }
                                                                       
                                                                   }else{
                                                                       address.cityId = Empty;
                                                                       address.cityName = Empty;
                                                                       address.cityCode = Empty;
                                                                       
                                                                       if(tag == PACKAGE_FROM){
                                                                           
                                                                           fromSelectCityViewHeightConstraint.constant = 80;
                                                                           viewFromCity.superview.hidden = NO;
                                                                           fromSelectCityViewBottomConstraint.constant = 15;
                                                                           _arrFromCityList = [responData objectForKey:@"cityList"];
                                                                       }else if(tag == PACKAGE_TO){
                                                                           
                                                                           toSelectCityViewHeightConstraint.constant = 80;
                                                                           viewToCity.superview.hidden = NO;
                                                                           toSelectCityViewBottomConstraint.constant = 15;
                                                                           _arrToCityList = [responData objectForKey:@"cityList"];
                                                                       }
                                                                   }
                                                                   
                                                                   [self setAddress:address forTag:tag andCityEditable:cityEditable];
                                                               }
                                                           }
                                                           else
                                                           {
                                                               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                           }
                                                       }
                                                       else
                                                       {
                                                           [Helper showAlertWithTitle:Empty Message:[responseObject valueForKey:@"MESSAGE"]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       NSLog(@"failure response ===   %@",responseObject);
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                       //Web request call failed//Web service failed to retrieve data
                                                   }];
}
- (void)setAddress:(AddressClass *)address forTag:(TAG_VALUE)tag andCityEditable:(BOOL)editable{
    
    if (tag == PACKAGE_FROM) {
        self.packageObject.from_Address = address;
        
        lblFromAddressLocation.text = address.Address;
        fromSelectCityValueTextfield.userInteractionEnabled = editable;
        
        fromSelectCityValueTextfield.text = address.cityName;
        self.packageObject.from_Address.cityName = address.cityName;
        self.packageObject.from_Address.cityId = address.cityId;
        self.packageObject.from_Address.cityCode = address.cityCode;
        
        
    }
    if (tag == PACKAGE_TO) {
        
        self.packageObject.to_Address = address;
        
        lblToAddressLocation.text = address.Address;
        toSelectCityValueTextfield.text = address.cityName;
        toSelectCityValueTextfield.userInteractionEnabled = editable;
        
        self.packageObject.to_Address.cityName = address.cityName;
        self.packageObject.to_Address.cityId = address.cityId;
        self.packageObject.to_Address.cityCode = address.cityCode;
    }
}

- (void)pickerDoneTapped:(UIButton*)sender{
    UIPickerView *piker;
   

    if (sender.tag == DEPRT_CITY) {
        piker = (UIPickerView*)fromSelectCityValueTextfield.inputView;
        
        if([piker selectedRowInComponent:0] != 0)
        {
        fromSelectCityValueTextfield.text = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityName"];
        
        self.packageObject.from_Address.cityName = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityName"];
        self.packageObject.from_Address.cityId = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityId"];
        self.packageObject.from_Address.cityCode = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityCode"];
        }
    }
    
    if (sender.tag == DEST_CITY) {
        piker = (UIPickerView*)toSelectCityValueTextfield.inputView;
        if([piker selectedRowInComponent:0] != 0)
        {
        toSelectCityValueTextfield.text = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityName"];
        
        self.packageObject.to_Address.cityName = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityName"];
        self.packageObject.to_Address.cityId = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityId"];
        self.packageObject.to_Address.cityCode = [[_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]-1] objectForKey:@"cityCode"];
        }
    }
    if (sender.tag == PACKAGE_TYPE) {
        piker = (UIPickerView*)txtDocument.inputView;
        txtDocument.text = [GlobalInfo TypeOfPackage:[piker selectedRowInComponent:0]];
        self.packageObject.pack_Type = [NSString stringWithFormat:@"%ld",[piker selectedRowInComponent:0]];
    }
    
    if (sender.tag == PACKAGE_DATE_TIME) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        UIDatePicker *dtPic = (UIDatePicker*)deliveryDateTimeValueTextfield.inputView;
        deliveryDateTimeValueTextfield.text = [dateFormatter stringFromDate:[dtPic date]];
        [self.packageObject setPack_Delivery_Date_Time:deliveryDateTimeValueTextfield.text];
        [deliveryDateTimeValueTextfield resignFirstResponder];
    }
    
    
    [self.view endEditing:YES];
}
#pragma mark --- PickerView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == PACKAGE_TYPE) return 3;
    
    return _arrPickerData.count + 1;
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == PACKAGE_TYPE) {
        return [GlobalInfo TypeOfPackage:row];
    }else{
        
        if (row == 0)
        {
            return NSLocalizedString(@"Choose", nil);
        }
        return [[_arrPickerData objectAtIndex:row-1] objectForKey:@"cityName"];
    }
    return Empty;
}

#pragma mark --- TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    UIPickerView *piker = (UIPickerView*)textField.inputView;

    if ([textField isEqual:fromSelectCityValueTextfield]) {
        if ([self.packageObject.from_Address.countryId isEqualToString:Empty]) {
            return NO;
        }
        _arrPickerData = _arrFromCityList;
    }
    if ([textField isEqual:toSelectCityValueTextfield]) {
        if ([self.packageObject.to_Address.countryId isEqualToString:Empty]) {
            return NO;
        }
        _arrPickerData = _arrToCityList;
    }
    [piker reloadAllComponents];
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark --- Package Image upload/cancel


- (IBAction)cancelImageTapped:(UIButton*)sender{
    
    [_arrMediaObjects removeObjectAtIndex:sender.tag-1];
    [self setImagesToItem];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = info[UIImagePickerControllerEditedImage];
    ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
    
    //Or you can get the image url from AssetsLibrary
    //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@"Path: %@",path);
    
    //=========get image type and compress========
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
   // NSString *strImageType = Empty;
    
    if (UTTypeConformsTo(imageUTI, kUTTypeJPEG))
    {
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
        CFRelease(imageUTI);
    }
    else if (UTTypeConformsTo(imageUTI, kUTTypePNG))
    {
        // Handle PNG
        imageObj.mimeType = @"png";
        imageObj.imgData = UIImagePNGRepresentation(image);
        CFRelease(imageUTI);
    }
    else
    {
        NSLog(@"Unhandled Image UTI: %@", imageUTI);
        
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
    }
    
    //_imageData = [_imageData base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //CFRelease(imageUTI);
    //===============================
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
//        [mediaData setObject:_imageData forKey:@"MediaData"];
//        [mediaData setObject:@"File" forKey:@"TagName"];
//        [imageObj setObject:@"image" forKey:@"MediaType"];
        imageObj.mediaType = @"image";
        [_arrMediaObjects addObject:imageObj];
        
        [self setImagesToItem];
    }];
}
- (void)setImagesToItem
{
    itemPicOneImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    itemPicSecondImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    itemPicThirdImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    
    itemPicOneCancelButton.hidden = YES;
    itemPicSecondCancelButton.hidden = YES;
    itemPicThirdCancelButton.hidden = YES;
    
    
    int counter = 1;
    for (ImageObjectClass *img in _arrMediaObjects) {
        if (counter == 1) {
            itemPicOneImgView.image = [UIImage imageWithData:img.imgData];
            itemPicOneCancelButton.hidden = NO;
        }
        if (counter == 2) {
            itemPicSecondImgView.image = [UIImage imageWithData:img.imgData];
            itemPicSecondCancelButton.hidden = NO;
        }
        if (counter == 3) {
            itemPicThirdImgView.image = [UIImage imageWithData:img.imgData];
            itemPicThirdCancelButton.hidden = NO;
        }
        
        counter++;
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
   // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

#pragma mark -- Add Package step two delegate
- (void)passPackageObject:(PackageClass *)package{
    self.packageObject = package;
}





@end


//============================= Old  & Already reuse code =============================
/* Viewdidload
 NSMutableAttributedString *wordString = [[NSMutableAttributedString alloc] initWithAttributedString:_lblTermsCondition.attributedText];
 [wordString beginEditing];
 [wordString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:NSMakeRange(54, 20)];
 [wordString endEditing];
 _lblTermsCondition.attributedText = wordString;
 
 
 // Do any additional setup after loading the view from its nib.
 
 [Helper addShadowToLayer:_headerView.layer];
 
 [Helper addBorderToLayer:_btnFromLocation.layer];
 ROUNDEDCORNER(_btnFromLocation.layer);
 
 [Helper addBorderToLayer:_btnToLocation.layer];
 ROUNDEDCORNER(_btnToLocation.layer);
 
 [Helper makeItRound:_circularView.layer withRadius:7.0];
 
 UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
 [self.view addGestureRecognizer:tap];

 
 //    [_txtDocumentType setInputView:[Helper pickerWithTag:PACKAGE_TYPE forControler:self]];
 //    [_txtDocumentType setInputAccessoryView:[Helper toolbarWithTag:PACKAGE_TYPE forControler:self]];
 //
 
 
 //    _btnCancelItem1.hidden = YES;
 //    _btnCancelItem2.hidden = YES;
 //    _btnCancelItem3.hidden = YES;
 //    _btnCancelItem4.hidden = YES;
 
 */


/*- (IBAction)pickLocationTapped:(UIButton*)sender {
 SingleLocationMapController *mapObj = [[SingleLocationMapController alloc] initWithNibName:@"SingleLocationMapController" bundle:nil];
 mapObj.delegate = self;
 
 if ([sender isEqual:_btnFromLocation]) {
 mapObj.tag = PACKAGE_FROM;
 mapObj.prevlat = self.packageObject.from_Address.lat_Value;
 mapObj.prevLong = self.packageObject.from_Address.long_Value;
 }else{
 mapObj.tag = PACKAGE_TO;
 mapObj.prevlat = self.packageObject.to_Address.lat_Value;
 mapObj.prevLong = self.packageObject.to_Address.long_Value;
 }
 
 [self.navigationController pushViewController:mapObj animated:YES];
 } */


/*//    if ([Helper checkEmptyField:_txtHeight.text]) {
 //        [Helper showAlertWithTitle:Empty Message:strKey_packageDimensionsRequired];
 //        return NO;
 //    }
 //    if ([Helper checkEmptyField:_txtWidth.text]) {
 //        [Helper showAlertWithTitle:Empty Message:strKey_packageDimensionsRequired];
 //        return NO;
 //    }
 //    if ([Helper checkEmptyField:_txtLength.text]) {
 //        [Helper showAlertWithTitle:Empty Message:strKey_packageDimensionsRequired];
 //        return NO;
 //    }
 //    if ([Helper checkEmptyField:_txtWeight.text]) {
 //        [Helper showAlertWithTitle:Empty Message:strKey_packageWeightRequired];
 //        return NO;
 //    } */


/*- (IBAction)nextTapped:(id)sender {
 
 if ([self validatePackage]) {
 self.packageObject.arrItemImages = self.arrMediaObjects;
 self.packageObject.pack_Height = _txtHeight.text;
 self.packageObject.pack_Width = _txtWidth.text;
 self.packageObject.pack_Length = _txtLength.text;
 self.packageObject.pack_Weight = [NSString stringWithFormat:@"%ld",_wtCounter];
 
 AddShipmentsStepTwoController *nxtStep = [[AddShipmentsStepTwoController alloc] initWithNibName:@"AddShipmentsStepTwoController" bundle:nil];
 nxtStep.packageObject = self.packageObject;
 nxtStep.arrCountry = _arrCountryList;
 [self.navigationController pushViewController:nxtStep animated:YES];
 
 }
 } */


/*- (IBAction)TermsConditionTapped:(UIButton *)sender {
 if (sender.tag == 1) {
 //checkBox
 UIImage *imgSel = [UIImage imageNamed:@"check_box_select"];
 UIImage *imgDeSel = [UIImage imageNamed:@"check_box_deselect"];
 
 _imgCheckTerms.image = [_imgCheckTerms.image isEqual:imgSel]?imgDeSel:imgSel;
 }else{
 //show terms condition
 }
 }
 */

/* //@property (weak, nonatomic) IBOutlet UIView *headerView;
 //@property (weak, nonatomic) IBOutlet UIView *circularView;
 //
 //@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
 //@property (weak, nonatomic) IBOutlet UITextField *txtWidth;
 //@property (weak, nonatomic) IBOutlet UITextField *txtLength;
 
 //@property (weak, nonatomic) IBOutlet UITextField *txtDocumentType;
 //@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
 
 
 //@property (weak, nonatomic) IBOutlet UIButton *btnFromLocation;
 
 //@property (weak, nonatomic) IBOutlet UILabel *lblPackageFrom;
 //@property (weak, nonatomic) IBOutlet UITextField *txtFromCity;
 
 //@property (weak, nonatomic) IBOutlet UILabel *lblPackageTo;
 //@property (weak, nonatomic) IBOutlet UITextField *txtToCity;
 //
 //@property (weak, nonatomic) IBOutlet UIImageView *picItem1;
 //@property (weak, nonatomic) IBOutlet UIButton *btnCancelItem1;
 //
 //@property (weak, nonatomic) IBOutlet UIImageView *picItem2;
 //@property (weak, nonatomic) IBOutlet UIButton *btnCancelItem2;
 //
 //@property (weak, nonatomic) IBOutlet UIImageView *picItem3;
 //@property (weak, nonatomic) IBOutlet UIButton *btnCancelItem3;
 //
 //@property (weak, nonatomic) IBOutlet UIImageView *picItem4;
 //@property (weak, nonatomic) IBOutlet UIButton *btnCancelItem4;
 
 
 
 
 //@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityFromHeightConstraint;
 //
 //@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityToheightConstraint;
 //
 //@property (weak, nonatomic) IBOutlet UIButton *btnToLocation;
 //
 //
 //@property (weak, nonatomic) IBOutlet UIImageView *imgCheckTerms;
 //@property (weak, nonatomic) IBOutlet UILabel *lblTermsCondition;
*/


/* - (IBAction)backTapped:(id)sender{
 MyShipmentsListController *managObj = [[MyShipmentsListController alloc] initWithNibName:@"MyShipmentsListController" bundle:nil];
 AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
 }
 
 - (IBAction)modeOfTravelTapped:(UIButton *)sender {
 for (UIButton *cutB in [sender.superview subviews]) {
 if ([cutB isKindOfClass:[UIButton class]]) {
 cutB.selected = NO;
 }
 }
 [sender setSelected:YES];
 self.packageObject.travel_Mode = sender.tag;
 }
 
 - (void)hideKeyBoard{
 [self.view endEditing:YES];
 }
*/

/* - (void)showViewNamed:(UIView*)vie withTopName:(NSString*)topName{
 if (![topName isEqualToString:Empty]) {
 for (NSLayoutConstraint *cons in vie.superview.constraints) {
 if ([cons.identifier isEqualToString:topName]) {
 cons.constant = 8;
 }
 }
 }
 [vie addConstraint:[NSLayoutConstraint constraintWithItem:vie attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:40]];
 vie.hidden = NO;
 }
 
 #pragma mark - UitextField Delegate
 
 
 
 
 //- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
 //    if ([textField isEqual:_txtLength] || [textField isEqual:_txtWidth] || [textField isEqual:_txtHeight]) {
 //        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
 //        NSInteger length = [currentString length];
 //        if (length > 3) {
 //            return NO;
 //        }
 //    }
 //    return YES;
 //}
 //
 //- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
 //    if ([textField isEqual:_txtLength] || [textField isEqual:_txtWidth] || [textField isEqual:_txtHeight]) {
 //        if (textField.text.length == 0) {
 //            return NO;
 //        }
 //    }
 //    return YES;
 //}
 
 
*/


/*#pragma mark --- Fetch Country City
 
 - (void)callServiceToLoadCountryCityWithCountryId:(NSString*)countryId tagValue:(TAG_VALUE)tag{
 NSString *serviceString = kCountryList;
 NSDictionary *param = @{@"caller":@"default"};
 if (countryId){
 serviceString = kCityList;
 param = @{@"countryId":countryId};
 }
 
 //=======Call WebService Engine========
 [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:serviceString
 Method:REQMETHOD_GET
 parameters:param
 media:nil
 viewController:self
 success:^(id responseObject){
 
 NSLog(@"response ===   %@",responseObject);
 if ([GlobalInfo checkStatusSuccess:responseObject]) {
 _arrPickerData = [responseObject objectForKey:@"responseData"];
 if (tag == TAG_VALUE_NONE){
 _arrCountryList  = _arrPickerData;
 }
 else {
 UIPickerView *piker;
 
 if (tag == DEPRT_CITY) {
 piker = (UIPickerView*)fromSelectCityValueTextfield.inputView;
 }
 
 if (tag == DEST_CITY) {
 piker = (UIPickerView*)toSelectCityValueTextfield.inputView;
 }
 [piker reloadAllComponents];
 }
 }
 }
 failure:^(id responseObject){
 NSLog(@"failure response ===   %@",responseObject);
 [Helper showAlertWithTitle:Empty Message:strKey_error_wentwrong];
 
 }];
 }
 */
