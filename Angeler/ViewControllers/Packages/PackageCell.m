//
//  PackageCell.m
//  Angeler
//
//  Created by AJAY on 04/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "PackageCell.h"

@implementation PackageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
    //[Helper addBorderToLayer:_menu_Container.layer];
    
    [Helper addBorderToLayer:_btnDelete.layer];
    [Helper addBorderToLayer:_btnRequestList.layer];
   // [Helper addBorderToLayer:_btnShipmentDetails.layer];
    
    [self.menu_Container dropShadowWithColor:[UIColor lightishGrayColor]];
    
//    [self.btnShipmentDetails setTitle:[NSString stringWithFormat:@"    %@",
//                                       [NSLocalizedString(@"Shipment_Details﻿", nil) capitalizedString] ]forState: UIControlStateNormal];
    [self.btnDelete setTitle:[NSString stringWithFormat:@"    %@",
                              NSLocalizedString(@"Delete", nil)]  forState: UIControlStateNormal];
    [self.btnRequestList setTitle:[NSString stringWithFormat:@"    %@",
                              NSLocalizedString(@"History", nil)]  forState: UIControlStateNormal];
    
}

- (IBAction)btnMenuTapped:(id)sender {
    self.menu_Container.hidden = !self.menu_Container.hidden;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
