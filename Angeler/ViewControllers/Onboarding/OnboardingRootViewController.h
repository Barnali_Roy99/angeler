//
//  OnboardingRootViewController.h
//  Angeler
//
//  Created by AJAY on 07/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN


@interface OnboardingRootViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>

@property (nonatomic,strong) UIPageViewController *PageViewController;

@property  NSUInteger pageIndex;
@property  NSUInteger numberOfOnboardingSlides;
@property (weak, nonatomic) IBOutlet UIView *nextView;
@property (weak, nonatomic) IBOutlet UIView *startView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *lblNext;
@property (weak, nonatomic) IBOutlet UILabel *lblStart;

- (UIViewController*)viewControllerAtIndex:(NSUInteger)index;



@end

NS_ASSUME_NONNULL_END
