//
//  OnboardingPageContentViewControllerThird.h
//  Angeler
//
//  Created by AJAY on 07/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OnboardingPageContentViewControllerThird : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *typeOfTransportCollectionView;


@end

NS_ASSUME_NONNULL_END
