//
//  OnboardingPageContentViewControllerFirst.m
//  Angeler
//
//  Created by AJAY on 07/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "OnboardingPageContentViewControllerFirst.h"

@interface OnboardingPageContentViewControllerFirst ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblBottomDesc;

@end

@implementation OnboardingPageContentViewControllerFirst

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLocalization];
    // Do any additional setup after loading the view.
}

-(void)setLocalization
{
    self.lblTitle.text = NSLocalizedString(@"Become_An_Angeler", nil);
    self.lblDesc.text = NSLocalizedString(@"Add_Your_Trip_And_Get_Paid_By_Participating_In_The_Delivery", nil);
    self.lblBottomDesc.text = NSLocalizedString(@"All_Our_Services_Are_Aimed_At_Professionals_And_Individuals", nil);
}



@end
