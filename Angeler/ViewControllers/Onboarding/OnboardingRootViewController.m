//
//  OnboardingRootViewController.m
//  Angeler
//
//  Created by AJAY on 07/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "OnboardingRootViewController.h"
#import "LoginViewController.h"

@interface OnboardingRootViewController ()

@end

@implementation OnboardingRootViewController
@synthesize PageViewController,pageIndex,numberOfOnboardingSlides,nextView;


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.navigationController.navigationBarHidden = YES;
    
    //First App install onboard show otherwise not
    
    [[GlobalInfo sharedInfo] setIsHideOnboarding:true];
    
    pageIndex = 1;
    numberOfOnboardingSlides = 3;
    
    [self showHideNextStart];
    [self setTapGesture];
    
    [self.startView makeCornerRound:25.0];
    
    // Create page view controller
    self.PageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.PageViewController.dataSource = self;
    self.PageViewController.delegate = self;
    
    UIViewController *startingViewController = [self viewControllerAtIndex:1];
    
    
    NSArray *viewControllers = @[startingViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.PageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 100);
    
    [self addChildViewController:PageViewController];
    [self.view addSubview:PageViewController.view];
    [self.PageViewController didMoveToParentViewController:self];
    [self setLocalization];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    //Set pagecontrol
    [self.pageControl customPageControlWithFillColor:[UIColor whiteColor] borderColor:[UIColor whiteColor] borderWidth:1.0];
    self.pageControl.transform = CGAffineTransformMakeScale(2.0, 2.0);
    
}

-(void)setLocalization
{
    self.lblStart.text = NSLocalizedString(@"Start", nil);
    self.lblNext.text = NSLocalizedString(@"Next", nil);
   
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Tapgesture

- (void)setTapGesture
{
    UITapGestureRecognizer *nextTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextViewTapped:)];
    [self.nextView addGestureRecognizer:nextTapRecognizer];
    
    
    UITapGestureRecognizer *startTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startViewTapped:)];
    [self.startView addGestureRecognizer:startTapRecognizer];
    
}

- (void)nextViewTapped:(UITapGestureRecognizer*)sender
{
    if (pageIndex<numberOfOnboardingSlides)
    {
        pageIndex++;
        [self showHideNextStart];
        UIViewController *pageContentViewController = [self viewControllerAtIndex:pageIndex];
        [PageViewController setViewControllers:@[pageContentViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
}


- (void)startViewTapped:(UITapGestureRecognizer*)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *logInVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVCID"];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:logInVC];
    
}

- (void)showHideNextStart
{
    if(pageIndex == numberOfOnboardingSlides)
    {
        [self.startView setHidden:NO];
        [self.nextView setHidden:YES];
    }
    else
    {
        [self.startView setHidden:YES];
        [self.nextView setHidden:NO];
    }
    
    [self.pageControl setCurrentPage:pageIndex-1];
    [self.pageControl customPageControlWithFillColor:[UIColor whiteColor] borderColor:[UIColor whiteColor] borderWidth:1.0];
}


#pragma mark - Page View Datasource Methods
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
   
    if (pageIndex <= 1)
    {
        return nil;
    }
    
    pageIndex--;
    
   
    return [self viewControllerAtIndex:pageIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
    if (pageIndex>=numberOfOnboardingSlides)
    {
        return nil;
    }
    pageIndex++;
    
    
    return [self viewControllerAtIndex:pageIndex];
}
#pragma mark - Other Methods

- (UIViewController*)viewControllerAtIndex:(NSUInteger)index
{
    
    // Create a new view controller and pass suitable data.
    NSString *identifierName = [NSString stringWithFormat:@"PageContentViewController%lu",(unsigned long)index];
    UIViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifierName];
    
    return pageContentViewController;
}



-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed

{
    if (!completed)
    {
        return;
    }
    pageIndex = pageViewController.viewControllers.firstObject.view.tag;
   [self showHideNextStart];
}

#pragma mark - No of Pages Methods

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return numberOfOnboardingSlides;
}



@end
