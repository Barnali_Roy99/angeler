//
//  OnboardingPageContentViewControllerThird.m
//  Angeler
//
//  Created by AJAY on 07/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "OnboardingPageContentViewControllerThird.h"
#import "TypeOfTransportCollectionViewCell.h"

static NSString *typeOfTransportIdentifier = @"TypeOfTransportCollectionViewCellID";

@interface OnboardingPageContentViewControllerThird ()
{
    NSArray *typeTransportImgArr;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblBottomDesc;


@end

@implementation OnboardingPageContentViewControllerThird
@synthesize typeOfTransportCollectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    typeTransportImgArr = [[NSArray alloc] initWithObjects:@"OnboardingAeroplane",@"OnboardingCar",@"OnboardingCruize",@"OnboardingBus",@"OnboardingWalk", @"OnboardingShip",@"OnboardingCycle",@"OnboardingByke",nil];
     [self.typeOfTransportCollectionView registerNib:[UINib nibWithNibName:@"TypeOfTransportCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:typeOfTransportIdentifier];
    
    [self setLocalization];
    
    // Do any additional setup after loading the view.
}


-(void)setLocalization
{
    self.lblTitle.text = NSLocalizedString(@"Any_Type_Of_Transport", nil);
    self.lblDesc.text = NSLocalizedString(@"Everyone_Gains_Join_Our_Global_Mobility_Network", nil);
    self.lblBottomDesc.text = NSLocalizedString(@"All_Our_Services_Are_Aimed_At_Professionals_And_Individuals", nil);
}



// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TypeOfTransportCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:typeOfTransportIdentifier forIndexPath:indexPath];
    cell.typeOfTransportImgView.image = [UIImage imageNamed:[typeTransportImgArr objectAtIndex:indexPath.row]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat collectionViewWidth = collectionView.frame.size.width / typeTransportImgArr.count;
    CGFloat collectionViewHeight = collectionView.frame.size.height;
    
    NSLog(@"collectionViewWidth %f collectionViewHeight %f",collectionViewWidth,collectionViewHeight);
    return CGSizeMake(collectionViewWidth, collectionViewHeight);
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return typeTransportImgArr.count;
}




@end
