//
//  OnboardingPageContentViewControllerSecond.m
//  Angeler
//
//  Created by AJAY on 07/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "OnboardingPageContentViewControllerSecond.h"

@interface OnboardingPageContentViewControllerSecond ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblBottomDesc;

@end

@implementation OnboardingPageContentViewControllerSecond

- (void)viewDidLoad {
    [super viewDidLoad];
        [self setLocalization];
    // Do any additional setup after loading the view.
}

-(void)setLocalization
{
    self.lblTitle.text = NSLocalizedString(@"Easier_And_More_Ecological_Delivery", nil);
    self.lblDesc.text = NSLocalizedString(@"Send_All_your_Shipment_Faster_And_Cheaper_By_Using_Existing_Trips", nil);
    self.lblBottomDesc.text = NSLocalizedString(@"All_Our_Services_Are_Aimed_At_Professionals_And_Individuals", nil);
}



@end
