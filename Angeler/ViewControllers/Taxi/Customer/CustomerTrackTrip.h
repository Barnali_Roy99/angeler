//
//  CustomerTrackTrip.h
//  Angeler
//
//  Created by AJAY on 14/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickUpDropOffAddressModel.h"
#import "Angeler-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomerTrackTrip : UIViewController
@property (nonatomic, strong)  NSString *bookingID;


@end

NS_ASSUME_NONNULL_END
