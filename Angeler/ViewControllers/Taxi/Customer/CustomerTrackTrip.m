//
//  CustomerTrackTrip.m
//  Angeler
//
//  Created by AJAY on 14/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "CustomerTrackTrip.h"
#import "RideCancelReasonVC.h"
#import "SharePopUpViewController.h"

@interface CustomerTrackTrip ()<ARCarMovementDelegate,topNavActionDelegate>
{
    GMSMarker *driverCarMarker,*customerPickUpMarker,*dropOffMarker;
    NSMutableArray *markerArr,*oldPolylines;
    GMSPolyline *mainPolyline;
    NSDictionary *rideDetailsDic;
    BOOL isContinousLocationTrackStart,isFirstTimeLoad;
    CLLocationCoordinate2D prevCoOrdinate;
    NSTimer *driverContinuousMoveCarTimer;
    NSNumber *dropOffLat,*dropOffLong;
   
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *lblFromTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblToTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFromLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblToLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverDetailsViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *btnRideCancel;

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet RoundImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *driverCarNumber;
@property (weak, nonatomic) IBOutlet UILabel *driverCarName;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UILabel *driverNumberOfTrips;

@property (strong, nonatomic) ARCarMovement *moveMent;
@property (nonatomic, strong)  PickUpDropOffAddressModel *addressObj;
@property (nonatomic, strong)  NSString *driverCurrentStatus;


@end

@implementation CustomerTrackTrip

- (void)viewDidLoad {
    
    //Socket open
    
    if(![[SocketIOManager swiftSharedInstance] isSocketConnected]){
        [[SocketIOManager swiftSharedInstance] establishConnectionWithSessionID:[[GlobalInfo sharedInfo] sessionId] userID:[[GlobalInfo sharedInfo] userId]];
    }
    
    
    [super viewDidLoad];
    NSLog(@"viewDidLoad Called");
    [_btnRideCancel setHidden:true];
    isFirstTimeLoad = true;
    self.addressObj = [[PickUpDropOffAddressModel alloc] init];
    self.moveMent = [[ARCarMovement alloc]init];
    self.moveMent.delegate = self;
    
    [Helper showLocationServiceGoToSettingsAlert];
    isContinousLocationTrackStart = false;
    oldPolylines = [[NSMutableArray alloc]init];
    markerArr = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(riderStatusNotificationAction:) name:TaxiPostNotificationCustomerTrackTripRiderStatus object:nil];
    
    //socket
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(getDriverCurrentLocation:) name:@"CustomerTrackTripRiderGetDriverCurrentLocation" object:nil];
    
    [self initialSetUp];
    [self setLocalization];
    
    
    
    
    [[SocketIOManager swiftSharedInstance] getDriverLocationDetailsViaSocket];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    NSLog(@"viewDidAppear Customer track Trip Called");
     if (_bookingID != nil) {
       [self callGetRiderDetailsWithIsFromAutoDirectNotification:false];
     }
  
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
   // [self stopTimer];
}


#pragma mark - Notification
-(void)riderStatusNotificationAction:(NSNotification *) notification
{
    NSDictionary *dict = notification.userInfo;
    _bookingID = [dict valueForKey:@"bookingId"];
    if (_bookingID != nil) {
       [self callGetRiderDetailsWithIsFromAutoDirectNotification:true];
    }
}

-(void)getDriverCurrentLocation:(NSNotification *) notification
{
    
     NSDictionary *dict = notification.userInfo;
     NSLog(@"Socket.io getDriverCurrentLocation %@" ,dict);
    
    [self updateDriverCarLocation:dict];
   
}
#pragma mark - Continiuous Location Track

- (void)methodToGetDriverCurrentLocation
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue,^{
        
        [self callServiceToGetDriverUpdatedLocation];
        
    });
    
    
}


- (void)updateDriverCarLocation:(NSDictionary*)driverLocationDetailsDic{
    if(isContinousLocationTrackStart)
    {
        NSLog(@"Location change Continuous tracking called");
        CLLocationCoordinate2D newCoordinate;
        
        
        
        if([self isDriverJustAcceptedRequest] || [self isDriverReachedBeforeStart])
        {
            prevCoOrdinate  = CLLocationCoordinate2DMake([_addressObj.dropOffLatitude doubleValue],[_addressObj.dropOffLongitude doubleValue]);
            _addressObj.dropOffLatitude = @([[driverLocationDetailsDic objectForKey:@"driverLastLocationLatitude"] doubleValue]);
            _addressObj.dropOffLongitude = @([[driverLocationDetailsDic objectForKey:@"driverLastLocationLongitude"] doubleValue]);
            
            if(![self isDriverReachedBeforeStart])
            {
                [self drawRoute];
            }
            
            newCoordinate = CLLocationCoordinate2DMake([_addressObj.dropOffLatitude doubleValue],[_addressObj.dropOffLongitude doubleValue]);
            
        }
        else
        {
            prevCoOrdinate  = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
            _addressObj.pickUpLatitude =  @([[driverLocationDetailsDic objectForKey:@"driverLastLocationLatitude"] doubleValue]);;
            _addressObj.pickUpLongitude = @([[driverLocationDetailsDic objectForKey:@"driverLastLocationLongitude"] doubleValue]);
            
            newCoordinate = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
        }
        
        
        
        /**
         *  You need to pass the created/updating marker, old & new coordinate, mapView and bearing value from backend
         *  to turn properly. Here coordinates json files is used without new bearing value. So that
         *  bearing won't work as expected.
         */
        [self.moveMent arCarMovementWithMarker:driverCarMarker oldCoordinate:prevCoOrdinate newCoordinate:newCoordinate mapView:self.mapView bearing:0];  //instead value 0, pass latest bearing value from backend
        
        prevCoOrdinate = newCoordinate;
        
    }
}


#pragma mark - ARCarMovementDelegate
- (void)arCarMovementMoved:(GMSMarker * _Nonnull)Marker {
    driverCarMarker = Marker;
    driverCarMarker.map = self.mapView;
    
    //animation to make car icon in center of the mapview
    //
    GMSCameraUpdate *updatedCamera = [Helper focusMapToShowAllMarkers:markerArr];
  
//    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:pickUpMarker.position zoom:15.0f];
    [self.mapView animateWithCameraUpdate:updatedCamera];
}


#pragma mark- API Call

- (void)callServiceToGetDriverUpdatedLocation{

    NSDictionary *param = @{@"bookingId":_bookingID,@"HideHUD":@"YES"
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kCustomerGetDriverLocationDetails
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:nil
                                                   success:^(id responseObject){
                                                       NSLog(@"response kCustomerGetDriverLocationDetails ===   %@",responseObject);
                                                      NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       if (statusCode == 200){
                                                           [self updateDriverCarLocation:[responseObject objectForKey:@"responseData"]];
                                                       }
                                                    
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       
                                                   }];
}


-(void)callGetRiderDetailsWithIsFromAutoDirectNotification:(BOOL)isFromScreenAlreadyLoadedNotificationArise{
    
    NSDictionary *param = @{@"bookingId":_bookingID
                            };
    
    NSLog(@"bookingId Customer RideDetails %@",param);
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kCustomerGetRideDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       
                                                      // if ([GlobalInfo checkStatusSuccess:responseObject])
                                                       if (statusCode == 200)
                                                       {
                                                           
                                                           rideDetailsDic = [responseObject objectForKey:@"responseData"];
                                                           NSLog(@"rideDetailsDic Driver kCustomerGetRideDetails %@",rideDetailsDic);
                                                       
                                                           _driverCurrentStatus = [rideDetailsDic objectForKey:@"status"];
                                                           
                                                           if([self isDriverStatusExpired]){
                                                               [GlobalInfo showTaxiDashboardScreen];
                                                               [Helper showAlertWithTitle:NSLocalizedString(@"Ride_Expired_Alert",nil) Message:@""];
                                                           }
                                                          else if([self isDriverEndTrip])
                                                           {
                                                                [self showTripEndPopUp];
                                                            
                                                           }
                                                          else if([self isDriverRejectedTrip] || [self isDriverStatusPending] || [self isRiderCancelledTrip] ||  [self  isDriverCancelledTrip]){
                                                              if([self isRiderCancelledTrip]){
                                                                  [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Customer_Cancel_Ride_AlertMsg", nil)];
                                                              }
                                                              else if([self isDriverCancelledTrip]){
                                                                  [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Driver_Cancel_Ride_AlertMsg", nil)];
                                                              }
                                                              [GlobalInfo showTaxiDashboardScreen];
                                                              
                                                          }
                                                           else{
                                                            [self setRideDetatilsdata];
                                                           }
                                                           
                                                          
                                                       }
                                                       else  if (statusCode == 600) {
                                                           
                                                           [GlobalInfo showLoginScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       else if(statusCode == 250){
                                                           [GlobalInfo showTaxiDashboardScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       else
                                                       {
                                                           
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       
                                                       
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



#pragma mark- General Functions

-(void)initialSetUp
{
    [self setPickUpDropOffPlaceHolderText];
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
        _driverDetailsViewHeightConstraint.constant = 60;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        _driverDetailsViewHeightConstraint.constant = 40;
    }
     _topNavbarView.delegate = self;
    [self.mapView addObserver:self
                   forKeyPath:@"selectedMarker"
                      options:(NSKeyValueObservingOptionNew |
                               NSKeyValueObservingOptionOld)
                      context:NULL];
    
    [_topNavbarView showNavbackBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Ride_Details", nil) capitalizedString]];
    _driverImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    _driverImageView.layer.borderWidth = 1.0;
    
}

-(void)setLocalization
{
    _lblFromTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"From", nil)];
    _lblToTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"To", nil)];
  
}

-(void)setPickUpDropOffPlaceHolderText
{
    _lblToLocation.text = NSLocalizedString(@"Search_Destination", nil);
    [_lblToLocation setTextColor:[UIColor placeholderTextColor]];
    
    _lblFromLocation.text = NSLocalizedString(@"Search_Pickup_Location", nil);
    [_lblFromLocation setTextColor:[UIColor placeholderTextColor]];
}

-(void)setRideDetatilsdata
{
    _addressObj.pickUpAddress = [rideDetailsDic objectForKey:@"pickupAddress"];
    _addressObj.dropOffAddress = [rideDetailsDic objectForKey:@"dropoffAddress"];
    _addressObj.pickUpLatitude = @([[rideDetailsDic objectForKey:@"pickupLatitude"] doubleValue]);
    _addressObj.pickUpLongitude = @([[rideDetailsDic objectForKey:@"pickupLongitude"] doubleValue]);
    
    _addressObj.dropOffLatitude = @([[rideDetailsDic objectForKey:@"dropoffLatitude"] doubleValue]);
    _addressObj.dropOffLongitude = @([[rideDetailsDic objectForKey:@"dropoffLongitude"] doubleValue]);
    
//    if(isFirstTimeLoad){
//        [self initialMapSetUp];
//        isFirstTimeLoad = false;
//    }
    [self setInitialData];
    [self setScreenShowWithRespectToDriverStatus];
    
}

-(void)setInitialData{
    _lblFromLocation.text = _addressObj.pickUpAddress;
    _lblToLocation.text = _addressObj.dropOffAddress;
    
    [_lblToLocation setTextColor:[UIColor blackColor]];
    [_lblFromLocation setTextColor:[UIColor blackColor]];
  
    //Final destination Lat, Long save
    dropOffLat = _addressObj.dropOffLatitude;
    dropOffLong = _addressObj.dropOffLongitude;
    
    [_driverImageView sd_setImageWithURL:[NSURL URLWithString:[rideDetailsDic objectForKey:@"driverImageURL"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    _driverName.text = [rideDetailsDic objectForKey:@"driverName"];
    _driverCarName.text = [rideDetailsDic objectForKey:@"driverVehicleName"];
    _driverCarNumber.text = [rideDetailsDic objectForKey:@"driverVehicleNo"];
    _driverNumberOfTrips.text = [NSString stringWithFormat:NSLocalizedString(@"Driver_Trips_Completed", nil),[rideDetailsDic objectForKey:@"driverCompletedRideCount"]];
}

-(void)setScreenShowWithRespectToDriverStatus
{
  //  [self startTimer];
    
    if ([self isDriverReachedBeforeStart] || [self isDriverJustAcceptedRequest])
    {
        [self driverStatusReachedChanges];
    }
    else if ([self isDriverTripStarted])
    {
        [self driverStatusStartTripChanges];
    }
}

-(BOOL)isDriverReachedBeforeStart{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Reached])
    {
        return true;
    }
    return false;
}
-(BOOL)isDriverTripStarted{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Started])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverJustAcceptedRequest{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Accepted])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverEndTrip{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Completed])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverRejectedTrip{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Rejected])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverStatusPending{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Pending])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverStatusExpired{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Expired])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverCancelledTrip{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Cancelled])
    {
        return true;
    }
    return false;
}

-(BOOL)isRiderCancelledTrip{
    if ([_driverCurrentStatus isEqualToString:RiderStatus_Cancelled])
    {
        return true;
    }
    return false;
}


-(void)driverStatusReachedChanges
{
    [_btnRideCancel setHidden:false];
    _addressObj.dropOffLatitude = @([[rideDetailsDic objectForKey:@"driverLastLocationLatitude"] doubleValue]);
    _addressObj.dropOffLongitude = @([[rideDetailsDic objectForKey:@"driverLastLocationLongitude"] doubleValue]);
    
    NSLog(@"customerPickUpMarker driverStatusReachedChanges %@",customerPickUpMarker);
    NSLog(@"_mapView driverStatusReachedChanges %@",_mapView);
    if(markerArr.count > 0){
      [self clearAllAddedMarker];
        
    }
    
    [markerArr removeAllObjects];
    [self addMarkerToMapViewAtCameraPosition];
}

-(void)driverStatusStartTripChanges{
  
    [_btnRideCancel setHidden:true];
    //Now Driver car marker will pick up marker and main destination will be drop off marker
    _addressObj.dropOffLatitude = dropOffLat;
    _addressObj.dropOffLongitude = dropOffLong;
    
    _addressObj.pickUpLatitude = @([[rideDetailsDic objectForKey:@"driverLastLocationLatitude"] doubleValue]);
    _addressObj.pickUpLongitude = @([[rideDetailsDic objectForKey:@"driverLastLocationLongitude"] doubleValue]);
NSLog(@"_mapView driverStatusStartTripChanges %@",_mapView);
    if(markerArr.count > 0){
        [self clearAllAddedMarker];
    }
    
    [markerArr removeAllObjects];
    [self addMarkerToMapViewAtCameraPosition];
}

-(void)clearAllAddedMarker{
    
    dropOffMarker.map = nil;
    for(GMSMarker *marker in markerArr){
        if(marker != dropOffMarker){
            marker.map = nil;
        }
        
    }
    
}


#pragma mark- Timer Action

-(void)startTimer
{
    if(!driverContinuousMoveCarTimer)
    {
        driverContinuousMoveCarTimer =  [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(methodToGetDriverCurrentLocation) userInfo:nil repeats:YES];
    }
}

-(void)stopTimer
{
    if ([driverContinuousMoveCarTimer isValid]) {
        [driverContinuousMoveCarTimer invalidate];
    }
    driverContinuousMoveCarTimer = nil;
}

#pragma mark- Key value Observer

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([keyPath isEqual:@"selectedMarker"]) {
        if ([[change objectForKey:NSKeyValueChangeNewKey] isKindOfClass:[NSNull class]]) {
            if(customerPickUpMarker.map != nil || dropOffMarker.map != nil)
            _mapView.selectedMarker = [change objectForKey:NSKeyValueChangeOldKey];
        }
    }
}


#pragma mark- map Action

-(void)initialMapSetUp
{
    [CATransaction begin];
    [_mapView animateToZoom:20];
    
    [self.mapView animateToLocation:CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue], [_addressObj.pickUpLongitude doubleValue])];
    
    [CATransaction commit];
    
}

- (void)addMarkerToMapViewAtCameraPosition{
    
    //Driver Car Marker Set
    
    [self setDriverCarMarker];
    
    //Drop Off Marker Set
    
    if([self isDriverJustAcceptedRequest] || [self isDriverReachedBeforeStart])
    {
        customerPickUpMarker = [[GMSMarker alloc] init];
        CGRect frame1 = CGRectMake(0, 0, 23,23);
        UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:frame1];
        imageView1.image = [UIImage imageNamed:@"Location_Round_Blue_marker"];
        customerPickUpMarker.iconView = imageView1;
        customerPickUpMarker.appearAnimation = kGMSMarkerAnimationPop;
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        CLLocationCoordinate2D customerPickUpPosition  = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
        
        customerPickUpMarker.layer.latitude = customerPickUpPosition.latitude;
        customerPickUpMarker.layer.longitude = customerPickUpPosition.longitude;
        customerPickUpMarker.map = _mapView;
        if([self isDriverJustAcceptedRequest]){
             NSLog(@"customerPickUpMarker addMarkerToMapViewAtCameraPosition %@",customerPickUpMarker);
            [_mapView setSelectedMarker:customerPickUpMarker];
        }
        
        [markerArr addObject:customerPickUpMarker];
        [CATransaction commit];
        
       
     
    }
    
    dropOffMarker = [[GMSMarker alloc] init];
    CGRect frame = CGRectMake(0, 0, 23,33);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [UIImage imageNamed:@"Location_Red_marker"];
    dropOffMarker.iconView = imageView;
    dropOffMarker.appearAnimation = kGMSMarkerAnimationPop;
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    CLLocationCoordinate2D dropOffPosition  = CLLocationCoordinate2DMake([dropOffLat doubleValue],[dropOffLong doubleValue]);
    
    dropOffMarker.layer.latitude = dropOffPosition.latitude;
    dropOffMarker.layer.longitude = dropOffPosition.longitude;
    dropOffMarker.map = _mapView;
    
    if([self isDriverTripStarted] || [self isDriverReachedBeforeStart])
    {
        if([self isDriverTripStarted])
        {
             [_mapView setSelectedMarker:dropOffMarker];
        }
        [markerArr addObject:dropOffMarker];
       
    }
    
    [CATransaction commit];
    
    
    NSLog(@"_mapView addMarkerToMapViewAtCameraPosition %@",_mapView);
    
    isContinousLocationTrackStart = false;
    [self setMapWithinBound];
    [self drawRoute];
   
}


-(void)setMapWithinBound
{
    GMSCameraUpdate *update = [Helper focusMapToShowAllMarkers:markerArr];
    [_mapView animateWithCameraUpdate:update];
    
    NSLog(@"_mapView setMapWithinBound %@",_mapView);
    
}

-(void)setDriverCarMarker
{
    
    driverCarMarker = [[GMSMarker alloc] init];
    CGRect frame = CGRectMake(0, 0, 16,35);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [UIImage imageNamed:@"Car_icon"];
    driverCarMarker.iconView = imageView;
    // pickUpMarker.appearAnimation = kGMSMarkerAnimationPop;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    
    CLLocationCoordinate2D driverCarPosition;
    if([self isDriverTripStarted])
    {
        driverCarPosition = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
    }
    else
    {
        driverCarPosition = CLLocationCoordinate2DMake([_addressObj.dropOffLatitude doubleValue],[_addressObj.dropOffLongitude doubleValue]);
    }
    
    driverCarMarker.layer.latitude = driverCarPosition.latitude;
    driverCarMarker.layer.longitude = driverCarPosition.longitude;
    driverCarMarker.map = _mapView;
    
    [markerArr addObject:driverCarMarker];
    [CATransaction commit];
}
#pragma mark- Button Action

- (IBAction)onDriverCallBtnClicked:(id)sender {
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[rideDetailsDic objectForKey:@"driverPhoneNo"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (IBAction)onRideCancelBtnClicked:(id)sender {
    
    RideCancelReasonVC *Obj = [[RideCancelReasonVC alloc] initWithNibName:@"RideCancelReasonVC" bundle:nil];
    Obj.isDriverCancelRide = NO;
    Obj.bookingID = _bookingID;
    Obj.currentRideStatus = _driverCurrentStatus;
    self.definesPresentationContext = YES; //self is presenting view controller
    Obj.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    Obj.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:Obj animated:YES completion:nil];
}

#pragma mark- Draw Route

- (void)drawRoute
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue,^{
        [self callPolyLineGoogleMapServiceWithcompletionHandler:^(GMSPolyline *polyline) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // do work here
                if(polyline)
                {
                    [self clearAllPreviousPolyLines];
                    if(![self isDriverReachedBeforeStart])
                    {
                        [oldPolylines addObject:polyline];
                        polyline.map = self.mapView;
                    }
                    if(!isContinousLocationTrackStart)
                    {
                        isContinousLocationTrackStart = YES;
                    }
                    
                }
            });
            
        }];
        
    });
    
}


-(void)clearAllPreviousPolyLines
{
    if(oldPolylines.count > 0)
    {
        for(int i = 0 ;i<oldPolylines.count ;i++ ) {
            GMSPolyline *polyLine = [oldPolylines objectAtIndex:i];
            polyLine.map = nil;
        }
    }
}

- (void)callPolyLineGoogleMapServiceWithcompletionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%@,%@", _addressObj.pickUpLatitude, _addressObj.pickUpLongitude];
    NSString *destinationString = [NSString stringWithFormat:@"%@,%@", _addressObj.dropOffLatitude, _addressObj.dropOffLongitude];
    
    NSDictionary *param = @{@"originString":originString,@"destinationString":destinationString
                            ,@"HideHUD":@"YES"};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGoogleMapDirectionApi
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"RESPONSEOBJECT DIRECTION API %@", responseObject);
                                                       
                                                       NSArray *routesArray = [responseObject objectForKey:@"routes"];
                                                       
                                                    
                                                       
                                                       if ([routesArray count] > 0)
                                                       {
                                                           NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                           if([self isDriverJustAcceptedRequest])
                                                           {
                                                               customerPickUpMarker.snippet = [Helper calculateTimeFromDirectionAPI:routeDict withIsDriver:NO];
                                                           }
                                                           else if([self isDriverTripStarted])
                                                           {
                                                               dropOffMarker.snippet = [Helper calculateTimeFromDirectionAPI:routeDict withIsDriver:NO];
                                                           }
                                                           else{
                                                               customerPickUpMarker.snippet = nil;
                                                               dropOffMarker.snippet = nil;
                                                           }
                                                           NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                           NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                           GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                           mainPolyline = [GMSPolyline polylineWithPath:path];
                                                           [mainPolyline setStrokeWidth:2.0];
                                                           [mainPolyline setStrokeColor:[UIColor blackColor]];
                                                           
                                                       }
                                                       
                                                      
                                                       if(completionHandler)
                                                           completionHandler(mainPolyline);
                                                    
                                                   }
                                                   failure:^(id responseObject){
                                                    //   [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}


#pragma mark - SharePopUpDismiss

-(void)showTripEndPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"TripEndedAlert_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"TripEndedAlert_Desc", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}
-(void)dismissPopUp
{
    [GlobalInfo showTaxiDashboardScreen];
}

#pragma mark - topNavActionDelegate

-(void)backButtonClicked{
    
    if( [_driverCurrentStatus isEqualToString:DriverStatus_Accepted] || [_driverCurrentStatus isEqualToString:DriverStatus_Reached] || [_driverCurrentStatus isEqualToString:DriverStatus_Started]){
        [GlobalInfo showHomeScreen];
    }
    else{
        [GlobalInfo showTaxiDashboardScreen];
    }
    
    
}
@end
