//
//  FindingRideVC.m
//  Angeler
//
//  Created by AJAY on 03/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "FindingRideVC.h"

@interface FindingRideVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblFindingYourRide;
@property (weak, nonatomic) IBOutlet UILabel *lblCancel;
@property (weak, nonatomic) IBOutlet UIView *viewCancel;
@property (weak, nonatomic) IBOutlet UIView *viewSeparator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ViewCAncelHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;

@end

@implementation FindingRideVC

#pragma mark- View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _containerViewHeightConstraint.constant = 63;
    _ViewCAncelHeightConstraint.constant = 0;
    _viewSeparator.hidden = YES;

    
    [self setLocalization];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(bookTripServiceSuccess:) name:TaxiPostNotificationCustomerBookTripAPISuccess object:nil];
    // Do any additional setup after loading the view from its nib.
}


#pragma mark - Notification

-(void)bookTripServiceSuccess:(NSNotification *) notification
{
     _containerViewHeightConstraint.constant = 128;
    _ViewCAncelHeightConstraint.constant = 52;
    _viewSeparator.hidden = NO;
    
}


#pragma mark- General Functions

-(void)setLocalization
{
    _lblFindingYourRide.text = NSLocalizedString(@"Finding_Your_Ride", nil);
    _lblCancel.text = NSLocalizedString(@"Cancel", nil);
}

#pragma mark- Button Action

- (IBAction)onCancelBtnClicked:(id)sender {
    

    [self showCancelRideAlert];
}

-(void)showCancelRideAlert
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:NSLocalizedString(@"Cancel_Ride_Alert", nil)
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                  
                                  
                                    [_delegate customerCancelRide];
                                  
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       /** What we write here???????? **/
                                       NSLog(@"you pressed No, thanks button");
                                       // call method whatever u need
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
