//
//  FindingRideVC.h
//  Angeler
//
//  Created by AJAY on 03/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CancelRideDismissDelegate <NSObject>
@optional
- (void)customerCancelRide;
@end

@interface FindingRideVC : UIViewController
@property (nonatomic, weak) id <CancelRideDismissDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
