//
//  CustomerBookTaxiVC.m
//  Angeler
//
//  Created by AJAY on 30/12/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "CustomerBookTaxiVC.h"
#import "PickUpDropOffAddressModel.h"
#import "FindingRideVC.h"
#import "CustomerTrackTrip.h"

@interface CustomerBookTaxiVC ()<GMSAutocompleteViewControllerDelegate,GMSMapViewDelegate,CancelRideDismissDelegate>
{
  // GMSAutocompleteFilter *_filter;
    GMSMarker *pickUpMarker , *dropOffMarker;
    BOOL isPickUpLocation,isLocationTrack,isSixtysecServiceCallCancel;
    NSMutableArray *markerArr;
    GMSPolyline *polyline;
    NSString *bookingID;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *lblFromTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblToTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFromLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblToLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnBookNow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bookNowConstraint;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@property (nonatomic, strong)  PickUpDropOffAddressModel *addressObj;

@end

@implementation CustomerBookTaxiVC

- (void)viewDidLoad {
    [super viewDidLoad];
    isSixtysecServiceCallCancel = true;
    bookingID = @"";
    [self initialSetUp];
    [self initialMapSetUp];
    [self setLocalization];
    [self setGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForegroundCalled:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    NSLog(@"View will appear called");
    [Helper showLocationServiceGoToSettingsAlert];
    
}


#pragma mark- General Functions

-(void)initialSetUp
{
    [self setPickUpDropOffPlaceHolderText];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
        _bookNowConstraint.constant = 60;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        _bookNowConstraint.constant = 40;
    }
    
    [_topNavbarView showNavbackBtn];
 
   // [_btnBookNow setEnabled:false];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Book_A_Taxi", nil) capitalizedString]];
    
    self.addressObj = [[PickUpDropOffAddressModel alloc] init];
    markerArr = [[NSMutableArray alloc] init];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(delayTimerEnd:) name:TaxiPostNotificationCustomerDelayTimerBookingEnd object:nil];
    

}

-(void)resetData{
    bookingID = @"";
    _lblToLocation.text = @"";
    //[_btnBookNow setEnabled:false];
    self.addressObj = [[PickUpDropOffAddressModel alloc] init];
    [markerArr removeAllObjects];
    
    _lblToLocation.text = @"";
    //NSLocalizedString(@"Search_Destination", nil);
    [_lblToLocation setTextColor:[UIColor placeholderTextColor]];
     [polyline setMap:nil];
    [_mapView clear];
    
    isPickUpLocation = YES;
    [self setMapAddressWithLatitude:[[AppDel shareModel] myLocation].latitude longitude:[[AppDel shareModel] myLocation].longitude];
    
    [self performSelector:@selector(setPlaceHolderTxt) withObject:self afterDelay:2.0];
}

-(void)setPlaceHolderTxt{
    
    _lblToLocation.text = NSLocalizedString(@"Search_Destination", nil);
    [_lblToLocation setTextColor:[UIColor placeholderTextColor]];
}

-(void)setBookTripNoDriverAlertAfterDelay:(NSString*)message{
    isSixtysecServiceCallCancel = true;
    [self dismissViewControllerAnimated:true completion:nil];
    [Helper showAlertWithTitle:Empty Message:message];
}


#pragma mark - Notification
-(void)appEnterForegroundCalled:(NSNotification *) notification
{
    NSLog(@"appEnterForegroundCalled");
    [Helper showLocationServiceGoToSettingsAlert];
   //  [[AppDel shareModel] startMonitoringLocation];
    
     if(![Helper isLocationServiceDenied])
    [self performSelector:@selector(initialMapSetUp) withObject:nil afterDelay:2.0];
   
}

-(void)delayTimerEnd:(NSNotification *) notification
{
    
    UIViewController *currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController visibleViewController];
    if([currentVc isKindOfClass:[self class]]){
       if(!isSixtysecServiceCallCancel)
        [self checkDriverStatusAPICall];
        
    }
   
}

-(void)setPickUpDropOffPlaceHolderText
{
    _lblToLocation.text = NSLocalizedString(@"Search_Destination", nil);
    [_lblToLocation setTextColor:[UIColor placeholderTextColor]];
    
    _lblFromLocation.text = NSLocalizedString(@"Search_Pickup_Location", nil);
    [_lblFromLocation setTextColor:[UIColor placeholderTextColor]];
}



-(void)initialMapSetUp
{
    [_mapView setMyLocationEnabled:YES];
    _mapView.settings.myLocationButton = true;
    
    isPickUpLocation = YES;
    [self setMapAddressWithLatitude:[[AppDel shareModel] myLocation].latitude longitude:[[AppDel shareModel] myLocation].longitude];
  
}

-(void)setMapAddressWithLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude{
    
    NSLog(@"Latitude %f Longitude %f",latitude,longitude);
    [CATransaction begin];

//    [CATransaction setAnimationDuration:5.0];
//    [self.mapView animateToCameraPosition:[GMSCameraPosition
//                                           cameraWithLatitude:latitude
//                                           longitude:longitude
//                                           zoom:17]];
    
    [self.mapView animateToZoom:17];
    [self.mapView animateToLocation:CLLocationCoordinate2DMake(latitude, longitude)];
    
    

    [CATransaction commit];
    
    if(![Helper isLocationServiceDenied])
    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
}

-(void)setLocalization
{
    _lblFromTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"From", nil)];
    _lblToTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"To", nil)];
    [_btnBookNow setTitle:NSLocalizedString(@"Book_Now", nil) forState:UIControlStateNormal];
}

-(void)setGesture
{
    UITapGestureRecognizer *fromLocationViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onFromLocationClicked:)];
    [_lblFromLocation addGestureRecognizer:fromLocationViewTapRecognizer];
    
    UITapGestureRecognizer *toLocationViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onToLocationClicked:)];
    [_lblToLocation addGestureRecognizer:toLocationViewTapRecognizer];
}

-(void)redirectToCustomerTrackScreen
{
    CustomerTrackTrip *Obj = [[CustomerTrackTrip alloc] initWithNibName:@"CustomerTrackTrip" bundle:nil];
    Obj.bookingID = bookingID;
    [self.navigationController pushViewController:Obj animated:YES];
}


#pragma mark - Webservice Call

- (void)callServiceBookTaxi{
    
    NSLog(@"callServiceBookTaxi");
    
    NSDictionary *param = @{@"pickupAddress":_addressObj.pickUpAddress ,
                            @"pickupLatitude":_addressObj.pickUpLatitude,
                            @"pickupLongitude":_addressObj.pickUpLongitude,
                            @"dropoffAddress":_addressObj.dropOffAddress ,
                            @"dropoffLatitude":_addressObj.dropOffLatitude,
                            @"dropoffLongitude":_addressObj.dropOffLongitude,
                            @"IsDelayHideHUD" : @"YES",
                            @"HideHUD":@"YES"
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kCustomerBookTaxi
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response kCustomerBookTaxi ===   %@",responseObject);
                                                    
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                         NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       if (statusCode == 200)
                                                       {
                                                             bookingID = [[[responseObject objectForKey:@"responseData"] objectForKey:@"bookingId"] stringValue];
                                                           isSixtysecServiceCallCancel = false;
                                                           [[NSNotificationCenter defaultCenter]
                                                            postNotificationName:TaxiPostNotificationCustomerBookTripAPISuccess
                                                            object:nil ];
                                                       }
                                                       else{
                                                          
                                                       //    [Helper HideAllLoadingHude];
                                                        
                                                           
                                                            if (statusCode == 601) {
                                                           
                                                                
                                                                [self performSelector:@selector(setBookTripNoDriverAlertAfterDelay:) withObject:message afterDelay:5.0];
                                                                
                                                                
                                                            }
                                                      else if (statusCode == 600) {
                                                           isSixtysecServiceCallCancel = true;
                                                           [self dismissViewControllerAnimated:true completion:nil];
                                                            [GlobalInfo showLoginScreen];
                                                          [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                          else if (statusCode == 249) {
                                                              isSixtysecServiceCallCancel = true;
                                                              [self dismissViewControllerAnimated:true completion:nil];
                                                               [GlobalInfo showTaxiDashboardScreen];
                                                               [Helper showAlertWithTitle:Empty Message:message];
                                                           }
                                                       else
                                                       {
                                                           isSixtysecServiceCallCancel = true;
                                                           [self dismissViewControllerAnimated:true completion:nil];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       }
        
                                                   }
                                                   failure:^(id responseObject){
                                                        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


- (void)callApiToLoadNearByDriver{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",(long)0],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"latitude":_addressObj.pickUpLatitude,
                            @"longitude":_addressObj.pickUpLongitude};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kNearByDriverList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response nearby Driver ===   %@",responseObject);
                                                      
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601){
                                                               
                                                               [self clearCarMap];
//                                                               NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
//                                                                [Helper showAlertWithTitle:Empty Message:message];
                                                           }
                                                           else
                                                           [self createCarMarker:[responseObject objectForKey:@"responseData"]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

-(void)checkDriverStatusAPICall{

NSDictionary *param = @{@"bookingId":bookingID
                        };

NSLog(@"bookingId Customer RideDetails %@",param);
//=======Call WebService Engine========
[[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kCustomerGetRideDetails
                                                Method:REQMETHOD_GET
                                            parameters:[GlobalInfo getParmWithSession:param]
                                                 media:nil
                                        viewController:self
                                               success:^(id responseObject){
                                                   NSLog(@"response kDriverGetRideDetails ===   %@",responseObject);
                                                   if ([GlobalInfo checkStatusSuccess:responseObject]){

                                                       NSString *driverStatus = [[responseObject objectForKey:@"responseData"] objectForKey:@"status"];
                                                       if( [driverStatus isEqualToString:DriverStatus_Accepted] || [driverStatus isEqualToString:DriverStatus_Reached] || [driverStatus isEqualToString:DriverStatus_Started]){
                                                           [self redirectToCustomerTrackScreen];
                                                       }
                                                       else{
                                                            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Book_NoDriverFound_Alert",nil)];
                                                       }


                                                   }
                                                   
                                                    [self dismissViewControllerAnimated:true completion:nil];

                                               }
                                               failure:^(id responseObject){
                                                   [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                    [self dismissViewControllerAnimated:true completion:nil];
                                               }];
}

- (void)callApiToCancelRide{
    NSLog(@"bookingID callApiToCancelRide %@",bookingID);
    NSDictionary *param = @{@"bookingId":bookingID,
                            @"reasonId" : @"",
                            @"screen" : @"booking"
                            };
    
    
    NSLog(@"param callApiToCancelRide %@",param);
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:KCustomerCancelBooking
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       
                                                        if (statusCode == 200) {
                                                           
                                                            [self resetData];
                                                           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Trip_Cancelled_Successfully", nil)];
                                                           
                                                       }
                                                        else  if (statusCode == 600) {
                                                            
                                                            [GlobalInfo showLoginScreen];
                                                            [Helper showAlertWithTitle:Empty Message:message];
                                                        }
                                                        else  if (statusCode == 603) {
                                                            
                                                            [GlobalInfo showTaxiDashboardScreen];
                                                            [Helper showAlertWithTitle:Empty Message:message];
                                                        }
                                                        else{
                                                            [Helper showAlertWithTitle:Empty Message:message];
                                                        }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


#pragma mark- GMSMapViewDelegate


- (BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView
{
    isPickUpLocation = YES;
    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",mapView.myLocation.coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",mapView.myLocation.coordinate.longitude]];
    return YES;
}



#pragma mark- Gesture Action

- (void)onFromLocationClicked:(UITapGestureRecognizer*)sender
{
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    
    // Specify the place data types to return.
//    GMSPlaceField fields = (GMSPlaceFieldName | GMSPlaceFieldPlaceID);
//    acController.placeFields = fields;
    
    // Specify a filter.
//    _filter = [[GMSAutocompleteFilter alloc] init];
//    _filter.type = kGMSPlacesAutocompleteTypeFiltes;
//    acController.autocompleteFilter = _filter;

    isPickUpLocation = YES;
    
    // Display the autocomplete view controller.
    [self presentViewController:acController animated:YES completion:nil];
}

- (void)onToLocationClicked:(UITapGestureRecognizer*)sender
{
    isPickUpLocation = NO;
    
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    
  
    // Display the autocomplete view controller.
    [self presentViewController:acController animated:YES completion:nil];
}


#pragma mark- Button Action


- (IBAction)onBookNowBtnClicked:(id)sender {

    
    if([_lblFromLocation.text isEqualToString:@""] || [_lblFromLocation.text isEqualToString:NSLocalizedString(@"Search_Pickup_Location", nil)])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"From_Addreess_Enter_Alert", nil)];
    }
    else  if([_lblToLocation.text isEqualToString:@""] || [_lblToLocation.text isEqualToString:NSLocalizedString(@"Search_Destination", nil)])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"To_Addreess_Enter_Alert", nil)];
    }
    else{
    [self callServiceBookTaxi];
    [self redirectToFindingRideScreen];
    }
   
}

-(void)redirectToFindingRideScreen{
    FindingRideVC *Obj = [[FindingRideVC alloc] initWithNibName:@"FindingRideVC" bundle:nil];
    Obj.delegate = self;
    self.definesPresentationContext = YES; //self is presenting view controller
    Obj.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    Obj.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:Obj animated:YES completion:nil];
}

#pragma mark- map Action

- (void)addMarkerToMapViewAtCameraPosition{
    
   
    if(isPickUpLocation)
    {
        pickUpMarker.map  = nil;
        [markerArr removeObject:pickUpMarker];
        pickUpMarker = [[GMSMarker alloc] init];
        CGRect frame = CGRectMake(0, 0, 23,33);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = [UIImage imageNamed:@"Location_Green_marker"];
        pickUpMarker.iconView = imageView;
        pickUpMarker.appearAnimation = kGMSMarkerAnimationPop;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        CLLocationCoordinate2D pickUpPosition  = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
        
        pickUpMarker.layer.latitude = pickUpPosition.latitude;
        pickUpMarker.layer.longitude = pickUpPosition.longitude;
        pickUpMarker.map = _mapView;
       
        [markerArr addObject:pickUpMarker];
        [CATransaction commit];
        
    }
    else
    {
       // [_btnBookNow setEnabled:true];
        
        dropOffMarker.map = nil;
         [markerArr removeObject:dropOffMarker];
        dropOffMarker = [[GMSMarker alloc] init];
        CGRect frame = CGRectMake(0, 0, 23,33);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = [UIImage imageNamed:@"Location_Red_marker"];
        dropOffMarker.iconView = imageView;
        dropOffMarker.appearAnimation = kGMSMarkerAnimationPop;
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        CLLocationCoordinate2D dropOffPosition  = CLLocationCoordinate2DMake([_addressObj.dropOffLatitude doubleValue],[_addressObj.dropOffLongitude doubleValue]);
     
        dropOffMarker.layer.latitude = dropOffPosition.latitude;
        dropOffMarker.layer.longitude = dropOffPosition.longitude;
        dropOffMarker.map = _mapView;
        
        [markerArr addObject:dropOffMarker];
        [CATransaction commit];
        
        
      
    }
    
    NSLog(@"markerArr count %d", [markerArr count]);
    
    if([markerArr count] > 1)
    {
        [self focusMapToShowAllMarkers];
    }
    
}

-(void)createCarMarker:(NSArray*)nearByLocationArr
{
    
    if(markerArr.count >= 1)
    {
        [self clearCarMap];
        if(markerArr.count > 1 && dropOffMarker != nil)
        {
            [markerArr removeAllObjects];
            [markerArr addObject:pickUpMarker];
            [markerArr addObject:dropOffMarker];
        }
        else
        {
             [markerArr removeAllObjects];
             [markerArr addObject:pickUpMarker];
        }
    }
   
    for(int i=0 ; i<nearByLocationArr.count ;i++)
    {
        NSDictionary *dic = [nearByLocationArr objectAtIndex:i];
        GMSMarker *marker = [[GMSMarker alloc] init];
        CGRect frame = CGRectMake(0, 0, 16,35);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = [UIImage imageNamed:@"Car_icon"];
        marker.iconView = imageView;
        CLLocationCoordinate2D position  = CLLocationCoordinate2DMake([[dic objectForKey:@"lastLocationLatitude"] doubleValue],[[dic objectForKey:@"lastLocationLongitude"] doubleValue]);
        marker.layer.latitude = position.latitude;
        marker.layer.longitude = position.longitude;
        marker.map = _mapView;
        
        [markerArr addObject:marker];
    }
   
    NSLog(@"markerArr Customer booking %@",markerArr);
   
    [self focusMapToShowAllMarkers];
}

-(void)clearCarMap
{
    for (GMSMarker *marker in markerArr)
    {
        if (marker == pickUpMarker || marker == dropOffMarker )
        {
            NSLog(@"Only Clear Car Marker");
        }
        else
        {
            marker.map  = nil;
        }
    }
}

- (void)focusMapToShowAllMarkers
{
    CLLocationCoordinate2D myLocation = ((GMSMarker *)markerArr.firstObject).position;
  
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    for (GMSMarker *marker in markerArr)
    {
        bounds = [bounds includingCoordinate:marker.position];
    }
    
   
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:60.0f];
            
    [_mapView animateWithCameraUpdate:update];
   
}


- (void)callRevGeoCodingWithLatitude:(NSString*)latitude longitude:(NSString*)longitude
{
    NSDictionary *param = @{@"lat":latitude,@"long":longitude
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGoogleMapApi
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       
                                                       
                                                       NSLog(@"response country API ===   %@",responseObject);
                                                       NSArray *array = [responseObject objectForKey:@"results"];
                                                       if ([array count] > 0)
                                                       {
                                                           NSArray *addressComponents = [[array objectAtIndex:0] objectForKey:@"address_components"];
                                                           
                                                           NSString *_CurrentLocationCity;
                                                           NSString *LocationCountry;
                                                           NSString *FormatedAddress = [[array objectAtIndex:0] objectForKey:@"formatted_address"];;
                                                           
                                                           for (NSDictionary *dict in addressComponents)
                                                           {
                                                               NSArray *types = [dict objectForKey:@"types"];
                                                               
                                                               if ([types containsObject:@"locality"] || [types containsObject:@"administrative_area_level_2"]||[types containsObject:@"administrative_area_level_3"])
                                                               {
                                                                   if([types containsObject:@"locality"]){
                                                                       _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                                                                       break;
                                                                   }else if ([types containsObject:@"administrative_area_level_3"]){
                                                                       _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                                                                       break;
                                                                   }
                                                                   else if ([types containsObject:@"administrative_area_level_2"]){
                                                                       
                                                                       _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                                                                       break;
                                                                   }
                                                               }
                                                           }
                                                           for (NSDictionary *dict in addressComponents)
                                                           {
                                                               NSArray *types = [dict objectForKey:@"types"];
                                                               
                                                               if ([types containsObject:@"country"])
                                                               {
                                                                   LocationCountry=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                                                               }
                                                           }
                                                          
                                                           
                                               if(isPickUpLocation)
                                               {
                                                   _lblFromLocation.text = FormatedAddress;
                                                    [_lblFromLocation setTextColor:[UIColor blackColor]];
                                                   _addressObj.pickUpLatitude = [NSNumber numberWithDouble:[latitude doubleValue]];
                                                   _addressObj.pickUpLongitude = [NSNumber numberWithDouble:[longitude doubleValue]];
                                                   _addressObj.pickUpAddress = FormatedAddress;
                                                   
                                                   [self addMarkerToMapViewAtCameraPosition];
                                                   [self callApiToLoadNearByDriver];
                                                   if(![_lblToLocation.text isEqualToString:Empty]){
                                                        [self drawRoute];
                                                   }
                                                  
                                               }
                                               else
                                               {
                                                   _lblToLocation.text = FormatedAddress;
                                                    [_lblToLocation setTextColor:[UIColor blackColor]];
                                                   _lblToLocation.alpha = 0.85;
                                                   _addressObj.dropOffLatitude = [NSNumber numberWithDouble:[latitude doubleValue]];
                                                   _addressObj.dropOffLongitude = [NSNumber numberWithDouble:[longitude doubleValue]];
                                                   _addressObj.dropOffAddress = FormatedAddress ;
                                                   [self addMarkerToMapViewAtCameraPosition];
                                                   [self drawRoute];
                                                  
                                               }
                                                          

                                                       }
                                                    }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}


#pragma mark- GMSAutocompleteViewControllerDelegate

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place ID %@", place.placeID);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSLog(@"Autocomplete Place Latitude %f Place Longitude %f", place.coordinate.latitude,place.coordinate.longitude);
     [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",place.coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",place.coordinate.longitude]];
  
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
   
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(void)sampleCode7
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_sync(aQueue,^{
        NSLog(@"sampleCode7 part1 %s",dispatch_queue_get_label(aQueue));
        NSLog(@"sampleCode7 This is the global Dispatch Queue");
    });
    
    dispatch_sync(aQueue,^{
        NSLog(@"sampleCode7 part2 %s",dispatch_queue_get_label(aQueue));
        for (int i =0; i<5;i++)
        {
            NSLog(@"sampleCode7 This is samplecode sync i %d",i);
            sleep(1);
        }
    });
    
   
}

#pragma mark- Draw Route

- (void)drawRoute
{
  
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue,^{
        [self callPolyLineGoogleMapServiceWithcompletionHandler:^(GMSPolyline *polyline) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // do work here
                if(polyline)
                {
                    
                    polyline.map = self.mapView;
                    if(!isLocationTrack)
                    {
                        isLocationTrack = YES;
                    }
                    
                }
            });
            
        }];
        
    });
   
}

- (void)callPolyLineGoogleMapServiceWithcompletionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%@,%@", _addressObj.pickUpLatitude, _addressObj.pickUpLongitude];
    NSString *destinationString = [NSString stringWithFormat:@"%@,%@", _addressObj.dropOffLatitude, _addressObj.dropOffLongitude];
    
    NSDictionary *param = @{@"originString":originString,@"destinationString":destinationString,@"HideHUD":@"YES"};
    
    NSLog(@"callPolyLineGoogleMapServiceWithcompletionHandler param %@",param);
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGoogleMapDirectionApi
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"RESPONSEOBJECT DIRECTION API %@", responseObject);
                                                       
                                                       NSArray *routesArray = [responseObject objectForKey:@"routes"];
                                                       
                                                     
                                                       
                                                       if ([routesArray count] > 0)
                                                       {
                                                           NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                           NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                           NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                           GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                           
                                                           [polyline setMap:nil];
                                                        
                                                           polyline = [GMSPolyline polylineWithPath:path];
                                                        
                                                           [polyline setStrokeWidth:2.0];
                                                           polyline.geodesic = YES;
                                                           [polyline setStrokeColor:[UIColor blackColor]];
                                                           
                                                       }
                                                       
                                                   
                                                          if(completionHandler)
                                                               completionHandler(polyline);
                                                      
                                                   }
                                                   failure:^(id responseObject){
                                                      
                                                   }];
   
}

#pragma mark - CancelRideDismissDelegate

- (void)customerCancelRide{
    
    if([bookingID isEqualToString:Empty]){
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Please_Try_Again", nil)];
    }
    else{
        [self dismissViewControllerAnimated:true completion:nil];
        isSixtysecServiceCallCancel = true;
        [self callApiToCancelRide];
    }
    
}



@end
