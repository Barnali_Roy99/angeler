//
//  RideCancelCell.h
//  Angeler
//
//  Created by AJAY on 11/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundView.h"

NS_ASSUME_NONNULL_BEGIN

@interface RideCancelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RoundView *notSelectedRoundView;
@property (weak, nonatomic) IBOutlet RoundView *selectedRoundView;
@property (weak, nonatomic) IBOutlet UILabel *lblCancelResonTitleText;

@end

NS_ASSUME_NONNULL_END
