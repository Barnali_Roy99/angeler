//
//  RideCancelCell.m
//  Angeler
//
//  Created by AJAY on 11/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "RideCancelCell.h"

@implementation RideCancelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _notSelectedRoundView.layer.borderColor = [[UIColor appThemeBlueColor] CGColor];
    _notSelectedRoundView.layer.borderWidth = 2.0;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
