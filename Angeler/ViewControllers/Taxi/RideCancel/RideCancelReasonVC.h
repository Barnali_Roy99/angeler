//
//  RideCancelReasonVC.h
//  Angeler
//
//  Created by AJAY on 11/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RideCancelReasonVC : UIViewController
@property (nonatomic,assign)BOOL isDriverCancelRide;
@property (nonatomic,strong)NSString *driverOrRiderGetCancelReasonAPIName;
@property (nonatomic,strong)NSString *bookingID;
@property (nonatomic,strong)NSString *currentRideStatus;
@end

NS_ASSUME_NONNULL_END
