//
//  RideCancelReasonVC.m
//  Angeler
//
//  Created by AJAY on 11/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "RideCancelReasonVC.h"
#import "RideCancelCell.h"


static NSString *CellIdentifier = @"RideCancelCell";


@interface RideCancelReasonVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger selectedIndex;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *showReasonListTableView;
@property (weak, nonatomic) IBOutlet UIView *viewRideCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblCancelRide;
@property (nonatomic, strong) NSMutableArray *arrCancelReasonList;
@property (weak, nonatomic) IBOutlet UIView *dismissControllerView;


@end

@implementation RideCancelReasonVC
@synthesize driverOrRiderGetCancelReasonAPIName;

#pragma mark- View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self setGesture];
    [self setLocalization];
    [self callApiToGetCancelReasonList];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark- General Functions

-(void)initialSetUp{
    
    
    NSLog(@"Driver Ride Cancel API name %@",driverOrRiderGetCancelReasonAPIName);
    [self.view endEditing:true];
    [_viewRideCancel makeCornerRound:15.0];
    
    [_showReasonListTableView registerNib:[UINib nibWithNibName:@"RideCancelCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _showReasonListTableView.estimatedRowHeight = 20.0;
    _showReasonListTableView.rowHeight = UITableViewAutomaticDimension;
    
    selectedIndex = -1;
    _viewRideCancel.alpha = 0.5;
    [_viewRideCancel setUserInteractionEnabled:false];
    
}

-(void)setLocalization
{
    _lblTitle.text = NSLocalizedString(@"Cancellation_Reason", nil);
    _lblCancelRide.text = [NSLocalizedString(@"Cancel_Ride", nil) uppercaseString];
}

-(void)setGesture{
    UITapGestureRecognizer *cancelRideViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelRideViewTapped:)];
    
    UITapGestureRecognizer *dismissViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissControllerViewTapped:)];
    
    [self.dismissControllerView addGestureRecognizer:dismissViewTapRecognizer];
    [self.viewRideCancel addGestureRecognizer:cancelRideViewTapRecognizer];
  
}

#pragma mark- Gesture Action

- (void)cancelRideViewTapped:(UITapGestureRecognizer*)sender{
    [self callApiToCancelRide];
}

- (void)dismissControllerViewTapped:(UITapGestureRecognizer*)sender{
     [self dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrCancelReasonList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RideCancelCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if(selectedIndex == indexPath.row)
    {
        [cell.selectedRoundView setHidden:false];
    }
    else{
         [cell.selectedRoundView setHidden:true];
    }
    cell.lblCancelResonTitleText.text = [[_arrCancelReasonList objectAtIndex:indexPath.row] objectForKey:@"reason"];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedIndex != indexPath.row){
        selectedIndex = indexPath.row;
        [_viewRideCancel setUserInteractionEnabled:true];
        _viewRideCancel.alpha = 1.0;
        [_showReasonListTableView reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - Webservice Call



- (void)callApiToGetCancelReasonList{
    
    NSDictionary *param = @{};
    NSString *apiName;
    if(_isDriverCancelRide)
        apiName = kDriverCancellationReasonList;
    else
         apiName = kCustomerCancellationReasonList;
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:apiName
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response nearby Driver ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           _arrCancelReasonList = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"responseData"]];
                                                           [_showReasonListTableView reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                       [self dismissViewControllerAnimated:true completion:nil];
                                                   }];
}

- (void)callApiToCancelRide{
    
    NSString *apiName;
    if(_isDriverCancelRide)
        apiName = KDriverCancelBooking;
    else
        apiName = KCustomerCancelBooking;
    
    NSDictionary *param = @{@"bookingId":_bookingID,
                            @"reasonId" : [[_arrCancelReasonList objectAtIndex:selectedIndex] objectForKey:@"reasonId"],
                            @"screen" : [_currentRideStatus lowercaseString]
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:apiName
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       //if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSLog(@"response kCustomerBookTaxi ===   %@",responseObject);
                                                           
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           
                                                           if (statusCode == 200)
                                                           {
                                                                //[self dismissViewControllerAnimated:true completion:nil];
                                                               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Trip_Cancelled_Successfully", nil)];
                                                               
                                                                [GlobalInfo showTaxiDashboardScreen];
                                                           }
                        else{
                                                              
                                        
                        if (statusCode == 600) {
                                                                    [self dismissViewControllerAnimated:true completion:nil];
                                                                   [GlobalInfo showLoginScreen];
                                                                   [Helper showAlertWithTitle:Empty Message:message];
                                                               }
                                                               else if(statusCode == 603)
                                                               {
                                                                   
                                                                   
                                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:Empty
                                                                                                                                 message:message
                                                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                                                                   
                                                                   UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                                                      style:UIAlertActionStyleDefault
                                                                                                                    handler:^(UIAlertAction * action)
                                                                                              {
                                                                       [GlobalInfo showTaxiDashboardScreen];
                                                                       
                                                                   }];
                                                                   
                                                                   
                                                                   
                                                                   [alert addAction:okButton];
                                                                   [self presentViewController:alert animated:YES completion:nil];
                                                               }
                                                               else{
                                                                   [Helper showAlertWithTitle:Empty Message:message];
                                                               }
                                                           }
                                                           
                                                       }
                                                           
     
     
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

@end
