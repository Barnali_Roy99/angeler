//
//  TaxiDashboardVC.m
//  Angeler
//
//  Created by AJAY on 30/12/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "TaxiDashboardVC.h"
#import "CustomerBookTaxiVC.h"
#import "DriverRequestListVC.h"
#import "DriverTripStartVC.h"
#import "CustomerTrackTrip.h"
#import "SharePopUpViewController.h"
#import "VerifyAccountViewController.h"
#import "DriverVerificationVC.h"

@interface TaxiDashboardVC ()<SharePopUpDismissDelegate>
{
    BOOL isDriverVerification;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *CustomerBookingTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverBookingTitlelabel;
@property (weak, nonatomic) IBOutlet UIView *viewCustomerBooking;
@property (weak, nonatomic) IBOutlet UIView *viewAngelerDriver;


@end

@implementation TaxiDashboardVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self initialSetUp];
    [self checkDriverRiderCancelSetUp];
    [self setGesture];
    
    [Helper showLocationServiceGoToSettingsAlert];
    
   if(!_isFromTaxidashboard){
     
        [self callApiToGetTaxiDriverRiderStatus];
    }
    // Do any additional setup after loading the view from its nib.
}

#pragma mark- General Functions


-(void)checkDriverRiderCancelSetUp{
  
    if(_isFromCanelByDriverNotification){
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Driver_Cancel_Ride_AlertMsg", nil)];
    }
    else if(_isFromCanelByRiderNotification){
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Customer_Cancel_Ride_AlertMsg", nil)];
    }
}

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView showSquareTitleLogo];
    
    //Localization
    
    _CustomerBookingTitleLabel.text = [NSLocalizedString(@"Book_A_Taxi", nil) uppercaseString];
    _driverBookingTitlelabel.text = [NSLocalizedString(@"Angeler_Driver", nil) uppercaseString];
}


-(void)setGesture
{
    UITapGestureRecognizer *bookTaxiViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bookTaxiViewTapped:)];
    [_viewCustomerBooking addGestureRecognizer:bookTaxiViewTapRecognizer];
    
    UITapGestureRecognizer *angelerDriverViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(angelerDriverViewTapped:)];
    [_viewAngelerDriver addGestureRecognizer:angelerDriverViewTapRecognizer];
}

#pragma mark- Gesture Action

- (void)bookTaxiViewTapped:(UITapGestureRecognizer*)sender
{
    if([Helper isAngelerStatusNotVerified]){
        isDriverVerification = NO;
        [self showNotVerifiedAccountAlertPopUp];
    }
    else if([Helper isAngelerStatusPending]){
         isDriverVerification = NO;
         [self showPendingAccountAlertPopUp];
    }
    else if([Helper isAngelerStatusRejected]){
         isDriverVerification = NO;
         [self showRejectedAccountAlertPopUp];
    }
    else{
        //if(_isFromTaxidashboard){
        [self redirectCustomerTaxiBooking];
    }
//    }
//    else{
//        [self callApiToGetTaxiDriverRiderStatus:false];
//    }
}

- (void)angelerDriverViewTapped:(UITapGestureRecognizer*)sender
{
    if([Helper isAngelerStatusNotVerified]){
        isDriverVerification = NO;
        [self showNotVerifiedAccountAlertPopUp];
    }
    else if([Helper isAngelerStatusPending]){
        isDriverVerification = NO;
        [self showPendingAccountAlertPopUp];
    }
    else if([Helper isAngelerStatusRejected]){
        isDriverVerification = NO;
        [self showRejectedAccountAlertPopUp];
    }
    else if([Helper isDriverStatusNotVerified]){
        isDriverVerification = YES;
        [self showDriverNotVerifiedAccountAlertPopUp];
    }
    else if([Helper isDriverStatusPending]){
        isDriverVerification = YES;
        [self showDriverPendingAccountAlertPopUp];
    }
    else if([Helper isDriverStatusRejected]){
        isDriverVerification = YES;
        [self showDriverRejectedAccountAlertPopUp];
    }
    else{
        //if(_isFromTaxidashboard){
        [self redirectDriverRequestListScreen];
    }
//    else{
//        [self callApiToGetTaxiDriverRiderStatus:true];
//    }
}

#pragma mark- API Call

//- (void)callApiToGetTaxiDriverRiderStatus:(BOOL)isDriverClicked
- (void)callApiToGetTaxiDriverRiderStatus
{
    
    NSDictionary *param = [[NSDictionary alloc] init];
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGetTaxiDeriverRiderStatus
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
                                                          
//                                                           [self redirectToTaxiScreenWithStatusCheck:dataDict withIsDriver:isDriverClicked];
                                                           [self redirectToTaxiScreenWithStatusCheck:dataDict];
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


-(void)redirectToTaxiScreenWithStatusCheck:(NSDictionary*)dataDic{
                             
    NSString *driverStatus = [dataDic objectForKey:@"bookingStatus"];
    
    if([[dataDic objectForKey:@"driverStatus"] isEqualToString:DriverRiderStatusFree] && [[dataDic objectForKey:@"riderStatus"] isEqualToString:DriverRiderStatusFree])
    {
//        if(isDriverClicked)
//        [self redirectDriverRequestListScreen];
//        else
//           [self redirectCustomerTaxiBooking];
    }
    else if([[dataDic objectForKey:@"driverStatus"] isEqualToString:DriverRiderStatusEngaged])
    {
        if( [driverStatus isEqualToString:DriverStatus_Accepted] || [driverStatus isEqualToString:DriverStatus_Reached] || [driverStatus isEqualToString:DriverStatus_Started]){
            [self redirectToDriverStartTripScreen:[dataDic objectForKey:@"bookingId"]];
        }
        
    }
    else if([[dataDic objectForKey:@"riderStatus"] isEqualToString:DriverRiderStatusEngaged])
    {
        if( [driverStatus isEqualToString:DriverStatus_Accepted] || [driverStatus isEqualToString:DriverStatus_Reached] || [driverStatus isEqualToString:DriverStatus_Started]){
             [self redirectToCustomerTrackScreen:[dataDic objectForKey:@"bookingId"]];
        }
       
    }
}

-(void)redirectCustomerTaxiBooking{
    CustomerBookTaxiVC *Obj = [[CustomerBookTaxiVC alloc] initWithNibName:@"CustomerBookTaxiVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}

-(void)redirectDriverRequestListScreen{
    DriverRequestListVC *Obj = [[DriverRequestListVC alloc] initWithNibName:@"DriverRequestListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
    
}

-(void)redirectToDriverStartTripScreen:(NSString*)driverBookingID
{
    DriverTripStartVC *Obj = [[DriverTripStartVC alloc] initWithNibName:@"DriverTripStartVC" bundle:nil];
    Obj.bookingID = driverBookingID;
    [self.navigationController pushViewController:Obj animated:YES];
}

-(void)redirectToCustomerTrackScreen:(NSString*)bookingID
{
    CustomerTrackTrip *Obj = [[CustomerTrackTrip alloc] initWithNibName:@"CustomerTrackTrip" bundle:nil];
    Obj.bookingID = bookingID;
    [self.navigationController pushViewController:Obj animated:YES];
}

#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    if(isDriverVerification){
        DriverVerificationVC *verifyAccountObj = [[DriverVerificationVC alloc] initWithNibName:@"DriverVerificationVC" bundle:nil];
        [self.navigationController pushViewController:verifyAccountObj animated:YES];
    }
    else{
        VerifyAccountViewController *verifyAccountObj = [[VerifyAccountViewController alloc] initWithNibName:@"VerifyAccountViewController" bundle:nil];
        [self.navigationController pushViewController:verifyAccountObj animated:YES];
    }
}



#pragma mark - Share Pop Up show

-(void)showNotVerifiedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_NotVerified_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_NotVerified_Message", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showPendingAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_Pending_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Pending_Message", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showRejectedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Account_Verification_Rejected_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Verification_Rejected_Msg", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}


-(void)showDriverNotVerifiedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"DriverAccount_NotVerified_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"DriverAccount_NotVerified_Message", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showDriverPendingAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.headerTitleText = NSLocalizedString(@"DriverAccount_Pending_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"DriverAccount_Pending_Message", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showDriverRejectedAccountAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"DriverAccount_Verification_Rejected_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"DriverAccount_Verification_Rejected_Msg", nil);
    sharePopUpVC.bottomDescriptionText = @"";
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"Start", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}


@end
