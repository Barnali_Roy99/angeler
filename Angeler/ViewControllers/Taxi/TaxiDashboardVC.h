//
//  TaxiDashboardVC.h
//  Angeler
//
//  Created by AJAY on 30/12/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaxiDashboardVC : UIViewController
@property(assign)BOOL isFromTaxidashboard;
@property(assign)BOOL isFromCanelByDriverNotification;
@property(assign)BOOL isFromCanelByRiderNotification;
@end

NS_ASSUME_NONNULL_END
