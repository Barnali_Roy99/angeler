//
//  DriverRequestListCell.h
//  Angeler
//
//  Created by AJAY on 06/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DriverRequestListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblFromTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblToTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFromLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblToLocation;
@property (weak, nonatomic) IBOutlet UIView *detailsView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsViewTitle;

@end

NS_ASSUME_NONNULL_END
