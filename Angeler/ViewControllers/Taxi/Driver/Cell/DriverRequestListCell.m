//
//  DriverRequestListCell.m
//  Angeler
//
//  Created by AJAY on 06/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "DriverRequestListCell.h"

@implementation DriverRequestListCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    _lblFromTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"From", nil)];
    _lblToTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"To", nil)];
    _lblDetailsViewTitle.text = NSLocalizedString(@"View", nil);
    [_containerView makeCornerRound:15.0];
    [_containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
