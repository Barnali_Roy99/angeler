//
//  DriverTripStartVC.h
//  Angeler
//
//  Created by AJAY on 15/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickUpDropOffAddressModel.h"
#import "Angeler-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface DriverTripStartVC : UIViewController

@property (nonatomic, strong)  NSString *bookingID;
@property (nonatomic, strong)  NSString *driverCurrentStatus;

@end

NS_ASSUME_NONNULL_END
