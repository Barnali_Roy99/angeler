//
//  DriverVerificationVC.m
//  Angeler
//
//  Created by AJAY on 12/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "DriverVerificationVC.h"
#import "MWPhotoBrowser.h"
#import "SharePopUpViewController.h"

//dicUploadedDocmentInfo key name 1.UploadDrivingLicence 2.UploadRegCertificate
//3.UploadVehicle

@interface DriverVerificationVC ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MWPhotoBrowserDelegate,SharePopUpDismissDelegate>
{
    NSString *documentType;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (nonatomic, weak) IBOutlet UIImageView *imgProfile;

//Driver Info

@property (weak, nonatomic) IBOutlet UIView *viewDriverInfo;
@property (weak, nonatomic) IBOutlet UIView *viewDriverInfoHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverInfoTitle;

@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIView *viewLicenceNumber;
@property (weak, nonatomic) IBOutlet UIView *viewVehicleName;
@property (weak, nonatomic) IBOutlet UIView *viewVehicleNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleLicenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleVehicleName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleVehicleNumber;
@property (nonatomic, weak) IBOutlet UITextField *txtLicenceNumber;
@property (nonatomic, weak) IBOutlet UITextField *txtVehicleName;
@property (nonatomic, weak) IBOutlet UITextField *txtVehicleNumber;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleUploadLicence;
@property (weak, nonatomic) IBOutlet UIImageView *imgUploadLicence;
@property (weak, nonatomic) IBOutlet UIImageView *imgCameraLicence;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadLicense;


@property (weak, nonatomic) IBOutlet UILabel *lblTitleUploadRegsCertificate;
@property (weak, nonatomic) IBOutlet UIImageView *imgUploadRegsCertificate;
@property (weak, nonatomic) IBOutlet UIImageView *imgCameraRegsCertificate;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadRegsCertificate;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleUploadVehiclePicture;
@property (weak, nonatomic) IBOutlet UIImageView *imgUploadVehiclePicture;
@property (weak, nonatomic) IBOutlet UIImageView *imgCameraVehiclePicture;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadVehiclePicture;

@property (nonatomic, strong)NSMutableDictionary *dicUploadedDocmentInfo;
@property (weak, nonatomic) IBOutlet UIView *submitView;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmit;
@property (nonatomic, strong) NSMutableArray *arrDocPhotos;


//ImageObjectClass *imageObj;

@end

@implementation DriverVerificationVC

#pragma mark - Object Life Cycle

- (void)viewDidLoad {
    _arrDocPhotos = [[NSMutableArray alloc] init];
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:true];
    [self setUpView];
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
   [self callAPIToViewDriverInfo];

    _dicUploadedDocmentInfo = [[NSMutableDictionary alloc] init];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:[NSLocalizedString(@"Driver_Verification", nil) capitalizedString]];
    

    //Profile Image Set
    
    [_imgProfile sd_setImageWithURL:[NSURL URLWithString:[[GlobalInfo sharedInfo] profilePic]] placeholderImage:[UIImage imageNamed:@""]];
    
    
}

-(void)setUpView{
    
    //Driver Info
    
    [self.viewDriverInfo makeCornerRound:20.0];
    [self.viewDriverInfoHeader dropShadowWithColor:[UIColor lightGrayColor]];
    [self.viewDriverInfoHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    
    [self.mainContentView makeCornerRound:15.0];
    [self.viewLicenceNumber makeCornerRound:15.0];
    [self.viewVehicleName makeCornerRound:15.0];
    [self.viewVehicleNumber makeCornerRound:15.0];
    
    //Document Upload
    
    [self.imgUploadLicence makeCornerRound:20.0 withBoarderColor:[UIColor appThemeBlueColor] borderWidth:2.5];
    [self.imgUploadLicence dropShadowWithColor:[UIColor whiteColor]];
    
    [self.imgUploadRegsCertificate makeCornerRound:20.0 withBoarderColor:[UIColor appThemeBlueColor] borderWidth:2.5];
    [self.imgUploadRegsCertificate dropShadowWithColor:[UIColor whiteColor]];
    
    [self.imgUploadVehiclePicture makeCornerRound:20.0 withBoarderColor:[UIColor appThemeBlueColor] borderWidth:2.5];
    [self.imgUploadVehiclePicture dropShadowWithColor:[UIColor whiteColor]];
    
    [_submitView makeCornerRound:15.0];
}

-(void)setLocalization{
    
    //Driver Info
    _lblDriverInfoTitle.text = NSLocalizedString(@"Driver_Details", nil);
    
    _lblTitleLicenceNumber.text = NSLocalizedString(@"Licence_Number", nil);
    _lblTitleVehicleName.text = NSLocalizedString(@"Vehicle_Name", nil);
    _lblTitleVehicleNumber.text = NSLocalizedString(@"Vehicle_Number", nil);
    
    _lblTitleUploadLicence.text = NSLocalizedString(@"Upload_Driving_Licence", nil);
    _lblTitleUploadRegsCertificate.text = NSLocalizedString(@"Upload_Registration_Certificate", nil);
    _lblTitleUploadVehiclePicture.text = NSLocalizedString(@"Upload_Vehicle_Picture", nil);
    _lblSubmit.text = [NSLocalizedString(@"Submit", nil) uppercaseString];
}

-(void)setGesture
{
    UITapGestureRecognizer *submitTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped:)];
    [_submitView addGestureRecognizer:submitTapRecognizer];
}

-(void)checkDriverVeificationStatus
{
    NSLog(@"Driver Verification Status %@", [[GlobalInfo sharedInfo] userAngelerStatus]);
    if([[[[GlobalInfo sharedInfo] taxiDriverVerificationStatus] lowercaseString] isEqualToString:verificationStatus_Verified])
    {
        [self setViewStatusVerified];
    }
    
}

-(void)setViewStatusVerified{
    _txtVehicleNumber.userInteractionEnabled = false;
    _txtVehicleName.userInteractionEnabled = false;
    _txtLicenceNumber.userInteractionEnabled = false;
    
    _imgUploadVehiclePicture.userInteractionEnabled = false;
    _imgUploadLicence.userInteractionEnabled = false;
    _imgUploadRegsCertificate.userInteractionEnabled = false;
    
     [_btnUploadLicense setEnabled:false];
     [_btnUploadVehiclePicture setEnabled:false];
     [_btnUploadRegsCertificate setEnabled:false];
    
    _submitView.hidden = YES;
}

- (BOOL)validate{
    
    
    if ([Helper checkEmptyField:_txtLicenceNumber.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"LicenceNumber_Blank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtVehicleName.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"VehicleName_Blank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtVehicleNumber.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"VehicleNumber_Blank_Alert", nil)];
        return NO;
    }
    if (![_dicUploadedDocmentInfo objectForKey:KUploadDrivingLicence]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Upload_Driving_Licence_Blank_Alert", nil)];
        return NO;
    }
    if (![_dicUploadedDocmentInfo objectForKey:KUploadRegCertificate]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Upload_Registration_Certificate_Blank_Alert", nil)];
        return NO;
    }
    if (![_dicUploadedDocmentInfo objectForKey:KUploadVehiclePicture]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Upload_Vehicle_Picture_Blank_Alert", nil)];
        return NO;
    }
    
    return YES;
}

#pragma mark - Webservice Call

-(void)callAPIToViewDriverInfo
{
    NSDictionary *param = @{@"mode":@"View",
                            @"driverLicenseNo":@"",
                            @"driverVehicleName":@"",
                            @"driverVehicleNo":@"",
                            @"driverLicenseImage":@"",
                            @"driverRCImage":@"",
                            @"driverVehicleImage":@""
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:KDriverVerificationDetailsUpdate
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           
                                                           [self updateValuesWithData:[responseObject objectForKey:@"responseData"]];
                                                           
                                                           
                                                           
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}


- (void)updateValuesWithData:(NSDictionary*)data{
    
    [self setDriverStatus:data];
    [self checkDriverVeificationStatus];
    _txtLicenceNumber.text = [data objectForKey:@"driverLicenseNo"];
    _txtVehicleName.text = [data objectForKey:@"driverVehicleName"];
    _txtVehicleNumber.text = [data objectForKey:@"driverVehicleNo"];
    
    if(![[data objectForKey:@"driverLicenseImage"] isEqualToString:Empty]){
    [_imgUploadLicence sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"driverLicenseImage"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
        _imgCameraLicence.hidden = YES;
    }
    
    if(![[data objectForKey:@"driverRCImage"] isEqualToString:Empty]){
        [_imgUploadRegsCertificate sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"driverRCImage"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
        _imgCameraRegsCertificate.hidden = YES;
    }
    if(![[data objectForKey:@"driverVehicleImage"] isEqualToString:Empty]){
        [_imgUploadVehiclePicture sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"driverVehicleImage"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
        _imgCameraVehiclePicture.hidden = YES;
    }
    

}

-(void)setDriverStatus:(NSDictionary*)dataDic{
    [[GlobalInfo sharedInfo] setTaxiDriverVerificationStatusText:[dataDic objectForKey:@"driverVerifiedStatusText"]];
    [[GlobalInfo sharedInfo] setTaxiDriverVerificationStatus:[dataDic objectForKey:@"driverVerificationStatus"]];
}

#pragma mark - Share Pop Up show

-(void)showAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Thanks", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Verify_Submit_Alert", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    [self.navigationController popViewControllerAnimated:false];
}




#pragma mark - Gesture Action


- (void)submitViewTapped:(UITapGestureRecognizer*)sender
{
    if (![self validate]) {
        return;
    }

    NSDictionary *param = @{@"mode":@"Add",
                            @"driverLicenseNo":_txtLicenceNumber.text,
                            @"driverVehicleName":_txtVehicleName.text,
                            @"driverVehicleNo":_txtVehicleNumber.text,
                            @"driverLicenseImage":[self getImageJsonFromImgObj:[_dicUploadedDocmentInfo objectForKey:KUploadDrivingLicence]],
                            @"driverRCImage":[self getImageJsonFromImgObj:[_dicUploadedDocmentInfo objectForKey:KUploadRegCertificate]],
                            @"driverVehicleImage":[self getImageJsonFromImgObj:[_dicUploadedDocmentInfo objectForKey:KUploadVehiclePicture]]
                            };
    
    NSLog(@"Param KDriverVerificationDetailsUpdate %@",param);
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:KDriverVerificationDetailsUpdate
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response KDriverVerificationDetailsUpdate ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
                                                           [self showAlertPopUp];
                                                           [self setDriverStatus:dataDict];
                                                           [self checkDriverVeificationStatus];
                                                         
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
    
}

#pragma mark - UITextfieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
    
}
#pragma mark - Button Action

- (IBAction)onUploadLicenceClicked:(id)sender {
    documentType = KUploadDrivingLicence;
    [self showCameraGalleryOnTap];
    
}

- (IBAction)onUploadRegCertificateClicked:(id)sender {
    documentType = KUploadRegCertificate;
    [self showCameraGalleryOnTap];
    
}

- (IBAction)onUploadVehiclePictureClicked:(id)sender {
    documentType = KUploadVehiclePicture;
    [self showCameraGalleryOnTap];
    
}

- (void)showCameraGalleryOnTap
{
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
               // imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
                
                [Helper showCameraGoToSettingsAlert];
            }
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CameraNotAvailable_Alert", nil)];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
         //   imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"GalleryNotAvailable_Alert", nil)];
        }
    }];
    
    [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Upload_Image_From", nil) actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];
    
}

#pragma mark- UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = info[UIImagePickerControllerOriginalImage] ;
    //info[UIImagePickerControllerEditedImage];
    ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
    
   
    //=========get image type and compress========
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
    // NSString *strImageType = Empty;
    
    if (UTTypeConformsTo(imageUTI, kUTTypeJPEG))
    {
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
        CFRelease(imageUTI);
    }
    else if (UTTypeConformsTo(imageUTI, kUTTypePNG))
    {
        // Handle PNG
        imageObj.mimeType = @"png";
        imageObj.imgData = UIImagePNGRepresentation(image);
        CFRelease(imageUTI);
    }
    else
    {
        NSLog(@"Unhandled Image UTI: %@", imageUTI);
        
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        imageObj.mediaType = @"image";
        if([documentType isEqualToString:KUploadDrivingLicence])
        {
            _imgUploadLicence.image = image;
            [_imgCameraLicence setHidden:true];
            
        }
        else if([documentType isEqualToString:KUploadRegCertificate])
        {
            _imgUploadRegsCertificate.image = image;
            [_imgCameraRegsCertificate setHidden:true];
        }
        else  if([documentType isEqualToString:KUploadVehiclePicture])
        {
            _imgUploadVehiclePicture.image = image;
            [_imgCameraVehiclePicture setHidden:true];
        }
        [_dicUploadedDocmentInfo setObject:imageObj forKey:documentType];
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
}

#pragma mark- Utility Method

-(NSString*)getImageJsonFromImgObj:(ImageObjectClass*)imgObj{
   
    NSMutableArray *dictImages = [[NSMutableArray alloc] init];
        NSDictionary *dict = @{@"action":@"added",
                               @"base64String":[imgObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                               @"imageExt":imgObj.mimeType,
                               @"imageId":Empty,
                               @"imageName":imgObj.keyName,
                               @"imageURL":Empty
                               };
    [dictImages addObject:dict];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0//NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = Empty;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"json data === %@",jsonString);
    }
    
    return jsonString;
    
}



@end
