//
//  DriverTripStartVC.m
//  Angeler
//
//  Created by AJAY on 15/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "DriverTripStartVC.h"
#import "RideCancelReasonVC.h"
#import "SharePopUpViewController.h"

@interface DriverTripStartVC ()<ARCarMovementDelegate,topNavActionDelegate>
{
    
    GMSMarker *pickUpMarker,*customerPickUpMarker,*dropOffMarker;
    NSMutableArray *markerArr,*oldPolylines;
    GMSPolyline *mainPolyline;
    BOOL isContinousLocationTrackStart;
    //isReachingScreenShow;
    NSNumber *dropOffLat,*dropOffLong;
    CLLocationCoordinate2D prevCoOrdinate;
    NSDictionary *rideDetailsDic;
    NSTimer *driverContinuousMoveCarTimer;
    
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *lblFromTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblToTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFromLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblToLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnReached;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet RoundImageView *customerImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet UIView *viewEndTrip;
@property (weak, nonatomic) IBOutlet UIButton *btnEndTrip;
@property (weak, nonatomic) IBOutlet UIView *viewReachedStartTrip;
@property (nonatomic, strong)  PickUpDropOffAddressModel *addressObj;

@property (strong, nonatomic) ARCarMovement *moveMent;

@end

@implementation DriverTripStartVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForegroundCalled:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(riderCancelTripNotificationAction:) name:TaxiPostNotificationRiderCancelTrip object:nil];
    
    self.addressObj = [[PickUpDropOffAddressModel alloc] init];
    self.moveMent = [[ARCarMovement alloc]init];
    self.moveMent.delegate = self;
    
    isContinousLocationTrackStart = false;
    oldPolylines = [[NSMutableArray alloc]init];
    markerArr = [[NSMutableArray alloc] init];
    
    [self initialSetUp];
    [self setLocalization];
   
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
      [Helper showLocationServiceGoToSettingsAlert];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if(![Helper isLocationServiceDenied])
    [self callGetRideDetailsServiceWithIsFirstLoad:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [self stopTimer];
}

#pragma mark -Notification
//if location service not enabled => back from settings or background
-(void)appEnterForegroundCalled:(NSNotification *) notification
{
    NSLog(@"appEnterForegroundCalled");
    [Helper showLocationServiceGoToSettingsAlert];
    
    if(![Helper isLocationServiceDenied])
    [self performSelector:@selector(startPageFromInitial) withObject:nil afterDelay:2.0];
    
}

-(void)riderCancelTripNotificationAction:(NSNotification *) notification
{
     [self callGetRideDetailsServiceWithIsFirstLoad:false];
}

-(void)startPageFromInitial{
     [self callGetRideDetailsServiceWithIsFirstLoad:YES];
}

#pragma mark - Continiuous Location Track

- (void)updateLocation:(id)sender{
    if(isContinousLocationTrackStart)
    {
        NSLog(@"Location change Continuous tracking called");
        prevCoOrdinate  = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
        [self currentLocationSetToPickUpPoint];
       
        if(![self isDriverReachedBeforeStart])
        {
            [self drawRoute];
        }
        
        CLLocationCoordinate2D newCoordinate = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
        
        
        /**
         *  You need to pass the created/updating marker, old & new coordinate, mapView and bearing value from backend
         *  to turn properly. Here coordinates json files is used without new bearing value. So that
         *  bearing won't work as expected.
         */
        [self.moveMent arCarMovementWithMarker:pickUpMarker oldCoordinate:prevCoOrdinate newCoordinate:newCoordinate mapView:self.mapView bearing:0];  //instead value 0, pass latest bearing value from backend
        
        prevCoOrdinate = newCoordinate;
        
    
    }
}

#pragma mark - ARCarMovementDelegate
- (void)arCarMovementMoved:(GMSMarker * _Nonnull)Marker {
    pickUpMarker = Marker;
    pickUpMarker.map = self.mapView;
    
    //animation to make car icon in center of the mapview
    
//    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:pickUpMarker.position zoom:15.0f];
//
     GMSCameraUpdate *updatedCamera = [Helper focusMapToShowAllMarkers:markerArr];
    [self.mapView animateWithCameraUpdate:updatedCamera];
}

#pragma mark- General Functions

-(void)initialSetUp
{
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
       
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
     _topNavbarView.delegate = self;
    [self.mapView addObserver:self
                   forKeyPath:@"selectedMarker"
                      options:(NSKeyValueObservingOptionNew |
                               NSKeyValueObservingOptionOld)
                      context:NULL];
    
    [_topNavbarView showNavbackBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Ride_Details", nil) capitalizedString]];
    _customerImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    _customerImageView.layer.borderWidth = 1.2;
    
    
}

-(void)setLocalization
{
    _lblFromTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"From", nil)];
    _lblToTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"To", nil)];
    [_btnCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [_btnReached setTitle:NSLocalizedString(@"Reached", nil) forState:UIControlStateNormal];
    [_btnEndTrip setTitle:NSLocalizedString(@"End_Trip", nil) forState:UIControlStateNormal];
    
}

-(void)setRideDetatilsdata
{
    _addressObj.pickUpAddress = [rideDetailsDic objectForKey:@"pickupAddress"];
    _addressObj.dropOffAddress = [rideDetailsDic objectForKey:@"dropoffAddress"];
    _addressObj.pickUpLatitude = @([[rideDetailsDic objectForKey:@"pickupLatitude"] doubleValue]);
    _addressObj.pickUpLongitude = @([[rideDetailsDic objectForKey:@"pickupLongitude"] doubleValue]);
    _addressObj.dropOffLatitude = @([[rideDetailsDic objectForKey:@"dropoffLatitude"] doubleValue]);
    _addressObj.dropOffLongitude = @([[rideDetailsDic objectForKey:@"dropoffLongitude"] doubleValue]);
    
    
    [_customerImageView sd_setImageWithURL:[NSURL URLWithString:[rideDetailsDic objectForKey:@"riderImageURL"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    _lblCustomerName.text = [rideDetailsDic objectForKey:@"riderName"];
    
    [self setInitialData];
    [self initialMapSetUp];
    [self setScreenShowWithDriverStatus];
    
}

-(void)setInitialData{
    
    
    //Final destination Lat, Long save start trip action start
    dropOffLat = _addressObj.dropOffLatitude;
    dropOffLong = _addressObj.dropOffLongitude;
    
    //At first driver destination location will be user's pick location.After start trip destination will be reset to final destination .
    
    _addressObj.dropOffLatitude = _addressObj.pickUpLatitude;
    _addressObj.dropOffLongitude = _addressObj.pickUpLongitude;
    
    //Pickupltitude before start trip will be current  location
    
     [self currentLocationSetToPickUpPoint];
    
    _lblFromLocation.text = _addressObj.pickUpAddress;
    _lblToLocation.text = _addressObj.dropOffAddress;
    

}

-(void)setScreenShowWithDriverStatus
{
     [self startTimer];
    
    if([self isDriverStatusExpired]){
        [GlobalInfo showTaxiDashboardScreen];
        [Helper showAlertWithTitle:NSLocalizedString(@"Ride_Expired_Alert",nil) Message:@""];
    }
   else  if ([self isDriverReachedBeforeStart])
    {
        [self driverStatusReachedChanges];
    }
    else if ([self isDriverTripStarted])
    {
        [self driverStatusStartTripChanges];
    }
}

-(BOOL)isDriverStatusExpired{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Expired])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverReachedBeforeStart{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Reached])
    {
        return true;
    }
    return false;
}
-(BOOL)isDriverTripStarted{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Started])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverJustAcceptedRequest{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Accepted])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverRejectedTrip{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Rejected])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverEndTrip{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Completed])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverStatusPending{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Pending])
    {
        return true;
    }
    return false;
}

-(BOOL)isRiderCancelledTrip{
    if ([_driverCurrentStatus isEqualToString:RiderStatus_Cancelled])
    {
        return true;
    }
    return false;
}

-(BOOL)isDriverCancelledTrip{
    if ([_driverCurrentStatus isEqualToString:DriverStatus_Cancelled])
    {
        return true;
    }
    return false;
}



-(void)driverStatusReachedChanges
{
 
    customerPickUpMarker.snippet = nil;
    [markerArr addObject:dropOffMarker];
    [self setMapWithinBound];
    [self clearAllPreviousPolyLines];
    [_btnReached setTitle:NSLocalizedString(@"Start_Trip", nil) forState:UIControlStateNormal];
}

-(void)driverStatusStartTripChanges{
    [self currentLocationSetToPickUpPoint];
    
    _addressObj.dropOffLatitude = dropOffLat;
    _addressObj.dropOffLongitude = dropOffLong;
    [_mapView clear];
  
    [markerArr removeAllObjects];

    [self addMarkerToMapViewAtCameraPosition];
    
    [_viewEndTrip setHidden:false];
    [_viewReachedStartTrip setHidden:true];
    isContinousLocationTrackStart = false;
    [self startTimer];
}

#pragma mark- map Action

-(void)initialMapSetUp
{
    [CATransaction begin];
    [_mapView animateToZoom:20];
    [self.mapView animateToLocation:CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue], [_addressObj.pickUpLongitude doubleValue])];
    
    [CATransaction commit];
    [self addMarkerToMapViewAtCameraPosition];
}

-(void)currentLocationSetToPickUpPoint
{
    _addressObj.pickUpLatitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
    _addressObj.pickUpLongitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
    
}

- (void)addMarkerToMapViewAtCameraPosition{
    //PickUp Marker Set
    
    [self setPickUpMarker];
    
    //Drop Off Marker Set
    
    if([self isDriverJustAcceptedRequest] || [self isDriverReachedBeforeStart])
    {
        customerPickUpMarker = [[GMSMarker alloc] init];
        CGRect frame1 = CGRectMake(0, 0, 23,23);
        UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:frame1];
        imageView1.image = [UIImage imageNamed:@"Location_Round_Blue_marker"];
        customerPickUpMarker.iconView = imageView1;
        customerPickUpMarker.appearAnimation = kGMSMarkerAnimationPop;
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        CLLocationCoordinate2D customerPickUpPosition  = CLLocationCoordinate2DMake([_addressObj.dropOffLatitude doubleValue],[_addressObj.dropOffLongitude doubleValue]);
        
        customerPickUpMarker.layer.latitude = customerPickUpPosition.latitude;
        customerPickUpMarker.layer.longitude = customerPickUpPosition.longitude;
        customerPickUpMarker.map = _mapView;
        if([self isDriverJustAcceptedRequest]){
            [_mapView setSelectedMarker:customerPickUpMarker];
        }
        [markerArr addObject:customerPickUpMarker];
        [CATransaction commit];
    }
    
    dropOffMarker = [[GMSMarker alloc] init];
    CGRect frame = CGRectMake(0, 0, 23,33);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [UIImage imageNamed:@"Location_Red_marker"];
    dropOffMarker.iconView = imageView;
    dropOffMarker.appearAnimation = kGMSMarkerAnimationPop;
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    CLLocationCoordinate2D dropOffPosition  = CLLocationCoordinate2DMake([dropOffLat doubleValue],[dropOffLong doubleValue]);

    dropOffMarker.layer.latitude = dropOffPosition.latitude;
    dropOffMarker.layer.longitude = dropOffPosition.longitude;
    dropOffMarker.map = _mapView;
   
    if([self isDriverTripStarted])
    {
        [markerArr addObject:dropOffMarker];
        [_mapView setSelectedMarker:dropOffMarker];
    }
    
    [CATransaction commit];
    
    
    NSLog(@"markerArr count addMarkerToMapViewAtCameraPosition %d",markerArr.count);
    
    [self setMapWithinBound];
  //  [self setSelectedMarkerOnMap];
    [self drawRoute];
    
}


-(void)setMapWithinBound
{
    GMSCameraUpdate *update = [Helper focusMapToShowAllMarkers:markerArr];
    [_mapView animateWithCameraUpdate:update];
    
}

-(void)setPickUpMarker
{
    pickUpMarker.map  = nil;
    [markerArr removeObject:pickUpMarker];
    pickUpMarker = [[GMSMarker alloc] init];
    CGRect frame = CGRectMake(0, 0, 16,35);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [UIImage imageNamed:@"Car_icon"];
    pickUpMarker.iconView = imageView;
    // pickUpMarker.appearAnimation = kGMSMarkerAnimationPop;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    CLLocationCoordinate2D pickUpPosition  = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
    
    pickUpMarker.layer.latitude = pickUpPosition.latitude;
    pickUpMarker.layer.longitude = pickUpPosition.longitude;
    pickUpMarker.map = _mapView;
    
    [markerArr addObject:pickUpMarker];
    [CATransaction commit];
}

#pragma mark- Key value Observer

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([keyPath isEqual:@"selectedMarker"]) {
        if ([[change objectForKey:NSKeyValueChangeNewKey] isKindOfClass:[NSNull class]]) {
            self.mapView.selectedMarker = [change objectForKey:NSKeyValueChangeOldKey];
        }
    }
}

#pragma mark- API Call

- (void)callGetRideDetailsServiceWithIsFirstLoad:(BOOL)isFirstLoad{
    
    NSDictionary *param = @{@"bookingId":_bookingID
                            };
    
    NSLog(@"bookingId Driver TripStartVC %@",param);
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDriverGetRideDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                     
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       if (statusCode == 200)
                                                       {
                                                           
                                                           rideDetailsDic = [responseObject objectForKey:@"responseData"];
                                                           NSLog(@"rideDetailsDic Driver TripStartVC %@",rideDetailsDic);
                                                           _driverCurrentStatus = [rideDetailsDic objectForKey:@"status"];
                                                           if(isFirstLoad){
                                                               [self setRideDetatilsdata];
                                                           }
                                                           else{
                                                               
                                                               if([self isDriverStatusExpired]){
                                                                   [GlobalInfo showTaxiDashboardScreen];
                                                                   [Helper showAlertWithTitle:NSLocalizedString(@"Ride_Expired_Alert",nil) Message:@""];
                                                               }
                                                              else  if([self isDriverReachedBeforeStart])
                                                               {
                                                                   
                                                                   [self driverStatusReachedChanges];
                                                               }
                                                               else if([self isDriverTripStarted])
                                                               {
                                                                   [self driverStatusStartTripChanges];
                                                               }
                                                               else if([self isDriverEndTrip])
                                                               {
                                                                   [self showTripEndPopUp];
                                                               }
                                                               else if([self isDriverRejectedTrip] || [self isDriverStatusPending] || [self isRiderCancelledTrip] ||  [self  isDriverCancelledTrip]){
                                                                   if([self isRiderCancelledTrip]){
                                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Customer_Cancel_Ride_AlertMsg", nil)];
                                                                   }
                                                                   else if([self isDriverCancelledTrip]){
                                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Driver_Cancel_Ride_AlertMsg", nil)];
                                                                   }
                                                                   [GlobalInfo showTaxiDashboardScreen];
                                                                   
                                                               }
                                                               
                                                           }
                                                       }
                                                       else  if (statusCode == 600) {
                                                           
                                                           [GlobalInfo showLoginScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       else if(statusCode == 250){
                                                               [GlobalInfo showTaxiDashboardScreen];
                                                               [Helper showAlertWithTitle:Empty Message:message];
                                                           }
                                                       else{
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}




- (void)callDriverAcceptGetRideDetailsServiceWithRiderStatus:(NSString*)riderStatus andCaller:(NSString*)caller isEndTrip:(BOOL)isEndTrip{
    
    NSLog(@"callDriverAcceptService");
    
    NSDictionary *param = @{@"bookingId":_bookingID,
                            @"rideStatus" :riderStatus,
                            @"caller" : caller
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDriverUpdateRideStatus
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       if (statusCode == 200)
                                                       {
                                                           if(isEndTrip)
                                                               [self showTripEndPopUp];
                                                           else
                                                               [self callGetRideDetailsServiceWithIsFirstLoad:false];
                                                           
                                                       }
                                                       else  if (statusCode == 600) {
                                                           
                                                           [GlobalInfo showLoginScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       else if(statusCode == 250 || statusCode == 251 || statusCode == 604){
                                                           [GlobalInfo showTaxiDashboardScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       else{
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



#pragma mark- Button Action

- (IBAction)onCustomerCallBtnClicked:(id)sender {
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[rideDetailsDic objectForKey:@"riderPhoneNo"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (IBAction)onReachedStartTripBtnClicked:(id)sender {
   
    if([self isDriverJustAcceptedRequest])
    {
       
        [self callDriverAcceptGetRideDetailsServiceWithRiderStatus:DriverStatus_Reached andCaller:DriverCaller_RideReached isEndTrip:false];
    }
    else
    {
       
          [self callDriverAcceptGetRideDetailsServiceWithRiderStatus:DriverStatus_Started andCaller:DriverCaller_RideStart isEndTrip:false];
    }
}

- (IBAction)onCancelBtnClicked:(id)sender {
    
    RideCancelReasonVC *Obj = [[RideCancelReasonVC alloc] initWithNibName:@"RideCancelReasonVC" bundle:nil];
    Obj.isDriverCancelRide = YES;
    Obj.bookingID = _bookingID;
    Obj.currentRideStatus = _driverCurrentStatus;
    self.definesPresentationContext = YES; //self is presenting view controller
    Obj.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    Obj.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:Obj animated:YES completion:nil];
}

- (IBAction)onEndTripBtnClicked:(id)sender {
    
     [self callDriverAcceptGetRideDetailsServiceWithRiderStatus:DriverStatus_Completed andCaller:DriverCaller_RideComplete isEndTrip:true];
    
}

#pragma mark- Timer Action

-(void)startTimer
{
    if(!driverContinuousMoveCarTimer)
    {
         driverContinuousMoveCarTimer =  [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(updateLocation:) userInfo:nil repeats:YES];
    }
}

-(void)stopTimer
{
    if ([driverContinuousMoveCarTimer isValid]) {
        [driverContinuousMoveCarTimer invalidate];
    }
    driverContinuousMoveCarTimer = nil;
}



#pragma mark- Draw Route
- (void)drawRoute
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue,^{
        [self callPolyLineGoogleMapServiceWithcompletionHandler:^(GMSPolyline *polyline) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // do work here
                if(polyline)
                {
                    [self clearAllPreviousPolyLines];
                    if(![self isDriverReachedBeforeStart])
                    {
                        [oldPolylines addObject:polyline];
                        polyline.map = self.mapView;
                    }
                    if(!isContinousLocationTrackStart)
                    {
                        isContinousLocationTrackStart = YES;
                    }
                    
                }
            });
            
        }];
        
    });
    
}

-(void)clearAllPreviousPolyLines
{
    if(oldPolylines.count > 0)
    {
        for(int i = 0 ;i<oldPolylines.count ;i++ ) {
            GMSPolyline *polyLine = [oldPolylines objectAtIndex:i];
            polyLine.map = nil;
        }
    }
}


- (void)callPolyLineGoogleMapServiceWithcompletionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%@,%@", _addressObj.pickUpLatitude, _addressObj.pickUpLongitude];
    NSString *destinationString = [NSString stringWithFormat:@"%@,%@", _addressObj.dropOffLatitude, _addressObj.dropOffLongitude];
    
    NSDictionary *param = @{@"originString":originString,@"destinationString":destinationString
                            ,@"HideHUD":@"YES"};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGoogleMapDirectionApi
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"RESPONSEOBJECT DIRECTION API %@", responseObject);
                                                       
                                                       NSArray *routesArray = [responseObject objectForKey:@"routes"];
                                                       
                                                       
                                                       
                                                       if ([routesArray count] > 0)
                                                       {
                                                           NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                           if([self isDriverJustAcceptedRequest])
                                                           {
                                                               customerPickUpMarker.snippet = [Helper calculateTimeFromDirectionAPI:routeDict withIsDriver:YES];
                                                           }
                                                           else
                                                           {
                                                             dropOffMarker.snippet = [Helper calculateTimeFromDirectionAPI:routeDict withIsDriver:YES];
                                                           }
                                                           NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                           NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                           GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                           mainPolyline = [GMSPolyline polylineWithPath:path];
                                                           [mainPolyline setStrokeWidth:2.0];
                                                           [mainPolyline setStrokeColor:[UIColor blackColor]];
                                                           
                                                       }
                                                       
                                                       if(completionHandler)
                                                           completionHandler(mainPolyline);
                                                    
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                     
                                                   }];
    
}


#pragma mark - SharePopUpDismiss

-(void)showTripEndPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"TripEndedAlert_Title", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"TripEndedAlert_Desc", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}
-(void)dismissPopUp
{
    [GlobalInfo showTaxiDashboardScreen];
}

#pragma mark - topNavActionDelegate

-(void)backButtonClicked{
    
    if( [_driverCurrentStatus isEqualToString:DriverStatus_Accepted] || [_driverCurrentStatus isEqualToString:DriverStatus_Reached] || [_driverCurrentStatus isEqualToString:DriverStatus_Started]){
        [GlobalInfo showHomeScreen];
    }
    else{
        [GlobalInfo showTaxiDashboardScreen];
    }
    
    
}

@end
