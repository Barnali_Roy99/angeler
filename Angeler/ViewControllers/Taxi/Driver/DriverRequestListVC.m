//
//  DriverRequestListVC.m
//  Angeler
//
//  Created by AJAY on 06/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "DriverRequestListVC.h"
#import "DriverRequestListCell.h"
#import "DriverRequestDetailsVC.h"

static NSString *CellIdentifier = @"DriverRequestListCell";

@interface DriverRequestListVC ()
{
        NSInteger START_POSITION;
        BOOL moreRecordsAvailable;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblRequestList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, strong) NSMutableArray *arrDriverReqList;

@end

@implementation DriverRequestListVC

#pragma mark- View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    [self callServiceToLoadDriverRequestList];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:true];
}

#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadDriverRequestList];
    [refreshController endRefreshing];
}


#pragma mark- General Functions

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
      
    }
 
    [_topNavbarView showNavbackBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"New_Driver_Request", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Driver_Request_Msg", nil);
    [_tblRequestList registerNib:[UINib nibWithNibName:@"DriverRequestListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblRequestList.estimatedRowHeight = 20;
    _tblRequestList.rowHeight = UITableViewAutomaticDimension;
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblRequestList addSubview:refreshController];

  
}

#pragma mark - Webservice Call Method

- (void)callServiceToLoadDriverRequestList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDriverRequestList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       if ([GlobalInfo checkStatusSuccess:responseObject])  {
                                                           
                                                           if (_arrDriverReqList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrDriverReqList removeAllObjects];
                                                               }else if(_arrDriverReqList.count > START_POSITION){
                                                                   [_arrDriverReqList removeObjectsInRange:NSMakeRange(START_POSITION, _arrDriverReqList.count - START_POSITION)];
                                                               }
                                                               [_arrDriverReqList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrDriverReqList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           _lblNoRecord.hidden = _arrDriverReqList.count > 0;
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601)
                                                           {
                                                               _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           }
                                                           [_tblRequestList reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}



#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrDriverReqList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DriverRequestListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
     NSDictionary *data = [_arrDriverReqList objectAtIndex:indexPath.row];
    
    cell.lblFromLocation.text = [data objectForKey:@"pickupAddress"];
    cell.lblToLocation.text =[data objectForKey:@"dropoffAddress"];

    UITapGestureRecognizer *detailsViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(detailsViewTapped:)];
    [cell.detailsView setUserInteractionEnabled:YES];
    [cell.detailsView addGestureRecognizer:detailsViewTap];
    
    cell.detailsView.tag = indexPath.row + 1;
    
    NSLog(@"indexPath.row = %d" , indexPath.row);
    NSLog(@"moreRecordsAvailable = %d" , moreRecordsAvailable);
    //Load More Data
    
//    if (indexPath.row == [self.arrDriverReqList count] - 1 && moreRecordsAvailable)
//    {
//        [self callServiceToLoadItineraryList];
//    }
    
    
    return cell;
}



- (void)detailsViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *detailsView = (UIView *)sender.view;
    [self redirectToDriverAcceptScreen:[[_arrDriverReqList objectAtIndex:detailsView.tag - 1] objectForKey:@"bookingId"]];
}

-(void)redirectToDriverAcceptScreen:(NSString*)bookingID
{
    NSLog(@"redirectToDriverAcceptScreen called");
    DriverRequestDetailsVC *Obj = [[DriverRequestDetailsVC alloc] initWithNibName:@"DriverRequestDetailsVC" bundle:nil];
    Obj.bookingID = bookingID;
    [self.navigationController pushViewController:Obj animated:YES];
}

#pragma mark - Scrollview Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
//    NSLog(@"currentOffset %f",currentOffset);
    NSLog(@"maximumOffset currentOffset %f %f",maximumOffset,currentOffset);
    
    //NSInteger result = maximumOffset - currentOffset;

    CGFloat offsetDifference = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (offsetDifference <= 100.0) {
        if(moreRecordsAvailable)
        {
            NSLog(@"API Called");
            [self callServiceToLoadDriverRequestList];
        }
    }

}

@end
