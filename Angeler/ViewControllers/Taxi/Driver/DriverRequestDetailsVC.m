//
//  DriverRequestDetailsVC.m
//  Angeler
//
//  Created by AJAY on 06/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//



#import "DriverRequestDetailsVC.h"
#import "DriverTripStartVC.h"


@interface DriverRequestDetailsVC ()<ARCarMovementDelegate>
{
    NSMutableArray *markerArr,*oldPolylines;;
    GMSPolyline *mainPolyline;
    GMSMarker *dropOffMarker,*pickUpMarker;
    BOOL isContinousLocationTrackStart;
    CLLocationCoordinate2D prevCoOrdinate;
    NSTimer *driverContinuousMoveCarTimer;
    
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *lblFromTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblToTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFromLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblToLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acceptCancelViewHeightConstraint;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, strong)  PickUpDropOffAddressModel *addressObj;
@property (strong, nonatomic) ARCarMovement *moveMent;
@property (nonatomic, strong) NSDictionary *detailsData;

@end

@implementation DriverRequestDetailsVC
@synthesize detailsData,bookingID;

#pragma mark- View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForegroundCalled:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    [self initialSetUp];
    [self initialDataSetUp];
    [self initialMapSetUp];
    [self setLocalization];
    if(![Helper isLocationServiceDenied])
    [self callGetRideDetailsService];
    
  
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
   [Helper showLocationServiceGoToSettingsAlert];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [self stopTimer];
}

#pragma mark - Notification
-(void)appEnterForegroundCalled:(NSNotification *) notification
{
    NSLog(@"appEnterForegroundCalled");
    [Helper showLocationServiceGoToSettingsAlert];
    //  [[AppDel shareModel] startMonitoringLocation];
    
     if(![Helper isLocationServiceDenied])
    [self performSelector:@selector(initialMapSetUp) withObject:nil afterDelay:2.0];
    
}

#pragma mark - Continiuous Location Track

- (void)updateLocation:(id)sender{
    if(isContinousLocationTrackStart)
    {
        NSLog(@"Location change Continuous tracking called");
        prevCoOrdinate  = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
        [self currentLocationSetToPickUpPoint];
        
        [self drawRoute];
        
        CLLocationCoordinate2D newCoordinate = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
        
        
        /**
         *  You need to pass the created/updating marker, old & new coordinate, mapView and bearing value from backend
         *  to turn properly. Here coordinates json files is used without new bearing value. So that
         *  bearing won't work as expected.
         */
        [self.moveMent arCarMovementWithMarker:pickUpMarker oldCoordinate:prevCoOrdinate newCoordinate:newCoordinate mapView:self.mapView bearing:0];  //instead value 0, pass latest bearing value from backend
        
        prevCoOrdinate = newCoordinate;
        
        //        [self setPickUpMarker];
        //    [self setMapWithinBound];
    }
}

#pragma mark - ARCarMovementDelegate
- (void)arCarMovementMoved:(GMSMarker * _Nonnull)Marker {
    pickUpMarker = Marker;
    pickUpMarker.map = self.mapView;
    
    //animation to make car icon in center of the mapview
    
    //    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:pickUpMarker.position zoom:15.0f];
    //
    GMSCameraUpdate *updatedCamera = [Helper focusMapToShowAllMarkers:markerArr];
    [self.mapView animateWithCameraUpdate:updatedCamera];
}

#pragma mark- Key value Observer

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([keyPath isEqual:@"selectedMarker"]) {
        if ([[change objectForKey:NSKeyValueChangeNewKey] isKindOfClass:[NSNull class]]) {
            self.mapView.selectedMarker = [change objectForKey:NSKeyValueChangeOldKey];
        }
    }
}

#pragma mark- General Functions

-(void)initialSetUp
{
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
        _acceptCancelViewHeightConstraint.constant = 60;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        _acceptCancelViewHeightConstraint.constant = 40;
    }
    
    [_topNavbarView showNavbackBtn];
    
    
    [self.topNavbarView setTitle:[NSLocalizedString(@"Ride_Details", nil) capitalizedString]];
    
    
    markerArr = [[NSMutableArray alloc] init];
    oldPolylines = [[NSMutableArray alloc]init];
}

-(void)initialDataSetUp{
    [self.mapView addObserver:self
                   forKeyPath:@"selectedMarker"
                      options:(NSKeyValueObservingOptionNew |
                               NSKeyValueObservingOptionOld)
                      context:NULL];
    
    self.moveMent = [[ARCarMovement alloc]init];
    self.moveMent.delegate = self;
    
    _addressObj = [[PickUpDropOffAddressModel alloc] init];
    [self currentLocationSetToPickUpPoint];
   
}

-(void)setRideDetails{
    [self startTimer];
    _lblFromLocation.text = [detailsData objectForKey:@"pickupAddress"];
    _lblToLocation.text = [detailsData objectForKey:@"dropoffAddress"];
    
  
    _addressObj.dropOffLatitude = @([[detailsData objectForKey:@"pickupLatitude"] doubleValue]);
    _addressObj.dropOffLongitude = @([[detailsData objectForKey:@"pickupLongitude"] doubleValue]);
    
    _addressObj.pickUpAddress = _lblFromLocation.text;
    _addressObj.dropOffAddress = _lblToLocation.text;
    [self initialMapSetUp];
    [self addMarkerToMapViewAtCameraPosition];
}

-(void)setLocalization
{
    _lblFromTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"From", nil)];
    _lblToTitle.text = [NSString stringWithFormat:@"%@ :", NSLocalizedString(@"To", nil)];
    [_btnCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [_btnAccept setTitle:NSLocalizedString(@"Accept", nil) forState:UIControlStateNormal];
}



#pragma mark- map Action

-(void)initialMapSetUp
{
    //    [_mapView setMyLocationEnabled:YES];
    //    _mapView.settings.myLocationButton = true;
    
    [CATransaction begin];
    [_mapView animateToZoom:30];
    [self.mapView animateToLocation:CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue], [_addressObj.pickUpLongitude doubleValue])];
    
    [CATransaction commit];
    
}

-(void)currentLocationSetToPickUpPoint
{
    _addressObj.pickUpLatitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
    _addressObj.pickUpLongitude = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
}


- (void)addMarkerToMapViewAtCameraPosition{
    //PickUp Marker Set
    
    pickUpMarker = [[GMSMarker alloc] init];
    CGRect frame = CGRectMake(0, 0, 16,35);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.image = [UIImage imageNamed:@"Car_icon"];
    pickUpMarker.iconView = imageView;
    pickUpMarker.appearAnimation = kGMSMarkerAnimationPop;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    CLLocationCoordinate2D pickUpPosition  = CLLocationCoordinate2DMake([_addressObj.pickUpLatitude doubleValue],[_addressObj.pickUpLongitude doubleValue]);
    //Driver Current location
    
    pickUpMarker.layer.latitude = pickUpPosition.latitude;
    pickUpMarker.layer.longitude = pickUpPosition.longitude;
    pickUpMarker.map = _mapView;
    
    [markerArr addObject:pickUpMarker];
    [CATransaction commit];
    
    //Drop Off Marker Set
    
    dropOffMarker = [[GMSMarker alloc] init];
    CGRect frame1 = CGRectMake(0, 0, 23,23);
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:frame1];
    imageView1.image = [UIImage imageNamed:@"Location_Round_Blue_marker"];
    dropOffMarker.iconView = imageView1;
    dropOffMarker.appearAnimation = kGMSMarkerAnimationPop;
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    CLLocationCoordinate2D dropOffPosition  = CLLocationCoordinate2DMake([_addressObj.dropOffLatitude doubleValue],[_addressObj.dropOffLongitude doubleValue]);//Customer Pick Up location
    
    dropOffMarker.layer.latitude = dropOffPosition.latitude;
    dropOffMarker.layer.longitude = dropOffPosition.longitude;
    
    
    // dropOffMarker.infoWindowAnchor = CGPointMake(2.5, 0);
    dropOffMarker.map = _mapView;
    [_mapView setSelectedMarker:dropOffMarker];
    
    [markerArr addObject:dropOffMarker];
    [CATransaction commit];
    
    NSLog(@"markerArr count %@",markerArr);
    
    GMSCameraUpdate *update = [Helper focusMapToShowAllMarkers:markerArr];
    [_mapView animateWithCameraUpdate:update];
    [self drawRoute];
    
}

#pragma mark- Draw Route
- (void)drawRoute
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue,^{
        [self callPolyLineGoogleMapServiceWithcompletionHandler:^(GMSPolyline *polyline) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // do work here
                if(polyline)
                {
                    [self clearAllPreviousPolyLines];
                    [oldPolylines addObject:polyline];
                    polyline.map = self.mapView;
                    if(!isContinousLocationTrackStart)
                    {
                        isContinousLocationTrackStart = YES;
                    }
                    
                }
            });
            
        }];
        
    });
    
}

-(void)clearAllPreviousPolyLines
{
    if(oldPolylines.count > 0)
    {
        for(int i = 0 ;i<oldPolylines.count ;i++ ) {
            GMSPolyline *polyLine = [oldPolylines objectAtIndex:i];
            polyLine.map = nil;
        }
    }
}

- (void)callPolyLineGoogleMapServiceWithcompletionHandler:(void (^)(GMSPolyline *))completionHandler
{
    //Route draw from Driver current location -> Customer Pick up Location
    
    NSString *originString = [NSString stringWithFormat:@"%@,%@", _addressObj.pickUpLatitude, _addressObj.pickUpLongitude];
    NSString *destinationString = [NSString stringWithFormat:@"%@,%@", _addressObj.dropOffLatitude, _addressObj.dropOffLongitude];
    
    NSDictionary *param = @{@"originString":originString,@"destinationString":destinationString,@"HideHUD":@"YES"};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGoogleMapDirectionApi
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"RESPONSEOBJECT DIRECTION API %@", responseObject);
                                                       
                                                       NSArray *routesArray = [responseObject objectForKey:@"routes"];
                                                       
                                                       
                                                       if ([routesArray count] > 0)
                                                       {
                                                           NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                           dropOffMarker.snippet = [Helper calculateTimeFromDirectionAPI:routeDict withIsDriver:YES];
                                                           NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                           NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                           GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                           mainPolyline = [GMSPolyline polylineWithPath:path];
                                                           [mainPolyline setStrokeWidth:2.0];
                                                           [mainPolyline setStrokeColor:[UIColor blackColor]];
                                                           
                                                       }
                                                       
                                                       // run completionHandler on main thread
                                                       //                                                       dispatch_sync(dispatch_get_main_queue(), ^{
                                                       if(completionHandler)
                                                           completionHandler(mainPolyline);
                                                       // }
                                                       //);
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                      
                                                   }];
    
}
#pragma mark- Button Action

- (IBAction)onCancelBtnClicked:(id)sender {
    
    [self showCancelRideAlert];
}

- (IBAction)onAcceptBtnClicked:(id)sender {
     [self callAcceptCancelRideDetailsServiceWithRideStatus:DriverStatus_Accepted andcaller:DriverCaller_RideReq];
    
}

-(void)showCancelRideAlert
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:NSLocalizedString(@"Cancel_Ride_Alert", nil)
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   [self callAcceptCancelRideDetailsServiceWithRideStatus:DriverStatus_Rejected andcaller:DriverCaller_RideReq];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       /** What we write here???????? **/
                                       NSLog(@"you pressed No, thanks button");
                                       // call method whatever u need
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)redirectToStartTripScreen
{
    DriverTripStartVC *Obj = [[DriverTripStartVC alloc] initWithNibName:@"DriverTripStartVC" bundle:nil];
    Obj.bookingID = [detailsData objectForKey:@"bookingId"];
    [self.navigationController pushViewController:Obj animated:YES];
}
#pragma mark - Webservice Call

- (void)callAcceptCancelRideDetailsServiceWithRideStatus:(NSString*)rideStatus andcaller:(NSString*)caller{

    NSLog(@"callDriverAcceptService");

    NSDictionary *param = @{@"bookingId":[detailsData objectForKey:@"bookingId"],
                            @"rideStatus" : rideStatus,
                            @"caller" : caller
                            };

    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDriverUpdateRideStatus
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       
                                                       // if ([GlobalInfo checkStatusSuccess:responseObject]){
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       
                                                       if (statusCode == 200)
                                                       {
                                                           
                                                           NSString *currentRideStatus = [[responseObject objectForKey:@"responseData"] objectForKey:@"currentRideStatus"];
                                                           
                                                           if([currentRideStatus isEqualToString:DriverStatus_Rejected] || [currentRideStatus isEqualToString:DriverStatus_Completed] || [currentRideStatus isEqualToString:DriverStatus_Expired] || [currentRideStatus isEqualToString:RiderStatus_Cancelled] || [currentRideStatus isEqualToString:DriverStatus_Cancelled]){
                                                               if([currentRideStatus isEqualToString:DriverStatus_Rejected]){
                                                                   [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Request_Rejected_Successfully", nil)];
                                                               }
                                                               [GlobalInfo showTaxiDashboardScreen];
                                                           }
                                                           else if([currentRideStatus isEqualToString:DriverStatus_Accepted] || [currentRideStatus isEqualToString:DriverStatus_Reached] || [currentRideStatus isEqualToString:DriverStatus_Started]){
                                                               
                                                               [self redirectToStartTripScreen];
                                                               
                                                           }
                                                       }
                                                       else  if (statusCode == 600) {
                                                           
                                                           [GlobalInfo showLoginScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       //
                                                       
//                                                       else if(statusCode == 250 || statusCode == 251 || statusCode == 604){
//                                                           [GlobalInfo showTaxiDashboardScreen];
//                                                           [Helper showAlertWithTitle:Empty Message:message];
//                                                           //
//                                                       }
                                                       else
                                                       {
                                                            [GlobalInfo showTaxiDashboardScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


- (void)callGetRideDetailsService{
    
    NSLog(@"callDriverAcceptService");
    
    NSDictionary *param = @{@"bookingId":bookingID
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDriverGetRideDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       
                                                       if (statusCode == 200)
                                                       {
                                                       
                                                     //  if ([GlobalInfo checkStatusSuccess:responseObject]){
                                                           
                                                           detailsData = [responseObject objectForKey:@"responseData"];
                                                          
                                                           [self setRideDetails];
                                                           if([[detailsData objectForKey:@"status"] isEqualToString:DriverStatus_Rejected] || [[detailsData objectForKey:@"status"] isEqualToString:DriverStatus_Completed] || [[detailsData objectForKey:@"status"] isEqualToString:DriverStatus_Expired] || [[detailsData objectForKey:@"status"] isEqualToString:RiderStatus_Cancelled] || [[detailsData objectForKey:@"status"] isEqualToString:DriverStatus_Cancelled]){
                                                               [GlobalInfo showTaxiDashboardScreen];
                                                           }
                                                           else if([[detailsData objectForKey:@"status"] isEqualToString:DriverStatus_Accepted] || [[detailsData objectForKey:@"status"] isEqualToString:DriverStatus_Reached] || [[detailsData objectForKey:@"status"] isEqualToString:DriverStatus_Started]){
                                                               
                                                               [self redirectToStartTripScreen];
                                                               
                                                           }
                                                           
                                                       }
                                                       else  if (statusCode == 600) {
                                                           
                                                           [GlobalInfo showLoginScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       else
                                                       {
                                                            [GlobalInfo showTaxiDashboardScreen];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       
                                                       
                                                   }
                                                       
     
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

#pragma mark- Timer Action

-(void)startTimer
{
    if(!driverContinuousMoveCarTimer)
    {
        driverContinuousMoveCarTimer =  [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(updateLocation:) userInfo:nil repeats:YES];
    }
}

-(void)stopTimer
{
    if ([driverContinuousMoveCarTimer isValid]) {
        [driverContinuousMoveCarTimer invalidate];
    }
    driverContinuousMoveCarTimer = nil;
}

@end
