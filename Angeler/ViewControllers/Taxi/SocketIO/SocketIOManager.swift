//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by AJAY on 27/03/19.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit
import SocketIO


@objc @objcMembers class SocketIOManager: NSObject {
   
    
    // swiftSharedInstance is  accessible from ObjC
//       class var swiftSharedInstance: SocketIOManager? {
//           struct Socket {
//               static let instance = SocketIOManager()
//           }
//           return Socket.instance
//       }
    
    
    private static var privateShared : SocketIOManager?

    class func swiftSharedInstance() -> SocketIOManager { // change class to final to prevent override
        guard let sharedObj = privateShared else {
            privateShared = SocketIOManager()
            return privateShared!
        }
        return sharedObj
    }
  

    let manager = SocketManager(socketURL: URL(string: "http://angelerapp.com:2020")!, config: [.log(true), .compress])
   
 public var socket: SocketIOClient
    
    
    private override init() {
      socket  = manager.defaultSocket
      super.init()
        
//          DispatchQueue.main.async {
//        if var topController = UIApplication.shared.keyWindow?.rootViewController {
//            while let presentedViewController = topController.presentedViewController {
//                topController = presentedViewController
//
//
//                let alertController = UIAlertController.init(title: "init singleton", message: "", preferredStyle: .alert)
//                let actionOK = UIAlertAction.init(title: "OK", style: .default) { (UIAlertAction) in
//
//                        topController.dismiss(animated: true, completion: nil)
//
//                }
//                alertController.addAction(actionOK)
//                alertController.show(topController, sender:topController)
//
//            }
//
//            }
//        }
//
        print("init singleton")
    }
    
    deinit {
        
//        DispatchQueue.main.async {
//
//        if var topController = UIApplication.shared.keyWindow?.rootViewController {
//                   while let presentedViewController = topController.presentedViewController {
//                       topController = presentedViewController
//
//
//                       let alertController = UIAlertController.init(title: "deinit singleton", message: "", preferredStyle: .alert)
//                       let actionOK = UIAlertAction.init(title: "OK", style: .default) { (UIAlertAction) in
//
//                               topController.dismiss(animated: true, completion: nil)
//
//                       }
//                       alertController.addAction(actionOK)
//                       alertController.show(topController, sender:topController)
//
//                   }
//
//
//               }
//        }
        
        print("deinit singleton")
    }
    
    func establishConnection(sessionID:String,userID:String) {

        let paramdic = ["sessionId":sessionID,"userId":userID]
       print("paramdic establishConnection \(paramdic)")
        self.manager.config = SocketIOClientConfiguration(arrayLiteral: .connectParams(paramdic), .secure(false) , .reconnects(true)
               )
               socket.connect()

        socket.on("disconnect") { (newMSG, ack) in
                   print("Get disconnect New Message = \(newMSG)")
               }

        
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    

    func updateDriverCurrentLocation(locationDetails:NSDictionary){
        
        if(isSocketConnected)(){
            socket.emit("last_location_update",locationDetails)
            
        }
    }
    
    func removeUserLogout(sessionID:String,userID:String) {

    let paramdic = ["sessionId":sessionID,"userId":userID]
        print("paramdic removeUserLogout \(paramdic)")
        if(isSocketConnected)(){
            socket.emit("remove",paramdic)
            SocketIOManager.privateShared = nil
        }
    }
    
    func getDriverLocationDetailsViaSocket() {
        
        if(isSocketConnected)(){
            socket.on("driver_location_details") { (dataArray, socketAck) -> Void in
                print("driver_location_details socket response \(dataArray)")
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CustomerTrackTripRiderGetDriverCurrentLocation"), object: nil, userInfo: dataArray[0] as? [String: AnyObject])
            }
        }
        else{
            print("Socket Not Connected getDriverLocationDetailsViaSocket")
        }
        
    }
    
    func isSocketConnected() -> Bool{
        let socketConnectionStatus = SocketIOManager.swiftSharedInstance().socket.status
        
        switch socketConnectionStatus {
        case SocketIOStatus.connected:
            print("socket.ioo connected g")
            return true
            
        case SocketIOStatus.connecting:
            print("socket.ioo connecting g")
             return true
           
        case SocketIOStatus.disconnected:
            print("socket.ioo disconnected g")
             return false

        case SocketIOStatus.notConnected:
            print("socket.ioo not connected g")
             return false
         
            
        }
    }

    
}
