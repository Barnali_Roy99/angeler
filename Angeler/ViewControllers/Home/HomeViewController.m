//
//  HomeViewController.m
//  Angeler
//
//  Created by AJAY on 22/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "HomeViewController.h"
#import "ManageDeliveryPointViewController.h"
#import "DeliveryPointViewController.h"
#import "MyTripsListViewController.h"
#import "DeliveryListController.h"

#import "MyShipmentsListController.h"
#import "AddShipmentsStepOneController.h"
#import <Crashlytics/Crashlytics.h>
#import "HomeCollectionViewCell.h"
#import "TaxiDashboardVC.h"
#import "DriverTripStartVC.h"
#import "CustomerTrackTrip.h"
#import "CharityListVCViewController.h"

#import "DriverVerificationVC.h"
#import "DutyFreeDashboradVC.h"

#import "LocalDeliveryDashboradVC.h"

static NSString * const homeCollectionCellIdentifier = @"HomeCollectionViewCellID";

#define kEventName @"EventName"
#define kEventIcon @"EventImage"

@interface HomeViewController ()
{
    CGFloat maximumDynamicHeightPerRow;
    NSString *foodURL;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic,readwrite) NSArray *arrEventChoiceData;

@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@end

@implementation HomeViewController
@synthesize topBarHeightConstraint,topNavbarView,showEventChoiceCollectionView;


#pragma mark- View Life Cycle



- (void)viewDidLoad {
    [super viewDidLoad];
    [self callApiDashboardInitialization];
    [self initialSetUp];
    if([Helper isAngelerStatusVerified] && [Helper isDriverStatusVerified]){
      [Helper showLocationServiceGoToSettingsAlert];
    }
    
 
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_isSignUp)
    {
        [self showPopUp];
       
    }
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}

#pragma mark - Notification
-(void)appEnterForegroundCalled:(NSNotification *) notification
{
    NSLog(@"appEnterForegroundCalled");
    
}


#pragma mark- General Functions

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavMenuBtn];
    [topNavbarView showTitleLogo];
    //   [topNavbarView showNotificationBtn];
    
    [self.showEventChoiceCollectionView registerNib:[UINib nibWithNibName:@"HomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:homeCollectionCellIdentifier];
    self.showEventChoiceCollectionView.backgroundColor = [UIColor clearColor];
    
    
    //Array Data Set Up
    
    _arrEventChoiceData = [NSArray arrayWithObjects:
                           @"",
                           @{kEventName:[NSLocalizedString(@"Share_Angeler", nil) uppercaseString],
                             kEventIcon:@"Home_Share"},
                           @"",
                           @{kEventName:[NSLocalizedString(@"My Trips", nil) uppercaseString],
                             kEventIcon:@"Home_MyTrips"},
                           @{kEventName:[NSLocalizedString(@"My Shipments", nil) uppercaseString],
                             kEventIcon:@"Home_MyShipments"},
                           
                           @{kEventName:[NSLocalizedString(@"My Delivery", nil) uppercaseString],
                             kEventIcon:@"Home_MyDelivery"},
                           
                           @{kEventName:[NSLocalizedString(@"Angeler_Storage", nil) uppercaseString],
                             kEventIcon:@"Home_Storage"},
                           
                           @{kEventName:[NSLocalizedString(@"Angeler_Duty_Free", nil) uppercaseString],
                             kEventIcon:@"Home_DutyFree"},
                           
                           @{kEventName:[NSLocalizedString( @"Angeler_Trip", nil) uppercaseString],
                             kEventIcon:@"Home_AngelerTrip"},
                           
                           @{kEventName:[NSLocalizedString( @"Angeler_Taxi", nil) uppercaseString],
                             kEventIcon:@"Home_Angeler_Taxi"},
                           
                           @{kEventName:[NSLocalizedString( @"Angeler_Car_Pooling", nil) uppercaseString],
                             kEventIcon:@"Home_Angeler_Carpooling"},
                           @{kEventName:[NSLocalizedString( @"Angeler_Food", nil) uppercaseString],
                             kEventIcon:@"Home_Angeler_Food"},
                           @{kEventName:[NSLocalizedString( @"Angeler_Ship", nil) uppercaseString],
                             kEventIcon:@"Home_Angeler_Ship"},
                           @{kEventName:[NSLocalizedString( @"Angeler_Truck", nil) uppercaseString],
                             kEventIcon:@"Home_Angeler_Truck"},
                           @{kEventName:[NSLocalizedString( @"Angeler_Charity", nil) uppercaseString],
                             kEventIcon:@"Home_Angeler_Charity"},
                           @{kEventName:[NSLocalizedString( @"Angeler_Local_Delivery", nil) uppercaseString],
                             kEventIcon:@"Home_AngelerTrip"},nil];
    
    
    
    //Localization
    
    _lblTitle.text = NSLocalizedString(@"Be_Paid_To_Travel", nil);
    _lblDesc.text = NSLocalizedString(@"Sender_Easier", nil);
    //Angeler_Local_Delivery
}

-(void)redirectTotaxiDashboardScreen{
    TaxiDashboardVC *Obj = [[TaxiDashboardVC alloc] initWithNibName:@"TaxiDashboardVC" bundle:nil];
    Obj.isFromTaxidashboard = true;
    [self.navigationController pushViewController:Obj animated:YES];
}

-(void)redirectToDriverStartTripScreen:(NSString*)driverBookingID
{
    DriverTripStartVC *Obj = [[DriverTripStartVC alloc] initWithNibName:@"DriverTripStartVC" bundle:nil];
    Obj.bookingID = driverBookingID;
    [self.navigationController pushViewController:Obj animated:YES];
}

-(void)redirectToCustomerTrackScreen:(NSString*)bookingID
{
    CustomerTrackTrip *Obj = [[CustomerTrackTrip alloc] initWithNibName:@"CustomerTrackTrip" bundle:nil];
    Obj.bookingID = bookingID;
    [self.navigationController pushViewController:Obj animated:YES];
}

-(void)redirectToTaxiScreenWithStatusCheck:(NSDictionary*)dataDic {
    if([[dataDic objectForKey:@"driverStatus"] isEqualToString:DriverRiderStatusFree] && [[dataDic objectForKey:@"riderStatus"] isEqualToString:DriverRiderStatusFree])
    {
        [self redirectTotaxiDashboardScreen];
    }
    else if([[dataDic objectForKey:@"driverStatus"] isEqualToString:DriverRiderStatusEngaged])
    {
        [self redirectToDriverStartTripScreen:[dataDic objectForKey:@"bookingId"]];
    }
    else if([[dataDic objectForKey:@"riderStatus"] isEqualToString:DriverRiderStatusEngaged])
    {
        [self redirectToCustomerTrackScreen:[dataDic objectForKey:@"bookingId"]];
    }
}

-(void)redirectToDutyFreeDashBoard
{
    DutyFreeDashboradVC *Obj = [[DutyFreeDashboradVC alloc] initWithNibName:@"DutyFreeDashboradVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}


-(void)redirectToFoodURL{
    NSURL *url = [NSURL URLWithString:foodURL];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark : API Call
- (void)callApiDashboardInitialization
{
    
    NSDictionary *param = [[NSDictionary alloc] init];
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserHomeDashboardInitialization
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
                                                           
                                                           [[GlobalInfo sharedInfo] setUserVerificationStatusText:[dataDict objectForKey:@"verifiedStatusText"]];
                                                           [[GlobalInfo sharedInfo] setUserAngelerStatus:[dataDict objectForKey:@"angelerStatus"]];
                                                           [[GlobalInfo sharedInfo] setTaxiDriverVerificationStatusText:[dataDict objectForKey:@"driverVerifiedStatusText"]];
                                                           [[GlobalInfo sharedInfo] setTaxiDriverVerificationStatus:[dataDict objectForKey:@"driverStatus"]];
                                                           foodURL = [dataDict objectForKey:@"anglerFoodUrl"];
                                                           
                                                           BOOL isRideExist = [[dataDict objectForKey:@"isCustomerBookingExists"] boolValue];
                                                           NSLog(@"isRideExist %b",isRideExist);
                                                           [self openSocketwithRideEXist:isRideExist];
                                                                          
                                                                       
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

-(void)openSocketwithRideEXist:(BOOL)isOngoingRideExist{
    
    //Socket not connected
    if(![[SocketIOManager swiftSharedInstance] isSocketConnected]){
        if([Helper isAngelerStatusVerified]){
            if([Helper isDriverStatusVerified]){
                //Driver end
                [[SocketIOManager swiftSharedInstance] establishConnectionWithSessionID:[[GlobalInfo sharedInfo] sessionId] userID:[[GlobalInfo sharedInfo] userId]];
            }
            else if(isOngoingRideExist){
                //Customer End
                
                [[SocketIOManager swiftSharedInstance] establishConnectionWithSessionID:[[GlobalInfo sharedInfo] sessionId] userID:[[GlobalInfo sharedInfo] userId]];
            }
        }
    }
    else {
        
        
        if([Helper isAngelerStatusVerified]){
            if([Helper isDriverStatusVerified]){
                // For driver end will close socket on logout
            }
            else if(!isOngoingRideExist){
                
                //Customer End if ride not exist socket will close
                
                [[SocketIOManager swiftSharedInstance] removeUserLogoutWithSessionID:[[GlobalInfo sharedInfo] sessionId] userID:[[GlobalInfo sharedInfo] userId]];
                [[SocketIOManager swiftSharedInstance] closeConnection];
            }
        }
        
        
    }
}

- (void)callApiToGetTaxiDriverRiderStatus
{
    
    NSDictionary *param = [[NSDictionary alloc] init];
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGetTaxiDeriverRiderStatus
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
                                                         
                                                           [self redirectToTaxiScreenWithStatusCheck:dataDict];
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



#pragma mark : ShowPopUp

-(void)showPopUp{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
   
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Welcome", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"SignUp_Complete_Message_PopUp", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"OK", nil);

    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];

    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showComingSoonPopUp
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.headerTitleText = @"";
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"ComingSoon_Alert_Title", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText = NSLocalizedString(@"OK", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
}

#pragma mark : Collection View Delegate Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrEventChoiceData.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat collectionViewWidth = collectionView.frame.size.width / 3;
    
  //  CGFloat expectedHeight;
//    if(indexPath.row == 0 || indexPath.row == 2)
//    {
//        expectedHeight = collectionViewWidth;
//    }
//    else
//    {
//
//
//        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:13.0];
//        expectedHeight = collectionViewWidth + [Helper heightForLableWithText:[[_arrEventChoiceData objectAtIndex:indexPath.row] objectForKey:kEventName] font:font width:collectionViewWidth] + 5;
//    }
    
    if (indexPath.row % 3 == 0)
    {
      //  NSLog(@"collectionViewWidth Row number %d",indexPath.row);
        [self maximumExpectedHeightWithRowNumber:indexPath.row andCollectionViewWidth:collectionViewWidth];
    }
    
    
   // NSLog(@"collectionViewWidth %f collectionViewHeight %f",collectionViewWidth,maximumDynamicHeightPerRow);
    
  
    return CGSizeMake(collectionViewWidth, maximumDynamicHeightPerRow);
}


    
    

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:homeCollectionCellIdentifier forIndexPath:indexPath];
    
//    cell.layer.borderColor = [[UIColor blueColor] CGColor];
//    cell.layer.borderWidth = 2.0;
    
    if(indexPath.row == 0 || indexPath.row == 2)
    {
        cell.titlelabel.text = @"";
        cell.logoImageView.image = [UIImage imageNamed:@""];
    }
    
    else
    {
    
        if(indexPath.row == 1){
            [cell.titlelabel setTextColor:[UIColor appThemeYellowColor]];
        }
        else{
             [cell.titlelabel setTextColor:[UIColor whiteColor]];
        }
    cell.titlelabel.text = [[_arrEventChoiceData objectAtIndex:indexPath.row] objectForKey:kEventName];
    cell.logoImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[_arrEventChoiceData objectAtIndex:indexPath.row]objectForKey:kEventIcon]]];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!(indexPath.row == 0 || indexPath.row == 2))
    {
        
        NSString *eventName = [[_arrEventChoiceData objectAtIndex:indexPath.row] objectForKey:kEventName];
        
        if ([eventName isEqualToString:[NSLocalizedString(@"Share_Angeler", nil) uppercaseString]]){
            [self showShareApplink];
        }
        
        else if (indexPath.row == 3){
            MyTripsListViewController *managObj = [[MyTripsListViewController alloc] initWithNibName:@"MyTripsListViewController" bundle:nil];
            AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
        }
        else if (indexPath.row == 4){
            MyShipmentsListController *managObj = [[MyShipmentsListController alloc] initWithNibName:@"MyShipmentsListController" bundle:nil];
            AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
        }
        else if (indexPath.row == 5){
            DeliveryListController *homeObj = [[DeliveryListController alloc] initWithNibName:@"DeliveryListController" bundle:nil];
            AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeObj];
        }
        else if (indexPath.row == 7){
            [self redirectToDutyFreeDashBoard];
        }
        else if (indexPath.row == 9){
           [self callApiToGetTaxiDriverRiderStatus];
        }
        else if (indexPath.row == 11){
            [self redirectToFoodURL];
        }
        else if (indexPath.row == 14){
            CharityListVCViewController *Obj = [[CharityListVCViewController alloc] initWithNibName:@"CharityListVCViewController" bundle:nil];
            [self.navigationController pushViewController:Obj animated:YES];
        }
        else if (indexPath.row == _arrEventChoiceData.count - 1){
            LocalDeliveryDashboradVC *Obj = [[LocalDeliveryDashboradVC alloc] initWithNibName:@"LocalDeliveryDashboradVC" bundle:nil];
            [self.navigationController pushViewController:Obj animated:true];
        }
        else {
            
            [self showComingSoonPopUp];
        }
    }
    
}

-(void)maximumExpectedHeightWithRowNumber:(int)rowNumber andCollectionViewWidth:(CGFloat)collectionViewWidth
{
    CGFloat expectedHeight;
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:13.0];
    NSString *labelText = @"";
    
    if(rowNumber == 0 || rowNumber == 2)
    {
        labelText = @"";
        
    }
    else
    {
        labelText = [[_arrEventChoiceData objectAtIndex:rowNumber] objectForKey:kEventName];
    }
    
    CGFloat firstCellHeight = (collectionViewWidth * 0.9) * 0.9 + [Helper heightForLableWithText:labelText font:font width:collectionViewWidth] + 10 ;
    
    
    if ((rowNumber + 1) < _arrEventChoiceData.count)
    {
        if(rowNumber + 1 == 0 || rowNumber + 1 == 2)
        {
            labelText = @"";
            
        }
        else
        {
            labelText = [[_arrEventChoiceData objectAtIndex:rowNumber + 1] objectForKey:kEventName];
        }
        
        CGFloat secondCellHeight = (collectionViewWidth * 0.9) * 0.9 + [Helper heightForLableWithText:labelText font:font width:collectionViewWidth] + 10;
        
        CGFloat largerHeight = firstCellHeight > secondCellHeight ? firstCellHeight : secondCellHeight;
        
        if ((rowNumber + 2) < _arrEventChoiceData.count)
        {
            if(rowNumber + 2 == 0 || rowNumber + 2 == 2)
            {
                labelText = @"";
                
            }
            else
            {
                labelText = [[_arrEventChoiceData objectAtIndex:rowNumber + 2] objectForKey:kEventName];
            }
            
            CGFloat thirdCellHeight = (collectionViewWidth * 0.9) * 0.9 + [Helper heightForLableWithText:labelText font:font width:collectionViewWidth] + 10;
            
            CGFloat largestHeight = thirdCellHeight > largerHeight ? thirdCellHeight : largerHeight;
            expectedHeight = largestHeight;
        }
        else
        {
            expectedHeight = largerHeight;
        }
        
    }
    else
    {
        expectedHeight = firstCellHeight;
    }
    
    maximumDynamicHeightPerRow = expectedHeight;
   
}

-(void)showShareApplink
{
    NSString *shareData=[NSString stringWithFormat:NSLocalizedString(@"Share_Link_Title", nil),@""];
    
    NSArray *objectsToShare = @[shareData];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//            DriverVerificationVC *Obj = [[DriverVerificationVC alloc] initWithNibName:@"DriverVerificationVC" bundle:nil];
//            [self.navigationController pushViewController:Obj animated:YES];

@end
