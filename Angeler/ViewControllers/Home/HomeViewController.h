//
//  HomeViewController.h
//  Angeler
//
//  Created by AJAY on 22/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SharePopUpViewController.h"

@interface HomeViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (assign, nonatomic)BOOL isSignUp;
@property (weak, nonatomic) IBOutlet UICollectionView *showEventChoiceCollectionView;

@end
