//
//  ChooseAngelerStepThreeController.m
//  Angeler
//
//  Created by AJAY on 13/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ChooseAngelerStepThreeController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "MyShipmentsListController.h"
#import "AddShipmentsStepOneController.h"
#import "SharePopUpDoubleBtnActionVC.h"
#import "MWPhotoBrowser.h"
#import "SharePopUpViewController.h"
#import "WalletViewController.h"

@interface ChooseAngelerStepThreeController ()<GMSMapViewDelegate,SharePopUpDoubleBtnActionDismissDelegate,MWPhotoBrowserDelegate,SharePopUpDismissDelegate>
{
    NSString *userFirsatName;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollContent;
@property (weak, nonatomic) IBOutlet UIView *
headerView;


@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UIView *imgUserProfileBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffCost;
@property (nonatomic, strong) NSMutableArray *arrPhotos;

@property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransport;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblArrivalAdreess;
@property (weak, nonatomic) IBOutlet UILabel *lblTypeOfPackage;

@property (weak, nonatomic) IBOutlet UILabel *lblModeOfTransportTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureAddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblArrivalAdreessTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTypeOfPackageTitle;

@property (weak, nonatomic) IBOutlet GMSMapView *viFromLocation;
@property (weak, nonatomic) IBOutlet GMSMapView *viToLocation;

@property (weak, nonatomic) IBOutlet UIView *BookAngelerView;
@property (weak, nonatomic) IBOutlet UILabel *lblBook;


@property (weak, nonatomic) IBOutlet UILabel *lblPickUpCity;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpDateTime;

@property (weak, nonatomic) IBOutlet UILabel *lblDropOffCity;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffDateTime;

@property (weak, nonatomic) IBOutlet UIImageView *departureMeetingPointPremiumImageView;
@property (weak, nonatomic) IBOutlet UIImageView *arrivalMeetingPointPremiumImageView;

@property (weak, nonatomic) IBOutlet UILabel *lblPickUpTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffTitle;

@property (nonatomic, strong) NSDictionary *fetchData;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceTitle;



@end

@implementation ChooseAngelerStepThreeController
@synthesize topNavbarView,topBarHeightConstraint;

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"meetingPointPremiumServiceCost %@",_meetingPointPremiumServiceCost);
    [self initialSetUp];
    [self callApiToLoadDetails];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [_imgUserProfileBackgroundView makeRound];
    [_imgUserProfile makeRound];
    
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:NSLocalizedString(@"Angeler_Details", nil)];
    
    [_BookAngelerView makeCornerRound:15.0];
    [_BookAngelerView dropShadowWithColor:[UIColor lightishGrayColor]];
    
    [_viFromLocation makeCornerRound:15.0];
    [_viToLocation makeCornerRound:15.0];
    
    
    _lblModeOfTransportTitle.text = @"";
    //NSLocalizedString(@"Flight", nil);
    
    _lblPriceTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Price", nil)];
    _lblBook.text = [NSLocalizedString(@"Book", nil) uppercaseString];
     _lblPickUpTitle.text = NSLocalizedString(@"Pick_Up", nil);
     _lblDropOffTitle.text = NSLocalizedString(@"Drop_Off", nil);
    
    _viToLocation.delegate = self;
    _viFromLocation.delegate = self;
    
    
}

-(void)setGesture
{
     UITapGestureRecognizer *bookAngelerViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bookAngelerViewTapped:)];
    [_BookAngelerView addGestureRecognizer:bookAngelerViewTapRecognizer];
}

- (void)ShowDetailsWithData:(NSDictionary*)data{
    
    //Profile Bio
    
    
    NSArray *userNamearr = [[data objectForKey:@"angelerName"] componentsSeparatedByString:@" "];
    userFirsatName = userNamearr.firstObject;
    
    _lblPickUpCity.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"departureCityName"]];
    _lblPickUpCountry.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"departureCountryName"]];
    _lblPickUpDateTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"departureDate"] Time:[data objectForKey:@"departureTime"]];
    
    
    _lblDropOffCity.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"arrivalCityName"]];
    _lblDropOffCountry.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"arrivalCountryName"]];
    _lblDropOffDateTime.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"arrivalDate"] Time:[data objectForKey:@"arrivalTime"]];
    
    
    
    _lblUserName.text = [data objectForKey:@"angelerName"];
    _lblTypeOfPackageTitle.text = [NSString stringWithFormat:NSLocalizedString(@"Can_Carry", nil),[data objectForKey:@"angelerName"]];
    [_imgUserProfile sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    
    _imgUserProfile.layer.borderColor = [[UIColor blackColor] CGColor];
   _imgUserProfile.layer.borderWidth = 1.0;
    [_imgUserProfile setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *angelerImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(angelerImageViewTapped:)];
    angelerImageViewTap.numberOfTapsRequired = 1;
    [_imgUserProfile addGestureRecognizer:angelerImageViewTap];
    self.arrPhotos = [[NSMutableArray alloc] init];
    [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]]]];
    
   
    NSString *packageFromLat,*packageFromLong,*packageFromAddress;
    NSString *packageToLat,*packageToLong,*packageToAddress;
    
    if (_isMeetingPoint)
    {
        packageFromAddress = [data objectForKey:@"pickupMPAddress"];
        packageToAddress = [data objectForKey:@"dropoffMPAddress"];
        packageFromLat = [data objectForKey:@"pickupMPLatitude"];
        packageFromLong = [data objectForKey:@"pickupMPLongitude"];
        packageToLat = [data objectForKey:@"dropoffMPLatitude"];
        packageToLong = [data objectForKey:@"dropoffMPLongitude"];
        [_departureMeetingPointPremiumImageView setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
        [_arrivalMeetingPointPremiumImageView setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
        
       
     
    }
    else
    {
        packageFromAddress = [data objectForKey:@"fromAddress"];
        packageToAddress = [data objectForKey:@"toAddress"];
        packageFromLat = [data objectForKey:@"fromAddressLatitude"];
        packageFromLong = [data objectForKey:@"fromAddressLongitude"];
        packageToLat = [data objectForKey:@"toAddressLatitude"];
        packageToLong = [data objectForKey:@"toAddressLongitude"];
        [_departureMeetingPointPremiumImageView setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
        [_arrivalMeetingPointPremiumImageView setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];

    
    }
    
    _lblDropOffCost.text = [NSString stringWithFormat:@"%@ %@",_meetingPointPremiumServiceCost,NSLocalizedString(@"Credits", nil)];
    
    _lblDepartureAddress.text = packageFromAddress;
    _lblArrivalAdreess.text = packageToAddress;
    
    //Middle Info
    
    _lblModeOfTransport.text = [NSString stringWithFormat:@"%@ - %@",[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]],[data objectForKey:@"travelRefNo"]];
    

    _lblTypeOfPackage.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];
    
   
    
    
    
    [self loadMapWithAngelerLatVal:[[data objectForKey:@"angelerPickupLatitude"] doubleValue] angLongVal:[[data objectForKey:@"angelerPickupLongitude"] doubleValue] angAddress:[data objectForKey:@"angelerPickupAddress"] fromAdd:YES packageLatVal:[packageFromLat doubleValue] packLongVal:[packageFromLong doubleValue] packAddress:packageFromAddress];
    
    [self loadMapWithAngelerLatVal:[[data objectForKey:@"angelerDropoffLatitude"] doubleValue] angLongVal:[[data objectForKey:@"angelerDropoffLongitude"] doubleValue] angAddress:[data objectForKey:@"angelerDropoffAddress"] fromAdd:NO packageLatVal:[packageToLat doubleValue] packLongVal:[packageToLong doubleValue] packAddress:packageToAddress];
    
}

#pragma mark - Gesture Action

- (void)angelerImageViewTapped:(UITapGestureRecognizer*)sender{
    
    
    [self setMWPHOtoBrowser];
}

- (void)bookAngelerViewTapped:(UITapGestureRecognizer*)sender
{
    if (!_fetchData) {
        return;
    }
    
    NSDictionary *param = @{@"itineraryId":[_anglerData objectForKey:@"itineraryId"],
                            @"itineraryServiceId":[_anglerData objectForKey:@"itineraryServiceId"],
                            @"packageId":_packageId,
                            @"angelerPickupAddress":[_fetchData objectForKey:@"angelerPickupAddress"],
                            @"angelerPickupLatitude":[_fetchData objectForKey:@"angelerPickupLatitude"],
                            @"angelerPickupLongitude":[_fetchData objectForKey:@"angelerPickupLongitude"],
                            @"angelerDropoffAddress":[_fetchData objectForKey:@"angelerDropoffAddress"],
                            @"angelerDropoffLatitude":[_fetchData objectForKey:@"angelerDropoffLatitude"],
                            @"angelerDropoffLongitude":[_fetchData objectForKey:@"angelerDropoffLongitude"],
                             @"serviceCost":_meetingPointPremiumServiceCost
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPostAngelerData
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       
                                                       
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 245)
                                                           {
                                                               [self showCreditNotSufficientPopUpWithMessage:[[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"]];
                                                           }
                                                           else
                                                           {
                                                               
                                                               [self showAngelerBookedSuccessPopUp];
                                                           }
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
    
}

#pragma mark - Webservice Call

- (void)callApiToLoadDetails{
    //package/chosen_itinerary_details
    /*http://angeler.serveraddress.com/web_services/package/chosen_itinerary_details?userId=16&sessionId=dca60139618378764cd71e513ecbed4b14567abd&os=Android&osVersion=M&appVersion=1.0&itineraryId=50&packageId=6&itineraryServiceId=26*/
    
    NSDictionary *param = @{@"itineraryId":[_anglerData objectForKey:@"itineraryId"],
                            @"itineraryServiceId":[_anglerData objectForKey:@"itineraryServiceId"],
                            @"packageId":_packageId
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kAngelerData
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
              if ([GlobalInfo checkStatusSuccess:responseObject]){
                   _fetchData = [responseObject objectForKey:@"responseData"];
                   [self ShowDetailsWithData:[responseObject objectForKey:@"responseData"]];
               }
           }
           failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
           }];
    
}

#pragma mark - Mapview Load

- (void)loadMapWithAngelerLatVal:(double)angLatVal angLongVal:(double)angLongVal angAddress:(NSString*)angAddress fromAdd:(BOOL)fromAdd packageLatVal:(double)packLatVal packLongVal:(double)packLongVal packAddress:(NSString*)packAddress{
   
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:angLatVal longitude:angLongVal zoom:15];
    NSMutableArray *markerArr = [[NSMutableArray alloc] init];
    
//    GMSMarker *markerAng = [[GMSMarker alloc] init];
//    markerAng.position = CLLocationCoordinate2DMake(angLatVal,angLongVal);
//    markerAng.icon = [UIImage imageNamed:@"choose_angeler_marker"];
//    markerAng.title = angAddress;
//    markerAng.zIndex = 100;
    
   // markerAng.appearAnimation = kGMSMarkerAnimationPop;
    
    GMSMarker *markerPack = [[GMSMarker alloc] init];
    markerPack.position = CLLocationCoordinate2DMake(packLatVal,packLongVal);
    markerPack.icon = [UIImage imageNamed:@"LocationPin"];
    markerPack.title = packAddress;
 //   markerPack.appearAnimation = kGMSMarkerAnimationPop;
    
 //   [markerArr addObject:markerAng];
    [markerArr addObject:markerPack];
    
    CLLocationCoordinate2D myLocation = ((GMSMarker *)markerArr.firstObject).position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    for (GMSMarker *marker in markerArr)
        bounds = [bounds includingCoordinate:marker.position];
    
    
    if (fromAdd) {
        _viFromLocation.camera = camera;
      //  markerAng.map = _viFromLocation;
        markerPack.map = _viFromLocation;
        
        [_viFromLocation setSelectedMarker:markerPack];
        [_viFromLocation animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
        
    }else{
        _viToLocation.camera = camera;
     //   markerAng.map = _viToLocation;
        markerPack.map = _viToLocation;
        
        [_viToLocation setSelectedMarker:markerPack];
        [_viToLocation animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
        }
    
}





#pragma mark - SharePopUpDoubleBtnActionDismissDelegate

- (void)dismissPopUpLeftBtnAction
{
    AddShipmentsStepOneController *delObj = [[AddShipmentsStepOneController alloc] initWithNibName:@"AddShipmentsStepOneController" bundle:nil];
    [self.navigationController pushViewController:delObj animated:YES];
}
- (void)dismissPopUpRightBtnAction
{
    MyShipmentsListController *myShipmentObj = [[MyShipmentsListController alloc] initWithNibName:@"MyShipmentsListController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:myShipmentObj];
}

#pragma mark - Show PopUp
-(void)showCreditNotSufficientPopUpWithMessage:(NSString*)responseMessage{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Credit_Insufficient", nil);
    sharePopUpVC.headerDescriptionText = responseMessage;
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"Add_Credit", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

-(void)showAngelerBookedSuccessPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpDoubleBtnActionVC *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpDoubleBtnVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Congratulation", nil);
    sharePopUpVC.headerMiddleTitleText = [NSString stringWithFormat:NSLocalizedString(@"You_Just_Booked", nil), userFirsatName];
    sharePopUpVC.headerDescriptionText = [NSString stringWithFormat:NSLocalizedString(@"Angeler_Book_Desc_Msg", nil),userFirsatName ];
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.leftBtnActionitleText = NSLocalizedString(@"Add_new_Shipment", nil);
    sharePopUpVC.rightBtnActionitleText = NSLocalizedString(@"View_Shipments", nil);
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    WalletViewController *walletObj = [[WalletViewController alloc] initWithNibName:@"WalletViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:walletObj];
}


#pragma mark - MWPhotoBrowser

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


//========================== old + Reuse Code =============================

/*
 
 - (IBAction)backTapped:(id)sender{
 [self.navigationController popViewControllerAnimated:YES];
 }
 - (IBAction)bookingTapped:(id)sender {
 if (!_fetchData) {
 return;
 }
 
 //http://angeler.serveraddress.com/web_services/package/book_itinerary_service?userId=16&sessionId=dca60139618378764cd71e513ecbed4b14567abd&os=Android&osVersion=M&appVersion=1.0&itineraryId=50&packageId=6&itineraryServiceId=26&angelerPickupAddress=India%20-%20pickup%20Address&angelerPickupLatitude=22.324234&angelerPickupLongitude=88.3242432&angelerDropoffAddress=Singapore%20-%20drop%20off%20address&angelerDropoffLatitude=1.545654&angelerDropoffLongitude=103.4543543
 
 
 NSDictionary *param = @{@"itineraryId":[_anglerData objectForKey:@"itineraryId"],
 @"itineraryServiceId":[_anglerData objectForKey:@"itineraryServiceId"],
 @"packageId":_packageId,
 @"angelerPickupAddress":[_fetchData objectForKey:@"angelerPickupAddress"],
 @"angelerPickupLatitude":[_fetchData objectForKey:@"angelerPickupLatitude"],
 @"angelerPickupLongitude":[_fetchData objectForKey:@"angelerPickupLongitude"],
 @"angelerDropoffAddress":[_fetchData objectForKey:@"angelerDropoffAddress"],
 @"angelerDropoffLatitude":[_fetchData objectForKey:@"angelerDropoffLatitude"],
 @"angelerDropoffLongitude":[_fetchData objectForKey:@"angelerDropoffLongitude"]
 };
 
 
 //=======Call WebService Engine========
 [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPostAngelerData
 Method:REQMETHOD_POST
 parameters:[GlobalInfo getParmWithSession:param]
 media:nil
 viewController:self
 success:^(id responseObject){
 NSLog(@"response ===   %@",responseObject);
 if ([GlobalInfo checkStatusSuccess:responseObject]) {
 ChooseAngelerPopUpController *popUp = [[ChooseAngelerPopUpController alloc] initWithNibName:@"ChooseAngelerPopUpController" bundle:nil];
 popUp.bookingMessage = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
 [self addChildViewController:popUp];
 popUp.view.frame = self.view.frame;
 [self.view addSubview:popUp.view];
 }
 }
 failure:^(id responseObject){
 [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
 }];
 
 
 } */

/*  _lblDepartureDateTime.text = [NSString stringWithFormat:@"%@, %@",[data objectForKey:@"departureDate"],[data objectForKey:@"departureTime"]];
 _lblArrivalDateTime.text = [NSString stringWithFormat:@"%@, %@",[data objectForKey:@"arrivalDate"],[data objectForKey:@"arrivalTime"]];
 
 
 _lblTransportRefNo.text = [data objectForKey:@"travelRefNo"];
 
 _lblRegistrationDate.text = [data objectForKey:@"angelerRegnDate"];
 _lblCost.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"totalCost"]];
 
 NSInteger rating = [[data objectForKey:@"angelerRating"] integerValue];
 UIImage *starFill = [UIImage imageNamed:@"Choose_angeler_star_fill"];
 UIImage *starEmpty = [UIImage imageNamed:@"Choose_angeler_star_empty"];
 
 for (int i = 1; i <[_viRating subviews].count ; i++) {
 UIImageView *imgV = [_viRating viewWithTag:i];
 imgV.image = rating>=i?starFill:starEmpty;
 }
 
 
 
 
 _lblWeight.text = [data objectForKey:@"packageMaxWeight"];
 _lblHeight.text = [data objectForKey:@"packageHeightRange"];
 _lblWidth.text = [data objectForKey:@"packageWidthRange"];
 _lblLength.text = [data objectForKey:@"packageLengthRange"];*/

/*- (IBAction)ChooseLater:(id)sender{
 [self.navigationController popToRootViewControllerAnimated:YES];
 }
 
 - (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
 //NSLog(@"fff 11 ==  %@",NSStringFromCGPoint(scrollView.contentOffset));
 if (scrollView.contentOffset.y == 0) {
 [scrollView setContentOffset:CGPointMake(0, -20) animated:YES];
 }
 }
 */
/* // Do any additional setup after loading the view from its nib.
 
 [Helper addShadowToLayer:_headerView.layer];*/

/*@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDateTime;
 @property (weak, nonatomic) IBOutlet UILabel *lblArrivalDateTime;
 
 @property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNo;
 
 @property (weak, nonatomic) IBOutlet UILabel *lblRegistrationDate;
 @property (weak, nonatomic) IBOutlet UILabel *lblCost;
 @property (weak, nonatomic) IBOutlet UIView *viRating;
 
 
 @property (weak, nonatomic) IBOutlet UILabel *lblWeight;
 @property (weak, nonatomic) IBOutlet UILabel *lblHeight;
 @property (weak, nonatomic) IBOutlet UILabel *lblWidth;
 @property (weak, nonatomic) IBOutlet UILabel *lblLength; */
