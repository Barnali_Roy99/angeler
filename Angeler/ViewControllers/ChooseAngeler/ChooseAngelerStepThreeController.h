//
//  ChooseAngelerStepThreeController.h
//  Angeler
//
//  Created by AJAY on 13/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseAngelerStepThreeController : UIViewController
@property (nonatomic, strong) NSDictionary *anglerData;
@property (nonatomic, strong) NSString *packageId;
@property (nonatomic, strong) NSString *meetingPointPremiumServiceCost;
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (assign, nonatomic) BOOL isMeetingPoint;


@end
