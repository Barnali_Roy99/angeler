//
//  ChooseAngelerPopUpController.m
//  Angeler
//
//  Created by AJAY on 07/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//



//============== Now No use of this controller ===============
#import "ChooseAngelerPopUpController.h"

@interface ChooseAngelerPopUpController ()
@property (weak, nonatomic) IBOutlet UILabel *lblBookingNo;
@property (weak, nonatomic) IBOutlet UILabel *lblBookingMessage;

@end

@implementation ChooseAngelerPopUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    _lblBookingNo.text = _bookingNo;
    _lblBookingMessage.text = _bookingMessage;
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)closeTapped:(id)sender {
    [self.view removeFromSuperview];
    [[self.parentViewController navigationController] popToRootViewControllerAnimated:YES];
    [self removeFromParentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
