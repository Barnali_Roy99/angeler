//
//  ChooseAngelerStepTwoController.h
//  Angeler
//
//  Created by AJAY on 06/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseAngelerStepTwoController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) PackageClass *packObj;
@property (nonatomic, strong) AngelerClass *angelerObj;

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *premiumMeetPointImageView;
@property (weak, nonatomic) IBOutlet UILabel *premiumMeetPointPriceLabel;
@property (weak, nonatomic) IBOutlet UIView *PriceView;
@property (weak, nonatomic) IBOutlet UIView *noRecordFoundSmilyView;

@end
