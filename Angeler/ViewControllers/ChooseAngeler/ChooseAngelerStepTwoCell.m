//
//  ChooseAngelerStepTwoCell.m
//  Angeler
//
//  Created by AJAY on 11/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ChooseAngelerStepTwoCell.h"

@implementation ChooseAngelerStepTwoCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    [self.btnView setTitle:[NSLocalizedString(@"View", nil) uppercaseString] forState:UIControlStateNormal];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

//+ (CGFloat)cellHeightForData:(NSDictionary*)data{
//    
////    self.lblPickupAddress.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"pickupAddress"]];
////    self.lblDeliveryAddress.text = [data objectForKey:@"dropoffAddress"];
////    self.lblName.text = [data objectForKey:@"angelerName"];
//    
//    return self.bounds.size.height;
//}

@end
