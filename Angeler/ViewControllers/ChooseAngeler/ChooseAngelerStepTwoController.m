//
//  ChooseAngelerStepTwoController.m
//  Angeler
//
//  Created by AJAY on 06/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ChooseAngelerStepTwoController.h"
#import "ChooseAngelerStepTwoCell.h"
#import "ChooseAngelerStepThreeController.h"



static NSString *CellIdentifier = @"chooseAngelerStep2Cell";


@interface ChooseAngelerStepTwoController ()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (weak, nonatomic) IBOutlet UITableView *tblAngeler;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) NSMutableArray *arrAngeler;
@property (nonatomic) SORT_ORDER sortingOrder;
@property (nonatomic) SORT_TYPE sortingType;

@end

@implementation ChooseAngelerStepTwoController
@synthesize topBarHeightConstraint,topNavbarView,premiumMeetPointImageView,premiumMeetPointPriceLabel;

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self callApiToLoadAngelerList];
}


#pragma mark - General Functions

-(void)initialSetUp
{

    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"Choose_Angeler", nil) capitalizedString]];
    
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    
    _sortingOrder = DECENDING;
    _sortingType = RATE;
    
    [_tblAngeler registerNib:[UINib nibWithNibName:@"ChooseAngelerStepTwoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblAngeler.estimatedRowHeight = 140;
    [_tblAngeler makeCornerRound:15.0];
    [_backgroundView makeCornerRound:15.0];
    _tblAngeler.rowHeight = UITableViewAutomaticDimension;
    [_tblAngeler setNeedsLayout];
    [_tblAngeler layoutIfNeeded];
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblAngeler addSubview:refreshController];
    
    
    if (self.angelerObj.from_Meet_Point_Checked)
    {
        premiumMeetPointPriceLabel.text = _packObj.meetingPointPrice;
        [premiumMeetPointImageView setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
    }
    else
    {
         premiumMeetPointPriceLabel.text = _packObj.premiumPrice;
         [premiumMeetPointImageView setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
    }
    
}

- (NSString*)sortingTypeText{
    if (_sortingType == DELIVERY) return @"Delivery";
    if (_sortingType == PRICE) return @"Cost";
    if (_sortingType == RATE) return @"Rating";
    return Empty;
}
- (NSString*)sortingOrderText{
    if (_sortingOrder == ASCENDING) return @"Asc";
    if (_sortingOrder == DECENDING) return @"Desc";
    return Empty;
}


- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToLoadAngelerList];
    [refreshController endRefreshing];
}

#pragma mark - API Call

- (void)callApiToLoadAngelerList{
    
    
    NSMutableArray *arrPickPoint = [[NSMutableArray alloc] init];
    for (AddressClass *add in self.angelerObj.From_Point_Address_Array) {
        NSDictionary *dict = @{@"address":add.Address,
                               @"id":add.addresId};
        [arrPickPoint addObject:dict];
    }
    
    NSError *error;
    
    NSData *jsonFromData = [NSJSONSerialization dataWithJSONObject:arrPickPoint options:0 error:&error];
    NSString *jsonFromStrin = [[NSString alloc] initWithData:jsonFromData encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonFromStrin);
    
    
    
    NSMutableArray *arrDropPoint = [[NSMutableArray alloc] init];
    for (AddressClass *add in self.angelerObj.to_Point_Address_Array) {
        NSDictionary *dict = @{@"address":add.Address,
                               @"id":add.addresId};
        [arrDropPoint addObject:dict];
    }
    
    NSData *jsonToData = [NSJSONSerialization dataWithJSONObject:arrDropPoint options:0 error:&error];
    NSString *jsonToString = [[NSString alloc] initWithData:jsonToData encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonToString);
    
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"packageId":self.packObj.pack_Id,
                            @"pickupPoint":jsonFromStrin,
                            @"pickupPremium":self.angelerObj.from_Premium_Checked?@"1":@"0",
                            @"pickupMeetingPoint":self.angelerObj.from_Meet_Point_Checked?@"1":@"0",
                            @"dropoffPoint":jsonToString,
                            @"dropoffPremium":self.angelerObj.to_Premium_Checked?@"1":@"0",
                            @"dropoffMeetingPoint":self.angelerObj.to_Meet_Point_Checked?@"1":@"0",
                            @"sortOrder":[self sortingOrderText],
                            @"sortType":[self sortingTypeText]
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kAngelerList
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           _lblNoRecord.text = NSLocalizedString(@"No_Angeler_Found_Text", nil);
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           if (arr.count == 0 && START_POSITION != 0) {
                                                               moreRecordsAvailable = NO;
                                                               return ;
                                                           }
                                                           
                                                           NSString *responseStatus = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUS"] ;
                                                           if ([responseStatus isEqualToString:kResponseSuccess]) {
                                                               
                                                               if (_arrAngeler.count > 0) {
                                                                   if (START_POSITION == 0) {
                                                                       [_arrAngeler removeAllObjects];
                                                                   }else if(_arrAngeler.count > START_POSITION){
                                                                       [_arrAngeler removeObjectsInRange:NSMakeRange(START_POSITION, _arrAngeler.count - START_POSITION)];
                                                                   }
                                                                   [_arrAngeler addObjectsFromArray:arr];
                                                               }else{
                                                                   _arrAngeler = [NSMutableArray arrayWithArray:arr];
                                                               }
                                                               
                                                               START_POSITION = START_POSITION + [arr count];
                                                               
                                                               
                                                               _noRecordFoundSmilyView.hidden = _arrAngeler.count > 0;
                                                               _backgroundView.hidden = _arrAngeler.count > 0;
                                                               _tblAngeler.hidden = _arrAngeler.count <= 0;
//                                                              
//
                                                               [_tblAngeler reloadData];
                                                           }
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
   // return 10;
    return _arrAngeler.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 220;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseAngelerStepTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *data = [_arrAngeler objectAtIndex:indexPath.row];
//    [cell.backgroundProfileView makeRound];
//    [cell.profileImgView makeRound];
    [cell.containerView makeCornerRound:15.0];
     [cell.btnView makeCornerRound:15.0];
    [cell.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    
    NSArray *fullName = [[data objectForKey:@"angelerName"] componentsSeparatedByString:@" "];
    if (fullName.count > 0)
    cell.lblName.text = [fullName objectAtIndex:0];
    else
    cell.lblName.text = @"";
    
    [cell.profileImgView sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
    

    
    cell.fromDateTimeLbl.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"departureDate"] Time:[data objectForKey:@"departureTime"]];
    
    
    
   cell.toDateTimeLbl.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"arrivalDate"] Time:[data objectForKey:@"arrivalTime"]];
    
   
    cell.btnView.tag = indexPath.row + 1;
    [cell.btnView addTarget:self action:@selector(showAngelerDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.fromLocationLbl.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"pickupAddress"]];
    cell.toLocationLbl.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"dropoffAddress"]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //Load More Data
    
    if (indexPath.row == [self.arrAngeler count] - 1 && moreRecordsAvailable)
    {
        [self callApiToLoadAngelerList];
    }
    
    

    return cell;
}




- (void)showAngelerDetails:(UIButton*)sender{
    
    
    ChooseAngelerStepThreeController *next = [[ChooseAngelerStepThreeController alloc] initWithNibName:@"ChooseAngelerStepThreeController" bundle:nil];
    next.anglerData = [_arrAngeler objectAtIndex:sender.tag - 1];
    next.packageId = self.packObj.pack_Id;
    next.isMeetingPoint = self.angelerObj.from_Meet_Point_Checked;
    if(self.angelerObj.from_Meet_Point_Checked)
    {
        next.meetingPointPremiumServiceCost = _packObj.meetingPointPrice;
    }
    else
    {
        next.meetingPointPremiumServiceCost = _packObj.premiumPrice;
    }
    [self.navigationController pushViewController:next animated:YES];
    
}


#pragma mark - Scrollview Delegate

CGPoint _lastContentOffset;

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _lastContentOffset = scrollView.contentOffset;
}


//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//
//    // UITableView only moves in one direction, y axis
//    CGFloat currentOffset = scrollView.contentOffset.y;
//    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//
//    //NSInteger result = maximumOffset - currentOffset;
//
//    // Change 10.0 to adjust the distance from bottom
//    if (maximumOffset - currentOffset <= 5.0) {
//        if (_lastContentOffset.y < scrollView.contentOffset.y && moreRecordsAvailable) {
//            NSLog(@"Scrolled Down");
//            [self callApiToLoadAngelerList];
//        }
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


//================old code===================

/*//    cell.lblPickupAddress.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"pickupAddress"]];
 //    cell.lblDeliveryAddress.text = [data objectForKey:@"dropoffAddress"];
 
 //    cell.lblName.text = [data objectForKey:@"angelerName"];
 //    cell.lblCurrencyAmount.text = [NSString stringWithFormat:@"SGD %@",[data objectForKey:@"totalCost"]];
 //    cell.imgPick.image = [GlobalInfo AngelerImageForPickupType:[data objectForKey:@"pickupType"]];
 //    cell.imgDelivery.image = [GlobalInfo AngelerImageForPickupType:[data objectForKey:@"dropoffType"]];
 //    cell.lblNote.text = [data objectForKey:@"deliveryInterval"];
 //    cell.lblNote.textColor = [Helper colorWithHexString:[[data objectForKey:@"deliveryIntervalType"] isEqualToString:@"Early"]?@"#008000":@"#ff3232"];
 //    [cell.profileImgView sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
 //    NSInteger rating = [[data objectForKey:@"angelerRating"] integerValue];
 //    UIImage *starFill = [UIImage imageNamed:@"Choose_angeler_star_fill"];
 //    UIImage *starEmpty = [UIImage imageNamed:@"Choose_angeler_star_empty"];
 //
 //    for (int i = 1; i <[cell.ratingView subviews].count ; i++) {
 //        UIImageView *imgV = [cell.ratingView viewWithTag:i];
 //        imgV.image = rating>=i?starFill:starEmpty;
 //    }
 //
 //
 //    cell.selectionStyle = UITableViewCellSelectionStyleNone;*/

/*- (IBAction)ChooseLater:(id)sender{
 [self.navigationController popToRootViewControllerAnimated:YES];
 } */


/* - (IBAction)backTapped:(id)sender{
 [self.navigationController popViewControllerAnimated:YES];
 }
 
 - (void)pickerDoneTapped:(UIButton*)sender{
 UIPickerView *piker;
 
 piker = (UIPickerView*)_txtFPriority.inputView;
 _txtFPriority.text = [_arrPickerData objectAtIndex:[piker selectedRowInComponent:0]];
 
 NSInteger selectedIndx = [piker selectedRowInComponent:0];
 
 if (selectedIndx == 0 || selectedIndx == 1) {
 self.sortingType = DELIVERY;
 }else if (selectedIndx == 2 || selectedIndx == 3) {
 self.sortingType = PRICE;
 }else{
 self.sortingType = RATE;
 }
 self.sortingOrder = [Helper checkEvenNumber:selectedIndx]?ASCENDING:DECENDING;
 
 START_POSITION = 0;
 moreRecordsAvailable = YES;
 [self callApiToLoadAngelerList];
 
 [self.view endEditing:YES];
 
 }
 #pragma mark --- PickerView Delegate
 - (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
 return 1;
 }
 // returns the # of rows in each component..
 - (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
 return _arrPickerData.count;
 }
 - (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
 return [_arrPickerData objectAtIndex:row];
 }*/


//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//
//    if (_lastContentOffset.x < (int)scrollView.contentOffset.x) {
//        NSLog(@"Scrolled Right");
//    }
//    else if (_lastContentOffset.x > (int)scrollView.contentOffset.x) {
//        NSLog(@"Scrolled Left");
//    }
//
//    else if (_lastContentOffset.y < scrollView.contentOffset.y) {
//        NSLog(@"Scrolled Down");
//    }
//
//    else if (_lastContentOffset.y > scrollView.contentOffset.y) {
//        NSLog(@"Scrolled Up");
//    }
//}

/* @property (weak, nonatomic) IBOutlet UITextField *txtFPriority;
 [_txtFPriority setInputView:[Helper pickerWithTag:TAG_VALUE_NONE forControler:self]];
 [_txtFPriority setInputAccessoryView:[Helper toolbarWithTag:TAG_VALUE_NONE forControler:self]];*/

/*@property (weak, nonatomic) IBOutlet UIView *headerView;
  [Helper addShadowToLayer:_headerView.layer];*/

/*@property (strong, nonatomic) NSArray *arrPickerData;
 _arrPickerData = [NSArray arrayWithObjects:
 @"Delivery-Early to Late",
 @"Delivery-Late to Early",
 @"Price-Low to High",
 @"Price-High to Low",
 @"Rating-Low to High",
 @"Rating-High to Low", nil];
 */

/*@property (weak, nonatomic) IBOutlet UIButton *btnChooseLater;
 _btnChooseLater.hidden = _arrAngeler.count > 0;
 
 @property (weak, nonatomic) IBOutlet UILabel *lblNote;
 _lblNote.text = [[responseObject objectForKey:@"extraData"] objectForKey:@"availabilityMessage"];*/

/* - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 ChooseAngelerStepThreeController *next = [[ChooseAngelerStepThreeController alloc] initWithNibName:@"ChooseAngelerStepThreeController" bundle:nil];
 next.anglerData = [_arrAngeler objectAtIndex:indexPath.row];
 next.packageId = self.packObj.pack_Id;
 [self.navigationController pushViewController:next animated:YES];
 }
*/
