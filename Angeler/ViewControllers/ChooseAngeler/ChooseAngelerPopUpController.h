//
//  ChooseAngelerPopUpController.h
//  Angeler
//
//  Created by AJAY on 07/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseAngelerPopUpController : UIViewController
@property (nonatomic, strong) NSString *bookingNo,*bookingMessage;
@end
