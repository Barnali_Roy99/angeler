//
//  ChooseAngelerStepTwoCell.h
//  Angeler
//
//  Created by AJAY on 11/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseAngelerStepTwoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyAmount;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UIImageView *imgPick;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelivery;
@property (weak, nonatomic) IBOutlet UIView *backgroundProfileView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *fromLocationLbl;
@property (weak, nonatomic) IBOutlet UILabel *fromDateTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *toLocationLbl;
@property (weak, nonatomic) IBOutlet UILabel *toDateTimeLbl;
@property (weak, nonatomic) IBOutlet UIButton *btnView;

//+ (CGFloat)cellHeightForData:(NSDictionary*)data;

@end
