//
//  ChooseAngelerStepOneController.h
//  Angeler
//
//  Created by AJAY on 05/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseAngelerStepOneController : UIViewController
@property (nonatomic, strong) PackageClass *packObj;
@property (nonatomic, strong) AngelerClass *angelerObj;

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *viewMeetingPoint;
@property (weak, nonatomic) IBOutlet UIView *viewPremium;
@property (weak, nonatomic) IBOutlet UILabel *meetingPointTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *premiumTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *nextView;
@property (weak, nonatomic) IBOutlet UILabel *lblNext;
@property (weak, nonatomic) IBOutlet UIImageView *meetingPointImgView;
@property (weak, nonatomic) IBOutlet UIImageView *premiumImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingPointPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPremiumPointPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingPointPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingPointCredits;
@property (weak, nonatomic) IBOutlet UILabel *lblPremiumPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPremiumCredits;
@property (weak, nonatomic) IBOutlet UIView *showCostMeetingPointView;
@property (weak, nonatomic) IBOutlet UIView *showCostPremiumView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMeetingPointHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewPremiumHeightConstraint;


@end
