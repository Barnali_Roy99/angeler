//
//  ChooseAngelerStepOneController.m
//  Angeler
//
//  Created by AJAY on 05/04/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ChooseAngelerStepOneController.h"
#import "MultipleLocationMapViewController.h"
#import "HomeViewController.h"
#import "ChooseAngelerStepTwoController.h"
#import "MyShipmentsListController.h"
#import "MeetPopUpViewController.h"

@interface ChooseAngelerStepOneController ()<MultipleLocationAddressDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *viOne;
@property (weak, nonatomic) IBOutlet UIView *viTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnSelcetPickPoint;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDeliveryPoint;

@property (nonatomic, weak) IBOutlet UIImageView *imgDeprtMtPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgDeprtPkPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgDeprtPremSer;
@property (nonatomic, weak) IBOutlet UILabel *lblFromPick;


@property (nonatomic, weak) IBOutlet UIImageView *imgArrivMtPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgArrivPkPt;
@property (nonatomic, weak) IBOutlet UIImageView *imgArrivPremSer;
@property (nonatomic, weak) IBOutlet UILabel *lblToPick;

@property (weak, nonatomic) IBOutlet UILabel *lblBasePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPMeetPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblPStoragePoint;
@property (weak, nonatomic) IBOutlet UILabel *lblPPremium;

@property (weak, nonatomic) IBOutlet UILabel *lblDMeetPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblDStoragePoint;
@property (weak, nonatomic) IBOutlet UILabel *lblDPremium;


@end

@implementation ChooseAngelerStepOneController

@synthesize topBarHeightConstraint,topNavbarView,viewPremium,viewMeetingPoint,premiumTitleLabel,meetingPointTitleLabel,nextView,meetingPointImgView,premiumImageView,showCostPremiumView,showCostMeetingPointView,viewMeetingPointHeightConstraint,viewPremiumHeightConstraint;

- (void)viewDidLoad {
    
    self.angelerObj = [[AngelerClass alloc] init];
    [self initialSetUp];
    
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setGesture];
    [self callApiToLoadPriceDetails];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"Price", nil) capitalizedString]];
    
    [viewMeetingPoint makeCornerRound:15.0];
    [viewPremium makeCornerRound:15.0];
    
    [meetingPointTitleLabel setText:NSLocalizedString(@"AddTrip_MeetingPoint", nil)];
    [premiumTitleLabel setText:NSLocalizedString(@"Premium", nil)];
     self.lblNext.text =  NSLocalizedString(@"Next", nil);
    
    self.lblMeetingPointPriceTitle.text =  NSLocalizedString(@"Price", nil);
    self.lblPremiumPriceTitle.text =  NSLocalizedString(@"Price", nil);
    self.lblMeetingPointCredits.text = NSLocalizedString(@"Credits", nil);
    self.lblPremiumCredits.text = NSLocalizedString(@"Credits", nil);
}


-(void)setGesture
{
    //Next View
    
    UITapGestureRecognizer *nextViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nextViewTapped:)];
    [self.nextView addGestureRecognizer:nextViewTapRecognizer];
    
    
    UITapGestureRecognizer *meetingPointViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(meetingPointViewTapped:)];
    [self.viewMeetingPoint addGestureRecognizer:meetingPointViewTapRecognizer];
    
    
    UITapGestureRecognizer *premiumViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(premiumViewTapped:)];
    [self.viewPremium addGestureRecognizer:premiumViewTapRecognizer];
    
    UITapGestureRecognizer *meetingPointImageViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMeetPointPopUp:)];
    [self.meetingPointImgView addGestureRecognizer:meetingPointImageViewTapRecognizer];
    
    UITapGestureRecognizer *premiumImageViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPremiumPopUp:)];
    [self.premiumImageView addGestureRecognizer:premiumImageViewTapRecognizer];
}

#pragma mark - Webservice Call

- (void)callApiToLoadPriceDetails{
    
    NSDictionary *param = @{@"packageId":_packObj.pack_Id};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPackageEstimatedCost
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]){
                                                           NSDictionary *responseDic = [responseObject objectForKey:@"responseData"];
                                                           _packObj.meetingPointPrice = [responseDic objectForKey:@"mpCost"];
                                                           _packObj.premiumPrice = [responseDic objectForKey:@"premiumCost"];
                                                           
                                                           [self loadWithData];
                                                           
                                                           if([[responseDic objectForKey:@"selectedServiceType"] isEqualToString:serviceType_Meeting_Point])
                                                           {
                                                               [self disablePremiumView];
                                                               [self setSelectedMeetingPoint];
                                                               
                                                           }
                                                           else if([[responseDic objectForKey:@"selectedServiceType"] isEqualToString:serviceType_Premium])
                                                           {
                                                               [self disableMeetingPointView];
                                                               [self setSelectedPremium];
                                                               
                                                           }
                                                       
                                                           
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}

-(void)loadWithData
{
    _lblMeetingPointPrice.text = _packObj.meetingPointPrice;
    _lblPremiumPointPrice.text = _packObj.premiumPrice;
}


-(void)disableMeetingPointView
{
    [self.viewMeetingPoint setUserInteractionEnabled:false];
    self.viewMeetingPointHeightConstraint.constant = 0;
    self.viewMeetingPoint.hidden = YES;
}

-(void)disablePremiumView
{
    [self.viewPremium setUserInteractionEnabled:false];
     self.viewPremiumHeightConstraint.constant = 0;
    self.viewPremium.hidden = YES;
}


#pragma mark - Gesture Action


- (void)nextViewTapped:(UITapGestureRecognizer*)sender
{
    if ([self validationPassed] ) {
        ChooseAngelerStepTwoController *nextObj = [[ChooseAngelerStepTwoController alloc] initWithNibName:@"ChooseAngelerStepTwoController" bundle:nil];
        nextObj.packObj = self.packObj;
        nextObj.angelerObj = self.angelerObj;
        [self.navigationController pushViewController:nextObj animated:YES];
    }
}

- (void)meetingPointViewTapped:(UITapGestureRecognizer*)sender
{
    [self setSelectedMeetingPoint];
}

- (void)premiumViewTapped:(UITapGestureRecognizer*)sender
{
    [self setSelectedPremium];
}

-(void)setSelectedMeetingPoint
{
    self.angelerObj.from_Premium_Checked = NO;
    self.angelerObj.to_Premium_Checked = NO;
    
    self.angelerObj.to_Meet_Point_Checked = YES;
    self.angelerObj.from_Meet_Point_Checked = YES;
    
    [viewPremium makeCornerRound:15.0 withBoarderColor:[UIColor clearColor] borderWidth:2.0];
    [viewMeetingPoint makeCornerRound:15.0 withBoarderColor:[UIColor appThemeBlueColor] borderWidth:2.0];
    
}

-(void)setSelectedPremium
{
    self.angelerObj.from_Premium_Checked = YES;
    self.angelerObj.to_Premium_Checked = YES;
    
    self.angelerObj.to_Meet_Point_Checked = NO;
    self.angelerObj.from_Meet_Point_Checked = NO;
    
    [viewPremium makeCornerRound:15.0 withBoarderColor:[UIColor appThemeBlueColor] borderWidth:2.0];
    [viewMeetingPoint makeCornerRound:15.0 withBoarderColor:[UIColor clearColor] borderWidth:2.0];
}

- (void)showMeetPointPopUp:(UITapGestureRecognizer*)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MeetPopUpViewController *meetPopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"MeetPopUpVCID"];
    
    meetPopUpVC.headerTitleText = NSLocalizedString(@"AddTrip_MeetingPoint", nil);
    meetPopUpVC.bottomTitleText = NSLocalizedString(@"Meet_The_Angeler_Title", nil);
    meetPopUpVC.bottomDescriptionText = NSLocalizedString(@"MeetingPoint_Description_Text", nil);
    meetPopUpVC.singleActionLabelTitleText = NSLocalizedString(@"OK", nil);
    meetPopUpVC.imageIconName = @"MeetingPoint_Blue_icon";
    
    self.definesPresentationContext = YES; //self is presenting view controller
    meetPopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
   
    
    meetPopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:meetPopUpVC animated:YES completion:nil];
}

- (void)showPremiumPopUp:(UITapGestureRecognizer*)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MeetPopUpViewController *meetPopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"MeetPopUpVCID"];
    
    meetPopUpVC.headerTitleText = NSLocalizedString(@"Premium_Service_Title", nil);
    meetPopUpVC.bottomTitleText = NSLocalizedString(@"Premium_Service_Title", nil);
    meetPopUpVC.bottomDescriptionText = NSLocalizedString(@"Premium_Service_Description_Text", nil);
    meetPopUpVC.singleActionLabelTitleText = NSLocalizedString(@"OK", nil);
    meetPopUpVC.imageIconName = @"Premium_Blue_icon";
    
 
    self.definesPresentationContext = YES; //self is presenting view controller
    meetPopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    meetPopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:meetPopUpVC animated:YES completion:nil];
}


- (BOOL)validationPassed{
    
//    NSLog(@"Object description angelerObj %@",[ICHObjectPrinter descriptionForObject:angelerObj]);
    
    NSLog(@"angelerObj Desciption %@", [self.angelerObj description]);
    
    if ((self.angelerObj.from_Meet_Point_Checked == NO) && (self.angelerObj.from_Premium_Checked == NO)) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Select_Pickup_Dropoff_Blank_Alert", nil)];
        return NO;
    }
    if([_packObj.meetingPointPrice isEqualToString:Empty])
    {
        return NO;
    }
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//=================== Old + Reuse Code =========================

/*
 - (void)callServiceToGetEstimatedCost{
 //os, osVersion, appVersion, userId, sessionId, fromCountryId, toCountryId, fromCityId, toCityId, packageId
 NSDictionary *param = @{@"fromCountryId":_packObj.from_Address.countryId,
 @"toCountryId":_packObj.to_Address.countryId,
 @"fromCityId":_packObj.from_Address.cityId,
 @"toCityId":_packObj.to_Address.cityId,
 @"packageId":_packObj.pack_Id
 };
 
 //=======Call WebService Engine========
 [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kPackageEstimatedCost
 Method:REQMETHOD_GET
 parameters:[GlobalInfo getParmWithSession:param]
 media:nil
 viewController:self
 success:^(id responseObject){
 NSLog(@"response ===   %@",responseObject);
 
 NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
 if (statusCode != 200) return ;
 
 if ([GlobalInfo checkStatusSuccess:responseObject]) {
 NSDictionary *data = [responseObject objectForKey:@"responseData"];
 _lblBasePrice.attributedText = [self formatBasePrice:[NSString stringWithFormat:@"Base Price: %@ SGD",[data objectForKey:@"basePrice"]]];
 _lblPMeetPoint.text = [NSString stringWithFormat:@"Meeting Point (%@)",[data objectForKey:@"PMP"]];
 _lblPStoragePoint.text = [NSString stringWithFormat:@"Pickup Storage Point (%@ SGD)",[data objectForKey:@"PPP"]];
 _lblPPremium.text = [NSString stringWithFormat:@"Premium (%@ SGD)",[data objectForKey:@"PP"]];
 _lblDMeetPoint.text = [NSString stringWithFormat:@"Meeting Point (%@)",[data objectForKey:@"DMP"]];
 _lblDStoragePoint.text = [NSString stringWithFormat:@"Delivery Storage Point (%@ SGD)",[data objectForKey:@"DDP"]];
 _lblDPremium.text = [NSString stringWithFormat:@"Premium (%@ SGD)",[data objectForKey:@"DP"]];
 }
 }
 failure:^(id responseObject){
 [Helper showAlertWithTitle:Empty Message:strKey_error_wentwrong];
 }];
 }
 - (NSAttributedString*)formatBasePrice:(NSString*)basePrice{
 NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:basePrice];
 
 [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Regular" size:14.0] range:NSMakeRange(0, 11)];
 [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:16.0] range:NSMakeRange(11, str.length-11)];
 [str addAttribute:NSForegroundColorAttributeName value:[Helper colorWithHexString:@"#68C1D4"] range:NSMakeRange(11, str.length-11)];
 return str;
 
 } */
/*self.automaticallyAdjustsScrollViewInsets = NO;
 [(UIScrollView*)[self.viOne superview] setContentInset:UIEdgeInsetsZero];
 [super viewDidLoad];
 NSLog(@"log---  %@",self.packObj);
 self.angelerObj = [[AngelerClass alloc] init];
 
 [Helper addShadowToLayer:_headerView.layer];
 
 [Helper addShadowToLayer:_viOne.layer];
 ROUNDEDCORNER(_viOne.layer);
 
 [Helper addShadowToLayer:_viTwo.layer];
 ROUNDEDCORNER(_viTwo.layer);
 
 [Helper addBorderToLayer:_btnSelcetPickPoint.layer];
 ROUNDEDCORNER(_btnSelcetPickPoint.layer);
 
 [Helper addBorderToLayer:_btnSelectDeliveryPoint.layer];
 ROUNDEDCORNER(_btnSelectDeliveryPoint.layer);
 // Do any additional setup after loading the view from its nib.
 
 [self callServiceToGetEstimatedCost];
 */
/*if ((!self.angelerObj.from_Meet_Point_Checked) && (!self.angelerObj.from_Pick_Point_Checked)
 && (!self.angelerObj.from_Premium_Checked)) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertPickIdEmpty];
 return NO;
 }else{
 if ((self.angelerObj.from_Pick_Point_Checked) && (self.angelerObj.From_Point_Address_Array.count == 0)) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertPickPointsEmpty];
 return NO;
 }
 }
 if ((!self.angelerObj.to_Meet_Point_Checked) && (!self.angelerObj.to_Pick_Point_Checked) && (!self.angelerObj.to_Premium_Checked)) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertDropoffIdEmpty];
 return NO;
 }else{
 if ((self.angelerObj.to_Pick_Point_Checked) && (self.angelerObj.to_Point_Address_Array.count == 0)) {
 [Helper showAlertWithTitle:Empty Message:strKey_itineryItinertDropPointsEmpty];
 return NO;
 }
 }
 */

/*- (IBAction)backTapped:(UIButton*)sender {
 MyShipmentsListController *managObj = [[MyShipmentsListController alloc] initWithNibName:@"MyShipmentsListController" bundle:nil];
 AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
 
 }
 - (IBAction)nextTapped:(UIButton*)sender {
 
 }
 
 */
/* */
