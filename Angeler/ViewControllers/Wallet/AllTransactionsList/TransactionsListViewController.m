//
//  TransactionsListViewController.m
//  Angeler
//
//  Created by AJAY on 21/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "TransactionsListViewController.h"
#import "WalletCell.h"

static NSString *CellIdentifier = @"WalletCell";

@interface TransactionsListViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
    
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;


@property (weak, nonatomic) IBOutlet UITableView *transactionListTableview;
@property (nonatomic, strong) NSMutableArray *arrTransactionsList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;

@end

@implementation TransactionsListViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callApiToLoadTransactionList];
    [self initialSetUp];
    
    
}

#pragma mark - General Functions

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:[NSLocalizedString(@"All_Transactions", nil) capitalizedString]];
    
    //Tableview Set up
    
    [_transactionListTableview registerNib:[UINib nibWithNibName:@"WalletCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _transactionListTableview.estimatedRowHeight = 10;
    _transactionListTableview.rowHeight = UITableViewAutomaticDimension;
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_transactionListTableview addSubview:refreshController];
    
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
}


#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToLoadTransactionList];
    [refreshController endRefreshing];
}

#pragma mark - Webservice Call

- (void)callApiToLoadTransactionList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",(long)START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kTransactionList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           
                                                           
                                                           if (_arrTransactionsList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrTransactionsList removeAllObjects];
                                                               }else if(_arrTransactionsList.count > START_POSITION){
                                                                   [_arrTransactionsList removeObjectsInRange:NSMakeRange(START_POSITION, _arrTransactionsList.count - START_POSITION)];
                                                               }
                                                               [_arrTransactionsList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrTransactionsList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [arr count];
                                                           _lblNoRecord.hidden = _arrTransactionsList.count > 0;
                                                    _transactionListTableview.hidden = !_lblNoRecord.hidden;       [_transactionListTableview reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arrTransactionsList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WalletCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *transactionInfoDic = [_arrTransactionsList objectAtIndex:indexPath.row];
  //  cell.transactionTitle.text = [transactionInfoDic objectForKey:@"title"];
    cell.transactionDesc.text = [transactionInfoDic objectForKey:@"description"];
    cell.transactionReferenceNumber.text = [NSString stringWithFormat:@"Ref | %@",[transactionInfoDic objectForKey:@"transactionRefNo"]];
    cell.transactionDate.text = [transactionInfoDic objectForKey:@"recordDateTime"];
    cell.transactionAmount.text = [transactionInfoDic objectForKey:@"noOfCredits"];
   // cell.transactionAmount.text = [NSString stringWithFormat:@"%@ %@",[transactionInfoDic objectForKey:@"noOfCredits"],NSLocalizedString(@"Credits", nil)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
  /*  NSString *transactionType = [transactionInfoDic objectForKey:@"transactionType"];
    
    if([transactionType isEqualToString:Transaction_Added])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Added_icon"];
    }
    else if([transactionType isEqualToString:Transaction_Transferred])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Transferred_icon"];
    }
    else if([transactionType isEqualToString:Transaction_Received])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Received_icon"];
    }
    else if([transactionType isEqualToString:Transaction_Blocked])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Blocked_icon"];
    }
    else if([transactionType isEqualToString: Transaction_Paid])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Paid_icon"];
    }
    else if([transactionType isEqualToString: Transaction_Income])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Income_icon"];
    }
    else if([transactionType isEqualToString: Transaction_Withdrawn])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Withdrawn_icon"];
    }*/
    
    //Load More Data
    
    if (indexPath.row == [self.arrTransactionsList count] - 1  && moreRecordsAvailable)
    {
        [self callApiToLoadTransactionList];
    }
    
    return cell;
    
}



@end
