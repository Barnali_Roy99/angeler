//
//  WalletCell.h
//  Angeler
//
//  Created by AJAY on 19/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WalletCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *transactionTitle;
@property (weak, nonatomic) IBOutlet UILabel *transactionDesc;
@property (weak, nonatomic) IBOutlet UILabel *transactionReferenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *transactionDate;
@property (weak, nonatomic) IBOutlet UILabel *transactionAmount;
@property (weak, nonatomic) IBOutlet UIImageView *transactionStatusImageView;


@end

NS_ASSUME_NONNULL_END
