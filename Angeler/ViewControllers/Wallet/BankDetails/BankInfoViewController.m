//
//  BankInfoViewController.m
//  Angeler
//
//  Created by AJAY on 23/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "BankInfoViewController.h"
#import "BankInfoCell.h"

static NSString *CellIdentifier = @"BankInfoCell";

@interface BankInfoViewController ()

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *BankInfoTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tblBankInfoList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoData;
@property (nonatomic, strong) NSMutableArray *arrBankInfo;

@end

@implementation BankInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialSetUp];
    [self callApiToLoadBankInfo];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - General Functions

-(void)initialSetUp
{
    
    if ([Helper isIphoneX])
    {
        self.topBarHeightConstraint.constant = 104;
    }
    else
    {
        self.topBarHeightConstraint.constant = 84;
    }
    
    [self.topNavbarView showNavbackBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Bank_Info", nil) capitalizedString]];
    
    //Table view Set Up
    
    [_tblBankInfoList registerNib:[UINib nibWithNibName:@"BankInfoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblBankInfoList.estimatedRowHeight = 20;
    [_tblBankInfoList makeCornerRound:15.0];
    _tblBankInfoList.rowHeight = UITableViewAutomaticDimension;
    [_tblBankInfoList setNeedsLayout];
    [_tblBankInfoList layoutIfNeeded];
}

-(void)loadWithData:(NSDictionary*)data
{
    _BankInfoTitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Bank_Transfer_Amount_Title", nil),[data objectForKey:@"price"]];
    
    _arrBankInfo = [NSMutableArray arrayWithArray:[data objectForKey:@"bankList"]];
    
    _lblNoData.hidden = _arrBankInfo.count > 0;
    _tblBankInfoList.hidden = !_lblNoData.hidden;
    [self.tblBankInfoList reloadData];
    
}

#pragma mark - Webservice Call

- (void)callApiToLoadBankInfo{
    
    NSDictionary *param = @{@"countryId":self.countryID,
                            @"creditsSelected":self.creditAmount,
                            };
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kBankList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       NSDictionary *responseDic = [responseObject objectForKey:@"responseData"];
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           [self loadWithData:responseDic];
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arrBankInfo.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BankInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.lblBankDetails.text = [[_arrBankInfo objectAtIndex:indexPath.row] objectForKey:@"bankDetails"];
 
    return cell;
    
}



@end
