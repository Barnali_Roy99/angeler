//
//  BankInfoCell.m
//  Angeler
//
//  Created by AJAY on 23/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "BankInfoCell.h"

@implementation BankInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.containerView makeCornerRound:15.0];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
