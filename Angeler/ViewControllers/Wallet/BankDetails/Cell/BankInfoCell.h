//
//  BankInfoCell.h
//  Angeler
//
//  Created by AJAY on 23/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblBankDetails;

@end

NS_ASSUME_NONNULL_END
