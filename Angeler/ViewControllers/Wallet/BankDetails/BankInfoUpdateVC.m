//
//  BankInfoUpdateVC.m
//  Angeler
//
//  Created by AJAY on 28/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "BankInfoUpdateVC.h"

@interface BankInfoUpdateVC ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UIView *bankDetailsMainView;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsUpdateAddTitle;
@property (weak, nonatomic) IBOutlet UIView *BankDetailsView;
@property (weak, nonatomic) IBOutlet UITextView *bankDetailsTextView;

@property (weak, nonatomic) IBOutlet UIView *viewSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmit;

@end

@implementation BankInfoUpdateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLocalization];
    [self initialSetUp];
    [self setGesture];
    
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - General Functions

-(void)initialSetUp
{
    _bankDetailsTextView.text = _givenBankInfo;
    
    if ([Helper isIphoneX])
    {
        self.topBarHeightConstraint.constant = 104;
    }
    else
    {
        self.topBarHeightConstraint.constant = 84;
    }
    
    [self.topNavbarView showNavbackBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Bank_Details", nil) capitalizedString]];
    
    [_BankDetailsView makeCornerRound:15.0];
    [_bankDetailsMainView makeCornerRound:15.0];
    [_bankDetailsTextView makeCornerRound:15.0];
    [_viewSubmit makeCornerRound:15.0];
    [_viewSubmit dropShadowWithColor:[UIColor lightishGrayColor]];
}

-(void)setLocalization
{
    _lblHeaderTitle.text = NSLocalizedString(@"Bank_Details_Main_Title", nil);
    _lblSubmit.text = [NSLocalizedString(@"Submit", nil) uppercaseString];
    
    if([_givenBankInfo isEqualToString:Empty])
    {
        _lblDetailsUpdateAddTitle.text = NSLocalizedString(@"Bank_Details_SubTitle_Add", nil);
        [_viewSubmit setHidden:false];
        [_bankDetailsTextView setUserInteractionEnabled:true];
    }
    else
    {
        if(_isBankDetailsVerified){
            _lblDetailsUpdateAddTitle.text = NSLocalizedString(@"Bank_Details_SubTitle_Already_Addeed", nil);
            [_viewSubmit setHidden:true];
             [_bankDetailsTextView setUserInteractionEnabled:false];
        }
        else{
            _lblDetailsUpdateAddTitle.text = NSLocalizedString(@"Bank_Details_SubTitle_Update", nil);
            [_viewSubmit setHidden:false];
             [_bankDetailsTextView setUserInteractionEnabled:true];
        }
    }
    
}
//Bank_Details_SubTitle_Update

-(void)setBankDetailsNotAdded{
   
}

-(void)setBankDetailsAlreadyAdded{
    _lblDetailsUpdateAddTitle.text = NSLocalizedString(@"Bank_Details_SubTitle_Already_Addeed", nil);
    [_viewSubmit setHidden:true];
}

-(void)setGesture
{
    UITapGestureRecognizer *selectViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped:)];
    [_viewSubmit addGestureRecognizer:selectViewTapRecognizer];
    
}

#pragma mark - Gesture Action

-(void)submitViewTapped:(UITapGestureRecognizer*)sender
{
    if([_bankDetailsTextView.text isEqualToString:Empty])
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Bank_Details_Empty_Validation_Alert_Title", nil)];
    else
    [self callApiToSaveBankDetails];
}
#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - Webservice Call

- (void)callApiToSaveBankDetails{
    
    NSDictionary *param = @{@"bankInfoForCommission":_bankDetailsTextView.text};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserSaveBankDetails
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                         NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



@end
