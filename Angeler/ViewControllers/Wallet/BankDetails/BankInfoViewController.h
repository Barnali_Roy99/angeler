//
//  BankInfoViewController.h
//  Angeler
//
//  Created by AJAY on 23/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankInfoViewController : UIViewController
@property (nonatomic,strong) NSString *countryID;
@property (nonatomic,strong) NSString *creditAmount;


@end

NS_ASSUME_NONNULL_END
