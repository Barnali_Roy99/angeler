//
//  BankInfoUpdateVC.h
//  Angeler
//
//  Created by AJAY on 28/01/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankInfoUpdateVC : UIViewController

@property(nonatomic,strong)NSString *givenBankInfo;
@property(assign)BOOL *isBankDetailsVerified;

@end

NS_ASSUME_NONNULL_END
