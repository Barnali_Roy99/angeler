//
//  AddCreditViewController.h
//  Angeler
//
//  Created by AJAY on 20/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddCreditViewController : UIViewController
@property (weak, nonatomic) IBOutlet NSString *myBalanceValue;
@end

NS_ASSUME_NONNULL_END
