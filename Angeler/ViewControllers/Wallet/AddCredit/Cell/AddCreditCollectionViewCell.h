//
//  AddCreditCollectionViewCell.h
//  Angeler
//
//  Created by AJAY on 20/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddCreditCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet RoundView *creditRoundView;
@property (weak, nonatomic) IBOutlet UILabel *lblCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditRoundViewWidthConstraint;

@end

NS_ASSUME_NONNULL_END
