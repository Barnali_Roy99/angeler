//
//  AddCreditViewController.m
//  Angeler
//
//  Created by AJAY on 20/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "AddCreditViewController.h"
#import "AddCreditCollectionViewCell.h"
#import "BankInfoViewController.h"


static NSString *CellIdentifier = @"AddCreditCollectionViewCell";

int percentageRateIncrease = 7;
int collctionViewItemHeight = 0;

@interface AddCreditViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSArray *creditArr;
    int selectedCreditIndex;
    int selectedCountryIndex;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UILabel *myBalanceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *myBalanceCreditLabel;

@property (weak, nonatomic) IBOutlet UILabel *addCreditTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *txtAddCredit;

@property (weak, nonatomic) IBOutlet UIView *viewSelect;
@property (weak, nonatomic) IBOutlet UILabel *lblSelect;

@property (weak, nonatomic) IBOutlet UILabel *orLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *showCreditCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblChoooseCredits;

@property (weak, nonatomic) IBOutlet UIView *chooseCountryMainView;
@property (weak, nonatomic) IBOutlet UILabel *lblChooseCountryTitle;
@property (weak, nonatomic) IBOutlet UIView *chooseCountrySubView;
@property (weak, nonatomic) IBOutlet UITextField *txtChooseCountry;
@property (nonatomic, strong) NSMutableArray *arrCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblCreditTitle;

@end

@implementation AddCreditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedCreditIndex = -1;
    selectedCountryIndex = -1;
    [self callApiToGetCountryList];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    collctionViewItemHeight = _showCreditCollectionView.frame.size.width / 4;
    CGFloat height = collctionViewItemHeight * 2;
    NSLog(@"Height %f",height);
    _collectionViewHeightConstraint.constant = height;
    
    [self.showCreditCollectionView reloadData];
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        self.topBarHeightConstraint.constant = 104;
    }
    else
    {
        self.topBarHeightConstraint.constant = 84;
    }
    
    [self.topNavbarView showNavbackBtn];
    [self.topNavbarView setTitle:NSLocalizedString(@"Add_Credit", nil)];
    
    [_mainContentView makeCornerRound:15.0];
    [_chooseCountryMainView makeCornerRound:15.0];
    [_chooseCountrySubView makeCornerRound:15.0];
    [_viewSelect makeCornerRound:15.0];
    [_viewSelect dropShadowWithColor:[UIColor lightishGrayColor]];
    
    [_showCreditCollectionView registerNib:[UINib nibWithNibName:@"AddCreditCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:CellIdentifier];
    creditArr = [[NSArray alloc] initWithObjects:@"50",@"100",@"150",@"200",@"300",@"500",@"1000",@"2000",nil];
    
    //Initial Set
    
   
    _txtChooseCountry.text = NSLocalizedString(@"Choose", nil);
    [self setCountryPicker];
    
    //Data Set
    _myBalanceCreditLabel.text = _myBalanceValue;
    
    
}

-(void)setLocalization
{
    _orLabel.text = NSLocalizedString(@"OR", nil);
    _lblSelect.text = [NSLocalizedString(@"Submit", nil) uppercaseString];
    _myBalanceTitleLabel.text = NSLocalizedString(@"My_Balance", nil);
    _addCreditTitleLabel.text = NSLocalizedString(@"Enter_Amount", nil);
    _lblChoooseCredits.text = NSLocalizedString(@"Choose_Amount", nil);
    _lblChooseCountryTitle.text = NSLocalizedString(@"Choose_Country", nil);
    _lblCreditTitle.text = NSLocalizedString(@"Credits", nil);
}

-(void)setGesture
{
    UITapGestureRecognizer *selectViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectViewTapped:)];
    [_viewSelect addGestureRecognizer:selectViewTapRecognizer];
    
}

-(void)setCountryPicker
{
    //Country List Set Up
    [_txtChooseCountry setInputView:[Helper pickerWithTag:COUNTRY_NAME forControler:self]];
    [_txtChooseCountry setInputAccessoryView:[Helper toolbarWithTag:COUNTRY_NAME forControler:self]];
}

- (BOOL)validate{
    
    if ([Helper checkEmptyField:_txtAddCredit.text])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Please_Enter_Credit", nil)];
        return NO;
        
    }
    if (selectedCountryIndex == -1)
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Select_Country_Alert", nil)];
        return NO;

    }
    
    
     return YES;
}

#pragma mark - Picker Call

- (void)pickerDoneTapped:(UIButton*)sender{
    
    if (sender.tag == COUNTRY_NAME) {
        
        UIPickerView *piker = (UIPickerView*)_txtChooseCountry.inputView;
        
        if(([piker selectedRowInComponent:0] - 1) >= 0)
        {
            selectedCountryIndex = (int)[piker selectedRowInComponent:0] - 1;
            _txtChooseCountry.text = [[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"countryName"];
            
        }
        [_txtChooseCountry resignFirstResponder];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        return _arrCountry.count + 1;
    }
    return 0;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        if (row == 0)
        {
            return NSLocalizedString(@"Choose", nil);
        }
        return [[_arrCountry objectAtIndex:row-1] objectForKey:@"countryName"];
    }
    return Empty;
}

#pragma mark -API Call

- (void)callApiToGetCountryList{
    
    NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kCountryList
                                                    Method:REQMETHOD_GET
                                              parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           _arrCountry = [NSMutableArray arrayWithArray:arr];
                                                         
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



#pragma mark - Gesture Action

-(void)selectViewTapped:(UITapGestureRecognizer*)sender
{
    if([self validate])
    {
        BankInfoViewController *bankInfoObj = [[BankInfoViewController alloc] initWithNibName:@"BankInfoViewController" bundle:nil];
        bankInfoObj.creditAmount = _txtAddCredit.text;
        bankInfoObj.countryID = [[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"countryId"];
        [self.navigationController pushViewController:bankInfoObj animated:YES];
    }
    
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _txtAddCredit)
    {
        selectedCreditIndex = -1;
        [self.showCreditCollectionView reloadData];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - CollectionView Delegate Datasource

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    AddCreditCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
   
    //Count view size increase gradually
    
    CGFloat itemWidthPercentage =  100 - (((creditArr.count - 1) - indexPath.row) * percentageRateIncrease);
    CGFloat eachItemWidth = ((collctionViewItemHeight - 5) * itemWidthPercentage) / 100;
    cell.creditRoundViewWidthConstraint.constant = eachItemWidth;
    if (selectedCreditIndex == indexPath.row)
    {
        cell.creditRoundView.backgroundColor = [UIColor appThemeBlueColor];
        cell.lblCredit.textColor = [UIColor blackColor];
        cell.creditRoundView.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.creditRoundView.layer.borderWidth = 2.0;
    }
    else
    {
        cell.creditRoundView.backgroundColor = [UIColor clearColor];
        cell.lblCredit.textColor = [UIColor appThemeBlueColor];
        cell.creditRoundView.layer.borderColor = [[UIColor appThemeBlueColor] CGColor];
        cell.creditRoundView.layer.borderWidth = 2.0;
    }
    
    cell.lblCredit.text = [creditArr objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCreditIndex = indexPath.row;
    _txtAddCredit.text = [creditArr objectAtIndex:indexPath.row];    [self.showCreditCollectionView reloadData];
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return creditArr.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collctionViewItemHeight, collctionViewItemHeight);
}


@end
