//
//  TransferCreditViewController.m
//  Angeler
//
//  Created by AJAY on 19/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "TransferCreditViewController.h"
#import "HomeViewController.h"
#import "WalletViewController.h"
#import "SharePopUpDoubleBtnActionVC.h"

@interface TransferCreditViewController ()<SharePopUpDoubleBtnActionDismissDelegate>

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;


@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIView *viewAngelerEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtAngelerEmail;

@property (weak, nonatomic) IBOutlet UIView *viewAngelerID;
@property (weak, nonatomic) IBOutlet UITextField *txtAngelerID;

@property (weak, nonatomic) IBOutlet UILabel *orLabel;

@property (weak, nonatomic) IBOutlet UIView *viewShare;
@property (weak, nonatomic) IBOutlet UILabel *lblShare;

@property (weak, nonatomic) IBOutlet UILabel *InputCreditTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *txtEnterCredit;

@property (weak, nonatomic) IBOutlet UILabel *myBalanceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *myBalanceCreditLabel;


@property (weak, nonatomic) IBOutlet UILabel *mainContentViewTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblCreditTitle;

@end

@implementation TransferCreditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}
#pragma mark - General Functions

-(void)initialSetUp
{
    [self setPlaceHolderText];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        self.topBarHeightConstraint.constant = 104;
    }
    else
    {
        self.topBarHeightConstraint.constant = 84;
    }
    
    [self.topNavbarView showNavbackBtn];
    [self.topNavbarView setTitle:NSLocalizedString(@"Transfer_Credit", nil)];
    
    
    [_mainContentView makeCornerRound:15.0];
    [_viewAngelerEmail makeCornerRound:15.0];
    [_viewAngelerID makeCornerRound:15.0];
    
    [_viewShare makeCornerRound:15.0];
    [_viewShare dropShadowWithColor:[UIColor lightishGrayColor]];
    
    _orLabel.text = NSLocalizedString(@"OR", nil);
    _lblShare.text = [NSLocalizedString(@"Transfer", nil) uppercaseString];
    _myBalanceTitleLabel.text = NSLocalizedString(@"My_Balance", nil);
    _InputCreditTitleLabel.text = NSLocalizedString(@"Enter_The_Credit_To_Transfer", nil);
    _mainContentViewTitleLabel.text = NSLocalizedString(@"Transfer_Credit_Info_Title", nil);
      _lblCreditTitle.text = NSLocalizedString(@"Credits", nil);
    
    //Data Set
    _myBalanceCreditLabel.text = _myBalanceValue;
    
}

-(void)setPlaceHolderText{

    UIColor *color = [UIColor lightGrayColor];
    self.txtAngelerID.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Angeler_ID", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtAngelerEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Angeler_Email_Account", nil) attributes:@{NSForegroundColorAttributeName: color}];
}

-(void)setGesture
{
    UITapGestureRecognizer *shareViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareViewTapped:)];
    [_viewShare addGestureRecognizer:shareViewTapRecognizer];

}

- (BOOL)validate{
    
     if ([Helper checkEmptyField:_txtEnterCredit.text])
     {
         [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Credit_Add_Blank_Alert", nil)];
         return NO;
        
     }
    
    if ([Helper checkEmptyField:_txtAngelerID.text] && [Helper checkEmptyField:_txtAngelerEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"EnterId_Or_Email_Blank_Alert", nil)];
        return NO;
    }
    
    if (![Helper checkEmptyField:_txtAngelerID.text] && ![Helper checkEmptyField:_txtAngelerEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"EnterId_Either_Or_Email_Blank_Alert", nil)];
        return NO;
    }
    
    if (![Helper checkEmptyField:_txtAngelerEmail.text]) {
        if (![Helper checkEmailFormat:_txtAngelerEmail.text]) {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"ValidEmail_Alert", nil)];
            return NO;
        }
    }
    
    return YES;
    
}

-(void)showAngelerCreditTransferSuccessPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpDoubleBtnActionVC *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpDoubleBtnVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = @"";
    sharePopUpVC.headerMiddleTitleText = NSLocalizedString(@"Tranfer_Credit_Title_Alert", nil);
    sharePopUpVC.headerDescriptionText = @"";
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.leftBtnActionitleText = [NSLocalizedString(@"My_Wallet", nil) uppercaseString];
    sharePopUpVC.rightBtnActionitleText = [NSLocalizedString(@"Home", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

#pragma mark -API Call

- (void)callApiToTransferCredit{
    
    NSDictionary *param = @{@"noOfCredits":self.txtEnterCredit.text,
                            @"sharedUserId":self.txtAngelerID.text,
                            @"sharedUserEmail":self.txtAngelerEmail.text
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kTransferCredit
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           [self showAngelerCreditTransferSuccessPopUp];
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


#pragma mark - Gesture Action

-(void)shareViewTapped:(UITapGestureRecognizer*)sender
{
    if([self validate])
    {
        [self callApiToTransferCredit];
    }
}

#pragma mark - SharePopUpDoubleBtnActionDismissDelegate

- (void)dismissPopUpLeftBtnAction
{
    WalletViewController *walletObj = [[WalletViewController alloc] initWithNibName:@"WalletViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:walletObj];
    
}
- (void)dismissPopUpRightBtnAction
{
    HomeViewController *homeObj = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:homeObj];
}




@end
