//
//  TransferCreditViewController.h
//  Angeler
//
//  Created by AJAY on 19/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransferCreditViewController : UIViewController
@property (weak, nonatomic) IBOutlet NSString *myBalanceValue;

@end

NS_ASSUME_NONNULL_END
