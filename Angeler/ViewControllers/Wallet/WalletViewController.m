//
//  WalletViewController.m
//  Angeler
//
//  Created by AJAY on 19/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "WalletViewController.h"
#import "WalletCell.h"
#import "TransferCreditViewController.h"
#import "AddCreditViewController.h"
#import "TransactionsListViewController.h"
#import "BankInfoUpdateVC.h"


static NSString *CellIdentifier = @"WalletCell";

@interface WalletViewController () <UITableViewDelegate,UITableViewDataSource>
{
    NSString *userGivenBankInfo;
  
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *showCreditView;

@property (weak, nonatomic) IBOutlet UIView *viewAddCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd;

@property (weak, nonatomic) IBOutlet UIView *viewTransferCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblTransfer;

//@property (weak, nonatomic) IBOutlet UIView *viewShareCredit;
//@property (weak, nonatomic) IBOutlet UILabel *lblShare;

@property (weak, nonatomic) IBOutlet UILabel *lblBalanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBalanceCredit;

@property (weak, nonatomic) IBOutlet UILabel *lblWithdrwableBalanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblWithdrwableBalanceCredit;

@property (weak, nonatomic) IBOutlet UIView *viewAllTransaction;
@property (weak, nonatomic) IBOutlet UILabel *lblAllTransaction;

@property (weak, nonatomic) IBOutlet UITableView *transactionListTableview;
@property (nonatomic, strong) NSMutableArray *arrWalletDetails;

@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (weak, nonatomic) IBOutlet UILabel *lblHistory;
@property (weak, nonatomic) IBOutlet UIImageView *bankInfoAddUpdateImageView;
@property(assign)BOOL *isBankDetailsVerified;

@end

@implementation WalletViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self callApiToLoadWalletHistoryList];
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView setTitle:[NSLocalizedString(@"Wallet", nil) capitalizedString]];
    
    [self.showCreditView makeCornerRound:15.0];
    [self.showCreditView dropShadowWithColor:[UIColor lightishGrayColor]];
    
    [self.viewAddCredit makeCornerRound:12.0];
    [self.viewTransferCredit makeCornerRound:12.0];
   
    
    [_viewAllTransaction makeCornerRound:15.0];
    [_viewAllTransaction dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Tableview Set up
    
    [_transactionListTableview registerNib:[UINib nibWithNibName:@"WalletCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _transactionListTableview.estimatedRowHeight = 10;
    _transactionListTableview.rowHeight = UITableViewAutomaticDimension;
    
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    self.lblHistory.text = NSLocalizedString(@"History", nil);
}

-(void)setBankInfoAddEditIconWithBankInfoAlreadyAdded:(BOOL)isBankInfoAlreadyAdded{
    if(isBankInfoAlreadyAdded)
        [_bankInfoAddUpdateImageView setImage:[UIImage imageNamed:@"BankInfo_GreenIcon"]];
    else
        [_bankInfoAddUpdateImageView setImage:[UIImage imageNamed:@"BankInfo_RedCrossIcon"]];
}

-(void)setLocalization
{
    //localization
    
    _lblBalanceTitle.text = NSLocalizedString(@"My_Balance", nil);
    _lblWithdrwableBalanceTitle.text = NSLocalizedString(@"My_Commision", nil);
    
    _lblAdd.text = [NSLocalizedString(@"Add_ANC", nil) uppercaseString];
    _lblTransfer.text = [NSLocalizedString(@"Transfer", nil) uppercaseString];
//    _lblShare.text = [NSLocalizedString(@"Share", nil) uppercaseString];
    
    _lblAllTransaction.text = [NSLocalizedString(@"All_Transactions", nil) uppercaseString];
}

-(void)setGesture
{
//    UITapGestureRecognizer *shareCreditViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareCreditViewTapped:)];
//    [_viewShareCredit addGestureRecognizer:shareCreditViewTapRecognizer];
    
    UITapGestureRecognizer *addCreditViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addCreditViewTapped:)];
    [_viewAddCredit addGestureRecognizer:addCreditViewTapRecognizer];
    
    UITapGestureRecognizer *transferCreditViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(transferCreditViewTapped:)];
    [_viewTransferCredit addGestureRecognizer:transferCreditViewTapRecognizer];
    
    UITapGestureRecognizer *allTransactionsViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(allTransactionsViewTapped:)];
    [_viewAllTransaction addGestureRecognizer:allTransactionsViewTapRecognizer];
}

-(void)loadWithData:(NSDictionary*)data
{
    NSDictionary *walletDetails = [data objectForKey:@"walletDetails"];
    _lblBalanceCredit.text = [NSString stringWithFormat:@"%@ %@",[walletDetails objectForKey:@"totalBalance"],NSLocalizedString(@"Credits", nil)];
    _lblWithdrwableBalanceCredit.text = [NSString stringWithFormat:@"%@ %@",[walletDetails objectForKey:@"withdrawableBalance"],NSLocalizedString(@"Credits", nil)];
  
    
    _arrWalletDetails = [NSMutableArray arrayWithArray:[data objectForKey:@"txn_list"]];
   
    _lblNoRecord.hidden = _arrWalletDetails.count > 0;
    _transactionListTableview.hidden = !_lblNoRecord.hidden;
    _viewAllTransaction.hidden = !_lblNoRecord.hidden;
    [self.transactionListTableview reloadData];
    
    userGivenBankInfo = [walletDetails objectForKey:@"bankInfoForCommission"];
    if([userGivenBankInfo isEqualToString:Empty])
    {
        [self setBankInfoAddEditIconWithBankInfoAlreadyAdded:false];
        _isBankDetailsVerified = false;
    }
    else{
        
        if([[walletDetails objectForKey:@"bankInfoVerificationStatus"] isEqualToString:@"Y"])
        {
            [self setBankInfoAddEditIconWithBankInfoAlreadyAdded:true];
            _isBankDetailsVerified = true;
        }
        else{
             [self setBankInfoAddEditIconWithBankInfoAlreadyAdded:false];
            _isBankDetailsVerified = false;
        }
    }
                              
}

#pragma mark - Button Action

- (IBAction)onBankDetailsUpdateClicked:(id)sender {
    BankInfoUpdateVC *Obj = [[BankInfoUpdateVC alloc] initWithNibName:@"BankInfoUpdateVC" bundle:nil];
    Obj.givenBankInfo = userGivenBankInfo;
    Obj.isBankDetailsVerified = _isBankDetailsVerified;
    [self.navigationController pushViewController:Obj animated:YES];
}


#pragma mark - Webservice Call

- (void)callApiToLoadWalletHistoryList{
    
    NSDictionary *param = [[NSDictionary alloc] init];
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kWalletDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       NSDictionary *responseDic = [responseObject objectForKey:@"responseData"];
                                                     
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           [self loadWithData:responseDic];
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


#pragma mark - Gesture Action

-(void)shareCreditViewTapped:(UITapGestureRecognizer*)sender
{
    TransferCreditViewController *transferCreditObj = [[TransferCreditViewController alloc] initWithNibName:@"TransferCreditViewController" bundle:nil];
    [self.navigationController pushViewController:transferCreditObj animated:YES];
}
-(void)addCreditViewTapped:(UITapGestureRecognizer*)sender
{
    AddCreditViewController *addCreditObj = [[AddCreditViewController alloc] initWithNibName:@"AddCreditViewController" bundle:nil];
    addCreditObj.myBalanceValue = _lblBalanceCredit.text;
    [self.navigationController pushViewController:addCreditObj animated:YES];
}
-(void)transferCreditViewTapped:(UITapGestureRecognizer*)sender
{
    TransferCreditViewController *transferCreditObj = [[TransferCreditViewController alloc] initWithNibName:@"TransferCreditViewController" bundle:nil];
    transferCreditObj.myBalanceValue = _lblBalanceCredit.text;
    [self.navigationController pushViewController:transferCreditObj animated:YES];
}
-(void)allTransactionsViewTapped:(UITapGestureRecognizer*)sender
{
    TransactionsListViewController *transactionListObj = [[TransactionsListViewController alloc] initWithNibName:@"TransactionsListViewController" bundle:nil];
    [self.navigationController pushViewController:transactionListObj animated:YES];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arrWalletDetails.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WalletCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *transactionInfoDic = [_arrWalletDetails objectAtIndex:indexPath.row];
   // cell.transactionTitle.text = [transactionInfoDic objectForKey:@"title"];
    cell.transactionDate.text = [transactionInfoDic objectForKey:@"recordDateTime"];
    cell.transactionDesc.text = [transactionInfoDic objectForKey:@"description"];
    cell.transactionReferenceNumber.text = [NSString stringWithFormat:@"%@ | %@", NSLocalizedString(@"Ref", nil),[transactionInfoDic objectForKey:@"transactionRefNo"]];
    cell.transactionAmount.text = [transactionInfoDic objectForKey:@"noOfCredits"];
    
    // cell.transactionAmount.text = [NSString stringWithFormat:@"%@ %@",[transactionInfoDic objectForKey:@"noOfCredits"],NSLocalizedString(@"Credits", nil)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
   /* NSString *transactionType = [transactionInfoDic objectForKey:@"transactionType"];
    
    if([transactionType isEqualToString:Transaction_Added])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Added_icon"];
    }
    else if([transactionType isEqualToString:Transaction_Transferred])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Transferred_icon"];
    }
    else if([transactionType isEqualToString:Transaction_Received])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Received_icon"];
    }
    else if([transactionType isEqualToString:Transaction_Blocked])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Blocked_icon"];
    }
    else if([transactionType isEqualToString: Transaction_Paid])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Paid_icon"];
    }
    else if([transactionType isEqualToString: Transaction_Income])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Income_icon"];
    }
    else if([transactionType isEqualToString: Transaction_Withdrawn])
    {
        cell.transactionStatusImageView.image = [UIImage imageNamed:@"Transaction_Type_Withdrawn_icon"];
    }*/
 
    return cell;

}

@end
