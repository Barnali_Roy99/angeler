//
//  CharityDetailsVC.h
//  Angeler
//
//  Created by AJAY on 23/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CharityDetailsVC : UIViewController
@property(nonatomic,strong)NSString *charityID;
@property(nonatomic,strong)NSString *selectedCallerStr;

@end

NS_ASSUME_NONNULL_END
