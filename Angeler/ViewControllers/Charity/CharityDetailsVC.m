//
//  CharityDetailsVC.m
//  Angeler
//
//  Created by AJAY on 23/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "CharityDetailsVC.h"

@interface CharityDetailsVC (){
    NSString *charityURL;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;


//Charity Contact Details

@property (weak, nonatomic) IBOutlet UIView *viewCharityContactDetails;
@property (weak, nonatomic) IBOutlet UIView *viewCharityContactDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityContactDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityAddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityCountryTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityName;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityURL;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityURLTitle;

//Charity Other details

@property (weak, nonatomic) IBOutlet UIView *viewCharityOtherDetails;
@property (weak, nonatomic) IBOutlet UIView *viewCharityOtherDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityOtherDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityDescTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityKeywordsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityKeyword;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityStatus;

@end

@implementation CharityDetailsVC

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
     [self initialSetUp];
    [self callApiToFetchData];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
    
}

#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:NSLocalizedString(@"Charity_Details", nil)];
    
    
    //Charity Contact Details
    
    [self.viewCharityContactDetails makeCornerRound:15.0];
    [self.viewCharityContactDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Charity Other Details
    
    [self.viewCharityOtherDetails makeCornerRound:15.0];
    [self.viewCharityOtherDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
}
-(void)roundAllSpecificCorner
{
    [self.viewCharityContactDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewCharityOtherDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
   
}

-(void)setLocalization
{
   //Charity Contact Details
    
    _lblCharityContactDetailsTitle.text = NSLocalizedString(@"Charity_Contact_Details",nil);
    
    _lblCharityNameTitle.text = NSLocalizedString(@"Name",nil);
    _lblCharityAddressTitle.text = NSLocalizedString(@"Address",nil);
   _lblCharityCountryTitle.text = NSLocalizedString(@"Country",nil);
    _lblCharityURLTitle.text = NSLocalizedString(@"URL", nil);
   
    //Charity Other Details
    
    _lblCharityOtherDetailsTitle.text = NSLocalizedString(@"Charity_Other_Details",nil);
    
    _lblCharityDescTitle.text = NSLocalizedString(@"Description",nil);
    _lblCharityKeywordsTitle.text = NSLocalizedString(@"Keyword",nil);
    _lblCharityStatusTitle.text = NSLocalizedString(@"Status",nil);
    
}

-(void)setGesture{
    UITapGestureRecognizer *urlLabelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(URLTapped:)];
    urlLabelTap.numberOfTapsRequired = 1;
    [_lblCharityURL addGestureRecognizer:urlLabelTap];
}

- (void)URLTapped:(UITapGestureRecognizer*)sender{
    
    NSURL *url = [NSURL URLWithString:charityURL];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - API Call

- (void)callApiToFetchData{

    
    NSDictionary *param = @{@"charityId":_charityID,
                            @"caller":_selectedCallerStr
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserGetCharityDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           [self updateValuesWithData:[responseObject objectForKey:@"responseData"]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}


- (void)updateValuesWithData:(NSDictionary*)data{
    
    //Charity contact Details
    
    _lblCharityName.text = [data objectForKey:@"name"];
    _lblCharityAddress.text   = [data objectForKey:@"address"];
    _lblCharityCountry.text   = [data objectForKey:@"country_name"];
    _lblCharityURL.text       = [data objectForKey:@"url"];
    
    charityURL = [data objectForKey:@"url"];
    
    //Other Details
    
    _lblCharityDesc.text   =  [data objectForKey:@"description"];
    _lblCharityKeyword.text = [data objectForKey:@"keyword"];
    _lblCharityStatus.text  = [data objectForKey:@"status"];
}

@end
