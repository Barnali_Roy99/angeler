//
//  CharityListVCViewController.m
//  Angeler
//
//  Created by AJAY on 18/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "CharityListVCViewController.h"
#import "CharityListCellTableViewCell.h"
#import "CharityDetailsVC.h"


static NSString *CellIdentifier = @"CharityListCellTableViewCell";

@interface CharityListVCViewController ()
{
        NSInteger START_POSITION;
        BOOL moreRecordsAvailable;
        int selectedCountryIndex,selectedSegmentIndex;
    
}


@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblCharityList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, strong) NSMutableArray *arrCharityList;
@property (weak, nonatomic) IBOutlet UIView *viewCountry;
@property (nonatomic, weak) IBOutlet UITextField *txtUserCountry;
@property (nonatomic, strong) NSMutableArray *arrCountry;
@property (weak, nonatomic) IBOutlet UISegmentedControl *charityListSegControl;

@end

@implementation CharityListVCViewController

#pragma mark- View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedCountryIndex = -1;
    selectedSegmentIndex = 0;
    [self setCountryPicker];
    [self callApiToGetCountryList];
     [self initialLoadList];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
}

#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
    [self initialLoadList];
    [refreshController endRefreshing];
}


#pragma mark- General Functions

-(void)initialLoadList{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    selectedCountryIndex = -1;
    _txtUserCountry.text = @"";
    [self callServiceToLoadCharityList];
}

-(void)setCountryPicker
{
    //Country List Set Up
    [_txtUserCountry setInputView:[Helper pickerWithTag:COUNTRY_NAME forControler:self]];
    [_txtUserCountry setInputAccessoryView:[Helper toolbarWithTag:COUNTRY_NAME forControler:self]];
}

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView showPlusBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"List_Of_Charity", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Charity_Found_Msg", nil);
    [_tblCharityList registerNib:[UINib nibWithNibName:@"CharityListCellTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblCharityList.estimatedRowHeight = 20;
    _tblCharityList.rowHeight = UITableViewAutomaticDimension;
   
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblCharityList addSubview:refreshController];
    
    [_viewCountry makeCornerRound:15.0];
    _viewCountry.layer.borderColor = [[[UIColor blackColor] colorWithAlphaComponent:0.5] CGColor];
    _viewCountry.layer.borderWidth = 1.0;
     self.txtUserCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Search by Country", nil) attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    [_charityListSegControl setTitle:NSLocalizedString(@"Approved_Charity", nil) forSegmentAtIndex:0];
     [_charityListSegControl setTitle:NSLocalizedString(@"My_Charity", nil) forSegmentAtIndex:1];
}


-(void)setCountryNameUsingID
{
    
    for(int i = 0 ; i<_arrCountry.count ; i++)
    {
        NSDictionary *tempDic = [_arrCountry objectAtIndex:i];
        if ([[tempDic objectForKey:@"country_id"] isEqualToString: [[GlobalInfo sharedInfo] userSelectedCountryID]])
        {
            _txtUserCountry.text = [tempDic objectForKey:@"country_name"];
            selectedCountryIndex = i;
            break;
        }
    }
    
}



#pragma mark - SEgment Action

- (IBAction)segCharityListBtnTapped:(id)sender {
    if(_charityListSegControl.selectedSegmentIndex==0){
        selectedSegmentIndex = 0;
    }
    else if(_charityListSegControl.selectedSegmentIndex==1){
        selectedSegmentIndex = 1;
    }
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadCharityList];
    
}

-(NSString*)getCallerNameAccordingSegmentIndex{
    NSString *callerStr = @"";
    
    if(selectedSegmentIndex == 0){
        callerStr = @"approved_charity";
    }
    else  if(selectedSegmentIndex == 1){
        callerStr = @"user_specific_charity";
    }
    
    return callerStr;
}

#pragma mark - API Call

- (void)callServiceToLoadCharityList{
    
    NSString *countryId = @"";
    
    if(selectedCountryIndex >= 0){
        countryId = [[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_id"];
    }
    
   
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"caller" : [self getCallerNameAccordingSegmentIndex],
                            @"countryId" : countryId
                            
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserGetCharityList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       if ([GlobalInfo checkStatusSuccess:responseObject])  {
                                                           
                                                           if (_arrCharityList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrCharityList removeAllObjects];
                                                               }else if(_arrCharityList.count > START_POSITION){
                                                                   [_arrCharityList removeObjectsInRange:NSMakeRange(START_POSITION, _arrCharityList.count - START_POSITION)];
                                                               }
                                                               [_arrCharityList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrCharityList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           _lblNoRecord.hidden = _arrCharityList.count > 0;
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601)
                                                           {
                                                               _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                               
                                                           }
                                                           [_tblCharityList reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



- (void)callApiToGetCountryList{
    
    NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserSelectCountryList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           _arrCountry = [NSMutableArray arrayWithArray:arr];
                                                           
                                                         //  [self setCountryNameUsingID];
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

#pragma mark - Scrollview Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //    NSLog(@"currentOffset %f",currentOffset);
    NSLog(@"maximumOffset currentOffset %f %f",maximumOffset,currentOffset);
    
    //NSInteger result = maximumOffset - currentOffset;
    
    CGFloat offsetDifference = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (offsetDifference <= 100.0) {
        if(moreRecordsAvailable)
        {
            NSLog(@"API Called");
            [self callServiceToLoadCharityList];
        }
    }
    
}

#pragma mark - Picker Call

- (void)pickerDoneTapped:(UIButton*)sender{
    
    if (sender.tag == COUNTRY_NAME) {
        
        UIPickerView *piker = (UIPickerView*)_txtUserCountry.inputView;
        NSLog(@"Index picker %ld",[piker selectedRowInComponent:0] - 1);
        if(([piker selectedRowInComponent:0] - 1) >= 0)
        {
             selectedCountryIndex = (int)[piker selectedRowInComponent:0] - 1;
            _txtUserCountry.text = [[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_name"];
            
        }
        else if(([piker selectedRowInComponent:0]) == 0){
            selectedCountryIndex = -1;
            _txtUserCountry.text = @"";
        }
        START_POSITION = 0;
        moreRecordsAvailable = YES;
        [self callServiceToLoadCharityList];
        [_txtUserCountry resignFirstResponder];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        return _arrCountry.count + 1;
    }
    return 0;
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        if (row == 0)
        {
            return NSLocalizedString(@"Choose_Country", nil);
        }
        return [[_arrCountry objectAtIndex:row-1] objectForKey:@"country_name"];
    }
    return Empty;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrCharityList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CharityListCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *data = [_arrCharityList objectAtIndex:indexPath.row];
    
    NSString *countryName = [data objectForKey:@"country_name"];
    cell.lblCharityName.text = [data objectForKey:@"name"];
    cell.lblCountryName.text = countryName;
    cell.lblCharityURL.text = [data objectForKey:@"url"];
    
    cell.btnViewDetails.tag = indexPath.row + 1;
    [cell.btnViewDetails addTarget:self action:@selector(showCharityDetails:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.lblCharityURL.tag = indexPath.row + 1;
    UITapGestureRecognizer *urlLabelTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(URLTapped:)];
    urlLabelTap.numberOfTapsRequired = 1;
    [cell.lblCharityURL addGestureRecognizer:urlLabelTap];
    
    return cell;
}

- (void)showCharityDetails:(UIButton*)sender{
     NSDictionary *data = [_arrCharityList objectAtIndex:sender.tag - 1];
    CharityDetailsVC *next = [[CharityDetailsVC alloc] initWithNibName:@"CharityDetailsVC" bundle:nil];
    next.charityID = [data objectForKey:@"id"];
    next.selectedCallerStr = [self getCallerNameAccordingSegmentIndex];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (void)URLTapped:(UITapGestureRecognizer*)sender{
    
    UIView *lblURL = (UILabel *)sender.view;
    NSDictionary *data = [_arrCharityList objectAtIndex:lblURL.tag - 1];
    NSURL *url = [NSURL URLWithString:[data objectForKey:@"url"]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
