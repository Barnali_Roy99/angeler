//
//  CharityListCellTableViewCell.m
//  Angeler
//
//  Created by AJAY on 18/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "CharityListCellTableViewCell.h"

@implementation CharityListCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    [self.btnViewDetails makeCornerRound:15.0];
    
    [self.containerView makeCornerRound:15.0];
    self.containerView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 0);
    self.containerView.layer.shadowOpacity = 2.0;
    self.containerView.layer.shadowRadius = 2.0;
    
    self.lblCharityURLTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"URL", nil)];
    self.lblCountryNameTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Country", nil)];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
