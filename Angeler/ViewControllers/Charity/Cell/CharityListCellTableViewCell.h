//
//  CharityListCellTableViewCell.h
//  Angeler
//
//  Created by AJAY on 18/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CharityListCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityName;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryName;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityURLTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityURL;
@property (weak, nonatomic) IBOutlet UIButton *btnViewDetails;

@end

NS_ASSUME_NONNULL_END
