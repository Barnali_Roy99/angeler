//
//  AddCharityVC.m
//  Angeler
//
//  Created by AJAY on 25/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AddCharityVC.h"
#import "SharePopUpViewController.h"

@interface AddCharityVC ()<UITextFieldDelegate,UITextViewDelegate,SharePopUpDismissDelegate>
{
    int selectedCountryIndex;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

//Country

@property (weak, nonatomic) IBOutlet UIView *viewCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtUserCountry;
@property (nonatomic, strong) NSMutableArray *arrCountry;


//Charity Name
@property (weak, nonatomic) IBOutlet UIView *viewCharityName;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityNameTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtCharityName;

//Charity Description

@property (weak, nonatomic) IBOutlet UIView *viewCharityDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblCharityDescTitle;
@property (nonatomic, weak) IBOutlet UITextView *txtCharityDesc;

//Keyword

@property (weak, nonatomic) IBOutlet UIView *viewKeyword;
@property (weak, nonatomic) IBOutlet UILabel *lblKeywordTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtKeyword;

//Address

@property (weak, nonatomic) IBOutlet UIView *viewAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressTitle;
@property (nonatomic, weak) IBOutlet UITextView *txtAddress;


//URL

@property (weak, nonatomic) IBOutlet UIView *viewURL;
@property (weak, nonatomic) IBOutlet UILabel *lblURLTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtURL;


//Charity Terms

@property (weak, nonatomic) IBOutlet UISwitch *termsConditionSwitch;
@property (nonatomic, weak) IBOutlet UILabel *termsConditionsTextLabel;

@property (weak, nonatomic) IBOutlet UIView *submitView;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmit;


@end

@implementation AddCharityVC

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - ViewLifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedCountryIndex = -1;
    [self setCountryPicker];
    [self callApiToGetCountryList];
    [self initialSetUp];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}

#pragma mark - Gesture Action

- (void)submitViewTapped:(UITapGestureRecognizer*)sender
{
        if (![self validate]) {
            return;
        }
    
    [self callApiToAAddCharity];
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setLocalization];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:[NSLocalizedString(@"Add_your_Charity", nil) capitalizedString]];
    [_topNavbarView dropShadowWithColor:[UIColor lightGrayColor]];
    
    [self viewSetUp];
    [self setLocalization];
    
}

-(void)setCountryPicker
{
    //Country List Set Up
    [_txtUserCountry setInputView:[Helper pickerWithTag:COUNTRY_NAME forControler:self]];
    [_txtUserCountry setInputAccessoryView:[Helper toolbarWithTag:COUNTRY_NAME forControler:self]];
}

-(void)viewSetUp{
    
    [_mainContentView makeCornerRound:15.0];
    [_viewCountry makeCornerRound:15.0];
    [_viewCharityName makeCornerRound:15.0];
    [_viewCharityDesc makeCornerRound:15.0];
    [_viewKeyword makeCornerRound:15.0];
    [_viewAddress makeCornerRound:15.0];
    [_viewURL makeCornerRound:15.0];
    [_submitView makeCornerRound:15.0];
    
    _termsConditionSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
    [_termsConditionSwitch setOn:NO];
    
    UIColor *color = [[UIColor blackColor] colorWithAlphaComponent:0.5] ;

    self.txtUserCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Choose_Country", nil) attributes:@{NSForegroundColorAttributeName:color}];
  //  self.txtCharityName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Name", nil) attributes:@{NSForegroundColorAttributeName:color}];
   // self.txtCharityDesc.text = NSLocalizedString(@"Description", nil);
   // self.txtKeyword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Keyword", nil) attributes:@{NSForegroundColorAttributeName:color}];
  //  self.txtAddress.text = NSLocalizedString(@"Address", nil);
 //   self.txtURL.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"URL", nil) attributes:@{NSForegroundColorAttributeName:color}];
}


-(void)setLocalization{
    _lblCountryTitle.text = NSLocalizedString(@"Country", nil);
    _lblCharityNameTitle.text = NSLocalizedString(@"Name", nil);
    _lblCharityDescTitle.text = NSLocalizedString(@"Description", nil);
    _lblKeywordTitle.text = NSLocalizedString(@"Keyword", nil);
    _lblAddressTitle.text = NSLocalizedString(@"Address", nil);
    _lblURLTitle.text = NSLocalizedString(@"URL", nil);
    _termsConditionsTextLabel.text = NSLocalizedString(@"Validate_Charity_Msg", nil);
    
}

-(void)setGesture
{
    UITapGestureRecognizer *submitTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped:)];
    [_submitView addGestureRecognizer:submitTapRecognizer];
}

/*"CharityName_Blank_Alert" = "Please enter charity name.";
 "CharityDesc_Blank_Alert" = "Please enter charity description.";
 "CharityKeywords_Blank_Alert" = "Please enter keywords.";
 "CharityAddress_Blank_Alert" = "Please enter charity address.";
 "CharityURL_Blank_Alert" = "Please enter charity URL." */

- (BOOL)validate{
    
    
    if ([Helper checkEmptyField:_txtUserCountry.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Select_Country_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtCharityName.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CharityName_Blank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtCharityDesc.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CharityDesc_Blank_Alert", nil)];
        return NO;
    }
    if ([Helper checkEmptyField:_txtKeyword.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CharityKeywords_Blank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtAddress.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CharityAddress_Blank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtURL.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CharityURL_Blank_Alert", nil)];
        return NO;
    }
   
    if (![_termsConditionSwitch isOn])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Validate_Charity_Accept_Alert", nil)];
        return NO;
    }
    
    return YES;
}

#pragma mark - API Call

- (void)callApiToGetCountryList{
    
    NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserSelectCountryList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           _arrCountry = [NSMutableArray arrayWithArray:arr];
                                                           
                                        
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

- (void)callApiToAAddCharity{
    
    NSDictionary *param = @{@"countryId":[[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_id"],
                            @"name"        :_txtCharityName.text,
                            @"description" :_txtCharityDesc.text,
                            @"keyword"     :_txtKeyword.text,
                            @"address"     :_txtAddress.text,
                            @"url"         :_txtURL.text
                            
                            
                            };
    
    
    //countryId, name, description, keyword, address, url
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserAddCharity
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                       
                                                           [self showAlertPopUp];
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}


#pragma mark - Picker Call

- (void)pickerDoneTapped:(UIButton*)sender{
    
    if (sender.tag == COUNTRY_NAME) {
        
        UIPickerView *piker = (UIPickerView*)_txtUserCountry.inputView;
        
        if(([piker selectedRowInComponent:0] - 1) >= 0)
        {
            selectedCountryIndex = (int)[piker selectedRowInComponent:0] - 1;
            _txtUserCountry.text = [[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_name"];
            
        }
        [_txtUserCountry resignFirstResponder];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        return _arrCountry.count + 1;
    }
    return 0;
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        if (row == 0)
        {
            return NSLocalizedString(@"Choose_Country", nil);
        }
        return [[_arrCountry objectAtIndex:row-1] objectForKey:@"country_name"];
    }
    return Empty;
}

#pragma mark - Share Pop Up show

-(void)showAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Thanks", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Add_Charity_Submit_Alert", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    [self.navigationController popViewControllerAnimated:false];
}




@end
