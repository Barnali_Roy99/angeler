//
//  ChangePasswordViewController.m
//  Angeler
//
//  Created by AJAY on 03/07/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController
@synthesize topBarHeightConstraint,topNavbarView,containerView,txtNewPassword,txtCurrentpassword,txtConfirmNewPassword,viewCurrentPassword,viewNewPassword,viewConfirmNewPassword,updateView,updateLabel;


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}


#pragma mark - General methods

-(void)initialSetUp
{
    [self setPlaceHolderText];
    [self setGesture];
    
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavMenuBtn];
    [topNavbarView setTitle:NSLocalizedString(@"Change_My_Password", nil)];
    
    [containerView makeCornerRound:15.0];
    [viewCurrentPassword makeCornerRound:15.0];
    [viewNewPassword makeCornerRound:15.0];
    [viewConfirmNewPassword makeCornerRound:15.0];
    
    [updateView makeCornerRound:15.0];
    [updateView dropShadowWithColor:[UIColor lightishGrayColor]];
    updateLabel.text = [NSLocalizedString(@"Update", nil) uppercaseString];
    
}

-(void)clearText
{
    self.txtCurrentpassword.text = @"";
    self.txtConfirmNewPassword.text = @"";
    self.txtNewPassword.text = @"";
}

-(void)setPlaceHolderText{
    
    //Localization
    
    UIColor *color = [UIColor lightGrayColor];
    self.txtCurrentpassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Current_Password", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtNewPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Your_New_Password", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtConfirmNewPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Confirm_New_Password", nil) attributes:@{NSForegroundColorAttributeName: color}];
}

- (BOOL)validate{
    
  if ([Helper checkEmptyField:txtCurrentpassword.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Current_Password_Blank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:txtNewPassword.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"New_Password_Blank_Alert", nil)];
        return NO;
    }
    
    if (txtNewPassword.text.length < 8 ) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"PasswordLenght_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:txtConfirmNewPassword.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Confirm_New_Password_Blank_Alert", nil)];
        return NO;
    }
    
    
    
    if (![txtNewPassword.text isEqualToString:txtConfirmNewPassword.text])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Current_New_Password_Same_Alert", nil)];
        return NO;
    }
    
    return YES;
}

#pragma mark - Gesture Action

-(void)setGesture{
    
    UITapGestureRecognizer *updateTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateViewTapped:)];
    [updateView addGestureRecognizer:updateTapRecognizer];
}

- (void)updateViewTapped:(UITapGestureRecognizer*)sender
{
    if ([self validate])
    {
        [self callChangePasswordAPI];
    }
}

-(void)callChangePasswordAPI
{
    NSDictionary *param = @{@"oldPassword":txtCurrentpassword.text,@"newPassword":txtNewPassword.text,@"confirmPassword":txtConfirmNewPassword.text,};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kChangePassword
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSLog(@"response Change Password ===   %@",responseObject) ;                                                                         [Helper showAlertWithTitle:Empty Message:[[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"]];
                                                           
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           if (statusCode == 200)
                                                           {
                                                               [self clearText];
                                                           }
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       NSLog(@"failure response ===   %@",responseObject);
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                      
                                                   }];
    
}

@end
