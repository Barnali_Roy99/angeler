//
//  ChangePasswordViewController.h
//  Angeler
//
//  Created by AJAY on 03/07/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangePasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *viewCurrentPassword;
@property (weak, nonatomic) IBOutlet UIView *viewNewPassword;
@property (weak, nonatomic) IBOutlet UIView *viewConfirmNewPassword;


@property (nonatomic, weak) IBOutlet UITextField *txtCurrentpassword;
@property (nonatomic, weak) IBOutlet UITextField *txtNewPassword;
@property (nonatomic, weak) IBOutlet UITextField *txtConfirmNewPassword;

@property (weak, nonatomic) IBOutlet UIView *updateView;
@property (weak, nonatomic) IBOutlet UILabel *updateLabel;


@end

NS_ASSUME_NONNULL_END
