//
//  MultipleLocationMapViewController.m
//  Angeler
//
//  Created by AJAY on 11/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "MultipleLocationMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "SelectedAddressCell.h"

@interface MultipleLocationMapViewController ()<GMSMapViewDelegate,GMSAutocompleteResultsViewControllerDelegate>{
    GMSAutocompleteResultsViewController *_resultsViewController;
    UISearchController *_searchController;
}
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectAddress;
@property (weak, nonatomic) IBOutlet UITableView *tblSelectedAddress;

@property (nonatomic, strong) NSArray *arrMarker;
@property (nonatomic) BOOL firstTimeLoad;

@end

static NSString *CellIdentifier = @"selectedAddressCell";

@implementation MultipleLocationMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tblSelectedAddress registerNib:[UINib nibWithNibName:@"SelectedAddressCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    if (!self.arrSelStorAddress) {
        self.arrSelStorAddress = [[NSMutableArray alloc] init];
    }
    // Do any additional setup after loading the view from its nib.
    
    [Helper addShadowToLayer:_headerView.layer];
    
    _mapView.delegate = self;
    [[AppDel shareModel] startMonitoringLocation];
    NSNumber *lat = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
    NSNumber *longt = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
    
    if ([self.addressObj.lat_Value doubleValue] != 0.0) {
        lat = self.addressObj.lat_Value;
    }
    if ([self.addressObj.long_Value doubleValue] != 0.0) {
        longt = self.addressObj.long_Value;
    }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[lat doubleValue] longitude:[longt doubleValue] zoom:12];
    _mapView.camera = camera;
    
    //[self addMarkerToMapViewAtCameraPosition];
    
    //[[AppDel shareModel] myLocation].latitude
   // [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",[[AppDel shareModel] myLocation].latitude] longitude:[NSString stringWithFormat:@"%f",[[AppDel shareModel] myLocation].longitude]];
    
    
    _resultsViewController = [[GMSAutocompleteResultsViewController alloc] init];
    _resultsViewController.delegate = self;
    
    //    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    //    filter.type = kGMSPlacesAutocompleteTypeFilterEstablishment;
    // filter.country = @"AE";
    
    //    _resultsViewController.autocompleteFilter = filter;
    
    _searchController = [[UISearchController alloc]
                         initWithSearchResultsController:_resultsViewController];
    _searchController.searchResultsUpdater = _resultsViewController;
    
    [self.containerView addSubview:_searchController.searchBar];
    
    
   // [self callServiceToLoadMarkedPoints];
}
- (void)callServiceToLoadMarkedPoints{
    //kDeliveryPointsList
    //userId=5&sessionId=153416b43dade724218fa6e012087472f87ed73a&os=Android&osVersion=M&appVersion=1.0&countryId=1&cityId=1&caller=PP&latitude=22.568667&longitude=88.4355043
    
    NSString *caller = Empty;
    NSString *latValue = Empty;
    NSString *longValue = Empty;
    if (_firstTimeLoad) {
        latValue = [NSString stringWithFormat:@"%f",_mapView.camera.target.latitude];
        longValue = [NSString stringWithFormat:@"%f",_mapView.camera.target.longitude];
    }else{
//        latValue = [self.addressObj.lat_Value stringValue];
//        longValue = [self.addressObj.long_Value stringValue];
    }
    
    if (self.tag == PICK_PICKUP_POINT_ADDRESS) {
        caller = @"AP";
    }else if (self.tag == DELIVERY_PICKUP_POINT_ADDRESS){
        caller = @"AD";
    }else if (self.tag == PACKAGE_FROM){
        caller = @"PP";
        latValue = [self.addressObj.lat_Value stringValue];
        longValue = [self.addressObj.long_Value stringValue];
    }else if (self.tag == PACKAGE_TO){
        caller = @"PD";
        latValue = [self.addressObj.lat_Value stringValue];
        longValue = [self.addressObj.long_Value stringValue];
    }
    
    NSDictionary *param = @{@"countryId":self.addressObj.countryId,
                            @"cityId":self.addressObj.cityId,
                            @"cityName":self.addressObj.cityName,
                            @"countryName":self.addressObj.countryName,
                            @"caller":caller,
                            @"latitude":latValue,
                            @"longitude":longValue
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryPointsList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
               if ([GlobalInfo checkStatusSuccess:responseObject]) {
                   _arrMarker = [responseObject objectForKey:@"responseData"];
                   [self focusMapToShowAllMarkers];
               }
           }
           failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
           }];
}

- (void)focusMapToShowAllMarkers
{
     [_mapView clear];
    
    NSMutableArray *arrMarkerLocation = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in _arrMarker) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([[dict objectForKey:@"latitude"] doubleValue],[[dict objectForKey:@"longitude"] doubleValue]);
        marker.map = _mapView;
        marker.icon = [UIImage imageNamed:@"store_location"];
        [arrMarkerLocation addObject:marker];
    }

    for (AddressClass *addObj in self.arrSelStorAddress) {
        // Creates a marker in the center of the map.
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([addObj.lat_Value doubleValue],[addObj.long_Value doubleValue]);
        marker.zIndex = 100;
        marker.map = _mapView;
        marker.icon = [UIImage imageNamed:@"store_marker"];
        [arrMarkerLocation addObject:marker];
    }

    
    NSLog(@"marker count ===  %lu",(unsigned long)arrMarkerLocation.count);
    
    if (arrMarkerLocation.count == 1) {
       // return;
    }else if (arrMarkerLocation.count == 2){
        GMSMarker *mrk1 = [arrMarkerLocation firstObject];
        GMSMarker *mrk2 = [arrMarkerLocation lastObject];
        if ((mrk1.position.longitude == mrk2.position.longitude) && (mrk1.position.latitude == mrk2.position.latitude)) {
            //return;
        }
    }
    
    if (!self.firstTimeLoad) {
        self.firstTimeLoad = YES;
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        for (GMSMarker *marker in arrMarkerLocation)
            bounds = [bounds includingCoordinate:marker.position];
        
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100]];
    }
}


-(void)viewDidAppear:(BOOL)animated{
    //Super
    [super viewDidAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowForChat:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideForChat:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)viewDidDisappear:(BOOL)animated{
    //Super
    [super viewDidDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    _lblSelectAddress.hidden = (self.arrSelStorAddress.count>0);
    
    return self.arrSelStorAddress.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectedAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    AddressClass *addresObj = [self.arrSelStorAddress objectAtIndex:indexPath.row];
    cell.lblAddress.text = addresObj.Address;
    cell.btnDelete.tag = indexPath.row+1;
    [cell.btnDelete addTarget:self action:@selector(deleteTheSelectedAddressRow:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)deleteTheSelectedAddressRow:(UIButton*)sender{
    [self.arrSelStorAddress removeObjectAtIndex:sender.tag-1];
    [_tblSelectedAddress reloadData];
    [self focusMapToShowAllMarkers];
}


- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    NSLog(@"camera change====");
    //    double latitude = mapView.camera.target.latitude;
    //    double longitude = mapView.camera.target.longitude;
    //
    //    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
    
    
}
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    NSLog(@"camera change due tap====");
}
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
//    double latitude = mapView.camera.target.latitude;
//    double longitude = mapView.camera.target.longitude;
    
    //[self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
    //here call service to fetch marker list
//    [self addMarkerToMapViewAtCameraPosition];
    if (!self.firstTimeLoad) {
        [self callServiceToLoadMarkedPoints];
    }
}
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    for (NSDictionary *dict in _arrMarker) {
        if (([[dict objectForKey:@"latitude"] doubleValue] == marker.position.latitude) &&
            ([[dict objectForKey:@"longitude"] doubleValue] == marker.position.longitude)) {
            
            
            BOOL notFound = YES;
            
            for (AddressClass *address in self.arrSelStorAddress) {
                if ([address.addresId isEqualToString:[dict objectForKey:@"deliveryPointId"]]) {
                    notFound = NO;
                    [Helper showAlertWithTitle:Empty Message:strKey_placepick_This_place_has_been_added];
                }
            }
            if (notFound) {
                
                if (self.arrSelStorAddress.count > MAX_STORE_SELECT) {
                    [Helper showAlertWithTitle:Empty Message:[NSString stringWithFormat:@"%@ %d %@",strKey_placepick_Maximum,MAX_STORE_SELECT+1,strKey_placepick_location_can_be_added]];
                    return NO;
                }
                
                AddressClass *addressObj = [[AddressClass alloc] init];
                addressObj.Address = [dict objectForKey:@"address"];
                addressObj.addressType = [dict objectForKey:@"addressType"];
                addressObj.addresId = [dict objectForKey:@"deliveryPointId"];
                addressObj.operatingFromHrs = [dict objectForKey:@"operatingFromHours"];
                addressObj.operatingToHrs = [dict objectForKey:@"operatingToHours"];
                
                addressObj.lat_Value = [NSNumber numberWithDouble:marker.position.latitude];
                addressObj.long_Value = [NSNumber numberWithDouble:marker.position.longitude];
                
                [self.arrSelStorAddress addObject:addressObj];
                
                [_tblSelectedAddress reloadData];
                [self focusMapToShowAllMarkers];
            }
        }
    }
    return NO;
}




// Handle the user's selection.
- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
 didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place name %@", place.addressComponents);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSLog(@"Place coor %f    %f ", place.coordinate.latitude,place.coordinate.longitude);
    
    _mapView.camera = [GMSCameraPosition cameraWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude zoom:12];
        
}

- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


- (IBAction)BackAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)NextAction:(id)sender{
    if (self.arrSelStorAddress.count > 0) {
        [self.delegate selectedAddressArray:self.arrSelStorAddress forTag:_tag];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark
#pragma mark Keyboard manipulation
- (void)keyboardWillHideForChat:(NSNotification *)note

{
    
    [UIView animateWithDuration:0.25 animations:^{
        
        [self.containerView setTransform:CGAffineTransformIdentity];
    }];
    
}


- (void) keyboardWillShowForChat:(NSNotification *)note
{
    
    [UIView animateWithDuration:0.25 animations:^{
        
        [self.containerView setTransform:CGAffineTransformMakeTranslation(0, -self.containerView.frame.origin.y)];
    }];
    
}

/*
- (void)callRevGeoCodingWithLatitude:(NSString*)latitude longitude:(NSString*)longitude{
    NSDictionary *param = @{@"lat":latitude,@"long":longitude
                            };
    
    //Username=merchant@gmail.com&password=merchant@spotdeal&devicetype=android&deviceRegId=98772626262
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGoogleMapApi
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
               NSArray *array = [responseObject objectForKey:@"results"];
               if ([array count] > 0)
               {
                   
                   
                   NSArray *addressComponents = [[array objectAtIndex:0] objectForKey:@"address_components"];
                   // self.formattedAddress=[NSString stringWithFormat:@"%@",[[array objectAtIndex:0] objectForKey:@"formatted_address"]];
                   
                   NSString *_CurrentLocationCity;
                   NSString *LocationCountry;
                   NSString *FormatedAddress = [[array objectAtIndex:0] objectForKey:@"formatted_address"];;
                   
                   for (NSDictionary *dict in addressComponents)
                   {
                       NSArray *types = [dict objectForKey:@"types"];
                       
                       if ([types containsObject:@"locality"] || [types containsObject:@"administrative_area_level_2"]||[types containsObject:@"administrative_area_level_3"])
                       {
                           if([types containsObject:@"locality"]){
                               _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                               break;
                           }else if ([types containsObject:@"administrative_area_level_3"]){
                               _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                               break;
                           }
                           else if ([types containsObject:@"administrative_area_level_2"]){
                               
                               _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                               break;
                           }
                       }
                   }
                   for (NSDictionary *dict in addressComponents)
                   {
                       NSArray *types = [dict objectForKey:@"types"];
                       
                       if ([types containsObject:@"country"])
                       {
                           LocationCountry=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                       }
                   }
                   //_lblAddress.text = FormatedAddress;

                   AddressClass *addressObj = [[AddressClass alloc] init];
                   addressObj.Address = FormatedAddress;
                   addressObj.countryName = LocationCountry;
                   addressObj.cityName = _CurrentLocationCity;
                   addressObj.lat_Value = [NSNumber numberWithDouble:[latitude doubleValue]];
                   addressObj.long_Value = [NSNumber numberWithDouble:[longitude doubleValue]];
                   
                   if (self.arrSelStorAddress.count > 0) {
                       BOOL alreadyExist = NO;
                       for (AddressClass *adObj in self.arrSelStorAddress) {
                           if ([FormatedAddress isEqualToString:adObj.Address]) {
                               alreadyExist = YES;
                           }
                       }
                       if (!alreadyExist) {
                           [self.arrSelStorAddress addObject:addressObj];
                       }
                   }else{
                       [self.arrSelStorAddress addObject:addressObj];
                   }
                   [_tblSelectedAddress reloadData];
                   
                   NSLog(@"Temp arr ===  %@   \n %@  \n %@",_CurrentLocationCity,LocationCountry,FormatedAddress);
                   
               }               
           }
           failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:strKey_error_wentwrong];
           }];
    
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
