//
//  SelectedAddressCell.h
//  Angeler
//
//  Created by AJAY on 14/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@end
