//
//  SingleLocationMapController.h
//  Angeler
//
//  Created by AJAY on 09/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SingleLocationAddressDelegate <NSObject>

- (void)selectedAddress:(AddressClass*)address forTag:(TAG_VALUE)tag;

@end
@interface SingleLocationMapController : UIViewController
@property (nonatomic) TAG_VALUE tag;
@property (nonatomic, weak) id <SingleLocationAddressDelegate> delegate;
@property (nonatomic, strong) NSNumber *prevlat,*prevLong;
@property (weak, nonatomic) IBOutlet UIView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (nonatomic, strong) AddressClass *addressObj;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonOK;
@property (weak, nonatomic) IBOutlet UILabel *selectedAddressBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectAddressTitle;
- (IBAction)onBackBtnClicked:(id)sender;

@end
