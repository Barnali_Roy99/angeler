//
//  SelectedAddressCell.m
//  Angeler
//
//  Created by AJAY on 14/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "SelectedAddressCell.h"

@implementation SelectedAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
