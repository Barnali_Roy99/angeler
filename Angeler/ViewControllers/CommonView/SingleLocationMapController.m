//
//  SingleLocationMapController.m
//  Angeler
//
//  Created by AJAY on 09/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "SingleLocationMapController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>

@interface SingleLocationMapController ()<GMSMapViewDelegate,GMSAutocompleteResultsViewControllerDelegate>{
    GMSAutocompleteResultsViewController *_resultsViewController;
    UISearchController *_searchController;
}

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@end

@implementation SingleLocationMapController
@synthesize topNavbarView,topBarHeightConstraint;

- (void)viewDidLoad {
    [super viewDidLoad];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.addressObj = [[AddressClass alloc] init];
    // Do any additional setup after loading the view from its nib.
    [self initialSetUp];
    
    _mapView.delegate = self;
    [[AppDel shareModel] startMonitoringLocation];
    
    [Helper addShadowToLayer:_headerView.layer];
    
    NSNumber *lat = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
    NSNumber *longt = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
    
    [_selectedAddressBackgroundView makeCornerRound:10.0];
    [_selectedAddressBackgroundView dropShadowWithColor:[UIColor lightishGrayColor]];
    
    if ([_prevlat doubleValue] != 0.0 && [_prevLong doubleValue] != 0.0) {
        lat = _prevlat;
        longt = _prevLong;
    }
//    else
//    {
//        [[AppDel shareModel] isLocationEnabled];
//    }
//
//
//    NSLog(@"Lat Value Single map : %@",lat);
//    NSLog(@"Long Value Single map : %@",longt);
//
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[lat doubleValue] longitude:[longt doubleValue] zoom:12];
    _mapView.camera = camera;
    
    [self addMarkerToMapViewAtCameraPosition];
    
    //[[AppDel shareModel] myLocation].latitude
    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%@",lat] longitude:[NSString stringWithFormat:@"%@",longt]];
    
    
    _resultsViewController = [[GMSAutocompleteResultsViewController alloc] init];
    _resultsViewController.delegate = self;
    
    //    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    //    filter.type = kGMSPlacesAutocompleteTypeFilterEstablishment;
    // filter.country = @"AE";
    
    //    _resultsViewController.autocompleteFilter = filter;
    
    _searchController = [[UISearchController alloc]
                         initWithSearchResultsController:_resultsViewController];
    _searchController.searchResultsUpdater = _resultsViewController;
    
    [self.containerView addSubview:_searchController.searchBar];
}


-(void)viewDidAppear:(BOOL)animated
{
    //Super
    [super viewDidAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowForChat:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideForChat:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)addMarkerToMapViewAtCameraPosition{
    [_mapView clear];
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = [_mapView.camera target];// CLLocationCoordinate2DMake(22.40454,88.52894);
    marker.title = @"My Location";
    marker.map = _mapView;
    marker.icon = [UIImage imageNamed:@"LocationPin"];

}
- (void)viewDidDisappear:(BOOL)animated
{
    //Super
    [super viewDidDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
//    double latitude = mapView.camera.target.latitude;
//    double longitude = mapView.camera.target.longitude;
//    
//    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
    
    
}

- (IBAction)onBackBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - General Functions

-(void)initialSetUp
{

    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    _titleLabel.text = NSLocalizedString(@"Pick_Location", nil);
    [_buttonOK setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
    _lblSelectAddressTitle.text = NSLocalizedString(@"Selected_Address﻿", nil);
    _lblAddress.text = NSLocalizedString(@"Pick_Your_Address", nil);
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    
    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
    [self addMarkerToMapViewAtCameraPosition];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)SearchAction:(id)sender {
}

// Handle the user's selection.
- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
 didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place name %@", place.addressComponents);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSLog(@"Place coor %f    %f ", place.coordinate.latitude,place.coordinate.longitude);
    
    //[self addressFromLatitude:[NSNumber numberWithDouble:place.coordinate.latitude] longitude:[NSNumber numberWithDouble:place.coordinate.longitude]];
    _mapView.camera = [GMSCameraPosition cameraWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude zoom:12];
   // _mapView.camera.target = place.coordinate;
  
    
    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",place.coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",place.coordinate.longitude]];
    
    
    //[_mapObj UpdateTheLocationWithLat:place.coordinate.latitude longval:place.coordinate.longitude];
    
}

- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


- (IBAction)BackAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)NextAction:(id)sender{
    if (self.addressObj.Address != nil) {
        [self.delegate selectedAddress:self.addressObj forTag:_tag];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark
#pragma mark Keyboard manipulation
- (void)keyboardWillHideForChat:(NSNotification *)note

{
    
    [UIView animateWithDuration:0.25 animations:^{
        
        [self.containerView setTransform:CGAffineTransformIdentity];
    }];
    
}


- (void) keyboardWillShowForChat:(NSNotification *)note
{
    
    [UIView animateWithDuration:0.25 animations:^{
        
        [self.containerView setTransform:CGAffineTransformMakeTranslation(0, -self.containerView.frame.origin.y)];
    }];
    
}


- (void)callRevGeoCodingWithLatitude:(NSString*)latitude longitude:(NSString*)longitude{
    NSDictionary *param = @{@"lat":latitude,@"long":longitude
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kGoogleMapApi
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
        success:^(id responseObject){
         
          
                NSLog(@"response country API ===   %@",responseObject);
                NSArray *array = [responseObject objectForKey:@"results"];
                if ([array count] > 0)
                {
                    NSArray *addressComponents = [[array objectAtIndex:0] objectForKey:@"address_components"];
                    
                    NSString *_CurrentLocationCity;
                    NSString *LocationCountry;
                    NSString *FormatedAddress = [[array objectAtIndex:0] objectForKey:@"formatted_address"];;
                    
                    for (NSDictionary *dict in addressComponents)
                    {
                        NSArray *types = [dict objectForKey:@"types"];
                        
                        if ([types containsObject:@"locality"] || [types containsObject:@"administrative_area_level_2"]||[types containsObject:@"administrative_area_level_3"])
                        {
                            if([types containsObject:@"locality"]){
                                _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                                break;
                            }else if ([types containsObject:@"administrative_area_level_3"]){
                                _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                                break;
                            }
                            else if ([types containsObject:@"administrative_area_level_2"]){
                                
                                _CurrentLocationCity=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                                break;
                            }
                        }
                    }
                    for (NSDictionary *dict in addressComponents)
                    {
                        NSArray *types = [dict objectForKey:@"types"];
                        
                        if ([types containsObject:@"country"])
                        {
                            LocationCountry=[NSString stringWithFormat:@"%@", [dict objectForKey:@"long_name"]];
                        }
                    }
                    _lblAddress.text = FormatedAddress;
                    //                _searchController.searchBar.text = FormatedAddress;
                    
                    self.addressObj.Address = FormatedAddress;
                    self.addressObj.countryName = LocationCountry;
                    self.addressObj.cityName = _CurrentLocationCity?_CurrentLocationCity:@"";
                    self.addressObj.lat_Value = [NSNumber numberWithDouble:[latitude doubleValue]];
                    self.addressObj.long_Value = [NSNumber numberWithDouble:[longitude doubleValue]];
                    NSLog(@"Temp arr ===  %@   \n %@  \n %@",_CurrentLocationCity,LocationCountry,FormatedAddress);
                }
          
         }
        failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
           }];

}




@end
