//
//  MultipleLocationMapViewController.h
//  Angeler
//
//  Created by AJAY on 11/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MultipleLocationAddressDelegate <NSObject>

- (void)selectedAddressArray:(NSMutableArray*)Arr_address forTag:(TAG_VALUE)tag;

@end

@interface MultipleLocationMapViewController : UIViewController
@property (nonatomic) TAG_VALUE tag;
@property (nonatomic,strong) AddressClass *addressObj;
@property (nonatomic, strong) NSMutableArray *arrSelStorAddress;
@property (nonatomic, weak) id <MultipleLocationAddressDelegate> delegate;
@end
