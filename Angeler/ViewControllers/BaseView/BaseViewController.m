//
//  BaseViewController.m
//  Angeler
//
//  Created by AJAY on 23/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "BaseViewController.h"
#import "SideMenuViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    //Add Swipe gesture
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:rightSwipe];
    
    // Do any additional setup after loading the view.
}
- (IBAction)sideMenuTapped:(id)sender {
    [AppDel openSideViewFromViewController:self];
}
#pragma mark -
#pragma mark - Right Swipe
- (void)swipeRight:(UISwipeGestureRecognizer*)swipeGest{
    [AppDel openSideViewFromViewController:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
