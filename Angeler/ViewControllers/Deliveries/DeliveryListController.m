//
//  DeliveryListController.m
//  Angeler
//
//  Created by AJAY on 12/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "DeliveryListController.h"
#import "DeliveryListCell.h"
#import "DeliveryDetailController.h"
#import "MWPhotoBrowser.h"


static NSString *CellIdentifier = @"deliveryCell";

@interface DeliveryListController ()<MWPhotoBrowserDelegate>{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;;
}

@property (weak, nonatomic) IBOutlet UITableView *tblDeliveryList;
@property (nonatomic, strong) NSMutableArray *arrDeliveryList;
@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;

@end

@implementation DeliveryListController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self callApiToLoadDeliveryList];
}


#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToLoadDeliveryList];
     [refreshController endRefreshing];
}


#pragma mark - General Functions

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView setTitle:[NSLocalizedString(@"Deliveries﻿", nil) capitalizedString]];
    
    //Table View Set Up
    [_tblDeliveryList registerNib:[UINib nibWithNibName:@"DeliveryListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    
    _tblDeliveryList.estimatedRowHeight = 192;
    _tblDeliveryList.rowHeight = UITableViewAutomaticDimension;
    
    [_tblDeliveryList setNeedsLayout];
    [_tblDeliveryList layoutIfNeeded];
    
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblDeliveryList addSubview:refreshController];
    
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
}

#pragma mark - Webservice Call

- (void)callApiToLoadDeliveryList{
    /*
     Request type: query string
     Request parameter:
     userId, sessionId, os, osVersion, appVersion, userEmail, packageId
     
     Request example :
     http://103.15.232.68/~angeler/web_services/recipient/delivery_details?userId=6&sessionId=a22fd561cdc365388b103be95c9ede6352bb69e&os=Android&osVersion=M&appVersion=1.0&packageId=10&userEmail=amit1@gmail.com
     */
//    Request parameter:
//    userId, sessionId, os, osVersion, appVersion, userEmail, start, offset
//    
//    Request example :
//http://103.15.232.68/~angeler/web_services/recipient/package_list?userId=1&sessionId=ff44e076a4fcc07577cf28597499faa622020f48&os=Android&osVersion=M&appVersion=1.0&start=0&offset=1&userEmail=amit1@gmqul.vom
    
    
    NSDictionary *param = @{@"userEmail":[[GlobalInfo sharedInfo] userEmail],
                            @"start":[NSString stringWithFormat:@"%ld",(long)START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           
           NSArray *arr = [responseObject objectForKey:@"responseData"];
           if ((arr.count == 0) && (START_POSITION != 0)){
               moreRecordsAvailable = NO;
               return ;
           }
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
           
               if (_arrDeliveryList.count > 0) {
                   if (START_POSITION == 0) {
                       [_arrDeliveryList removeAllObjects];
                   }else if(_arrDeliveryList.count > START_POSITION){
                       [_arrDeliveryList removeObjectsInRange:NSMakeRange(START_POSITION, _arrDeliveryList.count - START_POSITION)];
                   }
                   [_arrDeliveryList addObjectsFromArray:arr];
               }else{
                   _arrDeliveryList = [NSMutableArray arrayWithArray:arr];
               }
               START_POSITION = START_POSITION + [arr count];
               _lblNoRecord.hidden = _arrDeliveryList.count > 0;
               [_tblDeliveryList reloadData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //return 5;
    return _arrDeliveryList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeliveryListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *data = [_arrDeliveryList objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return [self manageCell:cell withData:data atIndex:indexPath.row];
}

- (DeliveryListCell*)manageCell:(DeliveryListCell *)cell withData:(NSDictionary*)data atIndex:(NSInteger)index{
    
    
    [cell.containerView makeCornerRound:15.0];
    [cell.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [cell.packageStatusActionBtn makeCornerRound:15.0];
    
    cell.lblDeliveryDateTitle.text = NSLocalizedString(@"Deliver_Before", nil);
    cell.lblSenderFromTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"From", nil)];
    
    
    //Address Set
    
    cell.fromAddressLabel.text = [data objectForKey:@"fromAddress"];
    cell.toAddressLabel.text = [data objectForKey:@"toAddress"];
    
    //Shipment image set
    
    NSArray *packageImages = [data objectForKey:@"images"];
    NSDictionary *firstImageDic = [packageImages objectAtIndex:0];
    
    UITapGestureRecognizer *documentImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(documentImageViewTapped:)];
    documentImageViewTap.numberOfTapsRequired = 1;
    [cell.shipmentImgBackgroundView addGestureRecognizer:documentImageViewTap];
    
    cell.shipmentImgBackgroundView.tag = index + 1;
    [cell.shipmentImageView sd_setImageWithURL:[NSURL URLWithString: [firstImageDic objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
    cell.shipmentImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    cell.shipmentImageView.layer.borderWidth = 1.0;
    
    //Package Weight Set
    cell.lblDocumentWeight.text = [NSString stringWithFormat:@"%@ | %@ %@",[GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]],[data objectForKey:@"packageWeight"],NSLocalizedString(@"package_weight_unit", nil)];
    
    
    //Delivery Date set
    cell.lblDeliveryDate.text = [Helper getDateTimeToStringWithDate:[data objectForKey:@"deliveryDate"] Time:[data objectForKey:@"deliveryTime"]];
    
    //Angeler Profile Image set
    
    [cell.angelerProfileImageview sd_setImageWithURL:[NSURL URLWithString: [data objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
    UITapGestureRecognizer *angelerImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(angelerImageViewTapped:)];
    angelerImageViewTap.numberOfTapsRequired = 1;
    [cell.angelerProfileImageview addGestureRecognizer:angelerImageViewTap];
    cell.angelerProfileImageview.tag = index + 1;
    
    cell.angelerProfileImageview.layer.borderColor = [[UIColor blackColor] CGColor];
    cell.angelerProfileImageview.layer.borderWidth = 1.0;
    
    
    //Action Button Set
    [cell.packageStatusActionBtn setTitle:[data objectForKey:@"statusText"] forState:UIControlStateNormal];
    cell.packageStatusActionBtn.tag = index + 1;
    [cell.packageStatusActionBtn addTarget:self action:@selector(showInfo:) forControlEvents:UIControlEventTouchUpInside];
    
    //Sender name set
    cell.lblSenderName.text = [data objectForKey:@"senderName"];
    
    //Premium / Meeting point icon set
    
    if ([[data objectForKey:@"serviceType"] isEqualToString:serviceType_Premium])
    {
        [cell.meetingPointPremiumImageView setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
    }
    else
    {
        [cell.meetingPointPremiumImageView setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
    }

    //Load More Data
    
    if (index == [self.arrDeliveryList count] - 1 && moreRecordsAvailable)
    {
        [self callApiToLoadDeliveryList];
    }
    
   
    return cell;
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

#pragma mark - Table view Action

- (void)documentImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *documentView = (UIView *)sender.view;
    NSDictionary *data = [_arrDeliveryList objectAtIndex:documentView.tag - 1];
    
    NSArray *packageImages = [data objectForKey:@"images"];
    self.arrPhotos = [[NSMutableArray alloc] init];
    [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[packageImages objectAtIndex:0] objectForKey:@"image"]]]];
    [self setMWPHOtoBrowser];
}

- (void)showInfo:(UIButton*)sender{
    START_POSITION = sender.tag-1;
    NSDictionary *data = [_arrDeliveryList objectAtIndex:sender.tag-1];
    DeliveryDetailController *next = [[DeliveryDetailController alloc] initWithNibName:@"DeliveryDetailController" bundle:nil];
    next.packageId = [data objectForKey:@"packageId"];
    [self.navigationController pushViewController:next animated:YES];
}

- (void)angelerImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIImageView *angelerImageView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrDeliveryList objectAtIndex:angelerImageView.tag - 1];
    self.arrPhotos = [[NSMutableArray alloc] init];
    MWPhoto *angelerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerImage"]]];
    angelerPhoto.caption =[NSString stringWithFormat:@"Angeler : %@",[data objectForKey:@"angelerName"]];
    
    [self.arrPhotos addObject:angelerPhoto];
    [self setMWPHOtoBrowser];
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


//========================= Old + Reuse Code ================================

/*
 
 //@property (weak, nonatomic) IBOutlet UIView *headerView;
 
 #pragma mark - Scrollview Delegate
 - (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
 
 // UITableView only moves in one direction, y axis
 CGFloat currentOffset = scrollView.contentOffset.y;
 CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
 
 //NSInteger result = maximumOffset - currentOffset;
 
 // Change 10.0 to adjust the distance from bottom
 if (maximumOffset - currentOffset <= 5.0) {
 if(moreRecordsAvailable)
 {
 [self callApiToLoadDeliveryList];
 }
 }
 } */

//
//    cell.lblPackDateTime.text = [NSString stringWithFormat:@"%@,%@",[data objectForKey:@"deliveryDate"],[data objectForKey:@"deliveryTime"]];
//    cell.lblFromAddress.text = [data objectForKey:@"fromAddress"];
//    cell.lblToAddress.text = [data objectForKey:@"toAddress"];
//
//    cell.lblTravelMode.attributedText =
//    [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Preferred travel mode: %@",[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]]] andIndex:22];
//    cell.lblDocType.attributedText =
//    [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Type of package: %@",[GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]]] andIndex:16];
//
//
////    cell.lblTravelMode.text = [NSString stringWithFormat:@"Travel Mode: %@",[GlobalInfo travelMode:[[data objectForKey:@"travelMode"] integerValue]]];
////
////    cell.lblDocType.text = [NSString stringWithFormat:@"Type: %@",[GlobalInfo packageType:[[data objectForKey:@"packageType"] integerValue]]];
//
//    [cell.btnStatus setTitle:[NSString stringWithFormat:@"Status: %@",[data objectForKey:@"statusText"]] forState:UIControlStateNormal];
//    cell.btnStatus.tag = index+1;
//    [cell.btnStatus addTarget:self action:@selector(showInfo:) forControlEvents:UIControlEventTouchUpInside];
//
