//
//  DeliveryDetailController.h
//  Angeler
//
//  Created by AJAY on 12/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryDetailController : UIViewController
@property (nonatomic, strong) NSString *packageId;
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@end
