//
//  DeliveryDetailController.m
//  Angeler
//
//  Created by AJAY on 12/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "DeliveryDetailController.h"
#import "MWPhotoBrowser.h"


@interface DeliveryDetailController ()<MWPhotoBrowserDelegate>
{
    BOOL isSenderImgTap,isAngelerImgTap,isPictureImgTap;
}

//Package Details

@property (weak, nonatomic) IBOutlet UIView *viewPackageDetails;
@property (weak, nonatomic) IBOutlet UIView *viewPackageDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageDetailsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblPackageTypeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWeightTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblPackageType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWt;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

//Collect From Addrsss

@property (weak, nonatomic) IBOutlet UIView *viewAdressDetails;
@property (weak, nonatomic) IBOutlet UIView *viewAdressDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *addressTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCollectFromAddress;
@property (weak, nonatomic) IBOutlet UIImageView *meetingPremiumImageIcon;

//Picture Of item

@property (weak, nonatomic) IBOutlet UIView *viewPictureItem;
@property (weak, nonatomic) IBOutlet UIView *viewPictureItemHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPictureItemTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;
@property (nonatomic, strong) NSMutableArray *arrPhotos,*arrSenderPhoto,*arrAngelerPhoto;

//Sender Details

@property (weak, nonatomic) IBOutlet UIView *viewSenderDetails;
@property (weak, nonatomic) IBOutlet UIView *viewSenderDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderDetailsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgSenderImage;
@property (weak, nonatomic) IBOutlet UIButton *btnSenderPhone;


//Angeler Details

@property (weak, nonatomic) IBOutlet UIView *viewAngelerDetails;
@property (weak, nonatomic) IBOutlet UIView *viewAngelerDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerInfoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgAngelerImage;
@property (weak, nonatomic) IBOutlet UIButton *btnAngelerPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTransportRefNo;

//QR code

@property (weak, nonatomic) IBOutlet UIView *viewQrCode;
@property (weak, nonatomic) IBOutlet UIView *viewQrCodeHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblviewQrCodeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblQrTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblQrCode;
@property (weak, nonatomic) IBOutlet UIImageView *imgQrCode;
@property (weak, nonatomic) IBOutlet UIView *QRCodeContainerView;
@property (weak, nonatomic) IBOutlet CopyTextLabel *lblShipmentUniqueID;
@property (weak, nonatomic) IBOutlet UILabel *lblIDTitle;


@end

@implementation DeliveryDetailController
@synthesize topBarHeightConstraint,topNavbarView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setGesture];
    [self initialSetUp];
    [self callApiToLoadDeliveryDetail];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"DeliveryDetailController"
                                               object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
   
    [self roundAllSpecificCorner];
}


- (void)receiveTestNotification:(id)sender{
    [self viewDidLoad];
}
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - General Functions

-(void)initialSetUp
{
     [self setLocalization];
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    [topNavbarView setTitle:[NSLocalizedString(@"Delivery_Info", nil) capitalizedString]];
    
    //Package Details
    
    [self.viewPackageDetails makeCornerRound:15.0];
    [self.viewPackageDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    
    //Collect From Address Details
    
    [self.viewAdressDetails makeCornerRound:15.0];
    [self.viewAdressDetails dropShadowWithColor:[UIColor lightishGrayColor]];
   
    //Picture Of item
    
    [self.viewPictureItem makeCornerRound:15.0];
    [self.viewPictureItem dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.viewPictureItemHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    
    //Sender Details
    
    [self.viewSenderDetails makeCornerRound:15.0];
    [self.viewSenderDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.viewSenderDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    
    //Angeler Details
    
    [self.viewAngelerDetails makeCornerRound:15.0];
    [self.viewAngelerDetails dropShadowWithColor:[UIColor lightishGrayColor]];
  
    
    //QR Code Details
    
    [self.viewQrCode makeCornerRound:15.0];
    [self.viewQrCode dropShadowWithColor:[UIColor lightishGrayColor]];
   
    _QRCodeContainerView.layer.borderColor = [[UIColor blackColor] CGColor];
    _QRCodeContainerView.layer.borderWidth = 1.0;
    
    
    
}

//Round specific corner Function => in some cases issue found view not loaded properly and got design issue .Thats why specific corner round we have to make after view already appear.

-(void)roundAllSpecificCorner
{
     [self.viewPackageDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewAdressDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewPictureItemHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
      [self.viewSenderDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
      [self.viewAngelerDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewQrCodeHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    
}



-(void)setLocalization
{
    _lblAddressDetailsTitle.text = NSLocalizedString(@"Appointment_Place_Detail", nil);
    _addressTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Address", nil)];
    _lblPackageTypeTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Item_Category", nil)];
    _lblPackageWeightTitle.text = [NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Weight", nil)];
     _lblTransportRefNoTitle.text = [NSString stringWithFormat:@"%@ : ",NSLocalizedString(@"Flight_Number", nil)];
    
    //Later want to hide that as per client requirement => making it blank
      _lblStatusTitle.text = @"";
    //[NSString stringWithFormat: @"%@ :",NSLocalizedString(@"Status", nil)];
    
     _lblPictureItemTitle.text = NSLocalizedString(@"Picture_Of_Item",nil);
    _lblSenderDetailsTitle.text = NSLocalizedString(@"Sender_Details",nil);
    _lblAngelerInfoTitle.text = NSLocalizedString(@"Angeler_Details",nil);
    _lblviewQrCodeTitle.text = NSLocalizedString(@"Qr_Code",nil);
    _lblPackageDetailsTitle.text = NSLocalizedString(@"Item_Details",nil);
    _lblIDTitle.text = NSLocalizedString(@"AGTN", nil);
}

-(void)setGesture
{
    [_lblQrTitle setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)];
    [_lblQrTitle addGestureRecognizer:tapges];
    
}

#pragma mark - Gesture Action

- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture
{
    isSenderImgTap = false;
    isAngelerImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
    
}


#pragma mark - API Call

- (void)callApiToLoadDeliveryDetail{
    /*
     Request type: query string
     Request parameter:
     userId, sessionId, os, osVersion, appVersion, userEmail, packageId
     
     Request example :
     http://103.15.232.68/~angeler/web_services/recipient/delivery_details?userId=6&sessionId=a22fd561cdc365388b103be95c9ede6352bb69e&os=Android&osVersion=M&appVersion=1.0&packageId=10&userEmail=amit1@gmail.com
     */

    
    NSDictionary *param = @{@"userEmail":[[GlobalInfo sharedInfo] userEmail],
                            @"packageId":_packageId
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               [self showInfoWithData:[responseObject objectForKey:@"responseData"]];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}




- (void)showInfoWithData:(NSDictionary*)data{
    
    
    
    //Package Details
    
     NSDictionary *senderData = [data objectForKey:@"packageSenderDetails"];
    
    
     _lblShipmentUniqueID.text = [senderData objectForKey:@"shipmentUniqueId"];
    
   
    
    _lblPackageType.text = [GlobalInfo TypeOfPackage:[[senderData objectForKey:@"packageType"] integerValue]];
    _lblPackageWt.text = [NSString stringWithFormat:@"%@ %@",[senderData objectForKey:@"packageWeight"],NSLocalizedString(@"package_weight_unit", nil)];
    
    //Later want to hide that as per client requirement => making it blank
    _lblStatus.text = @"";
    //[senderData objectForKey:@"statusText"];
    
   
    
    //Picture of Item
    
        self.arrPhotos = [NSMutableArray array];
        NSArray *imgArr = [data objectForKey:@"packageImages"];
    for (int i = 1; i < imgArr.count+1; i++) {
        UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
        //[_viewPictures viewWithTag:i];
        imgV.hidden = NO;
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[imgArr objectAtIndex:i-1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
        
        imgV.userInteractionEnabled = YES;
        [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
        [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[imgArr objectAtIndex:i-1] objectForKey:@"image"]]]];
    }
    
    
    //Sender Details
    
    _lblSenderName.text = [senderData objectForKey:@"senderName"];
    _lblSenderEmail.text = [senderData objectForKey:@"senderEmail"];
    
    self.arrSenderPhoto = [NSMutableArray array];
    MWPhoto *senderPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[senderData objectForKey:@"senderImage"]]];
    senderPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Sender", nil),[senderData objectForKey:@"senderName"]];
    [self.arrSenderPhoto addObject:senderPhoto];
    _imgSenderImage.userInteractionEnabled = YES;
    
    [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[senderData objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    [Helper makeItRound:_imgSenderImage.layer withRadius:25];
    _lblSenderPhone.hidden = [[senderData objectForKey:@"senderPhoneNo"] isEqualToString:Empty];
    _lblSenderPhone.text = [senderData objectForKey:@"senderPhoneNo"];
//    if (_lblSenderPhone.text.length > PHONE_MIN_DIGIT) {
//        _btnSenderPhone.hidden = NO;
//    }
//    else
//        _btnSenderPhone.hidden = YES;
    
    //Angeler Details
    
     NSDictionary *angelerData = [data objectForKey:@"angelerBookingDetails"];
    
    //Flight Number Set
    _lblTransportRefNo.text = [angelerData objectForKey:@"travelRefNo"];
    
    self.arrAngelerPhoto = [NSMutableArray array];
    MWPhoto *angelerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[angelerData objectForKey:@"angelerImage"]]];
    angelerPhoto.caption =[NSString stringWithFormat:@"Angeler : %@",[angelerData objectForKey:@"angelerName"]];
    [self.arrAngelerPhoto addObject:angelerPhoto];
    _imgAngelerImage.userInteractionEnabled = YES;
    
    
    [_imgAngelerImage sd_setImageWithURL:[NSURL URLWithString:[angelerData objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    _lblAngelerName.text = [angelerData objectForKey:@"angelerName"];
    _lblAngelerEmail.text = [angelerData objectForKey:@"angelerEmail"];
    _lblAngelerPhone.hidden = [[angelerData objectForKey:@"angelerPhoneNo"] isEqualToString:Empty];
    _lblAngelerPhone.text = [angelerData objectForKey:@"angelerPhoneNo"];
//    if (_lblAngelerPhone.text.length > PHONE_MIN_DIGIT) {
//        _btnAngelerPhone.hidden = NO;
//    }
//    else
//        _btnAngelerPhone.hidden = YES;
//    
    //Collect From address
    
    _lblCollectFromAddress.text = [angelerData objectForKey:@"angelerDropoffAddress"];
    if ([[angelerData objectForKey:@"serviceType"] isEqualToString:serviceType_Premium])
    {
        [_meetingPremiumImageIcon setImage:[UIImage imageNamed:@"Premium_Blue_icon"]];
    }
    else
    {
        [_meetingPremiumImageIcon setImage:[UIImage imageNamed:@"MeetingPoint_Blue_icon"]];
    }
    
    
    //QR Code Details
    
    if ([[angelerData objectForKey:@"angelerDropoffType"] isEqualToString:@"DMP"] || [[angelerData objectForKey:@"angelerDropoffType"] isEqualToString:@"DP"]) {
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"Scan_Code_Delivery_Info", nil),[[angelerData objectForKey:@"angelerName"] uppercaseString]];
        _lblQrTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[[angelerData objectForKey:@"angelerName"] uppercaseString]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
        
       
    }else if ([[angelerData objectForKey:@"angelerDropoffType"] isEqualToString:@"DDP"]){
        _lblQrTitle.text = NSLocalizedString(@"Show_QR_Code_Delivery_Info", nil);
        
    }
    
    if (![[senderData objectForKey:@"code"] isEqualToString:Empty]) {
        _lblQrCode.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Delivery_Code", nil),[senderData objectForKey:@"code"]];
        _imgQrCode.image = [self qrImageForCode:[senderData objectForKey:@"code"] andImageView:_imgQrCode];
    }
    
}

- (void)tapOnAngelerImage:(UITapGestureRecognizer*)tapGest
{
    isSenderImgTap = false;
    isAngelerImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
}

- (void)tapOnSenderImage:(UITapGestureRecognizer*)tapGest{
    
    isSenderImgTap = true;
    isAngelerImgTap = false;
    isPictureImgTap = false;
    [self setMWPhotoBrowserWithTag:0];
    
}

- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    
    isSenderImgTap = false;
    isAngelerImgTap = false;
    isPictureImgTap = true;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
    
    
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}


    

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isSenderImgTap)
    {
        return self.arrSenderPhoto.count;
    }
    else if (isAngelerImgTap)
    {
        return self.arrAngelerPhoto.count;
    }
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (isSenderImgTap)
    {
        if (index < self.arrSenderPhoto.count) {
            return [self.arrSenderPhoto objectAtIndex:index];
        }
    }
    else if (isAngelerImgTap)
    {
        if (index < self.arrAngelerPhoto.count) {
            return [self.arrAngelerPhoto objectAtIndex:index];
        }
    }
    else
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    return nil;
}


#pragma mark - Button Action

- (IBAction)callSender:(id)sender {
//    if (_lblSenderPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblSenderPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
   // }
}
- (IBAction)callAngeler:(id)sender {
  //  if (_lblAngelerPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblAngelerPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
   // }
}

- (UIImage*)qrImageForCode:(NSString*)qrString andImageView:(UIImageView*)qrImageView{
    //    NSString *qrString = @"My string to encode";
    NSData *stringData = [qrString dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = qrImageView.frame.size.width / qrImage.extent.size.width;
    float scaleY = qrImageView.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    return [UIImage imageWithCIImage:qrImage scale:[UIScreen mainScreen].scale
                         orientation:UIImageOrientationUp];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


//====================== Old + Reuse Code ==============================

//[Helper addShadowToLayer:_headerView.layer];
//// Do any additional setup after loading the view from its nib.
//[Helper addShadowToLayer:_viewPackage.layer];
//[Helper addShadowToLayer:_viewPictures.layer];
//[Helper addShadowToLayer:_viewSender.layer];
////    [Helper addShadowToLayer:_viewAngeler.layer];
//[Helper addShadowToLayer:_viewDeliveryPoint.layer];
//[Helper addShadowToLayer:_viewQrCode.layer];
//[Helper addShadowToLayer:_viewStatus.layer];

/* //@property (weak, nonatomic) IBOutlet UIView *viewPackage;
 //@property (weak, nonatomic) IBOutlet UILabel *lblPackageSize;
 //
 //
 //@property (weak, nonatomic) IBOutlet UIView *viewPictures;
 //
 //@property (weak, nonatomic) IBOutlet UIView *viewSender;
 
 
 //@property (weak, nonatomic) IBOutlet UIView *viewAngeler;
 //@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
 //@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEmail;
 //@property (weak, nonatomic) IBOutlet UILabel *lblAngelerPhone;
 //@property (weak, nonatomic) IBOutlet UIImageView *imgAngelerImage;
 
 
 //@property (weak, nonatomic) IBOutlet UIView *viewDeliveryPoint
 //;
 //@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryAddress;
 //@property (weak, nonatomic) IBOutlet UILabel *lblAddressType;
 //@property (weak, nonatomic) IBOutlet UILabel *lblName;
 //@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
 //@property (weak, nonatomic) IBOutlet UILabel *lblOperatingHours;
 
 //@property (weak, nonatomic) IBOutlet UIView *viewQrCode;
 //@property (weak, nonatomic) IBOutlet UILabel *lblQrTitle;
 //@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryCode;
 //@property (weak, nonatomic) IBOutlet UIImageView *imgQrCode;
 
 //@property (weak, nonatomic) IBOutlet UIView *viewStatus;
 //@property (weak, nonatomic) IBOutlet UILabel *lblStatusText;
 //
 //@property (weak, nonatomic) IBOutlet UIView *headerView;*/

/*if (packageImages.count == 0){
 [GlobalInfo hideViewNamed:_viewPictures withTopName:@"pictureTop"];
 }else{ */

/*  /* NSDictionary *deliveryPointData = [data objectForKey:@"deliveryPointDetails"];
 if ([deliveryPointData allKeys].count == 0) {
 [GlobalInfo hideViewNamed:_viewDeliveryPoint withTopName:@"deliveryPointTop"];
 }else{
 
 _lblDeliveryAddress.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address: %@",[deliveryPointData objectForKey:@"address"]] andIndex:8];
 
 _lblAddressType.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[deliveryPointData objectForKey:@"addressType"]] andIndex:12];
 
 _lblName.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Name: %@",[deliveryPointData objectForKey:@"contactName"]] andIndex:5];
 
 _lblPhone.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Phone: %@%@",[deliveryPointData objectForKey:@"contactPhoneISD"],[deliveryPointData objectForKey:@"contactPhone"]] andIndex:6];
 
 _lblOperatingHours.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Operating hours: %@-%@",[deliveryPointData objectForKey:@"operatingFromHours"],[deliveryPointData objectForKey:@"operatingToHours"]] andIndex:16];
 
 }
 
 
 
 if ([senderData allKeys].count == 0){
 [GlobalInfo hideViewNamed:_viewSender withTopName:@"senderTop"];
 }else{
 _lblPackageType.text = [GlobalInfo TypeOfPackage:[[senderData objectForKey:@"packageType"] integerValue]];
 _lblPackageSize.text = [NSString stringWithFormat:@"%@x%@x%@ CM",[senderData objectForKey:@"packageHeight"],[senderData objectForKey:@"packageWidth"],[senderData objectForKey:@"packageLength"]];
 _lblPackageWt.text = [NSString stringWithFormat:@"%@ GM",[senderData objectForKey:@"packageWeight"]];
 
 _lblSenderName.text = [senderData objectForKey:@"senderName"];
 _lblSenderEmail.text = [senderData objectForKey:@"senderEmail"];
 [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[senderData objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
 [Helper makeItRound:_imgSenderImage.layer withRadius:25];
 _lblSenderPhone.superview.hidden = [[senderData objectForKey:@"senderPhoneNo"] isEqualToString:Empty];
 _lblSenderPhone.text = [senderData objectForKey:@"senderPhoneNo"];
 }
 
 
 
 if ([angelerData allKeys].count == 0){
 // [GlobalInfo hideViewNamed:_viewAngeler withTopName:@"angelerTop"];
 }else{
 [Helper makeItRound:_imgAngelerImage.layer withRadius:25.0];
 [_imgAngelerImage sd_setImageWithURL:[NSURL URLWithString:[angelerData objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
 _lblAngelerName.text = [angelerData objectForKey:@"angelerName"];
 _lblAngelerEmail.text = [angelerData objectForKey:@"angelerEmail"];
 _lblAngelerPhone.superview.hidden = [[angelerData objectForKey:@"angelerPhoneNo"] isEqualToString:Empty];
 _lblAngelerPhone.text = [angelerData objectForKey:@"angelerPhoneNo"];
 }
 
 // if ([[senderData objectForKey:@"finalStatusText"] isEqualToString:Empty]) {
 
 //    }else{
 //        [GlobalInfo hideViewNamed:_viewQrCode withTopName:@"qrTop"];
 //    }
 
 //     _lblStatusText.attributedText = [GlobalInfo formatFinalStatusText:[NSString stringWithFormat:@"Status: %@",[senderData objectForKey:@"statusText"]] andIndex:7];
 
 //    _lblStatusText.text = [senderData objectForKey:@"statusText"];
 
 - (IBAction)backTapped:(id)sender{
 [self.navigationController popViewControllerAnimated:YES];
 }
 */
