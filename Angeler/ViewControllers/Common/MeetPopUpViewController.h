//
//  MeetPopUpViewController.h
//  Angeler
//
//  Created by AJAY on 28/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MeetPopUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *headerTitleView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *popUpIcon;

@property (weak, nonatomic) IBOutlet UILabel *singleBottomActionLabel;
@property (weak, nonatomic) IBOutlet UIView *singleBottomActionView;

@property (strong, nonatomic) NSString *headerTitleText;
@property (strong, nonatomic) NSString *bottomTitleText;
@property (strong, nonatomic) NSString *bottomDescriptionText;
@property (strong, nonatomic) NSString *singleActionLabelTitleText;
@property (strong, nonatomic) NSString *imageIconName;

@end

NS_ASSUME_NONNULL_END
