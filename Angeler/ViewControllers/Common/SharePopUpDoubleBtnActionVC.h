//
//  SharePopUpDoubleBtnActionVC.h
//  Angeler
//
//  Created by AJAY on 18/07/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SharePopUpDoubleBtnActionDismissDelegate <NSObject>
@optional
- (void)dismissPopUpLeftBtnAction;
- (void)dismissPopUpRightBtnAction;
@end

@interface SharePopUpDoubleBtnActionVC : UIViewController

@property (nonatomic, weak) id <SharePopUpDoubleBtnActionDismissDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerMiddleTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomDescLabel;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomActionView;

@property (strong, nonatomic) NSString *headerTitleText;
@property (strong, nonatomic) NSString *headerMiddleTitleText;
@property (strong, nonatomic) NSString *headerDescriptionText;
@property (strong, nonatomic) NSString *bottomDescriptionText;
@property (strong, nonatomic) NSString *leftBtnActionitleText;
@property (strong, nonatomic) NSString *rightBtnActionitleText;
@property (weak, nonatomic) IBOutlet UIImageView *shareIcon;

- (IBAction)onLeftBtnClicked:(id)sender;
- (IBAction)onRightBtnClicked:(id)sender;


@end

NS_ASSUME_NONNULL_END
