//
//  SharePopUpViewController.m
//  Angeler
//
//  Created by AJAY on 20/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "SharePopUpViewController.h"
#import "LoginViewController.h"

@interface SharePopUpViewController ()


@end

@implementation SharePopUpViewController
@synthesize containerView,headerTitleLabel,headerDescriptionLabel,BottomDescLabel,singleBottomActionLabel,singleBottomActionView,delegate,headerTitleText,headerDescriptionText,bottomDescriptionText,singleActionLabelTitleText,shareIconImgView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view.
}


-(void)initialSetUp{
    
    [self setGesture];
    
    NSLog(@"headerTitleText %@",headerTitleText);
    
    if ([bottomDescriptionText isEqualToString:@""])
    {
        shareIconImgView.hidden = YES;
    }
    else
    {
        shareIconImgView.hidden = NO;
    }
    
    headerTitleLabel.text = headerTitleText;
    headerDescriptionLabel.text = headerDescriptionText;
    BottomDescLabel.text = bottomDescriptionText;
    singleBottomActionLabel.text = singleActionLabelTitleText;
    
    [singleBottomActionView makeCornerRound:15.0];
    [containerView makeCornerRound:15.0];
    
}
-(void)showShareApplink
{
    NSString *shareData=[NSString stringWithFormat:NSLocalizedString(@"Share_Link_Title", nil),@""];
    
    NSArray *objectsToShare = @[shareData];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


-(void)setGesture{
    UITapGestureRecognizer *singleActionViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleActionViewTapped:)];
    [self.singleBottomActionView addGestureRecognizer:singleActionViewTapRecognizer];
}

- (void)singleActionViewTapped:(UITapGestureRecognizer*)sender
{
    [delegate dismissPopUp];
    [self dismissViewControllerAnimated:true completion:nil];
   
}

- (IBAction)onShareIconClicked:(id)sender {
    [self showShareApplink];
}


@end
