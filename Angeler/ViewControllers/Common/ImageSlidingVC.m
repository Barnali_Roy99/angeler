//
//  ImageSlidingVC.m
//  Angeler
//
//  Created by AJAY on 19/07/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "ImageSlidingVC.h"

@interface ImageSlidingVC ()

@end

@implementation ImageSlidingVC
{
    int selectedIndex;
}

@synthesize topNavbarView,topBarHeightConstraint,scrollView;

#pragma mark - Object Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"imageSlide arr %@",_imgArr);
    [self initialSetUp];
   
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self scrollViewSetUp];
}

#pragma mark - General Methods

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        topBarHeightConstraint.constant = 104;
    }
    else
    {
        topBarHeightConstraint.constant = 84;
    }
    
    [topNavbarView showNavbackBtn];
    selectedIndex = 0;
    self.pageControl.numberOfPages = _imgArr.count;
    
    if(_imgArr.count <= 1)
    {
        [self.pageControl setHidden:true];
    }
}


-(void)scrollViewSetUp
{
    CGFloat scrollViewWidth = self.scrollView.frame.size.width;
    CGFloat scrollViewHeight = self.scrollView.frame.size.height;
    
//    UITapGestureRecognizer *scrollViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTapped:)];
//    scrollViewTap.numberOfTapsRequired = 1;
//    [self.scrollView addGestureRecognizer:scrollViewTap];
    
    for(int i = 0;i<_imgArr.count;i++)
    {
        UIImageView *containerImgView = [[UIImageView alloc] initWithFrame:CGRectMake(scrollViewWidth * i, 0, scrollViewWidth, scrollViewHeight)];
        [containerImgView setContentMode:UIViewContentModeScaleAspectFit];
        [containerImgView sd_setImageWithURL:[NSURL URLWithString: [_imgArr objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"MyTripUser_icon"]];
      
        [self.scrollView addSubview:containerImgView];
        
    }
    self.scrollView.contentSize = CGSizeMake(scrollViewWidth * _imgArr.count, scrollViewHeight);
    
    self.pageControl.currentPage = 0;
   
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    CGFloat currentPage = floor((self.scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
    
    self.pageControl.currentPage = (int)currentPage;
}

@end
