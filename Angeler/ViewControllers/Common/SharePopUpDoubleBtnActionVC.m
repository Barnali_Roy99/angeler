//
//  SharePopUpDoubleBtnActionVC.m
//  Angeler
//
//  Created by AJAY on 18/07/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "SharePopUpDoubleBtnActionVC.h"

@interface SharePopUpDoubleBtnActionVC ()

@end

@implementation SharePopUpDoubleBtnActionVC

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view.
}

-(void)initialSetUp{
    
   
    NSLog(@"headerTitleText SharePopUpDoubleBtnActionVC %@",_headerTitleText);
    
    _headerTitleLabel.text = _headerTitleText;
    _headerMiddleTitleLabel.text = _headerMiddleTitleText;
    _headerDescriptionLabel.text = _headerDescriptionText;
    _BottomDescLabel.text = _bottomDescriptionText;
    [_leftBtn setTitle:_leftBtnActionitleText forState:UIControlStateNormal];
    [_rightBtn setTitle:_rightBtnActionitleText forState:UIControlStateNormal];
    
    [_bottomActionView makeCornerRound:15.0];
    [_containerView makeCornerRound:15.0];
    
}
-(void)showShareApplink
{
    NSString *shareData=[NSString stringWithFormat:NSLocalizedString(@"Share_Link_Title", nil),@""];
    
    NSArray *objectsToShare = @[shareData];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


#pragma mark - Button Action

- (IBAction)onLeftBtnClicked:(id)sender {
    [_delegate dismissPopUpLeftBtnAction];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)onRightBtnClicked:(id)sender {
    [_delegate dismissPopUpRightBtnAction];
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)onShareIconClicked:(id)sender {
    [self showShareApplink];
}
@end
