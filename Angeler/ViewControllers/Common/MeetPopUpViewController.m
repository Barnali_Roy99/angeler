//
//  MeetPopUpViewController.m
//  Angeler
//
//  Created by AJAY on 28/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "MeetPopUpViewController.h"

@interface MeetPopUpViewController ()

@end

@implementation MeetPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view.
}

-(void)initialSetUp{
    
    [self setGesture];

    [_popUpIcon setImage:[UIImage imageNamed:_imageIconName]];
    _headerTitleLabel.text = _headerTitleText;
    _bottomTitleLabel.text = _bottomTitleText;
    _descriptionLabel.text = _bottomDescriptionText;
    _singleBottomActionLabel.text = _singleActionLabelTitleText;
    
    [_singleBottomActionView makeCornerRound:15.0];
    [_headerTitleView makeCornerRound:15.0];
    
}


-(void)setGesture{
    UITapGestureRecognizer *singleActionViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleActionViewTapped:)];
    [self.singleBottomActionView addGestureRecognizer:singleActionViewTapRecognizer];
}

- (void)singleActionViewTapped:(UITapGestureRecognizer*)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
   
}



@end
