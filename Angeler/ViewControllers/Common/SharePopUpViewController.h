//
//  SharePopUpViewController.h
//  Angeler
//
//  Created by AJAY on 20/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SharePopUpDismissDelegate <NSObject>
@optional
- (void)dismissPopUp;
@end


@interface SharePopUpViewController : UIViewController
@property (nonatomic, weak) id <SharePopUpDismissDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *singleBottomActionLabel;
@property (weak, nonatomic) IBOutlet UIView *singleBottomActionView;

@property (strong, nonatomic) NSString *headerTitleText;
@property (strong, nonatomic) NSString *headerDescriptionText;
@property (strong, nonatomic) NSString *bottomDescriptionText;
@property (strong, nonatomic) NSString *singleActionLabelTitleText;
@property (weak, nonatomic) IBOutlet UIImageView *shareIconImgView;

@end

NS_ASSUME_NONNULL_END
