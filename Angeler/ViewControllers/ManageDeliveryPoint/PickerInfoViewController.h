//
//  PickerInfoViewController.h
//  Angeler
//
//  Created by AJAY on 27/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol PickerInfoDelegate <NSObject>

- (void)selectedItem:(NSDictionary*)selectedItem andTagReference:(TAG_VALUE)tagRef;

@end

@interface PickerInfoViewController : UIViewController
@property (nonatomic)TAG_VALUE type;
@property (nonatomic, strong) NSString *countryId;

@property(nonatomic,assign) id <PickerInfoDelegate> delegate;

@end
