//
//  MapViewController.m
//  SpotDeal
//
//  Created by AJAY on 21/10/16.
//  Copyright © 2016 Matrixnmedia. All rights reserved.
//

#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface MapViewController ()<GMSMapViewDelegate>
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _mapView.delegate = self;
    [[AppDel shareModel] startMonitoringLocation];
    NSNumber *lat = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
    NSNumber *longt = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];

    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[lat doubleValue] longitude:[longt doubleValue] zoom:12];
    _mapView.camera = camera;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = [camera target];// CLLocationCoordinate2DMake(22.40454,88.52894);
    marker.title = @"My Location";
    //marker.snippet = [branchDict objectForKey:@"mobile"];
    marker.map = _mapView;
    //marker.icon = [UIImage imageNamed:@"LocationPin"];
   
//    if (_arrBranchList.count == 1) {
//        [self showMapWithCenteredBranchIndex:0];
//    }else{
//        NSNumber *lat = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
//        NSNumber *longt = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
//        
//        NSMutableArray *locationArr = [[NSMutableArray alloc] init];
//        
//        for (NSDictionary *branchDict in _arrBranchList) {
//            CLLocation *locA = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[longt doubleValue]];
//            CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[branchDict objectForKey:@"Latitude"] doubleValue] longitude: [[branchDict objectForKey:@"Longitude"] doubleValue]];
//            CLLocationDistance distance = [locA distanceFromLocation:locB];
//            [locationArr addObject:[NSNumber numberWithDouble:distance]];
//        }
//        
//        NSArray *arrSort = [locationArr copy];
//        NSLog(@"===-- %@",arrSort);
//        arrSort = [arrSort sortedArrayUsingSelector:@selector(compare:)];
//        NSLog(@"--- %@",arrSort);
//        NSInteger indx = [locationArr indexOfObject:[arrSort firstObject]];
//        [self showMapWithCenteredBranchIndex:indx];
//    }
    
    
//    NSMutableArray *arrBranchLocation = [[NSMutableArray alloc] init];
//    for (NSDictionary *branchDict in _arrBranchList) {
//        MyAnnotation *annot = [[MyAnnotation alloc]initWithOnlyCoordinate:CLLocationCoordinate2DMake([[branchDict objectForKey:@"Latitude"] doubleValue], [[branchDict objectForKey:@"Longitude"] doubleValue])];
//        [annot setTitle:[NSString stringWithFormat:@"Branch Name: %@",[branchDict objectForKey:@"Name"]]];
//        //[annot setSubTitle:[branchDict objectForKey:@"mobile"]];
//        [arrBranchLocation addObject:annot];
//        
//    }
//    _MyMapObj=[[MyMapView alloc]initWithFrame:CGRectMake(0, 54, MainScreenWidth, MainScreenHeight-54)];
//    //_MyMapObj.MyLatLongProtocolDelegate=self;
//    _MyMapObj.locationAnnotatios = [arrBranchLocation copy];
//    _MyMapObj.MapFunctionality=DRAGABLE;
//    [_MyMapObj UpdateTheLocation];
//    [self.view addSubview:_MyMapObj];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)showMapWithCenteredBranchIndex:(int)indx {
    
    if (_arrBranchList.count == 1) {
        NSDictionary *branchDict = [_arrBranchList firstObject];
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[branchDict objectForKey:@"Latitude"] doubleValue] longitude:[[branchDict objectForKey:@"Longitude"] doubleValue] zoom:12];
        _mapView.camera = camera;
        
        // Creates a marker in the center of the map.
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([[branchDict objectForKey:@"Latitude"] doubleValue], [[branchDict objectForKey:@"Longitude"] doubleValue]);
        marker.title = [NSString stringWithFormat:@"Branch Name: %@",[branchDict objectForKey:@"Name"]];
        marker.snippet = [branchDict objectForKey:@"mobile"];
        marker.map = _mapView;
        marker.icon = [UIImage imageNamed:@"LocationPin"];

    }else{
        NSDictionary *branchDict = [_arrBranchList objectAtIndex:indx];
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[branchDict objectForKey:@"Latitude"] doubleValue] longitude:[[branchDict objectForKey:@"Longitude"] doubleValue] zoom:12];
        _mapView.camera = camera;
        
        for (NSDictionary *dict in _arrBranchList) {
            // Creates a marker in the center of the map.
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake([[dict objectForKey:@"Latitude"] doubleValue], [[dict objectForKey:@"Longitude"] doubleValue]);
            marker.title = [NSString stringWithFormat:@"Branch Name: %@",[dict objectForKey:@"Name"]];
            marker.snippet = [dict objectForKey:@"mobile"];
            marker.map = _mapView;
            marker.icon = [UIImage imageNamed:@"LocationPin"];
        }
    }
    
}

- (IBAction)backTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark--
#pragma GMSMap Delegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    return NO;
}

/**
 * Called after a marker's info window has been tapped.
 */
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
   
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
//        //markTitle = [[marker title] substringFromIndex:13];
//        //markTitle = [markTitle stringByReplacingOccurrencesOfString:@" " withString:@"+"];
//        
//        
//        NSString *markTitle = [NSString stringWithFormat:@"%f,%f",marker.position.latitude,marker.position.longitude];
//        
//        NSString *str = [NSString stringWithFormat:@"comgooglemaps://?q=%@&center=%f,%f&views=traffic",markTitle,marker.position.latitude,marker.position.longitude];
//        
//       // comgooglemaps://?q=&center=37.782652,-122.410126&views=satellite,traffic&zoom=15
//        
//        NSLog(@"url=====  %@",str);
//        
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
//    } else {
//        NSLog(@"Can't use comgooglemaps://");
//    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
