//
//  ManageDeliveryPointViewController.m
//  Angeler
//
//  Created by AJAY on 23/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ManageDeliveryPointViewController.h"
#import "DeliveryPointViewController.h"
#import "ManageDeliveryPointCell.h"
#import "ScanQrAtDeliveryPointController.h"
#import "DeliveryPackageListController.h"

@interface ManageDeliveryPointViewController (){
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
}

@property (weak, nonatomic) IBOutlet UITableView *tblManageDelivery;
@property (nonatomic, strong) NSMutableArray *arrDeliveryPoint;

@end

static NSString *CellIdentifier = @"ManageDeliveryPointCell";

@implementation ManageDeliveryPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [_tblManageDelivery registerNib:[UINib nibWithNibName:@"ManageDeliveryPointCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    
    
    _tblManageDelivery.estimatedRowHeight = 270;
    _tblManageDelivery.rowHeight = UITableViewAutomaticDimension;
    
    [_tblManageDelivery setNeedsLayout];
    [_tblManageDelivery layoutIfNeeded];
    
    // Do any additional setup after loading the view from its nib.
    
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblManageDelivery addSubview:refreshController];
}

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToListDeliveryPoints];
    [refreshController endRefreshing];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self callApiToListDeliveryPoints];
}

- (void)callApiToListDeliveryPoints{
    
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL]};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryPointList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           NSArray *arr = [responseObject objectForKey:@"responseData"];
           if ((arr.count == 0) && (START_POSITION != 0)) {
               moreRecordsAvailable = NO;
               return ;
           }
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               
               
               if (_arrDeliveryPoint.count > 0) {
                   if (START_POSITION == 0) {
                       [_arrDeliveryPoint removeAllObjects];
                   }else if(_arrDeliveryPoint.count > START_POSITION){
                       [_arrDeliveryPoint removeObjectsInRange:NSMakeRange(START_POSITION, _arrDeliveryPoint.count - START_POSITION)];
                   }
                   [_arrDeliveryPoint addObjectsFromArray:arr];
               }else{
                   _arrDeliveryPoint = [NSMutableArray arrayWithArray:arr];
               }
               START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
             
               [_tblManageDelivery reloadData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
    
}

- (IBAction)addDeliveryPoint:(id)sender{
    DeliveryPointViewController *delObj = [[DeliveryPointViewController alloc] initWithNibName:@"DeliveryPointViewController" bundle:nil];
    [self.navigationController pushViewController:delObj animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrDeliveryPoint.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ManageDeliveryPointCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *data = [_arrDeliveryPoint objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return [self manageCell:cell withData:data atIndex:indexPath.row];
}

- (ManageDeliveryPointCell*)manageCell:(ManageDeliveryPointCell *)cell withData:(NSDictionary*)data atIndex:(NSInteger)index{
    cell.lblAddress.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address: %@",[data objectForKey:@"address"]] andIndex:8];
    
    cell.lblAddressType.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[data objectForKey:@"addressType"]] andIndex:12];
    
    cell.lblName.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Name: %@",[data objectForKey:@"contactName"]] andIndex:5];
    
    cell.lblPhone.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Phone: %@",[data objectForKey:@"contactPhone"]] andIndex:6];
    
    cell.lblOpratingHours.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Operating hours: %@ to %@",[data objectForKey:@"operatingFromHours"],[data objectForKey:@"operatingToHours"]] andIndex:16];
    
    cell.lblStatus.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Status: %@",[data objectForKey:@"status"]] andIndex:7];
    cell.btnScanQr.hidden = [[data objectForKey:@"status"] isEqualToString:@"Pending"];
    cell.btnPackageList.hidden = [[data objectForKey:@"status"] isEqualToString:@"Pending"];
    
    cell.scanQrHeightConstraint.constant = [[data objectForKey:@"status"] isEqualToString:@"Pending"]?0:40;
    cell.buttonTopConstraint.constant = [[data objectForKey:@"status"] isEqualToString:@"Pending"]?10:10;
    [cell.btnPackageList addTarget:self action:@selector(openPackageList:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnScanQr addTarget:self action:@selector(openScanner:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnPackageList.tag = index+1;
    cell.btnScanQr.tag = index+1;
    
    if ([[data objectForKey:@"addressType"] isEqualToString:@"Residential"]) {
        cell.addressTypeHeightConstraint.constant = 0;
        cell.addressTypeTopConstraint.constant = 0;
        cell.commercialTopConstraint.constant = 0;
       // cell.imgAddressType.image = [UIImage imageNamed:@"Dp_home"];
        cell.imgCommercial.hidden = YES;
        cell.lblCommercial.hidden = YES;
    }else{
        cell.addressTypeHeightConstraint.constant = 17;
        cell.addressTypeTopConstraint.constant = 10;
        cell.commercialTopConstraint.constant = 10;
       // cell.imgAddressType.image = [UIImage imageNamed:@"Dp_building"];
        cell.imgCommercial.hidden = NO;
        cell.lblCommercial.hidden = NO;
        
       // cell.lblCommercial.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Address type: %@",[data objectForKey:@"addressType"]] andIndex:12];
        cell.lblCommercial.attributedText = [Helper getTheBoldAndRegularStringFrom:[NSString stringWithFormat:@"Shop/Co. name: %@",[data objectForKey:@"addressTypeDesc"]] andIndex:14];
    }
    

    return cell;
}

#pragma mark - Scrollview Delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 5.0) {
        if(moreRecordsAvailable)
        {
            [self callApiToListDeliveryPoints];
        }
    }
}

- (void)openScanner:(UIButton*)sender{
    START_POSITION = sender.tag-1;
    ScanQrAtDeliveryPointController *delObj = [[ScanQrAtDeliveryPointController alloc] initWithNibName:@"ScanQrAtDeliveryPointController" bundle:nil];
    delObj.deliveryPointId = [[_arrDeliveryPoint objectAtIndex:sender.tag-1] objectForKey:@"deliveryPointId"];
    [self.navigationController pushViewController:delObj animated:YES];
}

- (void)openPackageList:(UIButton*)sender{
    
    START_POSITION = sender.tag-1;
    DeliveryPackageListController *delObj = [[DeliveryPackageListController alloc] initWithNibName:@"DeliveryPackageListController" bundle:nil];
    delObj.deliveryPointId = [[_arrDeliveryPoint objectAtIndex:sender.tag-1] objectForKey:@"deliveryPointId"];
    [self.navigationController pushViewController:delObj animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
