//
//  ScanQrAtDeliveryPointController.h
//  Angeler
//
//  Created by AJAY on 09/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanQrAtDeliveryPointController : UIViewController
@property (nonatomic, strong) NSString *deliveryPointId;
@end
