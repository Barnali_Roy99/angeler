//
//  DeliveryPackageListCell.h
//  Angeler
//
//  Created by AJAY on 19/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryPackageListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderOrReceiver;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerOrReceiver;
@property (weak, nonatomic) IBOutlet UIButton *btnViewDetails;

@end
