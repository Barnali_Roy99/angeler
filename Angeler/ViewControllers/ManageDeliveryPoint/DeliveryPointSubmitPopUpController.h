//
//  DeliveryPointSubmitPopUpController.h
//  Angeler
//
//  Created by AJAY on 05/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryPointSubmitPopUpController : UIViewController
@property (nonatomic, strong) NSString *strMessage;
@end
