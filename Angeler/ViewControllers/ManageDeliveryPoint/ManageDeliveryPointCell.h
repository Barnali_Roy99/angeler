//
//  ManageDeliveryPointCell.h
//  Angeler
//
//  Created by AJAY on 24/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageDeliveryPointCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressType;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblOpratingHours;

@property (weak, nonatomic) IBOutlet UIImageView *imgAddressType;

@property (weak, nonatomic) IBOutlet UIImageView *imgCommercial;
@property (weak, nonatomic) IBOutlet UILabel *lblCommercial;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UIButton *btnScanQr;
@property (weak, nonatomic) IBOutlet UIButton *btnPackageList;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressTypeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scanQrHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressTypeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commercialTopConstraint;


@end
