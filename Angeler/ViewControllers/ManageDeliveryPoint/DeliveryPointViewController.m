//
//  DeliveryPointViewController.m
//  Angeler
//
//  Created by AJAY on 23/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "DeliveryPointViewController.h"
#import "ManageDeliveryPointViewController.h"
#import "MapViewController.h"
#import "SingleLocationMapController.h"
#import "DeliveryPointSubmitPopUpController.h"

typedef enum : NSUInteger {
    TagNumAddressType,
    TagNumCoutryCode,
    TagNumTimeFrom,
    TagNumTimeTo,
} TagNum;

@interface DeliveryPointViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,SingleLocationAddressDelegate>{
   // UIPickerView *pickerView;
}
@property (weak, nonatomic) IBOutlet UILabel *lblAddressPlaceholder;
@property (weak, nonatomic) IBOutlet UIButton *btnAddres;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressTypeCommercialheightConstraint;
@property (weak, nonatomic) IBOutlet UIView *viewFrom;
@property (weak, nonatomic) IBOutlet UIView *viewTo;
@property (weak, nonatomic) IBOutlet UIView *viewName;
@property (weak, nonatomic) IBOutlet UIView *viewNumber;
@property (weak, nonatomic) IBOutlet UIView *viewFax;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIView *viewWebsite;
@property (weak, nonatomic) IBOutlet UITextField *shopOrCompanyName;




@property (weak, nonatomic) IBOutlet UITextField *txtAddressType;

@property (weak, nonatomic) IBOutlet UITextField *txtOperatingHoursFrom;
@property (weak, nonatomic) IBOutlet UITextField *txtOperatingHoursTo;

@property (weak, nonatomic) IBOutlet UITextField *txtName;

@property (weak, nonatomic) IBOutlet UITextField *txtCountryCode;

@property (weak, nonatomic) IBOutlet UITextField *txtF_PhoneNymber;
@property (weak, nonatomic) IBOutlet UITextField *txtF_FaxNumber;

@property (weak, nonatomic) IBOutlet UITextField *txtF_Email;

//@property (weak, nonatomic) IBOutlet UITextField *txtF_Website;

//@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (nonatomic, strong) AddressClass *selectedAddress;
@property (nonatomic, strong) NSArray *countryList;


@end

@implementation DeliveryPointViewController



- (IBAction)pickAddressClicked:(UIButton*)sender {
    SingleLocationMapController *mapObj = [[SingleLocationMapController alloc] initWithNibName:@"SingleLocationMapController" bundle:nil];
    mapObj.delegate = self;
    mapObj.tag = DELIVERY_POINT_ADDRESS;
    [self.navigationController pushViewController:mapObj animated:YES];
}
#pragma mark-SingleAddressDelegate
- (void)selectedAddress:(AddressClass*)address forTag:(TAG_VALUE)tag
{
    if (tag == DELIVERY_POINT_ADDRESS) {
        _lblAddressPlaceholder.text = address.Address;
        _selectedAddress = address;
    }
}

- (IBAction)btnSubmitTapped:(id)sender {
    
    if ([self ValidationPassed]) {
        [self CallApiToAddDeliveryPoint];
    }
}
- (BOOL)ValidationPassed{
    if (self.selectedAddress == nil || [_lblAddressPlaceholder.text isEqualToString:@"address"]) {
        [Helper showAlertWithTitle:Empty Message:strKey_deliveryAddressRequired];
        return NO;
    }
//    if (_txtAddressType.text.length < 2) {
//        [Helper showAlertWithTitle:Empty Message:@"Please select address type."];
//        return NO;
//    }
    if ([_txtAddressType.text isEqualToString:@"Commercial"] && ([Helper checkEmptyField:_shopOrCompanyName.text])) {
        [Helper showAlertWithTitle:Empty Message:strKey_deliveryPointEnterShopName];
        return NO;
    }
    
    if ([_txtName.text isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:strKey_login_nameblank];
        return NO;
    }
//    if (_txtCountryCode.text.length == 0) {
//        [Helper showAlertWithTitle:Empty Message:@"Please select country code."];
//        return NO;
//    }
    if ([Helper checkEmptyField:_txtF_PhoneNymber.text]) {
        [Helper showAlertWithTitle:Empty Message:strKey_deliveryPointPhoneRequired];
        return NO;
    }
    if ([_txtF_Email.text isEqualToString:Empty]) {
        [Helper showAlertWithTitle:Empty Message:strKey_login_emailblank  ];
        return NO;
    }

//    if ([_txtF_Website.text isEqualToString:Empty]) {
//        [Helper showAlertWithTitle:Empty Message:@"Please enter website."];
//        return NO;
//    }

    return YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view from its nib.
    
    _countryList = [[GlobalInfo sharedInfo] countryList];
    
    ROUNDEDCORNER(_btnAddres.superview.layer);
    [Helper addBorderToLayer:_btnAddres.superview.layer];
    
    ROUNDEDCORNER(_btnAddres.layer);
    [Helper addBorderToLayer:_btnAddres.layer];
    
    ROUNDEDCORNER(_txtAddressType.layer);
    [Helper addBorderToLayer:_txtAddressType.layer];
    [Helper AddLeftViewToTxtField:_txtAddressType withWidth:10.0];
    
    ROUNDEDCORNER(_viewFrom.layer);
    [Helper addBorderToLayer:_viewFrom.layer];
    
    ROUNDEDCORNER(_viewTo.layer);
    [Helper addBorderToLayer:_viewTo.layer];
    
    ROUNDEDCORNER(_viewName.layer);
    [Helper addBorderToLayer:_viewName.layer];
    
    ROUNDEDCORNER(_viewNumber.layer);
    [Helper addBorderToLayer:_viewNumber.layer];
    
    ROUNDEDCORNER(_viewFax.layer);
    [Helper addBorderToLayer:_viewFax.layer];
    
    ROUNDEDCORNER(_viewEmail.layer);
    [Helper addBorderToLayer:_viewEmail.layer];
    
    ROUNDEDCORNER(_viewWebsite.layer);
    [Helper addBorderToLayer:_viewWebsite.layer];
    [Helper AddLeftViewToTxtField:_shopOrCompanyName withWidth:10.0];
    ROUNDEDCORNER(_shopOrCompanyName.layer);
    [Helper addBorderToLayer:_shopOrCompanyName.layer];
    
   
    [_txtCountryCode setInputView:[Helper pickerWithTag:TagNumCoutryCode forControler:self]];
    [_txtCountryCode setInputAccessoryView:[Helper toolbarWithTag:TagNumCoutryCode forControler:self]];
    
    [_txtAddressType setInputView:[Helper pickerWithTag:TagNumAddressType forControler:self]];
    [_txtAddressType setInputAccessoryView:[Helper toolbarWithTag:TagNumAddressType forControler:self]];
    
    [_txtOperatingHoursFrom setInputView:[Helper datePickerWithTag:TagNumTimeFrom]];
    [_txtOperatingHoursFrom setInputAccessoryView:[Helper toolbarWithTag:TagNumTimeFrom forControler:self]];
    
    [_txtOperatingHoursTo setInputView:[Helper datePickerWithTag:TagNumTimeTo]];
    [_txtOperatingHoursTo setInputAccessoryView:[Helper toolbarWithTag:TagNumTimeTo forControler:self]];
}

- (void)pickerDoneTapped:(UIButton*)sender{
    if (sender.tag == TagNumAddressType) {
        UIPickerView *pic = (UIPickerView*)_txtAddressType.inputView;
        _addressTypeCommercialheightConstraint.constant = [pic selectedRowInComponent:0] == 0?-30:10;
        _shopOrCompanyName.hidden = [pic selectedRowInComponent:0] == 0;
        _txtAddressType.text = [pic selectedRowInComponent:0] == 0?@"Residential":@"Commercial";
    }
    if (sender.tag == TagNumCoutryCode) {
        UIPickerView *pic = (UIPickerView*)_txtCountryCode.inputView;
        _txtCountryCode.text = [NSString stringWithFormat:@"+%@",[[_countryList objectAtIndex:[pic selectedRowInComponent:0]] objectForKey:@"countryIsdCode"]];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    if (sender.tag == TagNumTimeTo) {
        UIDatePicker *dtPic = (UIDatePicker*)_txtOperatingHoursTo.inputView;
        _txtOperatingHoursTo.text = [dateFormatter stringFromDate:[dtPic date]];
    }
    if (sender.tag == TagNumTimeFrom) {
        UIDatePicker *dtPic = (UIDatePicker*)_txtOperatingHoursFrom.inputView;
        _txtOperatingHoursFrom.text = [dateFormatter stringFromDate:[dtPic date]];
    }
    
    [self.view endEditing:YES];
}

- (void)CallApiToAddDeliveryPoint{
//    Request parameter:
//    userId, sessionId,appVersion, os, osVersion, contactEmail, contactPhone, contactFAX, address, city, country, addressType, operatingFromHours, operatingToHours, contactName,  contactPhoneISD, addressTypeDesc, addressLatitude, addressLongitude
//    
//    Note :
//    addressTypeDesc is visible for addressType = Commercial
//        City & Country Names are getting passed
//        
//        Request example :
//        http://angeler.serveraddress.com/web_services/delivery_points/add_delivery_point?userId=3&sessionId=4c2b2678e027b0bcbc93f0c7fdd9b5f07ac7a2d0&os=Android&osVersion=M&appVersion=1.0&contactName=Test&contactEmail=test@gmail.com&contactPhone=432423423&contactFAX=232432&address=Test&city=Kolkata&country=India&addressType=Residential&operatingFromHours=1&operatingToHours=2&contactPhoneISD=11&addressTypeDesc=ABC&addressLatitude=1.23232&addressLongitude=103.4324324
    
    NSDictionary *param = @{@"contactEmail":_txtF_Email.text,
                            @"contactPhone":_txtF_PhoneNymber.text,
                            @"contactFAX":_txtF_FaxNumber.text,
                            @"address":_lblAddressPlaceholder.text,
                            @"city":_selectedAddress.cityName,
                            @"country":_selectedAddress.countryName,
                            @"addressType":_txtAddressType.text,
                            @"operatingFromHours":_txtOperatingHoursFrom.text,
                            @"operatingToHours":_txtOperatingHoursTo.text,
                            @"contactName":_txtName.text,
                            @"contactPhoneISD":_txtCountryCode.text,
                            @"addressTypeDesc":[_txtAddressType.text isEqualToString:@"Commercial"]?_shopOrCompanyName.text:Empty,
                            @"addressLatitude":_selectedAddress.lat_Value,
                            @"addressLongitude":_selectedAddress.long_Value,
                            
                            
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kAddDeliveryPoint
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               DeliveryPointSubmitPopUpController *popUp = [[DeliveryPointSubmitPopUpController alloc] initWithNibName:@"DeliveryPointSubmitPopUpController" bundle:nil];
               popUp.strMessage = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
               [self addChildViewController:popUp];
               popUp.view.frame = self.view.frame;
               [self.view addSubview:popUp.view];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];

    
}
- (void)tapOnAddress:(UITapGestureRecognizer*)tapGest{
    NSLog(@"tap on textv");
    MapViewController *pick = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    [self presentViewController:pick animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backTapped:(id)sender{
    ManageDeliveryPointViewController *managObj = [[ManageDeliveryPointViewController alloc] initWithNibName:@"ManageDeliveryPointViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
    
//    [self.navigationController popViewControllerAnimated:YES];
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
}

#pragma mark
#pragma Picker delegate---
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
   
//    [_tfCurrencies resignFirstResponder];
//    _tfCurrencies.text = currencies[row];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == TagNumAddressType) {
        return 2;
    }
    return _countryList.count;
//    return currencies.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == TagNumAddressType) {
        return row == 0?@"Residential":@"Commercial";
    }
    return [NSString stringWithFormat:@"+%@",[[_countryList objectAtIndex:row] objectForKey:@"countryIsdCode"]];
//    return currencies[row];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
