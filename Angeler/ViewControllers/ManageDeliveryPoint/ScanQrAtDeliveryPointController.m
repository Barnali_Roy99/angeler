//
//  ScanQrAtDeliveryPointController.m
//  Angeler
//
//  Created by AJAY on 09/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ScanQrAtDeliveryPointController.h"
#import "MTBBarcodeScanner.h"
#import "DeliveryPointCodeDetailController.h"

@interface ScanQrAtDeliveryPointController (){
    BOOL foundCode;
}
@property (weak, nonatomic) IBOutlet UITextField *txtEnterCode;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIView *scannerView;
@property (nonatomic, strong) MTBBarcodeScanner *scannerMT;

@end

@implementation ScanQrAtDeliveryPointController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    ROUNDEDCORNER(_btnNext.layer);
    [Helper addBorderToLayer:_btnNext.layer];
    _scannerMT = [[MTBBarcodeScanner alloc] initWithPreviewView:self.scannerView];
    [self showScanner];
}
- (IBAction)scanQrClicked:(id)sender {
    [self showScanner];
}
- (IBAction)nextTapped:(id)sender {
    
    
//    Request parameter:
//    userId, sessionId, os, osVersion, appVersion, code, deliveryPointId
//    
//    Request example :
//http://103.15.232.68/~angeler/web_services/delivery_points/validate_code?userId=17&sessionId=8dc3535b3a1f36d0343d4664d53d693fe6f16ab4&os=Android&osVersion=M&appVersion=1.0&code=RL1dTzfm&deliveryPointId=27

    if (_txtEnterCode.text.length > 4) {
        NSDictionary *param = @{@"code":_txtEnterCode.text,
                                @"deliveryPointId":_deliveryPointId
                                };
        
        
        //=======Call WebService Engine========
        [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kValidateCodeAtDeliveryPoint
                                                        Method:REQMETHOD_GET
                                                    parameters:[GlobalInfo getParmWithSession:param]
                                                         media:nil
                                                viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
               if ([GlobalInfo checkStatusSuccess:responseObject]) {
                   [self showCodeDetailWithData:[responseObject objectForKey:@"responseData"]];
               }           }
           failure:^(id responseObject){
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
           }];
    }
}
- (void)showCodeDetailWithData:(NSDictionary*)data{
        DeliveryPointCodeDetailController *obj = [[DeliveryPointCodeDetailController alloc] initWithNibName:@"DeliveryPointCodeDetailController" bundle:nil];
        obj.code = [data objectForKey:@"code"];
        obj.deliveryPointId = [data objectForKey:@"deliveryPointId"];
        obj.bookingId = [data objectForKey:@"bookingId"];
        obj.type = [data objectForKey:@"type"];
        obj.packageStatus = [data objectForKey:@"packageStatus"];
        [self.navigationController pushViewController:obj animated:YES];

}
- (IBAction)backTapped:(id)sender{
    if (!_scannerView.hidden) {
        _scannerView.hidden = YES;
         [self.scannerMT stopScanning];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)showScanner{
    _scannerView.hidden = NO;
    foundCode = NO;
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            NSError* error = nil;
            [self.scannerMT startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                if (!foundCode) {
                    _txtEnterCode.text = code.stringValue;
                    foundCode = YES;
                }
                _scannerView.hidden = YES;
                [self.scannerMT stopScanning];
            } error:&error];
        } else {
            // The user denied access to the camera
            [self displayPermissionMissingAlert];
        }
    }];
}
#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
     [Helper showAlertWithTitle:strKey_ScanningUnAvailable Message:message];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
