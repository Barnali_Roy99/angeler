//
//  DeliveryPointCodeDetailController.h
//  Angeler
//
//  Created by AJAY on 10/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryPointCodeDetailController : UIViewController
@property (nonatomic, strong) NSString *code, *deliveryPointId, *bookingId, *type, *packageStatus;
@end
