//
//  PickerInfoViewController.m
//  Angeler
//
//  Created by AJAY on 27/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "PickerInfoViewController.h"

@interface PickerInfoViewController ()
@property (nonatomic, strong) IBOutlet UITableView *tblSearch;
@property (nonatomic, strong) NSArray *arrMainData;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray *arrSortedData;
@end
static NSString *CellIdentifier = @"Cell";
@implementation PickerInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.searchBar.placeholder = NSLocalizedString(@"Search_City", nil);
    [_tblSearch registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    [self callServiceToLoadData];
    
}
- (void)callServiceToLoadData{
//    NSString *serviceString = kCountryList;
//    NSDictionary *param = @{@"caller":@"default"};
//    if ((_type == DEPRT_CITY) || (_type == DEST_CITY)){
//
//        serviceString = kCityList;
//        param = @{@"countryId":_countryId};
//    }

    NSString *serviceString = kCityList;
    NSDictionary *param = @{};

    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:serviceString
                                                    Method:REQMETHOD_GET
                                                parameters:param
                                                     media:nil
                                            viewController:self
           success:^(id responseObject){
               NSLog(@"response ===   %@",responseObject);
               if ([GlobalInfo checkStatusSuccess:responseObject]) {
                   _arrMainData = [responseObject objectForKey:@"responseData"];

                   [self createFilterArrOnFirstLoad];
               }
           }
           failure:^(id responseObject){
               NSLog(@"failure response ===   %@",responseObject);
               [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
               //Web request call failed//Web service failed to retrieve data
           }];


}




- (IBAction)backTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(void)createFilterArrOnFirstLoad
{
//    if ([_countryId isEqualToString:Empty])
//    {
        _arrSortedData = _arrMainData;
//    }
//    else
//    {
//        NSPredicate* predicateCountryID = [NSPredicate predicateWithFormat:@"SELF.countryId ==[c] %@", _countryId];
//        NSArray* filteredData = [_arrMainData filteredArrayUsingPredicate:predicateCountryID];
//        _arrSortedData = filteredData;
//
//        if ([_arrSortedData count] > 0)
//
//        {
//            self.searchBar.text = [[_arrSortedData objectAtIndex:0] objectForKey:@"countryName"];
//        }
//    }
    
    [_tblSearch reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrSortedData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@, %@",[[_arrSortedData objectAtIndex:indexPath.row] objectForKey:@"cityName"],[[_arrSortedData objectAtIndex:indexPath.row] objectForKey:@"countryName"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate selectedItem:[_arrSortedData objectAtIndex:indexPath.row] andTagReference:self.type];
        
        
    }];
}


#pragma mark
#pragma search delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"sercj--- - %@",searchText);
    if ([searchText isEqualToString:Empty]) {
        _arrSortedData = _arrMainData;
        [_tblSearch reloadData];
        return;
    }
    
  
    NSPredicate* predicateCountry = [NSPredicate predicateWithFormat:@"SELF.countryName CONTAINS[cd] %@", searchText];
    NSPredicate* predicateCity = [NSPredicate predicateWithFormat:@"SELF.cityName CONTAINS[cd] %@", searchText];
   
    NSPredicate *compoundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicateCountry, predicateCity]];
    NSArray* filteredData = [_arrMainData filteredArrayUsingPredicate:compoundPredicate];
    _arrSortedData = filteredData;
    [_tblSearch reloadData];
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _arrSortedData = _arrMainData;
    [_tblSearch reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
