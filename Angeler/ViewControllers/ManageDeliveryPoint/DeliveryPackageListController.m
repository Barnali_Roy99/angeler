//
//  DeliveryPackageListController.m
//  Angeler
//
//  Created by AJAY on 19/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "DeliveryPackageListController.h"
#import "DeliveryPackageListCell.h"
#import "DeliveryPackageDetailController.h"
#import "ScanQrAtDeliveryPointController.h"

@interface DeliveryPackageListController (){
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
}

@property (weak, nonatomic) IBOutlet UITableView *tblDeliveryPackageList;
@property (nonatomic, strong) NSMutableArray *arrDeliveryPackageList;

@end

static NSString *CellIdentifier = @"DeliveryPackageListCell";

@implementation DeliveryPackageListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    {
        [_tblDeliveryPackageList registerNib:[UINib nibWithNibName:@"DeliveryPackageListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    
        _tblDeliveryPackageList.estimatedRowHeight = 150;
        _tblDeliveryPackageList.rowHeight = UITableViewAutomaticDimension;
        
        [_tblDeliveryPackageList setNeedsLayout];
        [_tblDeliveryPackageList layoutIfNeeded];
    }
    
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblDeliveryPackageList addSubview:refreshController];
}

- (void)callApiToLoadDeliveryPackagesList{
    
}

- (void)handleRefresh:(id)refreshController{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callApiToLoadDeliveryPackagesList];
    [refreshController endRefreshing];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self callApiToLoadPackageListAtDeliveryPoint];
}

- (void)callApiToLoadPackageListAtDeliveryPoint{
   // userId, sessionId,os, osVersion, appVersion,  deliveryPointId, start, offset
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",(long)START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"deliveryPointId":_deliveryPointId};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryPointPackageList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           NSArray *arr = [responseObject objectForKey:@"responseData"];
           if ((arr.count == 0) && (START_POSITION != 0)) {
               moreRecordsAvailable = NO;
               return ;
           }
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               
               if (_arrDeliveryPackageList.count > 0) {
                   if (START_POSITION == 0) {
                       [_arrDeliveryPackageList removeAllObjects];
                   }else if(_arrDeliveryPackageList.count > START_POSITION){
                       [_arrDeliveryPackageList removeObjectsInRange:NSMakeRange(START_POSITION, _arrDeliveryPackageList.count - START_POSITION)];
                   }
                   [_arrDeliveryPackageList addObjectsFromArray:arr];
               }else{
                   _arrDeliveryPackageList = [NSMutableArray arrayWithArray:arr];
               }
               START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
               [_tblDeliveryPackageList reloadData];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
    
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _arrDeliveryPackageList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DeliveryPackageListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *data = [_arrDeliveryPackageList objectAtIndex:indexPath.row];
    cell.btnViewDetails.tag = indexPath.row;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return [self manageCell:cell withData:data atIndex:indexPath.row];
}

- (DeliveryPackageListCell*)manageCell:(DeliveryPackageListCell *)cell withData:(NSDictionary*)data atIndex:(NSInteger)index{
    [cell.btnViewDetails addTarget:self action:@selector(openDetailsPage:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[data allKeys] containsObject:@"senderName"]) {
        cell.lblSenderName.text = [data objectForKey:@"senderName"];
        cell.lblSenderOrReceiver.text = @"Sender";
        
        cell.lblAngelerName.text = [data objectForKey:@"angelerName"];
        cell.lblAngelerOrReceiver.text = @"Angeler";
        
    }else{
        cell.lblSenderName.text = [data objectForKey:@"angelerName"];
        cell.lblSenderOrReceiver.text = @"Angeler";
        
        cell.lblAngelerName.text = [data objectForKey:@"receiverName"];
        cell.lblAngelerOrReceiver.text = @"Receiver";
    }
    
    
    
    cell.lblPackageType.text = [GlobalInfo TypeOfPackage:[[data objectForKey:@"packageType"] integerValue]];
    
    cell.lblPackageStatus.text = [data objectForKey:@"packageStatus"];
    
    return cell;
}

#pragma mark - Scrollview Delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 5.0) {
        if(moreRecordsAvailable)
        {
            [self callApiToLoadPackageListAtDeliveryPoint];
        }
    }
}

- (IBAction)backtapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scanQrtapped:(id)sender {
    START_POSITION = 0;
    ScanQrAtDeliveryPointController *delObj = [[ScanQrAtDeliveryPointController alloc] initWithNibName:@"ScanQrAtDeliveryPointController" bundle:nil];
    delObj.deliveryPointId = _deliveryPointId;
    [self.navigationController pushViewController:delObj animated:YES];
}

- (void)openDetailsPage:(UIButton*)sender{
    START_POSITION = 0;
    DeliveryPackageDetailController *delObj = [[DeliveryPackageDetailController alloc] initWithNibName:@"DeliveryPackageDetailController" bundle:nil];
    delObj.deliveryPointId = _deliveryPointId;
    delObj.bookingId = [[_arrDeliveryPackageList objectAtIndex:sender.tag] objectForKey:@"bookingId"];
    [self.navigationController pushViewController:delObj animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
