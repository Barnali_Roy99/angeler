//
//  DeliveryPointSubmitPopUpController.m
//  Angeler
//
//  Created by AJAY on 05/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "DeliveryPointSubmitPopUpController.h"
#import "ManageDeliveryPointViewController.h"

@interface DeliveryPointSubmitPopUpController ()
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIView *viContainer;
@end

@implementation DeliveryPointSubmitPopUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    ROUNDEDCORNER(_viContainer.layer);
   // [Helper addShadowToLayer:_viContainer.layer];
    _lblMessage.text = _strMessage;
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)CloseTapped:(id)sender {
    ManageDeliveryPointViewController *managObj = [[ManageDeliveryPointViewController alloc] initWithNibName:@"ManageDeliveryPointViewController" bundle:nil];
    AppDel.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:managObj];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
