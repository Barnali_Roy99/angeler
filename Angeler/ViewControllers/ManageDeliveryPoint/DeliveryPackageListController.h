//
//  DeliveryPackageListController.h
//  Angeler
//
//  Created by AJAY on 19/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryPackageListController : UIViewController
@property (nonatomic, strong) NSString *deliveryPointId;
@end
