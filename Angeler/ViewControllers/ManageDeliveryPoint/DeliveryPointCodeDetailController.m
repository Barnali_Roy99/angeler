//
//  DeliveryPointCodeDetailController.m
//  Angeler
//
//  Created by AJAY on 10/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "DeliveryPointCodeDetailController.h"
#import "MWPhotoBrowser.h"


@interface DeliveryPointCodeDetailController ()<MWPhotoBrowserDelegate>
@property (nonatomic, strong) NSMutableArray *arrPhotos;

@property (weak, nonatomic) IBOutlet UIView *viewPackageDetail;
@property (weak, nonatomic) IBOutlet UIView *viewPictures;
@property (weak, nonatomic) IBOutlet UIView *viewSender;
@property (weak, nonatomic) IBOutlet UIView *viewAngeler;
@property (weak, nonatomic) IBOutlet UIView *viewRecipient;
@property (weak, nonatomic) IBOutlet UIView *viewStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;

@property (weak, nonatomic) IBOutlet UILabel *lblPackageType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackSize;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWt;

@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgSender;

@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgAngeler;

@property (weak, nonatomic) IBOutlet UILabel *lblRecipientName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientPhone;

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;


@end

@implementation DeliveryPointCodeDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
       // Do any additional setup after loading the view from its nib.
    ROUNDEDCORNER(_viewPictures.layer);
    [Helper addShadowToLayer:_viewPictures.layer];
    
    ROUNDEDCORNER(_viewSender.layer);
    [Helper addShadowToLayer:_viewSender.layer];
    
    ROUNDEDCORNER(_viewPackageDetail.layer);
    [Helper addShadowToLayer:_viewPackageDetail.layer];
    
    ROUNDEDCORNER(_viewAngeler.layer);
    [Helper addShadowToLayer:_viewAngeler.layer];
    
    ROUNDEDCORNER(_viewRecipient.layer);
    [Helper addShadowToLayer:_viewRecipient.layer];
    
    ROUNDEDCORNER(_viewStatus.layer);
    [Helper addShadowToLayer:_viewStatus.layer];
    
    ROUNDEDCORNER(_btnUpdate.layer);
    [Helper addShadowToLayer:_btnUpdate.layer];
    
    [self callServiceApiToLoadPage];
}
- (void)callServiceApiToLoadPage{
    /*
     Request parameter:
     userId, sessionId, os, osVersion, appVersion, code, deliveryPointId, bookingId, type, packageStatus
     Request example :
     http://103.15.232.68/~angeler/web_services/delivery_points/code_details?userId=17&sessionId=6404fd10f69d860c4162877a6e489b9bfaef46b0&os=Android&osVersion=M&appVersion=1.0&code=J40lUpy6&deliveryPointId=32&bookingId=12&packageStatus=1&type=ReceiverPick
     */
    
    NSDictionary *param = @{@"code":_code,
                            @"deliveryPointId":_deliveryPointId,
                            @"bookingId":_bookingId,
                            @"type":_type,
                            @"packageStatus":_packageStatus
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryPointCodeDetail
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject])  {
               [self parseData:[responseObject objectForKey:@"responseData"]];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}
- (void)parseData:(NSDictionary*)data{
    NSDictionary *angelerData = [data objectForKey:@"angelerDetails"];
    if ([angelerData allKeys].count == 0){
        [GlobalInfo hideViewNamed:_viewAngeler withTopName:@"angelerTop"];
    }else{
        [Helper makeItRound:_imgAngeler.layer withRadius:25.0];
        [_imgAngeler sd_setImageWithURL:[NSURL URLWithString:[angelerData objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        _lblAngelerName.text = [angelerData objectForKey:@"angelerName"];
        _lblAngelerEmail.text = [angelerData objectForKey:@"angelerEmail"];
        _lblAngelerPhone.hidden = [[angelerData objectForKey:@"angelerPhone"] isEqualToString:Empty];
        _lblAngelerPhone.text = [angelerData objectForKey:@"angelerPhone"];
    }
    
    NSDictionary *senderData = [data objectForKey:@"senderDetails"];
    if ([senderData allKeys].count == 0){
        [GlobalInfo hideViewNamed:_viewSender withTopName:@"senderTop"];
    }else{
        [Helper makeItRound:_imgSender.layer withRadius:25.0];
        [_imgSender sd_setImageWithURL:[NSURL URLWithString:[senderData objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        _lblSenderName.text = [senderData objectForKey:@"senderName"];
        _lblSenderEmail.text = [senderData objectForKey:@"senderEmail"];
        _lblSenderPhone.hidden = [[senderData objectForKey:@"senderPhone"] isEqualToString:Empty];
        _lblSenderPhone.text = [senderData objectForKey:@"senderPhone"];
    }
    
    NSDictionary *recipientData = [data objectForKey:@"recipientDetails"];
    if ([recipientData allKeys].count == 0){
        [GlobalInfo hideViewNamed:_viewRecipient withTopName:@"recipientTop"];
    }else{
        _lblRecipientName.text = [recipientData objectForKey:@"recipientName"];
        _lblRecipientEmail.text = [recipientData objectForKey:@"recipientEmail"];
        _lblRecipientPhone.hidden = [[recipientData objectForKey:@"recipientPhone"] isEqualToString:Empty];
        _lblRecipientPhone.text = [recipientData objectForKey:@"recipientPhone"];
    }
   
    
    if ([[data objectForKey:@"message"]isEqualToString:Empty]) {
        [GlobalInfo hideViewNamed:_viewStatus withTopName:@"statusTop"];
        [_btnUpdate setTitle:[self titleForButtonWithCode] forState:UIControlStateNormal];
    }else{
        [GlobalInfo hideViewNamed:(UIView*)_btnUpdate withTopName:@"buttonTop"];
        _lblStatus.text = [data objectForKey:@"message"];
    }
    
    NSArray *imgArr = [data objectForKey:@"packageImages"];
    self.arrPhotos = [NSMutableArray array];
    for (int i = 1; i < imgArr.count+1; i++) {
        UIImageView *imgV = [_viewPictures viewWithTag:i];
        imgV.hidden = NO;
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[imgArr objectAtIndex:i-1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
        
        imgV.userInteractionEnabled = YES;
        [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
        [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[imgArr objectAtIndex:i-1] objectForKey:@"image"]]]];
    }
    
    NSDictionary *packData = [data objectForKey:@"packageDetails"];
    _lblPackageType.text = [GlobalInfo TypeOfPackage:[[packData objectForKey:@"packageType"] integerValue]];
    _lblPackSize.text = [NSString stringWithFormat:@"%@ X %@ X %@ CM",[packData objectForKey:@"packageHeight"],[packData objectForKey:@"packageWidth"],[packData objectForKey:@"packageLength"]];
    _lblPackageWt.text = [NSString stringWithFormat:@"%@ GM",[packData objectForKey:@"packageWeight"]];
    
}

- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:[tapGest view].tag-1];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

- (IBAction)backtapped:(id)sender {
   [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:YES];
}

- (IBAction)callSender:(id)sender {
    if (_lblSenderPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblSenderPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}
- (IBAction)callAngeler:(id)sender {
    if (_lblAngelerPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblAngelerPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}
- (IBAction)callRecipient:(id)sender {
    if (_lblRecipientPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblRecipientPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    }
}
- (IBAction)updateTapped:(id)sender {
    /*
     Request parameter:
     userId, sessionId, os, osVersion, appVersion, code, deliveryPointId, bookingId, type
     
     Request example :
     http://103.15.232.68/~angeler/web_services/delivery_points/update_package_status?userId=17&sessionId=f74a0a21ada2953a43658e999e5e9b0b952b4c6e&os=Android&osVersion=M&appVersion=1.0&code=J40lUpy6&deliveryPointId=32&bookingId=1&type=SenderDrop

     */
    NSDictionary *param = @{@"code":_code,
                            @"deliveryPointId":_deliveryPointId,
                            @"bookingId":_bookingId,
                            @"type":_type
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryPointStatusUpdate
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject])  {
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:YES];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}

- (NSString*)titleForButtonWithCode{
    NSString *title = @"Update";
    if ([_type isEqualToString:@"SenderDrop"]){
        title = @"Confirm package receive";}
    
    if ([_type isEqualToString:@"AngelerPick"]){
        title = @"Confirm package pickup";}
    
    if ([_type isEqualToString:@"AngelerDrop"]){
        title = @"Confirm package drop";}
    
    if ([_type isEqualToString:@"ReceiverPick"]){
        title = @"Confirm package delivery";}
    
    return title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
