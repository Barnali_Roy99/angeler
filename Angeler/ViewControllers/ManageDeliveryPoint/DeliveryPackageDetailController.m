//
//  DeliveryPackageDetailController.m
//  Angeler
//
//  Created by AJAY on 19/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "DeliveryPackageDetailController.h"
#import "MWPhotoBrowser.h"

@interface DeliveryPackageDetailController ()<MWPhotoBrowserDelegate>
@property (nonatomic, strong) NSMutableArray *arrPhotos;

@property (weak, nonatomic) IBOutlet UIView *viewPackage;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageType;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageSize;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageWt;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageRemarks;

@property (weak, nonatomic) IBOutlet UIView *viewPictures;

@property (weak, nonatomic) IBOutlet UIView *viewSender;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgSenderImage;

@property (weak, nonatomic) IBOutlet UIView *viewAngeler;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgAngelerImage;

@property (weak, nonatomic) IBOutlet UIView *viewRecipient;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgRecipientImage;

@property (weak, nonatomic) IBOutlet UIView *viewStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusText;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@end

@implementation DeliveryPackageDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Helper addShadowToLayer:_headerView.layer];
    [Helper addShadowToLayer:_viewPackage.layer];
    [Helper addShadowToLayer:_viewPictures.layer];
    [Helper addShadowToLayer:_viewSender.layer];
    [Helper addShadowToLayer:_viewAngeler.layer];
    [Helper addShadowToLayer:_viewRecipient.layer];
    [Helper addShadowToLayer:_viewStatus.layer];
    [self callApiToLoadDetails];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"DeliveryPackageDetailController"
                                               object:nil];
}

- (void)receiveTestNotification:(id)sender{
    [self callApiToLoadDetails];
}
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)callApiToLoadDetails{
    // userId, sessionId, os, osVersion, appVersion, deliveryPointId, bookingId
    NSDictionary *param = @{@"bookingId":_bookingId,
                            @"deliveryPointId":_deliveryPointId};
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kDeliveryPointPackageDetail
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
       success:^(id responseObject){
           NSLog(@"response ===   %@",responseObject);
           if ([GlobalInfo checkStatusSuccess:responseObject]) {
               [self showInfoWithData:[responseObject objectForKey:@"responseData"]];
           }
       }
       failure:^(id responseObject){
           [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
       }];
}

- (void)showInfoWithData:(NSDictionary*)data{
    
    NSDictionary *senderData = [data objectForKey:@"senderDetails"];
    
    if ([senderData allKeys].count == 0){
        [GlobalInfo hideViewNamed:_viewSender withTopName:@"senderTop"];
    }else{
        _lblSenderName.text = [senderData objectForKey:@"senderName"];
        _lblSenderEmail.text = [senderData objectForKey:@"senderEmail"];
        [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[senderData objectForKey:@"senderImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        [Helper makeItRound:_imgSenderImage.layer withRadius:25.0];
        _lblSenderPhone.hidden = [[senderData objectForKey:@"senderPhone"] isEqualToString:Empty];
        _lblSenderPhone.text = [senderData objectForKey:@"senderPhone"];
    }
    
    NSDictionary *angelerData = [data objectForKey:@"angelerDetails"];
    
    if ([angelerData allKeys].count == 0){
        [GlobalInfo hideViewNamed:_viewAngeler withTopName:@"angelerTop"];
    }else{
        [Helper makeItRound:_imgAngelerImage.layer withRadius:25.0];
        [_imgAngelerImage sd_setImageWithURL:[NSURL URLWithString:[angelerData objectForKey:@"angelerImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        _lblAngelerName.text = [angelerData objectForKey:@"angelerName"];
        _lblAngelerEmail.text = [angelerData objectForKey:@"angelerEmail"];
        _lblAngelerPhone.hidden = [[angelerData objectForKey:@"angelerPhone"] isEqualToString:Empty];
        _lblAngelerPhone.text = [angelerData objectForKey:@"angelerPhone"];
    }
    
    NSDictionary *recipientData = [data objectForKey:@"recipientDetails"];
    
    if ([recipientData allKeys].count == 0){
        [GlobalInfo hideViewNamed:_viewRecipient withTopName:@"recipientTop"];
    }else{
        [Helper makeItRound:_imgRecipientImage.layer withRadius:25.0];
        [_imgRecipientImage sd_setImageWithURL:[NSURL URLWithString:[angelerData objectForKey:@"recipientImage"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
        _lblRecipientName.text = [recipientData objectForKey:@"recipientName"];
        _lblRecipientEmail.text = [recipientData objectForKey:@"recipientEmail"];
        _lblRecipientPhone.hidden = [[recipientData objectForKey:@"recipientPhone"] isEqualToString:Empty];
        _lblRecipientPhone.text = [recipientData objectForKey:@"recipientPhone"];
    }

    
    NSDictionary *packageData = [data objectForKey:@"packageDetails"];
    {
        _lblPackageType.text = [GlobalInfo TypeOfPackage:[[packageData objectForKey:@"packageType"] integerValue]];
        _lblPackageSize.text = [NSString stringWithFormat:@"%@x%@x%@ CM",[packageData objectForKey:@"packageHeight"],[packageData objectForKey:@"packageWidth"],[packageData objectForKey:@"packageLength"]];
        _lblPackageWt.text = [NSString stringWithFormat:@"%@ GM",[packageData objectForKey:@"packageWeight"]];
        _lblStatusText.attributedText = [GlobalInfo formatFinalStatusText:[NSString stringWithFormat:@"Status: %@",[packageData objectForKey:@"status"]] andIndex:7];
        _lblPackageRemarks.text = [packageData objectForKey:@"packageRemarks"];
    }
    
    NSArray *packageImages = [data objectForKey:@"packageImages"];
    if (packageImages.count == 0){
        [GlobalInfo hideViewNamed:_viewPictures withTopName:@"pictureTop"];
    }else{
        self.arrPhotos = [NSMutableArray array];
       // NSArray *imgArr = [data objectForKey:@"packageImages"];
        for (int i = 1; i < packageImages.count+1; i++) {
            UIImageView *imgV = [_viewPictures viewWithTag:i];
            imgV.hidden = NO;
            [imgV sd_setImageWithURL:[NSURL URLWithString:[[packageImages objectAtIndex:i-1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
            
            imgV.userInteractionEnabled = YES;
            [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
            [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[packageImages objectAtIndex:i-1] objectForKey:@"image"]]]];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:[tapGest view].tag-1];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}
- (IBAction)callSender:(id)sender {
   // if (_lblSenderPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblSenderPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    //}
}
- (IBAction)callAngeler:(id)sender {
    //if (_lblAngelerPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblAngelerPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
    //}
}
- (IBAction)callRecipient:(id)sender {
   // if (_lblRecipientPhone.text.length > PHONE_MIN_DIGIT) {
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_lblRecipientPhone.text]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        }
   // }
}
- (IBAction)backtapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
