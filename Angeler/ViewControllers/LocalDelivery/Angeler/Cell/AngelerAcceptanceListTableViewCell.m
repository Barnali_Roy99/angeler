//
//  AngelerAcceptanceListTableViewCell.m
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AngelerAcceptanceListTableViewCell.h"

@implementation AngelerAcceptanceListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.containerView makeCornerRound:15.0];
    [self.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnShowStatus makeCornerRound:15.0];
    self.ItemImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.ItemImageView.layer.borderWidth = 1.0;
    self.senderProfileImageview.layer.borderColor = [[UIColor blackColor] CGColor];
    self.senderProfileImageview.layer.borderWidth = 1.0;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
