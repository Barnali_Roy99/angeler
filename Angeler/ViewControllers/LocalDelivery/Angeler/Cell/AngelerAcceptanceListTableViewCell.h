//
//  AngelerAcceptanceListTableViewCell.h
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AngelerAcceptanceListTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *btnShowStatus;
@property (weak, nonatomic) IBOutlet RoundImageView *ItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffLocation;
//@property (weak, nonatomic) IBOutlet UIImageView *chatIconImageview;
@property (weak, nonatomic) IBOutlet RoundImageView *senderProfileImageview;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEarningPrice;

@end

NS_ASSUME_NONNULL_END
