//
//  AngelerAcceptanceDetailsVC.m
//  Angeler
//
//  Created by AJAY on 28/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AngelerAcceptanceDetailsVC.h"
#import "MWPhotoBrowser.h"
#import "AngelerAcceptanveListVC.h"

@interface AngelerAcceptanceDetailsVC ()<MWPhotoBrowserDelegate,UITextFieldDelegate>
{
    BOOL isPickUp,isSenderImgTap,isPictureImgTap;
    NSDictionary *data;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

//PickUp / Delivery Code

@property (weak, nonatomic) IBOutlet UIView *viewPickupDeliveryCodeDetails;
@property (weak, nonatomic) IBOutlet UIView *viewPickupDeliveryCodeInfo;
@property (weak, nonatomic) IBOutlet UIView *viewPickupDeliveryCodeDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupDeliveryCodeDetailsTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtPickupDeliveryCode;
@property (weak, nonatomic) IBOutlet UIButton *btnPickUpDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterPickUpDeliveryCodeTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickUpDeliveryViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *contentView;

//Item Details

@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;

//Sender
@property (weak, nonatomic) IBOutlet UIView *viewSenderDetails;
@property (weak, nonatomic) IBOutlet UIView *viewSenderDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSenderPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgSenderImage;


//PickUp Details
@property (weak, nonatomic) IBOutlet UIView *viewPickUpDetails;
@property (weak, nonatomic) IBOutlet UIView *viewPickUpDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUprDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonEmail;

//DropOff Details
@property (weak, nonatomic) IBOutlet UIView *viewDropOffDetails;
@property (weak, nonatomic) IBOutlet UIView *viewDropOffDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffrDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonEmail;


@property (nonatomic, strong) NSMutableArray *arrSenderPhoto;
@property (nonatomic, strong) NSMutableArray *arrPhotos;


@end

@implementation AngelerAcceptanceDetailsVC

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToFetchPickupDeliveryDetailsInfo];

    // Do any additional setup after loading the view from its nib.
}


#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

#pragma mark - General Functions

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
    
}


#pragma mark - General functions

-(void)initialSetUp
{
    [self setLocalization];
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    [_topNavbarView showNavbackBtn];
    
    [self.topNavbarView setTitle:[NSLocalizedString(@"Accepted_Local_Delivery_Details", nil) capitalizedString]];
    
    //Item Details
    
    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
    //Sender Details
    
    [self.viewSenderDetails makeCornerRound:15.0];
    [self.viewSenderDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //PickUp Details
    
    [self.viewPickUpDetails makeCornerRound:15.0];
    [self.viewPickUpDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //DropOff Details
    
    [self.viewDropOffDetails makeCornerRound:15.0];
    [self.viewDropOffDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //PickUp Delivery Code
    
    [self.viewPickupDeliveryCodeDetails makeCornerRound:15.0];
    [self.viewPickupDeliveryCodeDetails makeCornerRound:15.0];
    [self.viewPickupDeliveryCodeDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnPickUpDelivery makeCornerRound:15.0];
    

}



-(void)roundAllSpecificCorner
{
    
    [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewSenderDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewPickupDeliveryCodeDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewPickUpDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewDropOffDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
  
    [_txtPickupDeliveryCode setPlaceholder:NSLocalizedString(@"Enter_Code", nil)];
    _lblPickUprDetailsTitle.text = NSLocalizedString(@"Pickup_Details",nil);
    _lblDropOffrDetailsTitle.text = NSLocalizedString(@"DropOff_Details",nil);
    
    //Item Details View
    _lblItemDetailsHeaderTitle.text = NSLocalizedString(@"Item_Details", nil);
    _lblItemDetailsTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Details",nil)];
    _lblItemNameTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Name",nil)];
    _lblItemDeliveryPriceTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Delivery_Price",nil)];
    _lblItemStatusTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Status",nil)];
    
    //Sender Details
    
    _lblSenderDetailsTitle.text = NSLocalizedString(@"Sender_Details", nil);
    
}

#pragma mark - API Call

- (void)callApiToFetchPickupDeliveryDetailsInfo{
    
    NSDictionary *param = @{@"deliveryId":_deliveryID,
                            @"caller" : @"accepted_delivery_details"
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliveryDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           data = [responseObject objectForKey:@"responseData"];
                                                           [self updateValueWithData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}



-(void)updateValueWithData{
    
    NSString *deliveryItemStatus = [data objectForKey:@"status"];
    
    if([deliveryItemStatus isEqualToString:LocalDeliveryStatus_Delivered]){
        [self setDeliveredStatusConstraint];
        _viewPickupDeliveryCodeDetails.hidden = YES;
        _pickUpDeliveryViewHeightConstraint.constant = 0;
    }
    
    //PickUp / Delivery Code
    
     if([deliveryItemStatus isEqualToString:LocalDeliveryStatus_Accepted]){
         
         [self setAcceptedStatusConstraint];
         
         isPickUp = true;
             _lblPickupDeliveryCodeDetailsTitle.text = NSLocalizedString(@"Pickup_Code", nil);
          NSString *title = [NSString stringWithFormat:NSLocalizedString(@"LocalDelivery_Enter_Pickup_Code", nil), [[data objectForKey:@"pickupName"] uppercaseString]];
         _lblEnterPickUpDeliveryCodeTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[[data objectForKey:@"pickupName"] uppercaseString]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
             [self.btnPickUpDelivery setTitle:NSLocalizedString(@"Validate_Pick_Up", nil) forState:UIControlStateNormal];
     }
     else  if([deliveryItemStatus isEqualToString:LocalDeliveryStatus_Recieved]){
         
          [self setRecievedStatusConstraint];
         isPickUp = false;
         _lblPickupDeliveryCodeDetailsTitle.text = NSLocalizedString(@"Delivery_Code", nil);
         NSString *title = [NSString stringWithFormat:NSLocalizedString(@"LocalDelivery_Enter_Delivery_Code", nil), [[data objectForKey:@"dropoffName"] uppercaseString]];
         _lblEnterPickUpDeliveryCodeTitle.attributedText = [Helper getBolHighlightedText:title withRange:[title rangeOfString:[[data objectForKey:@"dropoffName"] uppercaseString]] withColor:[UIColor appThemeBlueColor] withBoldTextFontSize:15.0];
         [self.btnPickUpDelivery setTitle:NSLocalizedString(@"Validate_Drop_Off", nil) forState:UIControlStateNormal];
     }
    

    //Sender Details
    
    if ([data objectForKey:@"senderProfilepic"] == nil || [[data objectForKey:@"senderProfilepic"] isEqualToString:@""])
    {
        _imgSenderImage.userInteractionEnabled = NO;
    }
    else
    {
        _imgSenderImage.userInteractionEnabled = YES;
        self.arrSenderPhoto = [NSMutableArray array];
        [_imgSenderImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSenderImage:)]];
        
        MWPhoto *senderPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"senderProfilepic"]]];
        senderPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Sender", nil),[data objectForKey:@"senderName"]];
        [self.arrSenderPhoto addObject:senderPhoto];
        [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"senderProfilepic"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
    }
    
    _lblSenderName.text = [data objectForKey:@"senderName"];
    _lblSenderEmail.text = [data objectForKey:@"senderEmail"];
    _lblSenderPhoneNumber.text = [data objectForKey:@"senderPhoneNo"];
    
    
    //Item Details
    _lblItemName.text = [data objectForKey:@"itemName"];
    _lblItemDetails.text = [data objectForKey:@"itemDetails"];
    _lblItemDeliveryPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"deliveryPrice"],NSLocalizedString(@"Credits", nil)];
    _lblItemStatus.text = [data objectForKey:@"statusText"];
    
    //Picture of item
    {
        
        NSArray *imges = [data objectForKey:@"itemImages"];
        if (imges.count == 0) {
            //  [GlobalInfo hideViewNamed:_pictureView withTopName:@"pictureTop"];
        }else{
            self.arrPhotos = [NSMutableArray array];
            for (int i = 1; i <= imges.count ; i++) {
                UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
                imgV.hidden = NO;
                imgV.userInteractionEnabled = YES;
                [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
                NSLog(@"image url =====  %@",[imges objectAtIndex:i-1] );
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"] ]placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
                MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]]];
                photo.caption = @"";
                [self.arrPhotos addObject:photo];
                
            }
        }
    }
    
    //PickUp Person Details
    
    _lblPickUpPersonName.text = [data objectForKey:@"pickupName"];
    _lblPickUpPersonEmail.text = [data objectForKey:@"pickupEmail"];
    _lblPickUpPersonAddress.text = [data objectForKey:@"pickupAddress"];
    
    //DropOff Person Details
    
    _lblDropOffPersonName.text = [data objectForKey:@"dropoffName"];
    _lblDropOffPersonEmail.text = [data objectForKey:@"dropoffEmail"];
    _lblDropOffPersonAddress.text = [data objectForKey:@"dropoffAddress"];
    
    
}

#pragma mark - Gesture Action

- (void)tapOnSenderImage:(UITapGestureRecognizer*)tapGest
{
    isSenderImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
}


- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    
    isSenderImgTap = false;
    isPictureImgTap = true;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
    
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isSenderImgTap)
    {
        return self.arrSenderPhoto.count;
    }
    
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (isSenderImgTap)
    {
        if (index < self.arrSenderPhoto.count) {
            return [self.arrSenderPhoto objectAtIndex:index];
        }
    }
    
    else
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    return nil;
}




#pragma mark - Button Action

- (IBAction)pickUpDeliveryTapped:(id)sender {
    if ([Helper checkEmptyField:_txtPickupDeliveryCode.text]) {
        
        if(isPickUp)
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Validate_LocalDelivery_PickUp_Alert", nil)];
        else
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Validate_LocalDelivery_Dropoff_Alert", nil)];
        return;
    }
    
    NSDictionary *param = @{@"deliveryId":_deliveryID,
                            @"code":_txtPickupDeliveryCode.text
                            };

    NSString *apiMethodname = (isPickUp) ? kUserLocalDeliveryCollectItem : kUserLocalDeliveryDropItem;
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:apiMethodname
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {

                                                           UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                                      //         [self.navigationController popViewControllerAnimated:true];
                                                               
                                                               CATransition* transition = [CATransition animation];
                                                                    transition.duration = 0.3;
                                                                    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                                                                    transition.type = kCATransitionMoveIn; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
                                                                    transition.subtype = kCATransitionFromLeft; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
                                                               
                                                               AngelerAcceptanveListVC *Obj = [[AngelerAcceptanveListVC alloc] initWithNibName:@"AngelerAcceptanveListVC" bundle:nil];
                                                               [self.navigationController.view.layer addAnimation:transition forKey:nil];
                                                               [self.navigationController pushViewController:Obj animated:YES];
                                                               
                                                               
                                                               
                                                           }];
                                                           NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [GlobalInfo showAlertTitle:Empty Message:message actions:[NSArray arrayWithObject:cancel]];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}

- (IBAction)callSender:(id)sender {
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[data objectForKey:@"senderPhoneNo"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    
}

#pragma mark - Constraint Set Dynamic reorder of block Based on status

//1. QR Code Layout
//2. Item Details Layout
//3. Sender Details Layout
//4. Pickup Details Layout
//5. Drop Off Details Layout

/* For status: Accepted

1
4
2
3
5*/

-(void)setAcceptedStatusConstraint{
    [self.viewPickUpDetails.topAnchor constraintEqualToAnchor:self.viewPickupDeliveryCodeDetails.bottomAnchor constant:15].active = YES;
    [self.viewItemDetails.topAnchor constraintEqualToAnchor:self.viewPickUpDetails.bottomAnchor constant:15].active = YES;
    [self.viewSenderDetails.topAnchor constraintEqualToAnchor:self.viewItemDetails.bottomAnchor constant:15].active = YES;
    [self.viewDropOffDetails.topAnchor constraintEqualToAnchor:self.viewSenderDetails.bottomAnchor constant:15].active = YES;
    [self.contentView.bottomAnchor constraintEqualToAnchor:self.viewDropOffDetails.bottomAnchor constant:15].active = YES;

}

/*For status: Recieved

1
5
2
3
4*/

-(void)setRecievedStatusConstraint{
    
   [self.viewDropOffDetails.topAnchor constraintEqualToAnchor:self.viewPickupDeliveryCodeDetails.bottomAnchor constant:15].active = YES;
    [self.viewItemDetails.topAnchor constraintEqualToAnchor:self.viewDropOffDetails.bottomAnchor constant:15].active = YES;
     [self.viewSenderDetails.topAnchor constraintEqualToAnchor:self.viewItemDetails.bottomAnchor constant:15].active = YES;
     [self.viewPickUpDetails.topAnchor constraintEqualToAnchor:self.viewSenderDetails.bottomAnchor constant:15].active = YES;
    [self.contentView.bottomAnchor constraintEqualToAnchor:self.viewPickUpDetails.bottomAnchor constant:15].active = YES;

}
/* For status: Delivered

2
3
4
5*/

-(void)setDeliveredStatusConstraint{
    
     [self.viewItemDetails.topAnchor constraintEqualToAnchor:self.viewPickupDeliveryCodeDetails.bottomAnchor constant:15].active = YES;
    [self.viewSenderDetails.topAnchor constraintEqualToAnchor:self.viewItemDetails.bottomAnchor constant:15].active = YES;
    [self.viewPickUpDetails.topAnchor constraintEqualToAnchor:self.viewSenderDetails.bottomAnchor constant:15].active = YES;
    [self.viewDropOffDetails.topAnchor constraintEqualToAnchor:self.viewPickUpDetails.bottomAnchor constant:15].active = YES;
     [self.contentView.bottomAnchor constraintEqualToAnchor:self.viewDropOffDetails.bottomAnchor constant:15].active = YES;
    
}

@end
