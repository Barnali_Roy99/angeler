//
//  AngelerAcceptanveListVC.m
//  Angeler
//
//  Created by AJAY on 27/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AngelerAcceptanveListVC.h"
#import "AngelerAcceptanceListTableViewCell.h"
#import "MWPhotoBrowser.h"
#import "AngelerAcceptanceDetailsVC.h"

static NSString *CellIdentifier = @"AngelerAcceptanceListTableViewCell";

@interface AngelerAcceptanveListVC ()<MWPhotoBrowserDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
    BOOL isItemImgTap,isSenderImgTap;
    
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblAngelerAcceptanceItemList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, strong) NSMutableArray *arrAngelerAcceptanceItemList;
@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (nonatomic, strong) NSMutableArray *arrItemSenderImage;

@end

@implementation AngelerAcceptanveListVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
     [self initialLoadList];
}

#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
     [self initialLoadList];
    [refreshController endRefreshing];
}


#pragma mark- General Functions

-(void)initialLoadList{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadAngelerAcceptanceList];
}

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    //[_topNavbarView showNavbackBtn];
    [_topNavbarView showNavMenuBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Accepted_Local_Deliveries", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    [_tblAngelerAcceptanceItemList registerNib:[UINib nibWithNibName:@"AngelerAcceptanceListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblAngelerAcceptanceItemList.estimatedRowHeight = 20;
    _tblAngelerAcceptanceItemList.rowHeight = UITableViewAutomaticDimension;
    
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblAngelerAcceptanceItemList addSubview:refreshController];
    
}

#pragma mark - Webservice Call Method

- (void)callServiceToLoadAngelerAcceptanceList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"caller" : @"accepted_delivery"
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliveryList  //Put it here while service will be ready
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       if ([GlobalInfo checkStatusSuccess:responseObject])  {
                                                           
                                                           if (_arrAngelerAcceptanceItemList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrAngelerAcceptanceItemList removeAllObjects];
                                                               }else if(_arrAngelerAcceptanceItemList.count > START_POSITION){
                                                                   [_arrAngelerAcceptanceItemList removeObjectsInRange:NSMakeRange(START_POSITION, _arrAngelerAcceptanceItemList.count - START_POSITION)];
                                                               }
                                                               [_arrAngelerAcceptanceItemList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrAngelerAcceptanceItemList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           _lblNoRecord.hidden = _arrAngelerAcceptanceItemList.count > 0;
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601)
                                                           {
                                                               _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           }
                                                           [_tblAngelerAcceptanceItemList reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



#pragma mark - Scrollview Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //    NSLog(@"currentOffset %f",currentOffset);
    NSLog(@"maximumOffset currentOffset %f %f",maximumOffset,currentOffset);
    
    //NSInteger result = maximumOffset - currentOffset;
    
    CGFloat offsetDifference = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (offsetDifference <= 100.0) {
        if(moreRecordsAvailable)
        {
            NSLog(@"API Called");
            [self callServiceToLoadAngelerAcceptanceList];
        }
    }
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrAngelerAcceptanceItemList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AngelerAcceptanceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITapGestureRecognizer *itemImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemImageViewTapped:)];
    itemImageViewTap.numberOfTapsRequired = 1;
    [cell.ItemImageView addGestureRecognizer:itemImageViewTap];
    cell.ItemImageView.tag = indexPath.row + 1;
    [cell.ItemImageView setUserInteractionEnabled:YES];
    
    
    UITapGestureRecognizer *itemSenderImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemSenderImageViewTapped:)];
    itemSenderImageViewTap.numberOfTapsRequired = 1;
    [cell.senderProfileImageview addGestureRecognizer:itemSenderImageViewTap];
    cell.senderProfileImageview.tag = indexPath.row + 1;
    [cell.senderProfileImageview setUserInteractionEnabled:YES];
    
    
    
   
    
    cell.btnShowStatus.tag = indexPath.row + 1;
    [cell.btnShowStatus addTarget:self action:@selector(cellActionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *data = [_arrAngelerAcceptanceItemList objectAtIndex:indexPath.row];
    
    [cell.ItemImageView sd_setImageWithURL:[NSURL URLWithString:[[data objectForKey:@"itemImages"] objectAtIndex:0]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    [cell.senderProfileImageview sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"senderProfilepic"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    cell.lblItemName.text = [data objectForKey:@"itemName"];
   
    cell.lblPickUpLocation.text = [data objectForKey:@"pickupAddress"];
    cell.lblDropOffLocation.text = [data objectForKey:@"dropoffAddress"];
    NSString *totalText = [NSString stringWithFormat:@"%@ %@ ANC",NSLocalizedString(@"You_Earn", nil),[data objectForKey:@"deliveryPrice"]];
    cell.lblAngelerEarningPrice.attributedText = [Helper getColorAttributedText:totalText :[totalText rangeOfString:NSLocalizedString(@"You_Earn", nil)] withColor:[UIColor appThemeBlueColor]];
    
    
     [cell.btnShowStatus setTitle:[data objectForKey:@"buttonText"] forState:UIControlStateNormal];
    
    
    return cell;
}

#pragma mark - Table view Action

- (void)itemImageViewTapped:(UITapGestureRecognizer*)sender{
    UIView *itemImgViewView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrAngelerAcceptanceItemList objectAtIndex:itemImgViewView.tag - 1];
    
    NSArray *itemImages = [data objectForKey:@"itemImages"];
    self.arrPhotos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < itemImages.count; i++) {
        [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[itemImages objectAtIndex:i] ]]];
    }
    
    isSenderImgTap = false;
    isItemImgTap = true;
    
    [self setMWPHOtoBrowser];
    
}

- (void)itemSenderImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *buyerProfileImgViewView = (UIImageView *)sender.view;
    
    NSDictionary *data = [_arrAngelerAcceptanceItemList objectAtIndex:buyerProfileImgViewView.tag - 1];
    
    
    self.arrItemSenderImage = [[NSMutableArray alloc] init];
    
    MWPhoto *buyerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"senderProfilepic"]]];
    buyerPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Sender", nil),[data objectForKey:@"senderName"]];
    
    
    [self.arrItemSenderImage addObject:buyerPhoto];
    
    
    isSenderImgTap = true;
    isItemImgTap = false;
    
    [self setMWPHOtoBrowser];
    
}



- (void)cellActionButtonTapped:(UIButton*)sender{
    NSDictionary *data = [_arrAngelerAcceptanceItemList objectAtIndex:sender.tag - 1];
    AngelerAcceptanceDetailsVC *Obj = [[AngelerAcceptanceDetailsVC alloc] initWithNibName:@"AngelerAcceptanceDetailsVC" bundle:nil];
    Obj.deliveryID = [data objectForKey:@"deliveryId"];
    [self.navigationController pushViewController:Obj animated:YES];
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if(isItemImgTap)
        return self.arrPhotos.count;
    else if(isSenderImgTap)
        return self.arrItemSenderImage.count;
    else
        return 0;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    
    if(isItemImgTap)
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    else if(isSenderImgTap){
        if (index < self.arrItemSenderImage.count) {
            return [self.arrItemSenderImage objectAtIndex:index];
        }
    }
    return nil;
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}


@end
