//
//  LocalDeliveryDashboradVC.m
//  Angeler
//
//  Created by AJAY on 05/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "LocalDeliveryDashboradVC.h"
#import "AddLocalDeliveryVC.h"
#import "LocalDeliveryRequestListVC.h"
#import "AngelerAcceptanveListVC.h"
#import "SenderRequestListVC.h"

@interface LocalDeliveryDashboradVC ()

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblShareAngelerTitle;
@property (weak, nonatomic) IBOutlet UIView *viewShareAngeler;
@property (weak, nonatomic) IBOutlet UIView *viewAddLocalDelivery;
@property (weak, nonatomic) IBOutlet UIView *viewAllLocalDeliveryReqList;
@property (weak, nonatomic) IBOutlet UIView *viewMyPostedLocalDeliveryList;
@property (weak, nonatomic) IBOutlet UIView *viewMyAcceptedLocalDeliveryList;
@property (weak, nonatomic) IBOutlet UILabel *lblAddLocalDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lblAllLocalDeliveryReqList;
@property (weak, nonatomic) IBOutlet UILabel *lblMyPostedLocalDeliveryList;
@property (weak, nonatomic) IBOutlet UILabel *lblMyAcceptedLocalDeliveryList;


@end

@implementation LocalDeliveryDashboradVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self setLocalization];
    [self setGesture];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark- General Functions

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView showTitleLogo];
   
}

-(void)setLocalization{
    //Localization
    
    _lblTitle.text = NSLocalizedString(@"Be_Paid_To_Travel", nil);
    _lblDesc.text = NSLocalizedString(@"Sender_Easier", nil);
    _lblAddLocalDelivery.text = NSLocalizedString(@"Add_Local_Delivery",nil);
    _lblAllLocalDeliveryReqList.text = NSLocalizedString(@"Local_Delivery_List",nil);
    _lblMyPostedLocalDeliveryList.text = NSLocalizedString(@"Local_Delivery_Requests",nil);
    _lblMyAcceptedLocalDeliveryList.text = NSLocalizedString(@"Accepted_Local_Deliveries",nil);
}

-(void)setGesture
{
    UITapGestureRecognizer *shareAngelerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareAngelerViewTapped:)];
    [_viewShareAngeler addGestureRecognizer:shareAngelerTapRecognizer];
    
    
    UITapGestureRecognizer *addLocalDeliveryTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addLocalDeliveryViewTapped:)];
    [_viewAddLocalDelivery addGestureRecognizer:addLocalDeliveryTapRecognizer];
    
    
    UITapGestureRecognizer *allLocalDeliverTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(AllLocalDeliveryReqListViewTapped:)];
    [_viewAllLocalDeliveryReqList addGestureRecognizer:allLocalDeliverTapRecognizer];
    
    UITapGestureRecognizer *myPostedLocalDeliveryTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myPostedLocalDeliveryViewTapped:)];
    [_viewMyPostedLocalDeliveryList addGestureRecognizer:myPostedLocalDeliveryTapRecognizer];
    
    
    UITapGestureRecognizer *viewMyAcceptanceTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMyAcceptanceListViewTapped:)];
    [_viewMyAcceptedLocalDeliveryList addGestureRecognizer:viewMyAcceptanceTapRecognizer];
}

#pragma mark- Gesture Action

- (void)addLocalDeliveryViewTapped:(UITapGestureRecognizer*)sender
{
    AddLocalDeliveryVC *Obj = [[AddLocalDeliveryVC alloc] initWithNibName:@"AddLocalDeliveryVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}

- (void)AllLocalDeliveryReqListViewTapped:(UITapGestureRecognizer*)sender
{

    LocalDeliveryRequestListVC *Obj = [[LocalDeliveryRequestListVC alloc] initWithNibName:@"LocalDeliveryRequestListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
    
}
- (void)myPostedLocalDeliveryViewTapped:(UITapGestureRecognizer*)sender
{
    SenderRequestListVC *Obj = [[SenderRequestListVC alloc] initWithNibName:@"SenderRequestListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}

- (void)showMyAcceptanceListViewTapped:(UITapGestureRecognizer*)sender
{
   AngelerAcceptanveListVC *Obj = [[AngelerAcceptanveListVC alloc] initWithNibName:@"AngelerAcceptanveListVC" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}

- (void)shareAngelerViewTapped:(UITapGestureRecognizer*)sender
{
    NSString *shareData=[NSString stringWithFormat:NSLocalizedString(@"Share_Link_Title", nil),@""];
    
    NSArray *objectsToShare = @[shareData];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


@end
