//
//  AngelerPendingReqDetails.m
//  Angeler
//
//  Created by AJAY on 28/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "AngelerPendingReqDetails.h"
#import "MWPhotoBrowser.h"
#import "SharePopUpViewController.h"
#import "AngelerAcceptanceDetailsVC.h"

@interface AngelerPendingReqDetails ()<MWPhotoBrowserDelegate,UITextFieldDelegate>
{
    BOOL isSenderImgTap,isPictureImgTap;
    NSDictionary *data;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;



//Item Details

@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemFeeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemFee;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;


//Sender
@property (weak, nonatomic) IBOutlet UIView *viewSenderDetails;
@property (weak, nonatomic) IBOutlet UIView *viewSenderDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSenderPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acceptViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIImageView *imgSenderImage;



//PickUp Details
@property (weak, nonatomic) IBOutlet UIView *viewPickUpDetails;
@property (weak, nonatomic) IBOutlet UIView *viewPickUpDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUprDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonEmail;
@property (weak, nonatomic) IBOutlet GMSMapView *pickUpmapView;

//DropOff Details
@property (weak, nonatomic) IBOutlet UIView *viewDropOffDetails;
@property (weak, nonatomic) IBOutlet UIView *viewDropOffDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffrDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonEmail;
@property (weak, nonatomic) IBOutlet GMSMapView *dropOffmapView;


@property (nonatomic, strong) NSMutableArray *arrSenderPhoto;
@property (nonatomic, strong) NSMutableArray *arrPhotos;


@end

@implementation AngelerPendingReqDetails

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToFetchPickupDeliveryDetailsInfo];

    // Do any additional setup after loading the view from its nib.
}


#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

#pragma mark - General Functions

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
    
}


#pragma mark - General functions

-(void)initialSetUp
{
    [self setLocalization];
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    [_topNavbarView showNavbackBtn];
    
    [self.topNavbarView setTitle:[NSLocalizedString(@"Local_Delivery_Details", nil) capitalizedString]];
    
    
    
    //Item Details
    
    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
    //Sender Details
    
    [self.viewSenderDetails makeCornerRound:15.0];
    [self.viewSenderDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Accept
       
       [self.btnAccept makeCornerRound:15.0];
       [self.btnAccept dropShadowWithColor:[UIColor lightishGrayColor]];
     
    
    //PickUp Details
    
    [self.viewPickUpDetails makeCornerRound:15.0];
    [self.viewPickUpDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //DropOff Details
    
    [self.viewDropOffDetails makeCornerRound:15.0];
    [self.viewDropOffDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
  

}



-(void)roundAllSpecificCorner
{
    
    [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewSenderDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewPickUpDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewDropOffDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
  
    _lblPickUprDetailsTitle.text = NSLocalizedString(@"Pickup_Details",nil);
    _lblDropOffrDetailsTitle.text = NSLocalizedString(@"DropOff_Details",nil);
    [_btnAccept setTitle:NSLocalizedString(@"Accept",nil) forState:UIControlStateNormal];
    
    //Item Details View
    _lblItemDetailsHeaderTitle.text = NSLocalizedString(@"Item_Details", nil);
    _lblItemDetailsTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Details",nil)];
    _lblItemNameTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Name",nil)];
    _lblItemDeliveryPriceTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Delivery_Price",nil)];
//    _lblItemFeeTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Fee",nil)];
    _lblItemStatusTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Status",nil)];
    
   
    
    //Sender Details
    
    _lblSenderDetailsTitle.text = NSLocalizedString(@"Sender_Details", nil);
    
    
}

#pragma mark - API Call

- (void)callApiToAcceptShopItem{
    
   
    NSDictionary *param = @{@"deliveryId":_deliveryID
                            };
    
    
    //countryId, name, description, keyword, address, url
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserAcceptLocalDelivery
                                                    Method:REQMETHOD_POST
                                                    parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                           if (statusCode == 200)
                                                           {
                                                               [self showAlertPopUp];
                                                           }
                                                          
                                                           else{
                                                               [Helper showAlertWithTitle:@"" Message:message];
                                                           }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}




- (void)callApiToFetchPickupDeliveryDetailsInfo{
    
    NSDictionary *param = @{@"deliveryId":_deliveryID,
                            @"caller" : @"pending_delivery_details"
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliveryDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           data = [responseObject objectForKey:@"responseData"];
                                                           [self updateValueWithData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}



-(void)updateValueWithData{
    
    //Sender Details
    
    if ([data objectForKey:@"senderProfilepic"] == nil || [[data objectForKey:@"senderProfilepic"] isEqualToString:@""])
    {
        _imgSenderImage.userInteractionEnabled = NO;
    }
    else
    {
        _imgSenderImage.userInteractionEnabled = YES;
        self.arrSenderPhoto = [NSMutableArray array];
        [_imgSenderImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSenderImage:)]];
        
        MWPhoto *senderPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"senderProfilepic"]]];
        senderPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Sender", nil),[data objectForKey:@"senderName"]];
        [self.arrSenderPhoto addObject:senderPhoto];
        [_imgSenderImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"senderProfilepic"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
    }
    
    _lblSenderName.text = [data objectForKey:@"senderName"];
    _lblSenderEmail.text = [data objectForKey:@"senderEmail"];
    _lblSenderPhoneNumber.text = [data objectForKey:@"senderPhoneNo"];
    
    if([[[GlobalInfo sharedInfo]userId] isEqualToString:[data objectForKey:@"senderId"]]){
        self.btnAccept.hidden = YES;
        self.acceptViewHeightConstraint.constant = 0;
        self.separatorView.hidden = YES;
    }
   
    
    //Item Details
    _lblItemName.text = [data objectForKey:@"itemName"];
    _lblItemDetails.text = [data objectForKey:@"itemDetails"];
    _lblItemDeliveryPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"deliveryPrice"],NSLocalizedString(@"Credits", nil)];
//    _lblItemFee.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"fee"],NSLocalizedString(@"Credits", nil)];
    _lblItemStatus.text = [data objectForKey:@"statusText"];
    
    //Picture of item
    {
        
        NSArray *imges = [data objectForKey:@"itemImages"];
        if (imges.count == 0) {
            //  [GlobalInfo hideViewNamed:_pictureView withTopName:@"pictureTop"];
        }else{
            self.arrPhotos = [NSMutableArray array];
            for (int i = 1; i <= imges.count ; i++) {
                UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
                imgV.hidden = NO;
                imgV.userInteractionEnabled = YES;
                [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
                NSLog(@"image url =====  %@",[imges objectAtIndex:i-1] );
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"] ]placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
                MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]]];
                photo.caption = @"";
                [self.arrPhotos addObject:photo];
                
            }
        }
    }
    
    //PickUp Person Details
    
    _lblPickUpPersonName.text = [data objectForKey:@"pickupName"];
    _lblPickUpPersonEmail.text = [data objectForKey:@"pickupEmail"];
    _lblPickUpPersonAddress.text = [data objectForKey:@"pickupAddress"];
    
    
    
    //Map
    {
        CLLocationCoordinate2D pickUp_coordinate = CLLocationCoordinate2DMake([[data objectForKey:@"pickupLat"] doubleValue] ,[[data objectForKey:@"pickupLon"] doubleValue]);
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:pickUp_coordinate zoom:12];
        
        GMSMarker *pickUp_marker = [[GMSMarker alloc] init];
        pickUp_marker.position = pickUp_coordinate;
        pickUp_marker.title = [data objectForKey:@"pickupAddress"];
        pickUp_marker.map = _pickUpmapView;
        

        _pickUpmapView.camera = camera;
        [_pickUpmapView setSelectedMarker:pickUp_marker];
        
    }
    
    
    //DropOff Person Details
    
    _lblDropOffPersonName.text = [data objectForKey:@"dropoffName"];
    _lblDropOffPersonEmail.text = [data objectForKey:@"dropoffEmail"];
    _lblDropOffPersonAddress.text = [data objectForKey:@"dropoffAddress"];
    
    //Map
    {
        CLLocationCoordinate2D dropOff_coordinate = CLLocationCoordinate2DMake([[data objectForKey:@"dropoffLat"] doubleValue] ,[[data objectForKey:@"dropoffLon"] doubleValue]);
        
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:dropOff_coordinate zoom:12];
        
        GMSMarker *dropOff_marker = [[GMSMarker alloc] init];
        dropOff_marker.position = dropOff_coordinate;
        dropOff_marker.title = [data objectForKey:@"dropoffAddress"];
        dropOff_marker.map = _dropOffmapView;
        

        _dropOffmapView.camera = camera;
        [_dropOffmapView setSelectedMarker:dropOff_marker];
        
    }
    
    
    
}

#pragma mark - Gesture Action

- (void)tapOnSenderImage:(UITapGestureRecognizer*)tapGest
{
    isSenderImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
}



- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    
    isSenderImgTap = false;
    isPictureImgTap = true;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
    
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isSenderImgTap)
    {
        return self.arrSenderPhoto.count;
    }
    
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
     if (isSenderImgTap)
       {
           if (index < self.arrSenderPhoto.count) {
               return [self.arrSenderPhoto objectAtIndex:index];
           }
       }
       
       else
       {
           if (index < self.arrPhotos.count) {
               return [self.arrPhotos objectAtIndex:index];
           }
       }

    return nil;
}

#pragma mark - Button Action

- (IBAction)onAcceptBtnClicked:(id)sender {
    [self callApiToAcceptShopItem];

}

#pragma mark - Share Pop Up show

-(void)showAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Congratulation", nil);
    
    sharePopUpVC.headerDescriptionText = [NSString stringWithFormat: NSLocalizedString(@"Accept_ShoItem_Title_Alert", nil),[data objectForKey:@"itemName"]];
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}


#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    AngelerAcceptanceDetailsVC *Obj = [[AngelerAcceptanceDetailsVC alloc] initWithNibName:@"AngelerAcceptanceDetailsVC" bundle:nil];
    Obj.deliveryID = _deliveryID;
    [self.navigationController pushViewController:Obj animated:YES];
}

@end
