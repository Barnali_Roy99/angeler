//
//  AngelerPendingReqDetails.h
//  Angeler
//
//  Created by AJAY on 28/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AngelerPendingReqDetails : UIViewController
@property(nonatomic,strong)NSString *deliveryID;

@end

NS_ASSUME_NONNULL_END
