//
//  LocalDeliveryRequestListVCTableViewCell.m
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "LocalDeliveryRequestListVCTableViewCell.h"

@implementation LocalDeliveryRequestListVCTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.containerView makeCornerRound:15.0];
    [self.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnViewDetails makeCornerRound:15.0];
    self.ItemImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.ItemImageView.layer.borderWidth = 1.0;
   
    _lblItemDropLocationTitle.text = NSLocalizedString(@"Drop_Location", nil);
    _lblItemPostedDateTitle.text = NSLocalizedString(@"Posted_On", nil);
   _lblViewTitle.text = NSLocalizedString(@"View", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
