//
//  LocalDeliveryRequestListVC.m
//  Angeler
//
//  Created by AJAY on 27/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "LocalDeliveryRequestListVC.h"
#import "LocalDeliveryRequestListVCTableViewCell.h"
#import "AllPostedProductListTableViewCell.h"
#import "SharePopUpViewController.h"
#import "MWPhotoBrowser.h"
#import "AngelerAcceptanceDetailsVC.h"
#import "AngelerPendingReqDetails.h"

static NSString *CellIdentifier = @"LocalDeliveryRequestListVCTableViewCell";


@interface LocalDeliveryRequestListVC ()<MWPhotoBrowserDelegate,SharePopUpDismissDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
    NSString *selectedDeliveryID;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblAllLocalDeliveryReqList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, strong) NSMutableArray *arrAllLocalDeliveryReqList;
@property (nonatomic, strong) NSMutableArray *arrPhotos;

@end

@implementation LocalDeliveryRequestListVC
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
     [self initialLoadList];
}

#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
     [self initialLoadList];
    [refreshController endRefreshing];
}


#pragma mark- General Functions

-(void)initialLoadList{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadAllLocalDelioveryReqList];
}

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
   // [_topNavbarView showNavbackBtn];
     [_topNavbarView showNavMenuBtn];
    [self.topNavbarView setTitle:[NSLocalizedString(@"Local_Delivery_List", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    [_tblAllLocalDeliveryReqList registerNib:[UINib nibWithNibName:@"LocalDeliveryRequestListVCTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblAllLocalDeliveryReqList.estimatedRowHeight = 20;
    _tblAllLocalDeliveryReqList.rowHeight = UITableViewAutomaticDimension;
    
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblAllLocalDeliveryReqList addSubview:refreshController];
    
}

#pragma mark - Webservice Call Method

- (void)callServiceToLoadAllLocalDelioveryReqList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"caller" : @"delivery_lists"
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliveryList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       
                                                       NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                       
                                                       
                                                       NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject])
                                                           {
                                                           
                                                           if (_arrAllLocalDeliveryReqList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrAllLocalDeliveryReqList removeAllObjects];
                                                               }else if(_arrAllLocalDeliveryReqList.count > START_POSITION){
                                                                   [_arrAllLocalDeliveryReqList removeObjectsInRange:NSMakeRange(START_POSITION, _arrAllLocalDeliveryReqList.count - START_POSITION)];
                                                               }
                                                               [_arrAllLocalDeliveryReqList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrAllLocalDeliveryReqList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           _lblNoRecord.hidden = _arrAllLocalDeliveryReqList.count > 0;
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601)
                                                           {
                                                               _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           }
                                                           [_tblAllLocalDeliveryReqList reloadData];
                                                       }
//                                                       else  if (statusCode == 624 || statusCode == 625) {
//                                                           [self showAlertWithMsg:message];
//                                                       }
//                                                       else{
//                                                           [Helper showAlertWithTitle:@"" Message:message];
//                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}



#pragma mark - Scrollview Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //    NSLog(@"currentOffset %f",currentOffset);
    NSLog(@"maximumOffset currentOffset %f %f",maximumOffset,currentOffset);
    
    //NSInteger result = maximumOffset - currentOffset;
    
    CGFloat offsetDifference = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (offsetDifference <= 100.0) {
        if(moreRecordsAvailable)
        {
            NSLog(@"API Called");
            [self callServiceToLoadAllLocalDelioveryReqList];
        }
    }
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrAllLocalDeliveryReqList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LocalDeliveryRequestListVCTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITapGestureRecognizer *itemImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemImageViewTapped:)];
    itemImageViewTap.numberOfTapsRequired = 1;
    [cell.ItemImageView addGestureRecognizer:itemImageViewTap];
    cell.ItemImageView.tag = indexPath.row + 1;
    
    
    
    cell.btnViewDetails .tag = indexPath.row + 1;
    [cell.btnViewDetails addTarget:self action:@selector(viewDetailsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *data = [_arrAllLocalDeliveryReqList objectAtIndex:indexPath.row];
    
    if([[data objectForKey:@"itemImages"] count] > 0){
    [cell.ItemImageView sd_setImageWithURL:[NSURL URLWithString:[[data objectForKey:@"itemImages"] objectAtIndex:0]]
     placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
      cell.ItemImageView.userInteractionEnabled = YES;
    }
    else{
         cell.ItemImageView.userInteractionEnabled = NO;
    }
    cell.lblItemName.text = [data objectForKey:@"itemName"];
    cell.lblItemPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"deliveryPrice"],NSLocalizedString(@"Credits", nil)];
     cell.lblPickUpLocation.text = [data objectForKey:@"pickupAddress"];
    cell.lblDropOffLocation.text = [data objectForKey:@"dropoffAddress"];
    

    NSString *totalText = [NSString stringWithFormat:@"%@ %@ ANC",NSLocalizedString(@"You_Earn", nil),[data objectForKey:@"deliveryPrice"]];
    
    cell.lblAngelerEarningPrice.attributedText = [Helper getColorAttributedText:totalText :[totalText rangeOfString:NSLocalizedString(@"You_Earn", nil)] withColor:[UIColor appThemeBlueColor]];
    
   
    cell.lblItemPostedDate.text = [data objectForKey:@"recordDateTime"];
    
    
    
    return cell;
}

#pragma mark - Table view Action

- (void)itemImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *itemImgViewView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrAllLocalDeliveryReqList objectAtIndex:itemImgViewView.tag - 1];
    
    NSArray *itemImages = [data objectForKey:@"itemImages"];
    self.arrPhotos = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < itemImages.count; i++) {
        [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[itemImages objectAtIndex:i] ]]];
    }
    // [self showZoomImageScreen: self.arrPhotos];
    [self setMWPHOtoBrowser];
    
}

- (void)viewDetailsButtonTapped:(UIButton*)sender{
    
    NSDictionary *data = [_arrAllLocalDeliveryReqList objectAtIndex:sender.tag - 1];
    AngelerPendingReqDetails *Obj = [[AngelerPendingReqDetails alloc] initWithNibName:@"AngelerPendingReqDetails" bundle:nil];
       Obj.deliveryID = [data objectForKey:@"deliveryId"];
       [self.navigationController pushViewController:Obj animated:YES];
    
//    [self callApiToAcceptShopItemWithDeliveryID:[data objectForKey:@"deliveryId"] andItemName:[data objectForKey:@"itemName"]];
}




#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrPhotos.count) {
        return [self.arrPhotos objectAtIndex:index];
    }
    return nil;
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}

@end
