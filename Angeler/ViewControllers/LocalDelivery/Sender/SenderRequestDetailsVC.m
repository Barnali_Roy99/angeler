//
//  SenderRequestDetailsVC.m
//  Angeler
//
//  Created by AJAY on 28/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "SenderRequestDetailsVC.h"
#import "MWPhotoBrowser.h"

@interface SenderRequestDetailsVC ()<MWPhotoBrowserDelegate,UITextFieldDelegate>
{
    BOOL isAngelerImgTap,isPictureImgTap;
    NSDictionary *data;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

//Tracking Status

@property (weak, nonatomic) IBOutlet UIView *viewTrackingStatus;
@property (weak, nonatomic) IBOutlet UIView *viewTrackingStatusHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblTrackingStatusTitle;
@property (weak, nonatomic) IBOutlet UIView *viewTrakingStep;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trackHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *trackViewFirst;
@property (weak, nonatomic) IBOutlet UIView *trackViewlast;

//Item Details

@property (weak, nonatomic) IBOutlet UIView *viewItemDetails;
@property (weak, nonatomic) IBOutlet UIView *viewItemDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemFeeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemFee;
@property (weak, nonatomic) IBOutlet UILabel *lblItemStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackagePic;

//Sender
@property (weak, nonatomic) IBOutlet UIView *viewAngelerDetails;
@property (weak, nonatomic) IBOutlet UIView *viewAngelerDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblAngelerEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnAngelerPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgAngelerImage;

//PickUp Details
@property (weak, nonatomic) IBOutlet UIView *viewPickUpDetails;
@property (weak, nonatomic) IBOutlet UIView *viewPickUpDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUprDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpPersonEmail;

//DropOff Details
@property (weak, nonatomic) IBOutlet UIView *viewDropOffDetails;
@property (weak, nonatomic) IBOutlet UIView *viewDropOffDetailsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffrDetailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffPersonEmail;


@property (nonatomic, strong) NSMutableArray *arrAngelerPhoto;
@property (nonatomic, strong) NSMutableArray *arrPhotos;


@end

@implementation SenderRequestDetailsVC

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetUp];
    [self callApiToFetchPickupDeliveryDetailsInfo];

    // Do any additional setup after loading the view from its nib.
}


#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

#pragma mark - General Functions

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
    
}


#pragma mark - General functions

-(void)initialSetUp
{
    [self setLocalization];
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    [_topNavbarView showNavbackBtn];
    
    [self.topNavbarView setTitle:[NSLocalizedString(@"Local_Delivery_Request_Details", nil) capitalizedString]];
    
    //Tracking Status
    
    [self.viewTrackingStatus makeCornerRound:15.0];
    [self.viewTrackingStatus dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //Item Details
    
    [self.viewItemDetails makeCornerRound:15.0];
    [self.viewItemDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    
    //Sender Details
    
    [self.viewAngelerDetails makeCornerRound:15.0];
    [self.viewAngelerDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //PickUp Details
    
    [self.viewPickUpDetails makeCornerRound:15.0];
    [self.viewPickUpDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
    //DropOff Details
    
    [self.viewDropOffDetails makeCornerRound:15.0];
    [self.viewDropOffDetails dropShadowWithColor:[UIColor lightishGrayColor]];
    
  

}



-(void)roundAllSpecificCorner
{
     [self.viewTrackingStatusHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewItemDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
    [self.viewAngelerDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewPickUpDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
     [self.viewDropOffDetailsHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)setLocalization
{
  
    //Tracking Status
    _lblTrackingStatusTitle.text = NSLocalizedString(@"Tracking_Status", nil);
    
    _lblPickUprDetailsTitle.text = NSLocalizedString(@"Pickup_Details",nil);
    _lblDropOffrDetailsTitle.text = NSLocalizedString(@"DropOff_Details",nil);
    
    //Item Details View
    _lblItemDetailsHeaderTitle.text = NSLocalizedString(@"Item_Details", nil);
    _lblItemDetailsTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Details",nil)];
    _lblItemNameTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Name",nil)];
    _lblItemDeliveryPriceTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Delivery_Price",nil)];
    _lblItemFeeTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Fee",nil)];
    _lblItemStatusTitle.text = [NSString stringWithFormat:@"%@ :",NSLocalizedString(@"Status",nil)];
    
    //Sender Details
    
    _lblAngelerDetailsTitle.text = NSLocalizedString(@"Angeler_Details", nil);
    
}

#pragma mark - API Call

- (void)callApiToFetchPickupDeliveryDetailsInfo{
    
    NSDictionary *param = @{@"deliveryId":_deliveryID,
                            @"caller" : @"delivery_request_details"
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliveryDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           data = [responseObject objectForKey:@"responseData"];
                                                           [self updateValueWithData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}



-(void)updateValueWithData{
    //Track Info
    
    
    { //track
        NSArray *arrTrackInfo = [data objectForKey:@"trackingDetails"];
        
        if (arrTrackInfo.count == 0) {[GlobalInfo hideViewNamed:_trackViewFirst.superview withTopName:Empty]; return;}
        
        BOOL FistIsNotLast = NO;
        
        if (arrTrackInfo.count == 1) {
            _trackHeightConstraint.constant = 0;
            _trackViewlast.hidden = YES;
            
        }else if(arrTrackInfo.count > 2){
            FistIsNotLast = YES;
            
            for (int i = 1; i < arrTrackInfo.count-1; i++) {
                CGRect fromImg = [_trackViewFirst viewWithTag:10].frame;
                CGRect fromLbl = [_trackViewFirst viewWithTag:20].frame;
                
                UIImageView *newImgV = [[UIImageView alloc] init];
                UILabel *lblNew = [[UILabel alloc] init];
                lblNew.font = [UIFont fontWithName:@"Roboto-Regular" size:15.0];
                //lblNew.backgroundColor = [UIColor yellowColor];
                
                newImgV.image = [[[arrTrackInfo objectAtIndex:i] objectForKey:@"status"] isEqualToString:@""]?[UIImage imageNamed:@"package_tracking_gray"]:[UIImage imageNamed:@"package_tracking_Blue"];
                lblNew.text = [[arrTrackInfo objectAtIndex:i] objectForKey:@"text"];
                
                
                fromImg.origin.y = fromImg.origin.y + 15 + (40*i);
                fromLbl.origin.y = fromLbl.origin.y + 15 + (40*i);
                
                fromImg.origin.x = fromImg.origin.x + 20;
                fromLbl.origin.x = fromLbl.origin.x + 20;
                
                newImgV.frame = fromImg;
                lblNew.frame = fromLbl;
                
                [_viewTrakingStep addSubview:newImgV];
                [_viewTrakingStep addSubview:lblNew];
                _trackHeightConstraint.constant = _trackHeightConstraint.constant + 25;
            }
            
        }else{
            FistIsNotLast = YES;
        }
        {// for first step
            UIImageView *imgV = [_trackViewFirst viewWithTag:10];
            UILabel *lblStep = [_trackViewFirst viewWithTag:20];
            imgV.image = [[[arrTrackInfo firstObject] objectForKey:@"status"] isEqualToString:@""]?[UIImage imageNamed:@"package_tracking_gray"]:[UIImage imageNamed:@"package_tracking_Blue"];
            lblStep.text = [[arrTrackInfo firstObject] objectForKey:@"text"];
        }
        if (FistIsNotLast) { // for last step
            UIImageView *imgV = [_trackViewlast viewWithTag:10];
            UILabel *lblStep = [_trackViewlast viewWithTag:20];
            imgV.image = [[[arrTrackInfo lastObject] objectForKey:@"status"] isEqualToString:@""]?[UIImage imageNamed:@"package_tracking_gray"]:[UIImage imageNamed:@"package_tracking_Blue"];
            lblStep.text = [[arrTrackInfo lastObject] objectForKey:@"text"];
        }
    }
   
    //Angeler Details
    
    if ([data objectForKey:@"angelerProfilepic"] == nil || [[data objectForKey:@"angelerProfilepic"] isEqualToString:@""])
    {
        _imgAngelerImage.userInteractionEnabled = NO;
    }
    else
    {
        _imgAngelerImage.userInteractionEnabled = YES;
        self.arrAngelerPhoto = [NSMutableArray array];
        [_imgAngelerImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnAngelerImage:)]];
        
        MWPhoto *angelerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]]];
        angelerPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Angeler", nil),[data objectForKey:@"angelerName"]];
        [self.arrAngelerPhoto addObject:angelerPhoto];
        [_imgAngelerImage sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
    }
    
    _lblAngelerName.text = [data objectForKey:@"angelerName"];
    _lblAngelerEmail.text = [data objectForKey:@"angelerEmail"];
    _lblAngelerPhoneNumber.text = [data objectForKey:@"angelerPhoneNo"];
    
    
    //Item Details
    _lblItemName.text = [data objectForKey:@"itemName"];
    _lblItemDetails.text = [data objectForKey:@"itemDetails"];
    _lblItemDeliveryPrice.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"deliveryPrice"],NSLocalizedString(@"Credits", nil)];
    _lblItemFee.text = [NSString stringWithFormat:@"%@ %@",[data objectForKey:@"fee"],NSLocalizedString(@"Credits", nil)];
    _lblItemStatus.text = [data objectForKey:@"statusText"];
    
    //Picture of item
    {
        
        NSArray *imges = [data objectForKey:@"itemImages"];
        if (imges.count == 0) {
            //  [GlobalInfo hideViewNamed:_pictureView withTopName:@"pictureTop"];
        }else{
            self.arrPhotos = [NSMutableArray array];
            for (int i = 1; i <= imges.count ; i++) {
                UIImageView *imgV = [[_imgPackagePic superview] viewWithTag:i];
                imgV.hidden = NO;
                imgV.userInteractionEnabled = YES;
                [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
                NSLog(@"image url =====  %@",[imges objectAtIndex:i-1] );
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"] ]placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
                MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]]];
                photo.caption = @"";
                [self.arrPhotos addObject:photo];
                
            }
        }
    }
    
    //PickUp Person Details
    
    _lblPickUpPersonName.text = [data objectForKey:@"pickupName"];
    _lblPickUpPersonEmail.text = [data objectForKey:@"pickupEmail"];
    _lblPickUpPersonAddress.text = [data objectForKey:@"pickupAddress"];
    
    //DropOff Person Details
    
    _lblDropOffPersonName.text = [data objectForKey:@"dropoffName"];
    _lblDropOffPersonEmail.text = [data objectForKey:@"dropoffEmail"];
    _lblDropOffPersonAddress.text = [data objectForKey:@"dropoffAddress"];
    
    
}

#pragma mark - Gesture Action

- (void)tapOnAngelerImage:(UITapGestureRecognizer*)tapGest
{
    isAngelerImgTap = true;
    isPictureImgTap = false;
    
    [self setMWPhotoBrowserWithTag:0];
}


- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    
    isAngelerImgTap = false;
    isPictureImgTap = true;
    [self setMWPhotoBrowserWithTag:[tapGest view].tag-1];
    
}

-(void)setMWPhotoBrowserWithTag:(int)tag
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:tag];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isAngelerImgTap)
    {
        return self.arrAngelerPhoto.count;
    }
    
    return self.arrPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (isAngelerImgTap)
    {
        if (index < self.arrAngelerPhoto.count) {
            return [self.arrAngelerPhoto objectAtIndex:index];
        }
    }
    
    else
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    return nil;
}


- (IBAction)callAngeler:(id)sender {
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[data objectForKey:@"angelerPhoneNo"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    
}

@end
