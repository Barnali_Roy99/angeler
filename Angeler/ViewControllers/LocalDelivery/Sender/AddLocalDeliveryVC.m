//
//  AddLocalDeliveryVC.m
//  Angeler
//
//  Created by AJAY on 24/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//


#import "AddLocalDeliveryVC.h"
#import "SingleLocationMapController.h"

#define MaxItemImageCount 3

@interface AddLocalDeliveryVC ()<UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SingleLocationAddressDelegate>
{
    NSNumber *pickUpLat,*PickUpLong,*dropOffLat,*dropOffLong;
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

//PickUp Address

@property (weak, nonatomic) IBOutlet UIView *viewPickUpAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpAddressTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblPickUpAddress;

//PickUp Name

@property (weak, nonatomic) IBOutlet UIView *viewPickUpName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpNameTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtPickUpName;


//PickUp Email

@property (weak, nonatomic) IBOutlet UIView *viewPickUpEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpEmailTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtPickUpEmail;


//DropOff Address

@property (weak, nonatomic) IBOutlet UIView *viewDropOffAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffAddressTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDropOffAddress;

//DropOff Name

@property (weak, nonatomic) IBOutlet UIView *viewDropOffName;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffNameTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtDropOffName;


//DropOff Email

@property (weak, nonatomic) IBOutlet UIView *viewDropOffEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffEmailTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtDropOffEmail;


//Item Name

@property (weak, nonatomic) IBOutlet UIView *viewItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemNameTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtItemName;


//Item Description

@property (weak, nonatomic) IBOutlet UIView *viewItemDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDescTitle;
@property (nonatomic, weak) IBOutlet UITextView *txtItemDesc;


//Item Price

@property (weak, nonatomic) IBOutlet UIView *viewItemDeliveryPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPriceTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtItemDeliveryPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNote;


//Picture of item


@property (weak, nonatomic) IBOutlet UILabel *itemPictureTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *itemPicOneView;
@property (weak, nonatomic) IBOutlet UIView *itemPicSecondView;
@property (weak, nonatomic) IBOutlet UIView *itemPicThirdView;

@property (weak, nonatomic) IBOutlet UIImageView *itemPicOneImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicSecondImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicThirdImgView;

@property (weak, nonatomic) IBOutlet UIButton *itemPicOneCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicSecondCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicThirdCancelButton;
@property (weak, nonatomic) IBOutlet UILabel *lblANCTitle;

@property (nonatomic, strong) NSMutableArray *arrMediaObjects;

//Add shop Details Terms

@property (weak, nonatomic) IBOutlet UISwitch *termsConditionSwitch;
@property (nonatomic, weak) IBOutlet UILabel *termsConditionsTextLabel;

@property (weak, nonatomic) IBOutlet UIView *addView;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd;



@end

@implementation AddLocalDeliveryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _arrMediaObjects = [[NSMutableArray alloc] init];
    [self initialSetUp];
   if(_isEditMode)
       [self callApiToFetchDetailsInfo];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}


#pragma mark - General Functions

-(void)initialSetUp
{
    
    [self setGesture];
    
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    if(_isEditMode)
        [_topNavbarView setTitle:NSLocalizedString(@"Edit_Local_Delivery", nil)];
    else
        [_topNavbarView setTitle:NSLocalizedString(@"Add_Local_Delivery", nil)];
    [_topNavbarView dropShadowWithColor:[UIColor lightGrayColor]];
    
    //Picture Item set Up
    
    _itemPicOneCancelButton.hidden = YES;
    _itemPicSecondCancelButton.hidden = YES;
    _itemPicThirdCancelButton.hidden = YES;
    
    pickUpLat = [NSNumber numberWithFloat:0.0];
    PickUpLong = [NSNumber numberWithFloat:0.0];
    dropOffLat = [NSNumber numberWithFloat:0.0];
    dropOffLong = [NSNumber numberWithFloat:0.0];
    
    [self viewSetUp];
    [self setLocalization];
    
}

-(void)viewSetUp{
    
    [_mainContentView makeCornerRound:15.0];
    [_viewPickUpAddress makeCornerRound:15.0];
    [_viewPickUpName makeCornerRound:15.0];
    [_viewPickUpEmail makeCornerRound:15.0];
    [_viewDropOffAddress makeCornerRound:15.0];
    [_viewDropOffName makeCornerRound:15.0];
    [_viewDropOffEmail makeCornerRound:15.0];
    [_viewItemName makeCornerRound:15.0];
    [_viewItemDesc makeCornerRound:15.0];
    [_viewItemDeliveryPrice makeCornerRound:15.0];
    [_addView makeCornerRound:15.0];
    
    _termsConditionSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
    [_termsConditionSwitch setOn:NO];
    
}


-(void)setLocalization{
    
    _lblPickUpAddressTitle.text = NSLocalizedString(@"Pickup_Address", nil);
    _lblPickUpNameTitle.text = NSLocalizedString(@"Pickup_Name", nil);
    _lblPickUpEmailTitle.text = NSLocalizedString(@"Pickup_Email", nil);
    _lblDropOffAddressTitle.text = NSLocalizedString(@"DropOff_Address", nil);
    _lblDropOffNameTitle.text = NSLocalizedString(@"DropOff_Name", nil);
    _lblDropOffEmailTitle.text = NSLocalizedString(@"DropOff_Email", nil);
    _lblItemNameTitle.text = NSLocalizedString(@"Item_Name", nil);
    _lblItemDescTitle.text = NSLocalizedString(@"Details", nil);
    _lblItemDeliveryPriceTitle .text = NSLocalizedString(@"Delivery_Price", nil);
    _itemPictureTitleLabel.text = NSLocalizedString(@"Picture_Of_Item", nil);
    _lblANCTitle.text = NSLocalizedString(@"Credits", nil);
    _lblPriceNote.text = NSLocalizedString(@"Price_Fee_Msg", nil);
    if(_isEditMode)
        _lblAdd.text = [NSLocalizedString(@"Update", nil) uppercaseString];
    else
         _lblAdd.text = [NSLocalizedString(@"Add", nil) uppercaseString];
   
}

-(void)setGesture
{
    
    UITapGestureRecognizer *pickUpTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickUpLocationViewTapped:)];
    [_viewPickUpAddress addGestureRecognizer:pickUpTapRecognizer];
    
    
    UITapGestureRecognizer *dropOffTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dropOffLocationViewTapped:)];
    [_viewDropOffAddress addGestureRecognizer:dropOffTapRecognizer];
    
    
    UITapGestureRecognizer *addTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addViewTapped:)];
    [_addView addGestureRecognizer:addTapRecognizer];
    
    UITapGestureRecognizer *imageFirstTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicOneImgView addGestureRecognizer:imageFirstTapRecognizer];
    
    UITapGestureRecognizer *imageSecondTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicSecondImgView addGestureRecognizer:imageSecondTapRecognizer];
    
    UITapGestureRecognizer *imageThirdTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicThirdImgView addGestureRecognizer:imageThirdTapRecognizer];
}

- (BOOL)validateItem{
    
    if ([_lblPickUpAddress.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Pickup_Address_Blank_Alert", nil)];
        return NO;
        
    }
    else if ([_txtPickUpName.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Pickup_Name_Blank_Alert", nil)];
        return NO;
        
    }
    else if ([_txtPickUpEmail.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Pickup_Email_Blank_Alert", nil)];
        return NO;
        
    }
    else if (![Helper checkEmailFormat:_txtPickUpEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Pickup_ValidEmail_Alert", nil)];
        return NO;
    }
    if ([_lblDropOffAddress.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"DropOff_Address_Blank_Alert", nil)];
        return NO;
        
    }
    else if ([_txtDropOffName.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"DropOff_Name_Blank_Alert", nil)];
        return NO;
        
    }
    else if ([_txtDropOffEmail.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"DropOff_Email_Blank_Alert", nil)];
        return NO;
        
    }
    else if (![Helper checkEmailFormat:_txtDropOffEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"DropOff_ValidEmail_Alert", nil)];
        return NO;
    }
    else if ([_txtPickUpEmail.text isEqualToString:_txtDropOffEmail.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Pickup_DropOff_Email_NotSame_Alert", nil)];
        return NO;
    }

    else if ([_txtItemName.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Item_Name_Blank_Alert", nil)];
        return NO;
        
    }
    
    else if ([_txtItemDesc.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Item_Details_Blank_Alert", nil)];
        return NO;
        
    }
   else if ([_txtItemDeliveryPrice.text isEqualToString:Empty])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Item_Delivery_Price_Blank_Alert", nil)];
        return NO;
        
    }
    
//    if (_arrMediaObjects.count == 0) {
//        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Add_Shipment_Image_Blank_Alert", nil)];
//        return NO;
//    }
    
   else if (![_termsConditionSwitch isOn])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Validate_Details_Alert", nil)];
        return NO;
    }
    
    return YES;
}


#pragma mark - Gesture Action

- (void)addViewTapped:(UITapGestureRecognizer*)sender
{
    if (![self validateItem]) {
        return;
    }
    [self callApiToAddLocalDeliveryItem];
}

- (void)pickUpLocationViewTapped:(UITapGestureRecognizer*)sender
{
   [self showSingleAddressMapWithTag:PACKAGE_FROM];
}

- (void)dropOffLocationViewTapped:(UITapGestureRecognizer*)sender
{
   [self showSingleAddressMapWithTag:PACKAGE_TO];
}

- (void)showSingleAddressMapWithTag:(TAG_VALUE)tag
{
    SingleLocationMapController *mapObj = [[SingleLocationMapController alloc] initWithNibName:@"SingleLocationMapController" bundle:nil];
    mapObj.delegate = self;
    
    mapObj.tag = tag;
    if (tag == PACKAGE_FROM) {
        
        mapObj.prevlat = pickUpLat;
        mapObj.prevLong = PickUpLong;
    }else{
        
        mapObj.prevlat = dropOffLat;
        mapObj.prevLong = dropOffLong;
    }
    
    [self.navigationController pushViewController:mapObj animated:YES];
}


- (void)captureImageTapped:(UITapGestureRecognizer*)sender
{
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
                imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
                [Helper showCameraGoToSettingsAlert];
            }
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CameraNotAvailable_Alert", nil)];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
            imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"GalleryNotAvailable_Alert", nil)];
        }
    }];
    
    if (_arrMediaObjects.count >= MaxItemImageCount) {
        [GlobalInfo showAlertTitle:Empty Message:@"You cannot upload any more image" actions:[NSArray arrayWithObject:cancel]];
    }else{
        [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Upload_Image_From", nil) actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];
    }
}



#pragma mark - API Call


- (void)callApiToAddLocalDeliveryItem{
    NSString *apiName = kUserAddLocalDeliveryItem;
    if(_isEditMode){
        apiName = kUserEditLocalDeliveryItem;
    }
    
    //Image Json
    
    NSMutableArray *dictImages = [[NSMutableArray alloc] init];
    
    for (ImageObjectClass *imgObj in self.arrMediaObjects) {
        NSDictionary *dict = @{@"action":@"added",
                               @"base64String":[imgObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                               @"imageExt":imgObj.mimeType,
                               @"imageId":Empty,
                               @"imageName":imgObj.keyName,
                               @"imageURL":Empty
                               };
        [dictImages addObject:dict];
        
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictImages
                                                       options:0//NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = Empty;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"json data === %@",jsonString);
    }
    

    
    NSDictionary *param ;
    
    if(_isEditMode){
        param = @{@"pickupAddress":_lblPickUpAddress.text,
                  @"pickupLat":pickUpLat,
                  @"pickupLon":PickUpLong,
                  @"pickupName":_txtPickUpName.text,
                  @"pickupEmail":_txtPickUpEmail.text,
                  @"dropoffAddress":_lblDropOffAddress.text,
                  @"dropoffLat":dropOffLat,
                  @"dropoffLon":dropOffLong,
                  @"dropoffName":_txtDropOffName.text,
                  @"dropoffEmail":_txtDropOffEmail.text,
                  @"itemName"    :_txtItemName.text,
                  @"details" :_txtItemDesc.text,
                  @"price"     :_txtItemDeliveryPrice.text,
                  @"imagesJSON" : jsonString,
                  @"deliveryId" : _deliveryID
                  };
    }
    else{
        param = @{@"pickupAddress":_lblPickUpAddress.text,
                  @"pickupLat":pickUpLat,
                  @"pickupLon":PickUpLong,
                  @"pickupName":_txtPickUpName.text,
                  @"pickupEmail":_txtPickUpEmail.text,
                  @"dropoffAddress":_lblDropOffAddress.text,
                  @"dropoffLat":dropOffLat,
                  @"dropoffLon":dropOffLong,
                  @"dropoffName":_txtDropOffName.text,
                  @"dropoffEmail":_txtDropOffEmail.text,
                  @"itemName"    :_txtItemName.text,
                  @"details" :_txtItemDesc.text,
                  @"price"     :_txtItemDeliveryPrice.text,
                  @"imagesJSON" : jsonString
                  };
    }
    
    //countryId, name, description, keyword, address, url
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:apiName
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                         
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           if (statusCode == 200)
                                                           {
                                                               NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                               
                                                               
                                                               [self showAlertWithMsg:message];
                                                                 
                                                           }
                                                           
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

-(void)showAlertWithMsg:(NSString*)message{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:Empty
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                  [self.navigationController popViewControllerAnimated:true];
                                   
                               }];
    
    
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



- (void)callApiToFetchDetailsInfo{
    
    NSDictionary *param = @{@"deliveryId":_deliveryID,
                            @"caller" : @"delivery_request_details"
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliveryDetails
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSDictionary *data = [responseObject objectForKey:@"responseData"];
                                                           [self updateValueWithData:data];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}


-(void)updateValueWithData:(NSDictionary*)data{
//    pickupAddress":_lblPickUpAddress.text,
//    @"pickupLat":pickUpLat,
//    @"pickupLon":PickUpLong,
//    @"pickupName":_txtPickUpName.text,
//    @"pickupEmail":_txtPickUpEmail.text,
//    @"dropoffAddress":_lblDropOffAddress.text,
//    @"dropoffLat":dropOffLat,
//    @"dropoffLon":dropOffLong,
//    @"dropoffName":_txtDropOffName.text,
//    @"dropoffEmail":_txtDropOffEmail.text,
//    @"itemName"    :_txtItemName.text,
//    @"details" :_txtItemDesc.text,
//    @"price"     :_txtItemDeliveryPrice.text,
//    @"imagesJSON" : jsonString
    
    
    _txtPickUpName.text = [data objectForKey:@"pickupNameNormalCase"];
    _txtPickUpEmail.text = [data objectForKey:@"pickupEmail"];
    _lblPickUpAddress.text = [data objectForKey:@"pickupAddress"];
    
    _txtDropOffName.text = [data objectForKey:@"dropoffNameNormalCase"];
    _txtDropOffEmail.text = [data objectForKey:@"dropoffEmail"];
    _lblDropOffAddress.text = [data objectForKey:@"dropoffAddress"];
    
    _txtItemName.text = [data objectForKey:@"itemName"];
    _txtItemDesc.text = [data objectForKey:@"itemDetails"];
    _txtItemDeliveryPrice.text = [data objectForKey:@"deliveryPrice"];
    
    pickUpLat = @([data[@"pickupLat"] doubleValue]);
    PickUpLong = @([data[@"pickupLon"] doubleValue]);
    
    dropOffLat = @([data[@"dropoffLat"] doubleValue]);
    dropOffLong = @([data[@"dropoffLon"] doubleValue]);
    
    
    NSArray *imges = [data objectForKey:@"itemImages"];
    if (imges.count == 0) {
        //  [GlobalInfo hideViewNamed:_pictureView withTopName:@"pictureTop"];
    }else{
        for (int i = 0; i < imges.count ; i++) {
            if([[[imges objectAtIndex:i] objectForKey:@"imageId"] isEqualToString:Empty]){
                break;
            }
            else{
            ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[imges objectAtIndex:i] objectForKey:@"image"] ]];
            UIImage * image = [UIImage imageWithData:data];
            
            NSString *imageExtType = [[imges objectAtIndex:i] objectForKey:@"imageExt"];
            imageObj.mediaType = @"image";
            imageObj.mimeType = imageExtType;
            if([imageExtType isEqualToString:@"png"]){
                
                imageObj.imgData = UIImagePNGRepresentation(image);
            }
            else{
               
                imageObj.imgData = [Helper compressImage:image];
                
            }
            [_arrMediaObjects addObject:imageObj];
        }
        }
        
        if(_arrMediaObjects.count > 0)
        [self setImagesToItem];
            
        
    }
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == _txtPickUpName || textField == _txtDropOffName || textField == _txtItemName){
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }

    NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if(newLength <= 100)
        {
            return YES;
        }
        else{
            [textField resignFirstResponder];
            if(textField == _txtPickUpName)
                [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Pickup_ContactName_CharacterCount_Alert", nil)];
                else if(textField == _txtDropOffName)
                    [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"DropOff_ContactName_CharacterCount_Alert", nil)];
                    else if(textField == _txtItemName)
                        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"ItemName_CharacterCount_Alert", nil)];
            return NO;
        }
    
    }
    return true;
}

#pragma mark - UITextViewDelegate

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    else{
        if(textView == _txtItemDesc){
            if(range.length + range.location > textView.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textView.text length] + [text length] - range.length;
            if(newLength <= 500)
            {
                return YES;
            }
            else{
                 [textView resignFirstResponder];
                [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"ItemDetails_CharacterCount_Alert", nil)];
                return NO;
            }
        }
    }
    return YES;
}


#pragma mark --- Package Image upload/cancel

- (IBAction)cancelImageTapped:(UIButton*)sender{
    
    [_arrMediaObjects removeObjectAtIndex:sender.tag-1];
    [self setImagesToItem];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = info[UIImagePickerControllerEditedImage];
    ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
    
    
    //=========get image type and compress========
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
    // NSString *strImageType = Empty;
    
    if (UTTypeConformsTo(imageUTI, kUTTypeJPEG))
    {
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
        CFRelease(imageUTI);
    }
    else if (UTTypeConformsTo(imageUTI, kUTTypePNG))
    {
        // Handle PNG
        imageObj.mimeType = @"png";
        imageObj.imgData = UIImagePNGRepresentation(image);
        CFRelease(imageUTI);
    }
    else
    {
        NSLog(@"Unhandled Image UTI: %@", imageUTI);
        
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
    }
    
    //_imageData = [_imageData base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //CFRelease(imageUTI);
    //===============================
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //        [mediaData setObject:_imageData forKey:@"MediaData"];
        //        [mediaData setObject:@"File" forKey:@"TagName"];
        //        [imageObj setObject:@"image" forKey:@"MediaType"];
        imageObj.mediaType = @"image";
        [_arrMediaObjects addObject:imageObj];
        
        [self setImagesToItem];
    }];
}
- (void)setImagesToItem
{
    _itemPicOneImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    _itemPicSecondImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    _itemPicThirdImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    
    _itemPicOneCancelButton.hidden = YES;
    _itemPicSecondCancelButton.hidden = YES;
    _itemPicThirdCancelButton.hidden = YES;
    
    
    int counter = 1;
    for (ImageObjectClass *img in _arrMediaObjects) {
        if (counter == 1) {
            _itemPicOneImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicOneCancelButton.hidden = NO;
        }
        if (counter == 2) {
            _itemPicSecondImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicSecondCancelButton.hidden = NO;
        }
        if (counter == 3) {
            _itemPicThirdImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicThirdCancelButton.hidden = NO;
        }
        
        counter++;
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}


#pragma mark-SingleAddressDelegate

- (void)selectedAddress:(AddressClass*)address forTag:(TAG_VALUE)tag
{
    if (tag == PACKAGE_FROM) {
        _lblPickUpAddress.text = address.Address;
        pickUpLat = address.lat_Value;
        PickUpLong = address.long_Value;
    }
    if (tag == PACKAGE_TO) {
        
        _lblDropOffAddress.text = address.Address;
        dropOffLat = address.lat_Value;
        dropOffLong = address.long_Value;
    }
    
}


@end
