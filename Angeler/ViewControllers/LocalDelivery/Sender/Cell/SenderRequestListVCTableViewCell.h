//
//  SenderRequestListVCTableViewCell.h
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SenderRequestListVCTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *btnShowStatus;
@property (weak, nonatomic) IBOutlet RoundImageView *ItemImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblPickUpLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblDropOffLocation;
@property (weak, nonatomic) IBOutlet RoundImageView *angelerProfileImageview;
@property (weak, nonatomic) IBOutlet UIImageView *editRequestImageview;
@property (weak, nonatomic) IBOutlet UILabel *lblItemDeliveryPrice;
@property (weak, nonatomic) IBOutlet UIView *menu_Container;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@end

NS_ASSUME_NONNULL_END
