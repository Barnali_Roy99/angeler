//
//  SenderRequestListVCTableViewCell.m
//  Angeler
//
//  Created by AJAY on 11/03/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "SenderRequestListVCTableViewCell.h"

@implementation SenderRequestListVCTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.containerView makeCornerRound:15.0];
    [self.containerView dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnShowStatus makeCornerRound:15.0];
    self.ItemImageView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.ItemImageView.layer.borderWidth = 1.0;
    self.angelerProfileImageview.layer.borderColor = [[UIColor blackColor] CGColor];
    self.angelerProfileImageview.layer.borderWidth = 1.0;
    
    [Helper addBorderToLayer:_btnEdit.layer];
    [Helper addBorderToLayer:_btnCancel.layer];
   
    
    [self.menu_Container dropShadowWithColor:[UIColor lightishGrayColor]];
    [self.btnEdit setTitle:[NSString stringWithFormat:@"    %@",
                              NSLocalizedString(@"Edit", nil)]  forState: UIControlStateNormal];
    [self.btnCancel setTitle:[NSString stringWithFormat:@"    %@",
                                   NSLocalizedString(@"Cancel", nil)]  forState: UIControlStateNormal];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
