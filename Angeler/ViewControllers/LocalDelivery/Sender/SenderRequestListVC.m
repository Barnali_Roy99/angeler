//
//  SenderRequestListVC.m
//  Angeler
//
//  Created by AJAY on 28/04/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "SenderRequestListVC.h"
#import "SenderRequestListVCTableViewCell.h"
#import "SenderRequestDetailsVC.h"
#import "MWPhotoBrowser.h"
#import "AddLocalDeliveryVC.h"
#import "AngelerAcceptanveListVC.h"

static NSString *CellIdentifier = @"SenderRequestListVCTableViewCell";


@interface SenderRequestListVC ()<MWPhotoBrowserDelegate>
{
    NSInteger START_POSITION;
    BOOL moreRecordsAvailable;
    BOOL isItemImgTap,isAngelerImgTap;
    
}

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tblSenderReqList;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, strong) NSMutableArray *arrSenderReqList;
@property (nonatomic, strong) NSMutableArray *arrPhotos;
@property (nonatomic, strong) NSMutableArray *arrItemAngelerImage;


@end

@implementation SenderRequestListVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initialSetUp];
    
    //Delete/Info hide
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self.tblSenderReqList addGestureRecognizer:gr];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
     [self initialLoadList];
}

#pragma mark - Gesture Action

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    
    [self hideMenuContainerView];
}

-(void)hideMenuContainerView{
    for (int row = 0; row < _arrSenderReqList.count ; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        SenderRequestListVCTableViewCell *cell = (SenderRequestListVCTableViewCell *)[self.tblSenderReqList cellForRowAtIndexPath:indexPath];
        cell.menu_Container.hidden = YES;
        
    }
}


#pragma mark - Refresh Action

- (void)handleRefresh:(id)refreshController{
     [self initialLoadList];
    [refreshController endRefreshing];
}


#pragma mark- General Functions

-(void)initialLoadList{
    START_POSITION = 0;
    moreRecordsAvailable = YES;
    [self callServiceToLoadSenderReqList];
}

-(void)initialSetUp
{
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
        
    }
    
    // [_topNavbarView showNavbackBtn];
    [_topNavbarView showNavMenuBtn];
     [_topNavbarView showPlusBtn];
    
    [self.topNavbarView setTitle:[NSLocalizedString(@"Local_Delivery_Requests", nil) capitalizedString]];
    self.lblNoRecord.text = NSLocalizedString(@"No_Data_Found", nil);
    [_tblSenderReqList registerNib:[UINib nibWithNibName:@"SenderRequestListVCTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentifier];
    _tblSenderReqList.estimatedRowHeight = 20;
    _tblSenderReqList.rowHeight = UITableViewAutomaticDimension;
    
    UIRefreshControl *refreshController = [[UIRefreshControl alloc] init];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tblSenderReqList addSubview:refreshController];
    
}

#pragma mark - Webservice Call Method

- (void)callServiceToLoadSenderReqList{
    
    NSDictionary *param = @{@"start":[NSString stringWithFormat:@"%ld",START_POSITION],
                            @"offset":[NSString stringWithFormat:@"%d",OFFSETVAL],
                            @"caller" : @"delivery_requests"
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliveryList  //Put it here while service will be ready
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                       if ((arr.count == 0) && (START_POSITION != 0)){
                                                           moreRecordsAvailable = NO;
                                                           return ;
                                                       }
                                                       if ([GlobalInfo checkStatusSuccess:responseObject])  {
                                                           
                                                           if (_arrSenderReqList.count > 0) {
                                                               if (START_POSITION == 0) {
                                                                   [_arrSenderReqList removeAllObjects];
                                                               }else if(_arrSenderReqList.count > START_POSITION){
                                                                   [_arrSenderReqList removeObjectsInRange:NSMakeRange(START_POSITION, _arrSenderReqList.count - START_POSITION)];
                                                               }
                                                               [_arrSenderReqList addObjectsFromArray:arr];
                                                           }else{
                                                               _arrSenderReqList = [NSMutableArray arrayWithArray:arr];
                                                           }
                                                           START_POSITION = START_POSITION + [[responseObject objectForKey:@"responseData"] count];
                                                           _lblNoRecord.hidden = _arrSenderReqList.count > 0;
                                                           NSInteger statusCode = [[[responseObject objectForKey:@"responseStatus"] objectForKey:@"STATUSCODE"] integerValue];
                                                           
                                                           if (statusCode == 601)
                                                           {
                                                               _lblNoRecord.text = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           }
                                                           [_tblSenderReqList reloadData];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}
//

- (void)callServiceToCancelDeliveryRequest:(NSString*)deliveryID{
    
    NSDictionary *param = @{@"deliveryId":deliveryID,
                            
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserCancelLocalDeliveryReqest
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                           
                                                           [self initialLoadList];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}

- (void)callServiceToSendReminderDeliveryRequest:(NSString*)deliveryID{
    
    NSDictionary *param = @{@"deliveryId":deliveryID,
                            
                            };
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserLocalDeliverySendReminderReq
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSString *message = [[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"];
                                                           [Helper showAlertWithTitle:Empty Message:message];
                                                           
                                                           [self initialLoadList];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}


#pragma mark - Scrollview Delegate


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //    NSLog(@"currentOffset %f",currentOffset);
    NSLog(@"maximumOffset currentOffset %f %f",maximumOffset,currentOffset);
    
    //NSInteger result = maximumOffset - currentOffset;
    
    CGFloat offsetDifference = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (offsetDifference <= 100.0) {
        if(moreRecordsAvailable)
        {
            NSLog(@"API Called");
            [self callServiceToLoadSenderReqList];
        }
    }
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrSenderReqList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SenderRequestListVCTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITapGestureRecognizer *itemImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemImageViewTapped:)];
    itemImageViewTap.numberOfTapsRequired = 1;
    [cell.ItemImageView addGestureRecognizer:itemImageViewTap];
    cell.ItemImageView.tag = indexPath.row + 1;
    [cell.ItemImageView setUserInteractionEnabled:YES];
    
    
    UITapGestureRecognizer *itemSenderImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemSenderImageViewTapped:)];
    itemSenderImageViewTap.numberOfTapsRequired = 1;
    [cell.angelerProfileImageview addGestureRecognizer:itemSenderImageViewTap];
    cell.angelerProfileImageview.tag = indexPath.row + 1;
    [cell.angelerProfileImageview setUserInteractionEnabled:YES];
    
    
//    UITapGestureRecognizer *itemEditReqImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemEditReqViewTapped:)];
//    itemEditReqImageViewTap.numberOfTapsRequired = 1;
//    [cell.editRequestImageview addGestureRecognizer:itemEditReqImageViewTap];
//    cell.editRequestImageview.tag = indexPath.row + 1;
//    [cell.editRequestImageview setUserInteractionEnabled:YES];
    cell.editRequestImageview.hidden = YES ; //Old Design Edit button was there and then chjanged to 3 dots list
    
    cell.btnShowStatus.tag = indexPath.row + 1;
    [cell.btnShowStatus addTarget:self action:@selector(cellActionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *data = [_arrSenderReqList objectAtIndex:indexPath.row];
    
    [cell.ItemImageView sd_setImageWithURL:[NSURL URLWithString:[[data objectForKey:@"itemImages"] objectAtIndex:0]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    [cell.angelerProfileImageview sd_setImageWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]] placeholderImage:[UIImage imageNamed:@"choose_angeler_user"]];
    cell.lblItemName.text = [data objectForKey:@"itemName"];
    
    cell.lblPickUpLocation.text = [data objectForKey:@"pickupAddress"];
    cell.lblDropOffLocation.text = [data objectForKey:@"dropoffAddress"];
    cell.lblItemDeliveryPrice.text = [NSString stringWithFormat:@"%@ ANC",[data objectForKey:@"netPrice"]];
    
    [cell.btnShowStatus setTitle:[data objectForKey:@"buttonText"] forState:UIControlStateNormal];
    cell.btnMenu.tag = indexPath.row + 1;
    [cell.btnMenu addTarget:self action:@selector(menuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnEdit.tag = indexPath.row+1;
    [cell.btnEdit addTarget:self action:@selector(editBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnCancel.tag = indexPath.row+1;
    [cell.btnCancel addTarget:self action:@selector(cancelBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.menu_Container.hidden = YES;
    
    if( [[data objectForKey:@"status"] isEqualToString:LocalDeliveryStatus_Pending] || [[data objectForKey:@"status"] isEqualToString:LocalDeliveryStatus_CustomerCanceled] ){
        
        if( [[data objectForKey:@"status"] isEqualToString:LocalDeliveryStatus_CustomerCanceled]){
            cell.btnMenu.hidden = YES;
        }
        else{
           cell.btnMenu.hidden = NO;
        }
        
        cell.angelerProfileImageview.hidden = YES;

    }
    else{
        cell.angelerProfileImageview.hidden = NO;
        cell.btnMenu.hidden = YES;
    }
    
    
    return cell;
}

#pragma mark - Table view Action

-(void)menuClicked:(UIButton*)sender
{
    for (int row = 0; row < _arrSenderReqList.count ; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        SenderRequestListVCTableViewCell *cell = (SenderRequestListVCTableViewCell *)[_tblSenderReqList cellForRowAtIndexPath:indexPath];
        if (row == sender.tag - 1)
        {
            cell.menu_Container.hidden = !cell.menu_Container.hidden;
        }
        else
        {
            cell.menu_Container.hidden = YES;
        }
    }
}


- (void)editBtnClicked:(UIButton*)sender{
   
    NSDictionary *data = [_arrSenderReqList objectAtIndex:sender.tag - 1];
    AddLocalDeliveryVC *Obj = [[AddLocalDeliveryVC alloc] initWithNibName:@"AddLocalDeliveryVC" bundle:nil];
    Obj.deliveryID = [data objectForKey:@"deliveryId"];
    Obj.isEditMode = YES;
    [self.navigationController pushViewController:Obj animated:YES];
     [self hideMenuContainerView];
}

- (void)cancelBtnClicked:(UIButton*)sender{
    NSDictionary *data = [_arrSenderReqList objectAtIndex:sender.tag - 1];
    [self showItemRequestCancelAlertWithDeliveryID:[data objectForKey:@"deliveryId"]];
     [self hideMenuContainerView];
}

- (void)itemImageViewTapped:(UITapGestureRecognizer*)sender{
    UIView *itemImgViewView = (UIImageView *)sender.view;
    NSDictionary *data = [_arrSenderReqList objectAtIndex:itemImgViewView.tag - 1];
    
    NSArray *itemImages = [data objectForKey:@"itemImages"];
    self.arrPhotos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < itemImages.count; i++) {
        [self.arrPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[itemImages objectAtIndex:i] ]]];
    }
    
    isAngelerImgTap = false;
    isItemImgTap = true;
    
    [self setMWPHOtoBrowser];
    
}

//- (void)itemEditReqViewTapped:(UITapGestureRecognizer*)sender{
//
//    UIView *editReqImgViewView = (UIImageView *)sender.view;
//
//    NSDictionary *data = [_arrSenderReqList objectAtIndex:editReqImgViewView.tag - 1];
//    AddLocalDeliveryVC *Obj = [[AddLocalDeliveryVC alloc] initWithNibName:@"AddLocalDeliveryVC" bundle:nil];
//    Obj.deliveryID = [data objectForKey:@"deliveryId"];
//    Obj.isEditMode = YES;
//    [self.navigationController pushViewController:Obj animated:YES];
//}

- (void)itemSenderImageViewTapped:(UITapGestureRecognizer*)sender{
    
    UIView *buyerProfileImgViewView = (UIImageView *)sender.view;
    
    NSDictionary *data = [_arrSenderReqList objectAtIndex:buyerProfileImgViewView.tag - 1];
    
    
    self.arrItemAngelerImage = [[NSMutableArray alloc] init];
    
    MWPhoto *buyerPhoto = [MWPhoto photoWithURL:[NSURL URLWithString:[data objectForKey:@"angelerProfilepic"]]];
    buyerPhoto.caption =[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Angeler", nil),[data objectForKey:@"angelerName"]];
    
    
    [self.arrItemAngelerImage addObject:buyerPhoto];
    
    
    isAngelerImgTap = true;
    isItemImgTap = false;
    
    [self setMWPHOtoBrowser];
    
}

- (void)cellActionButtonTapped:(UIButton*)sender{
    NSDictionary *data = [_arrSenderReqList objectAtIndex:sender.tag - 1];
    if( [[data objectForKey:@"status"] isEqualToString:LocalDeliveryStatus_Pending]){
        [self showSendReminderReqAlertWithDeliveryID:[data objectForKey:@"deliveryId"]];
    }
    else if( ![[data objectForKey:@"status"] isEqualToString:LocalDeliveryStatus_CustomerCanceled]){
        SenderRequestDetailsVC *Obj = [[SenderRequestDetailsVC alloc] initWithNibName:@"SenderRequestDetailsVC" bundle:nil];
        Obj.deliveryID = [data objectForKey:@"deliveryId"];
        [self.navigationController pushViewController:Obj animated:YES];
    }
    
    [self hideMenuContainerView];
   
}

-(void)showSendReminderReqAlertWithDeliveryID:(NSString*)deliveryID
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:NSLocalizedString(@"Send_Reminder_Req_Check_Alert", nil)
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   [self callServiceToSendReminderDeliveryRequest:deliveryID];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showItemRequestCancelAlertWithDeliveryID:(NSString*)deliveryID
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                                                                  message:NSLocalizedString(@"ShopRequest_Item_Cancel_Alert", nil)
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   [self callServiceToCancelDeliveryRequest:deliveryID];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if(isItemImgTap)
        return self.arrPhotos.count;
    else if(isAngelerImgTap)
        return self.arrItemAngelerImage.count;
    else
        return 0;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    
    if(isItemImgTap)
    {
        if (index < self.arrPhotos.count) {
            return [self.arrPhotos objectAtIndex:index];
        }
    }
    else if(isAngelerImgTap){
        if (index < self.arrItemAngelerImage.count) {
            return [self.arrItemAngelerImage objectAtIndex:index];
        }
    }
    return nil;
}

-(void)setMWPHOtoBrowser
{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
    
}

@end
