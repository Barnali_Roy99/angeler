//
//  RequestLocalAngelerController.m
//  Angeler
//
//  Created by AJAY on 11/07/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "RequestLocalAngelerController.h"



@interface RequestLocalAngelerController (){
    NSInteger counter;
}
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *rippleView;

@property (nonatomic, strong) NSArray *colors;

@property (weak, nonatomic) IBOutlet UIButton *btnAcceptOkay;
@property (weak, nonatomic) IBOutlet UIButton *btnDeny;

@property (weak, nonatomic) IBOutlet UILabel *lblOrderId;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@end

@implementation RequestLocalAngelerController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // [Helper addShadowToLayer:_headerView.layer];
    ROUNDEDCORNER(_btnAcceptOkay.layer);
    ROUNDEDCORNER(_btnDeny.layer);
    [Helper makeItRound:_mapView.layer withRadius:MainScreenWidth/2-20];
    [self setCost:@"200" AndOrder:@"ANG12345879"];
    [self ConfigureMap];
    
}

- (void)setCost:(NSString*)cost AndOrder:(NSString*)order{
    _lblCost.attributedText = [self formatMessage:[NSString stringWithFormat:@"COST: %@ SGD",cost] atIndex:5];
    _lblOrderId.attributedText = [self formatMessage:[NSString stringWithFormat:@"ORDERID: %@",order] atIndex:8];
}
- (void)ConfigureMap{
    _mapView.delegate = self;
    [_mapView setBackgroundColor:[UIColor clearColor]];
    
    // Map SetUp
    [[AppDel shareModel] startMonitoringLocation];
    NSNumber *lat = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].latitude];
    NSNumber *longt = [NSNumber numberWithDouble:[[AppDel shareModel] myLocation].longitude];
    
    if ([_packObj.from_Address.lat_Value doubleValue] != 0.0 && [_packObj.from_Address.long_Value doubleValue] != 0.0) {
        lat = _packObj.from_Address.lat_Value;
        longt = _packObj.from_Address.long_Value;
    }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[lat doubleValue] longitude:[longt doubleValue] zoom:12];
    _mapView.camera = camera;
    
    [self addMarkerToMapViewAtStartEndPosition:YES];
}

- (void)fireRippleEffect{
    // ripple effect timer
    [self addMarkerToMapViewAtStartEndPosition:NO];
    counter = 0;
    [NSTimer scheduledTimerWithTimeInterval:1.5f repeats:YES block:^(NSTimer * _Nonnull timer) {
        CGFloat radius = 40.0f;
        
        UIView *ripple = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, radius, radius)];
        ripple.layer.cornerRadius = radius * 0.5f;
        ripple.backgroundColor = [Helper colorWithHexString:@"#68C1D4"];
        ripple.alpha = 1.0f;
        [self.view insertSubview:ripple atIndex:100];
        
        ripple.center = [self.rippleView convertPoint:self.rippleView.center fromView:self.rippleView];
        
        CGFloat scale = 8.0f;
        [UIView animateWithDuration:1.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            ripple.transform = CGAffineTransformMakeScale(scale, scale);
            ripple.alpha = 0.0f;
            ripple.backgroundColor = [Helper colorWithHexString:@"#68C1D4"];
        } completion:^(BOOL finished) {
            [ripple removeFromSuperview];
        }];
        
        counter++;
        if (counter >= 100) {
            [timer invalidate];
        }
    }];
    
}

- (IBAction)AcceptDenyOkayTapped:(UIButton*)sender {
    switch (sender.tag) {
        case 10:
            NSLog(@"Accept Tapped");
            [_btnAcceptOkay setTitle:@"Okay" forState:UIControlStateNormal];
            [sender setTag:100];
            [_btnDeny removeFromSuperview];
            [self fireRippleEffect];
            break;
        case 20:
            NSLog(@"Dent Tapped");
            break;
        case 100:
            NSLog(@"Okay Tapped");
            [self backTapped:nil];
            break;
            
        default:
            break;
    }
}

- (void)addMarkerToMapViewAtStartEndPosition:(BOOL)startEnd{
    [_mapView clear];
    
    // Creates a marker in the center of the map.
    GMSMarker *marker1 = [[GMSMarker alloc] init];
    marker1.title = @"My Location";
    marker1.map = _mapView;
    marker1.icon = [UIImage imageNamed:@"LocationPin"];
    marker1.position = CLLocationCoordinate2DMake([_packObj.from_Address.lat_Value doubleValue],[_packObj.from_Address.long_Value doubleValue]);
    
    if (startEnd) {

        GMSMarker *marker2 = [[GMSMarker alloc] init];
        marker2.title = @"My Location";
        marker2.map = _mapView;
        marker2.icon = [UIImage imageNamed:@"LocationPin"];
        marker2.position = CLLocationCoordinate2DMake([_packObj.to_Address.lat_Value doubleValue],[_packObj.to_Address.long_Value doubleValue]);
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        bounds = [bounds includingCoordinate:marker1.position];
        bounds = [bounds includingCoordinate:marker2.position];
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100]];
    }
    else{
        [_mapView animateToCameraPosition:[GMSCameraPosition cameraWithTarget:marker1.position zoom:12]];
    }
    
    
}

- (IBAction)backTapped:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
//    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - GMS Delegate
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    //    double latitude = mapView.camera.target.latitude;
    //    double longitude = mapView.camera.target.longitude;
    //
    //    [self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
    
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    
    //[self callRevGeoCodingWithLatitude:[NSString stringWithFormat:@"%f",latitude] longitude:[NSString stringWithFormat:@"%f",longitude]];
   // [self addMarkerToMapViewAtCameraPosition];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSAttributedString*)formatMessage:(NSString*)message atIndex:(NSInteger)indx{
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:message];
    
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Regular" size:14.0] range:NSMakeRange(0, indx)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:17.0] range:NSMakeRange(indx, str.length-indx)];
    //[str addAttribute:NSForegroundColorAttributeName value:[Helper colorWithHexString:@"#68C1D4"] range:NSMakeRange(index, str.length-index)];
    return str;
}


/*
 
 // macro from https://gist.github.com/uechi/7688152
 //RGB color macro
 #define UIColorFromRGB(rgbValue) [UIColor \
 colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
 
 //RGB color macro with alpha
 #define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
 colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
 
 // Usage
 // UIColorFromRGB(0xCECECE);
 // UIColorFromRGBWithAlpha(0xCECECE, 0.8);
- (UIColor *)randomColor
{
    if (!_colors) {
        _colors = @[
                    UIColorFromRGB(0xff7f7f),
                    UIColorFromRGB(0xff7fbf),
                    UIColorFromRGB(0xff7fff),
                    UIColorFromRGB(0xbf7fff),
                    UIColorFromRGB(0x7f7fff),
                    UIColorFromRGB(0x7fbfff),
                    UIColorFromRGB(0x7fffff),
                    UIColorFromRGB(0x7fffbf),
                    UIColorFromRGB(0x7fff7f),
                    UIColorFromRGB(0xbfff7f),
                    UIColorFromRGB(0xffff7f),
                    UIColorFromRGB(0xffbf7f)
                    ];
    }
    
    NSInteger count = _colors.count;
    NSInteger r = arc4random() % count;
    return _colors[r];
}

- (void)rippleWithView:(UIView *)view colorFrom:(UIColor *)colorFrom colorTo:(UIColor *)colorTo
{
    if (!view) {
        return;
    }
    CGFloat radius = 40.0f;
    UIView *ripple = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, radius, radius)];
    ripple.layer.cornerRadius = radius * 0.5f;
    ripple.backgroundColor = colorFrom;
    ripple.alpha = 1.0f;
    [self.view insertSubview:ripple atIndex:100];
    
    ripple.center = [self.rippleView convertPoint:self.rippleView.center fromView:self.rippleView];

    CGFloat scale = 8.0f;
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        ripple.transform = CGAffineTransformMakeScale(scale, scale);
        ripple.alpha = 0.0f;
        ripple.backgroundColor = colorTo;
    } completion:^(BOOL finished) {
        [ripple removeFromSuperview];
    }];
}*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
