//
//  RequestLocalAngelerController.h
//  Angeler
//
//  Created by AJAY on 11/07/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>


@interface RequestLocalAngelerController : UIViewController<GMSMapViewDelegate>
@property (nonatomic, strong) PackageClass *packObj;
@end
