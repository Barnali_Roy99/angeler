//
//  VerifyAccountViewController.m
//  Angeler
//
//  Created by AJAY on 07/08/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "VerifyAccountViewController.h"
#import "MWPhotoBrowser.h"
#import "SharePopUpViewController.h"

#define MaxItemImageCount 3

@interface VerifyAccountViewController () <UITextFieldDelegate,UIImagePickerControllerDelegate,MWPhotoBrowserDelegate,SharePopUpDismissDelegate,UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (nonatomic, weak) IBOutlet UIImageView *imgProfile;

@property (weak, nonatomic) IBOutlet UIView *viewPhoneNumber;
@property (nonatomic, weak) IBOutlet UITextField *txtUserPhoneNumber;

@property (weak, nonatomic) IBOutlet UIView *addDocSubmitcontainerView;

@property (weak, nonatomic) IBOutlet UILabel *uploadDocTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *itemPicOneView;
@property (weak, nonatomic) IBOutlet UIView *itemPicSecondView;
@property (weak, nonatomic) IBOutlet UIView *itemPicThirdView;

@property (weak, nonatomic) IBOutlet UIImageView *itemPicOneImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicSecondImgView;
@property (weak, nonatomic) IBOutlet UIImageView *itemPicThirdImgView;

@property (weak, nonatomic) IBOutlet UIButton *itemPicOneCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicSecondCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *itemPicThirdCancelButton;
@property (weak, nonatomic) IBOutlet UISwitch *termsConditionSwitch;
@property (nonatomic, weak) IBOutlet UILabel *termsConditionsTextLabel;
@property (weak, nonatomic) IBOutlet UIView *submitView;
@property (nonatomic, strong) NSMutableArray *arrMediaObjects;


@property (weak, nonatomic) IBOutlet UIView *showDocReSubmitcontainerView;
@property (weak, nonatomic) IBOutlet UIView *reSubmitView;
@property (weak, nonatomic) IBOutlet UIView *viewDocumentItem;
@property (weak, nonatomic) IBOutlet UIView *viewDocumentItemHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblDocumentItemTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgDocumentPic;
@property (nonatomic, strong) NSMutableArray *arrDocPhotos;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblResubmit;

@end

@implementation VerifyAccountViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
     [self initialSetUp];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self roundAllSpecificCorner];
}


#pragma mark - General Functions

-(void)initialSetUp
{
    [self setPlaceHolderText];
    [self setGesture];
    [self checkAngelerVeifyStatus];
    
    _arrMediaObjects = [[NSMutableArray alloc] init];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavbackBtn];
    [_topNavbarView setTitle:[NSLocalizedString(@"Verify_Your_Account", nil) capitalizedString]];
    
    [_containerView makeCornerRound:15.0];
    [_viewPhoneNumber makeCornerRound:15.0];
    [_submitView makeCornerRound:15.0];
    [_reSubmitView makeCornerRound:15.0];
    
    
    //Document Id
    
    _itemPicOneCancelButton.hidden = YES;
    _itemPicSecondCancelButton.hidden = YES;
    _itemPicThirdCancelButton.hidden = YES;
    
    //Picture Document
    
    [self.viewDocumentItem makeCornerRound:15.0];
    [self.viewDocumentItem dropShadowWithColor:[UIColor lightishGrayColor]];
    
    _lblDocumentItemTitle.text = NSLocalizedString(@"Documents",nil);
    
    
    //Terms & Conditions
//    NSString *TC_text = NSLocalizedString(@"TermsConditions_Text", nil) ;
//   NSString *TC_underLineText = NSLocalizedString(@"TermsConditions_UnderlineText", nil) ;
//    _termsConditionsTextLabel.attributedText = [Helper getUnderlineText:TC_text :[TC_text rangeOfString:TC_underLineText]  withColor:[UIColor mediumBlueColor]];
    _termsConditionSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
    [_termsConditionSwitch setOn:NO];
    
    //Phone Number Set
    _txtUserPhoneNumber.text = [[GlobalInfo sharedInfo] phoneNo];
    
    //Localiztion
    _uploadDocTitleLabel.text = NSLocalizedString(@"Upload_Your_ID_Document", nil);
    _lblSubmit.text = [NSLocalizedString(@"Submit", nil) uppercaseString] ;
    _lblResubmit.text = [NSLocalizedString(@"Resubmit", nil) uppercaseString] ;
    
    //Profile Image Set
    
    [_imgProfile sd_setImageWithURL:[NSURL URLWithString:[[GlobalInfo sharedInfo] profilePic]] placeholderImage:[UIImage imageNamed:@""]];
    
    
}

//Round specific corner may be in some cases issue found view not loaded properly and got design issue .Thats why specific corner round we have to make after view already appear.

-(void)roundAllSpecificCorner
{
    [self.viewDocumentItemHeader roundSpicficCorner:UIRectCornerTopLeft | UIRectCornerTopRight withRadius:15.0];
}

-(void)checkAngelerVeifyStatus
{
    NSLog(@"Angeler Status %@", [[GlobalInfo sharedInfo] userAngelerStatus]);
    
    if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Pending] || [[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Rejected])
    {
        _addDocSubmitcontainerView.hidden = YES;
        _showDocReSubmitcontainerView.hidden = NO;
        [self callAPIToViewDocument];
    }
    else if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_NotVerified])
    {
        _addDocSubmitcontainerView.hidden = NO;
        _showDocReSubmitcontainerView.hidden = YES;
    }
    else  if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Verified])
    {
        _addDocSubmitcontainerView.hidden = YES;
        _showDocReSubmitcontainerView.hidden = NO;
        _reSubmitView.hidden = YES;
        _txtUserPhoneNumber.enabled = NO;
        [self callAPIToViewDocument];
    }
}

-(void)setGesture
{
    UITapGestureRecognizer *submitTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped:)];
    [_submitView addGestureRecognizer:submitTapRecognizer];
    
    UITapGestureRecognizer *reSubmitTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reSubmitViewTapped:)];
    [_reSubmitView addGestureRecognizer:reSubmitTapRecognizer];
    
    UITapGestureRecognizer *imageFirstTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicOneImgView addGestureRecognizer:imageFirstTapRecognizer];
    
    UITapGestureRecognizer *imageSecondTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicSecondImgView addGestureRecognizer:imageSecondTapRecognizer];
    
    UITapGestureRecognizer *imageThirdTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureImageTapped:)];
    [self.itemPicThirdImgView addGestureRecognizer:imageThirdTapRecognizer];
}

-(void)setPlaceHolderText
{
    //Phone_Number
    self.txtUserPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Phone_Number", nil) attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
}

- (BOOL)validate{

    
    if ([Helper checkEmptyField:_txtUserPhoneNumber.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"PhoneNumberBlank_Alert", nil)];
        return NO;
    }
    
    if (_arrMediaObjects.count == 0) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Select_Document_Alert", nil)];
        return NO;
    }
    
    if (![_termsConditionSwitch isOn])
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"TermsConditions_Alert", nil)];
        return NO;
    }

    return YES;
}

#pragma mark - Gesture Action


- (void)captureImageTapped:(UITapGestureRecognizer*)sender
{
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
                imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
               [Helper showCameraGoToSettingsAlert];
            }
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CameraNotAvailable_Alert", nil)];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
            imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"GalleryNotAvailable_Alert", nil)];
        }
    }];
    
    if (_arrMediaObjects.count >= MaxItemImageCount) {
        [GlobalInfo showAlertTitle:Empty Message:@"You cannot upload any more image" actions:[NSArray arrayWithObject:cancel]];
    }else{
        [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Upload_Image_From", nil) actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];
    }
}

- (void)reSubmitViewTapped:(UITapGestureRecognizer*)sender
{
    _addDocSubmitcontainerView.hidden = NO;
    _showDocReSubmitcontainerView.hidden = YES;
    
}

- (void)submitViewTapped:(UITapGestureRecognizer*)sender
{
    if (![self validate]) {
        return;
    }
    
    //Image Json
    
    NSMutableArray *dictImages = [[NSMutableArray alloc] init];
    
    for (ImageObjectClass *imgObj in self.arrMediaObjects) {
        NSDictionary *dict = @{@"action":@"added",
                               @"base64String":[imgObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                               @"imageExt":imgObj.mimeType,
                               @"imageId":Empty,
                               @"imageName":imgObj.keyName,
                               @"imageURL":Empty
                               };
        [dictImages addObject:dict];
        
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictImages
                                                       options:0//NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = Empty;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"json data === %@",jsonString);
    }
    
    
    NSDictionary *param = @{@"mode":@"Add",
                            @"phoneNo":_txtUserPhoneNumber.text,
                            @"imagesJSON":jsonString
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kVerifyViewAccount
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
        
                                                          [[GlobalInfo sharedInfo] setUserVerificationStatusText:[dataDict objectForKey:@"verifiedStatusText"]];
                                                           [[GlobalInfo sharedInfo] setUserAngelerStatus:[dataDict objectForKey:@"angelerStatus"]];
                                                           [[GlobalInfo sharedInfo] setPhoneNo:_txtUserPhoneNumber.text];
                                                           [self showAlertPopUp];
                                                          
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
    
}

#pragma mark - Webservice Call

-(void)callAPIToViewDocument
{
    NSDictionary *param = @{@"mode":@"View",
                            @"phoneNo":@"",
                            @"imagesJSON":@""
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kVerifyViewAccount
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                          
                                                           [self updateValuesWithData:[responseObject objectForKey:@"responseData"]];
                                                           
                                                           
                                                           
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
}

- (void)updateValuesWithData:(NSDictionary*)data{
    
    [[GlobalInfo sharedInfo] setUserVerificationStatusText:[data objectForKey:@"verifiedStatusText"]];
    [[GlobalInfo sharedInfo] setUserAngelerStatus:[data objectForKey:@"angelerStatus"]];
    [[GlobalInfo sharedInfo] setPhoneNo:[data objectForKey:@"phoneNo"]];
    
    // Document Images
    
    NSArray *imges = [data objectForKey:@"images"];
    if (imges.count == 0) {
        
    }else{
        self.arrDocPhotos = [NSMutableArray array];
        for (int i = 1; i <= imges.count ; i++) {
            UIImageView *imgV = [[_imgDocumentPic superview] viewWithTag:i];
            imgV.hidden = NO;
            imgV.userInteractionEnabled = YES;
            [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImage:)]];
            NSLog(@"image url =====  %@",[[imges objectAtIndex:i-1] objectForKey:@"image"]);
            [imgV sd_setImageWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"package_imageplaceholder"]];
            [self.arrDocPhotos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[imges objectAtIndex:i-1] objectForKey:@"image"]]]];
        }
    }
}

#pragma mark - UITextfieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
    
}


#pragma mark --- Document Image upload/cancel

#pragma mark --- Button Action

/*- (IBAction)uploadDocumentImageTapped:(id)sender {
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
                imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
                [Helper showAlertWithTitle:Empty Message:strKey_CameraPermissionRequired];
            }
            
        } else {
            [Helper showAlertWithTitle:Empty Message:strKey_CameraNotAvailable];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
            imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:strKey_GalleryNotAvailable];
        }
    }];
    
    if (_arrMediaObjects.count >= MaxItemImageCount) {
        [GlobalInfo showAlertTitle:Empty Message:@"You cannot upload any more image" actions:[NSArray arrayWithObject:cancel]];
    }else{
        [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Upload_Image_From", nil) actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];
    }
}*/
- (IBAction)cancelImageTapped:(UIButton*)sender{
    
    [_arrMediaObjects removeObjectAtIndex:sender.tag-1];
    [self setImagesToItem];
}

#pragma mark --- UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = info[UIImagePickerControllerEditedImage];
    ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
    
    //Or you can get the image url from AssetsLibrary
    //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@"Path: %@",path);
    
    //=========get image type and compress========
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
    // NSString *strImageType = Empty;
    
    if (UTTypeConformsTo(imageUTI, kUTTypeJPEG))
    {
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
        CFRelease(imageUTI);
    }
    else if (UTTypeConformsTo(imageUTI, kUTTypePNG))
    {
        // Handle PNG
        imageObj.mimeType = @"png";
        imageObj.imgData = UIImagePNGRepresentation(image);
        CFRelease(imageUTI);
    }
    else
    {
        NSLog(@"Unhandled Image UTI: %@", imageUTI);
        
        // Handle JPG
        imageObj.mimeType = @"jpeg";
        imageObj.imgData = [Helper compressImage:image];
    }
    
    //_imageData = [_imageData base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //CFRelease(imageUTI);
    //===============================
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //        [mediaData setObject:_imageData forKey:@"MediaData"];
        //        [mediaData setObject:@"File" forKey:@"TagName"];
        //        [imageObj setObject:@"image" forKey:@"MediaType"];
        imageObj.mediaType = @"image";
        [_arrMediaObjects addObject:imageObj];
        
        [self setImagesToItem];
    }];
}
- (void)setImagesToItem
{
    _itemPicOneImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    _itemPicSecondImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    _itemPicThirdImgView.image = [UIImage imageNamed:@"package_imageplaceholder"];
    
    _itemPicOneCancelButton.hidden = YES;
    _itemPicSecondCancelButton.hidden = YES;
    _itemPicThirdCancelButton.hidden = YES;
    
    
    int counter = 1;
    for (ImageObjectClass *img in _arrMediaObjects) {
        if (counter == 1) {
            _itemPicOneImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicOneCancelButton.hidden = NO;
        }
        if (counter == 2) {
            _itemPicSecondImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicSecondCancelButton.hidden = NO;
        }
        if (counter == 3) {
           _itemPicThirdImgView.image = [UIImage imageWithData:img.imgData];
            _itemPicThirdCancelButton.hidden = NO;
        }
        
        counter++;
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}


#pragma mark - Gesture Action

- (void)tapOnImage:(UITapGestureRecognizer*)tapGest{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    //    // Customise selection images to change colours if required
    //    browser.customImageSelectedIconName = @"ImageSelected.png";
    //    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:[tapGest view].tag-1];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    //    // Manipulate
    //    [browser showNextPhotoAnimated:YES];
    //    [browser showPreviousPhotoAnimated:YES];
    //    [browser setCurrentPhotoIndex:10];
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.arrDocPhotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.arrDocPhotos.count) {
        return [self.arrDocPhotos objectAtIndex:index];
    }
    return nil;
}
#pragma mark - Share Pop Up show

-(void)showAlertPopUp{
    //Localization
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SharePopUpViewController *sharePopUpVC = [storyboard instantiateViewControllerWithIdentifier:@"SharePopUpVCID"];
    
    sharePopUpVC.delegate = self;
    sharePopUpVC.headerTitleText = NSLocalizedString(@"Thanks", nil);
    sharePopUpVC.headerDescriptionText = NSLocalizedString(@"Account_Verify_Submit_Alert", nil);
    sharePopUpVC.bottomDescriptionText = NSLocalizedString(@"Share_Message_PopUp", nil);
    sharePopUpVC.singleActionLabelTitleText =
    [NSLocalizedString(@"OK", nil) uppercaseString];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    sharePopUpVC.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    
    sharePopUpVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:sharePopUpVC animated:YES completion:nil];
    
}

#pragma mark - SharePopUpDismissDelegate

-(void)dismissPopUp
{
    [self.navigationController popViewControllerAnimated:false];
}




@end
