//
//  ProfileController.h
//  Angeler
//
//  Created by AJAY on 26/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "BaseViewController.h"
#import <CoreServices/CoreServices.h>

@interface ProfileController : BaseViewController
@property (weak, nonatomic) IBOutlet TopNavbarView *topNavbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@end
