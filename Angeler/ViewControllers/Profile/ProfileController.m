//
//  ProfileController.m
//  Angeler
//
//  Created by AJAY on 26/05/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import "ProfileController.h"
#import "VerifyAccountViewController.h"
#import "DriverVerificationVC.h"

@interface ProfileController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
        int selectedCountryIndex;
}


@property (weak, nonatomic) IBOutlet UILabel *lblAngelerId;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *viewAccountVerified;
@property (weak, nonatomic) IBOutlet UIView *viewDriverVerified;
@property (weak, nonatomic) IBOutlet UIView *viewLastName;
@property (weak, nonatomic) IBOutlet UIView *viewFirstName;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIView *viewPhoneNumber;
@property (weak, nonatomic) IBOutlet UIView *viewAddress;
@property (weak, nonatomic) IBOutlet UIView *viewCountry;

@property (nonatomic, weak) IBOutlet UITextField *txtAccountVerified;
@property (nonatomic, weak) IBOutlet UITextField *txtDriverVerified;
@property (nonatomic, weak) IBOutlet UITextField *txtUserLastName;
@property (nonatomic, weak) IBOutlet UITextField *txtUserFirstName;
@property (nonatomic, weak) IBOutlet UITextField *txtUserEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtUserPhoneNumber;
@property (nonatomic, weak) IBOutlet UITextField *txtUserAddress;
@property (nonatomic, weak) IBOutlet UITextField *txtUserCountry;

@property (nonatomic, weak) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UISwitch *switchPushNotify;
@property (nonatomic, weak) IBOutlet UILabel *receivePushNotificationTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *camerPlusIcon;

@property (weak, nonatomic) IBOutlet UIView *submitView;
@property (weak, nonatomic) IBOutlet UIImageView *verifiedTickImageView;
@property (weak, nonatomic) IBOutlet UIImageView *taxiDriververifiedTickImageView;
@property (nonatomic, strong) ImageObjectClass *imageObj;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeProfilePic;
@property (nonatomic, strong) NSMutableArray *arrCountry;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverVerifiedViewheightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverVerifiedViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *driverIconWidthConstraint;

@end

@implementation ProfileController


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Making build without taxi feature , uncomment those below three lines
    
//    _driverVerifiedViewheightConstraint.constant = 0;
//    _driverVerifiedViewTopConstraint.constant = 0;
//    _driverIconWidthConstraint.constant = 0;
    
    selectedCountryIndex = -1;
    [self setCountryPicker];
    [self callApiToGetCountryList];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self initialSetUp];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15.0];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
}

#pragma mark- General Functions

-(void)initialSetUp
{
    [self setPlaceHolderText];
    [self setData];
    [self setGesture];
    
     _imageObj = [[ImageObjectClass alloc] init];
    
    if ([Helper isIphoneX])
    {
        _topBarHeightConstraint.constant = 104;
    }
    else
    {
        _topBarHeightConstraint.constant = 84;
    }
    
    [_topNavbarView showNavMenuBtn];
    [_topNavbarView setTitle:NSLocalizedString(@"Profile", nil)];
    
    
    [_containerView makeCornerRound:15.0];
    
    [_viewAccountVerified makeCornerRound:15.0];
    [_viewDriverVerified makeCornerRound:15.0];
    [_viewLastName makeCornerRound:15.0];
    [_viewFirstName makeCornerRound:15.0];
    [_viewEmail makeCornerRound:15.0];
    [_viewPhoneNumber makeCornerRound:15.0];
    [_viewAddress makeCornerRound:15.0];
    [_viewCountry makeCornerRound:15.0];
    [_submitView makeCornerRound:15.0];
    
     _switchPushNotify.transform = CGAffineTransformMakeScale(0.65, 0.65);
    _receivePushNotificationTextLabel.text = NSLocalizedString(@"Receive_Notification_Alert", nil);
    _lblSubmit.text = [NSLocalizedString(@"Submit", nil) uppercaseString];

    
}


-(void)setPlaceHolderText{
    
    UIColor *color = [UIColor lightGrayColor];
    self.txtUserLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Last_Name", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserFirstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"First_Name", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Phone_Number", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Address", nil) attributes:@{NSForegroundColorAttributeName: color}];
    self.txtUserCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Country", nil) attributes:@{NSForegroundColorAttributeName: color}];
}

-(void)setData
{
    [_imgProfile sd_setImageWithURL:[NSURL URLWithString:[[GlobalInfo sharedInfo] profilePic]] placeholderImage:[UIImage imageNamed:@""]];
    
    NSString *angelerID = [[GlobalInfo sharedInfo] userUniqueAngelerID];
    if ([angelerID isEqualToString:@""])
    {
        _lblAngelerId.text = @"";
    }
    else
    {
        _lblAngelerId.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"ID",nil),angelerID];
    }
    
    
    _txtUserFirstName.text = [[GlobalInfo sharedInfo] userFirstName];
    _txtUserLastName.text = [[GlobalInfo sharedInfo] userLastName];
    _txtUserPhoneNumber.text = [[GlobalInfo sharedInfo] phoneNo];
    _txtUserEmail.text = [[GlobalInfo sharedInfo] userEmail];
   
    _txtUserAddress.text = [[GlobalInfo sharedInfo] userAddress];
    _txtAccountVerified.text = [[GlobalInfo sharedInfo] userVerificationStatusText];
    _txtDriverVerified.text = [[GlobalInfo sharedInfo] taxiDriverVerificationStatusText];
    
    if ([[[GlobalInfo sharedInfo] receiveNotification] integerValue] == 1) {
        _switchPushNotify.on = YES;
    }else{
        _switchPushNotify.on = NO;
    }
    
    if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Verified])
    {
        _verifiedTickImageView.hidden = NO;
        _txtUserEmail.userInteractionEnabled = NO;
        _txtUserPhoneNumber.userInteractionEnabled = NO;
        _txtUserLastName.userInteractionEnabled = NO;
        _txtUserFirstName.userInteractionEnabled = NO;
        _btnChangeProfilePic.userInteractionEnabled = NO;
        _camerPlusIcon.hidden = YES;
    }
    else
    {
        _verifiedTickImageView.hidden = YES;
    }
    
    if([[[[GlobalInfo sharedInfo] taxiDriverVerificationStatus] lowercaseString] isEqualToString:verificationStatus_Verified])
    {
        _taxiDriververifiedTickImageView.hidden = NO;
    }
    else
    {
        _taxiDriververifiedTickImageView.hidden = YES;
    }
}

-(void)setGesture
{
    UITapGestureRecognizer *submitTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped:)];
    [_submitView addGestureRecognizer:submitTapRecognizer];
    
    UITapGestureRecognizer *verifyAccountTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(verifyAccountViewTapped:)];
    [_viewAccountVerified addGestureRecognizer:verifyAccountTapRecognizer];
    
    UITapGestureRecognizer *taxiDriverVerifiedTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(taxiDriververifyViewTapped:)];
    [_viewDriverVerified addGestureRecognizer:taxiDriverVerifiedTapRecognizer];
    
}

-(void)setCountryPicker
{
    //Country List Set Up
    [_txtUserCountry setInputView:[Helper pickerWithTag:COUNTRY_NAME forControler:self]];
    [_txtUserCountry setInputAccessoryView:[Helper toolbarWithTag:COUNTRY_NAME forControler:self]];
}

-(void)setCountryNameUsingID
{
    
    for(int i = 0 ; i<_arrCountry.count ; i++)
    {
        NSDictionary *tempDic = [_arrCountry objectAtIndex:i];
        if ([[tempDic objectForKey:@"country_id"] isEqualToString: [[GlobalInfo sharedInfo] userSelectedCountryID]])
        {
            _txtUserCountry.text = [tempDic objectForKey:@"country_name"];
            selectedCountryIndex = i;
            break;
        }
    }

}

- (BOOL)validate{
    
    if ([Helper checkEmptyField: _txtUserLastName.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"LastNameBlank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtUserFirstName.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"FirstNameBlank_Alert", nil)];
        return NO;
    }
    
    if ([Helper checkEmptyField:_txtUserPhoneNumber.text]) {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"PhoneNumberBlank_Alert", nil)];
        return NO;
    }

    if (selectedCountryIndex == -1)
    {
        [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Select_Country_Alert", nil)];
        return NO;
        
    }
    
    return YES;
}

#pragma mark - API Call

- (void)callApiToGetCountryList{
    
    NSDictionary *param = @{@"caller":@"default"};
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kUserSelectCountryList
                                                    Method:REQMETHOD_GET
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           
                                                           NSArray *arr = [responseObject objectForKey:@"responseData"];
                                                           _arrCountry = [NSMutableArray arrayWithArray:arr];
                                                           
                                                           [self setCountryNameUsingID];
                                                       }
                                                       
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
}

#pragma mark - Picker Call

- (void)pickerDoneTapped:(UIButton*)sender{
    
    if (sender.tag == COUNTRY_NAME) {
        
        UIPickerView *piker = (UIPickerView*)_txtUserCountry.inputView;
        
        if(([piker selectedRowInComponent:0] - 1) >= 0)
        {
            selectedCountryIndex = (int)[piker selectedRowInComponent:0] - 1;
            _txtUserCountry.text = [[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_name"];
            
        }
        [_txtUserCountry resignFirstResponder];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        return _arrCountry.count + 1;
    }
    return 0;
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == COUNTRY_NAME) {
        if (row == 0)
        {
            return NSLocalizedString(@"Choose_Country", nil);
        }
        return [[_arrCountry objectAtIndex:row-1] objectForKey:@"country_name"];
    }
    return Empty;
}

#pragma mark - Gesture Action

- (void)verifyAccountViewTapped:(UITapGestureRecognizer*)sender
{
    VerifyAccountViewController *verifyAccountObj = [[VerifyAccountViewController alloc] initWithNibName:@"VerifyAccountViewController" bundle:nil];
    [self.navigationController pushViewController:verifyAccountObj animated:YES];
}

- (void)taxiDriververifyViewTapped:(UITapGestureRecognizer*)sender{
    DriverVerificationVC *driverVerifyObj = [[DriverVerificationVC alloc] initWithNibName:@"DriverVerificationVC" bundle:nil];
    [self.navigationController pushViewController:driverVerifyObj animated:YES];
}

- (void)submitViewTapped:(UITapGestureRecognizer*)sender
{
    if (![self validate]) {
        return;
    }
    NSDictionary *param = @{@"userName":[NSString stringWithFormat:@"%@ %@",_txtUserFirstName.text,_txtUserLastName.text],
                            @"firstName":_txtUserFirstName.text,
                            @"lastName":_txtUserLastName.text,
                            @"userEmail":[[GlobalInfo sharedInfo] userEmail],
                            @"phoneNo":_txtUserPhoneNumber.text,
                            @"userDOB":@"",//Not Need now (old version needed)
                            @"profilePic":[_imageObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                            @"profilePicExt":_imageObj.mimeType,
                            @"receivePushNotifications":_switchPushNotify.on?@"1":@"0",
                            @"userGender":@"",//Not Need now (old version needed)
                            @"userAddress":_txtUserAddress.text,
                            @"userCountry" : _txtUserCountry.text,
                            @"userSelectedCountryId":[[_arrCountry objectAtIndex:selectedCountryIndex] objectForKey:@"country_id"]
                            
                            };
    
    
    //=======Call WebService Engine========
    [[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kEditProfile
                                                    Method:REQMETHOD_POST
                                                parameters:[GlobalInfo getParmWithSession:param]
                                                     media:nil
                                            viewController:self
                                                   success:^(id responseObject){
                                                       NSLog(@"response ===   %@",responseObject);
                                                       if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                           [Helper showAlertWithTitle:Empty Message:[[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"]];
                                                           NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
                                                           [[GlobalInfo sharedInfo] setValuesFromEditProfileData:dataDict];
                                                       }
                                                   }
                                                   failure:^(id responseObject){
                                                       [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"Went_Wrong_Alert",nil)];
                                                   }];
    
    
}

#pragma mark- Action Methods

- (IBAction)changeProfileTapped:(id)sender {
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *Camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Camera", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            if ([Helper CameraPermission]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
                imagePickerController.allowsEditing = YES;
                imagePickerController.delegate = self;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            }else{
               
                [Helper showCameraGoToSettingsAlert];
            }
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"CameraNotAvailable_Alert", nil)];
        }
        
    }];
    
    UIAlertAction *Library = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
            imagePickerController.allowsEditing = YES;
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:NULL];
            
        } else {
            [Helper showAlertWithTitle:Empty Message:NSLocalizedString(@"GalleryNotAvailable_Alert", nil)];
        }
    }];
    
     [GlobalInfo showAlertTitle:Empty Message:NSLocalizedString(@"Upload_Image_From", nil) actions:[NSArray arrayWithObjects:cancel,Camera,Library, nil]];

}



#pragma mark - UITextfieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
    
}

#pragma mark- UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = info[UIImagePickerControllerEditedImage];
    //ImageObjectClass *imageObj = [[ImageObjectClass alloc] init];
    _imgProfile.image = image;
    //Or you can get the image url from AssetsLibrary
    //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@"Path: %@",path);
    
    //=========get image type and compress========
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
    NSString *extension = [assetURL pathExtension];
    CFStringRef imageUTI = (UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,(__bridge CFStringRef)extension , NULL));
    // NSString *strImageType = Empty;
    
    if (UTTypeConformsTo(imageUTI, kUTTypeJPEG))
    {
        // Handle JPG
        _imageObj.mimeType = @"jpeg";
        _imageObj.imgData = [Helper compressImage:image];
        CFRelease(imageUTI);
    }
    else if (UTTypeConformsTo(imageUTI, kUTTypePNG))
    {
        // Handle PNG
        _imageObj.mimeType = @"png";
        _imageObj.imgData = UIImagePNGRepresentation(image);
        CFRelease(imageUTI);
    }
    else
    {
        NSLog(@"Unhandled Image UTI: %@", imageUTI);
        
        // Handle JPG
        _imageObj.mimeType = @"jpeg";
        _imageObj.imgData = [Helper compressImage:image];
    }
    
    //_imageData = [_imageData base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //CFRelease(imageUTI);
    //===============================
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        _imageObj.mediaType = @"image";
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end



//==================== old + reuse Code ===================


//- (void)pickerDoneTapped:(UIButton*)sender{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//
//    if (sender.tag == DATE_OF_BIRTH) {
//        UIDatePicker *dtPic = (UIDatePicker*)_txtDob.inputView;
//        _txtDob.text = [dateFormatter stringFromDate:[dtPic date]];
//        [_txtDob resignFirstResponder];
//    }
//}

//- (IBAction)GenderSelected:(UIButton *)sender {
//    if (sender.tag == 1) {
//        _imgRadioMale.image = [UIImage imageNamed:@"radio_button"];
//        _imgRadioFemale.image = [UIImage imageNamed:@"radio_button_deselect"];
//    }else{
//        _imgRadioMale.image = [UIImage imageNamed:@"radio_button_deselect"];
//        _imgRadioFemale.image = [UIImage imageNamed:@"radio_button"];
//    }
//}


//@property (weak, nonatomic) IBOutlet UITextField *lblEmail;
//@property (weak, nonatomic) IBOutlet UITextField *lblName;
//@property (weak, nonatomic) IBOutlet UITextField *lblPhoneNumber;
//@property (weak, nonatomic) IBOutlet UIImageView *imgRadioMale;
//@property (weak, nonatomic) IBOutlet UIImageView *imgRadioFemale;
//@property (weak, nonatomic) IBOutlet UITextField *txtDob;

//@property (weak, nonatomic) IBOutlet UIView *headerView;
/*- (IBAction)profileSaveTapped:(id)sender {
 
 //Request example :
// /user_settings/edit_profile?userId=17&sessionId=dc3535b3a1f36d0343d4664d53d693fe6f16ab4&os=Android&osVersion=M&appVersion=1.0&userName=Neha%20Agarwala&userEmail=mcheckrpi1@gmail.com&phoneNo=4635432546723547&userDOB=01-01-2017&receivePushNotifications=1&userGender=F

//userName=Neha%20Agarwala&userEmail=mcheckrpi1@gmail.com&phoneNo=4635432546723547&userDOB=01-01-2017&receivePushNotifications=1&userGender=F
//  NSString *strGender = Empty;

//    if ([_imgRadioMale.image isEqual:[UIImage imageNamed:@"radio_button"]]) {
//        strGender = @"M";
//    }
//    if ([_imgRadioFemale.image isEqual:[UIImage imageNamed:@"radio_button"]]) {
//        strGender = @"F";
//    }
//
NSDictionary *param = @{@"userName":[NSString stringWithFormat:@"%@ %@",_txtUserFirstName.text,_txtUserLastName.text],
                        @"firstName":_txtUserFirstName.text,
                        @"lastName":_txtUserLastName.text,
                        @"userEmail":[[GlobalInfo sharedInfo] userEmail],
                        @"phoneNo":_txtUserPhoneNumber.text,
                        @"userDOB":@"",//Not Need now (old version needed)
                        @"profilePic":[_imageObj.imgData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn],
                        @"profilePicExt":_imageObj.mimeType,
                        @"receivePushNotifications":_switchPushNotify.on?@"1":@"0",
                        @"userGender":@"",//Not Need now (old version needed)
                        @"userAddress":_txtUserAddress.text,
                        @"userCountry" : _txtUserCountry.text
                        
                        };


//=======Call WebService Engine========
[[WebServiceApi sharedWebServiceApi] reqServiceWithApi:kEditProfile
                                                Method:REQMETHOD_POST
                                            parameters:[GlobalInfo getParmWithSession:param]
                                                 media:nil
                                        viewController:self
                                               success:^(id responseObject){
                                                   NSLog(@"response ===   %@",responseObject);
                                                   if ([GlobalInfo checkStatusSuccess:responseObject]) {
                                                       [Helper showAlertWithTitle:Empty Message:[[responseObject objectForKey:@"responseStatus"] objectForKey:@"MESSAGE"]];
                                                       NSDictionary *dataDict = [responseObject objectForKey:@"responseData"];
                                                       
                                                       [[GlobalInfo sharedInfo] setProfilePic:[dataDict objectForKey:@"profilePic"]];
                                                       [[GlobalInfo sharedInfo] setReceiveNotification:[dataDict objectForKey:@"receivePushNotifications"]];
                                                       [[GlobalInfo sharedInfo] setUserDOB:[dataDict objectForKey:@"userDOB"]];
                                                       [[GlobalInfo sharedInfo] setUserGender:[dataDict objectForKey:@"userGender"]];
                                                       [[GlobalInfo sharedInfo] setUserName:[dataDict objectForKey:@"userName"]];
                                                       [[GlobalInfo sharedInfo] setPhoneNo:[dataDict objectForKey:@"userPhone"]];
                                                   }
                                               }
                                               failure:^(id responseObject){
                                                   [Helper showAlertWithTitle:Empty Message:strKey_error_wentwrong];
                                               }];

} */
