//
//  AppDelegate.h
//  Angeler
//
//  Created by AJAY on 20/02/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationManager.h"
#import <UserNotifications/UserNotifications.h>
#import "Angeler-Swift.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>//,GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) LocationManager * shareModel;

- (void)openSideViewFromViewController:(UIViewController*)CurrentViewController;
- (void)showHomeScreen;
@end

