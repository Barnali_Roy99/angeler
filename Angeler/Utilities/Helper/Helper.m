

#import "Helper.h"
#import "UIWindow+VisibleViewController.h"
#import "MBProgressHUD.h"
#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiansToDegrees(x) (x * 180.0 / M_PI)

@implementation Helper

+(void)checkProgressHUD:(UIViewController *)controller
{
    
    
}

+ (void)showAlertWithTitle:(NSString*)title Message:(NSString*)message
{
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [GlobalInfo showAlertTitle:title Message:message actions:[NSArray arrayWithObject:cancel]];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:OK otherButtonTitles:nil];
//    [alert show];
}

+(void)showCameraGoToSettingsAlert
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"CameraPermissionRequired_Alert", nil)
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Open_Settings", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Not_Now", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                      
                                   }];
    
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [[[AppDel window] visibleViewController] presentViewController:alert animated:YES completion:^{
        
    }];
}

+(BOOL)isLocationServiceDenied{
    if ([CLLocationManager locationServicesEnabled]){
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            return true;
        }
    }
    
     return false;
}

+(void)showLocationServiceGoToSettingsAlert
{
   
    if ([CLLocationManager locationServicesEnabled]){
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"Location_Service_Required_Alert", nil)
                                                                          message:@""
                                                                   preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Open_Settings", nil)
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action)
                                       {
                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                       }];
            
            UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Not_Now", nil)
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action)
                                           {
                                               
                                           }];
            
            
            [alert addAction:cancelButton];
            [alert addAction:okButton];
            
            [[[AppDel window] visibleViewController] presentViewController:alert animated:YES completion:^{
                
            }];
        }
    }
}

+ (BOOL)checkEmailFormat:(NSString*)aStrEmail {
    
    if([aStrEmail length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:aStrEmail options:0 range:NSMakeRange(0, [aStrEmail length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

+ (BOOL)checkPasswdType:(NSString*)aStrPasswd
{
    if (aStrPasswd.length<8) {
        return NO;
    }
    NSRange upperCaseRange;
    NSCharacterSet *upperCaseSet = [NSCharacterSet uppercaseLetterCharacterSet];
    
    upperCaseRange = [aStrPasswd rangeOfCharacterFromSet: upperCaseSet];
    if (upperCaseRange.location == NSNotFound)
    {
        return NO;
    }
    if ([aStrPasswd rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location!=NSNotFound)
    {
        return NO;
    }
    if ([aStrPasswd rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location==NSNotFound) {
        return NO;
    }
    NSString *specialCharacters = @"!#€%&/()[]=?$§*'@";
    if ([aStrPasswd rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacters]].location!=NSNotFound) {
        return NO;
    }
    return YES;
}

+ (BOOL)checkEvenNumber:(NSInteger)num{
    if (num % 2 == 0) {
        // even stuff here
        return YES;
    } else {
        // odd stuff here
        return NO;
    }
}
+ (BOOL)checkEmptyField:(NSString*)aStrText
{
    if ([aStrText isEqualToString:Empty]) {
        return true;
    }
    return false;
}


+(NSAttributedString*) getUnderlineText:(NSString*)text :(NSRange)range withColor:(UIColor*)color
{
   NSMutableAttributedString *underLineTextString = [[NSMutableAttributedString alloc] initWithString:text];
    [underLineTextString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
                            range:range];
    [underLineTextString addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    return underLineTextString;
    
}
+(NSAttributedString*) getColorAttributedText:(NSString*)text :(NSRange)range withColor:(UIColor*)color
{
    NSMutableAttributedString *coloredTextString = [[NSMutableAttributedString alloc] initWithString:text];
   
    [coloredTextString addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    return coloredTextString;
}
+(NSAttributedString*) getBolHighlightedText:(NSString*)text withRange:(NSRange)range withColor:(UIColor*)color withBoldTextFontSize:(CGFloat)fontSize{
    NSMutableAttributedString *boldTextString = [[NSMutableAttributedString alloc] initWithString:text];
  
    [boldTextString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Bold" size:fontSize]} range:range];
    
    return boldTextString;
    
}
    

+ (BOOL)checkMisMatchPassword:(NSString*)Password withConfirmPassword:(NSString*)ConfirmPassword
{
    if ([Password isEqualToString:ConfirmPassword]) {
        return NO;
    }
    return YES;
}

//+ (UIImage *)compressImage:(UIImage *)image
+ (NSData *)compressImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    //return [UIImage imageWithData:imageData];
    return imageData;
}


#pragma mark -
#pragma mark - Distance convertion methods

+ (double)meterToKilometer:(double)meter
{
    double kilometer=meter/1000;
    return kilometer;
}
+ (double)kilometerToMeter:(double)kilometer
{
    double meter=kilometer*1000;
    return meter;
}
+ (double)meterToMiles:(double)meter
{
    double miles=meter * 0.00062137119;
    return miles;
}
+ (double)milesToMeter:(double)miles
{
    double meter=miles/0.00062137119;
    return meter;
}
+ (double)kilometerToMiles:(double)kilometer
{
    double miles=kilometer * 0.62137;
    return miles;
}
+ (double)milesToKilometer:(double)miles
{
    double kilometer=miles/0.62137;
    return kilometer;
}


#pragma mark -
#pragma mark - String Utillity Methods

+ (NSString *)trimString:(NSString *)theString
{
    NSString *theStringTrimmed = [theString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return theStringTrimmed;
}

+ (NSString *)removeNull:(NSString *)string
{
    NSRange range = [string rangeOfString:@"null"];
    if (range.length > 0 || string == nil) {
        string = Empty;
    }
    string = [self trimString:string];
    return string;
}

+ (NSArray *)substring:(NSString *)haystack withString:(NSString *)niddle {
    NSArray * subStringArray = [haystack componentsSeparatedByString:niddle];
    return subStringArray;
}


#pragma mark -
#pragma mark - Directory Path Methods

+ (NSString *)applicationDocumentDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

+ (NSString *)applicationCacheDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return cacheDirectory;
}

+ (NSURL *)applicationDocumentsDirectoryURL
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark -
#pragma mark - datetime utility method

+(NSString*)getDateTimeToStringWithDate:(NSString*)dateStr Time:(NSString*)timeStr
{
    NSDate *givenDate = [self stringToDate: dateStr withFormate:@"yyyy-MM-dd"];
    NSString *givenFormattedDateString = [self DateToString:givenDate withFormate:@"dd/MM/yyyy"];
    NSDate *givenTime= [Helper stringToDate: timeStr withFormate:@"HH:mm:ss"];
    NSString *givenFormattedTimeString;
    
    if([[Helper getLocalData:kUserDeviceCurrentLanguage] isEqualToString:@"english"]) {
    
    givenFormattedTimeString = [Helper DateToString:givenTime withFormate:@"hh:mm a"];
    }
    else
    {
        givenFormattedTimeString = [Helper DateToString:givenTime withFormate:@"HH:mm"];
        givenFormattedTimeString = [givenFormattedTimeString stringByReplacingOccurrencesOfString:@":" withString:@"h"];
        
        NSLog(@"givenFormattedTimeString French %@",givenFormattedTimeString);
    }
    
    
//    }
    
    
    
    return [NSString stringWithFormat:NSLocalizedString(@"Date_Time", nil),givenFormattedDateString ,givenFormattedTimeString];
    
}

+(NSString*)getOnlyDateStrWithAppSpecificFormat:(NSString*)dateStr
{
    NSDate *givenDate = [self stringToDate: dateStr withFormate:@"yyyy-MM-dd"];
    NSString *givenFormattedDateString = [self DateToString:givenDate withFormate:@"dd/MM/yyyy"];
    
    return givenFormattedDateString;
}

+ (NSDate *)stringToDate:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd:HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: dateString];
    return date;
}

+ (NSDate *)stringToDateUTC:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd:HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString: dateString];
    return date;
}


+ (NSDate *)stringToDate:(NSString *)dateString withFormate:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString: dateString];
    return date;
}

+ (NSString *)DateToString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd:HH:mm:ss"];//2013-07-15:10:00:00
    NSString * strdate = [formatter stringFromDate:date];
    return strdate;
}

+ (NSString *)DateToString:(NSDate *)date withFormate:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];//2013-07-15:10:00:00
    NSString * strdate = [formatter stringFromDate:date];
    return strdate;
}

+ (NSString *)DateToStringForScanQueue:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    NSString * strdate = [formatter stringFromDate:date];
    int dd=[[self trimString:strdate]intValue];
    NSString *format=Empty;
    if (dd==1 || dd==21 || dd==31) {
        format=@"ddst MMMM yyyy, hh:mm:ss a";
    }
    else if (dd==2 || dd==22)
    {
        format=@"ddnd MMMM yyyy, hh:mm:ss a";
    }
    else if (dd==3)
    {
        format=@"ddrd MMMM yyyy, hh:mm:ss a";
    }
    else{
        format=@"ddth MMMM yyyy, hh:mm:ss a";
    }
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:format];
    strdate= [formatter2 stringFromDate:date];
    return strdate;
}

+ (NSString *)DateToString:(NSDate *)date withFormateSufix:(NSString *)format{
    
    NSDateFormatter *prefixDateFormatter = [[NSDateFormatter alloc] init];
    [prefixDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [prefixDateFormatter setDateFormat:format];
    NSString * prefixDateString = [prefixDateFormatter stringFromDate:date];
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [monthDayFormatter setDateFormat:@"d"];
    int date_day = [[monthDayFormatter stringFromDate:date] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [suffixes objectAtIndex:date_day];
    
    prefixDateString = [prefixDateString stringByReplacingOccurrencesOfString:@"." withString:suffix];
    NSString *dateString =prefixDateString;
    
    return dateString;
}

+ (int)dateDiffrenceFromDateInString:(NSString *)date1 second:(NSString *)date2
{
    // Manage Date Formation same for both dates
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *startDate = [formatter dateFromString:date1];
    NSDate *endDate = [formatter dateFromString:date2];
    
    
    unsigned flags = NSCalendarUnitDay;
    NSDateComponents *difference = [[NSCalendar currentCalendar] components:flags fromDate:startDate toDate:endDate options:0];
    
    int dayDiff = (int)[difference day];
    
    return dayDiff;
}

+ (int)dateDiffrenceFromDate:(NSDate *)startDate second:(NSDate *)endDate
{
    unsigned flags = NSCalendarUnitDay;
    NSDateComponents *difference = [[NSCalendar currentCalendar] components:flags fromDate:startDate toDate:endDate options:0];
    
    int dayDiff = (int)[difference day];
    if (dayDiff<1) {
        int hourDiff = (int)[difference hour];
        if (hourDiff>12) {
            return 1;
        }
    }
    return dayDiff;
}

+ (NSString*)getCurrentDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *currentDate = [NSDate date];
    NSString *dateString = [formatter stringFromDate:currentDate];
    
    return dateString;
}

// uicolor colorWithHex string. Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorWithHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(BOOL)isFromDate:(NSDate *)FromDate1 SmallerThanTodate:(NSDate *)ToDate1
{
    
    
    NSTimeInterval distanceBetweenDates = [FromDate1 timeIntervalSinceDate:ToDate1];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}

+(void)WriteAttachmentToDocumentDirectory:(UIImage *)image ImageName:(NSString *)name
{
    NSData *pngData = UIImagePNGRepresentation(image);
    if (pngData!=nil||pngData !=(id)[NSNull null]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",name]]; //Add the file name
        NSFileManager *manager = [NSFileManager defaultManager];
        NSError *err;
        if ([manager fileExistsAtPath:filePath]) {
            [manager removeItemAtPath:filePath error:&err];
        }
        [pngData writeToFile:filePath atomically:YES]; //Write the file
        NSLog(@"writing file");
    }
}


+(UIViewController *) getVisibleViewControllerFrom:(UIViewController *)vc{
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if (vc.presentedViewController) {
            return [self getVisibleViewControllerFrom:vc.presentedViewController];
        } else {
            return vc;
        }
    }
}


+(NSAttributedString *)SetTextFieldPlaceholderWithText:(NSString *)Placeholder withSize:(CGFloat)Size andTextColor:(UIColor *)TextColor
{
    NSDictionary *placeholderAttributes=nil;
//    if (IS_IPHONE||IS_IPHONE_SIMULATOR) {
//        placeholderAttributes= @{
//            NSForegroundColorAttributeName:TextColor ,
//            NSFontAttributeName:[Helper adaptiveFontWithName:ROBOTO_REGULAR AndSize:Size]
//            };
//    } else {
//        placeholderAttributes= @{
//            NSForegroundColorAttributeName:TextColor ,
//            NSFontAttributeName:[Helper adaptiveFontWithName:ROBOTO_REGULAR AndSize:Size+4]
//            };
//    }
    
    NSAttributedString *attributedPlaceholder = [[NSAttributedString alloc] initWithString:Placeholder attributes:placeholderAttributes];
    
    return attributedPlaceholder;
}
+(UIView *)AddLeftViewToTxtField:(UITextField *)TxtField withWidth:(CGFloat)Width
{
    UIView *LeftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Width, TxtField.frame.size.height)];
    [LeftView setBackgroundColor:[UIColor clearColor]];
    [TxtField setLeftView:LeftView];
    [TxtField setLeftViewMode:UITextFieldViewModeAlways];
    return LeftView;
}
+(void)AddLeftViewToTxtField:(UITextField *)TxtField withCustomView:(UIView *)leftView
{
    [TxtField setLeftView:leftView];
    [TxtField setLeftViewMode:UITextFieldViewModeAlways];
    
}
+(UIImage *)imageFromColor:(UIColor *)color {
    
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
+(UIFont *)adaptiveFontWithName:(NSString *)FontName AndSize:(CGFloat)FontSize
{
    CGFloat size=FontSize;
    
//    if (IS_IPHONE ||IS_IPHONE_SIMULATOR)
//    {
//        if (IS_IPHONE6Plus) {
//            size+=2.0f;
//        }
//        else if (IS_IPHONE6) {
//            size=FontSize;
//        }
//        else {
//            size-=3.0f;
//        }
//    }
//    else
//    {
//        
//        size+=4.0f;
//    }
    
    UIFont *CustomFont=[UIFont fontWithName:FontName size:size];
    
    if (CustomFont!=nil) {
         return CustomFont;
    } else {
         return CustomFont;
    }
}

#pragma mark
#pragma mark STRING TO NAME
+(NSString *)convertStringToNameFormat:(NSString *)Staring
{
    NSString *Name=Empty;
    NSArray *SplitArray=[Staring componentsSeparatedByString:@" "];
    for (NSString *Str in SplitArray) {
        
        
        Name=[NSString stringWithFormat:@"%@%@%@ ",Name,[[Str substringWithRange:NSMakeRange(0, 1)] uppercaseString],[Str substringWithRange:NSMakeRange(1,Str.length-1)]];
    }
    return [Name substringWithRange:NSMakeRange(0, Name.length-1)];
}

#pragma detecting ipad not working


+ (BOOL) isiPad
{
    
    static BOOL isIPad = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isIPad = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
    });
    return isIPad;
}
+(BOOL)hasTextviewtextLength:(NSInteger)TextLength ExceededTheTextLimit:(NSInteger)Limit{
    return TextLength <=Limit;
}

+ (void)addShadowToLayer:(CALayer*)layer{
    layer.shadowColor = [UIColor darkGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowOpacity = 0.5;
    layer.shadowRadius = 1.0;
}
+ (void)addBorderToLayer:(CALayer*)layer{
    layer.borderColor = [UIColor darkGrayColor].CGColor;
    layer.borderWidth = 0.5;
}
+(NSAttributedString*)getTheBoldAndRegularStringFrom:(NSString*)string andIndex:(NSInteger)index{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:13.0] range:NSMakeRange(0, index)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Regular" size:13.0] range:NSMakeRange(index, str.length-index)];
    return str;
}

+ (void)makeItRound:(CALayer*)layer withRadius:(float)radius{
    layer.cornerRadius = radius;
    layer.masksToBounds = YES;
}


+ (CGSize)sizeOfLabel:(UILabel *)label withText:(NSString *)text{
    CGSize maximumLabelSize = CGSizeMake(300, CGFLOAT_MAX);
    CGRect textRect = [text boundingRectWithSize:maximumLabelSize
                                         options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                      attributes:@{NSFontAttributeName: label.font}
                                         context:nil];
    return textRect.size;
}
+ (UIDatePicker*)datePickerWithTag:(NSInteger)picTag{
    UIDatePicker *datePickerView = [[UIDatePicker alloc] init];
    [datePickerView setTag:picTag];
    [datePickerView setDatePickerMode:UIDatePickerModeDateAndTime];
    
    if (picTag == DATE_OF_BIRTH) {
        [datePickerView setDatePickerMode:UIDatePickerModeDate];
        [datePickerView setMaximumDate:[NSDate date]];
    }else{
        [datePickerView setMinimumDate:[NSDate date]];
    }
    
    
    
    return datePickerView;
}

+ (UIPickerView*)pickerWithTag:(NSInteger)picTag forControler:(id)controler{
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    [pickerView setDelegate:controler];
    [pickerView setDataSource:controler];
    [pickerView setTag:picTag];
    return pickerView;
}

+ (UIToolbar*)toolbarWithTag:(NSInteger)toolTag forControler:(id)controler{
    // add a toolbar with Done button
    //CGRectMake(0, 0, 320, 44)
    UIToolbar *countryToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    countryToolBar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexCt = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:controler action:nil];
    
    UIBarButtonItem *ctDoneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:controler action:@selector(pickerDoneTapped:)];
    ctDoneButton.tag = toolTag;
    ctDoneButton.tintColor = [UIColor whiteColor];
    [countryToolBar setItems:[NSArray arrayWithObjects:flexCt,ctDoneButton, nil]];
    return  countryToolBar;
    
}
+ (BOOL)CameraPermission{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        return YES;
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
       // return NO;
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
       // return NO;
        
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something

            } else { // Access denied ..do something
                
            }
        }];
        return YES;
    }
    return NO;
}

+ (BOOL)isIphoneX
{
    BOOL IsiPhoneX = NO;
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.top > 24.0) {
            IsiPhoneX = YES;
        }
    }
    
    return IsiPhoneX;
}

+(void)setLocalData:(NSString *)Object Key:(NSString *)KeyName{
    
    [[NSUserDefaults standardUserDefaults] setObject:Object forKey:KeyName];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSString *)getLocalData:(NSString *)KeyName{
    
    NSString *Value = [[NSUserDefaults standardUserDefaults]
                       stringForKey:KeyName];
    return Value;
    
}

+(CGFloat)heightForLableWithText:(NSString*)text font:(UIFont*)font width:(CGFloat)width
 {
    // pass string, font, LableWidth
     UILabel*  label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width,CGFLOAT_MAX)];
     
    
     label.numberOfLines = 0;
     label.lineBreakMode = NSLineBreakByWordWrapping;
     label.font = font;
     label.text = text;
     [label sizeToFit];
    
     return label.frame.size.height;
}

+(GMSCameraUpdate*)focusMapToShowAllMarkers:(NSMutableArray*)markerArr
{
    CLLocationCoordinate2D myLocation = ((GMSMarker *)markerArr.firstObject).position;
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    for (GMSMarker *marker in markerArr)
    {
        bounds = [bounds includingCoordinate:marker.position];
    }
    
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:60.0f];
    
    return update;
}

//+(float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
//{
//    float fLat = degreesToRadians(fromLoc.latitude);
//    float fLng = degreesToRadians(fromLoc.longitude);
//    float tLat = degreesToRadians(toLoc.latitude);
//    float tLng = degreesToRadians(toLoc.longitude);
//
//    float degree = radiansToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)));
//
//    if (degree >= 0) {
//        return degree;
//    } else {
//        return 360+degree;
//    }
//}

+(void)HideAllLoadingHude
{
    //    dispatch_async(dispatch_get_main_queue(), ^{
    // do work here
    UIViewController *currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController visibleViewController];
    [MBProgressHUD hideAllHUDsForView:currentVc.view animated:YES];
    
    //  });
}

+(NSString*)calculateTimeFromDirectionAPI:(NSDictionary*)routeDic withIsDriver:(BOOL)isDriver
{
    NSArray *routeLegsArr = [routeDic objectForKey:@"legs"];
    if(routeLegsArr.count > 0)
    {
        NSDictionary *routeLegsDict = [routeLegsArr objectAtIndex:0];
        NSDictionary *routeDuratioDic = [routeLegsDict objectForKey:@"duration"];
        NSString *titleText = NSLocalizedString(@"Arriving_Time", nil);
        if(isDriver)
            titleText = NSLocalizedString(@"Reaching_Time", nil);
        
        return [NSString stringWithFormat:titleText,[routeDuratioDic objectForKey:@"text"]];
        
    }
    return @"";
}

+(BOOL)isAngelerStatusVerified
{
     if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Verified])
     {
         return true;
     }
    else
    {
        return false;
    }
}




+(BOOL)isAngelerStatusNotVerified{
    if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_NotVerified])
    {
        return true;
    }
    else
    {
        return false;
    }
    
}
+(BOOL)isAngelerStatusPending{
    if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Pending])
    {
        return true;
    }
    else
    {
        return false;
    }
    
}
+(BOOL)isAngelerStatusRejected{
    if([[[[GlobalInfo sharedInfo] userAngelerStatus] lowercaseString] isEqualToString:verificationStatus_Rejected])
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

+(BOOL)isDriverStatusVerified{
    if([[[[GlobalInfo sharedInfo] taxiDriverVerificationStatus] lowercaseString] isEqualToString:verificationStatus_Verified])
    {
        return true;
    }
    else
    {
        return false;
    }
}
+(BOOL)isDriverStatusNotVerified{
    NSLog(@"Driver verification Status %@", [[[GlobalInfo sharedInfo] taxiDriverVerificationStatus] lowercaseString]);
    if([[[[GlobalInfo sharedInfo] taxiDriverVerificationStatus] lowercaseString] isEqualToString:verificationStatus_unverified])
    {
        return true;
    }
    else
    {
        return false;
    }
}
+(BOOL)isDriverStatusPending{
    if([[[[GlobalInfo sharedInfo] taxiDriverVerificationStatus] lowercaseString] isEqualToString:verificationStatus_Pending])
    {
        return true;
    }
    else
    {
        return false;
    }
}
+(BOOL)isDriverStatusRejected{
    if([[[[GlobalInfo sharedInfo] taxiDriverVerificationStatus] lowercaseString] isEqualToString:verificationStatus_Rejected])
    {
        return true;
    }
    else
    {
        return false;
    }
}

+ (NSString *)getPostedTimeStampDifferance:(NSDate*)postedDate
{
    if (postedDate == nil)
    {
        return @" ";
    }
    NSString *dateString = @"";
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dayscomponents = [gregorianCalendar components:NSCalendarUnitDay
                                                            fromDate:postedDate
                                                              toDate:[NSDate date]
                                                             options:0];
    
    NSDateComponents *monthscomponents = [gregorianCalendar components:NSCalendarUnitMonth
                                                              fromDate:postedDate
                                                                toDate:[NSDate date]
                                                               options:0];
    
    NSDateComponents *yearcomponents = [gregorianCalendar components:NSCalendarUnitYear
                                                            fromDate:postedDate
                                                              toDate:[NSDate date]
                                                             options:0];
    NSDateComponents *hourscomponents = [gregorianCalendar components:NSCalendarUnitHour
                                                             fromDate:postedDate
                                                               toDate:[NSDate date]
                                                              options:0];
    
    NSDateComponents *minutescomponents = [gregorianCalendar components:NSCalendarUnitMinute
                                                               fromDate:postedDate
                                                                 toDate:[NSDate date]
                                                                options:0];
    NSDateComponents *secondscomponents = [gregorianCalendar components:NSCalendarUnitSecond
                                                               fromDate:postedDate
                                                                 toDate:[NSDate date]
                                                                options:0];
    
    
    
    if ([yearcomponents year] > 0)
    {
        NSString *yearsAgo = NSLocalizedString(@"years_ago", nil);
        if ([yearcomponents year] == 1){
            yearsAgo = NSLocalizedString(@"year_ago", nil);
        }
        dateString = [NSString stringWithFormat:@"%ld %@", (long)[yearcomponents year],yearsAgo];
    }
    else if ([monthscomponents month] > 0)
    {
        NSString *monthsAgo=NSLocalizedString(@"months_ago", nil);
        if ([monthscomponents month] == 1){
            monthsAgo = NSLocalizedString(@"month_ago", nil);
        }
        dateString = [NSString stringWithFormat:@"%ld %@", (long)[monthscomponents month],monthsAgo];
    }
    else if ([dayscomponents day] > 0)
    {
        NSString *daysAgo = NSLocalizedString(@"days_ago", nil);
        if ([dayscomponents day] == 1){
            daysAgo = NSLocalizedString(@"day_ago", nil);
        }
        dateString = [NSString stringWithFormat:@"%ld %@", (long)[dayscomponents day],daysAgo];
    }
    else if ([hourscomponents hour] > 0)
    {
        NSString *housAgo = NSLocalizedString(@"hrs_ago", nil);
        if ([hourscomponents hour] == 1){
            housAgo = NSLocalizedString(@"hr_ago", nil);
        }
        dateString = [NSString stringWithFormat:@"%ld %@", (long)[hourscomponents hour],housAgo];
    }
    else if ([minutescomponents minute] > 0)
    {
        NSString *minsAgo = NSLocalizedString(@"mins_ago", nil);
        if ([minutescomponents minute] == 1){
            minsAgo = NSLocalizedString(@"min_ago", nil);
        }
        dateString = [NSString stringWithFormat:@"%ld %@", (long)[minutescomponents minute],minsAgo];
    }
    else if ([secondscomponents second] >= 0)
    {
        dateString = NSLocalizedString(@"Just Now", nil);
    }
    
    // [MGLog debug:@"NC-Model" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd) message:[NSString stringWithFormat:@"%@ postedDate %@", postedDate, dateString]];
    
    return dateString;
}


@end
