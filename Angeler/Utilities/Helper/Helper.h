


//#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <GoogleMaps/GoogleMaps.h>

typedef enum{
    DEALTYPE_NONE = 0,
    DEALTYPE_SPECIAL = 1,
    DEALTYPE_NEARBY = 2,
    DEALTYPE_HOT = 3
} DEALTYPE;


@interface Helper : NSObject

+ (void)showAlertWithTitle:(NSString*)title Message:(NSString*)message;

+ (BOOL)checkEmailFormat:(NSString*)aStrEmail;
+ (BOOL)checkPasswdType:(NSString*)aStrPasswd;
+ (BOOL)checkEmptyField:(NSString*)aStrText;
+ (BOOL)checkMisMatchPassword:(NSString*)Password withConfirmPassword:(NSString*)ConfirmPassword;

//+ (UIImage *)compressImage:(UIImage *)image;
+ (NSData *)compressImage:(UIImage *)image;
+(CGFloat)heightForLableWithText:(NSString*)text font:(UIFont*)font width:(CGFloat)width;



//Distance convertion methods
+ (double)meterToKilometer:(double)meter;
+ (double)kilometerToMeter:(double)kilometer;
+ (double)meterToMiles:(double)meter;
+ (double)milesToMeter:(double)miles;
+ (double)kilometerToMiles:(double)kilometer;
+ (double)milesToKilometer:(double)miles;


//String Utillity Functions
+ (NSString *)trimString:(NSString *)theString;
+ (NSString *)removeNull:(NSString *) string;
+ (NSArray *)substring:(NSString *)haystack withString:(NSString *)niddle;


//Directory Path Methods
+ (NSString *)applicationDocumentDirectoryString;
+ (NSString *)applicationCacheDirectoryString;
+ (NSURL *)applicationDocumentsDirectoryURL;


//datetime convertion method
+(NSString*)getDateTimeToStringWithDate:(NSString*)dateStr Time:(NSString*)timeStr;
+(NSString*)getOnlyDateStrWithAppSpecificFormat:(NSString*)dateStr;
+ (NSDate *)stringToDate:(NSString *)dateString;
+ (NSDate *)stringToDate:(NSString *)dateString withFormate:(NSString *)format;
+ (NSString *)DateToString:(NSDate *)date;
+ (NSString *)DateToString:(NSDate *)date withFormate:(NSString *)format;
+ (NSString *)DateToStringForScanQueue:(NSDate *)date;
+ (int)dateDiffrenceFromDateInString:(NSString *)date1 second:(NSString *)date2;
+ (int)dateDiffrenceFromDate:(NSDate *)startDate second:(NSDate *)endDate;
+ (NSString*)getCurrentDate;

+ (UIColor *)colorWithHexString:(NSString *)hexString;
+(UIImage *)imageFromColor:(UIColor *)color;
+(UIFont *)adaptiveFontWithName:(NSString *)FontName AndSize:(CGFloat)FontSize;
+(void)checkProgressHUD:(UIViewController *)controller;



+(NSAttributedString *)SetTextFieldPlaceholderWithText:(NSString *)Placeholder withSize:(CGFloat)Size andTextColor:(UIColor *)TextColor;
+(UIView *)AddLeftViewToTxtField:(UITextField *)TxtField withWidth:(CGFloat)Width;
+(void)AddLeftViewToTxtField:(UITextField *)TxtField withCustomView:(UIView *)leftView;
+(NSAttributedString*)getTheBoldAndRegularStringFrom:(NSString*)string andIndex:(NSInteger)index;

+(BOOL)isFromDate:FromDate SmallerThanTodate:ToDate;
+(UIViewController *) getVisibleViewControllerFrom:(UIViewController *)vc;
+(void)WriteAttachmentToDocumentDirectory:(UIImage *)image ImageName:(NSString *)name;
+ (BOOL) isiPad;
+(NSString *)convertStringToNameFormat:(NSString *)Staring;
+(BOOL)hasTextviewtextLength:(NSInteger)TextLength ExceededTheTextLimit:(NSInteger)Limit;

+ (void)makeItRound:(CALayer*)layer withRadius:(float)radius;
+ (void)addShadowToLayer:(CALayer*)layer;
+ (void)addBorderToLayer:(CALayer*)layer;

+ (CGSize)sizeOfLabel:(UILabel *)label withText:(NSString *)text;
+ (UIDatePicker*)datePickerWithTag:(NSInteger)picTag;
+ (UIPickerView*)pickerWithTag:(NSInteger)picTag forControler:(id)controler;
+ (UIToolbar*)toolbarWithTag:(NSInteger)toolTag forControler:(id)controler;
+ (BOOL)CameraPermission;
+ (BOOL)checkEvenNumber:(NSInteger)num;


+(NSAttributedString*) getUnderlineText:(NSString*)text :(NSRange)range withColor:(UIColor*)color;
+(NSAttributedString*) getBolHighlightedText:(NSString*)text withRange:(NSRange)range withColor:(UIColor*)color withBoldTextFontSize:(CGFloat)fontSize;
+ (BOOL)isIphoneX;


//Device Language Set / Get

+(void)setLocalData:(NSString *)Param Key:(NSString *)KeyName;
+(NSString *)getLocalData:(NSString *)Param;


+(void)showCameraGoToSettingsAlert;

//Map

+( GMSCameraUpdate *)focusMapToShowAllMarkers:(NSMutableArray*)markerArr;
//+(float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc;
+(void)showLocationServiceGoToSettingsAlert;
+(BOOL)isLocationServiceDenied;
+(void)HideAllLoadingHude;
+(NSString*)calculateTimeFromDirectionAPI:(NSDictionary*)routeDic withIsDriver:(BOOL)isDriver;

//Driver + Angeler Verification

+(BOOL)isAngelerStatusVerified;
+(BOOL)isAngelerStatusNotVerified;
+(BOOL)isAngelerStatusPending;
+(BOOL)isAngelerStatusRejected;
+(BOOL)isDriverStatusVerified;
+(BOOL)isDriverStatusNotVerified;
+(BOOL)isDriverStatusPending;
+(BOOL)isDriverStatusRejected;
+ (NSString *)getPostedTimeStampDifferance:(NSDate*)postedDate;
+ (NSDate *)stringToDateUTC:(NSString *)dateString;


+(NSAttributedString*) getColorAttributedText:(NSString*)text :(NSRange)range withColor:(UIColor*)color;

@end
