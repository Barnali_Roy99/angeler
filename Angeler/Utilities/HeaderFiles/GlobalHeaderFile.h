//
//  GlobalHeaderFile.h
//  Angeler
//
//  Created by AJAY on 02/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#ifndef GlobalHeaderFile_h
#define GlobalHeaderFile_h

#define GOOGLE_API_KEY @"AIzaSyDNMA0A9zBVL8c-Z6QeLLZ-Zcm2NSs6WtI"

#pragma mark-- Key
static NSString *zeroUnit = @"0";
static NSString *hundredUnit = @"100";
static NSString *OneUnit = @"1";
static NSString *Empty = @"";
static NSString *OK = @"OK";
static NSString *kUsername = @"user_name";
static NSString *kUserFirstName = @"user_First_name";
static NSString *kUserLastName = @"user_Last_name";
static NSString *kUserAddress = @"user_Adreess";
static NSString *kUserCountry = @"user_Country";
static NSString *kUserVerificationStatusText = @"user_Verification_Text";
static NSString *kRegistrationType = @"registrationType";
static NSString *kUserSocialId = @"UserSocialId";
static NSString *kUserEmail = @"userEmail";
static NSString *kUserGender = @"userGender";
static NSString *kUserPhoneNo = @"userPhoneNo";
static NSString *kuserDob = @"userDOB";
static NSString *kReceiveNotification = @"receiveNotification";
static NSString *kUserId = @"userId";
static NSString *kSessionId = @"sessionId";
static NSString *kUserLoggedInStatus = @"userLoggedInStatus";
static NSString *kProfilePicUrl = @"userProfilePicUrl";
static NSString *kDeviceToken = @"deviceToken";
static NSString *kuserUniqueAngelerID = @"userUniqueAngelerID";
static NSString *kuserStatus = @"userStatus";
static NSString *kuserSelectedCountryID = @"userSelectedCountryID";
static NSString *kShipmentPremiumPrice = @"150";
static NSString *kShipmentMeetingPointPrice = @"65";


#pragma mark -- Notification Types
static NSString *kBookingResponse = @"Booking Response";

static NSString *kPPAngelerDP = @"PP-Angeler DP";
static NSString *kBookingResponsePP = @"Booking Response PP";
static NSString *kSenderAngelerDP = @"Sender-Angeler DP ";

static NSString *kBookingRequest = @"Booking Request";
static NSString *kSenderPPAngeler = @"Sender-PP Angeler";
static NSString *kPPAngelerAngeler = @"PP-Angeler Angeler";
static NSString *kAngelerDPAngeler = @"Angeler-DP Angeler";
static NSString *kDPReceiverAngeler = @"DP-Receiver Angeler";

static NSString *kSenderAngelerSender = @"Sender-Angeler Sender";
static NSString *kAngelerReceiverSender = @"Angeler-Receiver Sender";
static NSString *kSenderPPSender = @"Sender-PP Sender";
static NSString *kPPAngelerSender = @"PP-Angeler Sender";
static NSString *kAngelerDPSender = @"Angeler-DP Sender";
static NSString *kDPReceiverSender = @"DP-Receiver Sender";

static NSString *kSenderAngelerReceiver = @"Sender-Angeler Receiver";
static NSString *kAngelerReceiverReceiver = @"Angeler-Receiver Receiver";
static NSString *kPPAngelerReceiver = @"PP-Angeler Receiver";
static NSString *kAngelerDPReceiver = @"Angeler-DP Receiver";
static NSString *kDPReceiverReceiver = @"DP-Receiver Receiver";

static NSString *kNAngelerAddCredit = @"Add Credits";
static NSString *kNAngelerSahreCreditToReceiver = @"Share Credit Receiver";
static NSString *kNAngelerWithdrawCredit = @"Withdraw Credits";
static NSString *KNAngelerBankDetailsVerified = @"Bank Verification";

static NSString *kNAngelerAccountVerified = @"Account Verification";
static NSString *kNAngelerWalletCreditCommisionAfterDelivery = @"Angeler-Receiver Angeler";

//Angeler

static NSString *KNAngelerGetConnected = @"Get Connected";//will be fired on by admin to those users who are unverified or rejected
static NSString *KNAngelerShipmentExpired = @"Shipment Expired";// will be fired on by cron for packages that are expired
static NSString *KNAngelerANCRefunded = @"ANC Refunded";//will be fired on by cron for packages that are expired and anc is refunded to user

//taxi notification
//Driver
static NSString *KNTaxiBookingRequest = @"Taxi Booking Request";//Rider Book Now just , Driver Got notification
static NSString *KNTaxiDriverVerified = @"Driver Verification";
static NSString *KNTaxiDriverRideCancelAccepted = @"Driver Cancel Accepted";
static NSString *KNTaxiDriverRideCancelReached = @"Driver Cancel Reached";

//Rider / Customer
static NSString *KNTaxiCustomerRideStatusUpdate = @"Ride Status Update";
static NSString *KNTaxiCustomerRideCancelAccepted = @"Rider Cancel Accepted";
static NSString *KNTaxiCustomerRideCancelReached = @"Rider Cancel Reached";

//Angeler Food

static NSString *KNAngelerFoodBooking = @"Booking Food";

//Angeler Charity

static NSString *KNAngelerCharityStatus = @"Charity Status";

//Angeler Duty Free

static NSString *KNAngelerShoppingRequestAccepted = @"Shopping Request Accepted";
static NSString *KNAngelerShoppingRequestDelivered = @"Shopping Request Delivered";
static NSString *KNAngelerShoppingRequestDutyFreeEarning = @"Duty Free Earning";
static NSString *KNAngelerShoppingRequestCanceled = @"Shopping Request Canceled";
static NSString *KNAngelerShoppingMessage = @"Shopping Message";

//Angeler Local Delivery

static NSString *KNAngelerAddLocalDelivery           = @"Add Local Delivery";
static NSString *KNAngelerLocalDeliveryReqAccepted   = @"Delivery Request Accepted";
static NSString *KNAngelerLocalDeliveryReqCancelled  = @"Delivery Request Canceled";
static NSString *KNAngelerLocalDeliveryReqRecieved   = @"Delivery Request Recieved";
static NSString *KNAngelerLocalDeliveryReqDelivered  = @"Delivery Request Delivered";
static NSString *KNAngelerLocalDeliveryEarning       = @"Local Delivery Earning";

#pragma mark -- Navigation Menu
static NSString *kMenuHome = @"Home";
static NSString *kMenuProfile = @"Profile";
static NSString *kMenuChangePassword = @"Change Password";
static NSString *kMenuWallet = @"Wallet";
static NSString *kMenuMytrips = @"My Trips";
static NSString *kMenuMyShipments = @"My Shipments";
static NSString *kMenuMyDelivery = @"My Delivery";
static NSString *kMenuPrivacyPolicy = @"Privacy Policy";
static NSString *kMenuTermsConditions = @"Terms and Conditions";
static NSString *kMenuFAQ = @"FAQ";
static NSString *kMenuLogout = @"Logout";

#pragma mark -- UserDefault Key
static NSString *kUserIsHideOnboard = @"userIsHideOnboard";
static NSString *kUserDeviceCurrentLanguage = @"language";
static NSString *kDriverVerificationStatusText = @"driver_Verification_Text";
static NSString *kDriverVerificationStatus = @"driver_Verification_Status";

#pragma mark -- Stactic Page Link
static NSString *kMenuPrivacyPolicyUrlEngLang = @"https://angeler.com/Privacy-Policy.html";
static NSString *kMenuPrivacyPolicyUrlFrenchLang = @"https://angeler.com/fr/Code-de-conduite.html";
static NSString *kMenuTermsConditionsUrlEngLang = @"https://angeler.com/Terms-and-conditions.html";
static NSString *kMenuTermsConditionsUrlFrenchLang = @"https://angeler.com/fr/Termes-et-conditions.html";
static NSString *kMenuFAQUrl = @"http://103.15.232.68/~angeler/faq.php";

#pragma mark-- Enums

typedef NS_ENUM(NSInteger,TAG_VALUE)
{
    TAG_VALUE_NONE = 0,
    DEPRT_COUNTRY = 1,
    DEPRT_CITY = 2,
    DEST_COUNTRY = 3,
    DEST_CITY = 4,
    DEPRT_DATE_TIME = 5,
    ARRIVAL_DATE_TIME = 6,
    PICK_MEET_POINT_ADDRESS = 7,
    DELIVERY_MEET_POINT_ADDRESS = 8,
    PICK_PICKUP_POINT_ADDRESS = 9,
    DELIVERY_PICKUP_POINT_ADDRESS = 10,
    PACKAGE_FROM = 11,
    PACKAGE_TO = 12,
    PACKAGE_DATE_TIME = 13,
    PACKAGE_TYPE = 14,
    COUNTRY_CODE = 15,
    DELIVERY_POINT_ADDRESS = 16,
    DATE_OF_BIRTH = 17,
    COUNTRY_NAME = 18,
    AIRPORT_NAME = 19
    
};

typedef NS_ENUM(NSInteger,MODE_OF_TRAVEL)
{
    TRAVEL_MODE_ALL = 0,
    TRAVEL_MODE_FLIGHT = 1,
    TRAVEL_MODE_SHIP = 2,
    TRAVEL_MODE_CAR = 3,
    TRAVEL_MODE_TRAIN = 4
};

typedef NS_ENUM(NSInteger,SLIDER_TYPE)
{
    SLIDER_HEIGHT = 1,
    SLIDER_WIDTH = 2,
    SLIDER_LENGTH = 3
};
typedef NS_ENUM(NSInteger,SORT_TYPE)
{
    DELIVERY = 1,
    PRICE = 2,
    RATE = 3
};
typedef NS_ENUM(NSInteger,SORT_ORDER)
{
    ASCENDING = 1,
    DECENDING = 2
};
typedef NS_ENUM(NSInteger,ITINERARY_REQUEST_TITLE)
{
    PACKAGE_INFO = 1,
    CONFIRM_DENY = 2,
    OTHERS = 3,
    DROPOFF = 4,
    DELIVERED = 5
};
typedef NS_ENUM(NSInteger,NOTIF_TYPE)
{
    NOTIF_TYPE_NONE = 0,
    NOTIF_TYPE_SENDER = 1,
    NOTIF_TYPE_ANGELER = 2,
    NOTIF_TYPE_RECEIVER = 3,
    NOTIF_TYPE_DELIVRY_PACKAGE = 4
};



//typedef NS_ENUM(NSInteger,PACKAGE_TYPE)
//{
//    PACKAGE_TYPE_DOCUMENT = 1,
//    PACKAGE_TYPE_NON_DOCUMENT = 2,
//    PACKAGE_TYPE_BOTH = 3
//};



#endif /* GlobalHeaderFile_h */
