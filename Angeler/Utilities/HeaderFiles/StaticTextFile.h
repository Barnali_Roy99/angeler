//
//  StaticTextFile.h
//  Angeler
//
//  Created by AJAY on 05/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#ifndef StaticTextFile_h
#define StaticTextFile_h

//   (= @"    = @")   ( ";     "; )    ( static NSString *strKey_    static NSString *strKey_  )


static NSString *strKey_app_name= @"Angeler";
static NSString *strKey_facebook_app_id= @"1829722800628677";

static NSString *strKey_error_noconnection= @"No Internet connection available";
static NSString *strKey_error_wentwrong= @"Something went wrong.Try again later";
static NSString *strKey_error_imgProcesstwrong= @"Something went wrong with processing image.\nTry again later";
static NSString *strKey_navigation_drawer_open= @"Open drawer";
static NSString *strKey_navigation_drawer_close= @"Close drawer";
static NSString *strKey_loader_processImage= @"Processing images. Please wait....";

static NSString *strKey_permissionSettings= @"To enable them\nGo to app settings and enable permissions";
static NSString *strKey_locationPermissionask= @"Enable Location permission to get better experience";
static NSString *strKey_locationPermissionRequired= @"Location permission required to work";
static NSString *strKey_storagePermissionask= @"Enable Storage permission to get better experience";
static NSString *strKey_storagePermissionRequired= @"Storage permission required to work";
static NSString *strKey_callPermissionask= @"Enable Phone permission to make phone call";
static NSString *strKey_callPermissionRequired= @"Phone permission required to work";
static NSString *strKey_cameraPermissionask= @"Enable Camera permission to proceed";
static NSString *strKey_cameraPermissionRequired= @"Camera permission required to work";

static NSString *strKey_permission_OK= @"Okay";
static NSString *strKey_permission_APP_SETTINGS= @"APP SETTINGS";
static NSString *strKey_permission_CANCEL= @"Cancel";

static NSString *strKey_cameraPageTitle= @"Tap to select";
static NSString *strKey_cameraFolderModeTitle= @"Browse into...";
static NSString *strKey_cameraFolderName= @"Angeler";

static NSString *strKey_listEmtpty= @"No data found!";
static NSString *strKey_dialogClose= @"Close";

static NSString *strKey_Flight= @"Flight";
static NSString *strKey_Ship= @"Ship";
static NSString *strKey_Car= @"Car";
static NSString *strKey_Train= @"Train";
static NSString *strKey_All= @"All";

static NSString *verificationStatus_Pending = @"pending";
static NSString *verificationStatus_Rejected = @"rejected";
static NSString *verificationStatus_Verified = @"verified";
static NSString *verificationStatus_NotVerified = @"notverified";
static NSString *verificationStatus_unverified = @"unverified";

static NSString *Trip_Booking_Status_Reserved = @"Reserved";
static NSString *Trip_Booking_Status_Rejected = @"Rejected";
static NSString *Trip_Booking_Status_Accepted = @"Accepted";
static NSString *Trip_Booking_Status_Expired = @"Expired";


static NSString *Transaction_Added       = @"Added";
static NSString *Transaction_Transferred = @"Transferred";
static NSString *Transaction_Received    = @"Received";
static NSString *Transaction_Blocked     = @"Blocked";
static NSString *Transaction_Paid        = @"Paid";
static NSString *Transaction_Income      = @"Income";
static NSString *Transaction_Withdrawn   = @"Withdrawn";


static NSString *serviceType_Premium = @"Premium";
static NSString *serviceType_Meeting_Point= @"Meeting Point";

static NSString *strKey_package_TypeDocument= @"Document";
static NSString *strKey_package_TypeNonDocument= @"Non-document";
static NSString *strKey_CountryhintSearch= @"Search country";
static NSString *strKey_CityhintSearch= @"Search city";
static NSString *strKey_enterSearchText= @"enter search text";

static NSString *strKey_price_unit= @"SGD";
static NSString *strKey_package_dimension_unit= @"cm";
static NSString *strKey_package_weight_unit= @"gm";
static NSString *strKey_package_width_code= @"W";
static NSString *strKey_package_height_code= @"H";
static NSString *strKey_package_length_code= @"L";
static NSString *strKey_package_width_text= @"width";
static NSString *strKey_package_height_text= @"height";
static NSString *strKey_package_length_text= @"length";
static NSString *strKey_package_weight_text= @"weight";

//Taxi Driver / Rider

static NSString *DriverStatus_Expired      = @"Expired";
static NSString *DriverStatus_Accepted     = @"Accepted";
static NSString *DriverStatus_Rejected     = @"Rejected";
static NSString *DriverStatus_Reached      = @"Reached";
static NSString *DriverStatus_Started      = @"Started";
static NSString *DriverStatus_Completed    = @"Completed";
static NSString *DriverStatus_Pending      = @"Pending";
static NSString *RiderStatus_Cancelled     = @"Rider_Cancelled";
static NSString *DriverStatus_Cancelled    = @"Driver_Cancelled";

static NSString *DriverCaller_RideReq      = @"Ride_Request";
static NSString *DriverCaller_RideStart    = @"Ride_Start";
static NSString *DriverCaller_RideComplete = @"Ride_Complete";
static NSString *DriverCaller_RideReached  = @"Ride_Pickup_Location_Arrival";

static NSString *DriverRiderStatusFree       =  @"Free";
static NSString *DriverRiderStatusEngaged    =  @"Engaged";

//Angeler Duty Free status

static NSString *DutyFreeShopItemStatus_Pending               = @"Pending";
static NSString *DutyFreeShopItemStatus_Accepted              = @"Accepted";
static NSString *DutyFreeShopItemStatus_Delivered             = @"Delivered";
static NSString *DutyFreeShopItemStatus_CustomerCanceled      = @"Customer_Canceled";

//Angeler local Delivery Status
static NSString *LocalDeliveryStatus_Pending               = @"Pending";
static NSString *LocalDeliveryStatus_Accepted              = @"Accepted";
static NSString *LocalDeliveryStatus_Delivered             = @"Delivered";
static NSString *LocalDeliveryStatus_Recieved              = @"Recieved";
static NSString *LocalDeliveryStatus_CustomerCanceled      = @"Customer_Canceled";


static NSString *TaxiPostNotificationCustomerTrackTripRiderStatus = @"CustomerTrackTripRiderStatusNotification";
static NSString *TaxiPostNotificationCustomerBookTripAPISuccess = @"CustomerBookTripAPISuccess";
static NSString *TaxiPostNotificationRiderCancelTrip = @"RiderCancelTripNotification";
static NSString *TaxiPostNotificationCustomerDelayTimerBookingEnd = @"CustomerDelayTimerBookingEndNotification";
//static NSString *TaxiPostNotificationCustomerSideSocketGetDriverCurrentLocation = @"CustomerTrackTripRiderGetDriverCurrentLocation";

//Taxi Driver Verification Upload Doc key
static NSString *KUploadDrivingLicence = @"UploadDrivingLicence";
static NSString *KUploadRegCertificate = @"UploadRegCertificate";
static NSString *KUploadVehiclePicture = @"UploadVehicle";


//..<!--SPLASH SCREEN-->


//..<!--LAUNCHER SCREEN-->

static NSString *strKey_launcher_feelinglazy= @"FEELING LAZY? QUICKLY GET STARTED";
static NSString *strKey_launcher_facebook= @"Login with Facebook";
static NSString *strKey_launcher_or= @"OR";
static NSString *strKey_launcher_google= @"Login with Google";
static NSString *strKey_launcher_wepromise= @"We promise. We will not post anything without your permission.";
static NSString *strKey_launcher_signup= @"Signup with email";
static NSString *strKey_launcher_logincredential= @"LOGIN USING YOUR CREDENTIALS";
static NSString *strKey_launcher_login= @"Login";
static NSString *strKey_launcher_accept_policy= @"By continuing, you confirm you read our\n";
//..<!--By continuing, you confirm you read our\n[Terms and Conditions] and accept [User Policy]-->
static NSString *strKey_launcher_terms= @"[Terms and Conditions]";
//static NSString *strKey_launcher_andaccept= @"\u0020and accept\u0020";
static NSString *strKey_launcher_policy= @"[User Policy]";
static NSString *strKey_launcher_google_signinerror= @"Unexpected response from Google Account!";
static NSString *strKey_launcher_fb_signinerror= @"Unexpected response from Facebook Account!";
static NSString *strKey_launcher_email_required_header= @"Add Email";
static NSString *strKey_launcher_email_required_subheader= @"Enter your primary work or\n personal email address";
static NSString *strKey_launcher_email_required_editbox= @"Email Address";
static NSString *strKey_launcher_enter_email= @"Enter email address";

static NSString *strKey_launcher_email_reuquiredbox_SUBMIT= @"SUBMIT";


//..<!--LOGIN SCREEN-->
static NSString *strKey_login_Login= @"LOGIN";
static NSString *strKey_login_Email= @"Email";
static NSString *strKey_login_Password= @"Password";
static NSString *strKey_login_ForgotPassword= @"Forgot Password?";
static NSString *strKey_login_SignInWith= @"Sign in with";
static NSString *strKey_login_forgetpwd_header= @"Recover password";
static NSString *strKey_login_forgetpwd_subheader= @"Forgot your password?";
static NSString *strKey_strForgotPassword= @"In order to receive your access code by email, please enter the address you provided during the registration process";
static NSString *strKey_login_forgetpwd_editbox= @"Email Address";
static NSString *strKey_login_forgetpwd_enter_email= @"Enter email address";
static NSString *strKey_login_forgetpwd_SUBMIT= @"SUBMIT";
static NSString *strKey_login_forgetpwd_Back= @"Back";

static NSString *strKey_login_name= @"Name";
static NSString *strKey_login_confirm_password= @"Confirm password";
static NSString *strKey_login_phone_number= @"Phone number";
static NSString *strKey_button_register= @"REGISTER";
static NSString *strKey_title_registration= @"REGISTRATION";
static NSString *strKey_registration_gender= @"Gender";
static NSString *strKey_male= @"Male";
static NSString *strKey_female= @"Female";
static NSString *strKey_registration_date_of_birth= @"Date of Birth";
static NSString *strKey_registration_dob_dd_mm_yyyy= @"DD-MM-YYYY";
static NSString *strKey_registration_receive_notification= @"Receive notification?";
static NSString *strKey_profile_save= @"SAVE";


//..<!--NAVIGATION DRAWER-->
static NSString *strKey_drawer_Home= @"Home";
static NSString *strKey_drawer_Profile= @"Profile";
static NSString *strKey_drawer_Itinerary= @"Itinerary";
static NSString *strKey_drawer_Packages= @"Packages";
static NSString *strKey_drawer_Deliveries= @"Deliveries";
static NSString *strKey_drawer_DeliveryPoint= @"Manage delivery point";
static NSString *strKey_drawer_Privacy_Policy= @"Privacy Policy";
static NSString *strKey_drawer_Terms_and_conditions= @"Terms and Conditions";
static NSString *strKey_drawer_Faq= @"Faq";
static NSString *strKey_drawer_Logout= @"Logout";


//..<!--FORCE UPDATE APP-->
static NSString *strKey_updateapp_header= @"Update app";
static NSString *strKey_updateapp_subheader= @"Your app needs to be updated to use latest features";
static NSString *strKey_updateapp_updatenow= @"Update Now";
static NSString *strKey_updateapp_updatelater= @"Later";

//..<!--PLACE PICKER-->
static NSString *strKey_placepick_SelectedAddress= @"Selected Address";
static NSString *strKey_placepick_Pickyouraddress= @"Pick your address";
static NSString *strKey_placepick_Save= @"Save";
static NSString *strKey_placepick_Next= @"Next";
static NSString *strKey_placepick_Selected_Points= @"Selected Points:";

//..<!--HOME SCREEN-->
static NSString *strKey_home_itineries= @"My itineraries";
static NSString *strKey_home_packages= @"Send packages";
static NSString *strKey_home_deliveriespoint= @"Manage delivery point";
static NSString *strKey_home_pickuprequest= @"Pickup request";
static NSString *strKey_home_pickupconfirmed= @"Pickup confirmed";
static NSString *strKey_home_deliveryincoming= @"Delivery incoming";

//..<!--Itineries List-->

//..<!--Itineries-->
static NSString *strKey_itinerieAddPageTitle= @"Add Itinerary";
static NSString *strKey_itinerieNext= @"Next";
static NSString *strKey_itinerieStep1= @"1";
static NSString *strKey_itinerieStep2= @"2";
static NSString *strKey_itinerieStep3= @"3";
static NSString *strKey_itinerieStep= @"Step";
static NSString *strKey_itinerieWidth= @"cm width";
static NSString *strKey_itinerieHeight= @"cm height";
static NSString *strKey_itinerieLength= @"cm length";
static NSString *strKey_itinerieWeight= @"gm";
static NSString *strKey_Document = @"Document";
static NSString *strKey_NonDocument = @"Non-Document";

static NSString *strKey_itineryDepartingfrom= @"Departure";
static NSString *strKey_itineryCountry= @"Select Country";
static NSString *strKey_itineryCity= @"Select City";
static NSString *strKey_itineryCountryRequired= @"Select country first...";
static NSString *strKey_itineryDestination= @"Arrival";
static NSString *strKey_itineryDeparturedatetime= @"Departure date &amp; time";
static NSString *strKey_itineryArrivaldatetime= @"Arrival date &amp; time";

static NSString *strKey_Pleaseenter= @"* please enter";
static NSString *strKey_arrival= @"arrival";
static NSString *strKey_departure= @"departure";
static NSString *strKey_city= @"city";
static NSString *strKey_datetime= @"date &amp; time";


static NSString *strKey_itineryFromDate= @"Select date &amp; time";
static NSString *strKey_itineryPackageDetails= @"Package Details";
static NSString *strKey_itinerySizeunit= @"Size/unit";
static NSString *strKey_itineryWeightunit= @"Weight/unit";
static NSString *strKey_itineryMaximumUnit= @"Maximum unit";
static NSString *strKey_itineryTypeofpackage= @"Type of package";
static NSString *strKey_itineryModeoftravel= @"Mode of travel";
static NSString *strKey_itineryTravelNumber= @"Travel reference number";
static NSString *strKey_itineryEnterTravelNumber= @"Enter reference number";
static NSString *strKey_itineryPreferredPickupLocation= @"Your preferred pickup location";
static NSString *strKey_itineryEnterPreferredAddress= @"Enter preferred address";
static NSString *strKey_itineryAddress= @"Address";
static NSString *strKey_itineryPickupFrom= @"Pick up method for departure";
static NSString *strKey_itineryDropOfin= @"Delivery method for arrival";
static NSString *strKey_itinerryMeetingPoint= @"Meeting point";
static NSString *strKey_itinerryPickMeetingPointSender= @"pick address where to meet the sender";
static NSString *strKey_itinerryPickMeetingPointReceiver= @"pick address where to meet the receiver";
static NSString *strKey_itinerryPickupPoint= @"Pickup point";
static NSString *strKey_itinerryPickPickupPoint= @"select addresses where you can pickup packages";
static NSString *strKey_itinerryPremiumServiceSender= @"Premium service\n(check this box if you accept to pickup at the sender\'s address)";
static NSString *strKey_itinerryPremiumServiceReceiver= @"Premium service\n(check this box if you accept to drop off at the receiver\'s address)";
static NSString *strKey_itinerryDeliveryPoint= @"Delivery point";
static NSString *strKey_itinerryPickDeliveryPoint= @"select addresses where you can drop off packages";
static NSString *strKey_itineryPlacePickerDefaultTitle= @"Pick location";
static NSString *strKey_itineryPlacePickerMeetingPointTitle= @"Choose meeting point";
static NSString *strKey_itineryPlacePickerPickupPointTitle= @"Choose pickup points";
static NSString *strKey_itineryPlacePickerDeliveryPointTitle= @"Choose delivery points";

static NSString *strKey_itineryConfirm= @"Confirm";
                                                              
static NSString *strKey_itinerieVerifyIdentityPageTitle= @"Verify Identity";
static NSString *strKey_itineryThisisyourfirsttrip= @"This is your first trip. To get started fill in your home address and wait for verification";
static NSString *strKey_itineryHomeAddress= @"Home address";
static NSString *strKey_itineryTypeFullHomeAddress= @"Type your full home address";
static NSString *strKey_itineryCopyofIdcard= @"Upload copy of id card";
static NSString *strKey_itineryBasePrice= @"Base price";
static NSString *strKey_itineryPrice5= @"5 SGD";
static NSString *strKey_itineryPrice20= @"20 SGD";
static NSString *strKey_itineryPrice30= @"30 SGD";
static NSString *strKey_itineryVerify= @"Verify";
static NSString *strKey_itineryListPopupInfo= @"Info";
static NSString *strKey_itineryListPopupDelete= @"Delete";


static NSString *strKey_itineryTranportModeNumberEmpty= @"Transport referrence number required...";
static NSString *strKey_itineryCongratulations= @"Congratulations";
static NSString *strKey_itineryItinertSaved= @"Your itinerary has been saved";
static NSString *strKey_itineryShareText= @"share with your friends your travel to carry their packages!";
static NSString *strKey_itineryItinertAddAnother= @"View itinerary list";

static NSString *strKey_itineries_pick_up_location= @"From:";
static NSString *strKey_itineries_destination= @"To:";
static NSString *strKey_itineries_type= @"Type:";
static NSString *strKey_itineries_status= @"Status:";
static NSString *strKey_itineries_cost= @"Cost:";
static NSString *strKey_Itinerary_Request_List= @"Itinerary Request List";
static NSString *strKey_itineryListpickup= @"Pick up";
static NSString *strKey_itineryListDropup= @"Drop off";
static NSString *strKey_itinery_PackageInfo= @"Package Info";
static NSString *strKey_itinery_actionConfirmDeny= @"Confirm/Deny";
static NSString *strKey_itinery_actionAccepted= @"Pickup";
static NSString *strKey_itinery_actionPickedUp= @"Dropoff";
static NSString *strKey_itinery_actionDelivered= @"Delivered";
static NSString *strKey_itinery_actionDeliveredAtDesk= @"Delivered at delivery point";
static NSString *strKey_itinery_actionUSerCancelledRequest= @"Cancelled";
static NSString *strKey_itinery_actionRejected= @"Rejected";
static NSString *strKey_itinery_actionReuquestExpireed= @"Expired";
static NSString *strKey_itineryRequestConfirm= @"Confirm";
static NSString *strKey_itineryRequestDeny= @"Deny";
static NSString *strKey_itineryrequestInfo_page_title= @"Itinerary Request Info";
static NSString *strKey_itinery_packagesaccepted= @"packages accepted";
static NSString *strKey_itinery_request= @"request";
static NSString *strKey_deptureShortform= @"Dep. :";
static NSString *strKey_arrivalShortform= @"Arr. :";
static NSString *strKey_itinery_pickup_info_page_title= @"Pickup Info";
static NSString *strKey_itinery_pickup_Packagedetails= @"Package details";
static NSString *strKey_itnery_pickup_Senderdetails= @"Sender details";
static NSString *strKey_itinery_pickup_Enterdeliverycode= @"Enter delivery code";
static NSString *strKey_itinery_pickup_entercode= @"enter code";
static NSString *strKey_itinery_pickup_OR= @"OR";
static NSString *strKey_itinery_picup_ScanQRCode= @"Scan \nQR\nCode";
static NSString *strKey_itinery_pickup_PICKUP= @"PICK UP";
static NSString *strKey_itinery_dropoff_info_page_title= @"Dropoff Info";
static NSString *strKey_itinery_delivery_info_page_title= @"Delivery Info";
static NSString *strKey_itinery_dropoff_DROPOFF= @"DROPOFF";
static NSString *strKey_itinery_details_page_title= @"Itinerary Info";
static NSString *strKey_itinery_detailsMode_of_tranport= @"Mode of tranport";
static NSString *strKey_itinery_detailsTransport_reference_number= @"Flight_Number";
static NSString *strKey_itinery_detailsTypeofPackage= @"Type of package";
static NSString *strKey_itinery_detailsWeight= @"Weight(gm)";
static NSString *strKey_itinery_details_packageHeight= @"Height(cm)";
static NSString *strKey_itinery_details_packageWidth= @"Width (cm)";
static NSString *strKey_itinery_details_packageLength= @"Length (cm)";
static NSString *strKey_itinery_details_Maximumunit= @"Maximum unit";
static NSString *strKey_itinery_details_Packagesaccepted= @"Packages accepted";
static NSString *strKey_itinerydetails_meeting_point= @"Meeting point";
static NSString *strKey_itinerydetails_pickup_point= @"Pickup point";
static NSString *strKey_itinerydetails_premium_service= @"Premium service";
static NSString *strKey_itinerydetails_pickup= @"Pickup :";
static NSString *strKey_itinerydetails_dropoff= @"Dropoff :";
static NSString *strKey_itinerydetails_dropoff_point= @"Dropoff point";
static NSString *strKey_step1_itinerary_details= @"Itinerary details";
static NSString *strKey_step2_package_details= @"Package details";
static NSString *strKey_step3_services_offered= @"Services offered";
static NSString *strKey_confirmItineryDelete= @"Do you want to delete this itinerary?";
                                                                
//..<!--PLACE PICKER ACTIVITY-->
static NSString *strKey_placepicker_currentlocationnotfound= @"Unable to retrieve your current location";

//..<!--MANAGE DELIVERY POINT -->
static NSString *strKey_deliveryPointTitle= @"Manage Delivery Point";
static NSString *strKey_deliveryPointAddTitle= @"Create Delivery Point";
static NSString *strKey_deliveryEnterAddress= @"Enter address";
static NSString *strKey_deliveryAddress= @"Address";
static NSString *strKey_deliveryPointAddressTypeHeading= @"Address type";
static NSString *strKey_deliveryPointResidential = @"Residential";
static NSString *strKey_deliveryPointCommercial = @"Commercial";

static NSString *strKey_deliveryPointAvailableOperatingHours= @"Available operating hours";
static NSString *strKey_to= @"to";
static NSString *strKey_deliveryPointFrom= @"From";
static NSString *strKey_deliverPointTo= @"To";
static NSString *strKey_deliveryPointOpenTime= @"Open time";
static NSString *strKey_deliveryPointClosingTime= @"Closing time";
static NSString *strKey_deliveryPointName= @"Name";
static NSString *strKey_deliveryPointEnterContactName= @"Enter contact name";
static NSString *strKey_deliveryPointNumber= @"Number";
static NSString *strKey_deliveryPointFax= @"Fax";
static NSString *strKey_deliveryPointEmail= @"Email";
static NSString *strKey_deliveryPointWebsite= @"Website";
static NSString *strKey_deliveryPointSubmit= @"Submit";
static NSString *strKey_deliverypoint_enter_email= @"Enter email address";
static NSString *strKey_deliveryPointEnterWebsite= @"Enter website (if any)";
static NSString *strKey_countryRequired= @"Oops..something occurred in picking address\n Try to pick address 1 more time";
static NSString *strKey_cityRequired= @"City can not be left blank";
static NSString *strKey_deliveryPointTimeRequired= @"Operating hours can not be left blank!";
static NSString *strKey_deliveryPointNameRequired= @"Name field can not be left blank!";
static NSString *strKey_deliveryPointEmailRequired= @"Email can not be left blank!";
static NSString *strKey_deliveryPointListAddress= @"Address:";
static NSString *strKey_deliveryPointListAddressType= @"Address type:";
static NSString *strKey_deliveryPointListAddressName= @"Shop/Co. name:";
static NSString *strKey_deliveryPointListName= @"Name:";
static NSString *strKey_deliveryPointListPhone= @"Phone:";
static NSString *strKey_deliveryPointListOperatingHours= @"Operating hours:";
static NSString *strKey_deliveryPointListStatus= @"Status:";
static NSString *strKey_deliveryPointListPopupEdit= @"Edit";
static NSString *strKey_deliveryPointListPopupDelete= @"Delete";
static NSString *strKey_deliveryPointScanQRCode= @"Scan QR Code";
static NSString *strKey_deliverypoint_Scan_page_title= @"Scan QR Code";
static NSString *strKey_deliverypointScanPageHeader= @"In order to process package, scan relevant qr code and click Next";
static NSString *strKey_deliverypointScanPageSubHeader= @"Click below image to initiate Scan";
static NSString *strKey_deliverypointScanPageEnterCode= @"Enter code";
static NSString *strKey_deliverypointScanPageEnterCodeHint= @"enter code";
static NSString *strKey_deliverypointScanPageOR= @"OR";
static NSString *strKey_deliverypointScanPageNext= @"Next";
static NSString *strKey_delivery_point_qr_code_required= @"Scan/Enter qr code to proceed";
static NSString *strKey_deliverypoint_Packagedetails_page_title= @"Package Info";
static NSString *strKey_deliverypoints_Confirmpackagereceive= @"Confirm package receive";
static NSString *strKey_deliverypoints_Confirmpackagepickup= @"Confirm package pickup";
static NSString *strKey_deliverypoints_Confirmpackagedrop= @"Confirm package drop";
static NSString *strKey_deliverypoints_Confirmpackagedelivery= @"Confirm package delivery";
static NSString *strKey_delivery_point_add_suceess_text= @"Thank you for your submission!\n As soon as your delivery point will be checked by our team you will be able to receive packages.";
                                                                
  //..  <!--=============Packages==============-->
static NSString *strKey_packagesListPopupInfo= @"Info";
static NSString *strKey_packagesListPopupDelete= @"Delete";
static NSString *strKey_packagesAddPageTitle= @"Add Packages";
static NSString *strKey_packagesNext= @"Next";
static NSString *strKey_packagesStep1= @"1";
static NSString *strKey_packagesStep2= @"2";
static NSString *strKey_packagesStep3= @"3";
static NSString *strKey_packagesStep4= @"4";
static NSString *strKey_packagesStep= @"Step";
static NSString *strKey_packagesSave= @"Save";
static NSString *strKey_packagesFindLater= @"Find Later";
static NSString *strKey_packagespackageDetails= @"Package details";
static NSString *strKey_packagesFromAddress= @"From Address:";
static NSString *strKey_packagesToAddress= @"To Address:";
static NSString *strKey_packagesTypeofPackage= @"Type of package";
static NSString *strKey_packagesCM= @"cm";
static NSString *strKey_packagesGM= @"gm";
static NSString *strKey_packagesHeight= @"Height";
static NSString *strKey_packagesWidth= @"Width";
static NSString *strKey_packagesLength= @"Length";
static NSString *strKey_packagesWeight= @"Weight";
static NSString *strKey_packagesRecepientDetails= @"Recipient Details";
static NSString *strKey_packagesName= @"Name";
static NSString *strKey_packagesEmail= @"Email";
static NSString *strKey_packagesContactNumber= @"Contact Number";
static NSString *strKey_packagesDelieryDateAndTime= @"Delivery Date &amp; Time";
static NSString *strKey_packagesPickDelieryDateAndTime= @"Pick delivery date &amp; time";
static NSString *strKey_packagesRemarksIfAny= @"Remarks (if any)";
static NSString *strKey_packagesChooseYourAngeler= @"Choose Your Angeler";
static NSString *strKey_packagesChooseAngeler= @"Choose Angeler";
static NSString *strKey_packagesSeeDetails= @"See Details";
static NSString *strKey_packagesBookNow= @"Book Now";
static NSString *strKey_packageSelectCountry= @"Select country";
static NSString *strKey_packageSelectCity= @"Select city";
static NSString *strKey_packagePictureOfItem= @"Picture of item";
static NSString *strKey_packageDeclaration1= @"I declare that all information provided are accurate (";
static NSString *strKey_packageDeclaration2= @"Terms and Conditions)";



static NSString *strKey_package_info_page_title= @"Package Info";
static NSString *strKey_package_info_from= @"From:";
static NSString *strKey_package_info_to= @"To:";
static NSString *strKey_package_info_packagetype= @"Package type:";
static NSString *strKey_package_info_packagesize= @"Size (H x W x L):";
static NSString *strKey_package_info_packageweight= @"Weight:";
static NSString *strKey_package_info_transportmode= @"Mode of transport:";
static NSString *strKey_package_info_packagestatus= @"Status:";
static NSString *strKey_package_info_picture= @"Picture of item";
static NSString *strKey_package_info_recepinetdetails= @"Recipient details";
static NSString *strKey_package_info_senderdetails= @"Sender details";
static NSString *strKey_package_info_deliverytime= @"Delivery time:";
static NSString *strKey_package_info_remarks= @"Remarks:";
static NSString *strKey_packageListTravelMode= @"Travel Mode:";
static NSString *strKey_packageListType= @"Type:";
static NSString *strKey_packageListBookingrequest= @"Booking request:";
static NSString *strKey_package_request_info_page_title= @"Package Request Info";
static NSString *strKey_package_pick_up= @"Pickup:";
static NSString *strKey_package_destination= @"Drop off:";
static NSString *strKey_package_BookingDate= @"Booking Date :";
static NSString *strKey_package_request_cancel= @"<u>Cancel</u>";
static NSString *strKey_package_tracking_info_page_title= @"Tracking Info";
static NSString *strKey_package_AngelerInfo= @"Angeler Info";
static NSString *strKey_package_PickupLocation= @"Pickup Location";
static NSString *strKey_package_DropoffLocation= @"Delivery Location";
static NSString *strKey_package_recipient_details= @"Recipient Details";
static NSString *strKey_package_delivered_by= @"Deliver by";
static NSString *strKey_package_delivery_point_details= @"Delivery point details";
static NSString *strKey_package_pickup_point_details= @"Pickup point details";
static NSString *strKey_package_Qrcode= @"Qr Code";
static NSString *strKey_package_delivery_code= @"Delivery Code:";
static NSString *strKey_package_QrCodeTitle_PP_PMP= @"Show this code to angeler to confirm pickup";
static NSString *strKey_package_QrCodeTitle_PPP= @"Show this code to the pickup point desk to confirm package pickup";
static NSString *strKey_package_QrcodeTitlePickup= @"Show this code to the pickup point desk to confirm package pickup";
static NSString *strKey_package_QrcodeTitleDropoff= @"Show this code to the delivery point desk to confirm package delivery";
static NSString *strKey_package_ScanaQrcode= @"Scan a QR code";
static NSString *strKey_confirmPackageDelete= @"Do you want to delete this package?";

//..<!--choose angeler-->
static NSString *strKey_save= @"Save";
static NSString *strKey_chooseAngelerNext= @"Next";

static NSString *strKey_Choose_angeler_page_title= @"Choose angeler";

static NSString *strKey_Select_your_preferred_pickup_Method_from_eparture= @"Select your preferred pickup Method from departure";
static NSString *strKey_Select_your_preferred_delivery_method_from_arrival= @"Select your preferred delivery method from arrival";
static NSString *strKey_Meeting_point= @"Meeting point (SGD 15)";
static NSString *strKey_Pickup_point= @"Pickup point(SGD 25)";
static NSString *strKey_Select_pickup_points= @"Select pickup points";
static NSString *strKey_Premium_pic= @"Premium(SGD 40)";
                                                                
static NSString *strKey_Meeting_point_des= @"Meeting point(SGD 20)";
static NSString *strKey_Delivery_point= @"Delivery point(SGD 25)";
static NSString *strKey_Select_Delivery_points= @"Select Delivery points";
static NSString *strKey_Premium_des= @"Premium(SGD 40)";
static NSString *strKey_sortDeliveryEarlyLate = @"Delivery-Early to Late";
static NSString *strKey_sortDeliveryLateEarly = @"Delivery-Late to Early";

static NSString *strKey_sortPriceLowHigh = @"Price-Low to High";
static NSString *strKey_sortPriceHighLow = @"Price-High to Low";
static NSString *strKey_sortRateLowHigh = @"Rating-Low to High";
static NSString *strKey_sortRateHighLow = @"Rating-High to Low";

static NSString *strKey_choose_angeler_Pickup= @"Pickup : ";
static NSString *strKey_choose_angeler_Delivery= @"Delivery: ";
                                                                
static NSString *strKey_Departur_date= @"Departure date &amp; time";
static NSString *strKey_Arrival_date= @"Arrival date &amp; time";
static NSString *strKey_Mode_of_tranport= @"Mode of tranport";
static NSString *strKey_Transport_reference_number= @"Transport reference number ";
static NSString *strKey_Registration_date= @"Registration date";
static NSString *strKey_Angeler_ratings= @"Angeler ratings";
static NSString *strKey_chooseAngelerTypeofPackage= @"Type of package";
static NSString *strKey_chooseAngelerWeight= @"Weight(gm)";
static NSString *strKey_chooseAngeler_packageHeight= @"Height(cm)";
static NSString *strKey_chooseAngeler_packageWidth= @"Width (cm)";
static NSString *strKey_chooseAngeler_packageLength= @"Length (cm)";
static NSString *strKey_chooseAngelerBook= @"Book";
static NSString *strKey_chooseAngelerPickup= @"Pickup";
static NSString *strKey_chooseAngelerDelivery= @"Delivery";
static NSString *strKey_chooseAngelerChooseLater= @"Choose Later";
static NSString *strKey_dialog_you_book_an_angeler = @"You book an Angeler";


//..<!--recipient section-->
static NSString *strKey_recipient_package_info_page_title= @"Delivery Info";
static NSString *strKey_package_QrCodeTitle_DP_DMP= @"Show this code to angeler to confirm delivery";
static NSString *strKey_package_QrCodeTitle_DDP= @"Show this code to the dropoff point desk to confirm package delivery";


//Add Itinery step one
static NSString *strKey_itineryDepartureCountryEmpty= @"Departure country required...";
static NSString *strKey_itineryDepartureCityEmpty= @"Departure city required...";
static NSString *strKey_itineryArrivalCountryEmpty= @"Arrival country required...";
static NSString *strKey_itineryArrivalCityEmpty = @"Arrival city required...";
static NSString *strKey_itineryArrivalDepartureCityEquals = @"Arrival and Departure city needs to be different";
static NSString *strKey_itineryDepartureDateTimeEmpty= @"Departure datetime required...";
static NSString *strKey_itineryArrivalDateTimeEmpty= @"Arrival datetime required...";
static NSString *strKey_itineryTranportModeEmpty= @"Mode of transport required...";
//Add Itinery step three
static NSString *strKey_itineryItinertPickIdEmpty= @"Select atleast 1 pickup method";
static NSString *strKey_itineryItinertPickMeetingPointEmpty= @"Select pickup meeting point";
static NSString *strKey_itineryItinertPickPointsEmpty= @"Select pickup points";
static NSString *strKey_itineryItinertDropoffIdEmpty= @"Select atleast 1 delivery method";
static NSString *strKey_itineryItinertDropMeetingPointEmpty= @"Select delivery meeting point";
static NSString *strKey_itineryItinertDropPointsEmpty= @"Select delivery points";
// Itinerary Pick Up
static NSString *strKey_itinery_pickup_qr_code_required= @"Scan/Enter qr code to complete pickup";

// Add package step one
static NSString *strKey_packageFromAddressRequired= @"Select from address";
static NSString *strKey_packageFromCountryRequired= @"Select from country";
static NSString *strKey_packageFromCityRequired= @"Select from city";
static NSString *strKey_packageToAddressRequired= @"Select to address";
static NSString *strKey_packageToCountryRequired= @"Select to country";
static NSString *strKey_packageToCityRequired= @"Select to city";
static NSString *strKey_packageDimensionsRequired= @"Enter package dimensions";
static NSString *strKey_packageWeightRequired= @"Enter package weight";
static NSString *strKey_packageAcceptDeclaration= @"You have to accept declaration before further proceeding";
static NSString *strKey_packageTravelRequired= @"Select an travel mode";
static NSString *strKey_packageImageRequired= @"Attach some pictures of package";

// IOS Specific only
static NSString *strKey_CameraPermissionRequired = @"Please check camera permission in settings for Angeler App.";
static NSString *strKey_CameraNotAvailable = @"Camera is not available, please select a different option";
static NSString *strKey_GalleryNotAvailable = @"Gallery is not available, please select a different option";
static NSString *strKey_ScanningUnAvailable = @"Scanning Unavailable";

// Add Package Step Two
static NSString *strKey_packageReceipientNameRequired= @"Fill uprecipient name";
static NSString *strKey_packageReceipientEmailRequired= @"Fill uprecipient email";
static NSString *strKey_packageReceipientDuplicateEmail= @"You cannot enter your email id as recipient";
static NSString *strKey_packageReceipientPhoneRequired= @"Fill uprecipient phone number";
static NSString *strKey_packageReceipientDeliveryDateTimeRequired= @"Choose your preferred delivery date and time";
static NSString *strKey_packageRemarksRequired= @"enter some remarks";
// Email SignUp
static NSString *strKey_login_nameblank= @"Name field can not be left blank";
static NSString *strKey_login_firstnameblank= @"First Name field can not be left blank";
static NSString *strKey_login_lastnameblank= @"Last Name field can not be left blank";
static NSString *strKey_login_emailblank= @"Email field can not be left blank";
static NSString *strKey_launcher_enter_valid_email= @"Enter valid email address";
static NSString *strKey_login_passwordblank= @"Password field can not be left blank";
static NSString *strKey_login_passwordlength= @"Password should not be less than 8 character";
static NSString *strKey_login_confirmpasswordblank= @"Confirm password field can not be left blank";
static NSString *strKey_login_confirmpasswordMismatch= @"Both password should be same";

// Pick Up Location from map
static NSString *strKey_placepick_This_place_has_been_added = @"This place has been added";
static NSString *strKey_placepick_Maximum = @"Maximum";
static NSString *strKey_placepick_location_can_be_added = @"location can be added";

// Manage Delivery Points
static NSString *strKey_deliveryPointPhoneRequired = @"Phone number can not be left blank!";
static NSString *strKey_deliveryPointEnterShopName = @"Enter Shop/Company Name";
static NSString *strKey_deliveryAddressRequired = @"Address can not be left blank!";

//// Delivery Point Package List
static NSString *strKey_deliverypoint_pkglist_sender = @"Sender";
static NSString *strKey_deliverypoint_pkglist_receiver = @"Receiver";
static NSString *strKey_deliverypoint_pkglist_angeler = @"Angeler";
static NSString *strKey_deliverypoint_pkglist_package_type = @"Package type:";
static NSString *strKey_deliverypoint_pkglist_status = @"Status:";
static NSString *strKey_deliverypoint_pkglist_view_details = @"View details";

#endif /* StaticTextFile_h */
