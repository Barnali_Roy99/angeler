//
//  WebserviceHeader.h
//  Angeler
//
//  Created by AJAY on 14/03/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#ifndef WebserviceHeader_h
#define WebserviceHeader_h
static NSString *googlemaps  = @"https://maps.googleapis.com/maps/api/geocode/json?";
static NSString *kGoogleMapApi = @"MapApi";
static NSString *kGoogleMapDirectionApi = @"MapDirectionApi";
static NSString *googlemapsDirectionURL  = @"https://maps.googleapis.com/maps/api/directions/json?";

//BaseURl

static NSString *kApiUrl = @"https://angelerapp.com/testserver/web_services/";//Dev Server

//static NSString *kApiUrl = @"https://angelerapp.com/web_services/";//Live Server


//static NSString *kApiUrl = @"http://103.15.232.68/~angeler/testserver/web_services"; //test Server

static NSString *kSocialSignUp = @"user_settings/social_signup_login";          //Social Login/Registration
static NSString *kSocialEmailStatus = @"user_settings/social_email_status";     //Social Email Status
//static NSString *kGeneranLoginSignup =  @"user_settings/general_signup_login";  // General Regn or Login
static NSString *kGeneralRegLogin =     @"user_settings/general_signup_login";
static NSString *kForgotPassword = @"user_settings/forgot_password";
static NSString *kChangePassword = @"user_settings/change_password";
static NSString *kEditProfile = @"user_settings/edit_profile";//edit profile
static NSString *kVerifyViewAccount = @"user_settings/user_certificates";//verify Account
static NSString *kUserHomeDashboardInitialization = @"user_settings/dashboard_initialization";//Get user verification status
static NSString *kLogout = @"user_settings/logout";                             //User Logout
static NSString *kCountryList = @"user_settings/get_country_list";              //Country List
static NSString *kCityList = @"user_settings/get_city_list";                    //City List
static NSString *kCountryCityDetails = @"user_settings/get_country_city_details"; //Country City Details Based On Name
static NSString *kDashboardNotification = @"user_settings/dashboard_notification"; //dashboard notification
static NSString *kDeliveryPointsList = @"delivery_points/pd_list";//Pickup Drop off List (Lat/lng Based)
static NSString *kAddItinerary = @"itinerary/add_itinerary";//Add Itinerary
static NSString *kItineraryList = @"itinerary/itinerary_list";//Itinerary List
static NSString *kItineraryInfo = @"itinerary/itinerary_details";//Itinerary Details
static NSString *kitineraryDelete = @"itinerary/delete_itinerary";//Itinerary Delete
static NSString *kItineraryRequestList = @"itinerary/itinerary_request_list";//Itinerary Request List
static NSString *kItineraryPackBookInfo = @"itinerary/booking_info";//Itinerary Package/Booking Info
static NSString *kItineraryBookingStatus = @"itinerary/update_booking_status";//Itinerary Booking Status Update
static NSString *kItineraryPickupDetails = @"itinerary/pickup_details";//Itinerary Pickup Details
static NSString *kUpdatePickUpStatus = @"itinerary/update_pickup_status";//Itinerary Pickup Status Update
static NSString *kItineraryDropDetails = @"itinerary/dropoff_details";//Itinerary Dropoff Details
static NSString *kUpdateDropOffStatus = @"itinerary/update_dropoff_status";//Itinerary Dropoff Status Update
static NSString *kItineraryRateTable = @"itinerary/get_cost_table"; // Cost-Weight Table For Itinerary

static NSString *kAddPackage = @"package/add_package";//Add Package
static NSString *kPackageList = @"package/package_list";//Package List
static NSString *kPackageInfo = @"package/package_details";//Package Info
static NSString *kPackageDelete = @"package/delete_package";//Package Delete
static NSString *kPackageTrakingInfo = @"package/tracking_details";//Package Tracking Details
static NSString *kAngelerList = @"package/choose_itinerary";//Choose Angeler List
static NSString *kAngelerData = @"package/chosen_itinerary_details";//Chosen Angeler Details

static NSString *kPostAngelerData = @"package/book_itinerary_service";//Booking Angeler
static NSString *kPackageRequestList = @"package/request_list";//Package Request List
static NSString *kPackageEstimatedCost = @"package/estimated_service_cost"; //Services Cost For Package
static NSString *kDeliveryPointList = @"delivery_points/delivery_point_list";//Delivery Point List
static NSString *kAddDeliveryPoint = @"delivery_points/add_delivery_point";//Add Delivery Point
static NSString *kValidateCodeAtDeliveryPoint = @"delivery_points/validate_code";//Delivery Point - Validate Code
static NSString *kDeliveryPointCodeDetail = @"delivery_points/code_details";//Delivery Point - Code Details
static NSString *kDeliveryPointStatusUpdate = @"delivery_points/update_package_status";//Delivery  Point - Status Update
static NSString *kDeliveryPointPackageList = @"delivery_points/package_list"; //DP - Package List
static NSString *kDeliveryPointPackageDetail = @"delivery_points/package_details"; //DP - Package Details

static NSString *kDeliveryList = @"recipient/package_list";//Recipient Package List
static NSString *kDeliveryDetails = @"recipient/delivery_details";//Recipient Delivery Details

static NSString *kUserSelectCountryList = @"user_settings/user_selection_country_list";


static NSString *kResponseSuccess = @"SUCCESS";

//Wallet
static NSString *kWalletDetails = @"user_settings/get_wallet_and_lasttxns_details";
static NSString *kTransactionList = @"user_settings/get_txns_list";
static NSString *kTransferCredit = @"user_settings/share_credits";
static NSString *kBankList = @"user_settings/get_bank_list";
static NSString *kUserSaveBankDetails = @"user_settings/save_bank_details";

//Taxi

//Common

static NSString *kTrackCurrentLocation = @"user_settings/last_location_update"; 
static NSString *kGetTaxiDeriverRiderStatus = @"user_settings/taxi_dashboard_initialization";

//Customer Flow
static NSString *kNearByDriverList = @"rider/nearby_drivers";
static NSString *kCustomerBookTaxi = @"rider/book_trip";
static NSString *kCustomerGetRideDetails = @"rider/ride_details";
static NSString *kCustomerGetDriverLocationDetails = @"rider/driver_location_details";
static NSString *kCustomerCancellationReasonList = @"rider/cancellation_reasons_list";
static NSString *KCustomerCancelBooking = @"rider/cancel_booking";


//Driver Flow

static NSString *kDriverRequestList = @"driver/pending_ride_list";
static NSString *kDriverGetRideDetails = @"driver/ride_details";
static NSString *kDriverUpdateRideStatus = @"driver/update_ride_status";
static NSString *kDriverCancellationReasonList = @"driver/cancellation_reasons_list";
static NSString *KDriverVerificationDetailsUpdate = @"driver/driver_verification_details_update";
static NSString *KDriverCancelBooking = @"driver/cancel_booking";



//===== Charity =====

static NSString *kUserAddCharity     = @"charity/add_charity";
static NSString *kUserGetCharityList = @"charity/charity_list";
static NSString *kUserGetCharityDetails = @"charity/charity_details";


//===== Duty Free =====

static NSString *kUserSelectAirportList = @"dutyFree/airport_list";
static NSString *kUserAddBuyerShoppingPost = @"dutyFree/add_shopping";
static NSString *kUserShoppingList = @"dutyFree/shopping_list";
static NSString *kUserAcceptShopping = @"dutyFree/accept_shopping";
static NSString *kUserShoppingDetails = @"dutyFree/shopping_details";
static NSString *kUserUpdateShoppingDeliverStatus = @"dutyFree/update_shopping_deliver_status";
static NSString *kUserCancelShoppingRequest = @"dutyFree/cancel_shopping_request";
static NSString *kUserGetAddChatMessage = @"dutyFree/add_list_message";


//===== Local Delivery =====

static NSString *kUserAddLocalDeliveryItem = @"delivery/add_delivery";
static NSString *kUserEditLocalDeliveryItem = @"delivery/edit_delivery";
static NSString *kUserLocalDeliveryList = @"delivery/delivery_lists";
static NSString *kUserAcceptLocalDelivery = @"delivery/accept_delivery";
static NSString *kUserCancelLocalDeliveryReqest = @"delivery/cancel_deliver_request";
static NSString *kUserLocalDeliveryDetails = @"delivery/delivery_details";
static NSString *kUserLocalDeliveryCollectItem = @"delivery/collect_delivery";
static NSString *kUserLocalDeliveryDropItem = @"delivery/drop_delivery";
static NSString *kUserLocalDeliverySendReminderReq = @"delivery/send_reminder";
#endif /* WebserviceHeader_h */

/*Angeler Status Check*/ // removed

/*Pickup Dropoff List*/  // removed



/*
 */

