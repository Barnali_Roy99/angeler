//
//  UIPageControl+UIPageControl_Border.m
//  Angeler
//
//  Created by AJAY on 21/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "UIPageControl+UIPageControl_Border.h"

@implementation UIPageControl (UIPageControl_Border)


- (void) customPageControlWithFillColor:(UIColor*)dotFillColor borderColor:(UIColor*)dotBorderColor borderWidth:(CGFloat)dotBorderWidth {
    for (int pageIndex = 0; pageIndex < self.numberOfPages; pageIndex++) {
        UIView* dotView = [self.subviews objectAtIndex:pageIndex];
        if (self.currentPage == pageIndex) {
           // dotView.backgroundColor = dotFillColor;
           // dotView.layer.cornerRadius = dotView.frame.size.height / 2;
        } else {
           // dotView.backgroundColor = [UIColor clearColor];
           // dotView.layer.cornerRadius = dotView.frame.size.height / 2;
            dotView.layer.borderColor = dotBorderColor.CGColor;
            dotView.layer.borderWidth = dotBorderWidth;
        }
    }
}

@end
