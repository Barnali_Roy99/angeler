//
//  UIView+UIview_General.m
//  Angeler
//
//  Created by AJAY on 13/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import "UIView+UIview_General.h"

@implementation UIView (UIview_General)


-(void)makeCornerRound:(CGFloat)cornerRadius
{
    self.layer.cornerRadius = cornerRadius;
}

-(void)makeCornerRound:(CGFloat)cornerRadius withBoarderColor:(UIColor*)viewBorderColor borderWidth:(CGFloat)width
{
    self.layer.cornerRadius = cornerRadius;
    self.layer.borderColor = [viewBorderColor CGColor];
    self.layer.borderWidth = width;
    
}

-(void)makeRoundwithBoarderColor:(UIColor*)viewBorderColor borderWidth:(CGFloat)width
{
    self.layer.cornerRadius = self.frame.size.width / 2;;
    self.layer.borderColor = [viewBorderColor CGColor];
    self.layer.borderWidth = width;
    
}

-(void)makeRound
{
    self.layer.cornerRadius = self.bounds.size.width / 2;
    self.layer.masksToBounds = YES;
   
}

-(void)dropShadowWithColor:(UIColor*)shadowColor
{

    self.layer.shadowColor = [shadowColor CGColor];
    self.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.layer.shadowOpacity = 0.4f;
 //   [v.layer setShadowRadius:3.0];
  
}

-(void)roundSpicficCorner:(UIRectCorner)corners withRadius:(CGFloat)cornerRadius
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                   byRoundingCorners:corners
                                                                     cornerRadii:CGSizeMake(cornerRadius,cornerRadius)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}


    
@end
