//
//  UILabel+General.h
//  Angeler
//
//  Created by AJAY on 17/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface UILabel (General)

+(UILabel*)setSpecificTextHighlight:(NSString*)text withFullText:(NSString*)fullText;

@end

NS_ASSUME_NONNULL_END
