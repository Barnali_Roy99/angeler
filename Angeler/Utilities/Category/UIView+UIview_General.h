//
//  UIView+UIview_General.h
//  Angeler
//
//  Created by AJAY on 13/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (UIview_General)

- (void)makeCornerRound:(CGFloat)cornerRadius;
-(void)makeCornerRound:(CGFloat)cornerRadius withBoarderColor:(UIColor*)viewBorderColor borderWidth:(CGFloat)width;
-(void)makeRound;
-(void)makeRoundwithBoarderColor:(UIColor*)viewBorderColor borderWidth:(CGFloat)width;
-(void)dropShadowWithColor:(UIColor*)shadowColor;
-(void)roundSpicficCorner:(UIRectCorner)corners withRadius:(CGFloat)cornerRadius;

@end

NS_ASSUME_NONNULL_END
