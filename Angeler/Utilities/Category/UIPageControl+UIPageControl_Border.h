//
//  UIPageControl+UIPageControl_Border.h
//  Angeler
//
//  Created by AJAY on 21/06/19.
//  Copyright © 2019 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIPageControl (UIPageControl_Border)
- (void) customPageControlWithFillColor:(UIColor*)dotFillColor borderColor:(UIColor*)dotBorderColor borderWidth:(CGFloat)dotBorderWidth;

@end

NS_ASSUME_NONNULL_END
