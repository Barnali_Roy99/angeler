//
//  UIWindow+VisibleViewController.h
//  Angeler
//
//  Created by AJAY on 20/06/17.
//  Copyright © 2017 RPI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (VisibleViewController)
- (UIViewController *) visibleViewController;
@end
