//
//  UILabel+General.m
//  Angeler
//
//  Created by AJAY on 17/02/20.
//  Copyright © 2020 RPI. All rights reserved.
//

#import "UILabel+General.h"

@implementation UILabel (General)


+(UILabel*)setSpecificTextHighlight:(NSString*)text withFullText:(NSString*)fullText{
    UILabel *boldLabel = [[UILabel alloc] init];
   
//    NSString *TC_text = NSLocalizedString(@"TermsConditions_Text", nil) ;
//    NSString *TC_underLineText = NSLocalizedString(@"TermsConditions_UnderlineText", nil) ;
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:TC_text];
 //   NSAttributedString *boldText = [Helper getBolHighlightedText:fullText //withRange:[fullText rangeOfString:text] withColor:[UIColor appThemeBlueColor]];
//    NSLog(@"boldText %@", boldText);
//    boldLabel.attributedText = boldText;
    
    
    
    // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
//    layoutManager = [[NSLayoutManager alloc] init];
//    textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
//    textStorage = [[NSTextStorage alloc] initWithAttributedString:attributedString];
//
//    // Configure layoutManager and textStorage
//    [layoutManager addTextContainer:textContainer];
//    [textStorage addLayoutManager:layoutManager];
//
//    // Configure textContainer
//    textContainer.lineFragmentPadding = 0.0;
//    textContainer.lineBreakMode = self.lineBreakMode;
//    textContainer.maximumNumberOfLines = self.numberOfLines;
    [boldLabel setGesture];
    return  boldLabel;
    
}

-(void)setGesture
{
    [self setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)];
    [self addGestureRecognizer:tapges];
    
}

- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture
{
    
    NSLog(@"Name label tapped");
    
//    NSString *TC_text = NSLocalizedString(@"TermsConditions_Text", nil) ;
//    NSString *TC_underLineText = NSLocalizedString(@"TermsConditions_UnderlineText", nil) ;
//    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
//    CGSize labelSize = tapGesture.view.bounds.size;
//    CGRect textBoundingBox = [layoutManager usedRectForTextContainer:textContainer];
//    CGPoint textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
//                                              (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
//    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
//                                                         locationOfTouchInLabel.y - textContainerOffset.y);
//    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
//                                                       inTextContainer:textContainer
//                              fractionOfDistanceBetweenInsertionPoints:nil];
//    NSRange linkRange = [TC_text rangeOfString:TC_underLineText];
//    if (NSLocationInRange(indexOfCharacter, linkRange)) {
//        [self redirectToTermsConditionsScreen];
//    }
    
}

    
@end
