//
//  WebServiceApi.h
//  Improve
//
//  Created by Vivek Kumar Jain on 23/04/15.
//  Copyright (c) 2015 Developer's. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    REQMETHOD_GET = 0,
    REQMETHOD_POST = 1,
    REQMETHOD_PUT = 2,
} REQMETHOD;

@interface WebServiceApi : NSObject



//FOR MULTIPLE IMAGE UPLOAD
- (void)reqServiceWithApi:(NSString *)apiName
                                 Method:(REQMETHOD)aMethod
                             parameters:(NSDictionary *)parameters
                                  media:(NSArray *)mediaArray
                         viewController:(UIViewController *)viewController
                                success:(void (^)(id responseObject))success
                                failure:(void (^)(id responseObject))failure;

#pragma mark -
#pragma mark - Initialize
- (id)init;

#pragma mark -
#pragma mark - Shared WebServiceApi [Singleton Method]
+ (WebServiceApi *)sharedWebServiceApi;


@end
