//
//  WebServiceApi.m
//  Improve
//
//  Created by Vivek Kumar Jain on 23/04/15.
//  Copyright (c) 2015 Developer's. All rights reserved.
//

#import "AppDelegate.h"
#import "WebServiceApi.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "UIImage+GIF.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@implementation WebServiceApi

typedef void (^ReqSuccessBlock)(NSURLSessionTask * operation, id responseObject);
typedef void (^ReqFailedBlock)(NSURLSessionTask *operation, NSError *error);

#pragma mark -
#pragma mark - Initialize
- (id)init {
    if (self == [super init]) {
        // Custom Initialisation
    }
    return self;
}

#pragma mark -
#pragma mark - Shared WebServiceApi [Singleton Method]
+ (WebServiceApi *)sharedWebServiceApi {
    static WebServiceApi *objWebServiceApi = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objWebServiceApi = [[WebServiceApi alloc] init];
    });
    return objWebServiceApi;
}


//////////////////////////////////////////

-(void) showLoadingHUD {
    
    UIViewController *currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController visibleViewController];
    
     MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:currentVc.view animated:YES];
    UIImageView *imageViewAnimatedGif = [[UIImageView alloc]init];
    NSString *filePath = [[NSBundle mainBundle] pathForResource: @"loader" ofType: @"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile: filePath];
    imageViewAnimatedGif.image = [UIImage sd_animatedGIFWithData:gifData];
    HUD.backgroundColor = [UIColor clearColor];
    HUD.customView.backgroundColor = [UIColor clearColor];
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.color = [UIColor clearColor];
    HUD.customView = [[UIImageView alloc] initWithImage:imageViewAnimatedGif.image];
    [HUD show:YES];
    
    
}

-(void)HideAllLoadingHude
{
    [Helper HideAllLoadingHude];
}

-(void)HideDelayAllLoadingHude{
    [Helper HideAllLoadingHude];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:TaxiPostNotificationCustomerDelayTimerBookingEnd
     object:nil userInfo:nil];
}
- (void)reqServiceWithApi:(NSString *)apiName
                                 Method:(REQMETHOD)aMethod
                             parameters:(NSDictionary *)parameters
                                  media:(NSArray *)mediaArray
                         viewController:(UIViewController *)viewController
                                success:(void (^)(id responseObject))success
                                failure:(void (^)(id responseObject))failure{
    
    BOOL isShowNoNetworkAlert = true;
    
    //services called in background as timer will not show network alert
    if([apiName isEqualToString:kTrackCurrentLocation] || [apiName isEqualToString:kCustomerGetDriverLocationDetails] || [apiName isEqualToString:kGoogleMapDirectionApi])
    {
        isShowNoNetworkAlert = false;
    }
    
    if (![self isNetReachable]){
        if(isShowNoNetworkAlert)
        [self showNetworkIssue];
        return;
    }
    
    
   

    if ([parameters objectForKey:@"HideHUD"] == nil && viewController != nil )
    {
//        [SVProgressHUD show];
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLoadingHUD];
        });
    }
    
    
    //============== success block================
    ReqSuccessBlock successBlock = ^(NSURLSessionTask * operation, id responseObject) {
        
        
        if ([parameters objectForKey:@"HideHUD"] == nil )
        {
    
                [self HideAllLoadingHude];
           
        }
        
        if ([parameters objectForKey:@"IsDelayHideHUD"] != nil )
        {
            [self performSelector:@selector(HideDelayAllLoadingHude) withObject:self afterDelay:60.0 ]; //For customer taxi booking delay timer for 60 sec.
            
        }
        
      
        
        if (!success)
            return;
        
        success(responseObject);
    };
    
    //============== failure block================
    ReqFailedBlock failedBlock = ^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Failure  block : %@",error);
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"sssssssss%@",ErrorResponse);
        
     
        
        if ([parameters objectForKey:@"HideHUD"] == nil )
        {
            [self HideAllLoadingHude];
        }
        if (error.code == -1004) {
            if(isShowNoNetworkAlert)
            [self showNetworkIssue];
        }
        if (!failure) {
            return ;
        }
        
        failure(error);
    };
    
    
    //========Finally AFNetworking Guys will do the job for me; need to give them instruction!
    
    NSString *_strServerUrl = [NSString stringWithFormat:@"%@/%@",kApiUrl,apiName];
    if ([apiName isEqualToString:kGoogleMapApi]) {
        _strServerUrl = [NSString stringWithFormat:@"%@latlng=%@,%@&key=%@",googlemaps,[parameters valueForKey:@"lat"],[parameters valueForKey:@"long"],GOOGLE_API_KEY];
        parameters = nil;
    }
    else if([apiName isEqualToString:kGoogleMapDirectionApi]){
         _strServerUrl = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", googlemapsDirectionURL, [parameters valueForKey:@"originString"], [parameters valueForKey:@"destinationString"],GOOGLE_API_KEY];
        parameters = nil;
    }
    
    NSLog(@"The url in service page=%@",_strServerUrl);
   
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    //[requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
    //    [requestSerializer setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    [manager setRequestSerializer:requestSerializer];
    
    
    //==========NOTE:: uncomment below line if you want to avoid 'Request Timeout' issue
    //manager.requestSerializer.timeoutInterval = 50000;
    
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
   
    [responseSerializer setReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [NSMutableSet setWithObjects:@"text/html",@"text/json",@"application/json",@"text/javascript",@"multipart/form-data",nil];
    [manager setResponseSerializer:responseSerializer];
    

    //[Fabric with:@[[Crashlytics class]]];
    
    NSString *_testUrl = _strServerUrl; //[NSString stringWithFormat:@"%@/%@",kApiUrl,apiName];
    if (parameters != nil && parameters.allKeys.count > 0) {
        NSArray *allKey = parameters.allKeys;
        NSString *firstKey = [allKey firstObject];
        _testUrl = [NSString stringWithFormat:@"%@?%@=%@",_testUrl,firstKey,[parameters objectForKey:firstKey]];
        for (NSString *key in parameters.allKeys) {
            if (![key isEqualToString:firstKey]) {
                _testUrl = [NSString stringWithFormat:@"%@&%@=%@",_testUrl,key,[parameters objectForKey:key]];
            }
        }
    }
    
    NSString *tempTestUrl = _testUrl;
    NSLog(@"testurl-----  %@",_testUrl);
    
    [[Crashlytics sharedInstance] setObjectValue:_testUrl forKey:@"APICALL"];
    
//    [mediaData setObject:strImageType forKey:@"imageType"];
//    [mediaData setObject:_imageData forKey:@"MediaData"];
//    [mediaData setObject:@"doctors_document" forKey:@"TagName"];
//    [mediaData setObject:@"image" forKey:@"MediaType"];
    
//    NSString *encodeUrl = [_strServerUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    [_strServerUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    if(mediaArray !=nil) {
        
        //FeedbackImage1,FeedbackImage2,FeedbackImage3,FeedbackVideo
        
        
       
        [manager POST:_strServerUrl parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            ////IMAGE ONE
            for (NSDictionary *media in mediaArray) {
               
                if (media!=nil) {
                    if ([[NSString stringWithFormat:@"%@",[media objectForKey:@"MediaType"]] isEqualToString:@"image"]) {
                        NSString *imageTypeExt = [media objectForKey:@"imageType"];
                        NSString *mime_type = Empty;
                        NSString *fileExtn = Empty;
                        if ([imageTypeExt isEqualToString:@"jpeg"]) {
                            mime_type = @"image/jpeg";
                            fileExtn = @"jpg";
                            
                            
                            
                        }
                        else {
                            
                            mime_type = @"image/png";
                            fileExtn = @"png";
                        }
                        
                        NSData *imgData = [media objectForKey:@"MediaData"];
                        if (imgData!= nil) {
                            
                            
                            [formData appendPartWithFileData:imgData name:[media objectForKey:@"TagName"] fileName:[NSString stringWithFormat:@"Image.%@",fileExtn] mimeType:mime_type];
                        }
                        
                        
                        
                    } else {
                        if ([media objectForKey:@"VideoData"]!=nil) {
                            NSData *videoData=[media objectForKey:@"MediaData"];
                            if (videoData != nil) {
                                
                                [formData appendPartWithFileData:videoData name:[media objectForKey:@"videoTagName"] fileName:@"FeedbackVideo.mov" mimeType:@"video/quicktime"];
                            }
                        }
                    }
                    
                    
                }
                
                
            }
        } progress:nil success:successBlock failure:failedBlock];
    }
    else {
        
        // send request
        if (aMethod == REQMETHOD_GET){
            
          
            
            [manager GET:_strServerUrl parameters:parameters headers:nil progress:nil success:successBlock failure:failedBlock];
        }
        else if(aMethod == REQMETHOD_POST){
//            [manager POST:_strServerUrl parameters:parameters progress:nil success:successBlock failure:failedBlock];
            
        
            [manager POST:_strServerUrl parameters:parameters headers:nil constructingBodyWithBlock:nil progress:nil success:successBlock failure:failedBlock];
        }
        else if(aMethod == REQMETHOD_PUT){
            [manager PUT:_strServerUrl parameters:parameters headers:nil success:successBlock failure:failedBlock];
        }
    }
}





-(BOOL)isNetReachable
{
    Reachability *_reach = [Reachability reachabilityForInternetConnection];
    if([_reach currentReachabilityStatus] == NotReachable)
        return false;
    return true;
}

- (void)showNetworkIssue
{
    [Helper showAlertWithTitle:NSLocalizedString(@"No_Internet_Alert", nil) Message:Empty];
    
}

@end
