//
//  LocationShareModel.m
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location. All rights reserved.
//

#import "LocationManager.h"
#import <UIKit/UIKit.h>


@interface LocationManager () <CLLocationManagerDelegate,UIAlertViewDelegate>

@end


@implementation LocationManager

//Class method to make sure the share model is synch across the app
+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    
    return sharedMyModel;
}
-(instancetype)init
{
    self=[super init];
    if (self) {
        [self createLocationManagerObject];
    }
    return self;
}
- (void)createLocationManagerObject{
    _anotherLocationManager = [[CLLocationManager alloc] init];
    _anotherLocationManager.delegate = self;
    _anotherLocationManager.distanceFilter=YES;
    _anotherLocationManager.pausesLocationUpdatesAutomatically = false;
    _anotherLocationManager.allowsBackgroundLocationUpdates = true;
    _anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBest;
}

#pragma mark - CLLocationManager

- (void)startMonitoringLocation {
    NSLog(@"startMonitoringLocation");
   
//    if(IS_OS_8_OR_LATER){
//
//        NSUInteger code = [CLLocationManager authorizationStatus];
//        if (code == kCLAuthorizationStatusNotDetermined && ([_anotherLocationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [_anotherLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
//            // choose one request according to your business.
//            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
//
//                [_anotherLocationManager requestAlwaysAuthorization];
//
//            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
//
//                [_anotherLocationManager  requestWhenInUseAuthorization];
//                [_anotherLocationManager startUpdatingLocation];
//
//            } else {
//                [_anotherLocationManager startUpdatingLocation];
//            }
//        }
//        else{
//            [_anotherLocationManager startUpdatingLocation];
//        }
//    }
//    else{
//        [_anotherLocationManager startUpdatingLocation];
//    }

    
    UIDevice *currentDevice = [UIDevice currentDevice];
    if ([currentDevice.model rangeOfString:@"Simulator"].location == NSNotFound) {
        
        if(IS_OS_8_OR_LATER){
            NSUInteger code = [CLLocationManager authorizationStatus];
            if (code == kCLAuthorizationStatusNotDetermined && ([self.anotherLocationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.anotherLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
                // choose one request according to your business.
                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                    [self.anotherLocationManager requestAlwaysAuthorization];
                } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                    [self.anotherLocationManager  requestWhenInUseAuthorization];
                } else {
                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
                }
            }
        }
        
        if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined && [self.anotherLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
            [self.anotherLocationManager requestWhenInUseAuthorization];
        }
        
        [self.anotherLocationManager startUpdatingLocation];
        
    }
}

#pragma mark - CLLocationManager Delegate



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    NSLog(@"locationManager didUpdateLocations: %@",locations);
    
//    for (int i = 0; i < locations.count; i++) {
    
        CLLocation * newLocation = [locations lastObject];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        
        NSLog(@"Current location Latitude %f Longitude %f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
        self.myLocation = theLocation;
        self.myLocationAccuracy = theAccuracy;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"CurrenLocationTrack"
     object:nil];
        
 
//    if (self.myLocation.latitude) {
//        [manager stopUpdatingLocation];
//    }

   
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}


//- (void)startLocationUpdate
//{
//
//    if (nil == self.location)
//        self.location = [[CLLocationManager alloc] init];
//    self.location.delegate = self;
//    self.location.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
//    if([self.location respondsToSelector:@selector(requestWhenInUseAuthorization)])
//        [self.location requestWhenInUseAuthorization];
//    self.location.distanceFilter = 500;
//    [self.location startUpdatingLocation];
//    [self isLocationEnabled];
//}


#pragma mark - Location Alert
//- (void)isLocationEnabled
//{
//    if ([CLLocationManager locationServicesEnabled]){
//
//        NSLog(@"Location Services Enabled");
//
//        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
//            NSString *enableGPS = NSLocalizedString(@"Your location settings seem to be disabled. Please enable location. Settings -> Privacy -> Location Services",nil);
//            if (![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
//            {
//
//                UIAlertView *gpsErrorAlert = [[UIAlertView alloc] initWithTitle:@"" message:enableGPS delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Cancel", nil), NSLocalizedString(@"OK", nil), nil];
//                [gpsErrorAlert dismissWithClickedButtonIndex:0 animated:NO];
//                [gpsErrorAlert show];
//            }
//        }
//    }
//
//}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1)
    {
        
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

@end
