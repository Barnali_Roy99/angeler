

#ifndef DoTell_C_macro_h
#define DoTell_C_macro_h




//Device size History and iOS Version==================//
//          iPhone 3GS = iOS 6.1.6 [3.5 inch]          //
//          iPhone 4   = iOS 7.1.2 [3.5 inch]          //
//          iPhone 4S  = iOS 8.1.1 [3.5 inch]          //
//          iPhone 5   = iOS 8.1.1 [4 inch]            //
//          iPhone 5C  = iOS 8.1.1 [4 inch]            //
//          iPhone 5S  = iOS 8.1.1 [4 inch]            //
//          iPhone 6   = iOS 8.1.1 [4.7 inch]          //
//          iPhone 6+  = iOS 8.1.1 [5.5 inch]          //
//=====================================================//


/* System Versioning Preprocessor Macros */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

/*
 *  Usage
 */

//if (SYSTEM_VERSION_LESS_THAN(@"4.0")) {
//    ...
//}
//
//if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"3.1.1")) {
//    ...
//}





// OS Information
#define IOS_5_OR_LATER [[[UIDevice currentDevice] systemVersion] compare:@"5.0" options:NSNumericSearch] != NSOrderedAscending
#define IOS_4_OR_LATER [[[UIDevice currentDevice] systemVersion] compare:@"4.0" options:NSNumericSearch] != NSOrderedAscending
#define ONLY_IF_AT_LEAST_IOS_4(action) if ([[[UIDevice currentDevice] systemVersion] compare:@"4.0" options:NSNumericSearch] != NSOrderedAscending) { action; }
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPHONE_SIMULATOR [[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"]
#define IS_IPAD_SIMULATOR [[[UIDevice currentDevice] model] isEqualToString:@"iPad Simulator"]

#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )

#define IS_IPHONE5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667 ) < DBL_EPSILON )
#define IS_IPHONE6Plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736 ) < DBL_EPSILON )
#define IS_IPHONE4S MainScreenHeight==480

#define IS_PAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad





// App Information
#define AppName                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]
#define AppVersion              [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
#define AppDel   ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define kAppDelegate(type)       ((type *)[[UIApplication sharedApplication] delegate])
#define NSAppDelegate(type)     ((type *)[[NSApplication sharedApplication] delegate])
#define SharedApp               [UIApplication sharedApplication]
#define NSSharedApp             [NSApplication sharedApplication]
#define Bundle                  [NSBundle mainBundle]
#define MainScreen              [UIScreen mainScreen]
#define RootViewController(type) ([[(type *)[[UIApplication sharedApplication] delegate] window] rootViewController])

#define UserDefault [NSUserDefaults standardUserDefaults]





// Actions
#define OpenURL(urlString) [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:urlString]]







// User Default Preferences
#define UDValue(x)              [[NSUserDefaults standardUserDefaults] valueForKey:(x)]
#define UDBool(x)               [[NSUserDefaults standardUserDefaults] boolForKey:(x)]
#define UDInteger(x)            [[NSUserDefaults standardUserDefaults] integerForKey:(x)]
#define UDSetValue(x, y)        [[NSUserDefaults standardUserDefaults] setValue:(y) forKey:(x)]
#define UDSetBool(x, y)         [[NSUserDefaults standardUserDefaults] setBool:(y) forKey:(x)]
#define UDSetInteger(x, y)      [[NSUserDefaults standardUserDefaults] setInteger:(y) forKey:(x)]
#define UDObserveValue(x, y)    [[NSUserDefaults standardUserDefaults] addObserver:y forKeyPath:x options:NSKeyValueObservingOptionOld context:nil];
#define UDSync(ignored)         [[NSUserDefaults standardUserDefaults] synchronize]







// Network Activity Indicator
#define ShowNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
#define SetNetworkActivityIndicator(x)      [UIApplication sharedApplication].networkActivityIndicatorVisible = x







// User Interface [Color]
#define HexColor(c)                         [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
#define RGB(r, g, b)                        [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define NSHexColor(c)                       [NSColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
#define NSRGB(r, g, b)                      [NSColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define NSRGBA(r, g, b, a)                  [NSColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]






// User Interface [UI Elements]
#define NavBar                              self.navigationController.navigationBar
#define TabBar                              self.tabBarController.tabBar
#define NavBarHeight                        self.navigationController.navigationBar.bounds.size.height
#define TabBarHeight                        self.tabBarController.tabBar.bounds.size.height
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
#define TouchHeightDefault                  44
#define TouchHeightSmall                    32
#define ViewWidth(v)                        v.frame.size.width
#define ViewHeight(v)                       v.frame.size.height
#define ViewX(v)                            v.frame.origin.x
#define ViewY(v)                            v.frame.origin.y
#define BARBUTTONSYSTEM(TYPE, SELECTOR) [[UIBarButtonItem alloc] initWithBarButtonSystemItem:TYPE target:self action:SELECTOR]





// Rect stuff
#define CGWidth(rect)                   rect.size.width
#define CGHeight(rect)                  rect.size.height
#define CGOriginX(rect)                 rect.origin.x
#define CGOriginY(rect)                 rect.origin.y
#define CGRectCenter(rect)              CGPointMake(NSOriginX(rect) + NSWidth(rect) / 2, NSOriginY(rect) + NSHeight(rect) / 2)
#define CGRectModify(rect,dx,dy,dw,dh)  CGRectMake(rect.origin.x + dx, rect.origin.y + dy, rect.size.width + dw, rect.size.height + dh)







// Debugging Timer
#define StartTimer(ignored)     NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
#define EndTimer(msg)           NSTimeInterval stop = [NSDate timeIntervalSinceReferenceDate]; NSLog(@"%@", [NSString stringWithFormat:@"%@ Time = %f", msg, stop-start]);

// Target > Get Info > Build > GCC_PREPROCESSOR_DEFINITIONS
// Configuration = Release: <empty>
//               = Debug:   DEBUG_MODE=1

#ifdef DEBUG_MODE
#define DebugLog( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#endif

#define NSLogRect(rect)                 NSLog(@"%@", NSStringFromCGRect(rect))
#define NSLogSize(size)                 NSLog(@"%@", NSStringFromCGSize(size))
#define NSLogPoint(point)               NSLog(@"%@", NSStringFromCGPoint(point))






#define NSLocalCancel NSLocalizedString(@"Cancel", @"Cancel")
#define NSLocalOK NSLocalizedString(@"OK", @"OK")
#define LocalString(x) NSLocalizedString(x,x)


#define FloatTOString(x) [NSString stringWithFormat:@"%f",x]
#define TOString(x) [NSString stringWithFormat:@"%d",x]
#define TONumber(x) [x intValue]

#define RemoveKey(x,y) [x removeObjectForKey:y]
#define ObjForKey(x,y) [x objectForKey:y]
#define IntObjForKey(x,y) [[x objectForKey:y] intValue]
#define SetObjKey(x,y,z) [x setObject:y forKey:z]
/* key, observer, object */
#define ObserveValue(x, y, z) [(z) addObserver:y forKeyPath:x options:NSKeyValueObservingOptionOld context:nil];

#define RGBCOLOR(r,g,b,a) [UIColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:a]

#define MyTmpDirectory [NSString stringWithFormat:@"%@/tmp",NSHomeDirectory()]








//for design in simulator simulator 6


#define MainScreenHeight [[UIScreen mainScreen] bounds].size.height

#define MainScreenWidth [[UIScreen mainScreen] bounds].size.width

#define PARCENTAGEWIDTH6(w) 100*w/375.0f

#define PARCENTAGEX6(x) 100*x/375.0f

#define PARCENTAGEHEIGHT6(h) 100*h/667.0f

#define PARCENTAGEY6(y) 100*y/667.0f

#define PERCENTAGEWIDTHREVERSE5(x) 100.0*320.0f/x



#define AUTO_ADJUST_SPACE5(x) 320*x/MainScreenWidth

#define AUTOX6(x) MainScreenWidth*PARCENTAGEX6(x)/100

#define AUTOY6(y) MainScreenHeight*PARCENTAGEY6(y)/100



#define AUTOWIDTH6(w) MainScreenWidth*PARCENTAGEWIDTH6(w)/100

#define AUTOHEIGHT6(h) MainScreenHeight*PARCENTAGEHEIGHT6(h)/100



/////***********************************



//for design in simulator simulator 5C 5S

#define PARCENTAGEWIDTH5(w) 100*w/320.0f

#define PARCENTAGEX5(x) 100*x/320.0f

#define PARCENTAGEHEIGHT5(h) 100*h/568.0f

#define PARCENTAGEY5(y) 100*y/568.0f







#define AUTOX5(x) MainScreenWidth*PARCENTAGEX5(x)/100

#define AUTOY5(y) MainScreenHeight*PARCENTAGEY5(y)/100



#define AUTOWIDTH5(w) MainScreenWidth*PARCENTAGEWIDTH5(w)/100

#define AUTOHEIGHT5(h) MainScreenHeight*PARCENTAGEHEIGHT5(h)/100

// IPAD AUTO CLASS RESIZE



#define PARCENTAGEWIDTHIPAD(w) 100*w/768.0f

#define PARCENTAGEXIPAD(x) 100*x/768.0f

#define PARCENTAGEHEIGHTIPAD(h) 100*h/1024.0f

#define PARCENTAGEYIPAD(y) 100*y/1024.0f


#define AUTOXIPAD(x) MainScreenWidth*PARCENTAGEXIPAD(x)/100
#define AUTOYIPAD(y) MainScreenHeight*PARCENTAGEYIPAD(y)/100
#define AUTOWIDTHIPAD(w) MainScreenWidth*PARCENTAGEWIDTHIPAD(w)/100
#define AUTOHEIGHTIPAD(h) MainScreenHeight*PARCENTAGEHEIGHTIPAD(h)/100

#define OFFSETVAL 5
#define PHONE_MIN_DIGIT 6
#define MAX_STORE_SELECT 2

#define CORNER_VAL 5.0f
#define ROUNDEDCORNER(layer) layer.cornerRadius = CORNER_VAL




#endif
